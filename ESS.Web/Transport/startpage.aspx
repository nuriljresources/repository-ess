﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Transport/Transport.Master" CodeBehind="startpage.aspx.vb" Inherits="EXCELLENT.startpage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        ul#navigation li a.m1
        {        	
	        background-color:#00CC00;color: #000000;
        }
   </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br /><center>        
       <div><center>
       <table class="data-table" style="border: 1px solid #0066ff; width: 25%">
            <tr>
            <td colspan="2" bgcolor="#0066ff" 
                    
                    style="text-align:center;font-size: large; font-family: calibri; font-weight: bold; width:100%">
                USER LOGIN
            </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label1" runat="server" Text="Domain\UserName" Font-Size="Small">
                </asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox1" runat="server" class="tb10" Width="92%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label2" runat="server" Text="Label" Font-Size="Small">Password</asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox2" TextMode="password" runat="server" class="tb10" Width="92%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">v.1.0.0</td>                
            </tr>
            <tr>
                <td style="width:40%">
                    <img alt="" src="../images/Locked.png" />
                </td>
                <td style="width:60%">
                    <asp:Button ID="btnLogin" runat="server" Text="Login" Width="80" Font-Names="Calibri"/>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="80" Font-Names="Calibri" />
                </td>
            </tr>
            <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblConfirm" runat="server" Text="" Font-Size="Small" ForeColor="Red"></asp:Label>
            </td>
            </tr>
        </table> </center> 
    </div>
    </center>
</asp:Content>