﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="business_trip_list.aspx.vb" Inherits="EXCELLENT.business_trip_list" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 932px">
        <asp:TextBox ID="mth" runat="server" Visible="false">1</asp:TextBox><br />
        <asp:TextBox ID="yr" runat="server" Visible="false">2013</asp:TextBox><br />
        <asp:TextBox ID="nik" runat="server" Visible="false">JRN0010</asp:TextBox><br />
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" Height="536px" Width="888px">
            <LocalReport ReportPath="Laporan\business_trip_list.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource1" 
                        Name="DataSet6_V_H0011" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:jrn_citrixcon %>" 
            
            SelectCommand="SELECT TripNo, nik, sup_nik, detailTrip, purpose, tripfrom, tripto, apprby, apprdate, CASE fstatus WHEN 1 THEN 'Disable' WHEN 0 THEN 'enable' END AS fstatus, editGA FROM V_H001 WHERE (MONTH(reqdate) = @mth) AND (YEAR(reqdate) = @yr) AND (nik = @nik) AND (StEdit &lt;&gt; 2)" 
            ProviderName="<%$ ConnectionStrings:jrn_citrixcon.ProviderName %>">
            <SelectParameters>
                <asp:ControlParameter ControlID="mth" Name="mth" PropertyName="Text" />
                <asp:ControlParameter ControlID="yr" Name="yr" PropertyName="Text" />
                <asp:ControlParameter ControlID="nik" Name="nik" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            SelectMethod="GetData" 
            TypeName="EXCELLENT.DataSet6TableAdapters.V_H0011TableAdapter">
        </asp:ObjectDataSource>
    
    </div>
    </form>
</body>
</html>
