﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="print.aspx.vb" Inherits="EXCELLENT.print" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Report - Performance Appraisal</title>
<style type="text/css">

body{
  padding: 1em;
  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAAQElEQVQIW2P89OvDfwYo+PHjJ4zJwMHBzsAIk0SXAKkCS2KTAEu++vQSbizIKGQAl0SXAJkGlsQmAbcT2Shk+wH0sCzAEOZW1AAAAABJRU5ErkJggg==);
}
a{
  color: #739931;
}
.page{
  max-width: 79em;
  margin: 0 auto;
}
table th,
table td{
  text-align: left;
}
table.layout{
  width: 100%;
  border-collapse: collapse;
}
table.display{
  margin: 1em 0;
}
table.display th,
table.display td{
  border: 1px solid #B3BFAA;
  padding: .5em 1em;
}

table.display th{ background: #D5E0CC; }
table.display td{ background: #fff; }

table.responsive-table{
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
}

.center{
  text-align:center;
  }
.none{
	border-style:none;
    text-align:center;
  }
  
  /* Accordion */
.accordionHeader
{
    border: 1px solid #2F4F4F;
    color: white;
    background-color: #2E4d7B;
	font-family: Arial, Sans-Serif;
	font-size: 12px;
	font-weight: bold;
    padding: 5px;
    margin-top: 5px;
    cursor: pointer;
}

#master_content .accordionHeader a
{
	color: #FFFFFF;
	background: none;
	text-decoration: none;
}

#master_content .accordionHeader a:hover
{
	background: none;
	text-decoration: underline;
}

.accordionHeaderSelected
{
    border: 1px solid #2F4F4F;
    color: white;
    background-color: #5078B3;
	font-family: Arial, Sans-Serif;
	font-size: 12px;
	font-weight: bold;
    padding: 5px;
    margin-top: 5px;
    cursor: pointer;
}

#master_content .accordionHeaderSelected a
{
	color: #FFFFFF;
	background: none;
	text-decoration: none;
}

#master_content .accordionHeaderSelected a:hover
{
	background: none;
	text-decoration: underline;
}

.accordionContent
{
    background-color: #D3DEEF;
    border: 1px dashed #2F4F4F;
    border-top: none;
    padding: 5px;
    padding-top: 10px;
}
/*end accordion*/
  
  #footer {
	background:#373737;
	clear:both;
	bottom: 0;
	position: relative;
	margin-top: -50px; /* negative value of footer height */
	height: 20px;
	clear:both;
	}
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 
	 FOOTER
	 	 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

.nav-footer
{
	position: relative;
	text-align: center;
	clear: both;
}

.nav-footer ul{
	padding:0;
	margin:0;
	text-align:center;
	}

.nav-footer li{
	background:none;
	display:inline;
	border-right:1px dotted #686868;
	padding:0 10px;
	}

.nav-footer li.first {
	border-left:1px dotted #686868;
	}
	
.copyright {
	color:#999;
	font-size:.8em;
	clear:both;
	}

.nav-footer a:link,
.nav-footer a:visited {
	color:#CCC;
	}

@media (max-width: 30em){
    table.responsive-table{
      box-shadow: none;  
    }
    table.responsive-table thead{
      display: none; 
    }
  table.display th,
  table.display td{
    padding: .5em;
  }
    
  table.responsive-table td:nth-child(1):before{
    content: 'Number';
  }
  table.responsive-table td:nth-child(2):before{
    content: 'Name';
  }
  table.responsive-table td:nth-child(1),
  table.responsive-table td:nth-child(2){
    padding-left: 25%;
  }
  table.responsive-table td:nth-child(1):before,
  table.responsive-table td:nth-child(2):before{
    position: absolute;
    left: .5em;
    font-weight: bold;
  }
  
    table.responsive-table tr,
    table.responsive-table td{
        display: block;
    }
    table.responsive-table tr{
        position: relative;
        margin-bottom: 1em;
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
    }
    table.responsive-table td{
        border-top: none;
    }
    table.responsive-table td.organisationnumber{
        background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    table.responsive-table td.actions{
        position: absolute;
        top: 0;
        right: 0;
        border: none;
        background: none;
    }
    table.responsive-table td.top{
    	background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    .crtoolbarsyam
    {
        background-color : Red;
        padding-bottom :100px;
    }
}

</style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="page">
     <div id="div2" style="float:right;">
    <label><a href="ppa_entry.aspx">Home</a></label>
    <%=mreport%>
    <label><a href="ppa_logout.aspx">LogOut</a></label>
    
    </div>
    <img src="../images/logo.png" />
       
       <br /> <br /> 
       <%--<asp:Label ID="lblreportpath" runat="server" ></asp:Label>--%>
       <div style="width: 1280px; height: 788px;position:relative;background-color:White">
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
            AutoDataBind="True" ReportSourceID="CrystalReportSource1" 
            HasCrystalLogo="False" ShowAllPageIds="True" DisplayGroupTree="False" 
            EnableDatabaseLogonPrompt="False" ToolbarStyle-CssClass="crtoolbarsyam" 
               ClientTarget="Auto" ToolbarStyle-BackColor="Silver" />
        <CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
            <Report FileName="PPA\report\CrystalReport1.rpt">
            </Report>
        </CR:CrystalReportSource>
        </div>
    </div>
    </form>
</body>
</html>
