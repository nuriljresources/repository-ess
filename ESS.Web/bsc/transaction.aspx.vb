﻿Imports System.Data.SqlClient
Imports System.Web

Partial Public Class transaction
    Inherits System.Web.UI.Page

    Dim lastgroup As String = "@#@~"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") = "" Or Session("permission") = Nothing Then
            Response.Redirect("../Login.aspx", False)
            Exit Sub
        ElseIf ("1;2;8;3;4;5;6;7;B").IndexOf(Session("permission")) = -1 Then
            Response.Redirect("../default.aspx", False)
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            For Each x As ListItem In ddlYear.Items
                If x.Text = Date.Now.Year Then
                    x.Selected = True
                End If
            Next
            userid.Value = Session("NikUser")
            userip.Value = HttpContext.Current.Request.UserHostAddress
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim dr As SqlDataReader = Nothing
            Try
                Dim l As String = Request.QueryString("l")
                Dim y As String = Request.QueryString("y")
                Dim sqlQuery As String = ""
                Dim cmd As SqlCommand

                conn.Open()
                'Initialize Function
                If Session("permission") = "3" Or Session("permission") = "1" Or Session("permission") = "B" Then 'GRUP BOD dan MSD
                    sqlQuery = "select ID,FunctionName,active from MFunction where active='1' order by dirid,ID"
                ElseIf Session("permission") = "8" Or Session("permission") = "4" Or Session("permission") = "5" Then 'PIC PDCA dan GM dan Manager
                    sqlQuery = "select ID,FunctionName,active from MFunction a inner join MAPFUNC b on a.id=b.[Function] and b.Nik='" & Session("NikUser") & "' where active='1' order by dirid,ID"
                Else
                    sqlQuery = "select ID,FunctionName,active from MFunction where active='1' order by dirid,ID"
                End If
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "3" Or Session("permission") = "1" Or Session("permission") = "B" Then 'GRUP BOD dan MSD
                        KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "8" Or Session("permission") = "4" Or Session("permission") = "5" Then 'PIC PDCA dan GM dan Manager
                        KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Direktorat
                sqlQuery = "select ID,DirektoratName,active from MDirektorat where active='1' order by ID"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Then 'GRUP MDV
                        KPIDirektorat.Items.Add(New ListItem(dr(1), dr(0)))
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Section
                sqlQuery = "select ID,SectionName,active,funcid from MSection where active='1'  order by ID"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Or Session("permission") = "2" Or Session("permission") = "3" Or Session("permission") = "6" Then 'GRUP MDV atau sekpro atau BOD
                        KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "7" Or Session("permission") = "A" Then 'GRUP S HEAD & S HEAD ADMIN
                        If Session("bscdepar") = dr(0) Then
                            KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    ElseIf Session("permission") = "8" Or Session("permission") = "4" Or Session("permission") = "5" Then 'PIC PDCA dan GM dan Manager
                        If Session("bscdivisi") = dr(3) Then
                            KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Jobsite
                sqlQuery = "select id,kdsite,active from site order by id"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Or Session("permission") = "8" Or Session("permission") = "3" Or Session("permission") = "4" Or Session("permission") = "5" Then 'GRUP MDV & PIC PDCA & BOD & GM & MANAGER
                        KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "7" Or Session("permission") = "2" Or Session("permission") = "6" Or Session("permission") = "A" Then 'GRUP S HEAD & SEKPRO & PM & S HEAD ADMIN
                        If Session("jobsite") = dr(1) Then
                            KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                    'If dr(2) = 0 Then
                    '    KPIJobsite.Items(KPIJobsite.Items.Count - 1).Enabled = False
                    'End If
                End While
                dr.Close()
                dr = Nothing

                'Isi KPI Level
                KPILevel.Items.Clear()
                Select Case Session("permission")
                    Case "1"
                        KPILevel.Items.Add(New ListItem("JRN", "0"))
                        KPILevel.Items.Add(New ListItem("Direktorat", "4"))
                        KPILevel.Items.Add(New ListItem("Function", "1"))
                        KPILevel.Items.Add(New ListItem("Jobsite", "3"))
                        KPILevel.Items.Add(New ListItem("Section", "2"))
                    Case "B" 'MSD Func Head
                        KPILevel.Items.Add(New ListItem("JRN", "0"))
                        KPILevel.Items.Add(New ListItem("Direktorat", "4"))
                        KPILevel.Items.Add(New ListItem("Function", "1"))
                        KPILevel.Items.Add(New ListItem("Jobsite", "3"))
                        KPILevel.Items.Add(New ListItem("Section", "2"))
                    Case "2" 'Sek Pro
                        KPILevel.Items.Add(New ListItem("Jobsite", "3"))
                        'KPILevel.Items(1).Selected = True
                    Case "8" 'PIC PDCA
                        KPILevel.Items.Add(New ListItem("Function", "1"))
                        KPILevel.Items.Add(New ListItem("Section", "2"))
                    Case "3" 'BOD
                        KPILevel.Items.Add(New ListItem("JRN", "0"))
                        KPILevel.Items.Add(New ListItem("Direktorat", "4"))
                        KPILevel.Items.Add(New ListItem("Function", "1"))
                        KPILevel.Items.Add(New ListItem("Jobsite", "3"))
                        btnCreate.Visible = False
                    Case "4" 'GM
                        KPILevel.Items.Add(New ListItem("Function", "1"))
                        KPILevel.Items.Add(New ListItem("Section", "2"))
                        btnCreate.Visible = False
                    Case "5" 'Manager 
                        KPILevel.Items.Add(New ListItem("Function", "1"))
                        KPILevel.Items.Add(New ListItem("Section", "2"))
                        btnCreate.Visible = False
                    Case "6" 'PM
                        KPILevel.Items.Add(New ListItem("Jobsite", "3"))
                        btnCreate.Visible = False
                    Case "7" 'S. Head
                        KPILevel.Items.Add(New ListItem("Section", "2"))
                End Select
                'Selected Index Change
                Select Case KPILevel.SelectedValue
                    Case 0
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                        KPIDirektorat.Visible = False
                    Case 1
                        KPIFunc.Visible = True
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                        KPIDirektorat.Visible = False
                    Case 2
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = True
                        KPISection.Visible = True
                        KPIDirektorat.Visible = False
                    Case 3
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = True
                        KPISection.Visible = False
                        KPIDirektorat.Visible = False
                    Case 4
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                        KPIDirektorat.Visible = True
                End Select
                doSearch()
            Catch ex As Exception
                lblMessage.Text = ex.Message
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        End If
    End Sub

    Protected Sub KPILevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles KPILevel.SelectedIndexChanged
        Try
            Select Case KPILevel.SelectedValue
                Case 0
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                    KPIDirektorat.Visible = False
                Case 1
                    KPIFunc.Visible = True
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                    KPIDirektorat.Visible = False
                Case 2
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = True
                    KPIDirektorat.Visible = False
                Case 3
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = False
                    KPIDirektorat.Visible = False
                Case 4
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                    KPIDirektorat.Visible = True
            End Select
            result.InnerHtml = ""
            header1.InnerHtml = ""
            btnSaveNew.Style.Remove("visibility")
            btnSaveNew.Style.Add("visibility", "hidden")
            btnApprove.Style.Remove("visibility")
            btnApprove.Style.Add("visibility", "hidden")
            btnPrint.Style.Remove("visibility")
            btnPrint.Style.Add("visibility", "hidden")
            btnUnApprove.Style.Remove("visibility")
            btnUnApprove.Style.Add("visibility", "hidden")
            btnCalc.Visible = False
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCreate.Click
        Initialize()
    End Sub

    Function GetMonth(ByVal s as string) as string
        Select case s
            case "1"
                Return "jan"
            Case "2"
                return "feb"
            Case "3"
                return "mar"
            Case "4"
                return "apr"
            Case "5"
                return "mei"
            Case "6"
                return "jun"
            Case "7"
                return "jul"
            Case "8"
                return "agu"
            Case "9"
                return "sep"
            Case "10"
                return "okt"
            Case "11"
                return "nov"
            Case "12"
                return "des"
        End Select
    End Function

    Function GetFullMonth(ByVal s As String) As String
        Select Case s
            Case "1"
                Return "JANUARI"
            Case "2"
                Return "februari"
            Case "3"
                Return "maret"
            Case "4"
                Return "april"
            Case "5"
                Return "mei"
            Case "6"
                Return "juni"
            Case "7"
                Return "juli"
            Case "8"
                Return "agustus"
            Case "9"
                Return "september"
            Case "10"
                Return "oktober"
            Case "11"
                Return "november"
            Case "12"
                Return "desember"
        End Select
    End Function

    Function getMapLevel(ByVal level As String) As String
        If level = "3" Then
            Return "0"
        ElseIf level = "4" Then
            Return "4"
        ElseIf level = "1" Then
            Return "1"
        Else
            Return level
        End If
        Return level
    End Function

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        doSearch()
    End Sub

    Function checkreadonly(ByVal approved As String, Optional ByVal SAPFlag As Boolean = False, Optional ByVal formula As String = "0", Optional ByVal pgpapproved As String = "0") As String
        If approved <> 0 Then
            Return "READONLY style='border:0;'"
        ElseIf formula <> "0" Then
            Return "READONLY style='border:0;'"
        ElseIf pgpapproved <> "0" Then
            Return "READONLY style='border:0;' title='Data Parent / Grand Parent Sudah Diapprove, Data Tidak dapat diubah / Diedit'"
        Else
            If KPILevel.SelectedValue = "2" And Session("permission") = "2" And KPISection.SelectedValue = "9" Then 'Kalau Level Section dan merupakan sekpro dan section MSD
                btnSaveNew.Style.Remove("visibility")
                btnApprove.Style.Remove("visibility")
                btnApprove.Style.Add("visibility", "hidden")
                Return ""
            ElseIf KPILevel.SelectedValue = "2" And Session("permission") = "2" Then 'Kalau Level Section dan merupakan sekpro
                btnSaveNew.Style.Remove("visibility")
                btnSaveNew.Style.Add("visibility", "hidden")
                btnApprove.Style.Remove("visibility")
                btnApprove.Style.Add("visibility", "hidden")
                Return "READONLY style='border:0;'"
            ElseIf KPILevel.SelectedValue = "2" And Session("permission") = "8" Then 'Kalau Level Section dan merupakan PIC PDCA
                btnSaveNew.Style.Remove("visibility")
                btnSaveNew.Style.Add("visibility", "hidden")
                btnApprove.Style.Remove("visibility")
                btnApprove.Style.Add("visibility", "hidden")
                Return "READONLY style='border:0;'"
            ElseIf Session("permission") = "3" Or Session("permission") = "4" Or Session("permission") = "5" Or Session("permission") = "6" Then 'Kalau BOD,GM,MGR,PM hanya readonly
                btnSaveNew.Style.Remove("visibility")
                btnSaveNew.Style.Add("visibility", "hidden")
                Return "READONLY style='border:0;'"
            ElseIf Session("permission") = "1" And (KPILevel.SelectedValue = "2" Or KPILevel.SelectedValue = "3") Then
                btnSaveNew.Style.Remove("visibility")
                'btnSaveNew.Style.Add("visibility", "hidden")
                btnApprove.Style.Remove("visibility")
                btnApprove.Style.Add("visibility", "hidden")
                Return "READONLY style='border:0;'"
            ElseIf Session("permission") = "1" And KPILevel.SelectedValue = "1" Then
                If Not KPIFunc.SelectedItem.Text = "Management System Development" Then
                    btnSaveNew.Style.Remove("visibility")
                    'btnSaveNew.Style.Add("visibility", "hidden")
                    Return "READONLY style='border:0;'"
                End If
                btnApprove.Style.Remove("visibility")
                btnApprove.Style.Add("visibility", "hidden")
            ElseIf SAPFlag = True Then
                Return "READONLY style='border:0;'"
            End If
        End If
        Return ""
    End Function

    Function disabledUpload(ByVal approved As String, BYVal StEdit as string) As String
        If StEdit = "1" then
            return "DISABLED"
        End If
        If approved <> 0 Then
            Return "DISABLED"
        Else
            If KPILevel.SelectedValue = "2" And Session("permission") = "2" Then 'Kalau Level Section dan merupakan sekpro
                If KPISection.SelectedValue = 9 Then
                    Return ""
                End If
                Return "DISABLED"
            ElseIf Session("permission") = "3" Or Session("permission") = "4" Or Session("permission") = "5" Or Session("permission") = "6" Then 'Kalau BOD,GM,MGR,PM hanya readonly
                Return "DISABLED"
            End If
        End If
        Return ""
    End Function

    'Sub SetField(ByVal bool As Boolean)
    '    ddlYear.Enabled = bool
    '    ddlMonth.Enabled = bool
    '    ddlLevel.Enabled = bool
    '    ddlsf.Enabled = bool
    'End Sub

    Function getApproval(ByVal approved As Integer) As String
        If approved = 0 Then 'Draft
            Return "<img src='../images/draft.png' align='right'/>"
        Else
            Return "<img src='../images/approved.png'  align='right'/>"
        End If
    End Function

    Function getImage(ByVal value As String, ByVal redCond As String, ByVal redVal As Decimal, ByVal greenCond As String, ByVal greenVal As Decimal) As String
        Try
            If value = "-999999.314" Then
                Return "yellow"
            End If
            Select Case redCond
                Case "0"
                    If value > redVal Then
                        Return "red"
                    End If
                Case "1"
                    If value >= redVal Then
                        Return "red"
                    End If
                Case "2"
                    If value < redVal Then
                        Return "red"
                    End If
                Case "3"
                    If value <= redVal Then
                        Return "red"
                    End If
            End Select
            Select Case greenCond
                Case "0"
                    If value > greenVal Then
                        Return "green"
                    End If
                Case "1"
                    If value >= greenVal Then
                        Return "green"
                    End If
                Case "2"
                    If value < greenVal Then
                        Return "green"
                    End If
                Case "3"
                    If value <= greenVal Then
                        Return "green"
                    End If
            End Select
            Return "yellow"
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Function

    Function replacenull(ByVal o As Object) As String
        If IsDBNull(o) Then
            Return "0"
        Else : Return o.ToString()
        End If
    End Function

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
    '    'SetField(True)
    '    btnCreate.Visible = False
    '    btnCancel.Visible = False
    'End Sub

    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
    '    Dim sqlquery As String = ""
    '    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
    '    Try
    '        For Each x As RepeaterItem In rptTrans.Items
    '            sqlquery += "update tbsc set target=" & CType(x.FindControl("target"), TextBox).Text & ",actual=" & _
    '                        CType(x.FindControl("actual"), TextBox).Text & ",achievement=" & CType(x.FindControl("achievement"), TextBox).Text & " where kpiid='" & CType(x.FindControl("idhidden"), HiddenField).Value & "' " & _
    '                        "and year='" & ddlYear.SelectedValue & _
    '                        "' and month='" & ddlMonth.SelectedValue & "';"
    '            Try
    '                CType(x.FindControl("achievement"), TextBox).Text = CDec(CType(x.FindControl("actual"), TextBox).Text) / IIf(CDec(CType(x.FindControl("target"), TextBox).Text) = 0, 1, CDec(CType(x.FindControl("target"), TextBox).Text)) * 100
    '            Catch ex As Exception
    '                CType(x.FindControl("achievement"), TextBox).Text = 0
    '            End Try
    '        Next
    '        sqlquery = "BEGIN TRANSACTION; BEGIN TRY " & sqlquery
    '        sqlquery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end"
    '        Dim result As String = ""
    '        Dim cmsql As New SqlCommand(sqlquery, conn)
    '        conn.Open()
    '        result = cmsql.ExecuteScalar()
    '        If result = "0" Then
    '            lblMessage.Text = "Simpan Berhasil"
    '            ''-----------------------------------cek trend
    '            'Dim da As SqlDataAdapter
    '            'Dim monthbefore As Integer
    '            'Dim yearbefore As Integer
    '            'If ddlMonth.SelectedValue = 1 Then
    '            '    yearbefore = CInt(ddlYear.SelectedValue) - 1
    '            '    monthbefore = 12
    '            'Else
    '            '    monthbefore = CInt(ddlMonth.SelectedValue) - 1
    '            'End If

    '            'Dim dv As DataView
    '            'Dim dtBefore As New DataTable()

    '            'Select Case ddlLevel.SelectedValue
    '            '    Case "0"
    '            '        sqlquery = "select * from bsctransaction where year='" & yearbefore & _
    '            '                   "' and month='" & monthbefore & "' and level='" & ddlLevel.SelectedValue & "' order by year,month,st"
    '            '    Case "1"
    '            '        sqlquery = "select * from bsctransaction where year='" & yearbefore & _
    '            '                   "' and month='" & monthbefore & "' and level='" & ddlLevel.SelectedValue & "' " & _
    '            '                   " and workunit='" & ddlsf.SelectedValue & "' order by year,month,st"
    '            '    Case "2"
    '            '        sqlquery = "select * from bsctransaction where year='" & yearbefore & _
    '            '               "' and month='" & monthbefore & "' and level='" & ddlLevel.SelectedValue & "' " & _
    '            '               " and site='" & ddlSite.SelectedValue & "' order by year,month,st"
    '            '    Case "3"
    '            '        sqlquery = "select * from bsctransaction where year='" & yearbefore & _
    '            '                   "' and month='" & monthbefore & "' and level='" & ddlLevel.SelectedValue & "' " & _
    '            '                   " and workunit='" & ddlsf.SelectedValue & "'  and site='" & ddlSite.SelectedValue & "' order by year,month,st"
    '            '    Case Else
    '            '        sqlquery = "select * from bsctransaction where year='" & yearbefore & _
    '            '                   "' and month='" & monthbefore & "' and level='" & ddlLevel.SelectedValue & "' order by year,month,st"
    '            'End Select

    '            'da = New SqlDataAdapter(sqlquery, conn)
    '            'da.Fill(dtBefore)

    '            'If dtBefore.Rows.Count = 0 Then
    '            '    For Each x As RepeaterItem In rptTrans.Items
    '            '        Try
    '            '            If CDec(CType(x.FindControl("achievement"), TextBox).Text) > 0 Then
    '            '                CType(x.FindControl("imgtrend"), Image).ImageUrl = "images/up.png"
    '            '            ElseIf CDec(CType(x.FindControl("achievement"), TextBox).Text) = 0 Then
    '            '                CType(x.FindControl("imgtrend"), Image).ImageUrl = "images/equal.png"
    '            '            ElseIf CDec(CType(x.FindControl("achievement"), TextBox).Text) < 0 Then
    '            '                CType(x.FindControl("imgtrend"), Image).ImageUrl = "images/down.png"
    '            '            End If
    '            '        Catch ex As Exception
    '            '            CType(x.FindControl("imgtrend"), Image).ImageUrl = ""
    '            '        End Try
    '            '    Next
    '            'Else
    '            '    dv = New DataView(dtBefore)
    '            '    dv.Sort = "id"
    '            '    For Each x As RepeaterItem In rptTrans.Items
    '            '        Try
    '            '            If CDec(CType(x.FindControl("achievement"), TextBox).Text) > CDec(dv.FindRows(CType(x.FindControl("id"), TextBox).Text)(0)("actual")) / CDec(dv.FindRows(CType(x.FindControl("id"), TextBox).Text)(0)("target")) * 100 Then
    '            '                CType(x.FindControl("imgtrend"), Image).ImageUrl = "images/up.png"
    '            '            ElseIf CDec(CType(x.FindControl("achievement"), TextBox).Text) = CDec(dv.FindRows(CType(x.FindControl("id"), TextBox).Text)(0)("actual")) / CDec(dv.FindRows(CType(x.FindControl("id"), TextBox).Text)(0)("target")) * 100 Then
    '            '                CType(x.FindControl("imgtrend"), Image).ImageUrl = "images/equal.png"
    '            '            ElseIf CDec(CType(x.FindControl("achievement"), TextBox).Text) < CDec(dv.FindRows(CType(x.FindControl("id"), TextBox).Text)(0)("actual")) / CDec(dv.FindRows(CType(x.FindControl("id"), TextBox).Text)(0)("target")) * 100 Then
    '            '                CType(x.FindControl("imgtrend"), Image).ImageUrl = "images/down.png"
    '            '            End If
    '            '        Catch ex As Exception
    '            '            CType(x.FindControl("imgtrend"), Image).ImageUrl = ""
    '            '        End Try
    '            '    Next
    '            'End If
    '            '-------------------------------------------------
    '        Else
    '            lblMessage.Text += "Simpan Gagal"
    '        End If
    '        conn.Close()
    '    Catch ex As Exception
    '        lblMessage.Text = ex.Message
    '    End Try

    'End Sub

    Function RenderGroup(ByVal group As String) As String
        If group = lastgroup Then
            Return ""
        End If
        lastgroup = group
        Return "<div class='groupheader'>" & group & "</div>"
    End Function

    Function checkenabled(ByVal s As String) As Boolean
        If s = 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    'Private Sub rptTrans_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTrans.ItemCommand
    '    'Try
    '    '    If e.CommandName = "pica" Then
    '    '        ' Write code here to set up the controls in the panel. You can use e.CommandArgument to get hold of the ID and do whatever you need as normal here.
    '    '        ' Using the code-behind, make the modal popup appear
    '    '        Response.Write("<script type='text/javascript'>window.open('pica.aspx?y=" & ddlYear.SelectedValue & "&m=" & ddlMonth.SelectedValue & "&id=" & CType(e.Item.FindControl("id"), TextBox).Text & "','win');</script>")
    '    '    ElseIf e.CommandName = "viewdetail" Then
    '    '        If ddlLevel.SelectedValue = 1 Then
    '    '            Response.Write("<script type='text/javascript'>window.open('report.aspx?jns=sectionsummary&id=" & CType(e.Item.FindControl("id"), TextBox).Text & "','win');</script>")
    '    '        End If
    '    '    End If
    '    'Catch ex As Exception
    '    '    lblMessage.Text = ex.Message
    '    'End Try
    'End Sub

    'Private Sub rptTrans_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTrans.ItemDataBound
    '    Try
    '        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
    '            Dim txt1 As TextBox = e.Item.FindControl("target")
    '            Dim txt2 As TextBox = e.Item.FindControl("actual")
    '            Dim txt3 As TextBox = e.Item.FindControl("achievement")
    '            Dim txt4 As HiddenField = e.Item.FindControl("oldach")
    '            Dim img As SYstem.Web.UI.WebControls.Image = e.Item.FindControl("imgtrend")
    '            txt1.Attributes("onchange") = "ontextboxchange('" & txt2.ClientID & "','" & txt1.ClientID & "','" & txt3.ClientID & "','" & txt4.ClientID & "','" & img.ClientID & "')"
    '            txt2.Attributes("onchange") = "ontextboxchange('" & txt2.ClientID & "','" & txt1.ClientID & "','" & txt3.ClientID & "','" & txt4.ClientID & "','" & img.ClientID & "')"

    '        End If
    '    Catch ex As Exception
    '        lblMessage.Text = ex.Message
    '    End Try
    'End Sub

    Private Sub ddlMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMonth.SelectedIndexChanged
        result.InnerHtml = ""
        header1.InnerHtml = ""
        btnSaveNew.Style.Remove("visibility")
        btnSaveNew.Style.Add("visibility", "hidden")
        btnApprove.Style.Remove("visibility")
        btnApprove.Style.Add("visibility", "hidden")
        btnPrint.Style.Remove("visibility")
        btnPrint.Style.Add("visibility", "hidden")
        btnUnApprove.Style.Remove("visibility")
        btnUnApprove.Style.Add("visibility", "hidden")
        btnCalc.Visible = False
    End Sub

    Private Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        result.InnerHtml = ""
        header1.InnerHtml = ""
        btnSaveNew.Style.Remove("visibility")
        btnSaveNew.Style.Add("visibility", "hidden")
        btnApprove.Style.Remove("visibility")
        btnApprove.Style.Add("visibility", "hidden")
        btnPrint.Style.Remove("visibility")
        btnPrint.Style.Add("visibility", "hidden")
        btnUnApprove.Style.Remove("visibility")
        btnUnApprove.Style.Add("visibility", "hidden")
        btnCalc.Visible = False
    End Sub

    Sub doSearch()
        Try
            btnUnApprove.Style.Remove("visibility")
            btnUnApprove.Style.Add("visibility", "hidden")
            Select Case KPIViewType.SelectedValue
                Case "0" 'View Monthly
                    Dim sqlQuery As String = ""
                    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
                    Dim beforeyear As String
                    Dim beforemonth As String
                    Dim cmd As SqlCommand
                    Dim approved As Integer = 0
                    If ddlMonth.SelectedValue = 1 Then
                        beforeyear = ddlYear.SelectedValue - 1
                        beforemonth = 12
                    Else
                        beforeyear = ddlYear.SelectedValue
                        beforemonth = ddlMonth.SelectedValue - 1
                    End If

                    Select Case KPILevel.SelectedValue
                        Case "0"
                            sqlQuery = "Select count(*) from TBSCAPPR where level='0' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "'"
                        Case "1"
                            sqlQuery = "Select count(*) from TBSCAPPR where level='1' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "' and func='" & KPIFunc.SelectedValue & "'"
                        Case "2"
                            sqlQuery = "Select count(*) from TBSCAPPR where level='2' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "' and jobsite='" & KPIJobsite.SelectedValue & "' and section='" & KPISection.SelectedValue & "'"
                        Case "3"
                            sqlQuery = "Select count(*) from TBSCAPPR where level='3' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "' and jobsite='" & KPIJobsite.SelectedValue & "'"
                        Case "4"
                            sqlQuery = "Select count(*) from TBSCAPPR where level='4' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "' and dir='" & KPIDirektorat.SelectedValue & "'"
                    End Select
                    cmd = New SqlCommand(sqlQuery, conn)
                    conn.Open()
                    approved = cmd.ExecuteScalar
                    conn.Close()

                    'sqlQuery = "select a.year,a.month,a.kpiid as id,b.Level,'' as workunit,d.STName as st,c.SO,isnull(e.jabatan,'-') as soleader," & _
                    '            "b.Name as kpi,b.Owner as kpiowner,b.UoM,b.Formula,b.RedParam,b.GreenParam," & IIf(approved = 0, "isnull(dd." & GetMonth(ddlMonth.SelectedValue) & ",'-')", "isnull(cast(a.target as varchar(60)),'-')") & " as target,isnull(a.actual,0) as actual,isnull(a.achievement,0) as achievement, " & _
                    '            "ff.kpiid,isnull(ff.achievement,-9999) as oldach,p.perspective,isnull(ff.actual,-9999) as oldact " & _
                    '            ",isnull(b.RedCond,'0') as RedCond,isnull(b.GreenCond,'0') as GreenCond,b.calctype,b.basedon, isnull(tp.value,0) as bobot, (select count(Nomor) from dbo.TBSCLAMPIRAN l where l.kpiid=a.kpiid) as jmlhattachment, " & _
                    '            "isnull(dd." & GetMonth(ddlMonth.SelectedValue) & ",'-') as ttarget, isnull(cast(a.target as varchar(60)),'-') as atarget " & _
                    '            "from tbsc a inner join MKPI b on a.kpiid = b.id " & _
                    '            "inner join MSO c on b.SOID =c.ID  " & _
                    '            "inner join MST d on c.STID = d.STID  " & _
                    '            "left join MJABATAN e on e.id = c.BUMALeader  " & _
                    '            "left join " & _
                    '            "(select kpiid,achievement,actual from TBSC t where t.year='" & beforeyear & "' and t.month='" & beforemonth & "') ff on ff.kpiid =a.kpiid  left join mperspective p on d.PRID=p.id " & _
                    '            "left join " & _
                    '            "(select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year) b on a.versi=b.versi and a.kpiid = b.kpiid where a.year='" & ddlYear.SelectedValue & "')dd on a.kpiid=dd.kpiid  " & _
                    '            "left join tperspective tp on p.id=tp.pid and tp.year=a.year and tp.level=" & getMapLevel(KPILevel.SelectedValue) & IIf(KPILevel.SelectedValue = 0 Or KPILevel.SelectedValue = 3, "", IIf(KPILevel.SelectedValue = "1", " and tp.funcid=b.func ", " and tp.funcid= (select funcid from msection where id=b.section) ")) & _
                    '            "where a.year='" & ddlYear.SelectedValue & "' and a.month='" & ddlMonth.SelectedValue & "' and b.Level='" & KPILevel.SelectedValue & "' "

                    sqlQuery = "select isnull(b.bobot,0) as bobotkpi,a.year,a.month,a.kpiid as id,b.Level,'' as workunit,d.STName as st,c.SO,isnull(e.jabatan,'-') as soleader," & _
                                "b.Name as kpi,b.Owner as kpiowner,b.UoM,b.Formula,b.RedParam,b.GreenParam," & IIf(approved = 0, "isnull(dd." & GetMonth(ddlMonth.SelectedValue) & ",'-')", "isnull(cast(a.target as varchar(60)),'')") & " as target,a.actual as actual,isnull(a.achievement,0) as achievement, " & _
                                "ff.kpiid,isnull(ff.achievement,-9999) as oldach,p.perspective,isnull(ff.actual,-9999) as oldact " & _
                                ",isnull(b.RedCond,'0') as RedCond,isnull(b.GreenCond,'0') as GreenCond,b.calctype,b.basedon, isnull(tp.value,0) as bobot, (select count(Nomor) from dbo.TBSCLAMPIRAN l where l.kpiid=a.kpiid and l.year='" & ddlYear.SelectedValue & "' and l.month='" & ddlMonth.SelectedValue & "') as jmlhattachment, " & _
                                "isnull(dd." & GetMonth(ddlMonth.SelectedValue) & ",'-') as ttarget, isnull(cast(a.target as varchar(60)),'-') as atarget, cc.*, a.SAPFlag, b.StEdit, dbo.getPGPApproval('" & ddlYear.SelectedValue & "','" & ddlMonth.SelectedValue & "',a.kpiid) as approved " & _
                                "from tbsc a inner join MKPI b on a.kpiid = b.id " & _
                                "inner join MSO c on b.SOID =c.ID  " & _
                                "inner join MST d on c.STID = d.STID  " & _
                                "left join MJABATAN e on e.id = c.BUMALeader  " & _
                                "left join CALCCONTENT cc on b.id=cc.kpiid and cc.calctype=b.calctype " & _
                                "left join " & _
                                "(select kpiid,achievement,actual from TBSC t where t.year='" & beforeyear & "' and t.month='" & beforemonth & "') ff on ff.kpiid =a.kpiid  left join mperspective p on d.PRID=p.id " & _
                                "left join " & _
                                "(select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year) b on a.versi=b.versi and a.kpiid = b.kpiid where a.year='" & ddlYear.SelectedValue & "')dd on a.kpiid=dd.kpiid  " & _
                                "left join tperspective tp on p.id=tp.pid and tp.year=a.year and tp.level=" & getMapLevel(KPILevel.SelectedValue) & IIf(KPILevel.SelectedValue = 0 Or KPILevel.SelectedValue = 3, "", IIf(KPILevel.SelectedValue = "4", " and tp.dirid=b.dir ", "  and tp.dirid=b.func  ")) & _
                                "where a.year='" & ddlYear.SelectedValue & "' and a.month='" & ddlMonth.SelectedValue & "' and b.Level='" & KPILevel.SelectedValue & "' and b.stedit='0' "

                    Select Case KPILevel.SelectedValue
                        Case "0"
                            sqlQuery += " order by p.id,d.stid,c.ID,b.name"
                        Case "1"
                            sqlQuery += " and b.func='" & KPIFunc.SelectedValue & "' order by p.id,d.stid,c.ID,b.name"
                        Case "2"
                            sqlQuery += " and b.jobsite='" & KPIJobsite.SelectedValue & "' and b.section='" & KPISection.SelectedValue & "' order by p.id,d.stid,c.ID,b.name"
                        Case "3"
                            sqlQuery += " and b.jobsite='" & KPIJobsite.SelectedValue & "' order by p.id,d.stid,c.ID,b.name"
                        Case "4"
                            sqlQuery += " and b.dir='" & KPIDirektorat.SelectedValue & "' order by p.id,d.stid,c.ID,b.name"
                    End Select

                    Dim da As New SqlDataAdapter(sqlQuery, conn)
                    Dim dt As New DataTable()
                    da.Fill(dt)

                    '-----------------------------------
                    Dim mytableresult As String = ""
                    Dim perspective As String = ""
                    Dim so As String = ""
                    Dim socount As Integer = 1
                    Dim row As Integer = 1
                    Dim ach As Decimal = 0
                    If dt.Rows.Count = 0 Then
                        lblMessage.Text = "No Record Found, Press Refresh to Refresh transaction record"
                        btnCreate.Visible = True
                        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "tes", "<script type='text/javascript'>changeVisibility(false)</script>")
                        btnPrint.Style.Add("visibility", "hidden")
                        btnSaveNew.Style.Add("visibility", "hidden")
                        btnApprove.Style.Add("visibility", "hidden")
                        header1.InnerHtml = "BSC TRANSACTION"
                    Else
                        param.Value = KPIViewType.SelectedValue + ";" + ddlYear.SelectedValue + ";" + ddlMonth.SelectedValue + ";" + KPILevel.SelectedValue
                        Select Case KPILevel.SelectedValue
                            Case "0"
                                param.Value += ""
                            Case "1"
                                param.Value += ";" + KPIFunc.SelectedValue
                            Case "2"
                                param.Value += ";" + KPIJobsite.SelectedValue + ";" + KPISection.SelectedValue
                            Case "3"
                                param.Value += ";" + KPIJobsite.SelectedValue
                        End Select

                        lblMessage.Text = ""
                        btnCreate.Visible = True
                        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "tes", "<script type='text/javascript'>changeVisibility(true)</script>")
                        'btnSave.Visible = True 
                        'Check If Already Approve

                        If ("3;6").IndexOf(Session("permission")) <> -1 Then 'BOD,PM
                            If approved = 0 Then
                                If Session("permission") = "3" Then
                                    If KPILevel.SelectedValue = 0 Then
                                        btnApprove.Style.Remove("visibility")
                                    End If
                                Else
                                    If KPILevel.SelectedValue = 3 Or KPILevel.SelectedValue = 2 Then
                                        btnApprove.Style.Remove("visibility")
                                    End If
                                End If
                            Else
                                btnApprove.Style.Remove("visibility")
                                btnApprove.Style.Add("visibility", "hidden")
                                If Session("permission") = "3" Then
                                    If KPILevel.SelectedValue = 0 Then
                                        btnUnApprove.Style.Remove("visibility")
                                    End If
                                Else
                                    If KPILevel.SelectedValue = 3 Or KPILevel.SelectedValue = 2 Then
                                        btnUnApprove.Style.Remove("visibility")
                                    End If
                                End If
                            End If
                        ElseIf ("7").IndexOf(Session("permission")) <> -1 Then
                            If approved = 0 Then
                                btnSaveNew.Style.Remove("visibility")
                                'btnApprove.Style.Remove("visibility")
                            Else
                                btnSaveNew.Style.Remove("visibility")
                                btnSaveNew.Style.Add("visibility", "hidden")
                                btnApprove.Style.Remove("visibility")
                                btnApprove.Style.Add("visibility", "hidden")
                                btnUnApprove.Style.Remove("visibility")
                            End If
                        ElseIf ("4").IndexOf(Session("permission")) <> -1 Then 'GM
                            btnSaveNew.Style.Remove("visibility")
                            btnSaveNew.Style.Add("visibility", "hidden")
                            If KPILevel.SelectedValue = "1" Then
                                If approved = 0 Then
                                    btnApprove.Style.Remove("visibility")
                                Else
                                    btnApprove.Style.Remove("visibility")
                                    btnApprove.Style.Add("visibility", "hidden")
                                    btnUnApprove.Style.Remove("visibility")
                                End If
                            Else
                                btnApprove.Style.Remove("visibility")
                                btnApprove.Style.Add("visibility", "hidden")
                            End If
                        ElseIf ("B").IndexOf(Session("permission")) <> -1 Then
                            If approved = 0 Then
                                btnSaveNew.Style.Remove("visibility")
                                btnApprove.Style.Remove("visibility")
                            Else
                                btnApprove.Style.Remove("visibility")
                                btnApprove.Style.Add("visibility", "hidden")
                                btnSaveNew.Style.Remove("visibility")
                                btnSaveNew.Style.Add("visibility", "hidden")
                                btnUnApprove.Style.Remove("visibility")
                            End If
                        Else
                            If approved = 0 Then
                                btnSaveNew.Style.Remove("visibility")
                                btnApprove.Style.Remove("visibility")
                                btnApprove.Style.Add("visibility", "hidden")
                            Else
                                btnSaveNew.Style.Remove("visibility")
                                btnSaveNew.Style.Add("visibility", "hidden")
                                btnApprove.Style.Remove("visibility")
                                btnApprove.Style.Add("visibility", "hidden")
                            End If
                        End If
                        btnPrint.Style.Remove("visibility")
                        'ADD ON BTN RECALC--------------------
                        If approved = 1 Then
                            btnCalc.Visible = False
                        Else
                            If Session("permission") = "1" Then
                                If KPILevel.SelectedValue = "0" Or KPILevel.SelectedValue = "1" Or KPILevel.SelectedValue = "3" Then
                                    btnCalc.Visible = True
                                Else
                                    btnCalc.Visible = False
                                End If
                            Else
                                btnCalc.Visible = False
                            End If
                        End If
                        '-------------
                        Select Case KPILevel.SelectedValue
                            Case "0"
                                header1.InnerHtml = "<table style='width:100%'><tr><td style='width:90%'>" & "JRN PERFORMANCE MONTHLY " & GetFullMonth(ddlMonth.SelectedValue).ToUpper() & " " & ddlYear.SelectedValue & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                            Case "1"
                                header1.InnerHtml = "<table style='width:100%'><tr><td style='width:90%'>" & KPIFunc.SelectedItem.Text & " PERFORMANCE MONTHLY " & GetFullMonth(ddlMonth.SelectedValue).ToUpper() & " " & ddlYear.SelectedValue & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                            Case "2"
                                header1.InnerHtml = "<table style='width:100%'><tr><td style='width:90%'>" & KPISection.SelectedItem.Text & " " & KPIJobsite.SelectedItem.Text & " PERFORMANCE MONTHLY " & GetFullMonth(ddlMonth.SelectedValue).ToUpper() & " " & ddlYear.SelectedValue & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                            Case "3"
                                header1.InnerHtml = "<table style='width:100%'><tr><td style='width:90%'>" & KPIJobsite.SelectedItem.Text & " PERFORMANCE MONTHLY " & GetFullMonth(ddlMonth.SelectedValue).ToUpper() & " " & ddlYear.SelectedValue & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                        End Select
                    End If
                    'style='align:center;font-size:12px;background-color:#449A50;color:white;font-weight:bold;'
                    mytableresult += "<div id='totalach' style='font-size:24px;font-weight:bold;'></div>"
                    mytableresult += "<table border='1' id='BSCTABLE' class='bsc' style='width:100%;font-size:12px'>"
                    mytableresult += "<thead class='headbsctable'><td style='width:18%' align='center'>Strategic Objective</td><td style='width:30%' align='center'>KPI</td><td align='center' style='width:7%'>KPI Owner</td><td align='center'>UoM</td>" & _
                                     "<td align='center'>Target</td><td  align='center'>Actual</td><td  align='center'>Achievement <br> (Act Vs Target)</td><td align='center'>Trend</td><td>Attach</td><td align='center'>Details</td></thead>"
                    For Each dr As DataRow In dt.Rows
                        If perspective <> dr("perspective") Then
                            perspective = dr("perspective") '<div id='p" & row & "'>VAL</div>
                            mytableresult += "<tr><td colspan='12' align='center' style='background-color:#EACB92;'>" & perspective & "<br><input type='text' id='p" & row & "' style='width:3%;border-style:none;background:transparent;' READONLY size='5'/>% X <input type='text' style='width:4%;border-style:none;background:transparent;' READONLY size='5' id='tp" & row & "' value='" & System.Math.Round(dr("bobot"), 2) & "'/>% &nbsp; = <input type='text' style='font-size:16px;font-weight:bold;width:5%;border-style:none;background:transparent;' READONLY size='15' id='hp" & row & "'/><span style='font-size:22px'>%</span></td></tr>"
                        End If
                        If dr("StEdit") = "0" Then
                            mytableresult += "<tr>"
                        Else
                            mytableresult += "<tr style='color:gray'>"
                        End If


                        If so <> dr("SO") Then
                            mytableresult = mytableresult.Replace("x&x", socount)
                            so = dr("SO")
                            socount = 1
                            'mytableresult += "<td rowspan='x&x'>" & dr("SO") & "</td><td rowspan='x&x' align='center'><div id='so" & row & "'>VAL</div></td>"
                            mytableresult += "<td rowspan='x&x'>" & Replace(dr("SO"), "'", "`") & "<br/><div style='font-size:16px;text-align:center;padding-top:5px' id='so" & row & "'></div></td>"
                        Else
                            'mytableresult += "<td>" & dr("SO") & "</td>"
                            socount += 1
                        End If
                        idtorecalc.Value += Replace(dr("kpi"), """", "`") + ";"
                        mytableresult += "<td align='left'>" & Replace(dr("kpi"), """", "`") & " [" & dr("bobotkpi") & " %]"
                        mytableresult += "<input type='hidden' id='bobot" & row & "' value='" & dr("bobotkpi") & "'/>"
                        mytableresult += "<input type='hidden' id='kpiid" & row & "' value='" & dr("id") & "'/>"
                        mytableresult += "<input type='hidden' id='year" & row & "' value='" & dr("year") & "'/>"
                        mytableresult += "<input type='hidden' id='month" & row & "' value='" & dr("month") & "'/>"
                        mytableresult += "<input type='hidden' id='calctype" & row & "' value='" & dr("calctype") & "'/>"

                        If dr("calctype") = "0" Or dr("calctype") = "1" Then
                        ElseIf dr("calctype") = "2" Or dr("calctype") = "4" Then
                            mytableresult += "<input type='hidden' id='e1x" & row & "' value='" & dr("e1") & "'/>"
                            mytableresult += "<input type='hidden' id='e2x" & row & "' value='" & dr("e2") & "'/>"
                            mytableresult += "<input type='hidden' id='eax" & row & "' value='" & dr("ea") & "'/>"
                            mytableresult += "<input type='hidden' id='mr1x" & row & "' value='" & dr("mr1") & "'/>"
                            mytableresult += "<input type='hidden' id='mr2x" & row & "' value='" & dr("mr2") & "'/>"
                            mytableresult += "<input type='hidden' id='mrax" & row & "' value='" & dr("mra") & "'/>"
                            mytableresult += "<input type='hidden' id='bl1x" & row & "' value='" & dr("bl1") & "'/>"
                            mytableresult += "<input type='hidden' id='bl2x" & row & "' value='" & dr("bl2") & "'/>"
                            mytableresult += "<input type='hidden' id='blax" & row & "' value='" & dr("bla") & "'/>"
                            mytableresult += "<input type='hidden' id='p2x" & row & "' value='" & dr("p2") & "'/>"
                            mytableresult += "<input type='hidden' id='pax" & row & "' value='" & dr("pa") & "'/>"
                        Else
                            mytableresult += "<input type='hidden' id='e1x" & row & "' value='" & dr("e1") & "'/>"
                            mytableresult += "<input type='hidden' id='e2x" & row & "' value='" & dr("e2") & "'/>"
                            mytableresult += "<input type='hidden' id='eax" & row & "' value='" & dr("ea") & "'/>"
                            mytableresult += "<input type='hidden' id='mr1x" & row & "' value='" & dr("mr1") & "'/>"
                            mytableresult += "<input type='hidden' id='mr2x" & row & "' value='" & dr("mr2") & "'/>"
                            mytableresult += "<input type='hidden' id='mr3x" & row & "' value='" & dr("mr3") & "'/>"
                            mytableresult += "<input type='hidden' id='mr4x" & row & "' value='" & dr("mr4") & "'/>"
                            mytableresult += "<input type='hidden' id='mrax" & row & "' value='" & dr("mra") & "'/>"
                            mytableresult += "<input type='hidden' id='bl1x" & row & "' value='" & dr("bl1") & "'/>"
                            mytableresult += "<input type='hidden' id='bl2x" & row & "' value='" & dr("bl2") & "'/>"
                            mytableresult += "<input type='hidden' id='bl3x" & row & "' value='" & dr("bl3") & "'/>"
                            mytableresult += "<input type='hidden' id='bl4x" & row & "' value='" & dr("bl4") & "'/>"
                            mytableresult += "<input type='hidden' id='blax" & row & "' value='" & dr("bla") & "'/>"
                            mytableresult += "<input type='hidden' id='p1x" & row & "' value='" & dr("p1") & "'/>"
                            mytableresult += "<input type='hidden' id='p2x" & row & "' value='" & dr("p2") & "'/>"
                            mytableresult += "<input type='hidden' id='pax" & row & "' value='" & dr("pa") & "'/>"
                        End If
                        mytableresult += "</td><td>" & dr("kpiowner") & "</td><td align='center'>" & dr("UoM") & "</td>"


                        If dr("StEdit") = "1" Then
                            mytableresult += "<td><input type='text' style='border-style:none;width:2px' onChange='calc();' id='kpit" & row & "' value='' DISABLED/></td>"
                            mytableresult += "<td align='right'><img src='../images/off.png' alt='OFF'/><input type='text' class='printsap' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' DISABLED style='border:0;'" & "/></td><td style='background:#77FFB6'><input type='hidden' id='hkpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "'/><input type='text' size=3  onChange='calc();' style='width:0px;border-style:none;text-align:right;background:transparent;color:black' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' DISABLED />" & IIf(dr("target") = "", "", "<center>-</center>") & "</td>"
                        Else
                            If dr("target") = "-" Or dr("target") = "" Then
                                mytableresult += "<td><input type='text' style='border-style:none;width:2px' onChange='calc();' id='kpit" & row & "' value='" & dr("target") & "' DISABLED/>" & IIf(dr("target") = "-", "<span style='font-size:10px;color:gray;font-style:italic;margin:0;padding:0'>please set target</span>", "") & "</td>"
                            Else
                                mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='" & System.Math.Round(CDec(dr("target")), 2).ToString("#,##0.00") & "' DISABLED/></td>"
                            End If

                            If dr("target") = "-" Or dr("target") = "" Then
                                mytableresult += "<td><input type='text' title='Isi target dahulu untuk mengaktifkan field actual' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' DISABLED style='border:0;'" & "/></td><td style='background:#77FFB6'><input type='hidden' id='hkpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "'/><input type='text' size=3  onChange='calc();' style='width:0px;border-style:none;text-align:right;background:transparent;color:black' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' DISABLED />" & IIf(dr("target") = "", "", "<center>-</center>") & "</td>"
                            Else
                                If IsDBNull(dr("actual")) Then
                                    mytableresult += "<td>" & IIf(dr("Formula") <> "0", "<img src='../images/auto.png' alt='SAP'/>", "") & "<input type='text' class='" & IIf(dr("calctype") = "0" Or dr("calctype") = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='calc();' id='kpia" & row & "' value='' " & checkreadonly(approved, dr("SAPFlag"), dr("formula"), dr("approved")) & "/></td><td style='background:#77FFB6'><input type='hidden' id='hkpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "'/><input type='text' size=3  class='ach' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' READONLY/> %</td>"
                                    'mytableresult += "<td align='right'>" & IIf(dr("SAPFlag") = True, "<img src='../images/sap.png' alt='SAP'/><input type='text' class='printsap' ", IIf(dr("calctype") = "0" Or dr("calctype") = "1", "<input type='text' class='printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "<input type='text' class='printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}")) & ";' onChange='calc();' id='kpia" & row & "' value='" & Format(System.Math.Round(dr("actual"), 2), "#,##0.00") & "' " & checkreadonly(approved, dr("SAPFlag")) & "/></td><td style='background:#77FFB6'><input type='hidden' id='hkpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "'/><input type='text' size=3  class='ach'   onChange='calc();' style='border-style:none;text-align:right;color:black' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' READONLY/> %</td>"
                                Else
                                    'mytableresult += "<td align='right'>" & IIf(dr("SAPFlag") = True, "<img src='../images/sap.png' alt='SAP'/><input type='text' class='printsap' ", "<input type='text' class='printe' ") & " onkeypress='" & IIf(dr("calctype") = "0" Or dr("calctype") = "1", "fncInputNumericValuesOnlyNoMinus()", "fncInputNumericValuesOnly()") & ";' onChange='calc();' id='kpia" & row & "' value='" & Format(System.Math.Round(dr("actual"), 2), "#,##0.00") & "' " & checkreadonly(approved, dr("SAPFlag")) & "/></td><td style='background:#77FFB6'><input type='hidden' id='hkpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "'/><input type='text' size=3  class='ach'   onChange='calc();' style='border-style:none;text-align:right;color:black' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' READONLY/> %</td>"
                                    mytableresult += "<td align='right'>" & IIf(dr("SAPFlag") = True, "<img src='../images/sap.png' alt='SAP'/><input type='text' class='printsap' ", IIf(dr("Formula") <> "0", "<img src='../images/auto.png' alt='SAP'/><input type='text' class='printsap' ", IIf(dr("approved") <> "0", "<input type='text' title='Data Parent / Grand Parent Sudah Diapprove, Data Tidak dapat diubah / Diedit' class='printsap' ", IIf(dr("calctype") = "0" Or dr("calctype") = "1", "<input type='text' class='printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "<input type='text' class='printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}")))) & ";' onChange='calc();' id='kpia" & row & "' value='" & Format(System.Math.Round(dr("actual"), 2), "#,##0.00") & "' " & checkreadonly(approved, dr("SAPFlag"), dr("formula"), dr("approved")) & "/></td><td style='background:#77FFB6'><input type='hidden' id='hkpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "'/><input type='text' size=3  class='ach'   onChange='calc();' style='border-style:none;text-align:right;color:black' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' READONLY/> %</td>"
                                End If
                            End If
                        End If

                        If IsDBNull(dr("actual")) Then
                            ach = 0
                        Else
                            If dr("calctype") = "0" Then 'MIN
                                If dr("target") = "-" Or dr("target") = "" Then
                                    ach = -999999.314
                                ElseIf dr("actual") = 0 Then
                                    If dr("target") = 0 Then
                                        ach = 100
                                    Else
                                        ach = 105
                                    End If
                                Else
                                    ach = dr("target") / dr("actual") * 100
                                End If
                            ElseIf dr("calctype") = "1" Then 'MAX
                                If dr("target") = "-" Or dr("target") = "" Then
                                    ach = -999999.314
                                ElseIf dr("target") = 0 Then
                                    If dr("actual") = 0 Then
                                        ach = 100
                                    Else
                                        ach = 105
                                    End If
                                Else
                                    ach = dr("actual") / dr("target") * 100
                                End If
                            ElseIf dr("calctype") = "2" Or dr("calctype") = "4" Then
                                Select Case dr("calctype")
                                    Case 4 'Max
                                        If (dr("actual") <= dr("e2") And dr("actual") >= dr("e1")) Then
                                            ach = dr("ea")
                                        ElseIf (dr("actual") <= dr("mr2") And dr("actual") >= dr("mr1")) Then
                                            ach = dr("mra")
                                        ElseIf (dr("actual") <= dr("bl2") And dr("actual") >= dr("bl1")) Then
                                            ach = dr("bla")
                                        ElseIf (dr("actual") <= dr("p2")) Then
                                            ach = dr("pa")
                                        Else
                                            ach = 100
                                        End If
                                    Case 2 'Min
                                        If (dr("actual") <= dr("e2") And dr("actual") >= dr("e1")) Then
                                            ach = dr("ea")
                                        ElseIf (dr("actual") <= dr("mr2") And dr("actual") >= dr("mr1")) Then
                                            ach = dr("mra")
                                        ElseIf (dr("actual") <= dr("bl2") And dr("actual") >= dr("bl1")) Then
                                            ach = dr("bla")
                                        ElseIf (dr("actual") > dr("p2")) Then
                                            ach = dr("pa")
                                        Else
                                            ach = 100
                                        End If
                                End Select
                            ElseIf dr("calctype") = "3" Then
                                If (dr("actual") <= dr("e2") And dr("actual") >= dr("e1")) Then
                                    ach = dr("ea")
                                ElseIf (dr("actual") <= dr("mr2") And dr("actual") >= dr("mr1")) Or (dr("actual") <= dr("mr4") And dr("actual") >= dr("mr3")) Then
                                    ach = dr("mra")
                                ElseIf (dr("actual") <= dr("bl2") And dr("actual") >= dr("bl1")) Or (dr("actual") <= dr("bl4") And dr("actual") >= dr("bl3")) Then
                                    ach = dr("bla")
                                ElseIf (dr("actual") > dr("p2") Or dr("actual") < dr("p1")) Then
                                    ach = dr("pa")
                                Else
                                    ach = 100
                                End If
                            End If
                        End If

                        Dim myactual As Decimal = 0
                        If IsDBNull(dr("actual")) Then
                            myactual = 0
                        Else
                            myactual = dr("actual")
                        End If

                        If dr("target") = "-" Or dr("target") = "" Or dr("StEdit") = "1" Then
                            mytableresult += "<td align='center'></td>"
                        ElseIf dr("oldact") = -9999 Or IsDBNull(dr("actual")) Then
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "equal.png' style='width:100%'/></td>"
                        ElseIf dr("oldact") > myactual Then
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "down.png' style='width:100%'/></td>"
                        ElseIf dr("oldact") < myactual Then
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "up.png' style='width:100%'/></td>"
                        Else
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "equal.png' style='width:100%'/></td>"
                        End If
                        mytableresult += "<td align='center'>" & IIf(dr("jmlhattachment") > 0, "<img src='../images/book.jpg' alt='attachment' height='30px' width='30px'/>", "") & "<input type='button' id='btnUpload' " & disabledUpload(approved, dr("StEdit")) & " Style='width:10px;height:25px;background-image: url(../images/attachment_icon.gif); background-repeat: no-repeat;cursor:hand;background-color:transparent;border:0' onclick='javascript:openupload(""" & dr("id") & """,""" & dr("level") & """,""" & dr("year") & """,""" & dr("month") & """,""" & Replace(dr("kpi"), """", "`") & """)'/></td>"
                        mytableresult += "<td align='center'><input type='button' id='btnAdd' value='VIEW' class='btn' onclick='javascript:openmodal(""" & dr("id") & """,""" & dr("level") & """,""" & dr("year") & """,""" & dr("month") & """,""" & Replace(dr("kpi"), """", "`") & """)' " & IIf(dr("StEdit") = 0, "", "DISABLED") & "/></td></tr>"
                        row += 1
                    Next
                    mytableresult = mytableresult.Replace("x&x", socount)
                    result.InnerHtml = mytableresult + "</table>"
                    '-----------------------------------

                    'If dt.Rows.Count = 0 Then
                    '    lblMessage.Text = "No Record Found, Press Create to create transaction record"
                    '    btnCreate.Visible = True
                    '    btnSave.Visible = False
                    '    rptTrans.DataSource = Nothing
                    '    rptTrans.DataBind()
                    'Else
                    '    btnCreate.Visible = False
                    '    btnSave.Visible = True
                    '    rptTrans.DataSource = dt
                    '    rptTrans.DataBind()
                    'End If

                    'For Each x As RepeaterItem In rptTrans.Items
                    '    Try
                    '        If CDec(CType(x.FindControl("achievement"), TextBox).Text) > CDec(CType(x.FindControl("oldach"), HiddenField).Value) Then
                    '            CType(x.FindControl("imgtrend"), SYstem.Web.UI.WebControls.Image).ImageUrl = "images/up.png"
                    '        ElseIf CDec(CType(x.FindControl("achievement"), TextBox).Text) = CDec(CType(x.FindControl("oldach"), HiddenField).Value) Then
                    '            CType(x.FindControl("imgtrend"), SYstem.Web.UI.WebControls.Image).ImageUrl = "images/equal.png"
                    '        ElseIf CDec(CType(x.FindControl("achievement"), TextBox).Text) < CDec(CType(x.FindControl("oldach"), HiddenField).Value) Then
                    '            CType(x.FindControl("imgtrend"), SYstem.Web.UI.WebControls.Image).ImageUrl = "images/down.png"
                    '        End If
                    '    Catch ex As Exception
                    '        CType(x.FindControl("imgtrend"), SYstem.Web.UI.WebControls.Image).ImageUrl = ""
                    '    End Try
                    'Next
                Case "1" 'View Yearly
                    Dim sqlQuery As String = ""
                    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
                    Dim beforeyear As String
                    Dim beforemonth As String
                    Dim cmd As SqlCommand
                    Dim approved As Integer = 0
                    If ddlMonth.SelectedValue = 1 Then
                        beforeyear = ddlYear.SelectedValue - 1
                        beforemonth = 12
                    Else
                        beforeyear = ddlYear.SelectedValue
                        beforemonth = ddlMonth.SelectedValue - 1
                    End If

                    Select Case KPILevel.SelectedValue
                        Case "0"
                            sqlQuery = "Select count(*) from TBSCAPPR where level='0' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "'"
                        Case "1"
                            sqlQuery = "Select count(*) from TBSCAPPR where level='1' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "' and func='" & KPIFunc.SelectedValue & "'"
                        Case "2"
                            sqlQuery = "Select count(*) from TBSCAPPR where level='2' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "' and jobsite='" & KPIJobsite.SelectedValue & "' and section='" & KPISection.SelectedValue & "'"
                        Case "3"
                            sqlQuery = "Select count(*) from TBSCAPPR where level='3' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "' and jobsite='" & KPIJobsite.SelectedValue & "'"
                    End Select
                    cmd = New SqlCommand(sqlQuery, conn)
                    conn.Open()
                    approved = cmd.ExecuteScalar
                    conn.Close()

                    'sqlQuery = "select a.year,a.month,a.kpiid as id,b.Level,'' as workunit,d.STName as st,c.SO,isnull(e.jabatan,'-') as soleader," & _
                    '            "b.Name as kpi,b.Owner as kpiowner,b.UoM,b.Formula,b.RedParam,b.GreenParam,isnull(cast(a.target as varchar(50)),'-') as target,isnull(cast(a.actual  as varchar(50)),'') as actual,isnull(cast(xxx.target as varchar(60)),'-') as targetytd,xxx.actual as actualytd,isnull(a.achievement,0) as achievement, " & _
                    '            "ff.kpiid,isnull(ff.achievement,-9999) as oldach,p.perspective,isnull(ff.actual,-9999) as oldact,dbo.getTargetString('2011',a.kpiid,1) as targetsekarang " & _
                    '            ",isnull(b.RedCond,'0') as RedCond,isnull(b.GreenCond,'0') as GreenCond,b.calctype,b.basedon, isnull(tp.value,0) as bobot, (select count(Nomor) from dbo.TBSCLAMPIRAN l where l.kpiid=a.kpiid and l.year='" & ddlYear.SelectedValue & "' and l.month='" & ddlMonth.SelectedValue & "') as jmlhattachment, cc.*,isnull(XXX.targetcalc,'9') as  targetcalc, dbo.getTargetString('" & ddlYear.SelectedValue & "',a.kpiid," & ddlMonth.SelectedValue & ") as targetsekarang,b.StEdit " & _
                    '            "from tbsc a inner join MKPI b on a.kpiid = b.id " & _
                    '            "inner join MSO c on b.SOID =c.ID  " & _
                    '            "inner join MST d on c.STID = d.STID  " & _
                    '            " left join ( " & _
                    '            "select kpiid,calctype as targetcalc,case(calctype) when '0' then SUM(target) when '1' then AVG(target) end as target,case(calctype) when '0' then SUM(actual) when '1' then AVG(actual) end as actual " & _
                    '            "from( " & _
                    '            "select a.kpiid,a.month,a.target,case when a.actual is null and target is not null and achievement is null then NULL else a.actual end as actual,tt.calctype from TBSC a inner join MKPI b on a.kpiid = b.id " & _
                    '            "left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year)  b on a.versi=b.versi and a.kpiid = b.kpiid and a.year='" & ddlYear.SelectedValue & "') tt on a.kpiid=tt.kpiid " & _
                    '            "where a.year='" & ddlYear.SelectedValue & "' and a.month <= " & ddlMonth.SelectedValue & " and b.Level='" & KPILevel.SelectedValue & "' and tt.calctype in ('0','1') " & _
                    '            ")dd group by kpiid,calctype " & _
                    '            "        union " & _
                    '            "select kpiid,calctype as targetcalc,case when calctype is null then null else target end as target,actual as actual from( " & _
                    '            "select ROW_NUMBER() OVER(PARTITION BY a.KPIID ORDER BY a.KPIID,MONTH DESC) as rownum, a.kpiid,a.month,a.target,case when a.actual is null and target is not null and achievement is null then NULL else a.actual end as actual,tt.calctype " & _
                    '            "from TBSC a inner join MKPI b on a.kpiid = b.id " & _
                    '            "left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year)  b on a.versi=b.versi and a.kpiid = b.kpiid and a.year='" & ddlYear.SelectedValue & "') tt on a.kpiid=tt.kpiid  " & _
                    '            "where a.year='" & ddlYear.SelectedValue & "' and a.month <= " & ddlMonth.SelectedValue & " and b.Level='" & KPILevel.SelectedValue & "' and isnull(tt.calctype,'3') in ('2','3') " & _
                    '            ")dd where rownum=1 " & _
                    '            ")XXX on a.kpiid=xxx.kpiid " & _
                    '            "left join MJABATAN e on e.id = c.BUMALeader  " & _
                    '            "left join CALCCONTENT cc on b.id=cc.kpiid and cc.calctype=b.calctype " & _
                    '            "left join " & _
                    '            "(select kpiid,achievement,actual from TBSC t where t.year='" & beforeyear & "' and t.month='" & beforemonth & "') ff on ff.kpiid =a.kpiid  left join mperspective p on d.PRID=p.id " & _
                    '            "left join " & _
                    '            "(select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year) b on a.versi=b.versi and a.kpiid = b.kpiid where a.year='" & ddlYear.SelectedValue & "')dd on a.kpiid=dd.kpiid  " & _
                    '            "left join tperspective tp on p.id=tp.pid and tp.year=a.year and tp.level=" & getMapLevel(KPILevel.SelectedValue) & IIf(KPILevel.SelectedValue = 0 Or KPILevel.SelectedValue = 3, "", IIf(KPILevel.SelectedValue = "1", " and tp.funcid=b.func ", " and tp.funcid= (select funcid from msection where id=b.section) ")) & _
                    '            "where a.year='" & ddlYear.SelectedValue & "' and a.month='" & ddlMonth.SelectedValue & "' and b.Level='" & KPILevel.SelectedValue & "' "

                    sqlQuery = "select isnull(b.bobot,0) as bobotkpi,a.year,a.month,a.kpiid as id,b.Level,'' as workunit,d.STName as st,c.SO,isnull(e.jabatan,'-') as soleader," & _
                                "b.Name as kpi,b.Owner as kpiowner,b.UoM,b.Formula,b.RedParam,b.GreenParam,isnull(cast(a.target as varchar(50)),'-') as target,isnull(cast(a.actual  as varchar(50)),'') as actual,dd.annual as targetytd,xxx.actual as actualytd,isnull(a.achievement,0) as achievement, " & _
                                "ff.kpiid,isnull(ff.achievement,-9999) as oldach,p.perspective,isnull(ff.actual,-9999) as oldact,dbo.getTargetString('" & ddlYear.SelectedValue & "',a.kpiid,1) as targetsekarang " & _
                                ",isnull(b.RedCond,'0') as RedCond,isnull(b.GreenCond,'0') as GreenCond,b.calctype,b.basedon, isnull(tp.value,0) as bobot, (select count(Nomor) from dbo.TBSCLAMPIRAN l where l.kpiid=a.kpiid and l.year='" & ddlYear.SelectedValue & "' and l.month='" & ddlMonth.SelectedValue & "') as jmlhattachment, cc.*,isnull(XXX.targetcalc,'9') as  targetcalc, dbo.getTargetString('" & ddlYear.SelectedValue & "',a.kpiid," & ddlMonth.SelectedValue & ") as targetsekarang,b.StEdit,XXX.mynull,XXX.mynullt  " & _
                                "from tbsc a inner join MKPI b on a.kpiid = b.id " & _
                                "inner join MSO c on b.SOID =c.ID  " & _
                                "inner join MST d on c.STID = d.STID  " & _
                                " left join ( " & _
                                " select a.*,count(tt.kpiid) as mynullt  from ( " & _
                                "select kpiid,calctype as targetcalc,SUM(MYNULL) as MYNULL ,case(calctype) when '0' then SUM(actual) when '1' then AVG(actual) end as actual from( " & _
                                "select a.kpiid,a.month,case when a.target is null then null else a.actual end as actual,tt.calctype,case when actual is null then 1 else 0 end as MYNULL from TBSC a inner join MKPI b on a.kpiid = b.id left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year)  b on a.versi=b.versi and a.kpiid = b.kpiid and a.year='" & ddlYear.SelectedValue & "') tt on a.kpiid=tt.kpiid where a.year='" & ddlYear.SelectedValue & "' and a.month <= " & ddlMonth.SelectedValue & " and b.Level='" & KPILevel.SelectedValue & "' and tt.calctype in ('0','1') " & _
                                ")dd group by kpiid,calctype  " & _
                                "        union " & _
                                "select kpiid,calctype as targetcalc,MYNULL,actual from( " & _
                                "select ROW_NUMBER() OVER(PARTITION BY a.KPIID ORDER BY a.KPIID,MONTH DESC) as rownum, a.kpiid,a.month,a.actual as actual,tt.calctype,case when actual is null then 1 else 0 end as MYNULL from TBSC a inner join MKPI b on a.kpiid = b.id left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year)  b on a.versi=b.versi and a.kpiid = b.kpiid and a.year='" & ddlYear.SelectedValue & "') tt on a.kpiid=tt.kpiid  where a.year='" & ddlYear.SelectedValue & "' and a.month <= " & ddlMonth.SelectedValue & " and b.Level='" & KPILevel.SelectedValue & "' and isnull(tt.calctype,'3') in ('2','3') )dd where rownum=1 " & _
                                " )a left join TBSC tt on a.kpiid=tt.kpiid and tt.target is null and tt.year='" & ddlYear.SelectedValue & "'" & _
                                " group by a.kpiid,a.targetcalc,a.MYNULL,a.actual " & _
                                ")XXX on a.kpiid=xxx.kpiid " & _
                                "left join MJABATAN e on e.id = c.BUMALeader  " & _
                                "left join CALCCONTENT cc on b.id=cc.kpiid and cc.calctype=b.calctype " & _
                                "left join " & _
                                "(select kpiid,achievement,actual from TBSC t where t.year='" & beforeyear & "' and t.month='" & beforemonth & "') ff on ff.kpiid =a.kpiid  left join mperspective p on d.PRID=p.id " & _
                                "left join " & _
                                "(select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year) b on a.versi=b.versi and a.kpiid = b.kpiid where a.year='" & ddlYear.SelectedValue & "')dd on a.kpiid=dd.kpiid  " & _
                                "left join tperspective tp on p.id=tp.pid and tp.year=a.year and tp.level=" & getMapLevel(KPILevel.SelectedValue) & IIf(KPILevel.SelectedValue = 0 Or KPILevel.SelectedValue = 3, "", IIf(KPILevel.SelectedValue = "1", " and tp.funcid=b.func ", " and tp.funcid= (select funcid from msection where id=b.section) ")) & _
                                "where a.year='" & ddlYear.SelectedValue & "' and a.month='" & ddlMonth.SelectedValue & "' and b.Level='" & KPILevel.SelectedValue & "'  and b.stedit='0' "


                    Select Case KPILevel.SelectedValue
                        Case "0"
                            sqlQuery += " order by p.id,d.stid,c.ID,b.name"
                        Case "1"
                            sqlQuery += " and b.func='" & KPIFunc.SelectedValue & "' order by p.id,d.stid,c.ID,b.name"
                        Case "2"
                            sqlQuery += " and b.jobsite='" & KPIJobsite.SelectedValue & "' and b.section='" & KPISection.SelectedValue & "' order by p.id,d.stid,c.ID,b.name"
                        Case "3"
                            sqlQuery += " and b.jobsite='" & KPIJobsite.SelectedValue & "' order by p.id,d.stid,c.ID,b.name"
                    End Select

                    Dim da As New SqlDataAdapter(sqlQuery, conn)
                    Dim dt As New DataTable()
                    da.Fill(dt)

                    '-----------------------------------
                    Dim mytableresult As String = ""
                    Dim perspective As String = ""
                    Dim so As String = ""
                    Dim socount As Integer = 1
                    Dim row As Integer = 1
                    Dim ach As Decimal = 0
                    If dt.Rows.Count = 0 Then
                        lblMessage.Text = "No Record Found, Press Create to create transaction record"
                        btnCreate.Visible = False
                        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "tes", "<script type='text/javascript'>changeVisibility(false)</script>")
                        btnPrint.Style.Add("visibility", "hidden")
                        btnSaveNew.Style.Add("visibility", "hidden")
                        btnApprove.Style.Add("visibility", "hidden")
                        header1.InnerHtml = "BSC TRANSACTION"
                    Else
                        param.Value = KPIViewType.SelectedValue + ";" + ddlYear.SelectedValue + ";" + ddlMonth.SelectedValue + ";" + KPILevel.SelectedValue
                        Select Case KPILevel.SelectedValue
                            Case "0"
                                param.Value += ""
                            Case "1"
                                param.Value += ";" + KPIFunc.SelectedValue
                            Case "2"
                                param.Value += ";" + KPIJobsite.SelectedValue + ";" + KPISection.SelectedValue
                            Case "3"
                                param.Value += ";" + KPIJobsite.SelectedValue
                        End Select
                        lblMessage.Text = ""
                        btnCreate.Visible = False
                        btnApprove.Style.Remove("visibility")
                        btnApprove.Style.Add("visibility", "hidden")
                        btnSaveNew.Style.Remove("visibility")
                        btnSaveNew.Style.Add("visibility", "hidden")
                        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "tes", "<script type='text/javascript'>changeVisibility(true)</script>")
                        'btnSave.Visible = True 
                        'Check If Already Approve

                        If ("3;6").IndexOf(Session("permission")) <> -1 Then 'BOD,PM
                            If approved = 0 Then
                                'btnApprove.Style.Remove("visibility")
                            Else
                                'btnApprove.Style.Remove("visibility")
                                'btnApprove.Style.Add("visibility", "hidden")
                            End If
                        ElseIf ("7").IndexOf(Session("permission")) <> -1 Then
                            If approved = 0 Then
                                'btnSaveNew.Style.Remove("visibility")
                                'btnApprove.Style.Remove("visibility")
                            Else
                                'btnSaveNew.Style.Remove("visibility")
                                'btnSaveNew.Style.Add("visibility", "hidden")
                                'btnApprove.Style.Remove("visibility")
                                'btnApprove.Style.Add("visibility", "hidden")
                            End If
                        ElseIf ("4").IndexOf(Session("permission")) <> -1 Then 'GM
                            'btnSaveNew.Style.Remove("visibility")
                            'btnSaveNew.Style.Add("visibility", "hidden")
                            If KPILevel.SelectedValue = "1" Then
                                If approved = 0 Then
                                    'btnApprove.Style.Remove("visibility")
                                Else
                                    'btnApprove.Style.Remove("visibility")
                                    'btnApprove.Style.Add("visibility", "hidden")
                                End If
                            Else
                                'btnApprove.Style.Remove("visibility")
                                'btnApprove.Style.Add("visibility", "hidden")
                            End If
                        Else
                            If approved = 0 Then
                                'btnSaveNew.Style.Remove("visibility")
                            Else
                                'btnSaveNew.Style.Remove("visibility")
                                'btnSaveNew.Style.Add("visibility", "hidden")
                            End If
                        End If
                        btnPrint.Style.Remove("visibility")
                        Select Case KPILevel.SelectedValue
                            Case "0"
                                header1.InnerHtml = "<table style='width:100%'><tr><td style='width:90%'>" & "JRN PERFORMANCE YEARLY " & GetFullMonth(ddlMonth.SelectedValue).ToUpper() & " " & ddlYear.SelectedValue & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                            Case "1"
                                header1.InnerHtml = "<table style='width:100%'><tr><td style='width:90%'>" & KPIFunc.SelectedItem.Text & " PERFORMANCE YEARLY " & GetFullMonth(ddlMonth.SelectedValue).ToUpper() & " " & ddlYear.SelectedValue & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                            Case "2"
                                header1.InnerHtml = "<table style='width:100%'><tr><td style='width:90%'>" & KPISection.SelectedItem.Text & " " & KPIJobsite.SelectedItem.Text & " PERFORMANCE YEARLY " & GetFullMonth(ddlMonth.SelectedValue).ToUpper() & " " & ddlYear.SelectedValue & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                            Case "3"
                                header1.InnerHtml = "<table style='width:100%'><tr><td style='width:90%'>" & KPIJobsite.SelectedItem.Text & " PERFORMANCE YEARLY " & GetFullMonth(ddlMonth.SelectedValue).ToUpper() & " " & ddlYear.SelectedValue & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                        End Select
                    End If
                    'style='align:center;font-size:12px;background-color:#449A50;color:white;font-weight:bold;'
                    mytableresult += "<div id='totalach' style='font-size:24px;font-weight:bold;'></div>"
                    mytableresult += "<table border='1' id='BSCTABLE' class='bsc' style='width:100%;font-size:12px'>"
                    mytableresult += "<thead class='headbsctable'><td style='width:18%' align='center'>Strategic Objective</td><td style='width:30%' align='center'>KPI</td><td align='center' style='width:7%'>KPI Owner</td><td align='center'>UoM</td>" & _
                                     "<td align='center' style='width:10%'>Target</td><td  align='center'>Actual</td><td  align='center'>Achievement <br> (Act Vs Target)</td><td align='center'>Trend</td><td>Attach</td><td align='center'>Details</td></thead>"
                    For Each dr As DataRow In dt.Rows
                        If perspective <> dr("perspective") Then
                            perspective = dr("perspective") '<div id='p" & row & "'>VAL</div>
                            mytableresult += "<tr><td colspan='12' align='center' style='background-color:#EACB92;'>" & perspective & "<br><input type='text' id='p" & row & "' style='width:3%;border-style:none;background:transparent;' READONLY size='5'/>% X <input type='text' style='width:3%;border-style:none;background:transparent;' align='right' READONLY size='5' id='tp" & row & "' value='" & System.Math.Round(dr("bobot"), 2) & "'/> % &nbsp; = <input type='text' style='font-size:24px;font-weight:bold;width:5%;border-style:none;background:transparent;' READONLY size='12' id='hp" & row & "'/><span style='font-size:22px'>%</span></td></tr>"
                        End If
                        If dr("StEdit") = "0" Then
                            mytableresult += "<tr>"
                        Else
                            mytableresult += "<tr style='color:gray'>"
                        End If
                        If so <> dr("SO") Then
                            mytableresult = mytableresult.Replace("x&x", socount)
                            so = dr("SO")
                            socount = 1
                            'mytableresult += "<td rowspan='x&x'>" & dr("SO") & "</td><td rowspan='x&x' align='center'><div id='so" & row & "'>VAL</div></td>"
                            'mytableresult += "<td rowspan='x&x'>" & dr("SO") & "<div id='so" & row & "' style='font-size:16px;text-align:center;padding-top:5px'>VAL</div></td>"
                            mytableresult += "<td rowspan='x&x'>" & dr("SO") & "</td>"
                        Else
                            'mytableresult += "<td>" & dr("SO") & "</td>"
                            socount += 1
                        End If
                        mytableresult += "<td align='left'>" & Replace(dr("kpi"), """", "`") & " [" & dr("bobotkpi") & " %]"
                        mytableresult += "<input type='hidden' id='bobot" & row & "' value='" & dr("bobotkpi") & "'/>"
                        mytableresult += "<input type='hidden' id='kpiid" & row & "' value='" & dr("id") & "'/>"
                        mytableresult += "<input type='hidden' id='year" & row & "' value='" & dr("year") & "'/>"
                        mytableresult += "<input type='hidden' id='month" & row & "' value='" & dr("month") & "'/>"
                        mytableresult += "<input type='hidden' id='calctype" & row & "' value='" & dr("calctype") & "'/>"
                        mytableresult += "<input type='hidden' id='targetcalc" & row & "' value='" & dr("targetcalc") & "'/>"

                        If dr("calctype") = "0" Or dr("calctype") = "1" Then
                        ElseIf dr("calctype") = "2" Or dr("calctype") = "4" Then
                            mytableresult += "<input type='hidden' id='e1x" & row & "' value='" & dr("e1") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='e2x" & row & "' value='" & dr("e2") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='eax" & row & "' value='" & dr("ea") & "'/>"
                            mytableresult += "<input type='hidden' id='mr1x" & row & "' value='" & dr("mr1") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='mr2x" & row & "' value='" & dr("mr2") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='mrax" & row & "' value='" & dr("mra") & "'/>"
                            mytableresult += "<input type='hidden' id='bl1x" & row & "' value='" & dr("bl1") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='bl2x" & row & "' value='" & dr("bl2") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='blax" & row & "' value='" & dr("bla") & "'/>"
                            mytableresult += "<input type='hidden' id='p2x" & row & "' value='" & dr("p2") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='pax" & row & "' value='" & dr("pa") & "'/>"
                        Else
                            mytableresult += "<input type='hidden' id='e1x" & row & "' value='" & dr("e1") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='e2x" & row & "' value='" & dr("e2") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='eax" & row & "' value='" & dr("ea") & "'/>"
                            mytableresult += "<input type='hidden' id='mr1x" & row & "' value='" & dr("mr1") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='mr2x" & row & "' value='" & dr("mr2") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='mr3x" & row & "' value='" & dr("mr3") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='mr4x" & row & "' value='" & dr("mr4") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='mrax" & row & "' value='" & dr("mra") & "'/>"
                            mytableresult += "<input type='hidden' id='bl1x" & row & "' value='" & dr("bl1") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='bl2x" & row & "' value='" & dr("bl2") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='bl3x" & row & "' value='" & dr("bl3") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='bl4x" & row & "' value='" & dr("bl4") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='blax" & row & "' value='" & dr("bla") & "'/>"
                            mytableresult += "<input type='hidden' id='p1x" & row & "' value='" & dr("p1") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='p2x" & row & "' value='" & dr("p2") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='pax" & row & "' value='" & dr("pa") & "'/>"
                        End If

                        mytableresult += "</td><td>" & dr("kpiowner") & "</td><td align='center'>" & dr("UoM") & "</td>"
                        If dr("StEdit") = "1" Then
                            mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='' DISABLED/><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='-' DISABLED/></td>"
                            mytableresult += "<td align='right'><img id='imgoff" & row & "' src='../images/off.png' alt='OFF'/><input type='text' style='border-style:none;width:80%' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' " & checkreadonly(approved) & "/></td><td style='background:#77FFB6'><input type='text' size=3  onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:0' id='kpiach" & row & "' value='' READONLY/></td>"
                        Else
                            If IsDBNull(dr("targetytd")) Then
                                mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='-' DISABLED/><span style='font-size:10px;color:gray;font-style:italic;margin:0;padding:0'>please set target</span><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='-' DISABLED/></td>"
                            ElseIf CStr(dr("targetytd")) = "-" Then
                                If IsDBNull(dr("targetsekarang")) Then
                                    mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='-' DISABLED/><span style='font-size:10px;color:gray;font-style:italic;margin:0;padding:0'>please set target</span><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='-' DISABLED/></td>"
                                Else
                                    mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='' DISABLED/><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='-' DISABLED/></td>"
                                End If
                            Else
                                If dr("target") = "-" Then
                                    mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='" & Format(System.Math.Round(CDec(dr("targetytd")), 2), "#,##0.00") & "' DISABLED/><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='-' DISABLED/></td>"
                                Else
                                    mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='" & Format(System.Math.Round(CDec(dr("targetytd")), 2), "#,##0.00") & "' DISABLED/><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='" & System.Math.Round(CDec(dr("target")), 2) & "' DISABLED/></td>"
                                End If
                            End If

                            If IsDBNull(dr("targetytd")) Then
                                mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")) And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' size=3  onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:0' id='kpiach" & row & "' value='' READONLY/></td>"
                            ElseIf CStr(dr("targetytd")) = "-" Then
                                If IsDBNull(dr("actualytd")) Then
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")) And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' size=3  onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:0' id='kpiach" & row & "' value='' READONLY/></td>"
                                Else
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='" & IIf(dr("mynull") <> 0 And dr("targetytd") = 0, "", IIf(dr("mynull") = ddlMonth.SelectedValue And dr("actualytd") = 0, "", Format(System.Math.Round(dr("actualytd"), 2), "#,##0.00"))) & "' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")) And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' READONLY/> %</td>"
                                End If
                            ElseIf dr("target") = "-" Then
                                If IsDBNull(dr("actualytd")) Then
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='0' READONLY/> %</td>"
                                Else
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='" & IIf(dr("mynull") <> 0 And dr("targetytd") = 0, "", IIf(dr("mynull") = ddlMonth.SelectedValue And dr("actualytd") = 0, "", Format(System.Math.Round(dr("actualytd"), 2), "#,##0.00"))) & "' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")) And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' READONLY/> %</td>"
                                End If
                            ElseIf dr("actual") = "-" Then
                                If IsDBNull(dr("actualytd")) Then
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='0' READONLY/> %</td>"
                                Else
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='" & IIf(dr("mynull") <> 0 And dr("targetytd") = 0, "", IIf(dr("mynull") = ddlMonth.SelectedValue And dr("actualytd") = 0, "", Format(System.Math.Round(dr("actualytd"), 2), "#,##0.00"))) & "' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' READONLY/> %</td>"
                                End If
                            Else
                                If IsDBNull(dr("actualytd")) Then
                                    If dr("actual") = "" Then
                                        mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='0' READONLY/> %</td>"
                                    Else
                                        mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='" & IIf(dr("actual") = "", "", System.Math.Round(CDec(dr("actual")), 2)) & "' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='0' READONLY/> %</td>"
                                    End If
                                Else
                                    If dr("actual") = "" Then
                                        mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='" & IIf(dr("mynull") <> 0 And dr("targetytd") = 0 And dr("targetcalc") = "2", "", IIf(dr("mynull") = ddlMonth.SelectedValue And dr("actualytd") = 0, "", Format(System.Math.Round(dr("actualytd"), 2), "#,##0.00"))) & "' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' READONLY/> %</td>"
                                    Else
                                        mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='" & IIf(dr("mynull") <> 0 And dr("targetytd") = 0, "", IIf(dr("mynull") = ddlMonth.SelectedValue And dr("actualytd") = 0, "", Format(System.Math.Round(dr("actualytd"), 2), "#,##0.00"))) & "' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='" & IIf(dr("actual") = "", "", System.Math.Round(CDec(dr("actual")), 2)) & "' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' READONLY/> %</td>"
                                    End If

                                End If
                            End If
                        End If


                        If dr("calctype") = "0" Then 'MIN
                            If IsDBNull(dr("targetytd")) Then
                                ach = -999999.314
                            ElseIf CStr(dr("targetytd")) = "-" Or dr("target") = "-" Then
                                ach = -999999.314
                            ElseIf dr("actual") = "-" Or dr("actual") = "" Then
                                ach = 0
                            ElseIf dr("actual") = 0 Then
                                If dr("target") = 0 Then
                                    ach = 100
                                Else
                                    ach = 105
                                End If
                            Else
                                ach = dr("target") / dr("actual") * 100
                            End If
                        ElseIf dr("calctype") = "1" Then 'MAX
                            If IsDBNull(dr("targetytd")) Then
                                ach = -999999.314
                            ElseIf CStr(dr("targetytd")) = "-" Or dr("target") = "-" Then
                                ach = -999999.314
                            ElseIf dr("actual") = "-" Or dr("actual") = "" Then
                                ach = 0
                            ElseIf dr("target") = 0 Then
                                If dr("actual") = 0 Then
                                    ach = 100
                                Else
                                    ach = 105
                                End If
                            Else
                                ach = dr("actual") / dr("target") * 100
                            End If
                        ElseIf dr("calctype") = "2" Or dr("calctype") = "4" Then
                            If dr("actual") = "-" Or dr("actual") = "" Then
                                ach = 0
                            Else
                                Select Case dr("calctype")
                                    Case 4 'Max
                                        If (dr("actual") <= dr("e2") And dr("actual") >= dr("e1")) Then
                                            ach = dr("ea")
                                        ElseIf (dr("actual") <= dr("mr2") And dr("actual") >= dr("mr1")) Then
                                            ach = dr("mra")
                                        ElseIf (dr("actual") <= dr("bl2") And dr("actual") >= dr("bl1")) Then
                                            ach = dr("bla")
                                        ElseIf (dr("actual") <= dr("p2")) Then
                                            ach = dr("pa")
                                        Else
                                            ach = 100
                                        End If
                                    Case 2 'Min
                                        If (dr("actual") <= dr("e2") And dr("actual") >= dr("e1")) Then
                                            ach = dr("ea")
                                        ElseIf (dr("actual") <= dr("mr2") And dr("actual") >= dr("mr1")) Then
                                            ach = dr("mra")
                                        ElseIf (dr("actual") <= dr("bl2") And dr("actual") >= dr("bl1")) Then
                                            ach = dr("bla")
                                        ElseIf (dr("actual") > dr("p2")) Then
                                            ach = dr("pa")
                                        Else
                                            ach = 100
                                        End If
                                End Select
                            End If
                        ElseIf dr("calctype") = "3" Then
                            If dr("actual") = "-" Or dr("actual") = "" Then
                                ach = 0
                            Else
                                If (dr("actual") <= dr("e2") And dr("actual") >= dr("e1")) Then
                                    ach = dr("ea")
                                ElseIf (dr("actual") <= dr("mr2") And dr("actual") >= dr("mr1")) Or (dr("actual") <= dr("mr4") And dr("actual") >= dr("mr3")) Then
                                    ach = dr("mra")
                                ElseIf (dr("actual") <= dr("bl2") And dr("actual") >= dr("bl1")) Or (dr("actual") <= dr("bl4") And dr("actual") >= dr("bl3")) Then
                                    ach = dr("bla")
                                ElseIf (dr("actual") > dr("p2") Or dr("actual") < dr("p1")) Then
                                    ach = dr("pa")
                                Else
                                    ach = 100
                                End If
                            End If
                        End If

                        Dim myactual As Decimal = 0
                        If IsDBNull(dr("actual")) Then
                            myactual = 0
                        ElseIf dr("actual") = "" Then
                            myactual = 0
                        Else
                            myactual = dr("actual")
                        End If
                        If IsDBNull(dr("targetytd")) Then
                            mytableresult += "<td align='center'></td>"
                        ElseIf CStr(dr("targetytd")) = "-" Or dr("actual") = "-" Or dr("StEdit") = "1" Then
                            mytableresult += "<td align='center'></td>"
                        ElseIf dr("oldact") = -9999 Then
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "equal.png' style='width:100%'/></td>"
                        ElseIf dr("oldact") > myactual Then
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "down.png' style='width:100%'/></td>"
                        ElseIf dr("oldact") < myactual Then
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "up.png' style='width:100%'/></td>"
                        Else
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "equal.png' style='width:100%'/></td>"
                        End If
                        mytableresult += "<td align='center'>" & IIf(dr("jmlhattachment") > 0, "<img src='../images/book.jpg' alt='attachment' height='30px' width='30px'/>", "") & "<input type='button' id='btnUpload' " & disabledUpload(approved, dr("StEdit")) & " Style='width:10px;height:25px;background-image: url(../images/attachment_icon.gif); background-repeat: no-repeat;cursor:hand;background-color:transparent;border:0' onclick='javascript:openupload(""" & dr("id") & """,""" & dr("level") & """,""" & dr("year") & """,""" & dr("month") & """,""" & Replace(dr("kpi"), """", "`") & """)' DISABLED/></td>"
                        mytableresult += "<td align='center'><input type='button' id='btnAdd' class='btn' value='VIEW' onclick='javascript:openmodal(""" & dr("id") & """,""" & dr("level") & """,""" & dr("year") & """,""" & dr("month") & """,""" & Replace(dr("kpi"), """", "`") & """)'/></td></tr>"
                        row += 1
                    Next
                    mytableresult = mytableresult.Replace("x&x", socount)
                    result.InnerHtml = mytableresult + "</table>"
                      btnSaveNew.Style.Remove("visibility")
                        btnSaveNew.Style.Add("visibility", "hidden")
                    '-----------------------------------

                    'If dt.Rows.Count = 0 Then
                    '    lblMessage.Text = "No Record Found, Press Create to create transaction record"
                    '    btnCreate.Visible = True
                    '    btnSave.Visible = False
                    '    rptTrans.DataSource = Nothing
                    '    rptTrans.DataBind()
                    'Else
                    '    btnCreate.Visible = False
                    '    btnSave.Visible = True
                    '    rptTrans.DataSource = dt
                    '    rptTrans.DataBind()
                    'End If

                    'For Each x As RepeaterItem In rptTrans.Items
                    '    Try
                    '        If CDec(CType(x.FindControl("achievement"), TextBox).Text) > CDec(CType(x.FindControl("oldach"), HiddenField).Value) Then
                    '            CType(x.FindControl("imgtrend"), SYstem.Web.UI.WebControls.Image).ImageUrl = "images/up.png"
                    '        ElseIf CDec(CType(x.FindControl("achievement"), TextBox).Text) = CDec(CType(x.FindControl("oldach"), HiddenField).Value) Then
                    '            CType(x.FindControl("imgtrend"), SYstem.Web.UI.WebControls.Image).ImageUrl = "images/equal.png"
                    '        ElseIf CDec(CType(x.FindControl("achievement"), TextBox).Text) < CDec(CType(x.FindControl("oldach"), HiddenField).Value) Then
                    '            CType(x.FindControl("imgtrend"), SYstem.Web.UI.WebControls.Image).ImageUrl = "images/down.png"
                    '        End If
                    '    Catch ex As Exception
                    '        CType(x.FindControl("imgtrend"), SYstem.Web.UI.WebControls.Image).ImageUrl = ""
                    '    End Try
                    'Next

                Case "2" 'Try YTD
                    Dim sqlQuery As String = ""
                    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
                    Dim beforeyear As String
                    Dim beforemonth As String
                    Dim cmd As SqlCommand
                    Dim approved As Integer = 0
                    If ddlMonth.SelectedValue = 1 Then
                        beforeyear = ddlYear.SelectedValue - 1
                        beforemonth = 12
                    Else
                        beforeyear = ddlYear.SelectedValue
                        beforemonth = ddlMonth.SelectedValue - 1
                    End If

                    Select Case KPILevel.SelectedValue
                        Case "0"
                            sqlQuery = "Select count(*) from TBSCAPPR where level='0' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "'"
                        Case "1"
                            sqlQuery = "Select count(*) from TBSCAPPR where level='1' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "' and func='" & KPIFunc.SelectedValue & "'"
                        Case "2"
                            sqlQuery = "Select count(*) from TBSCAPPR where level='2' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "' and jobsite='" & KPIJobsite.SelectedValue & "' and section='" & KPISection.SelectedValue & "'"
                        Case "3"
                            sqlQuery = "Select count(*) from TBSCAPPR where level='3' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "' and jobsite='" & KPIJobsite.SelectedValue & "'"
                    End Select
                    cmd = New SqlCommand(sqlQuery, conn)
                    conn.Open()
                    approved = cmd.ExecuteScalar
                    conn.Close()

                    sqlQuery = " select ROW_NUMBER() OVER(PARTITION BY a.KPIID ORDER BY a.KPIID,MONTH DESC) as rownum, a.kpiid,a.month,a.target,case when a.actual is null and target is not null and achievement is null then NULL else a.actual end as actual,tt.calctype,case when actual is null then 1 else 0 end as MYNULL " & _
                               "into #LASTVAL " & _
                               "from TBSC a inner join MKPI b on a.kpiid = b.id left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year)  b on a.versi=b.versi and a.kpiid = b.kpiid and a.year='" & ddlYear.SelectedValue & "') tt on a.kpiid=tt.kpiid  where a.year='" & ddlYear.SelectedValue & "' and a.month <= " & ddlMonth.SelectedValue & " and b.Level='" & KPILevel.SelectedValue & "' and isnull(tt.calctype,'3') in ('2');"

                    sqlQuery += "select isnull(b.bobot,0) as bobotkpi,a.year,a.month,a.kpiid as id,b.Level,'' as workunit,d.STName as st,c.SO,isnull(e.jabatan,'-') as soleader," & _
                                "b.Name as kpi,b.Owner as kpiowner,b.UoM,b.Formula,b.RedParam,b.GreenParam,isnull(cast(a.target as varchar(50)),'-') as target,isnull(cast(a.actual  as varchar(50)),'') as actual,isnull(cast(xxx.target as varchar(60)),'-') as targetytd,xxx.actual as actualytd,isnull(a.achievement,0) as achievement, " & _
                                "ff.kpiid,isnull(ff.achievement,-9999) as oldach,p.perspective,isnull(ff.actual,-9999) as oldact,dbo.getTargetString('" & ddlYear.SelectedValue & "',a.kpiid,1) as targetsekarang " & _
                                ",isnull(b.RedCond,'0') as RedCond,isnull(b.GreenCond,'0') as GreenCond,b.calctype,b.basedon, isnull(tp.value,0) as bobot, (select count(Nomor) from dbo.TBSCLAMPIRAN l where l.kpiid=a.kpiid and l.year='" & ddlYear.SelectedValue & "' and l.month='" & ddlMonth.SelectedValue & "') as jmlhattachment, cc.*,isnull(XXX.targetcalc,'9') as  targetcalc, dbo.getTargetString('" & ddlYear.SelectedValue & "',a.kpiid," & ddlMonth.SelectedValue & ") as targetsekarang,b.StEdit,isnull(XXX.mynull,'1') as mynull,isnull(XXX.mynullt,'1') as mynullt " & _
                                "from tbsc a inner join MKPI b on a.kpiid = b.id " & _
                                "inner join MSO c on b.SOID =c.ID  " & _
                                "inner join MST d on c.STID = d.STID  " & _
                                " left join ( " & _
                                "select a.*,count(tt.kpiid) as mynullt  from (select kpiid,calctype as targetcalc,SUM(MYNULL) as MYNULL,case(calctype) when '0' then SUM(target) when '1' then AVG(target) end as target,case(calctype) when '0' then SUM(actual) when '1' then AVG(actual) end as actual " & _
                                "from( " & _
                                "select a.kpiid,a.month,a.target,case when a.actual is null and target is not null and achievement is null then NULL else a.actual end as actual,tt.calctype,case when actual is null then 1 else 0 end as MYNULL from TBSC a inner join MKPI b on a.kpiid = b.id " & _
                                "left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year)  b on a.versi=b.versi and a.kpiid = b.kpiid and a.year='" & ddlYear.SelectedValue & "') tt on a.kpiid=tt.kpiid " & _
                                "where a.year='" & ddlYear.SelectedValue & "' and a.month <= " & ddlMonth.SelectedValue & " and b.Level='" & KPILevel.SelectedValue & "' and tt.calctype in ('0','1') " & _
                                ")dd group by kpiid,calctype " & _
                                "        union " & _
                                "select kpiid,calctype as targetcalc,MYNULL,case when calctype is null then null else target end as target,actual as actual from( " & _
                                "select ROW_NUMBER() OVER(PARTITION BY a.KPIID ORDER BY a.KPIID,MONTH DESC) as rownum, a.kpiid,a.month,a.target,case when a.actual is null and target is not null and achievement is null then NULL else a.actual end as actual,tt.calctype,case when actual is null then 1 else 0 end as MYNULL " & _
                                "from TBSC a inner join MKPI b on a.kpiid = b.id " & _
                                "left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year)  b on a.versi=b.versi and a.kpiid = b.kpiid and a.year='" & ddlYear.SelectedValue & "') tt on a.kpiid=tt.kpiid  " & _
                                "where a.year='" & ddlYear.SelectedValue & "' and a.month <= " & ddlMonth.SelectedValue & " and b.Level='" & KPILevel.SelectedValue & "' and isnull(tt.calctype,'3') in ('3') " & _
                                ")dd where rownum=1   union  " & _
                                "select b.kpiid, calctype as targetcalc,mynull,b.target,b.actual as actual from (" & _
                                "select kpiid,MAX(month) as lastmonth from #LASTVAL where target is not null group by kpiid" & _
                                ") a left join #lastval b on a.kpiid=b.kpiid and a.lastmonth=b.month" & _
                                ")a left join TBSC tt on a.kpiid=tt.kpiid and tt.target is null and tt.year='" & ddlYear.SelectedValue & "'" & _
                                " group by a.kpiid,a.targetcalc,a.MYNULL,a.target,a.actual " & _
                                ")XXX on a.kpiid=xxx.kpiid " & _
                                "left join MJABATAN e on e.id = c.BUMALeader  " & _
                                "left join CALCCONTENT cc on b.id=cc.kpiid and cc.calctype=b.calctype " & _
                                "left join " & _
                                "(select kpiid,achievement,actual from TBSC t where t.year='" & beforeyear & "' and t.month='" & beforemonth & "') ff on ff.kpiid =a.kpiid  left join mperspective p on d.PRID=p.id " & _
                                "left join " & _
                                "(select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year) b on a.versi=b.versi and a.kpiid = b.kpiid where a.year='" & ddlYear.SelectedValue & "')dd on a.kpiid=dd.kpiid  " & _
                                "left join tperspective tp on p.id=tp.pid and tp.year=a.year and tp.level=" & getMapLevel(KPILevel.SelectedValue) & IIf(KPILevel.SelectedValue = 0 Or KPILevel.SelectedValue = 3, "", IIf(KPILevel.SelectedValue = "1", " and tp.funcid=b.func ", " and tp.funcid= (select funcid from msection where id=b.section) ")) & _
                                "where a.year='" & ddlYear.SelectedValue & "' and a.month='" & ddlMonth.SelectedValue & "' and b.Level='" & KPILevel.SelectedValue & "' and b.StEdit='0'"


                    'sqlQuery = "select a.year,a.month,a.kpiid as id,b.Level,'' as workunit,d.STName as st,c.SO,isnull(e.jabatan,'-') as soleader," & _
                    '            "b.Name as kpi,b.Owner as kpiowner,b.UoM,b.Formula,b.RedParam,b.GreenParam,isnull(cast(a.target as varchar(50)),'-') as target,isnull(cast(a.actual  as varchar(50)),'') as actual,dd.annual as targetytd,xxx.actual as actualytd,isnull(a.achievement,0) as achievement, " & _
                    '            "ff.kpiid,isnull(ff.achievement,-9999) as oldach,p.perspective,isnull(ff.actual,-9999) as oldact,dbo.getTargetString('" & ddlYear.SelectedValue & "',a.kpiid,1) as targetsekarang " & _
                    '            ",isnull(b.RedCond,'0') as RedCond,isnull(b.GreenCond,'0') as GreenCond,b.calctype,b.basedon, isnull(tp.value,0) as bobot, (select count(Nomor) from dbo.TBSCLAMPIRAN l where l.kpiid=a.kpiid and l.year='" & ddlYear.SelectedValue & "' and l.month='" & ddlMonth.SelectedValue & "') as jmlhattachment, cc.*,isnull(XXX.targetcalc,'9') as  targetcalc, dbo.getTargetString('" & ddlYear.SelectedValue & "',a.kpiid," & ddlMonth.SelectedValue & ") as targetsekarang,b.StEdit,XXX.mynull,XXX.mynullt  " & _
                    '            "from tbsc a inner join MKPI b on a.kpiid = b.id " & _
                    '            "inner join MSO c on b.SOID =c.ID  " & _
                    '            "inner join MST d on c.STID = d.STID  " & _
                    '            " left join ( " & _
                    '            " select a.*,count(tt.kpiid) as mynullt  from ( " & _
                    '            "select kpiid,calctype as targetcalc,SUM(MYNULL) as MYNULL ,case(calctype) when '0' then SUM(actual) when '1' then AVG(actual) end as actual from( " & _
                    '            "select a.kpiid,a.month,case when a.target is null then null else a.actual end as actual,tt.calctype,case when actual is null then 1 else 0 end as MYNULL from TBSC a inner join MKPI b on a.kpiid = b.id left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year)  b on a.versi=b.versi and a.kpiid = b.kpiid and a.year='" & ddlYear.SelectedValue & "') tt on a.kpiid=tt.kpiid where a.year='" & ddlYear.SelectedValue & "' and a.month <= " & ddlMonth.SelectedValue & " and b.Level='" & KPILevel.SelectedValue & "' and tt.calctype in ('0','1') " & _
                    '            ")dd group by kpiid,calctype  " & _
                    '            "        union " & _
                    '            "select kpiid,calctype as targetcalc,MYNULL,actual from( " & _
                    '            "select ROW_NUMBER() OVER(PARTITION BY a.KPIID ORDER BY a.KPIID,MONTH DESC) as rownum, a.kpiid,a.month,a.actual as actual,tt.calctype,case when actual is null then 1 else 0 end as MYNULL from TBSC a inner join MKPI b on a.kpiid = b.id left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year)  b on a.versi=b.versi and a.kpiid = b.kpiid and a.year='" & ddlYear.SelectedValue & "') tt on a.kpiid=tt.kpiid  where a.year='" & ddlYear.SelectedValue & "' and a.month <= " & ddlMonth.SelectedValue & " and b.Level='" & KPILevel.SelectedValue & "' and isnull(tt.calctype,'3') in ('2','3') )dd where rownum=1 " & _
                    '            " )a left join TBSC tt on a.kpiid=tt.kpiid and tt.target is null and tt.year='" & ddlYear.SelectedValue & "'" & _
                    '            " group by a.kpiid,a.targetcalc,a.MYNULL,a.actual " & _
                    '            ")XXX on a.kpiid=xxx.kpiid " & _
                    '            "left join MJABATAN e on e.id = c.BUMALeader  " & _
                    '            "left join CALCCONTENT cc on b.id=cc.kpiid and cc.calctype=b.calctype " & _
                    '            "left join " & _
                    '            "(select kpiid,achievement,actual from TBSC t where t.year='" & beforeyear & "' and t.month='" & beforemonth & "') ff on ff.kpiid =a.kpiid  left join mperspective p on d.PRID=p.id " & _
                    '            "left join " & _
                    '            "(select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year) b on a.versi=b.versi and a.kpiid = b.kpiid where a.year='" & ddlYear.SelectedValue & "')dd on a.kpiid=dd.kpiid  " & _
                    '            "left join tperspective tp on p.id=tp.pid and tp.year=a.year and tp.level=" & getMapLevel(KPILevel.SelectedValue) & IIf(KPILevel.SelectedValue = 0 Or KPILevel.SelectedValue = 3, "", IIf(KPILevel.SelectedValue = "1", " and tp.funcid=b.func ", " and tp.funcid= (select funcid from msection where id=b.section) ")) & _
                    '            "where a.year='" & ddlYear.SelectedValue & "' and a.month='" & ddlMonth.SelectedValue & "' and b.Level='" & KPILevel.SelectedValue & "'  and b.stedit='0' "


                    Select Case KPILevel.SelectedValue
                        Case "0"
                            sqlQuery += " order by p.id,d.stid,c.ID,b.name;"
                        Case "1"
                            sqlQuery += " and b.func='" & KPIFunc.SelectedValue & "' order by p.id,d.stid,c.ID,b.name;"
                        Case "2"
                            sqlQuery += " and b.jobsite='" & KPIJobsite.SelectedValue & "' and b.section='" & KPISection.SelectedValue & "' order by p.id,d.stid,c.ID,b.name;"
                        Case "3"
                            sqlQuery += " and b.jobsite='" & KPIJobsite.SelectedValue & "' order by p.id,d.stid,c.ID,b.name;"
                    End Select

                    sqlQuery += ";DROP TABLE #LASTVAL;"

                    Dim da As New SqlDataAdapter(sqlQuery, conn)
                    Dim dt As New DataTable()
                    da.Fill(dt)

                    '-----------------------------------
                    Dim mytableresult As String = ""
                    Dim perspective As String = ""
                    Dim so As String = ""
                    Dim socount As Integer = 1
                    Dim row As Integer = 1
                    Dim ach As Decimal = 0
                    If dt.Rows.Count = 0 Then
                        lblMessage.Text = "No Record Found, Press Create to create transaction record"
                        btnCreate.Visible = False
                        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "tes", "<script type='text/javascript'>changeVisibility(false)</script>")
                        btnPrint.Style.Add("visibility", "hidden")
                        btnSaveNew.Style.Add("visibility", "hidden")
                        btnApprove.Style.Add("visibility", "hidden")
                        header1.InnerHtml = "BSC TRANSACTION"
                    Else
                        param.Value = KPIViewType.SelectedValue + ";" + ddlYear.SelectedValue + ";" + ddlMonth.SelectedValue + ";" + KPILevel.SelectedValue
                        Select Case KPILevel.SelectedValue
                            Case "0"
                                param.Value += ""
                            Case "1"
                                param.Value += ";" + KPIFunc.SelectedValue
                            Case "2"
                                param.Value += ";" + KPIJobsite.SelectedValue + ";" + KPISection.SelectedValue
                            Case "3"
                                param.Value += ";" + KPIJobsite.SelectedValue
                        End Select
                        lblMessage.Text = ""
                        btnCreate.Visible = False
                        btnApprove.Style.Remove("visibility")
                        btnApprove.Style.Add("visibility", "hidden")
                        btnSaveNew.Style.Remove("visibility")
                        btnSaveNew.Style.Add("visibility", "hidden")
                        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "tes", "<script type='text/javascript'>changeVisibility(true)</script>")
                        'btnSave.Visible = True 
                        'Check If Already Approve

                        If ("3;6").IndexOf(Session("permission")) <> -1 Then 'BOD,PM
                            If approved = 0 Then
                                'btnApprove.Style.Remove("visibility")
                            Else
                                'btnApprove.Style.Remove("visibility")
                                'btnApprove.Style.Add("visibility", "hidden")
                            End If
                        ElseIf ("7").IndexOf(Session("permission")) <> -1 Then
                            If approved = 0 Then
                                'btnSaveNew.Style.Remove("visibility")
                                'btnApprove.Style.Remove("visibility")
                            Else
                                'btnSaveNew.Style.Remove("visibility")
                                'btnSaveNew.Style.Add("visibility", "hidden")
                                'btnApprove.Style.Remove("visibility")
                                'btnApprove.Style.Add("visibility", "hidden")
                            End If
                        ElseIf ("4").IndexOf(Session("permission")) <> -1 Then 'GM
                            'btnSaveNew.Style.Remove("visibility")
                            'btnSaveNew.Style.Add("visibility", "hidden")
                            If KPILevel.SelectedValue = "1" Then
                                If approved = 0 Then
                                    'btnApprove.Style.Remove("visibility")
                                Else
                                    'btnApprove.Style.Remove("visibility")
                                    'btnApprove.Style.Add("visibility", "hidden")
                                End If
                            Else
                                'btnApprove.Style.Remove("visibility")
                                'btnApprove.Style.Add("visibility", "hidden")
                            End If
                        Else
                            If approved = 0 Then
                                'btnSaveNew.Style.Remove("visibility")
                            Else
                                'btnSaveNew.Style.Remove("visibility")
                                'btnSaveNew.Style.Add("visibility", "hidden")
                            End If
                        End If
                        btnPrint.Style.Remove("visibility")
                        Select Case KPILevel.SelectedValue
                            Case "0"
                                header1.InnerHtml = "<table style='width:100%'><tr><td style='width:90%'>" & "JRN PERFORMANCE YEARLY " & GetFullMonth(ddlMonth.SelectedValue).ToUpper() & " " & ddlYear.SelectedValue & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                            Case "1"
                                header1.InnerHtml = "<table style='width:100%'><tr><td style='width:90%'>" & KPIFunc.SelectedItem.Text & " PERFORMANCE YEARLY " & GetFullMonth(ddlMonth.SelectedValue).ToUpper() & " " & ddlYear.SelectedValue & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                            Case "2"
                                header1.InnerHtml = "<table style='width:100%'><tr><td style='width:90%'>" & KPISection.SelectedItem.Text & " " & KPIJobsite.SelectedItem.Text & " PERFORMANCE YEARLY " & GetFullMonth(ddlMonth.SelectedValue).ToUpper() & " " & ddlYear.SelectedValue & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                            Case "3"
                                header1.InnerHtml = "<table style='width:100%'><tr><td style='width:90%'>" & KPIJobsite.SelectedItem.Text & " PERFORMANCE YEARLY " & GetFullMonth(ddlMonth.SelectedValue).ToUpper() & " " & ddlYear.SelectedValue & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                        End Select
                    End If
                    'style='align:center;font-size:12px;background-color:#449A50;color:white;font-weight:bold;'
                    mytableresult += "<div id='totalach' style='font-size:24px;font-weight:bold;'></div>"
                    mytableresult += "<table border='1' id='BSCTABLE' class='bsc' style='width:100%;font-size:12px'>"
                    mytableresult += "<thead class='headbsctable'><td style='width:18%' align='center'>Strategic Objective</td><td style='width:30%' align='center'>KPI</td><td align='center' style='width:7%'>KPI Owner</td><td align='center'>UoM</td>" & _
                                     "<td align='center' style='width:10%'>Target</td><td  align='center'>Actual</td><td  align='center'>Achievement <br> (Act Vs Target)</td><td align='center'>Trend</td><td>Attach</td><td align='center'>Details</td></thead>"
                    For Each dr As DataRow In dt.Rows
                        If perspective <> dr("perspective") Then
                            perspective = dr("perspective") '<div id='p" & row & "'>VAL</div>
                            mytableresult += "<tr><td colspan='12' align='center' style='background-color:#EACB92;'>" & perspective & "<br><input type='text' id='p" & row & "' style='width:3%;border-style:none;background:transparent;' READONLY size='5'/>% X <input type='text' style='width:3%;border-style:none;background:transparent;' align='right' READONLY size='5' id='tp" & row & "' value='" & System.Math.Round(dr("bobot"), 2) & "'/> % &nbsp; = <input type='text' style='font-size:24px;font-weight:bold;width:5%;border-style:none;background:transparent;' READONLY size='12' id='hp" & row & "'/><span style='font-size:22px'>%</span></td></tr>"
                        End If
                        If dr("StEdit") = "0" Then
                            mytableresult += "<tr>"
                        Else
                            mytableresult += "<tr style='color:gray'>"
                        End If
                        If so <> dr("SO") Then
                            mytableresult = mytableresult.Replace("x&x", socount)
                            so = dr("SO")
                            socount = 1
                            'mytableresult += "<td rowspan='x&x'>" & dr("SO") & "</td><td rowspan='x&x' align='center'><div id='so" & row & "'>VAL</div></td>"
                            'mytableresult += "<td rowspan='x&x'>" & dr("SO") & "<div id='so" & row & "' style='font-size:16px;text-align:center;padding-top:5px'>VAL</div></td>"
                            mytableresult += "<td rowspan='x&x'>" & dr("SO") & "</td>"
                        Else
                            'mytableresult += "<td>" & dr("SO") & "</td>"
                            socount += 1
                        End If
                        mytableresult += "<td align='left'>" & Replace(dr("kpi"), """", "`") & " [" & dr("bobotkpi") & " %]"
                        mytableresult += "<input type='hidden' id='bobot" & row & "' value='" & dr("bobotkpi") & "'/>"
                        mytableresult += "<input type='hidden' id='kpiid" & row & "' value='" & dr("id") & "'/>"
                        mytableresult += "<input type='hidden' id='year" & row & "' value='" & dr("year") & "'/>"
                        mytableresult += "<input type='hidden' id='month" & row & "' value='" & dr("month") & "'/>"
                        mytableresult += "<input type='hidden' id='calctype" & row & "' value='" & dr("calctype") & "'/>"
                        mytableresult += "<input type='hidden' id='targetcalc" & row & "' value='" & dr("targetcalc") & "'/>"

                        If dr("calctype") = "0" Or dr("calctype") = "1" Then
                        ElseIf dr("calctype") = "2" Or dr("calctype") = "4" Then
                            mytableresult += "<input type='hidden' id='e1x" & row & "' value='" & dr("e1") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='e2x" & row & "' value='" & dr("e2") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='eax" & row & "' value='" & dr("ea") & "'/>"
                            mytableresult += "<input type='hidden' id='mr1x" & row & "' value='" & dr("mr1") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='mr2x" & row & "' value='" & dr("mr2") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='mrax" & row & "' value='" & dr("mra") & "'/>"
                            mytableresult += "<input type='hidden' id='bl1x" & row & "' value='" & dr("bl1") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='bl2x" & row & "' value='" & dr("bl2") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='blax" & row & "' value='" & dr("bla") & "'/>"
                            mytableresult += "<input type='hidden' id='p2x" & row & "' value='" & dr("p2") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='pax" & row & "' value='" & dr("pa") & "'/>"
                        Else
                            mytableresult += "<input type='hidden' id='e1x" & row & "' value='" & dr("e1") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='e2x" & row & "' value='" & dr("e2") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='eax" & row & "' value='" & dr("ea") & "'/>"
                            mytableresult += "<input type='hidden' id='mr1x" & row & "' value='" & dr("mr1") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='mr2x" & row & "' value='" & dr("mr2") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='mr3x" & row & "' value='" & dr("mr3") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='mr4x" & row & "' value='" & dr("mr4") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='mrax" & row & "' value='" & dr("mra") & "'/>"
                            mytableresult += "<input type='hidden' id='bl1x" & row & "' value='" & dr("bl1") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='bl2x" & row & "' value='" & dr("bl2") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='bl3x" & row & "' value='" & dr("bl3") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='bl4x" & row & "' value='" & dr("bl4") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='blax" & row & "' value='" & dr("bla") & "'/>"
                            mytableresult += "<input type='hidden' id='p1x" & row & "' value='" & dr("p1") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='p2x" & row & "' value='" & dr("p2") * IIf(dr("targetcalc") = 0, (12 - dr("mynullt")), 1) & "'/>"
                            mytableresult += "<input type='hidden' id='pax" & row & "' value='" & dr("pa") & "'/>"
                        End If

                        mytableresult += "</td><td>" & dr("kpiowner") & "</td><td align='center'>" & dr("UoM") & "</td>"
                        If dr("StEdit") = "1" Then
                            mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='' DISABLED/><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='-' DISABLED/></td>"
                            mytableresult += "<td align='right'><img id='imgoff" & row & "' src='../images/off.png' alt='OFF'/><input type='text' style='border-style:none;width:80%' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' " & checkreadonly(approved) & "/></td><td style='background:#77FFB6'><input type='text' size=3  onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:0' id='kpiach" & row & "' value='' READONLY/></td>"
                        Else
                            If IsDBNull(dr("targetytd")) Then
                                mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='-' DISABLED/><span style='font-size:10px;color:gray;font-style:italic;margin:0;padding:0'>please set target</span><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='-' DISABLED/></td>"
                            ElseIf CStr(dr("targetytd")) = "-" Then
                                If IsDBNull(dr("targetsekarang")) Then
                                    mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='-' DISABLED/><span style='font-size:10px;color:gray;font-style:italic;margin:0;padding:0'>please set target</span><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='-' DISABLED/></td>"
                                Else
                                    mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='' DISABLED/><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='-' DISABLED/></td>"
                                End If
                            Else
                                If dr("target") = "-" Then
                                    mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='" & Format(System.Math.Round(CDec(dr("targetytd")), 2), "#,##0.00") & "' DISABLED/><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='-' DISABLED/></td>"
                                Else
                                    mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='" & Format(System.Math.Round(CDec(dr("targetytd")), 2), "#,##0.00") & "' DISABLED/><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='" & System.Math.Round(CDec(dr("target")), 2) & "' DISABLED/></td>"
                                End If
                            End If

                            If IsDBNull(dr("targetytd")) Then
                                mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")) And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' size=3  onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:0' id='kpiach" & row & "' value='' READONLY/></td>"
                            ElseIf CStr(dr("targetytd")) = "-" Then
                                If IsDBNull(dr("actualytd")) Then
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")) And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' size=3  onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:0' id='kpiach" & row & "' value='' READONLY/></td>"
                                Else
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='" & IIf(dr("mynull") <> 0 And dr("targetytd") = 0, "", IIf(dr("mynull") = ddlMonth.SelectedValue And dr("actualytd") = 0, "", Format(System.Math.Round(dr("actualytd"), 2), "#,##0.00"))) & "' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")) And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' READONLY/> %</td>"
                                End If
                            ElseIf dr("target") = "-" Then
                                If IsDBNull(dr("actualytd")) Then
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='0' READONLY/> %</td>"
                                Else
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='" & IIf(dr("mynull") <> 0 And dr("targetytd") = 0, "", IIf(dr("mynull") = ddlMonth.SelectedValue And dr("actualytd") = 0, "", Format(System.Math.Round(dr("actualytd"), 2), "#,##0.00"))) & "' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")) And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' READONLY/> %</td>"
                                End If
                            ElseIf dr("actual") = "-" Then
                                If IsDBNull(dr("actualytd")) Then
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='0' READONLY/> %</td>"
                                Else
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='" & IIf(dr("mynull") <> 0 And dr("targetytd") = 0, "", IIf(dr("mynull") = ddlMonth.SelectedValue And dr("actualytd") = 0, "", Format(System.Math.Round(dr("actualytd"), 2), "#,##0.00"))) & "' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' READONLY/> %</td>"
                                End If
                            Else
                                If IsDBNull(dr("actualytd")) Then
                                    If dr("actual") = "" Then
                                        mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='0' READONLY/> %</td>"
                                    Else
                                        mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='" & IIf(dr("actual") = "", "", System.Math.Round(CDec(dr("actual")), 2)) & "' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='0' READONLY/> %</td>"
                                    End If
                                Else
                                    If dr("actual") = "" Then
                                        mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='" & IIf(dr("mynull") <> 0 And dr("targetytd") = 0 And dr("targetcalc") = "2", "", IIf(dr("mynull") = ddlMonth.SelectedValue And dr("actualytd") = 0, "", Format(System.Math.Round(dr("actualytd"), 2), "#,##0.00"))) & "' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' READONLY/> %</td>"
                                    Else
                                        mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='" & IIf(dr("mynull") <> 0 And dr("targetytd") = 0, "", IIf(dr("mynull") = ddlMonth.SelectedValue And dr("actualytd") = 0, "", Format(System.Math.Round(dr("actualytd"), 2), "#,##0.00"))) & "' " & checkreadonly(approved) & " READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='" & IIf(dr("actual") = "", "", System.Math.Round(CDec(dr("actual")), 2)) & "' " & checkreadonly(approved) & "/></td><td style='" & IIf(dr("mynull") <> 0 And Not IsDBNull(dr("targetytd")), "background:#47BBFF", "background:#77FFB6") & "'><input type='text' onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:85%' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' READONLY/> %</td>"
                                    End If

                                End If
                            End If
                        End If


                        If dr("calctype") = "0" Then 'MIN
                            If IsDBNull(dr("targetytd")) Then
                                ach = -999999.314
                            ElseIf CStr(dr("targetytd")) = "-" Or dr("target") = "-" Then
                                ach = -999999.314
                            ElseIf dr("actual") = "-" Or dr("actual") = "" Then
                                ach = 0
                            ElseIf dr("actual") = 0 Then
                                If dr("target") = 0 Then
                                    ach = 100
                                Else
                                    ach = 105
                                End If
                            Else
                                ach = dr("target") / dr("actual") * 100
                            End If
                        ElseIf dr("calctype") = "1" Then 'MAX
                            If IsDBNull(dr("targetytd")) Then
                                ach = -999999.314
                            ElseIf CStr(dr("targetytd")) = "-" Or dr("target") = "-" Then
                                ach = -999999.314
                            ElseIf dr("actual") = "-" Or dr("actual") = "" Then
                                ach = 0
                            ElseIf dr("target") = 0 Then
                                If dr("actual") = 0 Then
                                    ach = 100
                                Else
                                    ach = 105
                                End If
                            Else
                                ach = dr("actual") / dr("target") * 100
                            End If
                        ElseIf dr("calctype") = "2" Or dr("calctype") = "4" Then
                            If dr("actual") = "-" Or dr("actual") = "" Then
                                ach = 0
                            Else
                                Select Case dr("calctype")
                                    Case 4 'Max
                                        If (dr("actual") <= dr("e2") And dr("actual") >= dr("e1")) Then
                                            ach = dr("ea")
                                        ElseIf (dr("actual") <= dr("mr2") And dr("actual") >= dr("mr1")) Then
                                            ach = dr("mra")
                                        ElseIf (dr("actual") <= dr("bl2") And dr("actual") >= dr("bl1")) Then
                                            ach = dr("bla")
                                        ElseIf (dr("actual") <= dr("p2")) Then
                                            ach = dr("pa")
                                        Else
                                            ach = 100
                                        End If
                                    Case 2 'Min
                                        If (dr("actual") <= dr("e2") And dr("actual") >= dr("e1")) Then
                                            ach = dr("ea")
                                        ElseIf (dr("actual") <= dr("mr2") And dr("actual") >= dr("mr1")) Then
                                            ach = dr("mra")
                                        ElseIf (dr("actual") <= dr("bl2") And dr("actual") >= dr("bl1")) Then
                                            ach = dr("bla")
                                        ElseIf (dr("actual") > dr("p2")) Then
                                            ach = dr("pa")
                                        Else
                                            ach = 100
                                        End If
                                End Select
                            End If
                        ElseIf dr("calctype") = "3" Then
                            If dr("actual") = "-" Or dr("actual") = "" Then
                                ach = 0
                            Else
                                If (dr("actual") <= dr("e2") And dr("actual") >= dr("e1")) Then
                                    ach = dr("ea")
                                ElseIf (dr("actual") <= dr("mr2") And dr("actual") >= dr("mr1")) Or (dr("actual") <= dr("mr4") And dr("actual") >= dr("mr3")) Then
                                    ach = dr("mra")
                                ElseIf (dr("actual") <= dr("bl2") And dr("actual") >= dr("bl1")) Or (dr("actual") <= dr("bl4") And dr("actual") >= dr("bl3")) Then
                                    ach = dr("bla")
                                ElseIf (dr("actual") > dr("p2") Or dr("actual") < dr("p1")) Then
                                    ach = dr("pa")
                                Else
                                    ach = 100
                                End If
                            End If
                        End If

                        Dim myactual As Decimal = 0
                        If IsDBNull(dr("actual")) Then
                            myactual = 0
                        ElseIf dr("actual") = "" Then
                            myactual = 0
                        Else
                            myactual = dr("actual")
                        End If
                        If IsDBNull(dr("targetytd")) Then
                            mytableresult += "<td align='center'></td>"
                        ElseIf CStr(dr("targetytd")) = "-" Or dr("actual") = "-" Or dr("StEdit") = "1" Then
                            mytableresult += "<td align='center'></td>"
                        ElseIf dr("oldact") = -9999 Then
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "equal.png' style='width:100%'/></td>"
                        ElseIf dr("oldact") > myactual Then
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "down.png' style='width:100%'/></td>"
                        ElseIf dr("oldact") < myactual Then
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "up.png' style='width:100%'/></td>"
                        Else
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "equal.png' style='width:100%'/></td>"
                        End If
                        mytableresult += "<td align='center'>" & IIf(dr("jmlhattachment") > 0, "<img src='../images/book.jpg' alt='attachment' height='30px' width='30px'/>", "") & "<input type='button' id='btnUpload' " & disabledUpload(approved, dr("StEdit")) & " Style='width:10px;height:25px;background-image: url(../images/attachment_icon.gif); background-repeat: no-repeat;cursor:hand;background-color:transparent;border:0' onclick='javascript:openupload(""" & dr("id") & """,""" & dr("level") & """,""" & dr("year") & """,""" & dr("month") & """,""" & Replace(dr("kpi"), """", "`") & """)' DISABLED/></td>"
                        mytableresult += "<td align='center'><input type='button' id='btnAdd' class='btn' value='VIEW' onclick='javascript:openmodal(""" & dr("id") & """,""" & dr("level") & """,""" & dr("year") & """,""" & dr("month") & """,""" & Replace(dr("kpi"), """", "`") & """)'/></td></tr>"
                        row += 1
                    Next
                    mytableresult = mytableresult.Replace("x&x", socount)
                    result.InnerHtml = mytableresult + "</table>"
                    '-----------------------------------

                    'If dt.Rows.Count = 0 Then
                    '    lblMessage.Text = "No Record Found, Press Create to create transaction record"
                    '    btnCreate.Visible = True
                    '    btnSave.Visible = False
                    '    rptTrans.DataSource = Nothing
                    '    rptTrans.DataBind()
                    'Else
                    '    btnCreate.Visible = False
                    '    btnSave.Visible = True
                    '    rptTrans.DataSource = dt
                    '    rptTrans.DataBind()
                    'End If

                    'For Each x As RepeaterItem In rptTrans.Items
                    '    Try
                    '        If CDec(CType(x.FindControl("achievement"), TextBox).Text) > CDec(CType(x.FindControl("oldach"), HiddenField).Value) Then
                    '            CType(x.FindControl("imgtrend"), SYstem.Web.UI.WebControls.Image).ImageUrl = "images/up.png"
                    '        ElseIf CDec(CType(x.FindControl("achievement"), TextBox).Text) = CDec(CType(x.FindControl("oldach"), HiddenField).Value) Then
                    '            CType(x.FindControl("imgtrend"), SYstem.Web.UI.WebControls.Image).ImageUrl = "images/equal.png"
                    '        ElseIf CDec(CType(x.FindControl("achievement"), TextBox).Text) < CDec(CType(x.FindControl("oldach"), HiddenField).Value) Then
                    '            CType(x.FindControl("imgtrend"), SYstem.Web.UI.WebControls.Image).ImageUrl = "images/down.png"
                    '        End If
                    '    Catch ex As Exception
                    '        CType(x.FindControl("imgtrend"), SYstem.Web.UI.WebControls.Image).ImageUrl = ""
                    '    End Try
                    'Next
            End Select
        Catch ex As Exception
            lblMessage.Text = "Error, Silahkan copy paste error ini ke IT : " & ex.Message
        End Try
    End Sub

    Function GetColor(ByVal approved As String, ByVal ttarget As Decimal, ByVal atarget As Decimal) As String
        If approved = 0 Then
            If Math.Round(ttarget, 2) <> Math.Round(atarget, 2) Then
                Return ";background-color:#DF4946"
            End If
        End If
        Return ""
    End Function

    '''BACKUP SCRIPT
    ''' Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
    '    Try
    'Dim sqlQuery As String = ""
    'Dim cmd As SqlCommand
    'Dim dr As SqlDataReader
    'Dim approved As Integer = 0
    '        Select Case KPILevel.SelectedValue
    '            Case "0"
    '                sqlQuery = "Select count(*) from TBSCAPPR where level='0' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "'"
    '            Case "1"
    '                sqlQuery = "Select count(*) from TBSCAPPR where level='1' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "' and func='" & KPIFunc.SelectedValue & "'"
    '            Case "2"
    '                sqlQuery = "Select count(*) from TBSCAPPR where level='2' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "' and jobsite='" & KPIJobsite.SelectedValue & "' and section='" & KPISection.SelectedValue & "'"
    '            Case "3"
    '                sqlQuery = "Select count(*) from TBSCAPPR where level='3' and month ='" & ddlMonth.SelectedValue & "' and year='" & ddlYear.SelectedValue & "' and jobsite='" & KPIJobsite.SelectedValue & "'"
    '        End Select
    '        cmd = New SqlCommand(sqlQuery, conn)
    '        conn.Open()
    '        approved = cmd.ExecuteScalar
    '        conn.Close()
    '        If approved = 1 Then
    '            lblMessage.Text = "BSC Dengan Kriteria Yang Dipilih Sudah Diapproved, Tidak Bisa Di-Create Lagi"
    '            Exit Sub
    '        End If

    ''----------------------------------
    ''sqlQuery = "select * from MKPI where ID not in (select kpiid from TBSC where YEAR='" & ddlYear.SelectedValue & "' and month='" & ddlMonth.SelectedValue & "')"
    '        Select Case KPILevel.SelectedValue
    '            Case "0"
    '                sqlQuery = "select kpi.*,dd." & GetMonth(ddlMonth.SelectedValue) & " as target from MKPI kpi "
    '                sqlQuery += " left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year) b on a.versi=b.versi and a.kpiid = b.kpiid where a.year='" & ddlYear.SelectedValue & "')dd on kpi.id=dd.kpiid "
    '                sqlQuery += " where kpi.ID not in (select kpiid from TBSC where YEAR='" & ddlYear.SelectedValue & "' and month='" & ddlMonth.SelectedValue & "') and level='0'"
    '            Case "1"
    '                sqlQuery = "select kpi.*,dd." & GetMonth(ddlMonth.SelectedValue) & " as target from MKPI kpi "
    '                sqlQuery += " left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year) b on a.versi=b.versi and a.kpiid = b.kpiid where a.year='" & ddlYear.SelectedValue & "')dd on kpi.id=dd.kpiid "
    '                sqlQuery += " where kpi.ID not in (select kpiid from TBSC where YEAR='" & ddlYear.SelectedValue & "' and month='" & ddlMonth.SelectedValue & "') and level='1' and func='" & KPIFunc.SelectedValue & "'"
    '            Case "2"
    '                sqlQuery = "select kpi.*,dd." & GetMonth(ddlMonth.SelectedValue) & " as target from MKPI kpi "
    '                sqlQuery += " left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year) b on a.versi=b.versi and a.kpiid = b.kpiid where a.year='" & ddlYear.SelectedValue & "')dd on kpi.id=dd.kpiid "
    '                sqlQuery += " where kpi.ID not in (select kpiid from TBSC where YEAR='" & ddlYear.SelectedValue & "' and month='" & ddlMonth.SelectedValue & "') and level='2' and jobsite='" & KPIJobsite.SelectedValue & "' and section='" & KPISection.SelectedValue & "'"
    '            Case "3"
    '                sqlQuery = "select kpi.*,dd." & GetMonth(ddlMonth.SelectedValue) & " as target from MKPI kpi "
    '                sqlQuery += " left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year) b on a.versi=b.versi and a.kpiid = b.kpiid where a.year='" & ddlYear.SelectedValue & "')dd on kpi.id=dd.kpiid "
    '                sqlQuery += " where kpi.ID not in (select kpiid from TBSC where YEAR='" & ddlYear.SelectedValue & "' and month='" & ddlMonth.SelectedValue & "') and level='3' and jobsite='" & KPIJobsite.SelectedValue & "'"
    '        End Select
    ''----------------------------------
    '        cmd = New SqlCommand(sqlQuery, conn)
    '        conn.Open()
    '        dr = cmd.ExecuteReader()
    '        If dr.HasRows Then
    '            sqlQuery = "BEGIN TRANSACTION; BEGIN TRY "
    '            While dr.Read
    '                if isdbnull(dr("target")) then
    '                    sqlQuery += "insert into TBSC (Year,Month,kpiid,target,actual,achievement,picaref,createdin,createdby) values ('" _
    '                    & ddlYear.SelectedValue & "','" & ddlMonth.SelectedValue & "','" & dr("ID") & "',NULL,NULL,NULL,NULL,'" & Request.UserHostName & "','" & Request.UserHostName & "');"
    '                else
    '                    sqlQuery += "insert into TBSC (Year,Month,kpiid,target,actual,achievement,picaref,createdin,createdby) values ('" _
    '                    & ddlYear.SelectedValue & "','" & ddlMonth.SelectedValue & "','" & dr("ID") & "'," & dr("target") & ",NULL,NULL,NULL,'" & Request.UserHostName & "','" & Request.UserHostName & "');"
    '                End If
    '            End While
    '            dr.Close()
    '            dr = Nothing
    '            sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
    '            cmd = New SqlCommand(sqlQuery, conn)
    'Dim result As String = cmd.ExecuteScalar()
    '            If result = 0 Then
    '                lblMessage.Text = "Data BSC Berhasil tergenerate"
    '            Else
    '                lblMessage.Text = result
    '            End If
    '        Else
    '            dr.Close()
    '            dr = Nothing
    '            lblMessage.Text = "Data Master KPI sudah tergenerate untuk data BSC tahun tersebut"
    '        End If
    '    Catch ex As Exception
    '        lblMessage.Text = ex.Message
    '    Finally
    '        If conn.State = ConnectionState.Open Then
    '            conn.Close()
    '        End If
    '    End Try

    Private Sub KPIViewType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles KPIViewType.SelectedIndexChanged
        Try
            Select Case KPIViewType.SelectedValue
                Case 0
                    btnCreate.Visible = True
                Case 1
                    btnCreate.Visible = False
            End Select
            result.InnerHtml = ""
            header1.InnerHtml = ""
            btnSaveNew.Style.Remove("visibility")
            btnSaveNew.Style.Add("visibility", "hidden")
            btnApprove.Style.Remove("visibility")
            btnApprove.Style.Add("visibility", "hidden")
            btnPrint.Style.Remove("visibility")
            btnPrint.Style.Add("visibility", "hidden")
            btnUnApprove.Style.Remove("visibility")
            btnUnApprove.Style.Add("visibility", "hidden")
        Catch ex As Exception

        End Try
    End Sub

    Sub Initialize()
         Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim sqlQuery As String = ""
            Dim cmd As SqlCommand
            Dim cmd1 As SqlCommand
            Dim dr As SqlDataReader
            Dim approved As Integer = 0
            conn.Open()
                For i As Integer = 1 To 12
                    sqlQuery = "select kpi.*,dd." & GetMonth(i) & " as target from MKPI kpi "
                    sqlQuery += " left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & ddlYear.SelectedValue & "' group by kpiid,year) b on a.versi=b.versi and a.kpiid = b.kpiid where a.year='" & ddlYear.SelectedValue & "')dd on kpi.id=dd.kpiid "
                    sqlQuery += " left join TBSCAPPR appr on kpi.Level=appr.level and isnull(kpi.Func,'')=isnull(appr.Func,'') and isnull(kpi.Jobsite,'')=isnull(appr.jobsite,'') and isnull(kpi.Section,'')=isnull(appr.section,'') and appr.year='" & ddlYear.SelectedValue & "' and appr.month='" & i.tostring() + "'"
                sqlQuery += " where kpi.year='" & ddlYear.SelectedValue & "' and kpi.ID not in (select kpiid from TBSC where YEAR='" & ddlYear.SelectedValue & "' and month='" & i & "') and appr.id is null"
                    cmd1 = New SqlCommand(sqlQuery, conn)
                    dr = cmd1.ExecuteReader()
                    If dr.HasRows Then
                        sqlQuery = "BEGIN TRANSACTION; BEGIN TRY "
                        While dr.Read
                            If IsDBNull(dr("target")) Then
                                sqlQuery += "insert into TBSC (Year,Month,kpiid,target,actual,achievement,picaref,createdin,createdby) values ('" _
                                & ddlYear.SelectedValue & "','" & i.tostring() & "','" & dr("ID") & "',NULL,NULL,NULL,NULL,'" & Request.UserHostName & "','" & Request.UserHostName & "');"
                            ElseIf dr("target") = "" Then
                                sqlQuery += "insert into TBSC (Year,Month,kpiid,target,actual,achievement,picaref,createdin,createdby) values ('" _
                                & ddlYear.SelectedValue & "','" & i.tostring() & "','" & dr("ID") & "',NULL,NULL,NULL,NULL,'" & Request.UserHostName & "','" & Request.UserHostName & "');"
                            Else
                                sqlQuery += "insert into TBSC (Year,Month,kpiid,target,actual,achievement,picaref,createdin,createdby) values ('" _
                                & ddlYear.SelectedValue & "','" & i.tostring() & "','" & dr("ID") & "'," & dr("target") & ",NULL,NULL,NULL,'" & Request.UserHostName & "','" & Request.UserHostName & "');"
                            End If
                        End While
                        dr.Close()
                        dr = Nothing
                        sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
                        cmd = New SqlCommand(sqlQuery, conn)
                        Dim result As String = cmd.ExecuteScalar()
                        If result = 0 Then
                            lblMessage.Text += "Data BSC bulan " & i & " berhasil tergenerate <br>"
                        Else
                            lblMessage.Text = result
                        End If
                    Else
                        dr.Close()
                        dr = Nothing
                    End If
                Next
                doSearch()
        Catch ex As Exception
            lblMessage.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub
End Class