﻿//alert("JANGAN LUPA MENGGANTI SPLIT IMAGES 4-->5 ketika deploy ke production")
function calc() {
    var tbl = document.getElementById('BSCTABLE');
    if (tbl != null) {
        var numofp = 0;
        var numofpso = 0;
        var numofkpi = 0;
        var tempp;
        var tempso;
        var sovalue = 0;
        var socount = 0;
        var soofp = 0;
        var row = 1;
        var totalach = 0;
        var totalperspective = 0;
        var achbobot = 0;
        for (var i = 1; i < tbl.rows.length; i++) {
            var id = document.getElementById("kpiid" + row).value;
            switch (tbl.rows[i].cells.length) {
                case 1:
                    {
                        if ((row - 1) != 0) {
                            tempp = "p" + numofp;
                            numofp = row;
                        } else {
                            numofp = row;
                        }

                        if (document.getElementById(tempp) != null) {
                            document.getElementById(tempp).value = achbobot;
                            document.getElementById("h" + tempp).value = Math.round((achbobot * document.getElementById("t" + tempp).value) / 100 * 100) / 100;
                            totalach += (Math.round(achbobot * 100) / 100 * document.getElementById("t" + tempp).value) / 100;
                            totalperspective += parseFloat(document.getElementById("t" + tempp).value);
                        }

                        achbobot = 0;
                        break;
                    }
                case 10:
                    {
                        //count achievement based on min or max
                        if (document.getElementById("kpia" + row).value == '') {
                            //document.getElementById("kpiach" + row).value = 0;
                            document.getElementById("kpiach" + row).value = '';
                        } else {
                            if (document.getElementById("calctype" + row).value == 0) {
                                if (document.getElementById("kpit" + row).value == '-') {
                                    document.getElementById("kpiach" + row).value = '-';
                                } else if (document.getElementById("kpit" + row).value == '') {
                                    document.getElementById("kpiach" + row).value = '';
                                } else if (document.getElementById("kpia" + row).value == 0) {
                                    if (document.getElementById("kpit" + row).value == 0)
                                        document.getElementById("kpiach" + row).value = 100;
                                    else
                                        document.getElementById("kpiach" + row).value = 105;
                                } else {
                                    document.getElementById("kpiach" + row).value = Math.round((stripNonNumeric(document.getElementById("kpit" + row).value) / stripNonNumeric(document.getElementById("kpia" + row).value) * 100) * 100) / 100
                                    if (parseFloat(document.getElementById("kpiach" + row).value) > 105)
                                        document.getElementById("kpiach" + row).value = 105;
                                }
                            } else if (document.getElementById("calctype" + row).value == 1) {
                                if (document.getElementById("kpit" + row).value == '-') {
                                    document.getElementById("kpiach" + row).value = '-';
                                } else if (document.getElementById("kpit" + row).value == '') {
                                    document.getElementById("kpiach" + row).value = '';
                                } else if (document.getElementById("kpit" + row).value == 0) {
                                    if (document.getElementById("kpia" + row).value == 0)
                                        document.getElementById("kpiach" + row).value = 100;
                                    else
                                        document.getElementById("kpiach" + row).value = 105;
                                } else {
                                    document.getElementById("kpiach" + row).value = Math.round((stripNonNumeric(document.getElementById("kpia" + row).value) / stripNonNumeric(document.getElementById("kpit" + row).value) * 100) * 100) / 100;
                                    if (parseFloat(document.getElementById("kpiach" + row).value) > 105)
                                        document.getElementById("kpiach" + row).value = 105;
                                }
                            } else if (document.getElementById("calctype" + row).value == 2 || document.getElementById("calctype" + row).value == 4) {
                                if (document.getElementById("kpit" + row).value == '-') {
                                    document.getElementById("kpiach" + row).value = '-';
                                } else if (document.getElementById("kpit" + row).value == '') {
                                    document.getElementById("kpiach" + row).value = '';
                                } else {
                                    if (document.getElementById("calctype" + row).value == 4) {
                                        if (document.getElementById("ctl00_ContentPlaceHolder1_KPIViewType").value == "1" && document.getElementById("targetcalc" + row).value == "0") { //YTD & SUM
                                            if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("e2x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("e1x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value))) {
                                                document.getElementById("kpiach" + row).value = document.getElementById("eax" + row).value;
                                            } else if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("mr2x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("mr1x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value))) {
                                                document.getElementById("kpiach" + row).value = document.getElementById("mrax" + row).value;
                                            } else if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("bl2x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("bl1x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value))) {
                                                document.getElementById("kpiach" + row).value = document.getElementById("blax" + row).value;
                                            } else if (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("p2x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value)) {
                                                document.getElementById("kpiach" + row).value = document.getElementById("pax" + row).value;
                                            } else {
                                                document.getElementById("kpiach" + row).value = 100;
                                            }
                                        } else {
                                            if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("e2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("e1x" + row).value))) {
                                                document.getElementById("kpiach" + row).value = document.getElementById("eax" + row).value;
                                            } else if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("mr2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("mr1x" + row).value))) {
                                                document.getElementById("kpiach" + row).value = document.getElementById("mrax" + row).value;
                                            } else if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("bl2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("bl1x" + row).value))) {
                                                document.getElementById("kpiach" + row).value = document.getElementById("blax" + row).value;
                                            } else if (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("p2x" + row).value)) {
                                                document.getElementById("kpiach" + row).value = document.getElementById("pax" + row).value;
                                            } else {
                                                document.getElementById("kpiach" + row).value = 100;
                                            }
                                        }
                                    } else {//MIN
                                        if (document.getElementById("ctl00_ContentPlaceHolder1_KPIViewType").value == "1" && document.getElementById("targetcalc" + row).value == "0") { //YTD & SUM
                                            if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("e2x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("e1x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value))) {
                                                document.getElementById("kpiach" + row).value = document.getElementById("eax" + row).value;
                                            } else if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("mr2x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("mr1x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value))) {
                                                document.getElementById("kpiach" + row).value = document.getElementById("mrax" + row).value;
                                            } else if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("bl2x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("bl1x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value))) {
                                                document.getElementById("kpiach" + row).value = document.getElementById("blax" + row).value;
                                            } else if (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) > parseFloat(document.getElementById("p2x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value)) {
                                                document.getElementById("kpiach" + row).value = document.getElementById("pax" + row).value;
                                            } else {
                                                document.getElementById("kpiach" + row).value = 100;
                                            }
                                        } else {
                                            if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("e2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("e1x" + row).value))) {
                                                document.getElementById("kpiach" + row).value = document.getElementById("eax" + row).value;
                                            } else if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("mr2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("mr1x" + row).value))) {
                                                document.getElementById("kpiach" + row).value = document.getElementById("mrax" + row).value;
                                            } else if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("bl2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("bl1x" + row).value))) {
                                                document.getElementById("kpiach" + row).value = document.getElementById("blax" + row).value;
                                            } else if (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) > parseFloat(document.getElementById("p2x" + row).value)) {
                                                document.getElementById("kpiach" + row).value = document.getElementById("pax" + row).value;
                                            } else {
                                                document.getElementById("kpiach" + row).value = 100;
                                            }
                                        }
                                    }
                                }
                            } else { //STABILIZE
                                if (document.getElementById("kpit" + row).value == '-') {
                                    document.getElementById("kpiach" + row).value = '-';
                                } else if (document.getElementById("kpit" + row).value == '') {
                                    document.getElementById("kpiach" + row).value = '';
                                } else {
                                    if (document.getElementById("ctl00_ContentPlaceHolder1_KPIViewType").value == "1" && document.getElementById("targetcalc" + row).value == "0") { //YTD & SUM
                                        if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("e2x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("e1x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value))) {
                                            document.getElementById("kpiach" + row).value = document.getElementById("eax" + row).value;
                                        } else if (((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("mr2x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("mr1x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value))) || ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("mr4x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("mr3x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value)))) {
                                            document.getElementById("kpiach" + row).value = document.getElementById("mrax" + row).value;
                                        } else if (((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("bl2x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("bl1x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value))) || ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("bl4x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("bl3x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value)))) {
                                            document.getElementById("kpiach" + row).value = document.getElementById("blax" + row).value;
                                        } else if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) > parseFloat(document.getElementById("p2x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value)) || (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) < parseFloat(document.getElementById("p1x" + row).value) * parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value))) {
                                            document.getElementById("kpiach" + row).value = document.getElementById("pax" + row).value;
                                        } else {
                                            document.getElementById("kpiach" + row).value = 100;
                                        }
                                    } else {
                                        if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("e2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("e1x" + row).value))) {
                                            document.getElementById("kpiach" + row).value = document.getElementById("eax" + row).value;
                                        } else if (((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("mr2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("mr1x" + row).value))) || ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("mr4x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("mr3x" + row).value)))) {
                                            document.getElementById("kpiach" + row).value = document.getElementById("mrax" + row).value;
                                        } else if (((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("bl2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("bl1x" + row).value))) || ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("bl4x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("bl3x" + row).value)))) {
                                            document.getElementById("kpiach" + row).value = document.getElementById("blax" + row).value;
                                        } else if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) > parseFloat(document.getElementById("p2x" + row).value)) || (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) < parseFloat(document.getElementById("p1x" + row).value))) {
                                            document.getElementById("kpiach" + row).value = document.getElementById("pax" + row).value;
                                        } else {
                                            document.getElementById("kpiach" + row).value = 100;
                                        }
                                    }
                                }
                            }
                        }

                        if (document.getElementById("ctl00_ContentPlaceHolder1_KPIViewType").selectedIndex == 0) {
                            if (document.getElementById("hkpiach" + row) != null) {
                                if ((parseFloat(document.getElementById("hkpiach" + row).value) != parseFloat(document.getElementById("kpiach" + row).value)) && (document.getElementById("kpiach" + row).value != ""))
                                    document.getElementById("kpiach" + row).style.backgroundColor = '#31FF26';
                                else
                                    document.getElementById("kpiach" + row).style.background = 'transparent';
                            }
                        }

                        //alert(tbl.rows[i].cells[0].innerHTML);
                        //                        if (tempso != undefined) {
                        //                            if (socount == 0)
                        //                                document.getElementById(tempso).innerHTML = 'Achievement = - ';
                        //                            else
                        //                                document.getElementById(tempso).innerHTML = 'Achievement = ' + (Math.round((sovalue / socount * 100)) / 100) + ' %';
                        //                            if (!isNaN(sovalue / socount)) {
                        //                                numofpso += sovalue / socount;
                        //                                numofkpi += 1;
                        //                            }
                        //                        }

                        sovalue = 0;
                        socount = 0;
                        tempso = "so" + row;
                        if (document.getElementById("kpiach" + row).value == '-') {
                        } else if (document.getElementById("kpiach" + row).value == '') {
                        } else if (document.getElementById("img" + row) == null) {
                        } else {
                            socount += 1;
                            switch (document.getElementById("img" + row).src.split("/")[5]) {
                                case "redup.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                                case "reddown.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                                case "redequal.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                                case "yellowup.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                                case "yellowdown.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                                case "yellowequal.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                                case "greenup.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                                case "greendown.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                                case "greenequal.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                            }
                        }

                        //CALCULATE ACH
                        //                        if (tempp != undefined) {
                        //                            document.getElementById(tempp).value = Math.round((achbobot) * 100) / 100;
                        //                        }

                        if (document.getElementById("kpiach" + row) != null) {
                            //alert(document.getElementById("kpiach" + row).value + "--" + document.getElementById("bobot" + row).value);
                            if (document.getElementById("kpit" + row).value == '') {   //KALAU GA ADA TARGET ITUNG FULL
                                achbobot += parseFloat(document.getElementById("bobot" + row).value);
                                //alert('item:' + achbobot);
                            } else if (document.getElementById("kpiach" + row).value != '') {
                                achbobot += parseFloat(document.getElementById("bobot" + row).value) * parseFloat(document.getElementById("kpiach" + row).value / 100);
                                //alert('item:' + achbobot);
                            } else { }
                        }
                        row += 1;

                        //                        if (tempp != undefined) {
                        //                            //document.getElementById(tempp).innerHTML = Math.round((numofpso / numofkpi) * 100) / 100;
                        //                            if (!isNaN(numofpso) && numofpso != 0 && numofkpi != 0) {
                        //                                document.getElementById(tempp).value = Math.round((numofpso / numofkpi) * 100) / 100;
                        //                                document.getElementById("h" + tempp).value = Math.round((Math.round((numofpso / numofkpi) * 100) / 100 * document.getElementById("t" + tempp).value) / 100 * 100) / 100;
                        //                                totalach += (Math.round((numofpso / numofkpi) * 100) / 100 * document.getElementById("t" + tempp).value) / 100;
                        //                                totalperspective += parseFloat(document.getElementById("t" + tempp).value);
                        //                            } else {
                        //                                if (document.getElementById(tempp) != null) {
                        //                                    //document.getElementById(tempp).value = 0;
                        //                                    document.getElementById(tempp).value = '';
                        //                                    //document.getElementById("h" + tempp).value = 0;
                        //                                    document.getElementById("h" + tempp).value = '';
                        //                                    totalach += 0;
                        //                                }
                        //                                if (numofkpi != 0) {
                        //                                    totalperspective += parseFloat(document.getElementById("t" + tempp).value);
                        //                                }
                        //                            }
                        //                            tempp = undefined;
                        //                            numofkpi = 0;
                        //                            numofpso = 0;
                        //                        }


                        break;
                    }
                case 9:
                    {
                        //count achievement based on min or max
                        if (document.getElementById("kpia" + row).value == '') {
                            //document.getElementById("kpiach" + row).value = 0;
                            document.getElementById("kpiach" + row).value = '';
                        } else {
                            if (document.getElementById("calctype" + row).value == 0) {
                                if (document.getElementById("kpit" + row).value == '-') {
                                    document.getElementById("kpiach" + row).value = '-';
                                } else if (document.getElementById("kpit" + row).value == '') {
                                    document.getElementById("kpiach" + row).value = '';
                                } else if (document.getElementById("kpia" + row).value == 0) {
                                    if (document.getElementById("kpit" + row).value == 0)
                                        document.getElementById("kpiach" + row).value = 100;
                                    else
                                        document.getElementById("kpiach" + row).value = 105;
                                } else {
                                    document.getElementById("kpiach" + row).value = Math.round((stripNonNumeric(document.getElementById("kpit" + row).value) / stripNonNumeric(document.getElementById("kpia" + row).value) * 100) * 100) / 100;
                                    if (parseFloat(document.getElementById("kpiach" + row).value) > 105)
                                        document.getElementById("kpiach" + row).value = 105;
                                }
                            } else if (document.getElementById("calctype" + row).value == 1) {
                                if (document.getElementById("kpit" + row).value == '-') {
                                    document.getElementById("kpiach" + row).value = '-';
                                } else if (document.getElementById("kpit" + row).value == '') {
                                    document.getElementById("kpiach" + row).value = '';
                                } else if (document.getElementById("kpit" + row).value == 0) {
                                    if (document.getElementById("kpia" + row).value == 0)
                                        document.getElementById("kpiach" + row).value = 100;
                                    else
                                        document.getElementById("kpiach" + row).value = 105;
                                } else {
                                    document.getElementById("kpiach" + row).value = Math.round((stripNonNumeric(document.getElementById("kpia" + row).value) / stripNonNumeric(document.getElementById("kpit" + row).value) * 100) * 100) / 100;
                                    if (parseFloat(document.getElementById("kpiach" + row).value) > 105)
                                        document.getElementById("kpiach" + row).value = 105;
                                }
                            } else if (document.getElementById("calctype" + row).value == 2 || document.getElementById("calctype" + row).value == 4) {
                                if (document.getElementById("kpit" + row).value == '-') {
                                    document.getElementById("kpiach" + row).value = '-';
                                } else if (document.getElementById("kpit" + row).value == '') {
                                    document.getElementById("kpiach" + row).value = '';
                                } else {
                                    if (document.getElementById("calctype" + row).value == 4) {
                                        if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("e2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("e1x" + row).value))) {
                                            document.getElementById("kpiach" + row).value = document.getElementById("eax" + row).value;
                                        } else if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("mr2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("mr1x" + row).value))) {
                                            document.getElementById("kpiach" + row).value = document.getElementById("mrax" + row).value;
                                        } else if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("bl2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("bl1x" + row).value))) {
                                            document.getElementById("kpiach" + row).value = document.getElementById("blax" + row).value;
                                        } else if (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("p2x" + row).value)) {
                                            document.getElementById("kpiach" + row).value = document.getElementById("pax" + row).value;
                                        } else {
                                            document.getElementById("kpiach" + row).value = 100;
                                        }
                                    } else {//MIN
                                        if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("e2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("e1x" + row).value))) {
                                            document.getElementById("kpiach" + row).value = document.getElementById("eax" + row).value;
                                        } else if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("mr2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("mr1x" + row).value))) {
                                            document.getElementById("kpiach" + row).value = document.getElementById("mrax" + row).value;
                                        } else if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("bl2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("bl1x" + row).value))) {
                                            document.getElementById("kpiach" + row).value = document.getElementById("blax" + row).value;
                                        } else if (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) > parseFloat(document.getElementById("p2x" + row).value)) {
                                            document.getElementById("kpiach" + row).value = document.getElementById("pax" + row).value;
                                        } else {
                                            document.getElementById("kpiach" + row).value = 100;
                                        }
                                    }
                                }
                            } else {
                                if (document.getElementById("kpit" + row).value == '-') {
                                    document.getElementById("kpiach" + row).value = '-';
                                } else if (document.getElementById("kpit" + row).value == '') {
                                    document.getElementById("kpiach" + row).value = '';
                                } else {
                                    if ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("e2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("e1x" + row).value))) {
                                        document.getElementById("kpiach" + row).value = document.getElementById("eax" + row).value;
                                    } else if (((stripNonNumeric(parseFloat(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("mr2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("mr1x" + row).value))) || ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("mr4x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("mr3x" + row).value)))) {
                                        document.getElementById("kpiach" + row).value = document.getElementById("mrax" + row).value;
                                    } else if (((stripNonNumeric(parseFloat(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("bl2x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("bl1x" + row).value))) || ((parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) <= parseFloat(document.getElementById("bl4x" + row).value)) && (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) >= parseFloat(document.getElementById("bl3x" + row).value)))) {
                                        document.getElementById("kpiach" + row).value = document.getElementById("blax" + row).value;
                                    } else if ((stripNonNumeric(parseFloat(document.getElementById("kpia" + row).value)) > parseFloat(document.getElementById("p2x" + row).value)) || (parseFloat(stripNonNumeric(document.getElementById("kpia" + row).value)) < parseFloat(document.getElementById("p1x" + row).value))) {
                                        document.getElementById("kpiach" + row).value = document.getElementById("pax" + row).value;
                                    } else {
                                        document.getElementById("kpiach" + row).value = 100;
                                    }
                                }
                            }
                        }

                        if (document.getElementById("ctl00_ContentPlaceHolder1_KPIViewType").selectedIndex == 0) {
                            if (document.getElementById("hkpiach" + row) != null) {
                                if ((parseFloat(document.getElementById("hkpiach" + row).value) != parseFloat(document.getElementById("kpiach" + row).value)) && (document.getElementById("kpiach" + row).value != ""))
                                    document.getElementById("kpiach" + row).style.backgroundColor = '#31FF26';
                                else
                                    document.getElementById("kpiach" + row).style.background = 'transparent';
                            }
                        }

                        if (document.getElementById("kpiach" + row).value == '-') {
                        } else if (document.getElementById("kpiach" + row).value == '') {
                        } else if (document.getElementById("img" + row) == null) {
                        } else {
                            socount += 1;
                            switch (document.getElementById("img" + row).src.split("/")[5]) {
                                case "redup.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                                case "reddown.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                                case "redequal.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                                case "yellowup.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                                case "yellowdown.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                                case "yellowequal.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                                case "greenup.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                                case "greendown.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                                case "greenequal.png":
                                    sovalue += parseFloat(document.getElementById("kpiach" + row).value); break;
                            }
                        }
                        //DEBUG PURPOSE
                        //                        if (document.getElementById("kpiach" + row) != null) {
                        //                            alert(document.getElementById("kpiach" + row).value + "--" + document.getElementById("bobot" + row).value);
                        //                        }
                        if (document.getElementById("kpiach" + row) != null) {
                            //alert(document.getElementById("kpiach" + row).value + "--" + document.getElementById("bobot" + row).value);
                            if (document.getElementById("kpit" + row).value == '') {   //KALAU GA ADA TARGET ITUNG FULL
                                achbobot += parseFloat(document.getElementById("bobot" + row).value);
                                //alert('item:' + achbobot);
                            } else if (document.getElementById("kpiach" + row).value != '') {
                                achbobot += parseFloat(document.getElementById("bobot" + row).value) * parseFloat(document.getElementById("kpiach" + row).value / 100);
                                //alert('item:' + achbobot);
                            } else { }
                        }
                        row += 1;


                        break;
                    }
            }

        }
        //        if (tempso != undefined) {
        //            if (socount == 0)
        //                document.getElementById(tempso).innerHTML = 'Achievement = - ';
        //            else
        //                document.getElementById(tempso).innerHTML = 'Achievement = ' + (Math.round((sovalue / socount * 100)) / 100) + ' %';
        //            if (!isNaN(sovalue / socount)) {
        //                numofpso += sovalue / socount;
        //                numofkpi += 1;
        //            }
        //        }
        tempp = "p" + numofp;
        if (tempp != undefined) {
            //document.getElementById(tempp).innerHTML = Math.round((numofpso / numofkpi) * 100) / 100;
            //            if (!isNaN(numofpso) && numofpso != 0 && numofkpi != 0) {
            //                document.getElementById(tempp).value = Math.round((numofpso / numofkpi) * 100) / 100;
            //                document.getElementById("h" + tempp).value = Math.round((Math.round((numofpso / numofkpi) * 100) / 100 * document.getElementById("t" + tempp).value) / 100 * 100) / 100;
            //                totalach += (Math.round((numofpso / numofkpi) * 100) / 100 * document.getElementById("t" + tempp).value) / 100;
            //                totalperspective += parseFloat(document.getElementById("t" + tempp).value);
            //            } else {
            //                if (document.getElementById(tempp) != null) {
            //                    //document.getElementById(tempp).value = 0;
            //                    document.getElementById(tempp).value = '';
            //                    //document.getElementById("h" + tempp).value = 0;
            //                    document.getElementById("h" + tempp).value = '';
            //                    totalach += 0;
            //                }
            //                if (numofkpi != 0) {
            //                    totalperspective += parseFloat(document.getElementById("t" + tempp).value);
            //                }
            //            }
            if (document.getElementById(tempp) != null) {
                document.getElementById(tempp).value = achbobot;
                document.getElementById("h" + tempp).value = Math.round((achbobot * document.getElementById("t" + tempp).value) / 100 * 100) / 100;
                totalach += (Math.round(achbobot * 100) / 100 * document.getElementById("t" + tempp).value) / 100;
                totalperspective += parseFloat(document.getElementById("t" + tempp).value);
            }
        }
        if (parseFloat(totalperspective) == 0)
            document.getElementById("totalach").innerHTML = "Total Achievement : 0 %";
        else
            document.getElementById("totalach").innerHTML = "Total Achievement : " + Math.round(totalach / totalperspective * 100 * 100) / 100 + " %";
    }
    //---------
}

function ontextboxchange(actid, targetid, achid, oldachid, imgid) {
    var result = (document.getElementById(actid).value / document.getElementById(targetid).value) * 100;
    if (result == Infinity) {
        document.getElementById(achid).value = 0;
    } else if (result == NaN) {
        document.getElementById(achid).value = 0;
    } else {
        document.getElementById(achid).value = Math.round(result * 100) / 100;
    }
    var oldresult = document.getElementById(oldachid).value
    if (result == oldresult) {
        document.getElementById(imgid).src = "images/equal.png";
    } else if (result > oldresult) {
        document.getElementById(imgid).src = "images/up.png";
    } else {
        document.getElementById(imgid).src = "images/down.png";
    }
}

function openmodal(id, level, year, month, kpiname) {
    //alert(screen.width);
    //var returnVal = window.showModalDialog('vdetails.aspx?id=' + id + '&level=' + level + '&year=' + year + '&month=' + month + '&kpiname=' + kpiname + '&time=' + new Date().getTime(), '', 'dialogWidth=' + screen.width + 'px;dialogHeight=' + screen.height + 'px;resizable=yes;help=no;unadorned=yes');
    var returnVal = window.open('vdetails.aspx?id=' + id + '&level=' + level + '&year=' + year + '&month=' + month + '&kpiname=' + kpiname + '&time=' + new Date().getTime(), '', 'width=' + window.screen.availWidth + ',height=' + window.screen.availHeight + ',top=0,left=0,resizable=yes,scrollbars=yes');
    //var returnVal = window.open('vdetails.aspx?id=' + id + '&level=' + level + '&year=' + year + '&month=' + month + '&kpiname=' + kpiname + '&time=' + new Date().getTime(), '', 'fullscreen=yes, scrollbars=yes');
    return false;
}

function openupload(id, level, year, month, kpiname) {
    //var returnVal = window.showModalDialog('tupload.aspx?id=' + id + '&level=' + level + '&year=' + year + '&month=' + month + '&kpiname=' + kpiname, '', 'dialogWidth=300px;dialogHeight=300px;resizable=yes;help=no;unadorned=yes');
    //return false;
    window.open('tupload.aspx?id=' + id + '&level=' + level + '&year=' + year + '&month=' + month + '&kpiname=' + escape(kpiname), '', 'dialogWidth=300px;dialogHeight=300px;resizable=yes;help=no;unadorned=yes');
}

function changeVisibility(visible) {
    if (visible) {
        document.getElementById('btnSaveNew').style.visibility = "visible";
        document.getElementById('btnPrint').style.visibility = "visible";
    } else {
        document.getElementById('btnSaveNew').style.visibility = "hidden";
        document.getElementById('btnPrint').style.visibility = "hidden";
    }
}

function approve(totkpi, totkpinull) {
    var answer = confirm(document.getElementById("totalach").innerHTML + " terdiri dari " + (parseFloat(totkpi) - parseFloat(totkpinull)) + " KPI (Total KPI " + totkpi + ")\n" + "Approve?");
    if (answer) {
        var level = document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').options[document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').selectedIndex].value;
        var year = document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').selectedIndex].value;
        var month = document.getElementById('ctl00_ContentPlaceHolder1_ddlMonth').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlMonth').selectedIndex].value;
        var func = 0;
        var sec = 0;
        var jobsite = 0;
        var dir = 0;
        switch (level) {
            case "1":
                func = document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').selectedIndex].value;
                break;
            case "2":
                {
                    sec = document.getElementById('ctl00_ContentPlaceHolder1_KPISection').options[document.getElementById('ctl00_ContentPlaceHolder1_KPISection').selectedIndex].value;
                    jobsite = document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').selectedIndex].value;
                    break;
                }
            case "3":
                jobsite = document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').selectedIndex].value;
                break;
            case "4":
                dir = document.getElementById('ctl00_ContentPlaceHolder1_KPIDirektorat').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIDirektorat').selectedIndex].value;
                break;
        }

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/ApproveBSC",
            data: "{'level':" + level + ",'year':" + year + ",'month':" + month + ",'dir':" + dir + ",'func':" + func + ",'sec':" + sec + ",'jobsite':" + jobsite + ",'user':" + document.getElementById("ctl00_ContentPlaceHolder1_userid").value + "}",
            dataType: "json",
            success: function(res) {
                if (res.d == "0") {
                    document.getElementById("ctl00_ContentPlaceHolder1_btnSearch").click();
                } else {
                    alert(res.d);
                }
            },
            error: function(err) {
                alert("error: " + err.responseText);
            }
        });
    } else {
        document.getElementById("ctl00_ContentPlaceHolder1_btnApprove").disabled = "";
    }
}
function IsNumeric(n) { return !isNaN(parseFloat(n)) && isFinite(n); }
function saveapprove() {
    document.getElementById("ctl00_ContentPlaceHolder1_btnApprove").disabled = "DISABLED";
    var tbl = document.getElementById('BSCTABLE');
    var totkpi = 0;
    var totkpinull = 0;
    if (tbl != null) {
        var row = 1;
        MyArray = new Array();
        for (var i = 1; i < tbl.rows.length; i++) {
            switch (tbl.rows[i].cells.length) {
                case 1:
                    {
                        break;
                    }
                case 11:
                    {
                        //                             if (document.getElementById("kpit" + row).value == '-') {
                        //                                 document.getElementById("ctl00_ContentPlaceHolder1_btnApprove").disabled = "";
                        //                                 alert('Terdapat target yang belum diisi, Tidak Bisa melakukan approval');
                        //                                 return;
                        //                             }
                        if (document.getElementById("kpit" + row).value != '') {
                            totkpi = totkpi + 1;
                        }
                        if (document.getElementById("kpia" + row).value == '' && IsNumeric(stripNonNumeric(document.getElementById("kpit" + row).value))) {
                            totkpinull = totkpinull + 1;
                        }
                        if (document.getElementById("kpit" + row).value == '') {
                            MyArray[MyArray.length] = new bsc(
                                       '',
                                       0,
                                       document.getElementById("kpiach" + row).value,
                                       document.getElementById("kpiid" + row).value,
                                       document.getElementById("year" + row).value,
                                       document.getElementById("month" + row).value
                                        )
                        } else if (document.getElementById("kpit" + row).value != '-' && document.getElementById("kpia" + row).value != '') {
                            MyArray[MyArray.length] = new bsc(
                                       document.getElementById("kpit" + row).value,
                                       document.getElementById("kpia" + row).value,
                                       document.getElementById("kpiach" + row).value,
                                       document.getElementById("kpiid" + row).value,
                                       document.getElementById("year" + row).value,
                                       document.getElementById("month" + row).value
                                       )
                        } else if (document.getElementById("kpia" + row).value == '' && document.getElementById("kpit" + row).value != '-') {
                            MyArray[MyArray.length] = new bsc(
                                           document.getElementById("kpit" + row).value,
                                           -88.314098314,
                                           document.getElementById("kpiach" + row).value,
                                           document.getElementById("kpiid" + row).value,
                                           document.getElementById("year" + row).value,
                                           document.getElementById("month" + row).value
                                        )
                        }
                        row += 1;
                        break;
                    }
                case 10:
                    {
                        //                             if (document.getElementById("kpit" + row).value == '-') {
                        //                                 document.getElementById("ctl00_ContentPlaceHolder1_btnApprove").disabled = "";
                        //                                 alert('Terdapat target yang belum diisi, Tidak Bisa melakukan approval');
                        //                                 return;
                        //                             }
                        if (document.getElementById("kpit" + row).value != '') {
                            totkpi = totkpi + 1;
                        }
                        if (document.getElementById("kpia" + row).value == '' && IsNumeric(stripNonNumeric(document.getElementById("kpit" + row).value))) {
                            totkpinull = totkpinull + 1;
                        }
                        if (document.getElementById("kpit" + row).value == '') {
                            MyArray[MyArray.length] = new bsc(
                                       '',
                                       0,
                                       document.getElementById("kpiach" + row).value,
                                       document.getElementById("kpiid" + row).value,
                                       document.getElementById("year" + row).value,
                                       document.getElementById("month" + row).value
                                        )
                        } else if (document.getElementById("kpit" + row).value != '-' && document.getElementById("kpia" + row).value != '') {
                            MyArray[MyArray.length] = new bsc(
                                       document.getElementById("kpit" + row).value,
                                       document.getElementById("kpia" + row).value,
                                       document.getElementById("kpiach" + row).value,
                                       document.getElementById("kpiid" + row).value,
                                       document.getElementById("year" + row).value,
                                       document.getElementById("month" + row).value
                                       )
                        } else if (document.getElementById("kpia" + row).value == '' && document.getElementById("kpit" + row).value != '-') {
                            MyArray[MyArray.length] = new bsc(
                                           document.getElementById("kpit" + row).value,
                                           -88.314098314,
                                           document.getElementById("kpiach" + row).value,
                                           document.getElementById("kpiid" + row).value,
                                           document.getElementById("year" + row).value,
                                           document.getElementById("month" + row).value
                                        )
                        }
                        row += 1;
                        break;
                    }
                case 9:
                    {
                        //                             if (document.getElementById("kpit" + row).value == '-') {
                        //                                 document.getElementById("ctl00_ContentPlaceHolder1_btnApprove").disabled = "";
                        //                                 alert('Terdapat target yang belum diisi, Tidak Bisa melakukan approval');
                        //                                 return;
                        //                             }
                        if (document.getElementById("kpit" + row).value != '') {
                            totkpi = totkpi + 1;
                        }
                        if (document.getElementById("kpia" + row).value == '' && IsNumeric(stripNonNumeric(document.getElementById("kpit" + row).value))) {
                            totkpinull = totkpinull + 1;
                        }
                        if (document.getElementById("kpit" + row).value == '') {
                            MyArray[MyArray.length] = new bsc(
                                       '',
                                       0,
                                       document.getElementById("kpiach" + row).value,
                                       document.getElementById("kpiid" + row).value,
                                       document.getElementById("year" + row).value,
                                       document.getElementById("month" + row).value
                                        )
                        } else if (document.getElementById("kpit" + row).value != '-' && document.getElementById("kpia" + row).value != '') {
                            MyArray[MyArray.length] = new bsc(
                                       document.getElementById("kpit" + row).value,
                                       document.getElementById("kpia" + row).value,
                                       document.getElementById("kpiach" + row).value,
                                       document.getElementById("kpiid" + row).value,
                                       document.getElementById("year" + row).value,
                                       document.getElementById("month" + row).value
                                       )
                        } else if (document.getElementById("kpia" + row).value == '' && document.getElementById("kpit" + row).value != '-') {
                            MyArray[MyArray.length] = new bsc(
                                           document.getElementById("kpit" + row).value,
                                           -88.314098314,
                                           document.getElementById("kpiach" + row).value,
                                           document.getElementById("kpiid" + row).value,
                                           document.getElementById("year" + row).value,
                                           document.getElementById("month" + row).value
                                        )
                        }
                        row += 1;
                        break;
                    }
            }
        }
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/SaveBSC",
            data: "{'mybsc':" + JSON.stringify(MyArray) + ",'userid':" + document.getElementById("ctl00_ContentPlaceHolder1_userid").value + ",'userip':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_userip").value) + "}",
            dataType: "json",
            success: function(res) {
                if (res.d == "SUKSES") {
                    approve(totkpi, totkpinull);
                } else {
                    alert(res.d);
                    document.getElementById("ctl00_ContentPlaceHolder1_lblMessage").innerText = res.d;
                }
            },
            error: function(err) {
                alert("error: " + err.responseText);
            }
        });
    }
}

function save() {
    var tbl = document.getElementById('BSCTABLE');
    if (tbl != null) {
        var row = 1;
        MyArray = new Array();
        for (var i = 1; i < tbl.rows.length; i++) {
            switch (tbl.rows[i].cells.length) {
                case 1:
                    {
                        break;
                    }
                case 11:
                    {
                        //                                  if (!IsNumeric(document.getElementById("kpia" + row).value) && IsNumeric(document.getElementById("kpit" + row).value)) {
                        //                                      alert('Terdapat Data Actual yang belum diisi, Tidak Bisa melakukan approval');
                        //                                      return;
                        //                                  }
                        if (document.getElementById("kpit" + row).value == '') {
                            MyArray[MyArray.length] = new bsc(
                                       '',
                                       0,
                                       document.getElementById("kpiach" + row).value,
                                       document.getElementById("kpiid" + row).value,
                                       document.getElementById("year" + row).value,
                                       document.getElementById("month" + row).value
                                        )
                        } else if (document.getElementById("kpit" + row).value != '-' && document.getElementById("kpia" + row).value != '') {
                            MyArray[MyArray.length] = new bsc(
                                       document.getElementById("kpit" + row).value,
                                       document.getElementById("kpia" + row).value,
                                       document.getElementById("kpiach" + row).value,
                                       document.getElementById("kpiid" + row).value,
                                       document.getElementById("year" + row).value,
                                       document.getElementById("month" + row).value
                                       )
                        } else if (document.getElementById("kpia" + row).value == '' && document.getElementById("kpit" + row).value != '-') {
                            MyArray[MyArray.length] = new bsc(
                                           document.getElementById("kpit" + row).value,
                                           -88.314098314,
                                           document.getElementById("kpiach" + row).value,
                                           document.getElementById("kpiid" + row).value,
                                           document.getElementById("year" + row).value,
                                           document.getElementById("month" + row).value
                                        )
                        }
                        row += 1;
                        break;
                    }
                case 10:
                    {
                        //                                  if (!IsNumeric(document.getElementById("kpia" + row).value) && IsNumeric(document.getElementById("kpit" + row).value)) {
                        //                                      alert('Terdapat Data Actual yang belum diisi, Tidak Bisa melakukan approval');
                        //                                      return;
                        //                                  }
                        if (document.getElementById("kpit" + row).value == '') {
                            MyArray[MyArray.length] = new bsc(
                                       '',
                                       0,
                                       document.getElementById("kpiach" + row).value,
                                       document.getElementById("kpiid" + row).value,
                                       document.getElementById("year" + row).value,
                                       document.getElementById("month" + row).value
                                        )
                        } else if (document.getElementById("kpit" + row).value != '-' && document.getElementById("kpia" + row).value != '') {
                            MyArray[MyArray.length] = new bsc(
                                       document.getElementById("kpit" + row).value,
                                       document.getElementById("kpia" + row).value,
                                       document.getElementById("kpiach" + row).value,
                                       document.getElementById("kpiid" + row).value,
                                       document.getElementById("year" + row).value,
                                       document.getElementById("month" + row).value
                                       )
                        } else if (document.getElementById("kpia" + row).value == '' && document.getElementById("kpit" + row).value != '-') {
                            MyArray[MyArray.length] = new bsc(
                                           document.getElementById("kpit" + row).value,
                                           -88.314098314,
                                           document.getElementById("kpiach" + row).value,
                                           document.getElementById("kpiid" + row).value,
                                           document.getElementById("year" + row).value,
                                           document.getElementById("month" + row).value
                                        )
                        }
                        row += 1;
                        break;
                    }
                case 9:
                    {
                        //                                  if (!IsNumeric(document.getElementById("kpia" + row).value) && IsNumeric(document.getElementById("kpit" + row).value)) {
                        //                                      alert('Terdapat Data Actual yang belum diisi, Tidak Bisa melakukan approval');
                        //                                      return;
                        //                                  }
                        if (document.getElementById("kpit" + row).value == '') {
                            MyArray[MyArray.length] = new bsc(
                                       '',
                                       0,
                                       document.getElementById("kpiach" + row).value,
                                       document.getElementById("kpiid" + row).value,
                                       document.getElementById("year" + row).value,
                                       document.getElementById("month" + row).value
                                        )
                        } else if (document.getElementById("kpit" + row).value != '-' && document.getElementById("kpia" + row).value != '') {
                            MyArray[MyArray.length] = new bsc(
                                       document.getElementById("kpit" + row).value,
                                       document.getElementById("kpia" + row).value,
                                       document.getElementById("kpiach" + row).value,
                                       document.getElementById("kpiid" + row).value,
                                       document.getElementById("year" + row).value,
                                       document.getElementById("month" + row).value
                                       )
                        } else if (document.getElementById("kpia" + row).value == '' && document.getElementById("kpit" + row).value != '-') {
                            MyArray[MyArray.length] = new bsc(
                                           document.getElementById("kpit" + row).value,
                                           -88.314098314,
                                           document.getElementById("kpiach" + row).value,
                                           document.getElementById("kpiid" + row).value,
                                           document.getElementById("year" + row).value,
                                           document.getElementById("month" + row).value
                                        )
                        }
                        row += 1;
                        break;
                    }
            }
        }
        //alert(JSON.stringify(MyArray));
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/SaveBSC",
            data: "{'mybsc':" + JSON.stringify(MyArray) + ",'userid':" + document.getElementById("ctl00_ContentPlaceHolder1_userid").value + ",'userip':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_userip").value) + "}",
            dataType: "json",
            success: function(res) {
                if (res.d == "SUKSES") {
                    var func; var jobsite;
                    if (document.getElementById("ctl00_ContentPlaceHolder1_KPILevel").value == "1") {
                        func = document.getElementById("ctl00_ContentPlaceHolder1_KPIFunc").value;
                        jobsite = 'x';
                    }
                    else if (document.getElementById("ctl00_ContentPlaceHolder1_KPILevel").value == "2") {
                        func = document.getElementById("ctl00_ContentPlaceHolder1_KPISection").value;
                        jobsite = document.getElementById("ctl00_ContentPlaceHolder1_KPIJobsite").value;
                    } else if (document.getElementById("ctl00_ContentPlaceHolder1_KPILevel").value == "3") {
                        func = 99;
                        jobsite = document.getElementById("ctl00_ContentPlaceHolder1_KPIJobsite").value;
                    } else {
                        func = 99; jobsite = 'x'
                    }

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "WS/BSCService.asmx/Recount",
                        data: "{'month':" + document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value + ",'year':" + document.getElementById("ctl00_ContentPlaceHolder1_ddlYear").value + ",'level':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_KPILevel").value) + ",'func':" + JSON.stringify(func) + ",'jobsite':" + JSON.stringify(jobsite) + "}",
                        dataType: "json",
                        success: function(res) {
                        },
                        error: function(err) {
                            alert("error: " + err.responseText);
                        }
                    });
                    document.getElementById("ctl00_ContentPlaceHolder1_btnSearch").click();
                    document.getElementById("ctl00_ContentPlaceHolder1_lblMessage").innerText = "Save Berhasil";
                } else {
                    alert(res.d);
                    document.getElementById("ctl00_ContentPlaceHolder1_lblMessage").innerText = res.d;
                }
            },
            error: function(err) {
                alert("error: " + err.responseText);
            }
        });
    }
}


function bsc(target, actual, achievement, kpiid, year, month) {
    this.target = target; this.actual = actual; this.achievement = achievement; this.kpiid = kpiid;
    this.year = year; this.month = month;
}
//---------

function fncInputNumericValuesOnly() { if (!(event.keyCode == 45 || event.keyCode == 46 || event.keyCode == 48 || event.keyCode == 49 || event.keyCode == 50 || event.keyCode == 51 || event.keyCode == 52 || event.keyCode == 53 || event.keyCode == 54 || event.keyCode == 55 || event.keyCode == 56 || event.keyCode == 57)) { event.returnValue = false; } }
function fncInputNumericValuesOnlyNoMinus() { if (!(event.keyCode == 46 || event.keyCode == 48 || event.keyCode == 49 || event.keyCode == 50 || event.keyCode == 51 || event.keyCode == 52 || event.keyCode == 53 || event.keyCode == 54 || event.keyCode == 55 || event.keyCode == 56 || event.keyCode == 57)) { event.returnValue = false; } }

function viewrpt() {
    window.open('popuptrans.aspx?p=' + document.getElementById("ctl00_ContentPlaceHolder1_param").value + '&a=' + document.getElementById("totalach").innerHTML, '', '');
}

function unapprove() {
    var answer = confirm("Un-Approve BSC ?");
    if (answer) {
        var level = document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').options[document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').selectedIndex].value;
        var year = document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').selectedIndex].value;
        var month = document.getElementById('ctl00_ContentPlaceHolder1_ddlMonth').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlMonth').selectedIndex].value;
        var func = 0;
        var sec = 0;
        var jobsite = 0;
        var dir = 0;
        switch (level) {
            case "1":
                func = document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').selectedIndex].value;
                break;
            case "2":
                {
                    sec = document.getElementById('ctl00_ContentPlaceHolder1_KPISection').options[document.getElementById('ctl00_ContentPlaceHolder1_KPISection').selectedIndex].value;
                    jobsite = document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').selectedIndex].value;
                    break;
                }
            case "3":
                jobsite = document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').selectedIndex].value;
                break;
            case "4":
                dir = document.getElementById('ctl00_ContentPlaceHolder1_KPIDirektorat').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIDirektorat').selectedIndex].value;
                break;
        }

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/UnApproveBSC",
            data: "{'level':" + level + ",'year':" + year + ",'month':" + month + ",'dir':" + dir + ",'func':" + func + ",'sec':" + sec + ",'jobsite':" + jobsite + ",'user':" + document.getElementById("ctl00_ContentPlaceHolder1_userid").value + "}",
            dataType: "json",
            success: function(res) {
                if (res.d == "0") {
                    document.getElementById("ctl00_ContentPlaceHolder1_btnSearch").click();
                } else {
                    alert(res.d);
                }
            },
            error: function(err) {
                alert("error: " + err.responseText);
            }
        });
    } else {
        document.getElementById("ctl00_ContentPlaceHolder1_btnApprove").disabled = "";
    }
}

function stripNonNumeric(str) {
    str += '';
    var rgx = /^\d|\.|-$/;
    var out = '';
    for (var i = 0; i < str.length; i++) {
        if (rgx.test(str.charAt(i))) {
            if (!((str.charAt(i) == '.' && out.indexOf('.') != -1) ||
             (str.charAt(i) == '-' && out.length != 0))) {
                out += str.charAt(i);
            }
        }
    }
    return out;
}

function dokonsolidasi() {
    var answer = confirm("Lakukan Konsolidasi Dengan Data Child?");
    if (answer) {
        var level = document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').options[document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').selectedIndex].value;
        var year = document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').selectedIndex].value;
        var month = document.getElementById('ctl00_ContentPlaceHolder1_ddlMonth').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlMonth').selectedIndex].value;
        var func = 0;
        var sec = 0;
        var jobsite = 0;
        switch (level) {
            case "1":
                func = document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').selectedIndex].value;
                break;
            case "2":
                {
                    sec = document.getElementById('ctl00_ContentPlaceHolder1_KPISection').options[document.getElementById('ctl00_ContentPlaceHolder1_KPISection').selectedIndex].value;
                    jobsite = document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').selectedIndex].value;
                    break;
                }
            case "3":
                jobsite = document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').selectedIndex].value;
                break;
        }

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/DoRecountBSC",
            data: "{'level':" + level + ",'year':" + year + ",'month':" + month + ",'func':" + func + ",'sec':" + sec + ",'jobsite':" + jobsite + ",'user':" + document.getElementById("ctl00_ContentPlaceHolder1_userid").value + "}",
            dataType: "json",
            success: function(res) {
                if (res.d == "0") {
                    alert("Konsolidasi Berhasil");
                    document.getElementById("ctl00_ContentPlaceHolder1_btnSearch").click();
                } else {
                    alert(res.d);
                }
            },
            error: function(err) {
                alert("error: " + err.responseText);
            }
        });
    } else {

    }
}

//       function viewchange() {
//           if (document.getElementById("ctl00_ContentPlaceHolder1_KPIViewType").selectedIndex == 1)
//               document.getElementById("ctl00_ContentPlaceHolder1_btnCreate").style.display = "none";
//           else
//               if (document.getElementById("ctl00_ContentPlaceHolder1_btnCreate") != null)
//                   document.getElementById("ctl00_ContentPlaceHolder1_btnCreate").style.display = "";
//       }
