﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="activityplan.aspx.vb"
   Inherits="EXCELLENT.activityplan" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
   <title></title>

   <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>

   <script src="../Scripts/json2.js" type="text/javascript"></script>

   <script src="scripts/table_floating_header.js" type="text/javascript"></script>

   <link rel="stylesheet" href="../css/calendar-c.css" />
   <style type="text/css">
      body
      {
         font-family: Calibri;
      }
      #divprogress
      {
         z-index: 1000;
         position: absolute;
         top: 0;
         bottom: 0;
         left: 0;
         width: 100%;
         background: #000;
         opacity: 0.55;
         -moz-opacity: 0.55;
         filter: alpha(opacity=55);
         visibility: hidden;
      }
      table.act
      {
         font-family: Calibri;
         border-width: 1px;
         border-spacing: 0px;
         border-style: solid;
         border-color: #C0C0C0;
         border-collapse: collapse;
         background-color: white;
      }      
      table.act thead
      {
         border-width: 1px;
         padding: 1px;
         border-style: groove;
         border-color: #ebebeb;
      }
      table.act td
      {
         border-width: 1px;
         padding: 1px;
         border-style: groove;
         border-color: #ebebeb;
      }
      input.plain
      {
         border: 1px solid #C0C0C0;
         font-family: Calibri;
      }
      input.plains
      {
         border: 0;
         background-color: Transparent;
         font-family: Calibri;
      }
      .plain:hover
      {
         background-color: #FFFFA8;
      }
      select
      {
         border: 1px solid #C0C0C0;
      }
      textarea.plain
      {
         border: 1px solid #C0C0C0;
         font-family: Calibri;
      }
      ul.dropdown *.dir2
      {
         padding-right: 20px;
         background-image: url("/memo/css/dropdown/themes/default/images/nav-arrow-down.png");
         background-position: 100% 50%;
         background-repeat: no-repeat;
         background-color: #FFA500;
         font-family: Calibri;
      }
      select.plains
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         border: 1px solid #C0C0C0;
         font-family: Calibri;
      }
      td.styleHeader
      {
         background-color: #00CC00;
         color: black;
         font-weight: bold;
         font-size: medium;
      }
      table.tes
      {
         font-family: Calibri;
         border-width: 1px;
         border-spacing: 0px;
         border-style: solid;
         border-color: #C0C0C0;
         border-collapse: collapse;
         background-color: white;
         width:100%;
      }
      table.tes tbody
      {
         border-width: 0px;
         padding: 0px;
      }
      table.tes td
      {
         border-width: 1px;
         padding: 0px;
         border-style: groove;
         border-color: #ebebeb;
      }
   </style>

   <script src="../Scripts/json2.js" type="text/javascript"></script>

   <script src="../Scripts/calendar_us.js" type="text/javascript"></script>

   <script type="text/javascript">

      var TABLE_NAME = 'activity';
      var g_row = 1;
      $(document).ready(function() {
         document.getElementById("divprogress").style.visibility = 'hidden';
      });
      function limitText(limitField, limitNum) {
         if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
         }
      }
      function imposeMaxLength(Event, Object, MaxLen) {
         //|| (Event.keyCode == 8 || Event.keyCode == 46 || (Event.keyCode >= 35 && Event.keyCode <= 40))
         try {
            if (Object.value.length < MaxLen)
               return true;
            else
               return false;
         } catch (err) {
            alert(err.description);
         }
      }

      function searchkary(nik, nama) {
         var returnVal = window.showModalDialog('SearchEmployeeBSC.aspx', '', 'dialogWidth=450px;dialogHeight=300px;resizable=yes;help=no;unadorned=yes');
         if (returnVal != undefined) {
            document.getElementById(nik).value = returnVal.split(';')[0];
            document.getElementById(nama).value = returnVal.split(';')[1];
         }
      }
      function searchkpi(kpiid, kpiname, skpiid) {
         var tipe;
         switch (document.getElementById("ctl00_ContentPlaceHolder1_Grup").value) {
            case "7":
               {
                  tipe = 2; break;
               }
            case "A":
               {
                  tipe = 2; break;
               }
            case "1":
               {
                  tipe = 1; break;
               }
            case "2":
               {
                  tipe = 2; break;
               }
            case "8":
               {
                  tipe = 1; break;
               }

         }
         var yearObj = document.getElementById('ctl00_ContentPlaceHolder1_ddlYear');
         var yearIndex = yearObj.selectedIndex;
         var returnVal = window.showModalDialog('PopUpSearch.aspx?y=' + yearObj.options[yearIndex].value + '&type=' + tipe + '&wtr=1', '', 'dialogWidth=300px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         if (returnVal != undefined) {
            document.getElementById(kpiid).value = returnVal.split("|")[0];
            document.getElementById(kpiname).value = returnVal.split("|")[1];
            if (document.getElementById(skpiid).value == 'E')
               document.getElementById(skpiid).value == 'U';
         }
      }

      function addProject(what, inserto, myid, myactivity, mystate) {
         var tbl = document.getElementById(TABLE_NAME);
         var nextRow = inserto;
         num = tbl.tBodies[0].rows.length;
         var row = tbl.tBodies[0].insertRow(num);
         row.setAttribute('id', 'trp' + g_row);
         row.setAttribute('style', 'vertical-align: top;');
         var cell0 = row.insertCell(0);
         cell0.setAttribute('style', 'visibility:hidden');

         var cell1 = row.insertCell(1);
         cell1.setAttribute('style', 'background-color:#A9F2A9');
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'hstate' + g_row);
         txtInp.setAttribute('id', 'hstate' + g_row);
         txtInp.setAttribute('value', 'NEW');
         if (mystate != undefined) {
            txtInp.setAttribute('value', mystate);
         }
         cell1.appendChild(txtInp);

         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'row' + g_row);
         txtInp.setAttribute('id', 'row' + g_row);
         txtInp.setAttribute('size', '0');
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('value', g_row);
         cell1.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'nox' + g_row);
         txtInp.setAttribute('id', 'nox' + g_row);
         txtInp.setAttribute('size', '0');
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('class', 'plain');
         if (myid != undefined) {
            txtInp.setAttribute('value', myid);
         }
         cell1.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'numbering' + g_row);
         txtInp.setAttribute('id', 'numbering' + g_row);
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('style', 'font-size:22px;text-align:left;width:100%');
         txtInp.setAttribute('class', 'plains');
         cell1.appendChild(txtInp);

         var cell2 = row.insertCell(2);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'jenis' + g_row);
         txtInp.setAttribute('id', 'jenis' + g_row);
         txtInp.setAttribute('size', '10');
         txtInp.setAttribute('value', 'PROJECT');
         cell2.appendChild(txtInp);
         cell2.setAttribute('style', 'display:none');

         var cell3 = row.insertCell(3);
         cell3.setAttribute('align', 'left');
         cell3.setAttribute('colspan', '5');
         cell3.setAttribute('style', 'vertical-align:bottom;text-align:left;');
         var txtInp = document.createElement('textarea');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('style', 'width:95%;background-color:#A9F2A9;font-size:20px;overflow:hidden');
         txtInp.setAttribute('name', 'activity' + g_row);
         txtInp.setAttribute('id', 'activity' + g_row);
         txtInp.setAttribute('cols', '20');
         txtInp.setAttribute('rows', '2');
         //txtInp.setAttribute('onkeypress', 'return imposeMaxLength(event,this, 500)')
         //txtInp.setAttribute('onkeydown', 'limitText(this,500)');
         if (myactivity != undefined) {
            var txt = document.createTextNode(myactivity);
            txtInp.appendChild(txt);
         }
         cell3.appendChild(txtInp);
         cell3.innerHTML = cell3.innerHTML + '<table border="0" align="right"><tr><td style="border:0;"><img title="Add New Project" style="cursor:hand" onclick=addProject(document.getElementById("trp' + g_row + '"),document.getElementById(\'row' + g_row + '\').value); src="../images/green.png"/><span style="font-size:9px">Prj</span></td></tr><tr><td style="border:0;"><img title="Add New Activity" width="15px" height="15px" style="cursor:hand" onclick=addRow(document.getElementById("trp' + g_row + '"),document.getElementById(\'row' + g_row + '\').value); src="../images/blue.png"/><span style="font-size:9px">Act</span></td></tr></table>'

         var cell9 = row.insertCell(4);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'checkbox');
         txtInp.setAttribute('name', 'dela' + g_row);
         txtInp.setAttribute('id', 'dela' + g_row);
         txtInp.setAttribute('onclick', "checkthis(this, 'PROJECT', document.getElementById('row" + g_row + "').value)");
         txtInp.setAttribute('style', 'visibility:hidden');
         cell9.appendChild(txtInp);
         cell9.innerHTML = cell9.innerHTML + '<center><img title="Delete Row" align="left" id="anchor1" class="tcalIcon" onclick="checkthis(this, \'PROJECT\', document.getElementById(\'row' + g_row + '\').value)" src="../images/delete.gif"/></center>';


         g_row += 1;
         reorderRows(tbl);
      }

      function addRow(what, inserto, myid, myactivity, mykpiid, mykpiname, mystate, mypicnik, mypicname, mystart, myend, mydeliverable, mytotalsecure) {
         if (what != '')
            inserto = what.rowIndex;
         var tbl = document.getElementById(TABLE_NAME);
         var nextRow = inserto;
         var num = nextRow;

         if (num != 0) {
            for (var i = num; i < tbl.tBodies[0].rows.length; i++) {
               if (tbl.tBodies[0].rows[i].cells[2].getElementsByTagName('input')[0].value == 'TASK') {
               } else {
                  num = i;
                  break;
               }
            }
         }
         num = i;
         if (num == tbl.tBodies[0].rows.length)
            num = tbl.tBodies[0].rows.length;
         var row = tbl.tBodies[0].insertRow(num);

         row.setAttribute('id', 'tr' + g_row);
         row.setAttribute('style', 'vertical-align: top;');
         var cell0 = row.insertCell(0);
         cell0.setAttribute('style', 'visibility:hidden');

         var cell1 = row.insertCell(1);

         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'hstate' + g_row);
         txtInp.setAttribute('id', 'hstate' + g_row);
         txtInp.setAttribute('value', 'NEW');
         if (mystate != undefined) {
            txtInp.setAttribute('value', mystate);
         }
         cell1.appendChild(txtInp);

         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'row' + g_row);
         txtInp.setAttribute('id', 'row' + g_row);
         txtInp.setAttribute('size', '0');
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('value', g_row);
         cell1.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'nox' + g_row);
         txtInp.setAttribute('id', 'nox' + g_row);
         txtInp.setAttribute('size', '0');
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('class', 'plain');
         if (myid != undefined) {
            txtInp.setAttribute('value', myid);
         }
         cell1.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'numbering' + g_row);
         txtInp.setAttribute('id', 'numbering' + g_row);
         txtInp.setAttribute('style', 'width:100%');
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('class', 'plains');
         cell1.appendChild(txtInp);

         var cell2 = row.insertCell(2);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'jenis' + g_row);
         txtInp.setAttribute('id', 'jenis' + g_row);
         txtInp.setAttribute('size', '10');
         txtInp.setAttribute('value', 'PROGRAM');
         cell2.appendChild(txtInp);
         cell2.setAttribute('style', 'display:none');

         var cell3 = row.insertCell(3);
         cell3.setAttribute('align', 'left');
         cell3.setAttribute('style', 'vertical-align:top;text-align:left;');
         var txtInp = document.createElement('textarea');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('style', 'width:85%;background-color:#93E5FF;');
         txtInp.setAttribute('name', 'activity' + g_row);
         txtInp.setAttribute('id', 'activity' + g_row);
         txtInp.setAttribute('cols', '20');
         txtInp.setAttribute('rows', '3');
         //txtInp.setAttribute('onkeypress', 'return imposeMaxLength(event,this, 500)')
         //txtInp.setAttribute('onkeydown', 'limitText(this,500)');
         if (myactivity != undefined) {
            var txt = document.createTextNode(myactivity);
            txtInp.appendChild(txt);
         }
         cell3.appendChild(txtInp);
         cell3.innerHTML = cell3.innerHTML + '<table border="0" align="left"><tr><td style="border:0;align:left"><img title="Add New Activity" style="cursor:hand" onclick=addRow(document.getElementById("tr' + g_row + '"),document.getElementById(\'row' + g_row + '\').value); src="../images/blue.png"/><span style="font-size:9px">Act</span></td></tr><tr><td style="border:0;"><img title="Add New Task" width="15px" height="15px" style="cursor:hand" onclick=addtask(document.getElementById("tr' + g_row + '"),document.getElementById(\'row' + g_row + '\').value); src="../images/white.png"/><span style="font-size:9px">Tsk</span></td></tr></table>'

         var cell4 = row.insertCell(4);
         var txtInp = document.createElement('textarea');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('name', 'd' + g_row);
         txtInp.setAttribute('id', 'd' + g_row);
         txtInp.setAttribute('style', 'width:100%;');
         txtInp.setAttribute('rows', '3');
         txtInp.setAttribute('cols', '20');
         txtInp.setAttribute('onkeypress', 'return imposeMaxLength(event,this, 300)')
         //txtInp.setAttribute('onkeydown', 'limitText(this,300)');
         if (mydeliverable != undefined) {
            var txt = document.createTextNode(mydeliverable);
            txtInp.appendChild(txt);
         }
         cell4.appendChild(txtInp);

         var cell5 = row.insertCell(5);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'hkpiid' + g_row);
         txtInp.setAttribute('id', 'hkpiid' + g_row);
         if (mykpiid != undefined) {
            txtInp.setAttribute('value', mykpiid);
         }
         cell5.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'skpiid' + g_row);
         txtInp.setAttribute('id', 'skpiid' + g_row);
         if (mykpiid != undefined) {
            txtInp.setAttribute('value', 'E');
         } else {
            txtInp.setAttribute('value', 'N');
         }
         cell5.appendChild(txtInp);
         cell5.innerHTML += "</td><td style='border-top:1;border-right:0;border-style:collapse;border-color:#C0C0C0'>"
         var txtInp = document.createElement('textarea');
         txtInp.setAttribute('rows', '3');
         txtInp.setAttribute('cols', '20');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('name', 'kpiname' + g_row);
         txtInp.setAttribute('id', 'kpiname' + g_row);
         txtInp.setAttribute('READONLY', 'READONLY')
         txtInp.setAttribute('style', 'width:100%;overflow:hidden;border:0')
         if (mykpiname != undefined) {
            var txt = document.createTextNode(mykpiname);
            txtInp.appendChild(txt);
         }
         cell5.appendChild(txtInp);
         cell5.innerHTML += "</td>"
         cell5.innerHTML = "<table class='tes'><tr><td style='border-left:0;'>" + cell5.innerHTML + '<td style="vertical-align:top;border-top:1;border-left:0;border-color:#C0C0C0"><img title="Cari KPI" style="cursor:hand" onclick=searchkpi("hkpiid' + g_row + '","kpiname' + g_row + '","skpiid' + g_row + '") src="../images/search.gif" align="right"/></td></tr></table>';

         var cell8 = row.insertCell(6);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'nikpic' + g_row);
         txtInp.setAttribute('id', 'nikpic' + g_row);
         if (mypicnik != undefined) {
            txtInp.setAttribute('value', mypicnik);
         }
         cell8.appendChild(txtInp);
         cell8.innerHTML += "</td><td style='border-top:1;border-right:0;border-style:collapse;border-color:#C0C0C0'>"
         var txtInp = document.createElement('textarea');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('align', 'left');
         txtInp.setAttribute('rows', '3');
         txtInp.setAttribute('cols', '20');
         txtInp.setAttribute('name', 'pic' + g_row);
         txtInp.setAttribute('id', 'pic' + g_row);
         txtInp.setAttribute('READONLY', 'READONLY')
         txtInp.setAttribute('style', 'width:100%;overflow:hidden;align:left;border:0')
         if (mypicname != undefined) {
            var txt = document.createTextNode(mypicname);
            txtInp.appendChild(txt);
         }
         cell8.appendChild(txtInp);
         cell8.innerHTML += "</td>"
         cell8.innerHTML = "<table class='tes'><tr><td style='border-left:0;'>" + cell8.innerHTML + '<td style="vertical-align:top;border-top:1;border-left:0;border-color:#C0C0C0"><img title="Cari Karyawan" style="cursor:hand" onclick=searchkary("nikpic' + g_row + '","pic' + g_row + '") src="../images/search.gif"/></td></tr></table>';
         
         var cell6 = row.insertCell(7);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('name', 'start' + g_row);
         txtInp.setAttribute('id', 'start' + g_row);
         txtInp.setAttribute('size', '7');
         txtInp.setAttribute('READONLY', 'READONLY');
         if (mystart != undefined) {
            txtInp.setAttribute('value', mystart);
         }
         cell6.innerHTML = cell6.innerHTML + '<span style="font-size:10px">ST</span>';
         cell6.appendChild(txtInp);
         cell6.innerHTML = cell6.innerHTML + '<img title="Open Calendar" src="../images/cal.gif" class="tcalIcon" onclick="A_TCALS[\'myCalstartID' + g_row + '\'].f_toggle()" id="tcalico_myCalstartID' + g_row + '" />';
         new tcal({
            'formname': 'aspnetForm',
            'controlname': 'start' + g_row,
            'id': 'myCalstartID' + g_row
         });
         cell6.innerHTML = cell6.innerHTML + '<br><span style="font-size:10px">ED</span>';
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('name', 'end' + g_row);
         txtInp.setAttribute('id', 'end' + g_row);
         txtInp.setAttribute('size', '7');
         txtInp.setAttribute('READONLY', 'READONLY');
         if (myend != undefined) {
            txtInp.setAttribute('value', myend);
         }
         cell6.appendChild(txtInp);
         cell6.innerHTML = cell6.innerHTML + '<img title="Open Calendar" src="../images/cal.gif" class="tcalIcon" onclick="A_TCALS[\'myCalendID' + g_row + '\'].f_toggle()" id="tcalico_myCalendID' + g_row + '"/>';
         new tcal({
            'formname': 'aspnetForm',
            'controlname': 'end' + g_row,
            'id': 'myCalendID' + g_row
         });

         var cell9 = row.insertCell(8);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'checkbox');
         txtInp.setAttribute('name', 'dela' + g_row);
         txtInp.setAttribute('id', 'dela' + g_row);
         txtInp.setAttribute('onclick', "checkthis(this, 'PROGRAM', document.getElementById('row" + g_row + "').value)");
         txtInp.setAttribute('style', 'visibility:hidden');
         cell9.appendChild(txtInp);
         cell9.innerHTML = cell9.innerHTML + '<center><img title="Delete Row" align="left" class="tcalIcon" onclick="checkthis(this, \'PROGRAM\', document.getElementById(\'row' + g_row + '\').value)" src="../images/delete.gif"/>';
         if (mytotalsecure != undefined) {
            if (mytotalsecure == "0")
               cell9.innerHTML = cell9.innerHTML + '<img title="Lock" id="img' + g_row + '" class="tcalIcon" align="right" width="21px" height="20px" onclick="lockthis(this, \'PROGRAM\', document.getElementById(\'row' + g_row + '\').value)" src="../images/locked.png"/></center>';
            else
               cell9.innerHTML = cell9.innerHTML + '<img title="Lock" id="img' + g_row + '" class="tcalIcon" align="right" width="21px" height="20px" onclick="lockthis(this, \'PROGRAM\', document.getElementById(\'row' + g_row + '\').value)" src="../images/lockchecked.jpg"/></center>';
         } else {
            cell9.innerHTML = cell9.innerHTML + '<img title="Lock" id="img' + g_row + '" class="tcalIcon" align="right" width="21px" height="20px" onclick="lockthis(this, \'PROGRAM\', document.getElementById(\'row' + g_row + '\').value)" src="../images/locked.png"/></center>';
         }
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'button');
         txtInp.setAttribute('name', 'adda' + g_row);
         txtInp.setAttribute('id', 'adda' + g_row);
         txtInp.setAttribute('style', 'width:80%')
         txtInp.setAttribute('value', '+ P');
         txtInp.setAttribute('onclick', 'addRow(document.getElementById(\'row' + g_row + '\').value);');
         //         cell0.setAttribute('align', 'right')
         //         cell0.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'button');
         txtInp.setAttribute('name', 'addt' + g_row);
         txtInp.setAttribute('id', 'addt' + g_row);
         txtInp.setAttribute('style', 'font-size:8px;')
         txtInp.setAttribute('value', 'CHILD');
         txtInp.setAttribute('onclick', 'addtask(document.getElementById(\'tr' + g_row + '\'),document.getElementById(\'row' + g_row + '\').value);');
         cell3.setAttribute('align', 'right')
         // cell3.appendChild(txtInp);
         //cell3.innerHTML += "</td></tr></table>"

         g_row += 1;

         reorderRows(tbl);
         //taInit(false);
      }

      function addtask(what, inserto, myid, myactivity, mykpiid, mykpiname, mystate, mypicnik, mypicname, mystart, myend, mydeliverable) {
         if (what != '')
            inserto = what.rowIndex;
         var tbl = document.getElementById(TABLE_NAME);
         var nextRow = inserto;
         var num = nextRow;
         var row = tbl.tBodies[0].insertRow(num);
         row.setAttribute('id', 'trt' + g_row);
         row.setAttribute('style', 'vertical-align: top');

         var cell0 = row.insertCell(0);

         var cell1 = row.insertCell(1);

         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'hstate' + g_row);
         txtInp.setAttribute('id', 'hstate' + g_row);
         txtInp.setAttribute('value', 'NEW');
         if (mystate != undefined) {
            txtInp.setAttribute('value', mystate);
         }
         cell1.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'row' + g_row);
         txtInp.setAttribute('id', 'row' + g_row);
         txtInp.setAttribute('size', '0');
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('value', g_row);
         cell1.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('name', 'nox' + g_row);
         txtInp.setAttribute('size', '0');
         txtInp.setAttribute('id', 'nox' + g_row);
         txtInp.setAttribute('disabled', 'disabled');
         if (myid != undefined) {
            txtInp.setAttribute('value', myid);
         }
         cell1.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'numbering' + g_row);
         txtInp.setAttribute('id', 'numbering' + g_row);
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('style', 'width:100%');
         txtInp.setAttribute('class', 'plains');
         cell1.appendChild(txtInp);


         var cell2 = row.insertCell(2);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'jenis' + g_row);
         txtInp.setAttribute('id', 'jenis' + g_row);
         txtInp.setAttribute('size', '10');
         txtInp.setAttribute('value', 'TASK');
         cell2.appendChild(txtInp);
         cell2.setAttribute('style', 'display:none');

         var cell3 = row.insertCell(3);
         var txtInp = document.createElement('textarea');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('style', 'width:85%;');
         txtInp.setAttribute('name', 'activity' + g_row);
         txtInp.setAttribute('id', 'activity' + g_row);
         txtInp.setAttribute('cols', '33');
         txtInp.setAttribute('rows', '3');
         txtInp.setAttribute('onkeydown', 'limitText(this,500)');
         if (myactivity != undefined) {
            var txt = document.createTextNode(myactivity);
            txtInp.appendChild(txt);
         }
         cell3.appendChild(txtInp);
         cell3.innerHTML = cell3.innerHTML + '<table align=\'left\' height="50px" border=\'0\'><tr><td style="border:0;vertical-align:bottom;"><img title="Add New Task" style="cursor:hand" onclick=addtask(document.getElementById("trt' + g_row + '"),document.getElementById(\'row' + g_row + '\').value); src="../images/white.png"/><span style="font-size:9px">Tsk</span></td></tr></table>'


         var cell4 = row.insertCell(4);
         var txtInp = document.createElement('textarea');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('name', 'd' + g_row);
         txtInp.setAttribute('id', 'd' + g_row);
         txtInp.setAttribute('style', 'width:100%;');
         txtInp.setAttribute('rows', '3');
         txtInp.setAttribute('cols', '20');
         txtInp.setAttribute('onkeydown', 'limitText(this,300)');
         //         txtInp.setAttribute('onkeyup', 'javascript:CountL(this, 300);')
         //         txtInp.setAttribute('onchange', 'javascript:CountL(this, 300);')
         if (mydeliverable != undefined) {
            //txtInp.setAttribute('value', mydeliverable);
            var txt = document.createTextNode(mydeliverable);
            txtInp.appendChild(txt);
         }
         cell4.appendChild(txtInp);

         var cell5 = row.insertCell(5);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'hkpiid' + g_row);
         txtInp.setAttribute('id', 'hkpiid' + g_row);
         if (mypicnik != undefined) {
            txtInp.setAttribute('value', mypicnik);
         }
         cell5.appendChild(txtInp);

         var cell8 = row.insertCell(6);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'nikpic' + g_row);
         txtInp.setAttribute('id', 'nikpic' + g_row);
         if (mypicnik != undefined) {
            txtInp.setAttribute('value', mypicnik);
         }
         cell8.appendChild(txtInp);
         cell8.innerHTML += "</td><td style='border-top:1;border-right:0;border-style:collapse;border-color:#C0C0C0'>"
         var txtInp = document.createElement('textarea');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('rows', '3');
         txtInp.setAttribute('cols', '20');
         txtInp.setAttribute('name', 'pic' + g_row);
         txtInp.setAttribute('id', 'pic' + g_row);
         txtInp.setAttribute('READONLY', 'READONLY')
         txtInp.setAttribute('style', 'width:100%;overflow:hidden;border:0')
         if (mypicname != undefined) {
            var txt = document.createTextNode(mypicname);
            txtInp.appendChild(txt);
         }
         cell8.appendChild(txtInp);
         cell8.innerHTML = "<table class='tes'><tr><td style='border-left:0;'>" + cell8.innerHTML + '<td style="vertical-align:top;border-top:1;border-left:0;border-color:#C0C0C0"><img title="Cari Karyawan" style="cursor:hand" onclick=searchkary("nikpic' + g_row + '","pic' + g_row + '") src="../images/search.gif" align="left"/></td></tr></table>';


         var cell6 = row.insertCell(7);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('name', 'start' + g_row);
         txtInp.setAttribute('id', 'start' + g_row);
         txtInp.setAttribute('size', '7');
         txtInp.setAttribute('READONLY', 'READONLY');
         if (mystart != undefined) {
            txtInp.setAttribute('value', mystart);
         }
         cell6.innerHTML = cell6.innerHTML + '<span style="font-size:10px">ST</span>';
         cell6.appendChild(txtInp);
         cell6.innerHTML = cell6.innerHTML + '<img title="Open Calendar" class="tcalIcon" onclick="A_TCALS[\'myCalstartID' + g_row + '\'].f_toggle()" id="tcalico_myCalstartID' + g_row + '" src="images/cal.gif"/>';
         new tcal({
            'formname': 'aspnetForm',
            'controlname': 'start' + g_row,
            'id': 'myCalstartID' + g_row
         });
         cell6.innerHTML = cell6.innerHTML + '<br><span style="font-size:10px">ED</span>';
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('name', 'end' + g_row);
         txtInp.setAttribute('id', 'end' + g_row);
         txtInp.setAttribute('size', '7');
         txtInp.setAttribute('READONLY', 'READONLY');
         if (myend != undefined) {
            txtInp.setAttribute('value', myend);
         }
         cell6.appendChild(txtInp);
         cell6.innerHTML = cell6.innerHTML + '<img title="Open Calendar" class="tcalIcon" onclick="A_TCALS[\'myCalendID' + g_row + '\'].f_toggle()" id="tcalico_myCalendID' + g_row + '" src="images/cal.gif"/>';
         new tcal({
            'formname': 'aspnetForm',
            'controlname': 'end' + g_row,
            'id': 'myCalendID' + g_row
         });


         var cell9 = row.insertCell(8);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'checkbox');
         txtInp.setAttribute('name', 'dela' + g_row);
         txtInp.setAttribute('id', 'dela' + g_row);
         txtInp.setAttribute('onclick', "checkthis(this, 'TASK', document.getElementById('row" + g_row + "').value)");
         txtInp.setAttribute('style', 'visibility:hidden');
         cell9.appendChild(txtInp);
         cell9.innerHTML = cell9.innerHTML + '<center><img title="Delete Row" align="left" id="anchor1" class="tcalIcon" onclick="checkthis(this, \'TASK\', document.getElementById(\'row' + g_row + '\').value)" src="../images/delete.gif"/></center>';


         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'button');
         txtInp.setAttribute('name', 'addt' + g_row);
         txtInp.setAttribute('id', 'addt' + g_row);
         txtInp.setAttribute('style', 'width:80%')
         txtInp.setAttribute('value', '+ T');
         txtInp.setAttribute('onclick', 'addtask(this,document.getElementById(\'row' + g_row + '\').value);');
         cell0.setAttribute('align', 'right')
         //         cell0.appendChild(txtInp);

         g_row += 1;

         reorderRows(tbl);
         //taInit(false);
      }

      function lockthis(what, tipe, nomor) {
         var id;
         id = document.getElementById(TABLE_NAME).tBodies[0].rows[nomor - 1].cells[1].getElementsByTagName('input')[2].value;
         if (id == '')
            alert('Harap melakukan save dahulu sebelum melakukan lock terhadap data');
         else {
            document.getElementById("ctl00_ContentPlaceHolder1_hid").value = id;
            document.getElementById("ctl00_ContentPlaceHolder1_imgid").value = what.id;
            window.open("PopUpAddSecurity.aspx", "List", "scrollbars=yes,resizable=no,width=800,height=400");
            //var returnVal = window.showModalDialog('PopUpAddSecurity.aspx', '', 'dialogWidth=800px;dialogHeight=400px;resizable=yes;help=no;unadorned=yes');

         }
      }

      function changepic(id, stat) {
         if (stat == "0")
            document.getElementById(id).src = "../images/locked.png";
         else
            document.getElementById(id).src = "../images/lockchecked.jpg";
      }

      function deleteRows(grow) {
         var answer = confirm("Yakin menghapus?")
         if (answer) {
            var tbl = document.getElementById(TABLE_NAME);
            var x = document.getElementById('nox' + grow).value;
            var rownum = document.getElementById('row' + grow).value;
            var jenis = document.getElementById('jenis' + grow).value;
            if (x == '' && jenis == 'TASK') {
               var tbl = document.getElementById(TABLE_NAME);
               tbl.tBodies[0].deleteRow(parseFloat(rownum) - 1);
               reorderRows(tbl);
            }
            else if (x == '' && jenis == 'PROGRAM') {
               var deleted = new Array();
               var count = 0;
               deleted[count] = parseFloat(rownum) - 1;
               count++;
               for (var r = parseFloat(rownum), n = tbl.tBodies[0].rows.length; r < n; r++) {
                  if (tbl.tBodies[0].rows[r].cells[2].getElementsByTagName('input')[0].value == "TASK") {
                     deleted[count] = r;
                     count++;
                  } else {
                     break;
                  }
               }
               for (var r = deleted.length - 1, n = 0; r >= n; r--) {
                  tbl.tBodies[0].deleteRow(deleted[r]);
               }
               reorderRows(tbl);
            } else if (jenis == 'TASK') {
               tbl.tBodies[0].deleteRow(rownum - 1);
               reorderRows(tbl);
               $.ajax({
                  type: "POST",
                  contentType: "application/json; charset=utf-8",
                  url: "WS/BSCService.asmx/DeleteTask",
                  data: "{'id':" + x + "}",
                  dataType: "json",
                  success: function(res) {
                     //document.getElementById("resultchild").innerHTML = res.d;
                     if (res.d == "SUKSES") {
                        retrieve();
                        document.getElementById("divprogress").style.visibility = 'hidden';
                        document.getElementById("lblMessage").innerText = "**Save Berhasil";
                        alert("Save Berhasil");
                     } else {
                        document.getElementById("divprogress").style.visibility = 'hidden';
                        alert(res.d);
                        document.getElementById("lblMessage").innerText = res.d;
                     }
                  },
                  error: function(err) {
                     document.getElementById("divprogress").style.visibility = 'hidden';
                     alert("error: " + err.responseText);
                  }
               });
            }
         }
      }


      function reorderRows(tbl) {
         if (tbl.tBodies[0].rows[0]) {
            var mySArray = new Array("A.", "B.", "C.", "D.", "E.", "F.", "G.", "H.", "I.", "J.", "K.", "L.", "M.", "N.", "O.", "P.", "Q.", "R.", "S.", "T.", "U.", "V.", "W.", "X.", "Y.", "Z.");

            var count = 1;
            var noproject = 0;
            var no = 0;
            var subno = 0;
            for (var i = 0; i < tbl.tBodies[0].rows.length; i++) {
               if (tbl.tBodies[0].rows[i].style.display != 'none') {
                  tbl.tBodies[0].rows[i].cells[1].getElementsByTagName('input')[1].value = count;

                  if (tbl.tBodies[0].rows[i].cells[2].getElementsByTagName('input')[0].value == 'PROJECT') {
                     if (parseInt(noproject) > 25) {
                        tbl.tBodies[0].rows[i].cells[1].getElementsByTagName('input')[3].value = mySArray[parseInt((parseInt(noproject) / parseInt(26)) - parseInt(1))] + mySArray[parseInt(noproject) % 26];
                     } else {
                        tbl.tBodies[0].rows[i].cells[1].getElementsByTagName('input')[3].value = mySArray[parseInt(noproject) % 26];
                     }
                     no = 0; subno = 0; noproject += 1;
                  }

                  if (tbl.tBodies[0].rows[i].cells[2].getElementsByTagName('input')[0].value == 'PROGRAM') {
                     no += 1;
                     subno = 0;
                     tbl.tBodies[0].rows[i].cells[1].getElementsByTagName('input')[3].value = no;
                  } else if (tbl.tBodies[0].rows[i].cells[2].getElementsByTagName('input')[0].value == 'TASK') {
                     subno += 1;
                     tbl.tBodies[0].rows[i].cells[1].getElementsByTagName('input')[3].value = no + '.' + subno;
                  }

                  count++;
               }
            }
         }
      }

      function objact(id, nomor, tipe, activity, pic, deliverable, start, enddate, state, del, kpiid, year, namapic) {
         this.id = id; this.nomor = nomor; this.tipe = tipe; this.activity = activity;
         this.pic = pic; this.deliverable = deliverable;
         this.start = start; this.enddate = enddate; this.state = state; this.del = del;
         this.kpiid = kpiid; this.year = year; this.namapic = namapic;
      }

      function save() {
         if (validasi()) {
            document.getElementById("btnSave").style.display = "none";
            document.getElementById("btnSave1").style.display = "none";
            document.getElementById("divprogress").style.visibility = 'visible';
            MyArray = new Array();
            var table = document.getElementById(TABLE_NAME);
            for (var r = 1, n = table.rows.length; r < n; r++) {
               if (table.rows[r].cells[2].getElementsByTagName('input')[0].value == "PROJECT") {
                  MyArray[MyArray.length] = new objact(
                     table.rows[r].cells[1].getElementsByTagName('input')[2].value,
                     table.rows[r].cells[1].getElementsByTagName('input')[1].value,
                     table.rows[r].cells[2].getElementsByTagName('input')[0].value,
                     table.rows[r].cells[3].getElementsByTagName('textarea')[0].value, '', '',
                     '', '', table.rows[r].cells[1].getElementsByTagName('input')[0].value,
                     table.rows[r].cells[4].getElementsByTagName('input')[0].checked,
                     '',
                     document.getElementById("ctl00_ContentPlaceHolder1_ddlYear").options[document.getElementById("ctl00_ContentPlaceHolder1_ddlYear").selectedIndex].value,
                     ''
                     );
               } else {
                  MyArray[MyArray.length] = new objact(
                     table.rows[r].cells[1].getElementsByTagName('input')[2].value,
                     table.rows[r].cells[1].getElementsByTagName('input')[1].value,
                     table.rows[r].cells[2].getElementsByTagName('input')[0].value,
                     table.rows[r].cells[3].getElementsByTagName('textarea')[0].value,
                     table.rows[r].cells[6].getElementsByTagName('input')[0].value,
                     table.rows[r].cells[4].getElementsByTagName('textarea')[0].value,
                     table.rows[r].cells[7].getElementsByTagName('input')[0].value,
                     table.rows[r].cells[7].getElementsByTagName('input')[1].value,
                     table.rows[r].cells[1].getElementsByTagName('input')[0].value,
                     table.rows[r].cells[8].getElementsByTagName('input')[0].checked,
                     table.rows[r].cells[5].getElementsByTagName('input')[0].value,
                     document.getElementById("ctl00_ContentPlaceHolder1_ddlYear").options[document.getElementById("ctl00_ContentPlaceHolder1_ddlYear").selectedIndex].value,
                     ''
                     );
               }
            }
            if (MyArray.length == 0) {
               document.getElementById("divprogress").style.visibility = 'hidden';
               alert("Harap isi activity plan terlebih dahulu");
               return false;
            }
            var funcid = '0';
            var param = '';
            switch (document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').options[document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').selectedIndex].value) {
               case '1': //FUNCTION
                  {
                     param = '1#' + document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').selectedIndex].value;
                     break;
                  }
               case '2': //SECTION
                  {
                     param = '2#' + document.getElementById('ctl00_ContentPlaceHolder1_KPISection').options[document.getElementById('ctl00_ContentPlaceHolder1_KPISection').selectedIndex].value + '#' + +document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').selectedIndex].value;
                     break;
                  }
            }
            $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "WS/BSCService.asmx/SaveActivity",
               data: "{'x':" + JSON.stringify(MyArray) + ",'userid':" + document.getElementById("ctl00_ContentPlaceHolder1_userid").value + ",'userip':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_userip").value) + ",'_param':" + JSON.stringify(param) + "}",
               dataType: "json",
               success: function(res) {
                  //document.getElementById("resultchild").innerHTML = res.d;
                  if (res.d == "SUKSES") {
                     retrieve();
                     document.getElementById("divprogress").style.visibility = 'hidden';
                     document.getElementById("lblMessage").innerText = "**Save Berhasil";
                     alert("Save Berhasil");
                  }
                  else if (res.d == "NODATA") {
                     emptyRow(); addRow(0); document.getElementById("divprogress").style.visibility = 'hidden';
                  } else {
                     document.getElementById("divprogress").style.visibility = 'hidden';
                     alert(res.d);
                     document.getElementById("lblMessage").innerText = res.d;
                  }
                  document.getElementById("btnSave").style.display = "";
                  document.getElementById("btnSave1").style.display = "";
               },
               error: function(err) {
                  document.getElementById("btnSave").style.display = "";
                  document.getElementById("btnSave1").style.display = "";
                  document.getElementById("divprogress").style.visibility = 'hidden';
                  alert("error: " + err.responseText);
               }
            });
         }
      }

      function validasi() {
         var table = document.getElementById(TABLE_NAME);
         for (var r = 1, n = table.rows.length; r < n; r++) {
            if (table.rows[r].cells[2].getElementsByTagName('input')[0].value != "PROJECT") {
               if (table.rows[r].cells[8].getElementsByTagName('input')[0].checked == true) {
               } else {
                  if (table.rows[r].cells[6].getElementsByTagName('input')[0].value == '') {
                     alert('PIC No. ' + table.rows[r].cells[1].getElementsByTagName('input')[3].value + ' harus diisi');
                     return false;
                  } else if (table.rows[r].cells[3].getElementsByTagName('textarea')[0].value == '') {
                     alert('Activity Plan No. ' + table.rows[r].cells[1].getElementsByTagName('input')[3].value + ' harus diisi');
                     return false;
                  } else if (table.rows[r].cells[3].getElementsByTagName('textarea')[0].value.length > 500) {
                     alert('Jumlah karakter activity plan No. ' + table.rows[r].cells[1].getElementsByTagName('input')[3].value + ' tidak boleh lebih dari 500 karakter');
                     return false;
                  } else if (table.rows[r].cells[4].getElementsByTagName('textarea')[0].value.length > 300) {
                     alert('Jumlah karakter deliverable No. ' + table.rows[r].cells[1].getElementsByTagName('input')[3].value + ' tidak boleh lebih dari 300 karakter');
                     return false;
                  }
               }
            }
         }
         return true;
      }

      function openSearchModal() {
         var tipe;
         switch (document.getElementById("ctl00_ContentPlaceHolder1_Grup").value) {
            case "7":
               {
                  tipe = 2; break;
               }
            case "A":
               {
                  tipe = 2; break;
               }
            case "1":
               {
                  tipe = 1; break;
               }
            case "2":
               {
                  tipe = 2; break;
               }
            case "8":
               {
                  tipe = 1; break;
               }

         }
         var returnVal = window.showModalDialog('PopUpSearch.aspx?type=' + tipe + '&wtr=1', '', 'dialogWidth=300px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         if (returnVal != undefined) {
            document.getElementById("KPIId").value = returnVal.split("|")[0];
            document.getElementById("KPIName").value = returnVal.split("|")[1];
            document.getElementById("divprogress").style.visibility = 'visible';
            retrieve();
         }
      }

      function retrieve() {
         document.getElementById("divprogress").style.visibility = 'visible';
         var funcid = '0';
         var param = '';
         switch (document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').options[document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').selectedIndex].value) {
            case '1': //FUNCTION
               {
                  param = '1#' + document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').selectedIndex].value;
                  break;
               }
            case '2': //SECTION
               {
                  param = '2#' + document.getElementById('ctl00_ContentPlaceHolder1_KPISection').options[document.getElementById('ctl00_ContentPlaceHolder1_KPISection').selectedIndex].value + '#' + +document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').selectedIndex].value;
                  break;
               }
         }


         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/RetrieveActivity",
            data: "{'_param':" + JSON.stringify(param) + ",'_year':" + document.getElementById("ctl00_ContentPlaceHolder1_ddlYear").options[document.getElementById("ctl00_ContentPlaceHolder1_ddlYear").selectedIndex].value + "}",
            dataType: "json",
            success: function(res) {
               //document.getElementById("resultchild").innerHTML = res.d;
               document.getElementById("divprogress").style.visibility = 'visible';
               MyAct = new Array();
               MyAct = res.d;
               emptyRow();
               var noprogram = 1;
               if (MyAct.length == 0) {
                  emptyRow(); addProject(0);
               }
               for (var i = 0; i < MyAct.length; i++) {
                  switch (MyAct[i].tipe) {
                     case 'PROJECT':
                        addProject('', MyAct[i].nomor, MyAct[i].id, MyAct[i].activity, MyAct[i].state);
                        break;
                     case 'PROGRAM':
                        addRow('', MyAct[i].nomor, MyAct[i].id, MyAct[i].activity, MyAct[i].kpiid, MyAct[i].kpiname, MyAct[i].state, MyAct[i].pic, MyAct[i].namapic, MyAct[i].start, MyAct[i].enddate, MyAct[i].deliverable, MyAct[i].totalsecure);
                        noprogram = i + 1;
                        break;
                     case 'TASK':
                        addtask('', MyAct[i].nomor, MyAct[i].id, MyAct[i].activity, MyAct[i].kpiid, MyAct[i].kpiname, MyAct[i].state, MyAct[i].pic, MyAct[i].namapic, MyAct[i].start, MyAct[i].enddate, MyAct[i].deliverable);
                        break;
                  }
               }
               //document.getElementById("rowtohide").style.visibility = "";
               document.getElementById("btnSave").style.visibility = "";
               document.getElementById("btnSave1").style.visibility = "";
               document.getElementById("divprogress").style.visibility = 'hidden';

            },
            error: function(err) {
               alert("error: " + err.responseText);
            }
         });
         document.getElementById("lblMessage").innerText = '';

      }

      function emptyRow() {
         var tbl = document.getElementById(TABLE_NAME);
         for (var i = tbl.rows.length; i > 1; i--) {
            tbl.deleteRow(i - 1);
         }
      }

      function ddlChange() {
         retrieve();
      }

      function CountL(text, howlong) {
         var maxlength = new Number(howlong); // Change number to your max length.
         if (document.getElementById(text.id).value.length > maxlength) {
            text.value = text.value.substring(0, maxlength);
            alert("Deliverables hanya bisa menampung 300 karakter");
         }
      }

      function checkthis(what, tipe, nomor) {
         var answer = confirm("Yakin menghapus?")
         if (answer) {
            var idtodel = what.parentNode.parentNode.parentNode.id
            var tbl = document.getElementById(TABLE_NAME);
            if (tipe == "PROJECT") {
               document.getElementById(idtodel).cells[4].getElementsByTagName('input')[0].checked = true;
               document.getElementById(idtodel).style.display = "none";
               for (var r = parseFloat(document.getElementById(idtodel).rowIndex), n = tbl.tBodies[0].rows.length; r < n; r++) {
                  if (tbl.tBodies[0].rows[r].cells[2].getElementsByTagName('input')[0].value == "PROJECT") {
                     break;
                  } else {
                     tbl.tBodies[0].rows[r].cells[8].getElementsByTagName('input')[0].checked = true;
                     tbl.tBodies[0].rows[r].style.display = "none";
                  }
               }
            } else if (tipe == "PROGRAM") {
               document.getElementById(idtodel).cells[8].getElementsByTagName('input')[0].checked = true;
               document.getElementById(idtodel).style.display = "none";
               for (var r = parseFloat(document.getElementById(idtodel).rowIndex), n = tbl.tBodies[0].rows.length; r < n; r++) {
                  if (tbl.tBodies[0].rows[r].cells[2].getElementsByTagName('input')[0].value == "TASK") {
                     tbl.tBodies[0].rows[r].cells[8].getElementsByTagName('input')[0].checked = true;
                     tbl.tBodies[0].rows[r].style.display = "none";
                  } else {
                     break;
                  }
               }
            } else {
               document.getElementById(idtodel).cells[8].getElementsByTagName('input')[0].checked = true;
               document.getElementById(idtodel).style.display = "none";
            }
            reorderRows(tbl);
         }
      }      
   </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div id="divprogress">
      <center>
         <img id="imgloading" alt="" src="/memo/images/tloader.gif" style="margin-top: 100px" /></center>
   </div>
   <div>
      <asp:HiddenField ID="Grup" runat="server" />
      <asp:HiddenField ID="Jobsite" runat="server" />
      <asp:HiddenField ID="Divisi" runat="server" />
      <asp:HiddenField ID="Section" runat="server" />
      <asp:HiddenField ID="userid" runat="server" />
      <asp:HiddenField ID="userip" runat="server" />
      <asp:HiddenField ID="hid" runat="server" />
      <asp:HiddenField ID="imgid" runat="server" />
      <!--<input type="button" value="Retrieve" onclick="retrieve();"/>!-->
      <table width="100%">
         <tr>
            <td colspan="3" class="styleHeader">
               Input Activity Plan
            </td>
         </tr>
         <tr>
            <td colspan="2" width="40%">
               <div id="lblMessage" style="color: red; font-weight: bold">
               </div>
            </td>
            <td rowspan="4" width="60%" align="right">
               <table align="left" style="text-align:left">
                  <tr style="vertical-align:bottom">
                     <td><img alt="Add" src="../images/green.png" /> PROGRAM </td><td> : </td><td> Program kerja yang akan dilaksanakan</td>
                  </tr>   
                   <tr style="vertical-align:bottom">
                     <td><img alt="Add" src="../images/blue.png" /> ACTIVITY </td><td> : </td><td> Aktivitas yang akan dilakukan untuk mencapai tujuan suatu program kerja</td>
                  </tr>
                   <tr style="vertical-align:bottom">
                     <td><img alt="Add" src="../images/white.png" /> TASK </td><td> : </td><td> Tugas detail yang dilaksanakan untuk mendukung sebuah aktivitas</td>
                  </tr>             
               </table>
               <input type="button" id="btnSave" onclick="save();" style="background-image: url(../images/save-icon-small.png);
                  background-repeat: no-repeat; cursor: hand; width: 55px; height: 55px; background-color: transparent;
                  border: 0" />
            </td>
         </tr>
         <tr>
            <td valign="top">
               Tahun
            </td>
            <td valign="top">
               <asp:DropDownList ID="ddlYear" runat="server" onchange="ddlChange();">
                  <asp:ListItem>2010</asp:ListItem>
                  <asp:ListItem>2011</asp:ListItem>
                  <asp:ListItem>2012</asp:ListItem>
               </asp:DropDownList>
            </td>
         </tr>
         <tr>
            <td valign="top">
               Level
            </td>
            <td valign="top">
               <asp:DropDownList ID="KPILevel" runat="server" AutoPostBack="true" CssClass="plains">
                  <asp:ListItem Value="0" Text="JRN"></asp:ListItem>
                  <asp:ListItem Value="1" Text="Function"></asp:ListItem>
                  <asp:ListItem Value="2" Text="Section"></asp:ListItem>
                  <asp:ListItem Value="3" Text="Jobsite"></asp:ListItem>
               </asp:DropDownList>
               <asp:DropDownList ID="KPIFunc" runat="server" Visible="False" CssClass="plains">
               </asp:DropDownList>
               <asp:DropDownList ID="KPISection" runat="server" Visible="False" CssClass="plains">
               </asp:DropDownList>
               <asp:DropDownList ID="KPIJobsite" runat="server" Visible="False" CssClass="plains">
               </asp:DropDownList>
            </td>
         </tr>
         <%--<tr id="rowtohide" style="visibility: hidden">
            <td colspan="2">
               <input type="button" value="Tambah Program" onclick="addRow();" />
            </td>
         </tr>--%>
      </table>
      <table id="activity" width="100%" class="act">
         <thead>
            <tr align="center" style="font-size: 14px; background-color: #00CC00; color: black;
               font-weight: bold;">
               <td width="0%">
               </td>
               <td width="1%">
                  No.
               </td>
               <td width="0px" style="display: none">
                  Jenis
               </td>
               <td width="29%">
                  Action Plan
               </td>
               <td width="20%">
                  Deliverable
               </td>
               <td width="18%">
                  KPI
               </td>
               <td width="15%">
                  PIC
               </td>
               <td width="11%">
                  Start - End
               </td>
               <td width="7%">
                  Action
               </td>
            </tr>
         </thead>
         <tbody>
         </tbody>
      </table>
      <input type="button" id="btnSave1" onclick="save();" style="background-image: url(../images/save-icon-small.png);
         background-repeat: no-repeat; float: right; cursor: hand; width: 55px; height: 55px;
         background-color: transparent; border: 0" />
   </div>

   <script type="text/javascript">
      document.getElementById("divprogress").style.visibility = 'visible';
      retrieve();
      //addProject();
      function taInit(bCols) {
         var i, ta = document.getElementsByTagName('textarea');
         for (i = 0; i < ta.length; ++i) {
            ta[i]._ta_resize_cols_ = bCols;
            ta[i]._ta_default_rows_ = ta[i].rows;
            ta[i]._ta_default_cols_ = ta[i].cols;
            ta[i].onkeyup = taExpand;
            ta[i].onmouseover = taExpand;
            ta[i].onmouseout = taRestore;
            ta[i].onfocus = taOnFocus;
            ta[i].onblur = taOnBlur;
            //ta[i].onpaste = alert(e);
            //jQuery(ta[i]).bind('input paste', function(e) { alert(e) })
         }
      }
      function taOnFocus(e) {
         this._ta_is_focused_ = true;
         this.onmouseover();
      }

      function Left(str, n) {
         if (n <= 0)
            return "";
         else if (n > String(str).length)
            return str;
         else
            return String(str).substring(0, n);
      }

      function taOnBlur() {
         var a = 0;
         a = this.value.length;
         if (this.name.indexOf('nmIssue') != -1)
         { if (a > 150) this.value = Left(this.value, 150) }
         else if (this.name.indexOf('action') != -1)
         { if (a > 500) this.value = Left(this.value, 500) }
         else if (this.name.indexOf('deliverable') != -1)
         { if (a > 70) this.value = Left(this.value, 70) }
      }

      function taRestore() {
         if (!this._ta_is_focused_) {
            this.rows = this._ta_default_rows_;
            if (this._ta_resize_cols_) {
               this.cols = this._ta_default_cols_;
            }
         }
      }

      function taExpand() {
         var a, i, r, c = 0;
         a = this.value.split('\n');
         if (this._ta_resize_cols_) {
            for (i = 0; i < a.length; i++) // find max line length
            {
               if (a[i].length > c) {
                  c = a[i].length;
               }
            }
            if (c < this._ta_default_cols_) {
               c = this._ta_default_cols_;
            }
            this.cols = c;
            r = a.length;
         }
         else {
            for (i = 0; i < a.length; i++) {
               if (a[i].length > this.cols) {
                  c += Math.floor(a[i].length / this.cols);
               }
            }
            r = c + a.length;
         }
         if (r < this._ta_default_rows_) {
            r = this._ta_default_rows_;
         }
         this.rows = r;
      }
      function applystyle() {
         taInit(false);
      }

      //window.onload = applystyle;
   </script>

</asp:Content>
