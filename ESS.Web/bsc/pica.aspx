﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pica.aspx.vb" Inherits="EXCELLENT.pica" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
        .newStyle1
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
            margin-right: 0px;
        }
        .style1
        {
            font-weight: bold;
            text-decoration: underline;
        }
        .styleAlternate
        {
            background-color:#D1FFC1;
            border-bottom:solid 1px #000000;
        }
        .styleNonAlternate
        {
         border-bottom:solid 1px #000000;
        }
        .style2
        {
            width: 100%;
        }
        .style3
        {
            width: 83px;
        }
        .style4
        {
            text-decoration: underline;
        }
         .style11
        {
            width: 100%;
            font: small Arial, Helvetica, sans-serif;
        }
          td.style5
        {
          background-color:#00CC00;
          color:white;
          font-weight:bold;
          font-size:medium;
        }
        .headerStyle
        {
        background-color:#00CC00;
          color:white;
          font-weight:bold;
          font-size:x-small;
        
        }
    </style>
    
</head>
<body>
    <form id="form1" runat="server">
    <div class="newStyle1">
    
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
        <br />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table class="style11">
                    <tr>
                        <td class="style5">
                            PICA Entry
                            </td>
                    </tr>
                </table>
                
                
                <table class="style2">
                    <tr>
                        <td class="style3">Year</td>
                        <td><asp:TextBox ID="txtYear" runat="server" CssClass="newStyle1"></asp:TextBox></td>
                    </tr>
                <tr>
                    <td class="style3">Month</td>
                    <td><asp:TextBox ID="txtMonth" runat="server" CssClass="newStyle1" Enabled="False"></asp:TextBox></td>
                </tr>
                <tr>
                     <td class="style3">KPI</td>
                     <td><asp:TextBox ID="txtKPI" runat="server" CssClass="newStyle1" Enabled="False" 
                           Columns="100"></asp:TextBox>
                        <asp:HiddenField ID="idkpi" runat="server" />
                     </td>
                </tr>
               </table>
                <br />
                <asp:Label ID="lbWarning" runat="server"></asp:Label>
                <asp:PlaceHolder ID="PlaceHolder1" runat="server">
                <asp:Table runat="server" ID="PICAtabel" cellpadding="5" cellspacing="0">
                    <asp:TableRow cssclass="headerStyle">
                        <asp:TableHeaderCell>Tanggal PICA</asp:TableHeaderCell>
                        <asp:TableHeaderCell>PI</asp:TableHeaderCell>
                        <asp:TableHeaderCell>CA</asp:TableHeaderCell>
                        <asp:TableHeaderCell>Target Hasil</asp:TableHeaderCell>
                        <asp:TableHeaderCell>PIC</asp:TableHeaderCell>
                        <asp:TableHeaderCell>Due Date</asp:TableHeaderCell>
                        <asp:TableHeaderCell>Status</asp:TableHeaderCell>
                    </asp:TableRow>
                    <asp:TableRow cssclass="styleNonAlternate">
                        <asp:TableCell VerticalAlign="Top"> 
                        <asp:hiddenfield runat="server" id="idpica1" value=""/>
                        <asp:TextBox ID="txtTglPICA1" runat="server" CssClass="newStyle1" Columns="14"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" 
                                Enabled="True" TargetControlID="txtTglPICA1" Format="MM/dd/yyyy">
                            </asp:CalendarExtender></asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                             <asp:TextBox ID="txtPI1" runat="server" Columns="30" CssClass="newStyle1" 
                                Rows="5" TextMode="MultiLine"></asp:TextBox>
                            <asp:Button ID="btnAddPI1" runat="server" CssClass="newStyle1" Text="Add" />
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                             <asp:TextBox ID="txtCA1" runat="server" Columns="30" CssClass="newStyle1" 
                                Rows="5" TextMode="MultiLine"></asp:TextBox>
                            <asp:Button ID="btnAddCA1" runat="server" CssClass="newStyle1" Text="Add" />
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                            <asp:TextBox ID="txtTarget1" runat="server" CssClass="newStyle1"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                            <asp:TextBox ID="txtPIC1" runat="server" CssClass="newStyle1" Columns="20"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                            <asp:TextBox ID="txtDueDate1" runat="server" CssClass="newStyle1" Columns="14"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" 
                                Enabled="True" TargetControlID="txtDueDate1" Format="MM/dd/yyyy">
                            </asp:CalendarExtender>
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                             <asp:DropDownList ID="ddlStatus1" runat="server" CssClass="newStyle1">
                                <asp:ListItem Selected="True" Value="0">Open</asp:ListItem>
                                <asp:ListItem Value="1">Close</asp:ListItem>
                            </asp:DropDownList>
                            </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                </asp:PlaceHolder>
                <asp:Button ID="btnSave" runat="server" CssClass="newStyle1" Text="Save" />
                <br />
                
                
                <br />
                <asp:HiddenField ID="hdnval" runat="server" />
                <asp:GridView ID="GridView1" runat="server" Visible="False">
                </asp:GridView>
                <br />
            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
