﻿Imports System.Data.SqlClient

Partial Public Class PopUpAddKPI
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                btnOk.Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        BindGrid()
    End Sub

    Private Sub srchResult_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles srchResult.PageIndexChanging
        RememberOldValues()
        BindGrid()
        srchResult.PageIndex = e.NewPageIndex
        srchResult.DataBind()
        RePopulateValues()
    End Sub

    Private Sub RePopulateValues()
        Dim categoryIDList As ArrayList = DirectCast(Session("CHECKED_ITEMS"), ArrayList)
        If categoryIDList IsNot Nothing AndAlso categoryIDList.Count > 0 Then
            For Each row As GridViewRow In srchResult.Rows
                Dim index As Integer = CInt(srchResult.DataKeys(row.RowIndex).Value)
                If categoryIDList.Contains(index) Then
                    Dim myCheckBox As CheckBox = DirectCast(row.FindControl("cbItem"), CheckBox)
                    myCheckBox.Checked = True
                End If
            Next
        End If
    End Sub

    Private Sub RememberOldValues()
        Dim categoryIDList As New ArrayList()
        Dim index As Integer = -1
        For Each row As GridViewRow In srchResult.Rows
            index = CInt(srchResult.DataKeys(row.RowIndex).Value)
            Dim result As Boolean = DirectCast(row.FindControl("cbItem"), CheckBox).Checked

            ' Check in the Session
            If Session("CHECKED_ITEMS") IsNot Nothing Then
                categoryIDList = DirectCast(Session("CHECKED_ITEMS"), ArrayList)
            End If
            If result Then
                If Not categoryIDList.Contains(index) Then
                    categoryIDList.Add(index)
                End If
            Else
                categoryIDList.Remove(index)
            End If
        Next
        If categoryIDList IsNot Nothing AndAlso categoryIDList.Count > 0 Then
            Session("CHECKED_ITEMS") = categoryIDList
        End If
    End Sub



    Sub BindGrid()
        Try
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim query As String = ""

            Select Case Request.QueryString("type")
                'Case "0"
                '    query = "select a.ID,p.abbr as Perspective,a.Name from MKPI a left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='0' order by p.id,a.name"
                'Case "1"
                '    query = "select a.ID,p.abbr as Perspective,a.Name,c.functionname from MKPI a inner join MFUNCTION c on a.Func=c.ID left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='1' order by p.id,a.func,a.name"
                'Case "2"
                '    query = "select a.ID,p.abbr as Perspective,a.Name,d.kdsite,c.sectionname from MKPI a inner join MSECTION c on a.section=c.ID inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='2' order by p.id,a.section,a.name"
                'Case "3"
                '    query = "select a.ID,p.abbr as Perspective,a.Name,d.kdsite,c.sectionname from MKPI a inner join MSECTION c on a.section=c.ID inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='2' and a.jobsite='" & Request.QueryString("site") & "' order by p.id,a.section,a.name"
                Case "0"
                    'query = "select a.ID,p.abbr as Perspective,a.Name,c.functionname from MKPI a inner join MFUNCTION c on a.Func=c.ID left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and a.id not in (select idchild from KPIRELATION  where IDParent='" & Request.QueryString("id") & "') and level='1' order by p.id,a.name"
                    'query = "select a.ID,p.abbr as Perspective,a.Name,c.functionname from MKPI a inner join MFUNCTION c on a.Func=c.ID left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and a.id not in (select idchild from KPIRELATION) and level='1' order by p.id,a.name"
                    query = "select a.ID,p.abbr as Perspective,a.Name,c.direktoratname from MKPI a inner join MDIREKTORAT c on a.dir=c.ID left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and a.id not in (select idchild from KPIRELATION) and level='4' order by p.id,a.name"
                Case "1" 'Function search Section
                    'query = "select a.ID,p.abbr as Perspective,a.Name,d.kdsite,c.sectionname from MKPI a inner join MSECTION c on a.section=c.ID inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='2' and a.id not in (select idchild from KPIRELATION  where IDParent='" & Request.QueryString("id") & "') and section=(select id from msection where FuncID='" & Request.QueryString("func") & "') order by p.id,a.section,a.name"
                    query = "select a.ID,p.abbr as Perspective,a.Name,d.kdsite,c.sectionname from MKPI a inner join MSECTION c on a.section=c.ID inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='2' and a.id not in (select idchild from KPIRELATION  where IDParent not in (select ID from mkpi where level='3')) and section in (select id from msection where FuncID='" & Request.QueryString("func") & "') order by p.id,a.section,a.name"
                Case "3" 'JOBSITE
                    'query = "select a.ID,p.abbr as Perspective,a.Name,d.kdsite,c.sectionname from MKPI a inner join MSECTION c on a.section=c.ID inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='2' and a.jobsite='" & Request.QueryString("site") & "' and a.id not in (select idchild from KPIRELATION  where IDParent='" & Request.QueryString("id") & "') order by p.id,a.section,a.name"
                    query = "select a.ID,p.abbr as Perspective,a.Name,d.kdsite,c.sectionname from MKPI a inner join MSECTION c on a.section=c.ID inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='2' and a.jobsite='" & Request.QueryString("site") & "' and a.id not in (select idchild from KPIRELATION  where IDParent not in (select ID from mkpi where level<>'3')) order by p.id,a.section,a.name"
                Case "4" 'Direktorat
                    query = "select a.ID,p.abbr as Perspective,a.Name,c.functionname from MKPI a inner join MFUNCTION c on a.Func=c.ID and c.DirID='" & Request.QueryString("dir") & "' left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and a.id not in (select idchild from KPIRELATION) and level='1' order by p.id,a.name"
            End Select
            Dim cmd As SqlCommand
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(query, conn)
            cmd = New SqlCommand(query, conn)
            conn.Open()
            da.Fill(dt)
            conn.Close()
            srchResult.DataSource = dt
            srchResult.DataBind()

            If dt.Rows.Count <> 0 Then
                btnOk.Visible = True
            Else
                btnOk.Visible = False
            End If

            'Dim s(1) As String
            's(0) = "ID"
            'srchResult.DataKeyNames = s
        Catch ex As Exception
            lblKet.Text = ex.Message
        End Try
    End Sub

    Private Sub srchResult_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles srchResult.RowDataBound
        Try
            'e.Row.Attributes.Add("ondblclick", "javascript:executeClose(" & e.Row.Cells(0).Text & ")")
        Catch ex As Exception
            lblKet.Text = ex.Message
        End Try
    End Sub


    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Dim index As Integer = -1
        Dim query As String = ""
        Dim categoryIDList As New ArrayList()
        lblKet.Text = ""

        For Each row As GridViewRow In srchResult.Rows
            index = CInt(srchResult.DataKeys(row.RowIndex).Value)
            Dim result As Boolean = DirectCast(row.FindControl("cbItem"), CheckBox).Checked
            If result = True Then
                query += "insert into KPIRELATION values ('" & Request.QueryString("id") & "','" & index.ToString & "','1');"
            End If
        Next
        If Session("CHECKED_ITEMS") IsNot Nothing Then
            categoryIDList = DirectCast(Session("CHECKED_ITEMS"), ArrayList)
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            For Each c As String In categoryIDList
                query += "insert into KPIRELATION values ('" & Request.QueryString("id") & "','" & c & "','1');"
            Next
        End If
        If query <> "" Then
            query = "BEGIN TRANSACTION; BEGIN TRY " & query
            query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim cmd As New SqlCommand(query, conn)
            Dim result As String
            conn.Open()
            result = cmd.ExecuteScalar()
            conn.Close()
            If result = 0 Then
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes8", "javascript:recount('" & Request.QueryString("id") & "');", True)
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes7", "javascript:executeClose('OK');", True)
            Else
                lblKet.Text = result
            End If
        End If

    End Sub
End Class