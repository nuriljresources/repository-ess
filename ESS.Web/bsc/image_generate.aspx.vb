﻿Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Imaging

Partial Public Class image_generate
    Inherits System.Web.UI.Page

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim id, year As String
        Dim month As Integer
        id = Request.QueryString("id")
        year = Request.QueryString("year")
        month = Request.QueryString("m")
        GenerateGraphic(id, year, month)
    End Sub

    Sub GenerateGraphic(ByVal id As String, ByVal year As String, ByVal month As Integer)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim cmd As SqlCommand
        Try
            Dim sqlQuery As String = "Select month,isnull(target,0) as target,isnull(actual,0) as actual from tbsc where kpiid='" & id & "' and year='" & year & "' order by cast(month as integer)"
            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter(sqlQuery, conn)
            conn.Open()
            da.Fill(dt)
            Dim objBitmap As New Bitmap(730, 320)
            Dim objGraphic As Graphics = Graphics.FromImage(objBitmap)
            Dim whiteBrush As New SolidBrush(Color.White)
            Dim blackPen As New Pen(Color.Black, 2)
            Dim redBrush As New SolidBrush(Color.Red)
            Dim blueBrush As New SolidBrush(Color.Blue)

            Dim drawFont As New Font("Verdana", 10)
            Dim drawFontN As New Font("Verdana", 8)
            Dim drawBrush As New SolidBrush(Color.Black)

            Dim highestval As Decimal = 0

            'Get Highest Value
            For Each dr As DataRow In dt.Rows
                If dr("target") > highestval Then
                    highestval = dr("target")
                End If
                If dr("month") <= month Then
                    If dr("actual") > highestval Then
                        highestval = dr("actual")
                    End If
                End If
            Next

            If highestval = 0 Then highestval = 1

            If highestval <= 10000 Then
                objGraphic.FillRectangle(whiteBrush, 0, 0, 730, 320)
                objGraphic.DrawLine(blackPen, New Point(60, 235), New Point(730, 235))
                objGraphic.DrawLine(blackPen, New Point(65, 45), New Point(65, 260))
                ''--
                objGraphic.DrawLine(blackPen, New Point(0, 260), New Point(730, 260))
                objGraphic.DrawLine(blackPen, New Point(0, 295), New Point(730, 295))
                ''--
                objGraphic.DrawString("ACTUAL", drawFontN, drawBrush, 12, 265)
                objGraphic.DrawString("TARGET", drawFontN, drawBrush, 12, 275)
                objGraphic.FillRectangle(redBrush, 0, 265, 10, 10)
                objGraphic.FillRectangle(blueBrush, 0, 275, 10, 10)
                objGraphic.DrawString("MONTHLY", New Font("Verdana", 16), drawBrush, 245, 5)
            Else
                objBitmap = New Bitmap(1150, 320)
                objGraphic = Graphics.FromImage(objBitmap)
                objGraphic.FillRectangle(whiteBrush, 0, 0, 1150, 320)
                objGraphic.DrawLine(blackPen, New Point(60, 235), New Point(1150, 235))
                objGraphic.DrawLine(blackPen, New Point(65, 45), New Point(65, 260))
                ''--
                objGraphic.DrawLine(blackPen, New Point(0, 262), New Point(1150, 262))
                objGraphic.DrawLine(blackPen, New Point(0, 283), New Point(1150, 283))
                objGraphic.DrawLine(blackPen, New Point(0, 315), New Point(1150, 315))
                ''--
                objGraphic.DrawString("ACTUAL", drawFontN, drawBrush, 12, 285)
                objGraphic.DrawString("TARGET", drawFontN, drawBrush, 12, 295)
                objGraphic.FillRectangle(redBrush, 0, 285, 10, 10)
                objGraphic.FillRectangle(blueBrush, 0, 295, 10, 10)
                objGraphic.DrawString("MTD", New Font("Verdana", 16), drawBrush, 245, 5)
            End If

            Dim x As Decimal = 70
            Dim cmonth As Integer = 1

            For Each dr As DataRow In dt.Rows
                objGraphic.FillRectangle(blueBrush, x, 234 - ((dr("target") / highestval) * 190), 15, ((dr("target") / highestval) * 190))
                'objGraphic.DrawString(dr("target"), drawFontN, drawBrush, x, 194 - ((dr("target") / highestval) * 190))
                If cmonth <= month Then
                    objGraphic.FillRectangle(redBrush, x + 15, 234 - ((dr("actual") / highestval) * 190), 15, ((dr("actual") / highestval) * 190))
                    cmonth += 1
                End If
                'objGraphic.DrawString(dr("actual"), drawFontN, drawBrush, x + 15, 194 - ((dr("actual") / highestval) * 190))
                objGraphic.DrawString(doMapping(dr("month")), drawFont, drawBrush, x, 245)
                x += 55
            Next
            x = 70
            cmonth = 1
            For Each dr As DataRow In dt.Rows
                If highestval <= 10000 Then
                    objGraphic.DrawLine(blackPen, New Point(x - 5, 262), New Point(x - 5, 295))
                    If cmonth <= month Then
                        objGraphic.DrawString(System.Math.Round(dr("actual"), 2), drawFontN, drawBrush, x, 265)
                        cmonth += 1
                    End If
                    objGraphic.DrawString(System.Math.Round(dr("target"), 2), drawFontN, drawBrush, x, 275)
                    x += 55
                Else
                    objGraphic.DrawLine(blackPen, New Point(x - 5, 262), New Point(x - 5, 315))
                    objGraphic.DrawString(doMapping(dr("month")), drawFont, drawBrush, x, 265)
                    If cmonth <= month Then
                        objGraphic.DrawString(System.Math.Round(dr("actual"), 2), drawFontN, drawBrush, x, 285)
                        cmonth += 1
                    End If
                    objGraphic.DrawString(System.Math.Round(dr("target"), 2), drawFontN, drawBrush, x, 295)
                    x += 90
                End If
            Next
            If highestval <= 10000 Then
                objGraphic.DrawLine(blackPen, New Point(760, 232), New Point(730, 295))
            Else
                objGraphic.DrawLine(blackPen, New Point(1150, 262), New Point(1150, 315))
            End If
            Response.ContentType = "image/gif"
            objBitmap.Save(Response.OutputStream, ImageFormat.Gif)
        Catch ex As Exception
            'lblMessage.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Function doMapping(ByVal val As String) As String
        Select Case val
            Case "1"
                Return "JAN"
            Case "2"
                Return "FEB"
            Case "3"
                Return "MAR"
            Case "4"
                Return "APR"
            Case "5"
                Return "MEI"
            Case "6"
                Return "JUN"
            Case "7"
                Return "JUL"
            Case "8"
                Return "AGU"
            Case "9"
                Return "SEP"
            Case "10"
                Return "OKT"
            Case "11"
                Return "NOV"
            Case "12"
                Return "DES"
        End Select
        Return ""
    End Function
End Class