﻿Imports System.Data
Imports System.Data.SqlClient

Partial Public Class supp_permit_lisense_entry
    Inherits System.Web.UI.Page
    Public opttype As String
    Public fsubject As String
    Public doctype As String
    Public rems As String
    Public mnentry As String
    Public mnview As String
    Public mnmgmt As String
    Public msgbox As String
    Public myDt As DataTable
    Public js As String
    Dim myDt2 As DataTable
    Dim llp As New lp
    Public mncontract As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strsqldet As String = "select * from docpermit_lisense_trans_det where id = "
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim dtbdet As DataTable = New DataTable()
        Dim dt As DataTable
        Dim sedit As String = "new"
        Dim strsch As String = ""
        Dim dtbsch As DataTable

        If Trim(Session("permission_permit")) = "1" Or Trim(Session("permission_permit")) = "2" Then
            mnentry = "<div id='mnentry'><img alt='entry document' src='../images/open_doc_small.png' /> <a href='Doc_center_entry.aspx'>&nbsp;Set Document</a></div>"
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"

            If Session("dept").ToString().ToLower = "contract" Or Session("dept").ToString().ToLower() = "contract management system" Then
                mncontract = "<div id='mncontract'><img alt='view document' src='../images/open_doc_small.png' /> <a href='supp_permit_lisense_entry.aspx'>&nbsp;Set Permit & License</a></div>"
            End If
        End If

        If Trim(Session("permission_permit")) = "1" Then
            mnmgmt = "<div id='mnmgmt'> <img alt='Management User' src='../images/open_doc_small.png' /> <a href='mgmnt_list.aspx'>&nbsp;Management User</a></div>"
        End If

        If Trim(Session("permission_permit")) = "3" Then
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
        End If

        Try
            hidpl.Value = Session("pid").ToString()
        Catch ex As Exception

        End Try

        If Not IsPostBack Then
            yes.Visible = False
            no.Visible = False
            sqlConn.Open()
            If Session("otorisasi_permit") = Nothing Or Session("otorisasi_permit") = "" Then
                Response.Redirect("signin_permit.aspx")
            End If

            '
            'sdaeditdetail.Fill(dtbdet)

            If hidpl.Value <> "" Or hidpl.Value <> Nothing Then
                doctp.SelectedValue = llp.lptyp
                txtvendor.Text = llp.lpsupplier
                txtyearx.Text = llp.lpentrydt
                subject.Text = llp.lpsubject
                strsqldet = strsqldet + "'" + hidpl.Value + "'"
                rem_days.SelectedValue = llp.lpremday
                Dim sdaeditdetail As SqlDataAdapter = New SqlDataAdapter(strsqldet, sqlConn)
                dt = New DataTable
                sdaeditdetail.Fill(dt)

                If dt.Rows.Count > 0 Then
                    Session("myDatatable") = dt
                    fillgrid()
                Else
                    dt = New DataTable
                    dt = llp.Fetch()

                    Session("myDatatable") = dt
                    fillgrid()
                End If

                strsch = "select *, (select nama from jrn_test..h_a101 where niksite = docpermit_trans_sch.nik) as Name  from docpermit_trans_sch where id = '" + Session("pid").ToString() + "'"
                Dim sdasch As SqlDataAdapter = New SqlDataAdapter(strsch, sqlConn)
                dtbsch = New DataTable
                sdasch.Fill(dtbsch)

                If dtbsch.Rows.Count > 0 Then
                    'AddDataToTable2(Me.txtUserName.Text.Trim(), Me.txtnik.Text.Trim(), Me.ddlsch.SelectedValue.Trim(), CType(Session("myDatatable2"), DataTable))
                    Session("myDatatable2") = dtbsch
                    Me.GridView2.DataSource = CType(Session("myDatatable2"), DataTable).DefaultView

                    Me.GridView2.DataBind()
                Else
                    myDt2 = New DataTable()
                    myDt2 = CreateDataTable2()
                    Session("myDatatable2") = myDt2
                End If

                hidpl2.Value = Session("pid").ToString()
                hedit.Value = "edit"
                Session("pid") = Nothing


            Else
                If sedit = "new" Then
                    dt = New DataTable
                    dt = llp.Fetch()

                    Session("myDatatable") = dt
                    fillgrid()
                Else
                    If dtbdet.Rows.Count > 0 Then
                        Session("myDatatable") = dtbdet
                        fillgrid()
                    Else
                        dt = New DataTable
                        dt = llp.Fetch()

                        Session("myDatatable") = dt
                        fillgrid()
                    End If
                End If
                myDt2 = New DataTable()
                myDt2 = CreateDataTable2()
                Session("myDatatable2") = myDt2
                hedit.Value = ""
            End If


            txtyearx.Text = Now.Date.ToString("yyyy-MM-dd")
            doctp.SelectedValue = "PMT"
            doctp.Enabled = False

            'Dim txtlnm As TextBox = DirectCast(gvlstPL.FooterRow.FindControl("txtlnm"), TextBox)
            js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn_dt_issuednew', '" + gvlstPL.FooterRow.FindControl("txtdtissuenew").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn_dt_expnew', '" + gvlstPL.FooterRow.FindControl("txtdtexpnew").ClientID + "', '%m/%d/%Y');</script>"
        End If

    End Sub

    Private Sub fillgrid()
        Dim dt As DataTable = Session("myDatatable")

        If dt.Rows.Count > 0 Then
            If dt.Rows(0)!id.ToString = "" Then
                gvlstPL.DataSource = dt
                gvlstPL.DataBind()

                Dim TotalColumns As Integer = gvlstPL.Rows(0).Cells.Count
                gvlstPL.Rows(0).Cells.Clear()
                gvlstPL.Rows(0).Cells.Add(New TableCell())
                gvlstPL.Rows(0).Cells(0).ColumnSpan = TotalColumns
                gvlstPL.Rows(0).Cells(0).Text = "Insert PL"
                gvlstPL.Rows(0).ForeColor = Drawing.Color.White
                Session("myDatatable") = dt
            Else
                gvlstPL.DataSource = dt
                gvlstPL.DataBind()
            End If
        Else
            dt.Rows.Add(dt.NewRow())
            gvlstPL.DataSource = dt
            gvlstPL.DataBind()

            Dim TotalColumns As Integer = gvlstPL.Rows(0).Cells.Count
            gvlstPL.Rows(0).Cells.Clear()
            gvlstPL.Rows(0).Cells.Add(New TableCell())
            gvlstPL.Rows(0).Cells(0).ColumnSpan = TotalColumns
            gvlstPL.Rows(0).Cells(0).Text = "Insert PL"
            gvlstPL.Rows(0).ForeColor = Drawing.Color.White
            Session("myDatatable") = dt
        End If
    End Sub

    Protected Sub gvlstPL_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvlstPL.RowCancelingEdit
        gvlstPL.EditIndex = -1

        fillgridedit()
        

        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn_dt_issuednew', '" + gvlstPL.FooterRow.FindControl("txtdtissuenew").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn_dt_expnew', '" + gvlstPL.FooterRow.FindControl("txtdtexpnew").ClientID + "', '%m/%d/%Y');</script>"


    End Sub

    Protected Sub gvlstPL_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvlstPL.RowEditing
        Dim dt As DataTable = Session("myDatatable")
        gvlstPL.EditIndex = e.NewEditIndex
        Dim index As String = e.NewEditIndex
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

        fillgridedit()

        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn_dt_issuededit', '" + gvlstPL.Rows(index).FindControl("txtdtissuededit").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn_dt_expedit', '" + gvlstPL.Rows(index).FindControl("txtdtexpedit").ClientID + "', '%m/%d/%Y');</script>"

    End Sub

    Protected Sub gvlstPL_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvlstPL.RowUpdating
        Dim txtplnameedit As TextBox = DirectCast(gvlstPL.Rows(e.RowIndex).FindControl("txtplnameedit"), TextBox)
        Dim txtpnoedit As TextBox = DirectCast(gvlstPL.Rows(e.RowIndex).FindControl("txtpnoedit"), TextBox)
        Dim txtdtissuededit As TextBox = DirectCast(gvlstPL.Rows(e.RowIndex).FindControl("txtdtissuededit"), TextBox)
        Dim txtdtexpedit As TextBox = DirectCast(gvlstPL.Rows(e.RowIndex).FindControl("txtdtexpedit"), TextBox)

        'myDt = Session("myDatatable")

        llp.Updatelp(txtplnameedit.Text, txtpnoedit.Text, txtdtissuededit.Text, txtdtexpedit.Text, CType(Session("myDatatable"), DataTable), e.RowIndex)

        gvlstPL.EditIndex = -1
        fillgrid()

        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn_dt_issuednew', '" + gvlstPL.FooterRow.FindControl("txtdtissuenew").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn_dt_expnew', '" + gvlstPL.FooterRow.FindControl("txtdtexpnew").ClientID + "', '%m/%d/%Y');</script>"
    End Sub

    Protected Sub gvlstPL_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvlstPL.RowCommand
        If e.CommandName.Equals("AddNew") Then
            Dim txtlnm As TextBox = DirectCast(gvlstPL.FooterRow.FindControl("txtlnm"), TextBox)
            Dim txtpnonew As TextBox = DirectCast(gvlstPL.FooterRow.FindControl("txtpnonew"), TextBox)
            Dim txtdtissuenew As TextBox = DirectCast(gvlstPL.FooterRow.FindControl("txtdtissuenew"), TextBox)
            Dim txtdtexpnew As TextBox = DirectCast(gvlstPL.FooterRow.FindControl("txtdtexpnew"), TextBox)
            myDt = Session("myDatatable")
            If gvlstPL.Rows(0).Cells(0).Text = "Insert PL" Then
                Dim TotalColumns As Integer = gvlstPL.Rows(0).Cells.Count
                gvlstPL.Rows(0).Cells.Clear()
                gvlstPL.Rows(0).Cells.Add(New TableCell())
                gvlstPL.Rows(0).Cells(0).ColumnSpan = TotalColumns
                gvlstPL.Rows(0).Cells(0).Text = "Insert PL"
                gvlstPL.Rows(0).ForeColor = Drawing.Color.White
            End If
            llp.addlp(txtlnm.Text, txtpnonew.Text, txtdtissuenew.Text, txtdtexpnew.Text, CType(Session("myDatatable"), DataTable))
            myDt = Session("myDatatable")
            fillgrid()
            js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn_dt_issuednew', '" + gvlstPL.FooterRow.FindControl("txtdtissuenew").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn_dt_expnew', '" + gvlstPL.FooterRow.FindControl("txtdtexpnew").ClientID + "', '%m/%d/%Y');</script>"
        End If

    End Sub

    Protected Sub gvlstPL_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvlstPL.RowDeleting

        llp.Delete(CType(Session("myDatatable"), DataTable), e.RowIndex)
        fillgrid()
       
    End Sub

    Private Sub fillgridedit()
        Dim dt As DataTable = Session("myDatatable")
        Dim lscount As Integer

        If dt.Rows.Count > 0 Then
            If dt.Rows(0)!id.ToString = "" Then
                gvlstPL.DataSource = dt
                gvlstPL.DataBind()

                Dim TotalColumns As Integer = gvlstPL.Rows(0).Cells.Count
                gvlstPL.Rows(0).Cells.Clear()
                gvlstPL.Rows(0).Cells.Add(New TableCell())
                gvlstPL.Rows(0).Cells(0).ColumnSpan = TotalColumns
                gvlstPL.Rows(0).Cells(0).Text = "Insert PL"
                gvlstPL.Rows(0).ForeColor = Drawing.Color.White
                Session("myDatatable") = dt
            Else
                For lscount = 0 To dt.Rows.Count - 1

                Next

                gvlstPL.DataSource = dt
                gvlstPL.DataBind()
            End If
        Else
            dt.Rows.Add(dt.NewRow())
            gvlstPL.DataSource = dt
            gvlstPL.DataBind()

            Dim TotalColumns As Integer = gvlstPL.Rows(0).Cells.Count
            gvlstPL.Rows(0).Cells.Clear()
            gvlstPL.Rows(0).Cells.Add(New TableCell())
            gvlstPL.Rows(0).Cells(0).ColumnSpan = TotalColumns
            gvlstPL.Rows(0).Cells(0).Text = "Insert PL"
            gvlstPL.Rows(0).ForeColor = Drawing.Color.White
            Session("myDatatable") = dt
        End If
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsave.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim dtblist As DataTable = New DataTable()
        Dim dtbpl As DataTable = New DataTable()
        dtblist.Merge(Session("mydatatable2"))
        dtbpl.Merge(Session("myDatatable"))
        Dim sqlQuery As String
        Dim cmd As SqlCommand
        Dim lcom As SqlCommand
        Dim dttm As String
        Dim dtmnow As String
        Dim dtynow As String
        Dim stripno As String
        Dim permitid As String
        Dim nmcomp As String
        Dim nmsite As String
        Dim mustreport As String
        Dim i As Integer
        Dim ls_sql As String
        Dim li_count As Integer

        dttm = Date.Now.ToString
        Dim createdin As String
        createdin = HttpContext.Current.Request.UserHostName

        If hedit.Value = "edit" Then
            conn.Open()
            sqlQuery = "update docpermit_lisense_trans set f_name = '" + doctp.SelectedValue + "', fsubject = '" + subject.Text + "', supplier_name='" + txtvendor.Text + "', ModifiedBy='" + Session("otorisasi_permit") + "', ModifiedIn='" + createdin + "', ModifiedTime='" + dttm + "', rem_code= '" + rem_days.SelectedValue + "' where id='" + hidpl2.Value + "'"
            cmd = New SqlCommand(sqlQuery, conn)
            cmd.ExecuteScalar()

            Dim ls_delquery As String = "Delete docpermit_trans_sch where id = '" + hidpl2.Value + "'"
            Dim ls_delcmd As SqlCommand
            ls_delcmd = New SqlCommand(ls_delquery, conn)
            ls_delcmd.ExecuteScalar()

            For li_count = 0 To dtblist.Rows.Count - 1
                ls_sql = "insert into docpermit_trans_sch (id, nik, schedule) values ('" + hidpl2.Value + "', '" + dtblist.Rows(li_count)!Nik.ToString + "', '" + dtblist.Rows(li_count)!Schedule.ToString + "')"
                lcom = New SqlCommand(ls_sql, conn)
                lcom.ExecuteScalar()
            Next

            ls_delquery = "Delete docpermit_lisense_trans_det where id='" + hidpl2.Value + "'"
            ls_delcmd = New SqlCommand(ls_delquery, conn)
            ls_delcmd.ExecuteScalar()

            li_count = 0
            For li_count = 0 To dtbpl.Rows.Count - 1
                If dtbpl.Rows(li_count)!pl_name.ToString() <> "" Then
                    ls_sql = "insert into docpermit_lisense_trans_det (id,no,pl_name,permit_no,dt_issued,dt_expired) values ('" + hidpl2.Value + "', '" + dtbpl.Rows(li_count)!no.ToString() + "', '" + dtbpl.Rows(li_count)!pl_name.ToString() + "', '" + dtbpl.Rows(li_count)!permit_no.ToString() + "', '" + dtbpl.Rows(li_count)!dt_issued.ToString() + "', '" + dtbpl.Rows(li_count)!dt_expired.ToString() + "')"
                    lcom = New SqlCommand(ls_sql, conn)
                    lcom.ExecuteScalar()
                End If
            Next

            msgbox = "<script type ='text/javascript'> alert('Data has been update'); </script>"
            '    Catch ex As Exception
            '    msgbox = "<script type ='text/javascript'> alert('" + ex.ToString() + "'); </script>"
            'Finally
            conn.Close()
            'End Try
        Else
            If dtblist.Rows.Count > 0 Then
                Try
                    msgbox = ""
                    conn.Open()
                    Dim strcon As String = "select id from docpermit_lisense_trans"
                    Dim dtb As DataTable = New DataTable
                    Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, conn)

                    dtmnow = DateTime.Now.ToString("MM")
                    dtynow = Date.Now.Year.ToString
                    stripno = "PL/" + dtynow + dtmnow + "/"

                    sda.Fill(dtb)

                    If dtb.Rows.Count = 0 Then
                        permitid = stripno + "000001"
                    ElseIf dtb.Rows.Count > 0 Then
                        i = 0
                        i = dtb.Rows.Count + 1
                        If dtb.Rows.Count > 0 And dtb.Rows.Count < 9 Then
                            permitid = stripno + "00000" + i.ToString
                        ElseIf dtb.Rows.Count >= 9 And dtb.Rows.Count < 99 Then
                            permitid = stripno + "0000" + i.ToString
                        ElseIf dtb.Rows.Count >= 99 And dtb.Rows.Count < 999 Then
                            permitid = stripno + "000" + i.ToString
                        ElseIf dtb.Rows.Count >= 999 And dtb.Rows.Count < 9999 Then
                            permitid = stripno + "00" + i.ToString
                        ElseIf dtb.Rows.Count >= 9999 And dtb.Rows.Count < 99999 Then
                            permitid = stripno + "0" + i.ToString
                        ElseIf dtb.Rows.Count >= 99999 And dtb.Rows.Count < 999999 Then
                            permitid = stripno + i.ToString
                        ElseIf dtb.Rows.Count >= 999999 Then
                            permitid = "Error on generate Tripnum"
                        End If
                    End If

                    'Dim strcon2 As String = "select Nmcompany from jrn_test.dbo.H_A110 where kdcompany = '" + ddlcomp.SelectedValue + "'"
                    'Dim dtb2 As DataTable = New DataTable
                    'Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, conn)
                    'sda2.Fill(dtb2)

                    'nmcomp = dtb2.Rows(0)!Nmcompany.ToString

                    'Dim strcon3 As String = "select Nmsite from jrn_test.dbo.H_A120 where kdsite = '" + ddlsite.SelectedValue + "'"
                    'Dim dtb3 As DataTable = New DataTable
                    'Dim sda3 As SqlDataAdapter = New SqlDataAdapter(strcon3, conn)
                    'sda3.Fill(dtb3)

                    'nmsite = dtb3.Rows(0)!Nmsite.ToString

                    sqlQuery = "Insert into docpermit_lisense_trans (id,f_name,fsubject, supplier_name,CreatedBy,CreatedIn,CreatedTime,StEdit,rem_code) values ('" + permitid + "' , '" + doctp.SelectedValue + "', '" + subject.Text + "', '" + txtvendor.Text + "', '" + Session("otorisasi_permit") + "', '" + createdin + "', '" + dttm + "', '1', '" + rem_days.SelectedValue + "')"
                    'sqlQuery = "Insert into docpermit_trans (id,ftype,fsubject,kdcom,company_name,kdsite,site_name,fyear,fcategory,doctype,department,subdepartment,number,must_report,cms_start,cms_end, f_path, f_name, stedit) values ('" + permitid + "' , '" + _type + "', '" + _subject + "', '" + _comp + "', '" + nmcomp + "', '" + _site + "', '" + nmsite + "', '" + _year + "', '" + _category + "', '" + _doctype + "', '" + _department + "', '" + _subdept + "', '" + _number + "', '" + _report + "', '" + _dtissue + "', '" + _dtexp + "', 'C:\uploads\" + _fileupload1 + "', '" + _filename + "', '1' )"
                    cmd = New SqlCommand(sqlQuery, conn)
                    cmd.ExecuteScalar()

                    For li_count = 0 To dtblist.Rows.Count - 1
                        ls_sql = "insert into docpermit_trans_sch (id, nik, schedule) values ('" + permitid + "', '" + dtblist.Rows(li_count)!Nik.ToString + "', '" + dtblist.Rows(li_count)!Schedule.ToString + "')"
                        lcom = New SqlCommand(ls_sql, conn)
                        lcom.ExecuteScalar()
                    Next
                    li_count = 0
                    For li_count = 0 To dtbpl.Rows.Count - 1
                        If dtbpl.Rows(li_count)!pl_name.ToString() <> "" Then

                            ls_sql = "insert into docpermit_lisense_trans_det (id,no,pl_name,permit_no,dt_issued,dt_expired) values ('" + permitid + "', '" + dtbpl.Rows(li_count)!no.ToString() + "', '" + dtbpl.Rows(li_count)!pl_name.ToString() + "', '" + dtbpl.Rows(li_count)!permit_no.ToString() + "', '" + dtbpl.Rows(li_count)!dt_issued.ToString() + "', '" + dtbpl.Rows(li_count)!dt_expired.ToString() + "')"
                            lcom = New SqlCommand(ls_sql, conn)
                            lcom.ExecuteScalar()
                        End If

                    Next

                    msgbox = "<script type ='text/javascript'> alert('Data has been saved'); </script>"
                Catch ex As Exception
                    msgbox = "<script type ='text/javascript'> alert('" + ex.ToString() + "'); </script>"
                Finally
                    conn.Close()
                End Try

                If msgbox <> "" Or msgbox <> Nothing Then
                    Response.Redirect("supp_permit_lisense_entry.aspx")
                End If
            Else
                lblTips.Text = "Please select user to associated with this document"
            End If
        End If

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Dim li_count As Integer

        If GridView2.Rows.Count > 0 Then
            For li_count = 0 To GridView2.Rows.Count - 1
                If GridView2.DataKeys(li_count).Value.ToString = txtnik.Text Then
                    lblTips.Text = "User already exists in the list"
                    Exit Sub
                End If
            Next
        End If

        If Me.txtUserName.Text.Trim().Equals("") Then

            Me.lblTips.Text = "You must fill a username."

        Else

            AddDataToTable2(Me.txtUserName.Text.Trim(), Me.txtnik.Text.Trim(), Me.ddlsch.SelectedValue.Trim(), CType(Session("myDatatable2"), DataTable))

            Me.GridView2.DataSource = CType(Session("myDatatable2"), DataTable).DefaultView

            Me.GridView2.DataBind()

            Me.txtUserName.Text = ""
            Me.txtnik.Text = ""
            Me.lblTips.Text = ""
            Me.ddlsch.SelectedValue = ""

        End If
        fillgrid()
        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn_dt_issuednew', '" + gvlstPL.FooterRow.FindControl("txtdtissuenew").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn_dt_expnew', '" + gvlstPL.FooterRow.FindControl("txtdtexpnew").ClientID + "', '%m/%d/%Y');</script>"
    End Sub

    Private Sub AddDataToTable2(ByVal name As String, ByVal nik As String, ByVal Schedule As String, ByVal myTable2 As DataTable)
        Dim row2 As DataRow

        row2 = myTable2.NewRow()

        row2("Name") = name
        row2("Nik") = nik
        row2("Schedule") = Schedule

        myTable2.Rows.Add(row2)

    End Sub

    Private Function CreateDataTable2() As DataTable
        Dim myDataTable2 As DataTable = New DataTable()

        Dim myDataColumn2 As DataColumn

        myDataColumn2 = New DataColumn()
        myDataColumn2.DataType = Type.GetType("System.String")
        myDataColumn2.ColumnName = "Name"
        myDataTable2.Columns.Add(myDataColumn2)

        myDataColumn2 = New DataColumn()
        myDataColumn2.DataType = Type.GetType("System.String")
        myDataColumn2.ColumnName = "Nik"
        myDataTable2.Columns.Add(myDataColumn2)

        myDataColumn2 = New DataColumn()
        myDataColumn2.DataType = Type.GetType("System.String")
        myDataColumn2.ColumnName = "Schedule"
        myDataTable2.Columns.Add(myDataColumn2)

        Return myDataTable2
    End Function

    Protected Sub GridView2_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridView2.RowDeleting
        Dim row As GridViewRow = GridView2.SelectedRow
        'Dim pid As String
        Dim i As Integer

        i = e.RowIndex
        'pid = GridView2.DataKeys(i).Value.ToString

        'GridView2.DeleteRow(i)
        Dim dtblist As DataTable = New DataTable()
        dtblist.Merge(Session("myDatatable2"))
        dtblist.Rows.RemoveAt(i)
        Me.GridView2.DataSource = dtblist
        Session("myDatatable2") = dtblist
        Me.GridView2.DataBind()
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btncancel.Click
        If hedit.Value = "edit" Then
            tny.Text = "Are you sure want to remove this document reminder?"
            yes.Visible = True
            no.Visible = True
            btnsave.Visible = False
            btncancel.Visible = False
            yes.Enabled = True
            no.Enabled = True

            js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn_dt_issuednew', '" + gvlstPL.FooterRow.FindControl("txtdtissuenew").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn_dt_expnew', '" + gvlstPL.FooterRow.FindControl("txtdtexpnew").ClientID + "', '%m/%d/%Y');</script>"

        Else
            Response.Redirect("index.aspx")
        End If
    End Sub

    Protected Sub yes_Click(ByVal sender As Object, ByVal e As EventArgs) Handles yes.Click
        Dim dtbfindemp As DataTable = New DataTable
        Dim cmd As SqlCommand
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Try
            sqlConn.Open()
            Dim sqlQuery As String = "UPDATE docpermit_lisense_trans SET stedit = '2', deleteby = '" + Session("otorisasi_permit") + "', deletetime = '" + DateTime.Now + "' where id = '" + hidpl2.Value + "'"
            cmd = New SqlCommand(sqlQuery, sqlConn)
            cmd.ExecuteScalar()
            msgbox = "<script language='javascript' type='text/javascript'> alert('data has been deleted') </script>"
            Session("docedituser") = ""

            Response.Redirect("permit_list.aspx")
        Catch ex As Exception
            msgbox = "<script language='javascript' type='text/javascript'> alert('Failed to delete data') </script>"
        Finally
            sqlConn.Close()
        End Try
    End Sub

    Protected Sub no_Click(ByVal sender As Object, ByVal e As EventArgs) Handles no.Click
        tny.Text = ""
        yes.Visible = False
        no.Visible = False
        btnsave.Visible = True
        btncancel.Visible = True
    End Sub
End Class