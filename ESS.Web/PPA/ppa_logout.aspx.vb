﻿Public Partial Class ppa_logout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("ppa_permission") = ""
        Session("ppa_otorisasi") = ""
        Session.Clear()
        Session.Abandon()
        Response.Redirect("login_ppa.aspx")
    End Sub

End Class