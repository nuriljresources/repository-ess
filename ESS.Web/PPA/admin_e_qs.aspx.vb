﻿Imports System.Data.SqlClient
Partial Public Class admin_e_qs
    Inherits System.Web.UI.Page
    Public stsedit As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("ppa_otorisasi") = "" Then
            Response.Redirect("login_ppa.aspx")
        End If

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("localdev").ConnectionString)
        Dim qrychkadmin As String = "Select nik, nama, admintype from admin_ppa where nik = '" + Session("ppa_niksite") + "'"
        Dim dtb As DataTable = New DataTable

        Dim sda As SqlDataAdapter = New SqlDataAdapter(qrychkadmin, conn)
        sda.Fill(dtb)

        If dtb.Rows.Count = 0 Then
            Response.Redirect("login_ppa.aspx")
        End If
    End Sub

    Private Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("localdev").ConnectionString)
        Dim sqlQuery As String
        Dim cmd As SqlCommand
        Dim dtb As DataTable = New DataTable()
        Dim newid As String

        If hstsedit.Value = "edit" Then
            Try
                conn.Open()
                sqlQuery = "update ms_questions set header2 = '" + txtheader.Text + "', det1 = '" + txtq1.Text + "', det2 = '" + txtq2.Text + "', det3 = '" + txtq3.Text + "', det4 = '" + txtq4.Text + "', det5 = '" + txtq5.Text + "' where ms_id = '" + ddlqs.SelectedValue + "'"
                cmd = New SqlCommand(sqlQuery, conn)
                cmd.ExecuteScalar()
            Catch ex As Exception
            Finally
                conn.Close()
            End Try
        Else
            Try
                conn.Open()

                Dim strcon As String = "select max(ms_id) as countid from ms_questions"
                Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, conn)
                sda.Fill(dtb)

                If dtb.Rows.Count > 0 Then
                    newid = dtb.Rows(0)!countid + 1
                End If

                sqlQuery = "insert ms_questions (ms_id, header2, det1, det2, det3, det4, det5) values ('" + newid + "','" + txtheader.Text + "','" + txtq1.Text + "','" + txtq2.Text + "','" + txtq3.Text + "', '" + txtq4.Text + "','" + txtq5.Text + "')"
                cmd = New SqlCommand(sqlQuery, conn)
                cmd.ExecuteScalar()
            Catch ex As Exception
            Finally
                conn.Close()
            End Try
        End If

    End Sub

    Private Sub ddlqs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlqs.SelectedIndexChanged
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("localdev").ConnectionString)
        Dim dtb As DataTable = New DataTable()
        Try
            Dim strcon As String = "select ms_id, header1, header2, det1, det2, det3, det4, det5 from ms_questions where ms_id = '" + ddlqs.SelectedValue + "'"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, conn)
            sda.Fill(dtb)

            If dtb.Rows.Count > 0 Then
                txtheader.Text = dtb.Rows(0)!header2
                txtq1.Text = dtb.Rows(0)!det1
                txtq2.Text = dtb.Rows(0)!det2
                txtq3.Text = dtb.Rows(0)!det3
                txtq4.Text = dtb.Rows(0)!det4
                txtq5.Text = dtb.Rows(0)!det5
                hstsedit.Value = "edit"
            End If
        Catch ex As Exception

        Finally
            conn.Close()
        End Try
    End Sub
End Class