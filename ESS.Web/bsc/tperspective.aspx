﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="tperspective.aspx.vb"
   Inherits="EXCELLENT.tperspective" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

   <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>

   <script src="../Scripts/json2.js" type="text/javascript"></script>

   <script type="text/javascript">
      var TABLE_NAME = 'perspective';
      function tes() {
         retrievep();
      }
      function addRowToTable(pid, pname, pbobot, pstate) {
         var tbl = document.getElementById(TABLE_NAME);
         var nextRow = tbl.tBodies[0].rows.length;
         var num = nextRow;
         var row = tbl.tBodies[0].insertRow(num);

         var cell0 = row.insertCell(0);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'nox' + (num + 1));
         txtInp.setAttribute('id', 'nox' + (num + 1));
         txtInp.setAttribute('size', '1');
         txtInp.setAttribute('style', 'border-style:none');
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('value', num + 1);
         cell0.appendChild(txtInp);

         var cell1 = row.insertCell(1);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'pid' + (num + 1));
         txtInp.setAttribute('id', 'pid' + (num + 1));
         txtInp.setAttribute('value', pid);
         cell1.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'pstate' + (num + 1));
         txtInp.setAttribute('id', 'pstate' + (num + 1));
         txtInp.setAttribute('value', pstate);
         cell1.appendChild(txtInp);

         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'pname' + (num + 1));
         txtInp.setAttribute('id', 'pname' + (num + 1));
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('style', 'width:100%;border-style:none');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('value', pname);
         cell1.appendChild(txtInp);

         var cell2 = row.insertCell(2);
         cell2.setAttribute('align', 'center');
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'pbobot' + (num + 1));
         txtInp.setAttribute('id', 'pbobot' + (num + 1));
         txtInp.setAttribute('style', 'width:25%;text-align:right;');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('onkeypress', 'fncInputNumericValuesOnly();');
         txtInp.setAttribute('value', pbobot);
         cell2.appendChild(txtInp);
         cell2.innerHTML += ' %';
      }
      function emptyRow() {
         var tbl = document.getElementById(TABLE_NAME);
         for (var i = tbl.rows.length; i > 1; i--) {
            tbl.deleteRow(i - 1);
         }
      }
      function retrievep() {
         var funcid = '0';
         switch (document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').options[document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').selectedIndex].value) {
            case '4':
               {
                  funcid = document.getElementById('ctl00_ContentPlaceHolder1_KPIDirektorat').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIDirektorat').selectedIndex].value;
                  break;
               }
            case '1':
               {
                  funcid = document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').selectedIndex].value;
                  break;
               }
         }
         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/retrievep",
            data: "{'level':" + document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').options[document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').selectedIndex].value + ",'year':" + document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').selectedIndex].value + ",'funcid':" + funcid + "}",
            dataType: "json",
            success: function(res) {
               MyIssue = new Array();
               MyIssue = res.d;
               emptyRow();
               for (var i = 0; i < MyIssue.length; i++) {
                  addRowToTable(MyIssue[i].Pid, MyIssue[i].Pname, MyIssue[i].Pbobot, MyIssue[i].Pstate);
               }
               document.getElementById("btnSave").style.visibility = "";
            },
            error: function(err) {
               alert(err.responseText);
            }
         });
      }

      function IsNumeric(input) { return (input - 0) == input && input.length > 0; }

      function stripNonNumeric(str) {
         str += '';
         var rgx = /^\d|\.|-$/;
         var out = '';
         for (var i = 0; i < str.length; i++) {
            if (rgx.test(str.charAt(i))) {
               if (!((str.charAt(i) == '.' && out.indexOf('.') != -1) ||
             (str.charAt(i) == '-' && out.length != 0))) {
                  out += str.charAt(i);
               }
            }
         }
         return out;
      }
      
      function save() {
         var totalbobot = 0;
         var tbl = document.getElementById(TABLE_NAME);
         for (var i = 1; i < tbl.rows.length; i++) {
            if (IsNumeric(stripNonNumeric(document.getElementById("pbobot" + i).value))) {
               totalbobot += parseFloat(document.getElementById("pbobot" + i).value);
            } else {
               alert('Bobot baris ke ' + i + ' belum diisi');
               return false;
            }
         }
         if (parseFloat(totalbobot) < 100 || parseFloat(totalbobot) > 100) {
            alert("Total bobot harus 100"); return;
         }

         MyArray = new Array();
         for (var i = 1; i < tbl.rows.length; i++) {
            MyArray[MyArray.length] = new perspective(
             document.getElementById("pid" + i).value,
             document.getElementById("pname" + i).value,
             document.getElementById("pbobot" + i).value,
             document.getElementById("pstate" + i).value
             )

         }
         var level = document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').options[document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').selectedIndex].value;
         var year = document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').selectedIndex].value;
         var funcid = 0;
         switch (document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').options[document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').selectedIndex].value) {
            case '4':
               {
                  funcid = document.getElementById('ctl00_ContentPlaceHolder1_KPIDirektorat').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIDirektorat').selectedIndex].value;
                  break;
               }
            case '1':
               {
                  funcid = document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').selectedIndex].value;
                  break;
               }
         }

         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/savep",
            data: "{'level':" + level + ",'year':" + year + ",'funcid':" + funcid + ",'p':" + JSON.stringify(MyArray) + "}",
            dataType: "json",
            success: function(res) {
               if (res.d == '0') {
                  retrievep();
                  document.getElementById("ctl00_ContentPlaceHolder1_lblMessage").innerText = "**Save Berhasil";
               } else {
                  alert(res.d);
                  document.getElementById("ctl00_ContentPlaceHolder1_lblMessage").innerText = "**Save Gagal";
               }
            },
            error: function(err) {
               alert(err.responseText);
            }
         });
      }

      function perspective(pid, pname, pbobot, pstate) {
         this.pid = pid; this.pname = pname; this.pbobot = pbobot; this.pstate = pstate;
      }

      function fncInputNumericValuesOnly() { if (!(event.keyCode == 46 || event.keyCode == 48 || event.keyCode == 49 || event.keyCode == 50 || event.keyCode == 51 || event.keyCode == 52 || event.keyCode == 53 || event.keyCode == 54 || event.keyCode == 55 || event.keyCode == 56 || event.keyCode == 57)) { event.returnValue = false; } }

   </script>

   <style type="text/css">
      body
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         font-size: 12px;
      }
      td.styleHeader
      {
         background-color: #00CC00;
         color: white;
         font-weight: bold;
         font-size: medium;
      }
      table.act
      {
         border-width: 1px;
         border-spacing: 0px;
         border-color: #C0C0C0;
         border-collapse: collapse;
         background-color: white;
      }
      table.act thead
      {
         border-width: 1px;
         padding: 1px;
         border-color: #C0C0C0;
      }
      table.act td
      {
         border-width: 1px;
         padding: 1px;
         border-color: #C0C0C0;
      }
      input.plain
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }
      .plain:hover
      {
         background-color: #FFFFA8;
      }
      select.plains
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }
      ul.dropdown *.dir1
      {
         padding-right: 20px;
         background-image: url("/remo/css/dropdown/themes/default/images/nav-arrow-down.png");
         background-position: 100% 50%;
         background-repeat: no-repeat;
         background-color: #FFA500;
      }
   </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <table class="act" border="1" width="50%">
      <tr>
         <td colspan="2">
            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
         </td>
      </tr>
      <tr>
         <td colspan="2" class="styleHeader">
            Setup Perspective
         </td>
      </tr>
      <tr>
         <td>
            KPI Level
         </td>
         <td>
            <asp:DropDownList ID="KPILevel" runat="server" AutoPostBack="true" CssClass="plains">
               <asp:ListItem Value="0" Text="JRN"></asp:ListItem>
               <asp:ListItem Value="1" Text="Function"></asp:ListItem>
               <asp:ListItem Value="2" Text="Section"></asp:ListItem>
               <asp:ListItem Value="3" Text="Jobsite"></asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="KPIDirektorat" runat="server" CssClass="plains" OnChange="tes()">
            </asp:DropDownList>
            <asp:DropDownList ID="KPIFunc" runat="server" CssClass="plains" OnChange="tes()"
               Visible="False">
            </asp:DropDownList>
            <asp:DropDownList ID="KPISection" runat="server" Visible="False">
            </asp:DropDownList>
            <asp:DropDownList ID="KPIJobsite" runat="server" Visible="False">
            </asp:DropDownList>
         </td>
      </tr>
      <tr>
         <td>
            Tahun
         </td>
         <td>
            <asp:DropDownList ID="ddlYear" runat="server" OnChange="tes()" CssClass="plains">
               <asp:ListItem>2010</asp:ListItem>
               <asp:ListItem>2011</asp:ListItem>
               <asp:ListItem>2012</asp:ListItem>
            </asp:DropDownList>
         </td>
      </tr>
      <tr>
         <td colspan="2">
            <table id="perspective" style="width: 100%">
               <thead>
                  <tr>
                     <th style="width: 2%">
                        No.
                     </th>
                     <th style="width: 75%">
                        Nama Perspective
                     </th>
                     <th style="width: 23%">
                        Bobot
                     </th>
                  </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
         </td>
      </tr>
      <tr>
         <td colspan="2" align="right">
            <input type="button" id="btnSave" style="visibility: hidden; background-image: url(../images/save-icon-small.png);
               background-repeat: no-repeat; cursor: hand; width: 55px; height: 55px; background-color: transparent;
               border: 0" onclick="save();" />
         </td>
      </tr>
   </table>

   <script type="text/javascript">
      retrievep();
   </script>

</asp:Content>
