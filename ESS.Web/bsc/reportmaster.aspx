﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/site.master"  CodeBehind="reportmaster.aspx.vb" Inherits="EXCELLENT.reportmaster" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <title></title>
    <style type="text/css">
       ul.dropdown *.dir1 {
          padding-right: 20px;
          background-image: url("/remo/css/dropdown/themes/default/images/nav-arrow-down.png");
          background-position: 100% 50%;
          background-repeat: no-repeat;
          background-color: #FFA500;
         }
          input.btn{
        color:#050;
        font: bold 90% 'trebuchet ms',helvetica,sans-serif;
        background-color:#fed;
        border: 1px solid;
        border-color: #696 #363 #363 #696;
        filter:progid:DXImageTransform.Microsoft.Gradient
        (GradientType=0,StartColorStr='#ffffffff',EndColorStr='#ffeeddaa');
        padding:0 10px 0 10px;
        cursor:hand;
       }
        select.plains{
         font-family:"trebuchet MS", verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }  
    </style> 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePartialRendering="false">
        </asp:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
           <div>
          <table>
             <tr>
                     <td>
                        Tahun KPI
                     </td>
                     <td colspan="3">
                        <asp:DropDownList ID="KPIYear" runat="server" CssClass="plains">
                           <asp:ListItem Value="2011" Text="2011"></asp:ListItem>
                           <asp:ListItem Value="2012" Text="2012"></asp:ListItem>
                           <asp:ListItem Value="2013" Text="2013"></asp:ListItem>
                           <asp:ListItem Value="2014" Text="2014"></asp:ListItem>
                           <asp:ListItem Value="2015" Text="2015"></asp:ListItem>
                        </asp:DropDownList>
                     </td>
                  </tr>
               <tr>
               <td>
                  KPI Level
               </td>
               <td>
                  <asp:DropDownList ID="KPILevel" runat="server" AutoPostBack="true" CssClass="plains">
                        <asp:ListItem Value="0" Text="JRN"></asp:ListItem>
                        <asp:ListItem Value="4" Text="Direktorat"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Function"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Section"></asp:ListItem>
                        <asp:ListItem Value="3" Text="Jobsite"></asp:ListItem>
                  </asp:DropDownList>
                  <asp:DropDownList ID="KPIDirektorat1" runat="server" CssClass="plains">
                  </asp:DropDownList>
                  <asp:DropDownList ID="KPIFunc" runat="server" Visible="False" CssClass="plains">                                                     
                  </asp:DropDownList>
                  <asp:DropDownList ID="KPISection" runat="server" Visible="False" CssClass="plains">                                                     
                  </asp:DropDownList>
                  <asp:DropDownList ID="KPIJobsite" runat="server" Visible="False" CssClass="plains">                                                     
                  </asp:DropDownList>
               </td>
               <td>
                  <asp:Button ID="btnView" runat="server" Text="View" CssClass="btn"/>
               </td>
            </tr>
          </table>

          <asp:Label ID="lblMessage" runat="server" Font-Names="Arial" Font-Size="8pt" 
             ForeColor="#FF3300"></asp:Label>
          <br />       
          <rsweb:ReportViewer ID="rpt" runat="server" Height="600px" Width="100%">
          </rsweb:ReportViewer>       
         </div>
        </ContentTemplate>
        </asp:UpdatePanel>
        
</asp:Content>
