﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="trip_list.aspx.vb" Inherits="EXCELLENT.trip_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Business Trip List
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<%--<script src="Scripts/tcal.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/tcal.css"/> --%>
<script type="text/javascript" >
    function frmcetak() {
        var d = document;
        window.location = "business_trip_list.aspx?mth=" + '<%=imonth %>' + "&yr=" + '<%=th %>'
    }
</script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
<caption style="text-align: center; font-size: 1.5em; color:White;">Business Trip List</caption>
<tr>
<td style="width:5%;">Periode</td>
<td style="width:8%;">
<asp:DropDownList ID="ddlbulan" runat="server"></asp:DropDownList>
<asp:TextBox ID="tripfrom" style="width:70px;" runat="server" width="90px" ReadOnly="true" Visible ="false"></asp:TextBox></td>
<td style="width:5%;"><asp:DropDownList ID="ddltahun" runat="server"></asp:DropDownList></td>
<td style="width:5%;"><asp:Button ID="btnSearch" runat="server" Text="Search"/></td>
<td style="width:5%;"><input type="button" id="cetak" value="Print" onclick="frmcetak()" style="width: 80px; font-family: Calibri; " /></td>
<td>
 <asp:TextBox ID="tripto" style="width:70px;" runat="server" ReadOnly="true" visible="false"></asp:TextBox>
</td>
<td></td>
</tr></table>


<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="tripno" AllowPaging="true" PageSize="100" Width="1200px">
    <RowStyle Font-Size="Smaller" />
<Columns>
<asp:CommandField ShowSelectButton="True" SelectText="Select" ButtonType="Button"  
        HeaderStyle-Width="40px" ItemStyle-VerticalAlign="top" >
<HeaderStyle Width="40px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:CommandField>
            <asp:BoundField DataField="tripno" HeaderText="Trip Number" 
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="niksite" HeaderText="NIK" 
        HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="90px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
     <asp:BoundField DataField="nama" HeaderText="Nama" 
        HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="200px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="superior" HeaderText="Supervisor" 
        HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="200px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="detailtrip" HeaderText="Trip Type" 
        HeaderStyle-Width="120px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="120 px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="purpose" HeaderText="Purpose" 
        HeaderStyle-Width="300px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="300px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="tripfrom" HeaderText="Trip From" 
        HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="90px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="tripto" HeaderText="Trip To" 
        HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="90px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="approve" HeaderText="Approve By" 
        HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="200px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="apprdate" HeaderText="Approve Date" 
        HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="90px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    
<asp:BoundField DataField="fstatus" HeaderText="Status" 
        HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="90px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>   
<asp:BoundField DataField="editga" HeaderText="GA Status" 
        HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top" ItemStyle-HorizontalAlign="Center" >
<HeaderStyle Width="90px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>  
    
    
    <asp:BoundField DataField="NxtAction" HeaderText="Next Action" 
        HeaderStyle-Width="70px"  ItemStyle-VerticalAlign="top">
        <HeaderStyle Width="70px"></HeaderStyle>

        <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    
    <asp:BoundField DataField="picUserRole" HeaderText="PIC User Role" 
        HeaderStyle-Width="70px"  ItemStyle-VerticalAlign="top">
        <HeaderStyle Width="70px"></HeaderStyle>

        <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    
    <asp:HyperLinkField DataNavigateUrlFields="tripno" ControlStyle-CssClass="PopUpViewHistory"
                DataNavigateUrlFormatString="trip_history.aspx?no={0}" 
                Text="View History" />


                
</Columns> 
    <SelectedRowStyle BackColor="#99CCFF" />
    <HeaderStyle BackColor="#0099FF" />
</asp:GridView>


<script>

    $('a.PopUpViewHistory').click(function() {


        newwindow = window.open($(this).attr('href'), "History Work Flow", 'height=300,width=800');
        if (window.focus) { newwindow.focus() }
        return false
        ;
    });
    
</script>

</asp:Content>