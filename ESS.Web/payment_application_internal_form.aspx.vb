﻿Imports Microsoft.Reporting.WebForms

Partial Public Class payment_application_internal_form
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Session("payment_usage") = Nothing And Session("kdsite") = "JKT" Then
            Dim payment_usage() As String = Session("payment_usage").ToString().Split("|")
            Dim PaymentNo As String = payment_usage(0)
            Dim Amount As String = payment_usage(1)
            Dim PaymentInternalType As String = payment_usage(2)
            Dim DateDocument As String = payment_usage(3)
            Dim ToPerson As String = payment_usage(4)

            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Laporan/rpt_payment_application_internal.rdlc")

            Dim user As ESS.DataAccess.EmployeeModel = ESS.DataAccess.UserLogic.GetUserByNIK(ToPerson)

            ' Two DataTables.
            Dim table1 As DataTable = ReportLogic.GetPaymentApplicationUsage(ToPerson, PaymentNo, PaymentInternalType, Amount, DateDocument, user.Nama)

            ' Create a DataSet. Put both tables in it.
            Dim set1 As DataSet = New DataSet("PaymentApplicationInternal")
            set1.Tables.Add(table1)

            Dim datasource As New ReportDataSource("DataSetReport_PaymentApplicationInternal", set1.Tables(0))
            ReportViewer1.LocalReport.DataSources.Clear()
            ReportViewer1.LocalReport.DataSources.Add(datasource)


        End If

    End Sub

End Class