<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="SMeeting.aspx.vb" Inherits="EXCELLENT.SMeeting" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/calendar-c.css"/> 
<script src="Scripts/calendar_us.js" type="text/javascript"></script>
    <script type="text/javascript">
            function OpenPrint(id) {
                window.open("PrintNotulen.aspx?id=" + id, "List", "location=0,menubar=0,scrollbars=yes,resizable=Yes,toolbar=no,width=800,height=600");
                return false;
            }

            function cekTanggal() {
                var d = document;
                var tg1 = d.getElementById("ctl00_ContentPlaceHolder1_TglAwal").value;
                var tg2 = d.getElementById("ctl00_ContentPlaceHolder1_TglAkhir").value;

                var d11 = tg1.substring(0, 2);
                var d12 = tg1.substring(3, 5);
                var d13 = tg1.substring(6,10);

                var d21 = tg2.substring(0, 2);
                var d22 = tg2.substring(3, 5); 
                var d23 = tg2.substring(6,10);
                
                if (parseFloat(d23) < parseFloat(d13)) {
                    alert("Date end must not smaller than date start ...!");
                    return false;
                }
                if (parseFloat(d23) == parseFloat(d13)){
                    if (parseFloat(d22) < parseFloat(d12)){
                        alert("Date end must not smaller than date start ...!");
                        return false;
                    }
                }
                if (parseFloat(d23) == parseFloat(d13)) {
                    if (parseFloat(d22) == parseFloat(d12)) {
                        if (parseFloat(d21) < parseFloat(d11)) {
                            alert("Date end must not smaller than date start ...!");
                            return false;
                        }
                    }
                }
            }
    </script>
    <style type="text/css">
        ul#navigation li a.m2
        {background-color:#00CC00;color: #000000;}
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ajaxtoolkit:toolkitscriptmanager  ID="ToolkitScriptManager1" runat="server" ScriptMode="Release"/>
 
  <% If Session("Permission") Is Nothing Then
               Response.Redirect("login.aspx")
     ElseIf ("S,1,2,7,8").IndexOf(Session("Permission").ToString()) > -1 Then
               %>
               <%--<table class="menu-table">--%>
               <%--<table cellspacing="2">
               <tr><td style="border-style: solid; border-width: 1px; background-color: #FFFF66"><a href="Meeting.aspx">Create Meeting</a></td>
               <td style="border-style: solid; border-width: 1px; background-color: #FFFF66"><a href="SMeeting.aspx">Search Meeting</a></td></tr>
         </table>
            <% Else %>
                  <%--<table class="menu-table">
                  <table cellspacing="2">
                  <tr>
                     <td style="border-style: solid; border-width: 1px; background-color: #FFFF66"><a href="SMeeting.aspx">Search Meeting</a></td>
                  </tr>
                </table>--%>
               <%End If%>
               
  <table width="100%" border="0" cellpadding="2" cellspacing="2" class="data-table">
       <caption style="text-align:center;font-size:1.5em">
       FIND FORM MEETING
       </caption>
       <tr>
         <td>Date Meeting </td>
         <td style="width:38%">
         <asp:TextBox ID="TglAwal" Name="TglAwal" runat="server" class="input-box-slim" ReadOnly="true" Width="80px"/>
    <script type="text/javascript">
      new tcal({
          // form name
          'formname': 'aspnetForm',
          // input name
          'controlname': 'ctl00$ContentPlaceHolder1$TglAwal'
      });
	                  </script>
           &nbsp; <span style="font-size: x-small;">S.D.</span>&nbsp;
            <asp:TextBox ID="TglAkhir" Name="TglAkhir" runat="server" class="input-box-slim" ReadOnly="true" Width="80px"/>
          <script type="text/javascript">
	                     new tcal ({
		                     // form name
	                     'formname': 'aspnetForm',
		                     // input name
	                     'controlname': 'ctl00$ContentPlaceHolder1$TglAkhir'
	                     });
	                  </script>
           </td><td>Jenis Meeting</td><td>
            <asp:DropDownList ID="ddlJenis" runat="server">
              <%-- <asp:ListItem Selected="True" Value="0">--Semua Meeting--</asp:ListItem>
               <asp:ListItem Value="1">Weekly Review Jobsite</asp:ListItem>
               <asp:ListItem Value="2">Monthly Review Jobsite</asp:ListItem>
               <asp:ListItem Value="3">Weekly Internal Functional</asp:ListItem>
               <asp:ListItem Value="4">Weekly Management Meeting</asp:ListItem>
               <asp:ListItem Value="5">Monthly Management Meeting</asp:ListItem>
               <asp:ListItem Value="6">Others</asp:ListItem>--%>
             </asp:DropDownList>
            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClientClick="cekTanggal()"/>
            <asp:Button ID="btnPrint" runat="server" Text="Print" PostBackUrl="~/PrintLogNotulen.aspx"/>
         </td>
       </tr>
       <tr>
       <td colspan="4">
       <asp:UpdatePanel id="MyUpdatePanel" runat="server">
        <ContentTemplate>
          <asp:Repeater ID="rptSearchResult" runat="server">
            <HeaderTemplate>
                           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table-inside">
                           <tr>               
                                <th>No.</th>
                                <th>No Meeting</th>
                                <th>Title Meeting</th>
                                <th>Type Meeting</th>
                                <th>Site</th>
                                <th>Department</th>
                                <th>Date</th>
                                <th>Chairman</th>
                                <th>Note taker</th>
                                <th>Is Mailed?</th> 
                                <th>Edit</th>      
                                <th>View</th>          
                           </tr>
                            </HeaderTemplate>  
                            <ItemTemplate>
                           <tr>
                            <td><%# Container.ItemIndex + 1 %></td>
                            <td><%#DataBinder.Eval(Container.DataItem, "IdRap")%></td>
                            <td><%#DataBinder.Eval(Container.DataItem, "NmRap")%></td>
                            <td align="center"><%#DataBinder.Eval(Container.DataItem, "JenisRap")%></td>
                            <td align="center"><%#DataBinder.Eval(Container.DataItem, "kdCabang")%></td>
                            <td align="center"><%#DataBinder.Eval(Container.DataItem, "Abbr")%></td>
                            <td style="text-align:center"><%#FormatDate(DataBinder.Eval(Container.DataItem, "TglRap"))%></td>
                            <td align="center"><%#DataBinder.Eval(Container.DataItem, "NmPimpinan")%></td>
                            <td align="center"><%#DataBinder.Eval(Container.DataItem, "NmNotulis")%></td>
                            <td align="center"><%#DataBinder.Eval(Container.DataItem, "Mail")%></td>
                            <%--<td align="center"><ASP:Button ID="btnDelete" Text="X" CommandName="Delete" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "IdRap")%>'/></td>--%>
                            <td align="center"><ASP:Button ID="btnEdit" Text="E" CommandName="Edit" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "IdRap")%>'/></td>
                            <td align="center"><input type="button" id="btnPrint" value="V" onclick = "OpenPrint('<%# DataBinder.Eval(Container.DataItem, "IdRap")%>')" /></td>
                            </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                            </table>
                            </FooterTemplate>
          </asp:Repeater>
       </td>
       </tr>
       <tr>
       <td colspan="4">
           <div id="log" runat="server" style="color:red;font:Trebechuet;"></div>
       </td>
       </tr>
      </table>

     </ContentTemplate>
     <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" /> 
                </Triggers> 

    </asp:UpdatePanel>
 <asp:UpdateProgress ID="progress1" runat="server" DisplayAfter="50">    
                  <ProgressTemplate>
                  <asp:Panel ID="pnlProgressBar" runat="server">    
                     <div><asp:Image ID="Image1" runat="Server" ImageUrl="images/ajax-loader.gif" 
                             Height="28px"/>Please Wait...</div>    
                  </asp:Panel>   
                  </ProgressTemplate> 
                  </asp:UpdateProgress>
</asp:Content>