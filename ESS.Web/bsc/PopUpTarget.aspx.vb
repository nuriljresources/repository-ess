﻿Imports System.Data.SqlClient

Partial Public Class PopUpTarget
    Inherits System.Web.UI.Page

    Dim levelkpi As Integer = 0
    Dim section As String = ""
    Dim adadata As Boolean = True
    Dim calctype As String

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache)
        Response.Cache.SetNoStore()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Not Page.IsPostBack Then
        '    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        '    Try
        '        For Each x As ListItem In cmbYear.Items
        '            If x.Text = Date.Now.Year Then
        '                x.Selected = True
        '            End If
        '        Next
        '        Dim query As String = ""
        '        Dim dt As New DataTable()
        '        Dim da As SqlDataAdapter
        '        If Not Page.IsPostBack Then
        '            txtUoM.Text = Request.QueryString("uom")
        '            query = "select * from TTarget where YEAR='" & cmbYear.SelectedValue & "' and kpiid='" & Request.QueryString("kpiid") & "' order by versi desc"
        '            da = New SqlDataAdapter(query, conn)
        '            conn.Open()
        '            da.Fill(dt)
        '            conn.Close()
        '            If dt.Rows.Count = 0 Then
        '                'No Data Found
        '                txtVersion.Text = "1"
        '                txtVersion.Enabled = False
        '                revised.Visible = False
        '            Else
        '                txtVersion.Text = CInt(dt.Rows(0)("versi").ToString()) + 1
        '                txtVersion.Enabled = False
        '                cmbYTD.SelectedValue = dt.Rows(0)("calctype")
        '                txtJan.Text = dt.Rows(0)("jan")
        '                txtJan.Enabled = False
        '                txtFeb.Text = dt.Rows(0)("feb")
        '                txtFeb.Enabled = False
        '                txtMar.Text = dt.Rows(0)("mar")
        '                txtMar.Enabled = False
        '                txtApr.Text = dt.Rows(0)("apr")
        '                txtApr.Enabled = False
        '                txtMei.Text = dt.Rows(0)("mei")
        '                txtMei.Enabled = False
        '                txtJun.Text = dt.Rows(0)("jun")
        '                txtJun.Enabled = False
        '                txtJul.Text = dt.Rows(0)("jul")
        '                txtJul.Enabled = False
        '                txtAgu.Text = dt.Rows(0)("agu")
        '                txtAgu.Enabled = False
        '                txtSep.Text = dt.Rows(0)("sep")
        '                txtSep.Enabled = False
        '                txtOkt.Text = dt.Rows(0)("okt")
        '                txtOkt.Enabled = False
        '                txtNov.Text = dt.Rows(0)("nov")
        '                txtNov.Enabled = False
        '                txtDes.Text = dt.Rows(0)("des")
        '                txtDes.Enabled = False
        '                ReCalc()
        '                revised.Visible = True
        '            End If
        '        End If
        '    Catch ex As Exception
        '        lblMessage.Text = ex.Message
        '    Finally
        '        If conn.State = ConnectionState.Open Then
        '            conn.Close()
        '        End If
        '    End Try
        'End If
        If Session("permission") = "" Or Session("permission") = Nothing Then
            Response.Redirect("../Login.aspx", False)
            Exit Sub
        ElseIf ("1;2;7;8;5;A").IndexOf(Session("permission")) = -1 Then 'yang bisa akses cuman mdv,sekpro,shead,picpdca, SH Admin
            Response.Redirect("../default.aspx", False)
            Exit Sub
        End If

        permission.Value = Session("permission")
        If Not Page.IsPostBack Then
            kpiid.Value = Request.QueryString("kpiid")
            createdby.Value = Session("NikUser")
            createdin.Value = HttpContext.Current.Request.UserHostAddress
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Try
                For Each x As ListItem In cmbYear.Items
                    If x.Text = Date.Now.Year Then
                        x.Selected = True
                    End If
                Next
                Dim query As String = ""
                Dim dr As SqlDataReader
                Dim cmd As SqlCommand
                Dim count As Integer = 1
                Dim selected As Integer = 0
                Dim annualtarget As Decimal = 0
                txtUoM.Text = Request.QueryString("uom")
                lblJudul.Text = Request.QueryString("addon")
                conn.Open()
                If Request.QueryString("level") = "2" Then
                    query = "select a.Year,a.Name,b.SectionName,c.kdsite,isnull(a.calctype,'0') as calctype  from MKPI a left join MSECTION b on a.Section=b.ID left join SITE c on a.jobsite=c.id where a.id='" & Request.QueryString("kpiid") & "'"
                    cmd = New SqlCommand(query, conn)
                    dr = cmd.ExecuteReader
                    If dr.HasRows Then
                        While dr.Read
                            lblJudul.Text = "[" & dr("kdsite") & " section " + dr("sectionname") & "] " & dr("Name")
                            calctype = dr("calctype")
                            cmbYear.SelectedValue = dr("Year")
                        End While
                    End If
                    dr.Close()
                    dr = Nothing
                End If
                query = "select year,isnull(calctype,0) as calctype from mkpi where id='" & Request.QueryString("kpiid") & "'"
                cmd = New SqlCommand(query, conn)
                dr = cmd.ExecuteReader
                If dr.HasRows Then
                    While dr.Read
                        calctype = dr("calctype")
                        cmbYear.SelectedValue = dr("Year")
                    End While
                End If
                dr.Close()
                dr = Nothing
                query = "select * from TTarget where YEAR='" & cmbYear.SelectedValue & "' and kpiid='" & Request.QueryString("kpiid") & "' order by versi asc"
                cmd = New SqlCommand(query, conn)
                dr = cmd.ExecuteReader
                result.InnerHtml += "<table border=""1"" class=""act"">"
                result.InnerHtml += "<tr style=""background-color:#00CC00;color:black;font-weight:bold;""><td style=""background-color:#93CDDD"">Version</td><td style=""background-color:#93CDDD"">YTD Target</td><td align=""center"">JAN</td><td align=""center"">FEB</td><td align=""center"">MAR</td> " & _
                                      "<td align=""center"">APR</td> " & _
                                      "<td align=""center"">MEI</td> " & _
                                      "<td align=""center"">JUN</td> " & _
                                      "<td align=""center"">JUL</td> " & _
                                      "<td align=""center"">AGU</td> " & _
                                      "<td align=""center"">SEP</td> " & _
                                      "<td align=""center"">OKT</td> " & _
                                      "<td align=""center"">NOV</td> " & _
                                      "<td align=""center"">DES</td> " & _
                                      "<td align=""center"">Annual Target</td> "
                If dr.HasRows Then
                    While dr.Read
                        result.InnerHtml = result.InnerHtml.Replace("#$#", "")
                        result.InnerHtml += "<tr>"
                        result.InnerHtml += "<td style=""background-color:#DBEEF3"">#$# " & dr("Versi") & "</td>"
                        result.InnerHtml += "<td style=""background-color:#DBEEF3"">" & GetCalcType(dr("CalcType")) & "</td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("JAN") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("FEB") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("MAR") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("APR") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("MEI") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("JUN") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("JUL") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("AGU") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("SEP") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("OKT") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("NOV") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("DES") & """ class=""plain"" READONLY/></td>"
                        selected = dr("CalcType")
                        annualtarget = 0
                        Select Case dr("CalcType")
                            Case "0" 'SUM
                                result.InnerHtml += "<td>" & Format(CDec(IIf(dr("JAN") = "", 0, dr("JAN"))) + CDec(IIf(dr("FEB") = "", 0, dr("FEB"))) + CDec(IIf(dr("MAR") = "", 0, dr("MAR"))) + CDec(IIf(dr("APR") = "", 0, dr("APR"))) + CDec(IIf(dr("MEI") = "", 0, dr("MEI"))) + CDec(IIf(dr("JUN") = "", 0, dr("JUN"))) + CDec(IIf(dr("JUL") = "", 0, dr("JUL"))) + CDec(IIf(dr("AGU") = "", 0, dr("AGU"))) + CDec(IIf(dr("SEP") = "", 0, dr("SEP"))) + CDec(IIf(dr("OKT") = "", 0, dr("OKT"))) + CDec(IIf(dr("NOV") = "", 0, dr("NOV"))) + CDec(IIf(dr("DES") = "", 0, dr("DES"))), "#,##0.0") & "</td>"
                                annualtarget = CDec(IIf(dr("JAN") = "", 0, dr("JAN"))) + CDec(IIf(dr("FEB") = "", 0, dr("FEB"))) + CDec(IIf(dr("MAR") = "", 0, dr("MAR"))) + CDec(IIf(dr("APR") = "", 0, dr("APR"))) + CDec(IIf(dr("MEI") = "", 0, dr("MEI"))) + CDec(IIf(dr("JUN") = "", 0, dr("JUN"))) + CDec(IIf(dr("JUL") = "", 0, dr("JUL"))) + CDec(IIf(dr("AGU") = "", 0, dr("AGU"))) + CDec(IIf(dr("SEP") = "", 0, dr("SEP"))) + CDec(IIf(dr("OKT") = "", 0, dr("OKT"))) + CDec(IIf(dr("NOV") = "", 0, dr("NOV"))) + CDec(IIf(dr("DES") = "", 0, dr("DES")))
                            Case "1" 'AVG
                                Dim myresult As Decimal
                                Dim divider As Decimal = 12
                                myresult = CDec(IIf(dr("JAN") = "", 0, dr("JAN"))) + CDec(IIf(dr("FEB") = "", 0, dr("FEB"))) + CDec(IIf(dr("MAR") = "", 0, dr("MAR"))) + CDec(IIf(dr("APR") = "", 0, dr("APR"))) + CDec(IIf(dr("MEI") = "", 0, dr("MEI"))) + CDec(IIf(dr("JUN") = "", 0, dr("JUN"))) + CDec(IIf(dr("JUL") = "", 0, dr("JUL"))) + CDec(IIf(dr("AGU") = "", 0, dr("AGU"))) + CDec(IIf(dr("SEP") = "", 0, dr("SEP"))) + CDec(IIf(dr("OKT") = "", 0, dr("OKT"))) + CDec(IIf(dr("NOV") = "", 0, dr("NOV"))) + CDec(IIf(dr("DES") = "", 0, dr("DES")))
                                If dr("JAN") = "" Then divider = divider - 1
                                If dr("FEB") = "" Then divider = divider - 1
                                If dr("MAR") = "" Then divider = divider - 1
                                If dr("APR") = "" Then divider = divider - 1
                                If dr("MEI") = "" Then divider = divider - 1
                                If dr("JUN") = "" Then divider = divider - 1
                                If dr("JUL") = "" Then divider = divider - 1
                                If dr("AGU") = "" Then divider = divider - 1
                                If dr("SEP") = "" Then divider = divider - 1
                                If dr("OKT") = "" Then divider = divider - 1
                                If dr("NOV") = "" Then divider = divider - 1
                                If dr("DES") = "" Then divider = divider - 1
                                result.InnerHtml += "<td>" & Format(myresult / divider, "#,##0.0") & "</td>"
                                annualtarget = CDec(myresult / divider)
                            Case "2" 'LAST VAL
                                result.InnerHtml += "<td>" & Format(CDec(dr("annual")), "#,##0.0") & "</td>"
                                annualtarget = CDec(dr("annual"))
                            Case "3" 'OTHERS
                                If IsDBNull(dr("annual")) Then
                                    result.InnerHtml += "<td>0</td>"
                                    annualtarget = 0
                                Else
                                    result.InnerHtml += "<td>" & Format(CDec(dr("annual")), "#,##0.0") & "</td>"
                                    annualtarget = CDec(dr("annual"))
                                End If
                        End Select
                        count += 1
                        pasteinput.Value = dr("JAN") & "|" & dr("FEB") & "|" & dr("MAR") & "|" & dr("APR") & "|" & dr("MEI") & "|" & dr("JUN") & "|" & dr("JUL") & "|" & dr("AGU") & "|" & dr("SEP") & "|" & dr("OKT") & "|" & dr("NOV") & "|" & dr("DES")
                        pasteinput.Value = dr("JAN") & vbTab & dr("FEB") & vbTab & dr("MAR") & vbTab & dr("APR") & vbTab & dr("MEI") & vbTab & dr("JUN") & vbTab & dr("JUL") & vbTab & dr("AGU") & vbTab & dr("SEP") & vbTab & dr("OKT") & vbTab & dr("NOV") & vbTab & dr("DES")
                    End While
                Else
                    adadata = False
                End If

                dr.Close()
                dr = Nothing

                query = "select isnull(level,55) as level,isnull(section,55) as section from MKPI where id='" & Request.QueryString("kpiid") & "'"
                cmd = New SqlCommand(query, conn)


                dr = cmd.ExecuteReader
                If dr.HasRows Then
                    While dr.Read
                        levelkpi = dr("level")
                        section = dr("section")
                    End While
                End If
                If (Session("permission") = "2" And levelkpi = 2 And section <> "9") Then 'Jika MSD Officer dan Level KPI adalah section
                    level.Value = levelkpi
                    bscdepar.Value = Session("bscdepar")
                    sectiondepar.Value = section
                    If Not adadata Then
                        result.InnerHtml = ""
                        result.InnerHtml += "<table width='100%'><tr align='center'><td colspan='14'><span style='font-size:22px'>Belum ada data</span></td></tr>"
                    End If
                Else
                    result.InnerHtml = result.InnerHtml.Replace("#$#", "<img alt='Copy' style='cursor:hand' src='../images/copy.gif' onclick='doCopy();'/>")
                    result.InnerHtml += "<tr>"
                    result.InnerHtml += "<td style=""background-color:#DBEEF3""><img alt='Paste' style='cursor:hand;align:left' src='../images/paste.gif' onclick='doPaste();'/>&nbsp;<input type=""text"" style=""border:0;width:30%"" id=""versi"" value=""" & count & """ READONLY></td>"
                    result.InnerHtml += "<td style=""background-color:#DBEEF3""><select id=""calctype"" class=""plains"" onChange=""javascript:recalc1();""><option value=""0""" & checkselected(selected, 0) & ">SUM</option><option value=""1""" & checkselected(selected, 1) & ">AVG</option><option value=""2""" & checkselected(selected, 2) & ">LAST VALUE</option><option value=""3""" & checkselected(selected, 3) & ">TARGET MANUAL</option></select></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""jan"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""feb"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""mar"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""apr"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""mei"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""jun"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""jul"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""agu"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""sep"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""okt"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""nov"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""des"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""at"" value="""" onkeypress=""fncInputNumericValuesOnly();"" class=""plain""></td>"
                    result.InnerHtml += "<td><input type='text' id='jan' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='feb' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='mar' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='apr' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='mei' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='jun' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='jul' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='agu' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='sep' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='okt' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='nov' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='des' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='at' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' class='plain'></td>"




                    result.InnerHtml += "</tr><script type=""text/javascript"">recalc1();</script>"
                End If
                result.InnerHtml += ""
                result.InnerHtml += "</table>"
                conn.Close()
            Catch ex As Exception
                lblMessage.Text = ex.Message
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        End If
    End Sub

    Function checkselected(ByVal x As Integer, ByVal y As Integer)
        If x = y Then
            Return "Selected"
        End If
    End Function

    Function GetCalcType(ByVal calctype As String) As String
        Select Case calctype
            Case "0"
                Return "SUM"
            Case "1"
                Return "AVG"
            Case "2"
                Return "Last Value"
            Case "3"
                Return "TARGET MANUAL"
        End Select
        Return ""
    End Function

    'Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
    '    'Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
    '    'Try
    '    '    Dim query As String = ""
    '    '    Dim cmd As SqlCommand
    '    '    If revised.Visible = True Then
    '    '        If validasirev() Then
    '    '            query = "insert into ttarget (year,kpiid,versi,calctype,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,stedit,CreatedBy,CreatedIn) values('" & _
    '    '                cmbYear.SelectedValue & "','" & Request.QueryString("kpiid") & "','" & txtVersion.Text & _
    '    '                "','" & cmbYTD.SelectedValue & "','" & Replace(CDbl(txtJanRev.Text).ToString(), ",", ".") & "','" & Replace(CDbl(txtFebRev.Text).ToString(), ",", ".") & "','" & _
    '    '                Replace(CDbl(txtMarRev.Text).ToString(), ",", ".") & "','" & Replace(CDbl(txtAprRev.Text).ToString(), ",", ".") & "','" & Replace(CDbl(txtMeiRev.Text).ToString(), ",", ".") & "','" & Replace(CDbl(txtJunRev.Text).ToString(), ",", ".") & "','" & _
    '    '                Replace(CDbl(txtJulRev.Text).ToString(), ",", ".") & "','" & Replace(CDbl(txtAguRev.Text).ToString(), ",", ".") & "','" & Replace(CDbl(txtSepRev.Text).ToString(), ",", ".") & "','" & Replace(CDbl(txtOktRev.Text).ToString(), ",", ".") & "','" & _
    '    '                Replace(CDbl(txtNovRev.Text).ToString(), ",", ".") & "','" & Replace(CDbl(txtDesRev.Text).ToString(), ",", ".") & "','0','" & Session("NikUser") & "','" & Request.UserHostName & "');SELECT @@IDENTITY"
    '    '        Else
    '    '            Exit Sub
    '    '        End If
    '    '    Else
    '    '        If validasi() Then
    '    '            query = "insert into ttarget (year,kpiid,versi,calctype,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,stedit,CreatedBy,CreatedIn) values('" & _
    '    '                cmbYear.SelectedValue & "','" & Request.QueryString("kpiid") & "','" & txtVersion.Text & _
    '    '                "','" & cmbYTD.SelectedValue & "','" & Replace(CDbl(txtJan.Text).ToString(), ",", ".") & "','" & Replace(CDbl(txtFeb.Text).ToString(), ",", ".") & "','" & _
    '    '                Replace(CDbl(txtMar.Text).ToString(), ",", ".") & "','" & Replace(CDbl(txtApr.Text).ToString(), ",", ".") & "','" & Replace(CDbl(txtMei.Text).ToString(), ",", ".") & "','" & Replace(CDbl(txtJun.Text).ToString(), ",", ".") & "','" & _
    '    '                Replace(CDbl(txtJul.Text).ToString(), ",", ".") & "','" & Replace(CDbl(txtAgu.Text).ToString(), ",", ".") & "','" & Replace(CDbl(txtSep.Text).ToString(), ",", ".") & "','" & Replace(CDbl(txtOkt.Text).ToString(), ",", ".") & "','" & _
    '    '                Replace(CDbl(txtNov.Text).ToString(), ",", ".") & "','" & Replace(CDbl(txtDes.Text).ToString(), ",", ".") & "','0','" & Session("NikUser") & "','" & Request.UserHostName & "');SELECT @@IDENTITY"
    '    '        Else
    '    '            Exit Sub
    '    '        End If
    '    '    End If
    '    '    cmd = New SqlCommand(query, conn)
    '    '    conn.Open()
    '    '    cmd.ExecuteScalar()
    '    '    conn.Close()
    '    '    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes2", "alert('Update Target Berhasil');", True)
    '    '    'Response.Redirect("PopUpTarget.aspx?kpiid=" & Request.QueryString("kpiid"))
    '    '    Reload()
    '    'Catch ex As Exception
    '    '    lblMessage.Text = ex.Message
    '    'End Try
    'End Sub

    Function validasirev() As Boolean
        'Try
        '    If Trim(txtJanRev.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Januari"
        '        Return False
        '    ElseIf Trim(txtFebRev.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Februari"
        '        Return False
        '    ElseIf Trim(txtMarRev.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Maret"
        '        Return False
        '    ElseIf Trim(txtAprRev.Text) = "" Then
        '        lblMessage.Text = "Harap isi target April"
        '        Return False
        '    ElseIf Trim(txtMeiRev.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Mei"
        '        Return False
        '    ElseIf Trim(txtJunRev.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Juni"
        '        Return False
        '    ElseIf Trim(txtJulRev.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Juli"
        '        Return False
        '    ElseIf Trim(txtAguRev.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Agustus"
        '        Return False
        '    ElseIf Trim(txtSepRev.Text) = "" Then
        '        lblMessage.Text = "Harap isi target September"
        '        Return False
        '    ElseIf Trim(txtOktRev.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Oktober"
        '        Return False
        '    ElseIf Trim(txtNovRev.Text) = "" Then
        '        lblMessage.Text = "Harap isi target November"
        '        Return False
        '    ElseIf Trim(txtDesRev.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Desember"
        '        Return False
        '    End If
        '    Return True
        'Catch ex As Exception
        '    lblMessage.Text = ex.Message
        'End Try
    End Function

    Function validasi() As Boolean
        'Try
        '    If Trim(txtJan.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Januari"
        '        Return False
        '    ElseIf Trim(txtFeb.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Februari"
        '        Return False
        '    ElseIf Trim(txtMar.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Maret"
        '        Return False
        '    ElseIf Trim(txtApr.Text) = "" Then
        '        lblMessage.Text = "Harap isi target April"
        '        Return False
        '    ElseIf Trim(txtMei.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Mei"
        '        Return False
        '    ElseIf Trim(txtJun.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Juni"
        '        Return False
        '    ElseIf Trim(txtJul.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Juli"
        '        Return False
        '    ElseIf Trim(txtAgu.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Agustus"
        '        Return False
        '    ElseIf Trim(txtSep.Text) = "" Then
        '        lblMessage.Text = "Harap isi target September"
        '        Return False
        '    ElseIf Trim(txtOkt.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Oktober"
        '        Return False
        '    ElseIf Trim(txtNov.Text) = "" Then
        '        lblMessage.Text = "Harap isi target November"
        '        Return False
        '    ElseIf Trim(txtDes.Text) = "" Then
        '        lblMessage.Text = "Harap isi target Desember"
        '        Return False
        '    End If
        '    Return True
        'Catch ex As Exception
        '    lblMessage.Text = ex.Message
        'End Try
    End Function

    Function ReCalc()
        'If revised.Visible = False Then
        '    Select Case cmbYTD.SelectedValue
        '        Case "0" 'SUM
        '            txtAnnual.Text = CDec(IIf(IsNumeric(txtJan.Text), txtJan.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtFeb.Text), txtFeb.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtMar.Text), txtMar.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtApr.Text), txtApr.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtMei.Text), txtMei.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtJun.Text), txtJun.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtJul.Text), txtJul.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtAgu.Text), txtAgu.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtSep.Text), txtSep.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtOkt.Text), txtOkt.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtNov.Text), txtNov.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtDes.Text), txtDes.Text, 0))
        '        Case "1" 'AVG
        '            txtAnnual.Text = CDec((IIf(IsNumeric(txtJan.Text), txtJan.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtFeb.Text), txtFeb.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtMar.Text), txtMar.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtApr.Text), txtApr.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtMei.Text), txtMei.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtJun.Text), txtJun.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtJul.Text), txtJul.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtAgu.Text), txtAgu.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtSep.Text), txtSep.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtOkt.Text), txtOkt.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtNov.Text), txtNov.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtDes.Text), txtDes.Text, 0))) / 12
        '        Case "2" 'LAST
        '            txtAnnual.Text = CDec(IIf(IsNumeric(txtDes.Text), txtDes.Text, 0))
        '    End Select
        'Else
        '    Select Case cmbYTD.SelectedValue
        '        Case "0" 'SUM
        '            txtAnnual.Text = CDec(IIf(IsNumeric(txtJanRev.Text), txtJanRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtFebRev.Text), txtFebRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtMarRev.Text), txtMarRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtAprRev.Text), txtAprRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtMeiRev.Text), txtMeiRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtJunRev.Text), txtJunRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtJulRev.Text), txtJulRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtAguRev.Text), txtAguRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtSepRev.Text), txtSepRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtOktRev.Text), txtOktRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtNovRev.Text), txtNovRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtDesRev.Text), txtDesRev.Text, 0))
        '        Case "1" 'AVG
        '            txtAnnual.Text = CDec((IIf(IsNumeric(txtJanRev.Text), txtJanRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtFebRev.Text), txtFebRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtMarRev.Text), txtMarRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtAprRev.Text), txtAprRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtMeiRev.Text), txtMeiRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtJunRev.Text), txtJunRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtJulRev.Text), txtJulRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtAguRev.Text), txtAguRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtSepRev.Text), txtSepRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtOktRev.Text), txtOktRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtNovRev.Text), txtNovRev.Text, 0)) + _
        '            CDec(IIf(IsNumeric(txtDesRev.Text), txtDesRev.Text, 0))) / 12
        '        Case "2" 'LAST
        '            txtAnnual.Text = CDec(IIf(IsNumeric(txtDesRev.Text), txtDesRev.Text, 0))
        '    End Select
        'End If

    End Function

    'Private Sub txtJan_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJan.TextChanged
    '    ReCalc()
    'End Sub

    'Private Sub txtFeb_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFeb.TextChanged
    '    ReCalc()
    'End Sub

    'Private Sub txtMar_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMar.TextChanged
    '    ReCalc()
    'End Sub

    'Private Sub txtApr_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtApr.TextChanged
    '    ReCalc()
    'End Sub

    'Private Sub txtMei_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMei.TextChanged
    '    ReCalc()
    'End Sub

    'Private Sub txtJun_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJun.TextChanged
    '    ReCalc()
    'End Sub

    'Private Sub txtJul_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJul.TextChanged
    '    ReCalc()
    'End Sub

    'Private Sub txtAgu_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAgu.TextChanged
    '    ReCalc()
    'End Sub

    'Private Sub txtSep_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSep.TextChanged
    '    ReCalc()
    'End Sub

    'Private Sub txtOkt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOkt.TextChanged
    '    ReCalc()
    'End Sub

    'Private Sub txtNov_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNov.TextChanged
    '    ReCalc()
    'End Sub

    'Private Sub txtDes_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDes.TextChanged
    '    ReCalc()
    'End Sub

    'Private Sub cmbYTD_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbYTD.SelectedIndexChanged

    'End Sub

    'Protected Sub btnCalc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCalc.Click
    '    ReCalc()
    'End Sub

    Sub Reload()
        'Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        'Try
        '    Dim query As String = ""
        '    Dim dt As New DataTable()
        '    Dim da As SqlDataAdapter
        '    query = "select * from TTarget where YEAR='" & cmbYear.SelectedValue & "' and kpiid='" & Request.QueryString("kpiid") & "' order by versi desc"
        '    da = New SqlDataAdapter(query, conn)
        '    conn.Open()
        '    da.Fill(dt)
        '    conn.Close()
        '    If dt.Rows.Count = 0 Then
        '        'No Data Found
        '        txtVersion.Text = "1"
        '        txtVersion.Enabled = False
        '        revised.Visible = True
        '        txtJan.Text = 0
        '        txtJan.Enabled = True
        '        txtFeb.Text = 0
        '        txtFeb.Enabled = True
        '        txtMar.Text = 0
        '        txtMar.Enabled = True
        '        txtApr.Text = 0
        '        txtApr.Enabled = True
        '        txtMei.Text = 0
        '        txtMei.Enabled = True
        '        txtJun.Text = 0
        '        txtJun.Enabled = True
        '        txtJul.Text = 0
        '        txtJul.Enabled = True
        '        txtAgu.Text = 0
        '        txtAgu.Enabled = True
        '        txtSep.Text = 0
        '        txtSep.Enabled = True
        '        txtOkt.Text = 0
        '        txtOkt.Enabled = True
        '        txtNov.Text = 0
        '        txtNov.Enabled = True
        '        txtDes.Text = 0
        '        txtDes.Enabled = True
        '        revised.Visible = False
        '    Else
        '        txtVersion.Text = CInt(dt.Rows(0)("versi").ToString()) + 1
        '        txtVersion.Enabled = False
        '        cmbYTD.SelectedValue = dt.Rows(0)("calctype")
        '        txtJan.Text = dt.Rows(0)("jan")
        '        txtJan.Enabled = False
        '        txtFeb.Text = dt.Rows(0)("feb")
        '        txtFeb.Enabled = False
        '        txtMar.Text = dt.Rows(0)("mar")
        '        txtMar.Enabled = False
        '        txtApr.Text = dt.Rows(0)("apr")
        '        txtApr.Enabled = False
        '        txtMei.Text = dt.Rows(0)("mei")
        '        txtMei.Enabled = False
        '        txtJun.Text = dt.Rows(0)("jun")
        '        txtJun.Enabled = False
        '        txtJul.Text = dt.Rows(0)("jul")
        '        txtJul.Enabled = False
        '        txtAgu.Text = dt.Rows(0)("agu")
        '        txtAgu.Enabled = False
        '        txtSep.Text = dt.Rows(0)("sep")
        '        txtSep.Enabled = False
        '        txtOkt.Text = dt.Rows(0)("okt")
        '        txtOkt.Enabled = False
        '        txtNov.Text = dt.Rows(0)("nov")
        '        txtNov.Enabled = False
        '        txtDes.Text = dt.Rows(0)("des")
        '        txtDes.Enabled = False
        '        ReCalc()
        '        revised.Visible = True
        '        txtJanRev.Text = "0"
        '        txtFebRev.Text = "0"
        '        txtMarRev.Text = "0"
        '        txtAprRev.Text = "0"
        '        txtMeiRev.Text = "0"
        '        txtJunRev.Text = "0"
        '        txtJulRev.Text = "0"
        '        txtAguRev.Text = "0"
        '        txtSepRev.Text = "0"
        '        txtOktRev.Text = "0"
        '        txtNovRev.Text = "0"
        '        txtDesRev.Text = "0"
        '    End If
        'Catch ex As Exception
        '    lblMessage.Text = ex.Message
        'Finally
        '    If conn.State = ConnectionState.Open Then
        '        conn.Close()
        '    End If
        'End Try
    End Sub

    Protected Sub cmbYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbYear.SelectedIndexChanged
        kpiid.Value = Request.QueryString("kpiid")
        createdby.Value = Session("NikUser")
        createdin.Value = HttpContext.Current.Request.UserHostAddress
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim query As String = ""
            Dim dr As SqlDataReader
            Dim cmd As SqlCommand
            Dim count As Integer = 1
            Dim selected As Integer = 0
            Dim annualtarget As Decimal = 0
            txtUoM.Text = Request.QueryString("uom")
            conn.Open()
            query = "select isnull(calctype,0) as calctype from mkpi where id='" & Request.QueryString("kpiid") & "'"
                cmd = New SqlCommand(query, conn)
                calctype = cmd.ExecuteScalar
                query = "select * from TTarget where YEAR='" & cmbYear.SelectedValue & "' and kpiid='" & Request.QueryString("kpiid") & "' order by versi asc"
                cmd = New SqlCommand(query, conn)
                dr = cmd.ExecuteReader
                result.InnerHtml = "<table border=""1"" class=""act"">"
                result.InnerHtml += "<tr style=""background-color:#00CC00;color:black;font-weight:bold;""><td style=""background-color:#93CDDD"">Version</td><td style=""background-color:#93CDDD"">YTD Target</td><td align=""center"">JAN</td><td align=""center"">FEB</td><td align=""center"">MAR</td> " & _
                                      "<td align=""center"">APR</td> " & _
                                      "<td align=""center"">MEI</td> " & _
                                      "<td align=""center"">JUN</td> " & _
                                      "<td align=""center"">JUL</td> " & _
                                      "<td align=""center"">AGU</td> " & _
                                      "<td align=""center"">SEP</td> " & _
                                      "<td align=""center"">OKT</td> " & _
                                      "<td align=""center"">NOV</td> " & _
                                      "<td align=""center"">DES</td> " & _
                                      "<td align=""center"">Annual Target</td> "
                If dr.HasRows Then
                    While dr.Read
                        result.InnerHtml = result.InnerHtml.Replace("#$#", "")
                        result.InnerHtml += "<tr>"
                        result.InnerHtml += "<td style=""background-color:#DBEEF3"">#$# " & dr("Versi") & "</td>"
                        result.InnerHtml += "<td style=""background-color:#DBEEF3"">" & GetCalcType(dr("CalcType")) & "</td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("JAN") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("FEB") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("MAR") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("APR") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("MEI") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("JUN") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("JUL") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("AGU") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("SEP") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("OKT") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("NOV") & """ class=""plain"" READONLY/></td>"
                        result.InnerHtml += "<td><input type=""text"" value=""" & dr("DES") & """ class=""plain"" READONLY/></td>"
                        selected = dr("CalcType")
                        annualtarget = 0
                        Select Case dr("CalcType")
                            Case "0" 'SUM
                                result.InnerHtml += "<td>" & Format(CDec(IIf(dr("JAN") = "", 0, dr("JAN"))) + CDec(IIf(dr("FEB") = "", 0, dr("FEB"))) + CDec(IIf(dr("MAR") = "", 0, dr("MAR"))) + CDec(IIf(dr("APR") = "", 0, dr("APR"))) + CDec(IIf(dr("MEI") = "", 0, dr("MEI"))) + CDec(IIf(dr("JUN") = "", 0, dr("JUN"))) + CDec(IIf(dr("JUL") = "", 0, dr("JUL"))) + CDec(IIf(dr("AGU") = "", 0, dr("AGU"))) + CDec(IIf(dr("SEP") = "", 0, dr("SEP"))) + CDec(IIf(dr("OKT") = "", 0, dr("OKT"))) + CDec(IIf(dr("NOV") = "", 0, dr("NOV"))) + CDec(IIf(dr("DES") = "", 0, dr("DES"))), "#,##0.0") & "</td>"
                                annualtarget = CDec(IIf(dr("JAN") = "", 0, dr("JAN"))) + CDec(IIf(dr("FEB") = "", 0, dr("FEB"))) + CDec(IIf(dr("MAR") = "", 0, dr("MAR"))) + CDec(IIf(dr("APR") = "", 0, dr("APR"))) + CDec(IIf(dr("MEI") = "", 0, dr("MEI"))) + CDec(IIf(dr("JUN") = "", 0, dr("JUN"))) + CDec(IIf(dr("JUL") = "", 0, dr("JUL"))) + CDec(IIf(dr("AGU") = "", 0, dr("AGU"))) + CDec(IIf(dr("SEP") = "", 0, dr("SEP"))) + CDec(IIf(dr("OKT") = "", 0, dr("OKT"))) + CDec(IIf(dr("NOV") = "", 0, dr("NOV"))) + CDec(IIf(dr("DES") = "", 0, dr("DES")))
                            Case "1" 'AVG
                                Dim myresult As Decimal
                                Dim divider As Decimal = 12
                                myresult = CDec(IIf(dr("JAN") = "", 0, dr("JAN"))) + CDec(IIf(dr("FEB") = "", 0, dr("FEB"))) + CDec(IIf(dr("MAR") = "", 0, dr("MAR"))) + CDec(IIf(dr("APR") = "", 0, dr("APR"))) + CDec(IIf(dr("MEI") = "", 0, dr("MEI"))) + CDec(IIf(dr("JUN") = "", 0, dr("JUN"))) + CDec(IIf(dr("JUL") = "", 0, dr("JUL"))) + CDec(IIf(dr("AGU") = "", 0, dr("AGU"))) + CDec(IIf(dr("SEP") = "", 0, dr("SEP"))) + CDec(IIf(dr("OKT") = "", 0, dr("OKT"))) + CDec(IIf(dr("NOV") = "", 0, dr("NOV"))) + CDec(IIf(dr("DES") = "", 0, dr("DES")))
                                If dr("JAN") = "" Then divider = divider - 1
                                If dr("FEB") = "" Then divider = divider - 1
                                If dr("MAR") = "" Then divider = divider - 1
                                If dr("APR") = "" Then divider = divider - 1
                                If dr("MEI") = "" Then divider = divider - 1
                                If dr("JUN") = "" Then divider = divider - 1
                                If dr("JUL") = "" Then divider = divider - 1
                                If dr("AGU") = "" Then divider = divider - 1
                                If dr("SEP") = "" Then divider = divider - 1
                                If dr("OKT") = "" Then divider = divider - 1
                                If dr("NOV") = "" Then divider = divider - 1
                                If dr("DES") = "" Then divider = divider - 1
                                result.InnerHtml += "<td>" & Format(myresult / divider, "#,##0.0") & "</td>"
                                annualtarget = CDec(myresult / divider)
                            Case "2" 'LAST VAL
                                result.InnerHtml += "<td>" & Format(CDec(dr("annual")), "#,##0.0") & "</td>"
                                annualtarget = CDec(dr("annual"))
                            Case "3" 'OTHERS
                                If IsDBNull(dr("annual")) Then
                                    result.InnerHtml += "<td>0</td>"
                                    annualtarget = 0
                                Else
                                    result.InnerHtml += "<td>" & Format(CDec(dr("annual")), "#,##0.0") & "</td>"
                                    annualtarget = CDec(dr("annual"))
                                End If
                        End Select
                        count += 1
                        pasteinput.Value = dr("JAN") & "|" & dr("FEB") & "|" & dr("MAR") & "|" & dr("APR") & "|" & dr("MEI") & "|" & dr("JUN") & "|" & dr("JUL") & "|" & dr("AGU") & "|" & dr("SEP") & "|" & dr("OKT") & "|" & dr("NOV") & "|" & dr("DES")
                        pasteinput.Value = dr("JAN") & vbTab & dr("FEB") & vbTab & dr("MAR") & vbTab & dr("APR") & vbTab & dr("MEI") & vbTab & dr("JUN") & vbTab & dr("JUL") & vbTab & dr("AGU") & vbTab & dr("SEP") & vbTab & dr("OKT") & vbTab & dr("NOV") & vbTab & dr("DES")
                    End While
                Else
                    adadata = False
                End If

                dr.Close()
                dr = Nothing

                query = "select isnull(level,55) as level,isnull(section,55) as section from MKPI where id='" & Request.QueryString("kpiid") & "'"
                cmd = New SqlCommand(query, conn)


                dr = cmd.ExecuteReader
                If dr.HasRows Then
                    While dr.Read
                        levelkpi = dr("level")
                        section = dr("section")
                    End While
                End If
                If (Session("permission") = "2" And levelkpi = 2 And section <> "9") Then 'Jika MSD Officer dan Level KPI adalah section
                    level.Value = levelkpi
                    bscdepar.Value = Session("bscdepar")
                    sectiondepar.Value = section
                    If Not adadata Then
                        result.InnerHtml = ""
                        result.InnerHtml += "<table width='100%'><tr align='center'><td colspan='14'><span style='font-size:22px'>Belum ada data</span></td></tr>"
                    End If
                Else
                    result.InnerHtml = result.InnerHtml.Replace("#$#", "<img alt='Copy' style='cursor:hand' src='../images/copy.gif' onclick='doCopy();'/>")
                    result.InnerHtml += "<tr>"
                    result.InnerHtml += "<td style=""background-color:#DBEEF3""><img alt='Paste' style='cursor:hand;align:left' src='../images/paste.gif' onclick='doPaste();'/>&nbsp;<input type=""text"" style=""border:0;width:30%"" id=""versi"" value=""" & count & """ READONLY></td>"
                    result.InnerHtml += "<td style=""background-color:#DBEEF3""><select id=""calctype"" class=""plains"" onChange=""javascript:recalc1();""><option value=""0""" & checkselected(selected, 0) & ">SUM</option><option value=""1""" & checkselected(selected, 1) & ">AVG</option><option value=""2""" & checkselected(selected, 2) & ">LAST VALUE</option><option value=""3""" & checkselected(selected, 3) & ">TARGET MANUAL</option></select></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""jan"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""feb"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""mar"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""apr"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""mei"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""jun"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""jul"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""agu"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""sep"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""okt"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""nov"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""des"" value="""" onkeypress=""fncInputNumericValuesOnly();"" onChange=""javascript:recalc1();"" class=""plain""></td>"
                    'result.InnerHtml += "<td><input type=""text"" id=""at"" value="""" onkeypress=""fncInputNumericValuesOnly();"" class=""plain""></td>"
                    result.InnerHtml += "<td><input type='text' id='jan' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='feb' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='mar' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='apr' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='mei' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='jun' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='jul' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='agu' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='sep' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='okt' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='nov' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='des' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' onChange='javascript:recalc1();' class='plain'></td>"
                    result.InnerHtml += "<td><input type='text' id='at' value='' class='" & IIf(calctype = "0" Or calctype = "1", "printe {mNum: ""12"",aSep: "","", aDec: "".""} ", "printe {mNum: ""12"",aNeg: ""-"", aSep: "","", aDec: "".""}") & ";' class='plain'></td>"




                    result.InnerHtml += "</tr><script type=""text/javascript"">recalc1();</script>"
                End If
                result.InnerHtml += ""
                result.InnerHtml += "</table>"
                conn.Close()
        Catch ex As Exception
            lblMessage.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub
End Class