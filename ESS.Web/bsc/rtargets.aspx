﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="rtargets.aspx.vb"
   Inherits="EXCELLENT.rtargets" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
   <title></title>
   <style type="text/css">
      input.btn
      {
         color: #050;
         font: bold 90% 'trebuchet ms' ,helvetica,sans-serif;
         background-color: #fed;
         border: 1px solid;
         border-color: #696 #363 #363 #696;
         filter: progid:DXImageTransform.Microsoft.Gradient (GradientType=0,StartColorStr= '#ffffffff' ,EndColorStr= '#ffeeddaa' );
         padding: 0 10px 0 10px;
         cursor: hand;
      }
      table.act
      {
         border-width: 1px;
         border-spacing: 0px;
         border-color: #C0C0C0;
         border-collapse: collapse;
         background-color: white;
         font-size: 14px;
      }
      table.act thead td
      {
         border-width: 1px;
         padding: 1px;
         border-color: #C0C0C0;
         text-align: center;
      }
      table.act td
      {
         border-width: 1px;
         padding: 1px;
         border-color: #C0C0C0;
         text-align: right;
      }
      ul.dropdown *.dir1
      {
         padding-right: 20px;
         background-image: url("/remo/css/dropdown/themes/default/images/nav-arrow-down.png");
         background-position: 100% 50%;
         background-repeat: no-repeat;
         background-color: #FFA500;
      }
      select.plains
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }
      input.plain
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }
      .plain:hover
      {
         background-color: #FFFFA8;
      }
      body
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
      }
   </style>

   <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>

   <script type="text/javascript">
      function openSearchModal() {
         var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPILevel');
         var selIndex = selObj.selectedIndex;
         var yearObj = document.getElementById('ctl00_ContentPlaceHolder1_ddlYear');
         var yearIndex = yearObj.selectedIndex;
         if (selObj.options[selIndex].value == '1') {
            var funcObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc');
            var funcselIndex = funcObj.selectedIndex;
            var returnVal = window.showModalDialog('PopUpSearch.aspx?y=' + yearObj.options[yearIndex].value + '&vt=1&type=' + selObj.options[selIndex].value + '&func=' + funcObj.options[funcselIndex].value, '', 'dialogWidth=300px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         } else if (selObj.options[selIndex].value == '2') {
            var secObj = document.getElementById('ctl00_ContentPlaceHolder1_KPISection');
            var secselIndex = secObj.selectedIndex;
            var returnVal = window.showModalDialog('PopUpSearch.aspx?y=' + yearObj.options[yearIndex].value + '&vt=1&type=' + selObj.options[selIndex].value + '&sec=' + secObj.options[secselIndex].value, '', 'dialogWidth=300px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         } else if (selObj.options[selIndex].value == '3') {
            var jobObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite');
            var jobselIndex = jobObj.selectedIndex;
            var returnVal = window.showModalDialog('PopUpSearch.aspx?y=' + yearObj.options[yearIndex].value + '&vt=1&type=' + selObj.options[selIndex].value + '&jobsite=' + jobObj.options[jobselIndex].value, '', 'dialogWidth=300px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         } else if (selObj.options[selIndex].value == '4') {
            var dirObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIDirektorat');
            var dirselIndex = dirObj.selectedIndex;
            var returnVal = window.showModalDialog('PopUpSearch.aspx?y=' + yearObj.options[yearIndex].value + '&type=' + selObj.options[selIndex].value + '&dir=' + dirObj.options[dirselIndex].value, '', 'dialogWidth=350px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         } else {
            var returnVal = window.showModalDialog('PopUpSearch.aspx?y=' + yearObj.options[yearIndex].value + '&vt=1&type=' + selObj.options[selIndex].value, '', 'dialogWidth=300px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         }
         if (returnVal != undefined) {
            $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "WS/BSCService.asmx/RetrieveKPI",
               data: "{'_id':" + returnVal + "}",
               dataType: "json",
               success: function(res) {
                  //kpiid = res.d.Id;
                  document.getElementById("ctl00_ContentPlaceHolder1_kpiname").value = res.d.Name;
                  document.getElementById("ctl00_ContentPlaceHolder1_kpiid").value = res.d.Id;
               },
               error: function(err) {
                  alert(err.responseText);
               }
            });

         }
      }
   </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
   </asp:ToolkitScriptManager>
   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
      <ContentTemplate>
         <div>
            <table>
               <tr>
                  <td>
                     Tahun
                  </td>
                  <td>
                     <asp:DropDownList ID="ddlYear" runat="server" CssClass="plains">
                        <asp:ListItem>2010</asp:ListItem>
                        <asp:ListItem>2011</asp:ListItem>
                        <asp:ListItem>2012</asp:ListItem>
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td>
                     KPI Level
                  </td>
                  <td>
                     <asp:DropDownList ID="KPILevel" runat="server" AutoPostBack="true" CssClass="plains">
                        <asp:ListItem Value="0" Text="JRN"></asp:ListItem>
                        <asp:ListItem Value="4" Text="Direktorat"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Function"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Section"></asp:ListItem>
                        <asp:ListItem Value="3" Text="Jobsite"></asp:ListItem>
                     </asp:DropDownList>
                     <asp:DropDownList ID="KPIDirektorat" runat="server" CssClass="plains">
                     </asp:DropDownList>
                     <asp:DropDownList ID="KPIFunc" runat="server" Visible="False" CssClass="plains">
                     </asp:DropDownList>
                     <asp:DropDownList ID="KPISection" runat="server" Visible="False" CssClass="plains">
                     </asp:DropDownList>
                     <asp:DropDownList ID="KPIJobsite" runat="server" Visible="False" CssClass="plains">
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td>
                     KPI
                  </td>
                  <td>
                     <asp:HiddenField ID="kpiid" Value="[AUTO]" runat="server" />
                     <asp:TextBox runat="Server" ID="kpiname" Width="200px" Enabled="false" CssClass="plain" />
                     <input type="button" id="btnSearch" style="background-image: url(../images/Search.gif);
                        background-repeat: no-repeat; cursor: hand; vertical-align: top; width: 20px;
                        height: 20px; background-color: transparent; border: 0" onclick="openSearchModal();" />
                  </td>
               </tr>
               <tr>
                  <td colspan="2">
                     <asp:Button ID="btnView" runat="server" Text="View" CssClass="btn" />
                  </td>
               </tr>
            </table>
            <asp:Label ID="lblMessage" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="#FF3300"></asp:Label>
            <br />
            <div id="result" runat="server">
            </div>
         </div>
      </ContentTemplate>
   </asp:UpdatePanel>

   <script type="text/javascript">
      function fnprint() {
         document.getElementById('btnprint').style.visibility = 'hidden';
         var pp = window.open();
         pp.document.writeln('<HTML><HEAD><title>Print Preview</title><style type="text/css">');
         pp.document.writeln('table.act {');
         pp.document.writeln('border-width: 1px;');
         pp.document.writeln('border-spacing: 0px;');
         pp.document.writeln('border-color: #C0C0C0;');
         pp.document.writeln('border-collapse: collapse;');
         pp.document.writeln('background-color: white;');
         pp.document.writeln('}');
         pp.document.writeln('table.act td {');
         pp.document.writeln('border-width: 1px;');
         pp.document.writeln('padding: 1px;');
         pp.document.writeln('border-color: #C0C0C0;');
         pp.document.writeln('text-align:right;');
         pp.document.writeln('}');
         pp.document.writeln('table.act thead td {');
         pp.document.writeln('border-width: 1px;');
         pp.document.writeln('padding: 1px;');
         pp.document.writeln('border-color: #C0C0C0;');
         pp.document.writeln('text-align:center;');
         pp.document.writeln('}');
         pp.document.writeln('</style></HEAD>');
         pp.document.writeln('<b>Summary Target ' + document.getElementById('ctl00_ContentPlaceHolder1_kpiname').value + ' Tahun ' + document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').selectedIndex].text + '</b><br/>');
         pp.document.writeln(document.getElementById('ctl00_ContentPlaceHolder1_result').innerHTML);
         pp.document.writeln('<\HTML>');
         document.getElementById('btnprint').style.visibility = 'visible';
      }
   </script>

</asp:Content>
