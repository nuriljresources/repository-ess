﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="searchcostcode.aspx.vb" Inherits="EXCELLENT.searchcostcode" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <link href="css/site.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function UseSelected(costcode) {
            var pw = window.opener;
            var inputFrm = pw.document.forms['aspnetForm'];
            if (inputFrm.elements['ctrlToFind'].value == 'Originator') {
                inputFrm.elements['ctl00_ContentPlaceHolder1_Originator'].value = nopeg;
                inputFrm.elements['ctl00_ContentPlaceHolder1_OriginatorName'].value = nama;
            } else if (inputFrm.elements['ctrlToFind'].value == 'PenanggungJawab') {
                inputFrm.elements['ctl00_ContentPlaceHolder1_PenanggungJawab'].value = nopeg;
                inputFrm.elements['ctl00_ContentPlaceHolder1_PenanggungJawabName'].value = nama;
            } else if (inputFrm.elements['ctrlToFind'].value == 'PIC') {
                inputFrm.elements['ctl00$ContentPlaceHolder1$AccordionPane1_content$PIC'].value = nopeg;
                inputFrm.elements['ctl00$ContentPlaceHolder1$AccordionPane1_content$PICName'].value = nama;
            } else if (Left(inputFrm.elements['ctrlToFind'].value, 2) == 'qq') {
                inputFrm.elements['ps' + Right(inputFrm.elements['ctrlToFind'].value, inputFrm.elements['ctrlToFind'].value.length - 3) + 'nik'].value = nopeg;
                inputFrm.elements['ps' + Right(inputFrm.elements['ctrlToFind'].value, inputFrm.elements['ctrlToFind'].value.length - 3) + 'name'].value = nama;
            } else if (inputFrm.elements['ctrlToFind'].value == 'PimRap') {
                inputFrm.elements['nikPimRap'].value = nopeg;
                inputFrm.elements['namaPimRap'].value = nama;
                pw.document.getElementById("jnsmeeting").focus();
            } else if (inputFrm.elements['ctrlToFind'].value == 'NotRap') {
                inputFrm.elements['nikNotRap'].value = nopeg;
                inputFrm.elements['namaNotRap'].value = nama;
            } else if (Left(inputFrm.elements['ctrlToFind'].value, 3) == 'pic') {
                inputFrm.elements[inputFrm.elements['ctrlToFind'].value].value = nopeg;
                inputFrm.elements[inputFrm.elements['ctrlToFind'].value + 'name'].value = nama;
            } else if (inputFrm.elements['ctrlToFind'].value == 'user') {
                inputFrm.elements['nik'].value = nopeg;
                inputFrm.elements['nama'].value = nama;
                inputFrm.elements['kddepar'].value = kddepar;
                inputFrm.elements['nmjabat'].value = nmjabat;
                inputFrm.elements['site'].value = kdsite;
            } else if (inputFrm.elements['ctrlToFind'].value == 'cc') {
            inputFrm.elements['costcode'].value = costcode;
            } else {
                inputFrm.elements[inputFrm.elements['ctrlToFind'].value].value = nopeg;
                inputFrm.elements[inputFrm.elements['ctrlToFind'].value + 'name'].value = nama;
            }
            window.close();
        }
        function Left(str, n) {
            if (n <= 0)
                return "";
            else if (n > String(str).length)
                return str;
            else
                return String(str).substring(0, n);
        }

        function Right(str, n) {
            if (n <= 0)
                return "";
            else if (n > String(str).length)
                return str;
            else {
                var iLen = String(str).length;
                return String(str).substring(iLen, iLen - n);
            }
        }

        function keyPressed(e) {
            switch (e.keyCode) {
                case 13:
                    {
                        document.getElementById("btnSearch").click();
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        function cekArgumen() {
            var d = document;
            if ((d.getElementById("SearchKey").value.length < 1) || (d.getElementById("SearchKey").value.length == 0)) {
                alert("Please Entry Specific Costcode (minimal 1 Character)");
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
  <ajaxToolkit:ToolkitScriptManager  ID="ToolkitScriptManager1" runat="server"/>
   <asp:UpdatePanel id="MyUpdatePanel" runat="server">
    <ContentTemplate>
      <div>           

      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
         <caption style="text-align:center">Search Costcode</caption>
         <tr>
            <td style="padding: 5px; width:20%">Cost Code</td>
            <td style="width:15%"><asp:TextBox ID="SearchKey" runat="server" class="tb10" onkeydown="keyPressed(event);"/></td>
            <td><input type="text" id="hide" style="display:none;"/><asp:Button ID="btnSearch" 
                    runat="server" Text="Find" Font-Bold="False" ForeColor="#0033CC" 
                    OnClientClick="cekArgumen()" Height="26px" Width="90px"/></td>
         </tr>
         <tr>
         <td>
         </td>
         </tr>
         <tr>
         <td colspan="3">
         <asp:Repeater ID="rptPages" Runat="server">
      <HeaderTemplate>
      <table cellpadding="0" cellspacing="0" border="0">
      <tr>
         <td style="vertical-align:top"><b>Page:</b>&nbsp;</td>
         <td>
      </HeaderTemplate>
      <ItemTemplate>
                        <asp:LinkButton ID="btnPage" CommandName="Page" 
                         CommandArgument="<%# Container.DataItem %>" 
                         CssClass="text" 
                         Runat="server" 
                         Visible='<%# (CInt(Container.DataItem) <> (PageNumber+1)).ToString() %>'>
                         <%#Container.DataItem%> 
                         </asp:LinkButton>
                         <asp:Label ID="lblPage" runat="server" Visible='<%# (CInt(Container.DataItem) = (PageNumber+1)).ToString()%>' 
                         Text="<%#Container.DataItem%>" 
                         ForeColor="black" Font-Bold="true">
                         </asp:Label>
      </ItemTemplate>
      <FooterTemplate>
         </td>
      </tr>
      </td>
      </table>
      </FooterTemplate>
      </asp:Repeater>
         </tr>
         <tr>
            <td colspan="3">
               <asp:Repeater ID="rptResult" runat="server">
               <HeaderTemplate>
                           <table width="50%" border="0" cellpadding="0" cellspacing="0" class="data-table-inside">
                           <tr>               
                                <th>No.</th>
                                <th>Cost Code</th>
                               <th>Cost Name</th>
                               <th> </th>
                               <th> </th>
                                 <%--<th>Kd. Departemen</th>
                                <th>Department</th>
                                <th>Jobsite</th>--%>
                           </tr>
                        </HeaderTemplate>  
                        <ItemTemplate>
                       <tr>
                        <td align="center"><%# Container.ItemIndex + 1 %></td>
                        <td align="center"><a href="#" onclick="UseSelected('<%#DataBinder.Eval(Container.DataItem, "costcode")%>','<%#DataBinder.Eval(Container.DataItem, "costname")%>');"><%#DataBinder.Eval(Container.DataItem, "costcode")%></a></td>
                        <td align="center"><%#DataBinder.Eval(Container.DataItem, "costname")%></td>
                        <%--<td align="center"><%#DataBinder.Eval(Container.DataItem, "KdDepar")%></td>--%>
                        <%--<td><%#DataBinder.Eval(Container.DataItem, "NmDepar")%></td>--%>
                        <%--<td align="center"><%#DataBinder.Eval(Container.DataItem, "KdSite")%></td>--%>
                        <%--<td align="center"><%#DataBinder.Eval(Container.DataItem, "NmJabat")%></td>--%>
                        </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                           </table>
                        </FooterTemplate>
               </asp:Repeater>             
            </td>
         </tr>
      
      </table>  
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
     <asp:UpdateProgress ID="progress1" runat="server" DisplayAfter="50">    
                  <ProgressTemplate>
                  <asp:Panel ID="pnlProgressBar" runat="server">    
                     <div><asp:Image ID="Image1" runat="Server" ImageUrl="images/ajax-loader.gif"/>Please 
                         Wait...</div>    
                  </asp:Panel>   
                  </ProgressTemplate> 
                  </asp:UpdateProgress>
    </form>
    <script type="text/javascript">
        document.getElementById("SearchKey").focus();
    </script>
</body>
</html>
