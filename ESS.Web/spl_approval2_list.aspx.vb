﻿Imports System.Data.SqlClient
Partial Public Class spl_approval2_list
    Inherits System.Web.UI.Page
    Public tglaw As String
    Public th As String
    Public v1 As String
    Public v2 As String
    Public imonth As Integer
    Public js As String
    Public msg As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("niksite") = Nothing Or Session("niksite") = "" Then Response.Redirect("login.aspx")
        Dim baris As Integer

        If Not IsPostBack Then
            Dim a As String

            tglaw = Request(ddlbulan.UniqueID)
            th = Request(ddltahun.UniqueID)
            Dim i As Integer

            Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

            sqlConn.Open()
            Dim strcon As String = "select min(Tanggal) as min_date ,max(Tanggal) as max_date from H_H110"
            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            v1 = dtb.Rows(0)!min_date.ToString
            If v1.ToString = "" Then
                v1 = Today.Year.ToString
            Else
                v1 = CDate(v1).ToString("yyyy")
            End If

            v2 = dtb.Rows(0)!max_date.ToString
            If v2.ToString = "" Then
                v2 = Today.Year.ToString
            Else
                v2 = CDate(v2).ToString("yyyy")
            End If

            ddltahun.Items.Clear()
            For i = v1 To v2
                ddltahun.Items.Add(i)
            Next

            ddltahun.SelectedValue = (i - 1).ToString

            Select Case tglaw
                Case "January"
                    imonth = 1
                Case "February"
                    imonth = 2
                Case "March"
                    imonth = 3
                Case "April"
                    imonth = 4
                Case "May"
                    imonth = 5
                Case "June"
                    imonth = 6
                Case "July"
                    imonth = 7
                Case "August"
                    imonth = 8
                Case "September"
                    imonth = 9
                Case "October"
                    imonth = 10
                Case "November"
                    imonth = 11
                Case "December"
                    imonth = 12
                Case Else
                    imonth = 1
            End Select

            Try

                Dim dtb2 As DataTable = New DataTable()

                Dim strcon2 As String = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, jam_start as waktu_start, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit, freject1, freject2 from H_H110 where app2 = '" + Session("niksite") + "' and stedit in ('1','3') and freject1 in ('0','1') and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + ""
                'strcon2 = strcon2 + "where nik = '" + Session("niksite") + "' and StEdit <> 2 and month(TglTrans) = " + Today.Month.ToString + " and YEAR(TglTrans) = " + Today.Year.ToString + ""
                Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn)
                sda2.Fill(dtb2)

                For baris = 0 To dtb2.Rows.Count - 1
                   
                    If dtb2.Rows(baris)!checkapp2.ToString = "" And dtb2.Rows(baris)!freject1.ToString = "0" And dtb2.Rows(baris)!freject2.ToString = "0" Then
                        dtb2.Rows(baris)!status = "Complete"
                    ElseIf dtb2.Rows(baris)!stedit.ToString = "3" Then
                        dtb2.Rows(baris)!status = "Complete"
                    ElseIf dtb2.Rows(baris)!checkapp1.ToString <> "" And dtb2.Rows(baris)!checkapp2.ToString <> "" And dtb2.Rows(baris)!stedit.ToString = "1" And dtb2.Rows(baris)!freject1.ToString = "0" Then
                        If Session("kdsite").ToString() = "BAK" Or Session("kdsite").ToString() = "LAN" Or Session("kdsite").ToString() = "JKT" Or Session("kdsite").ToString() = "PEN" Then
                            If CDate(dtb2.Rows(baris)!waktu_start) < Now.Date.AddDays(-2) Then
                                dtb2.Rows(baris)!status = "Expired"
                            Else
                                dtb2.Rows(baris)!status = "Approval 1"
                            End If
                        Else
                            If CDate(dtb2.Rows(baris)!waktu_start) < Now.Date Then
                                dtb2.Rows(baris)!status = "Expired"
                            Else
                                dtb2.Rows(baris)!status = "Approval 1"
                            End If
                        End If
                        
                    ElseIf dtb2.Rows(baris)!checkapp1.ToString = "" And dtb2.Rows(baris)!checkapp2.ToString <> "" And dtb2.Rows(baris)!stedit.ToString = "1" And dtb2.Rows(baris)!freject1.ToString = "0" And dtb2.Rows(baris)!freject2.ToString = "0" Then
                        If Session("kdsite").ToString() = "BAK" Or Session("kdsite").ToString() = "LAN" Or Session("kdsite").ToString() = "JKT" Or Session("kdsite").ToString() = "PEN" Then
                            If CDate(dtb2.Rows(baris)!waktu_start) < Now.Date.AddDays(-2) Then
                                dtb2.Rows(baris)!status = "Expired"
                            Else
                                dtb2.Rows(baris)!status = "Approval 2"
                            End If
                        Else
                            If CDate(dtb2.Rows(baris)!waktu_start) < Now.Date Then
                                dtb2.Rows(baris)!status = "Expired"
                            Else
                                dtb2.Rows(baris)!status = "Approval 2"
                            End If
                        End If
                        
                    ElseIf dtb2.Rows(baris)!checkapp1.ToString <> "" And dtb2.Rows(baris)!checkapp2.ToString <> "" And dtb2.Rows(baris)!stedit.ToString = "0" Then
                        If Session("kdsite").ToString() = "BAK" Or Session("kdsite").ToString() = "LAN" Or Session("kdsite").ToString() = "JKT" Or Session("kdsite").ToString() = "PEN" Then
                            If CDate(dtb2.Rows(baris)!waktu_start) < Now.Date.AddDays(-2) Then
                                dtb2.Rows(baris)!status = "Expired"
                            Else
                                dtb2.Rows(baris)!status = "Save"
                            End If
                        Else
                            If CDate(dtb2.Rows(baris)!waktu_start) < Now.Date Then
                                dtb2.Rows(baris)!status = "Expired"
                            Else
                                dtb2.Rows(baris)!status = "Save"
                            End If
                        End If
                        
                    ElseIf dtb2.Rows(baris)!freject1.ToString = "1" Then
                        dtb2.Rows(baris)!status = "Declined by App 1"
                    ElseIf dtb2.Rows(baris)!freject2.ToString = "1" Then
                        dtb2.Rows(baris)!status = "Declined by App 2"
                    End If
                Next
                GridView1.DataSource = dtb2
                GridView1.DataBind()
                ddlbulan.SelectedValue = Today.Month

            Catch ex As Exception
            Finally
                sqlConn.Close()
            End Try
        End If
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim baris As Integer
        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()

            Dim strcon As String = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, jam_start as waktu_start, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit, freject1, freject2 from H_H110 "
            strcon = strcon + "where app2 = '" + Session("niksite") + "' and freject1 in ('0','1') and stedit in ('1','3') and StEdit <> 2 and month(Tanggal) = " + ddlbulan.SelectedValue.ToString + " and YEAR(Tanggal) = " + ddltahun.SelectedValue.ToString
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)
            For baris = 0 To dtb.Rows.Count - 1


                If dtb.Rows(baris)!checkapp2.ToString = "" And dtb.Rows(baris)!freject1.ToString = "0" And dtb.Rows(baris)!freject2.ToString = "0" Then
                    dtb.Rows(baris)!status = "Complete"
                ElseIf dtb.Rows(baris)!stedit.ToString = "3" Then
                    dtb.Rows(baris)!status = "Complete"
                ElseIf dtb.Rows(baris)!checkapp1.ToString <> "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "1" And dtb.Rows(baris)!freject1.ToString = "0" Then
                    If Session("kdsite").ToString() = "BAK" Or Session("kdsite").ToString() = "LAN" Or Session("kdsite").ToString() = "JKT" Or Session("kdsite").ToString() = "PEN" Then
                        If CDate(dtb.Rows(baris)!waktu_start) < Now.Date.AddDays(-2) Then
                            dtb.Rows(baris)!status = "Expired"
                        Else
                            dtb.Rows(baris)!status = "Approval 1"
                        End If
                    Else
                        If CDate(dtb.Rows(baris)!waktu_start) < Now.Date Then
                            dtb.Rows(baris)!status = "Expired"
                        Else
                            dtb.Rows(baris)!status = "Approval 1"
                        End If
                    End If
                    
                ElseIf dtb.Rows(baris)!checkapp1.ToString = "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "1" And dtb.Rows(baris)!freject1.ToString = "0" And dtb.Rows(baris)!freject2.ToString = "0" Then
                    If Session("kdsite").ToString() = "BAK" Or Session("kdsite").ToString() = "LAN" Or Session("kdsite").ToString() = "JKT" Or Session("kdsite").ToString() = "PEN" Then
                        If CDate(dtb.Rows(baris)!waktu_start) < Now.Date.AddDays(-2) Then
                            dtb.Rows(baris)!status = "Expired"
                        Else
                            dtb.Rows(baris)!status = "Approval 2"
                        End If
                    Else
                        If CDate(dtb.Rows(baris)!waktu_start) < Now.Date Then
                            dtb.Rows(baris)!status = "Expired"
                        Else
                            dtb.Rows(baris)!status = "Approval 2"
                        End If
                    End If
                    
                ElseIf dtb.Rows(baris)!checkapp1.ToString <> "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "0" Then
                    If Session("kdsite").ToString() = "BAK" Or Session("kdsite").ToString() = "LAN" Or Session("kdsite").ToString() = "JKT" Or Session("kdsite").ToString() = "PEN" Then
                        If CDate(dtb.Rows(baris)!waktu_start) < Now.Date.AddDays(-2) Then
                            dtb.Rows(baris)!status = "Expired"
                        Else
                            dtb.Rows(baris)!status = "Save"
                        End If
                    Else
                        If CDate(dtb.Rows(baris)!waktu_start) < Now.Date Then
                            dtb.Rows(baris)!status = "Expired"
                        Else
                            dtb.Rows(baris)!status = "Save"
                        End If
                    End If
                    
                ElseIf dtb.Rows(baris)!freject1.ToString = "1" Then
                    dtb.Rows(baris)!status = "Declined by App 1"
                ElseIf dtb.Rows(baris)!freject2.ToString = "1" Then
                    dtb.Rows(baris)!status = "Declined by App 2"
                End If
            Next
            GridView1.DataSource = dtb
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging

        GridView1.PageIndex = e.NewPageIndex
        GridView1.DataBind()

    End Sub

    Private Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim drv As DataRowView = TryCast(e.Row.DataItem, DataRowView)
            If drv("status").ToString().Equals("Complete") Then
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#11FF66")
            ElseIf drv("status").ToString().Equals("Approval 1") Then
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF85")
            ElseIf drv("status").ToString().Equals("Approval 2") Then
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFE985")
            ElseIf drv("status").ToString().Equals("Save") Then
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#EDE8E8")
            Else
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF3B3B")
            End If
        End If
    End Sub

    Sub refresh()
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim baris As Integer
        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()

            Dim strcon As String = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, jam_start, jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 "
            strcon = strcon + "where checkapp1 = '" + Session("niksite") + "' and freject1 = '0' and stedit = '1' and StEdit <> 2 and month(Tanggal) = " + ddlbulan.SelectedValue.ToString + " and YEAR(Tanggal) = " + ddltahun.SelectedValue.ToString
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)
            For baris = 0 To dtb.Rows.Count - 1
                If dtb.Rows(baris)!stedit.ToString = "3" And dtb.Rows(baris)!checkapp2.ToString = "" Then
                    dtb.Rows(baris)!status = "Complete"
                ElseIf dtb.Rows(baris)!checkapp1.ToString <> "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "1" Then
                    dtb.Rows(baris)!status = "Approval 1"
                ElseIf dtb.Rows(baris)!checkapp1.ToString = "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "1" Then
                    dtb.Rows(baris)!status = "Approval 2"
                ElseIf dtb.Rows(baris)!checkapp1.ToString <> "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "0" Then
                    dtb.Rows(baris)!status = "Save"
                End If
            Next
            GridView1.DataSource = dtb
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btncomplist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncomplist.Click
        Response.Redirect("spl_approval2.aspx")
    End Sub

End Class