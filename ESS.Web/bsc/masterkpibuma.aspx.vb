﻿Imports System.Data.SqlClient

Partial Public Class masterkpibuma
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") = "" Or Session("permission") = Nothing Then
            Response.Redirect("../Login.aspx", False)
            Exit Sub
        ElseIf ("1;7").IndexOf(Session("permission")) = -1 Then 'yang bisa akses cuman msd
            Response.Redirect("../default.aspx", False)
            Exit Sub
        End If

        If ("5").IndexOf(Session("permission")) <> -1 Then
            btnSave.Visible = False
        End If

        'SH Admin tidak boleh save hanya boleh lihat
        If Session("permission") = "A" Then
            btnSave.Visible = False
        End If
        'Hanya MSD yang boleh delete
        If Session("permission") = "1" Then
            btnDelete.Visible = True
            KPIReady.Enabled = True
        Else
            btnDelete.Visible = False
            KPIReady.Enabled = False
        End If
        '---------------------------------------------
        Dim lengthFunction As String = "function isMaxLength(txtBox,panjang) {"
        lengthFunction += " if(txtBox) { "
        lengthFunction += "     return ( txtBox.value.length <= panjang-1);"
        lengthFunction += " }"
        lengthFunction += "}"

        Dim blurFunction As String = "function isBlurLength(txtBox,panjang) {"
        blurFunction += " if(txtBox) { "
        blurFunction += " if (txtBox.value.length > panjang) {"
        blurFunction += "  alert('Jumlah karakter tidak boleh lebih dari ' + panjang);"
        blurFunction += "  txtBox.value = txtBox.value.substring(1,panjang);"
        blurFunction += "  }"
        blurFunction += " }"
        blurFunction += "}"

        Me.KPIDesc.Attributes.Add("onkeypress", "return isMaxLength(this,1000);")
        Me.KPIDesc.Attributes.Add("onblur", "isBlurLength(this,1000);")
        Me.KPIFDesc.Attributes.Add("onkeypress", "return isMaxLength(this,500);")
        Me.KPIFDesc.Attributes.Add("onblur", "isBlurLength(this,500);")
        Me.KPILeads.Attributes.Add("onkeypress", "return isMaxLength(this,500);")
        Me.KPILeads.Attributes.Add("onblur", "isBlurLength(this,500);")
        Me.KPIProcess.Attributes.Add("onkeypress", "return isMaxLength(this,500);")
        Me.KPIProcess.Attributes.Add("onblur", "isBlurLength(this,500);")

        ClientScript.RegisterClientScriptBlock(Me.[GetType](), "txtLength", lengthFunction, True)
        ClientScript.RegisterClientScriptBlock(Me.[GetType](), "txtBlurLength", blurFunction, True)
        compname.Value = Request.UserHostName
        nikuser.Value = Session("NikUser")

        If Not Page.IsPostBack Then
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim dr As SqlDataReader = Nothing
            Try
                grup.Value = Session("permission")
                KPIRed.Enabled = False
                KPIGreen.Enabled = False
                RedParam.Enabled = False
                GreenParam.Enabled = False
                Dim type As String = ""
                type = Request.QueryString("type")
                Dim cmd As SqlCommand
                Dim query As String = "select id,kdsite,active from site order by id"
                Dim dt As New DataTable
                cmd = New SqlCommand(query, conn)
                conn.Open()
                'Initialize Jobsite
                dr = cmd.ExecuteReader
                While dr.Read
                    'KPIJobsite.Items.Add(dr(1))
                    'If dr(2) = 0 Then
                    '    KPIJobsite.Items(KPIJobsite.Items.Count - 1).Enabled = False
                    'End If
                    If Session("permission") = "1" Or Session("permission") = "B" Or Session("permission") = "8" Or Session("permission") = "5" Then 'GRUP MDV & PIC PDCA
                        KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "2" Or Session("permission") = "7" Or Session("permission") = "A" Then 'GRUP SEK PRO & S.Head
                        If Session("jobsite") = dr(1) Then
                            KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                        End If

                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Function
                query = "select ID,FunctionName,active from MFunction where active='1' order by dirid,id"
                cmd = New SqlCommand(query, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    'KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                    'If dr(2) = 0 Then
                    '    KPIFunc.Items(KPIJobsite.Items.Count - 1).Enabled = False
                    'End If
                    If Session("permission") = "1" Or Session("permission") = "B" Then 'GRUP MDV
                        KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "8" Or Session("permission") = "5" Then 'PIC PDCA 
                        If Session("bscdivisi") = dr(0) Then
                            KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Direktorat
                query = "select ID,DirektoratName,active from MDirektorat where active='1' order by ID"
                cmd = New SqlCommand(query, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Then 'GRUP MDV
                        KPIDirektorat.Items.Add(New ListItem(dr(1), dr(0)))
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Section
                query = "select ID,SectionName,active,funcid from MSection where active='1'  order by ID"
                cmd = New SqlCommand(query, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    'KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                    'If dr(2) = 0 Then
                    '    KPISection.Items(KPISection.Items.Count - 1).Enabled = False
                    'End If
                    If Session("permission") = "7" Or Session("permission") = "A" Then 'GRUP S HEAD
                        If Session("bscdepar") = dr(0) Then
                            KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    ElseIf Session("permission") = "8" Or Session("permission") = "5" Then 'PIC PDCA 
                        If Session("bscdivisi") = dr(3) Then
                            KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    ElseIf Session("permission") = "8" Or Session("permission") = "2" Or Session("permission") = "1" Or Session("permission") = "B" Then
                        KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                    End If
                End While
                KPISection.Visible = True
                dr.Close()
                dr = Nothing
                'Initialize SO
                query = "select a.id, c.abbr,a.SO from MSO a inner join MST b on a.stid=b.stid inner join mperspective c on b.prid=c.id where a.active='1' order by c.id,SO"
                cmd = New SqlCommand(query, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    SO.Items.Add(New ListItem(dr(1) & " - " & dr(2), dr(0)))
                End While
                dr.Close()
                dr = Nothing
                'Initialize KPI Level
                If type <> "" Or type IsNot Nothing Then
                    GenerateKPILevel(type)
                End If
                GenerateKPILevelByPermission(Session("permission"))
                Select Case KPILevel.SelectedValue
                    Case 0
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                        KPIDirektorat.Visible = False
                        searchChild.Visible = True
                    Case 1
                        KPIFunc.Visible = True
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                        KPIDirektorat.Visible = False
                        searchChild.Visible = False
                    Case 2
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = True
                        KPISection.Visible = True
                        KPIDirektorat.Visible = False
                        searchChild.Visible = False
                    Case 3
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = True
                        KPISection.Visible = False
                        KPIDirektorat.Visible = False
                        searchChild.Visible = False
                    Case 4
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                        KPIDirektorat.Visible = True
                        searchChild.Visible = False
                End Select
                GenerateState()
                If ("1;2;8").IndexOf(Session("permission")) <> -1 Then
                    KPIFormula.enabled=true
                else
                    KPIFormula.enabled=false
                End If
                KPIYear.SelectedValue = Now.Year
            Catch ex As Exception
                lblMessage.Text = ex.Message
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                If dr IsNot Nothing Then
                    dr = Nothing
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Value="0" Text=BUMA
    ''' Value="1" Text="Function"
    ''' Value="2" Text="Section"
    ''' Value="3" Text="Jobsite"
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub KPILevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles KPILevel.SelectedIndexChanged
        Try
            Select Case KPILevel.SelectedValue
                Case 0
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                    KPIDirektorat.Visible = False
                    searchChild.Visible = True
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes7", "addRowToFTable();", True)
                Case 1
                    KPIFunc.Visible = True
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                    KPIDirektorat.Visible = False
                    searchChild.Visible = False
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "change", "changeText()", True)
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes7", "addRowToFTable();", True)
                Case 2
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = True
                    KPIDirektorat.Visible = False
                    searchChild.Visible = False
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "change", "changeText()", True)
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes7", "addRowToFTable();", True)
                Case 3
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = False
                    KPIDirektorat.Visible = False
                    searchChild.Visible = False
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "change", "changeText()", True)
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes7", "addRowToFTable();", True)
                Case 4
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                    KPIDirektorat.Visible = True
                    searchChild.Visible = True
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "change", "changeText()", True)
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes7", "addRowToFTable();", True)
            End Select
            KPIID.Value = "[AUTO]"
            ClearField()
            GenerateState()
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "changekpicalc", "onchangekpicalch()", True)
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim addQuery As String = ""
            If Validasi() Then
                Dim sqlsplit() As String = Split(SQLTOSEND.Value, "~#~")
                For Each s As String In sqlsplit
                    Select Case Left(s, 3)
                        Case "DEL"
                            addQuery += "delete mkpiformula where id='" & Split(s, "*#*")(1) & "';"
                        Case "INS"
                            If KPIID.Value = "[AUTO]" Then
                                addQuery += "INSERT INTO MKPIFORMULA (kpiid,name,source,responsibility,prepared,cutofdate,createdby,createdin) values " & _
                                       "($%^,'" & Split(s, "*#*")(1) & "','" & Split(s, "*#*")(2) & "','" & Split(s, "*#*")(3) & "','" & Split(s, "*#*")(4) & "','" & Split(s, "*#*")(5) & "','" & Session("NikUser") & "','" & Request.UserHostName & "');"
                            Else
                                addQuery += "INSERT INTO MKPIFORMULA (kpiid,name,source,responsibility,prepared,cutofdate,createdby,createdin) values " & _
                                       "(" & KPIID.Value & ",'" & Split(s, "*#*")(1) & "','" & Split(s, "*#*")(2) & "','" & Split(s, "*#*")(3) & "','" & Split(s, "*#*")(4) & "','" & Split(s, "*#*")(5) & "','" & Session("NikUser") & "','" & Request.UserHostName & "');"
                            End If
                        Case "UPD"
                            addQuery += "UPDATE MKPIFORMULA set name='" & Split(s, "*#*")(1) & "'," & _
                                        "modifiedby='" & Session("NikUser") & "',modifiedtime=getdate(),modifiedin='" & Request.UserHostName & "'," & _
                                        "source='" & Split(s, "*#*")(2) & "',responsibility='" & Split(s, "*#*")(3) & "'," & _
                                        "prepared='" & Split(s, "*#*")(4) & "',cutofdate='" & Split(s, "*#*")(5) & "' where id=" & Split(s, "*#*")(6) & ";"
                    End Select
                Next
                Dim query As String = ""
                Dim result As String
                Dim cmd As SqlCommand
                If KPIID.Value = "[AUTO]" Then
                    query = "BEGIN TRANSACTION; BEGIN TRY "
                    Select Case KPILevel.SelectedValue
                        Case "0" 'BUMA
                            query += "insert into MKPI " _
                                    & " (Level,SOID,Name,Dir,Func,Jobsite,Section,UoM,Owner,Type,[Desc],Period,Leads,Formula,FormulaDesc,RedParam,GreenParam,BasedOn,CalcType,Parent,CreatedBy,CreatedIn,RedCond,GreenCond,KPIType,KPILeads,Process,Targetting,StEdit,Year, Bobot) values " _
                                    & "('" & KPILevel.SelectedValue & "','" & SO.SelectedValue & "','" & KPIName.Text & "'," _
                                    & "'','','','','" _
                                    & KPIUOM.Text & "','" & KPIOwner.Text & "','','" _
                                    & KPIDesc.Text & "','" & KPIPeriod.SelectedValue & "','','" _
                                    & KPIFormula.SelectedValue & "','" & KPIFDesc.Text & "','" & KPIRed.Text & "','" _
                                    & KPIGreen.Text & "','" & KPIBased.SelectedValue & "','" & IIf(KPICalcTypeH.SelectedValue = "0", KPICalcTypeD.SelectedValue, KPICalcTypeC.SelectedValue) & "','','" & Session("NikUser") & "','" & Request.UserHostName & "','" & RedParam.SelectedValue & "','" & GreenParam.SelectedValue & "','" & KPIType.SelectedValue & "','" & KPILeads.Text & "','" & KPIProcess.Text & "','" & KPITargetting.SelectedValue & "','" & KPIReady.SelectedValue & "','" & KPIYear.SelectedValue & "'," & KPIBobot.Text & ");"
                        Case "1" 'FUnc
                            query += "insert into MKPI " _
                                    & " (Level,SOID,Name,Dir,Func,Jobsite,Section,UoM,Owner,Type,[Desc],Period,Leads,Formula,FormulaDesc,RedParam,GreenParam,BasedOn,CalcType,Parent,CreatedBy,CreatedIn,RedCond,GreenCond,KPIType,KPILeads,Process,Targetting,StEdit,Year,Bobot) values " _
                                    & "('" & KPILevel.SelectedValue & "','" & SO.SelectedValue & "','" & KPIName.Text & "',''," _
                                    & "'" & KPIFunc.SelectedValue & "','','','" _
                                    & KPIUOM.Text & "','" & KPIOwner.Text & "','','" _
                                    & KPIDesc.Text & "','" & KPIPeriod.SelectedValue & "','','" _
                                    & KPIFormula.SelectedValue & "','" & KPIFDesc.Text & "','" & KPIRed.Text & "','" _
                                    & KPIGreen.Text & "','" & KPIBased.SelectedValue & "','" & IIf(KPICalcTypeH.SelectedValue = "0", KPICalcTypeD.SelectedValue, KPICalcTypeC.SelectedValue) & "','','" & Session("NikUser") & "','" & Request.UserHostName & "','" & RedParam.SelectedValue & "','" & GreenParam.SelectedValue & "','" & KPIType.SelectedValue & "','" & KPILeads.Text & "','" & KPIProcess.Text & "','" & KPITargetting.SelectedValue & "','" & KPIReady.SelectedValue & "','" & KPIYear.SelectedValue & "'," & KPIBobot.Text & ");"
                        Case "2" ' Section
                            query += "insert into MKPI " _
                                    & " (Level,SOID,Name,Dir,Func,Jobsite,Section,UoM,Owner,Type,[Desc],Period,Leads,Formula,FormulaDesc,RedParam,GreenParam,BasedOn,CalcType,Parent,CreatedBy,CreatedIn,RedCond,GreenCond,KPIType,KPILeads,Process,Targetting,StEdit,Year,Bobot) values " _
                                    & "('" & KPILevel.SelectedValue & "','" & SO.SelectedValue & "','" & KPIName.Text & "',''," _
                                    & "'','" & KPIJobsite.SelectedValue & "','" & KPISection.SelectedValue & "','" _
                                    & KPIUOM.Text & "','" & KPIOwner.Text & "','','" _
                                    & KPIDesc.Text & "','" & KPIPeriod.SelectedValue & "','','" _
                                    & KPIFormula.SelectedValue & "','" & KPIFDesc.Text & "','" & KPIRed.Text & "','" _
                                    & KPIGreen.Text & "','" & KPIBased.SelectedValue & "','" & IIf(KPICalcTypeH.SelectedValue = "0", KPICalcTypeD.SelectedValue, KPICalcTypeC.SelectedValue) & "','','" & Session("NikUser") & "','" & Request.UserHostName & "','" & RedParam.SelectedValue & "','" & GreenParam.SelectedValue & "','" & KPIType.SelectedValue & "','" & KPILeads.Text & "','" & KPIProcess.Text & "','" & KPITargetting.SelectedValue & "','" & KPIReady.SelectedValue & "','" & KPIYear.SelectedValue & "'," & KPIBobot.Text & ");"
                        Case "3" 'Jobsite
                            query += "insert into MKPI " _
                                    & " (Level,SOID,Name,Dir,Func,Jobsite,Section,UoM,Owner,Type,[Desc],Period,Leads,Formula,FormulaDesc,RedParam,GreenParam,BasedOn,CalcType,Parent,CreatedBy,CreatedIn,RedCond,GreenCond,KPIType,KPILeads,Process,Targetting,StEdit,Year,Bobot) values " _
                                    & "('" & KPILevel.SelectedValue & "','" & SO.SelectedValue & "','" & KPIName.Text & "',''," _
                                    & "'','" & KPIJobsite.SelectedValue & "','','" _
                                    & KPIUOM.Text & "','" & KPIOwner.Text & "','','" _
                                    & KPIDesc.Text & "','" & KPIPeriod.SelectedValue & "','','" _
                                    & KPIFormula.SelectedValue & "','" & KPIFDesc.Text & "','" & KPIRed.Text & "','" _
                                    & KPIGreen.Text & "','" & KPIBased.SelectedValue & "','" & IIf(KPICalcTypeH.SelectedValue = "0", KPICalcTypeD.SelectedValue, KPICalcTypeC.SelectedValue) & "','','" & Session("NikUser") & "','" & Request.UserHostName & "','" & RedParam.SelectedValue & "','" & GreenParam.SelectedValue & "','" & KPIType.SelectedValue & "','" & KPILeads.Text & "','" & KPIProcess.Text & "','" & KPITargetting.SelectedValue & "','" & KPIReady.SelectedValue & "','" & KPIYear.SelectedValue & "'," & KPIBobot.Text & ");"
                        Case "4" 'Direktorat
                            query += "insert into MKPI " _
                                & " (Level,SOID,Name,Dir,Func,Jobsite,Section,UoM,Owner,Type,[Desc],Period,Leads,Formula,FormulaDesc,RedParam,GreenParam,BasedOn,CalcType,Parent,CreatedBy,CreatedIn,RedCond,GreenCond,KPIType,KPILeads,Process,Targetting,StEdit,Year,Bobot) values " _
                                & "('" & KPILevel.SelectedValue & "','" & SO.SelectedValue & "','" & KPIName.Text & "','" & KPIDirektorat.SelectedValue & "'," _
                                & "'','','','" _
                                & KPIUOM.Text & "','" & KPIOwner.Text & "','','" _
                                & KPIDesc.Text & "','" & KPIPeriod.SelectedValue & "','','" _
                                & KPIFormula.SelectedValue & "','" & KPIFDesc.Text & "','" & KPIRed.Text & "','" _
                                & KPIGreen.Text & "','" & KPIBased.SelectedValue & "','" & IIf(KPICalcTypeH.SelectedValue = "0", KPICalcTypeD.SelectedValue, KPICalcTypeC.SelectedValue) & "','','" & Session("NikUser") & "','" & Request.UserHostName & "','" & RedParam.SelectedValue & "','" & GreenParam.SelectedValue & "','" & KPIType.SelectedValue & "','" & KPILeads.Text & "','" & KPIProcess.Text & "','" & KPITargetting.SelectedValue & "','" & KPIReady.SelectedValue & "','" & KPIYear.SelectedValue & "'," & KPIBobot.Text & ");"
                    End Select
                    query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select IDENT_CURRENT('MKPI');end "
                Else
                    query = "BEGIN TRANSACTION; BEGIN TRY "
                    query += "Update MKPI Set " _
                            & "SOID='" & SO.SelectedValue & "',Name='" & KPIName.Text & "'," _
                            & "UoM='" & KPIUOM.Text & "', Owner='" & KPIOwner.Text & "'," _
                            & "Type='', [Desc]='" & KPIDesc.Text & "'," _
                            & "Period='" & KPIPeriod.SelectedValue & "',Leads=''," _
                            & "Formula='" & KPIFormula.SelectedValue & "',FormulaDesc='" & KPIFDesc.Text & "'," _
                            & "RedParam=" & KPIRed.Text & ",GreenParam=" & KPIGreen.Text & "," _
                            & "BasedOn='" & KPIBased.SelectedValue & "',CalcType='" & IIf(KPICalcTypeH.SelectedValue = "0", KPICalcTypeD.SelectedValue, KPICalcTypeC.SelectedValue) & "'," _
                            & "RedCond='" & RedParam.SelectedValue & "',GreenCond='" & GreenParam.SelectedValue & "'," _
                            & "KPIType='" & KPIType.SelectedValue & "',KPILeads='" & KPILeads.Text & "'," _
                            & "Process='" & KPIProcess.Text & "',Targetting='" & KPITargetting.SelectedValue & "'," _
                            & "StEdit='" & KPIReady.SelectedValue & "',Bobot=" & KPIBobot.Text & "," _
                            & "ModifiedBy='" & Session("NikUser") & "', modifiedtime=getdate() where " _
                            & "ID='" & KPIID.Value & "';"
                    query += addQuery
                    query += Calculate(IIf(KPICalcTypeH.SelectedValue = "0", KPICalcTypeD.SelectedValue, KPICalcTypeC.SelectedValue), KPIID.Value)
                    If KPILevel.SelectedValue = "0" Then 'BUMA
                        query += "update mkpi set soid='" & SO.SelectedValue & "' where ID in (select idchild from KPIRELATION where IDParent='" & KPIID.Value & "');"
                        query += "update mkpi set stedit='" & KPIReady.SelectedValue & "' where ID in (select idchild from KPIRELATION where IDParent='" & KPIID.Value & "');"
                        query += "update mkpi set soid='" & SO.SelectedValue & "' where ID in (select idchild from KPIRelation where idparent in (select idchild from KPIRELATION where IDParent='" & KPIID.Value & "'));"
                        query += "update mkpi set stedit='" & KPIReady.SelectedValue & "' where ID in (select idchild from KPIRelation where idparent in (select idchild from KPIRELATION where IDParent='" & KPIID.Value & "'));"
                    ElseIf KPILevel.SelectedValue = "4" Then 'DIREKTORAT
                        query += "update mkpi set soid='" & SO.SelectedValue & "' where ID in (select idchild from KPIRELATION where IDParent='" & KPIID.Value & "');"
                        query += "update mkpi set stedit='" & KPIReady.SelectedValue & "' where ID in (select idchild from KPIRELATION where IDParent='" & KPIID.Value & "');"
                    End If
                    'Update Calculation Type
                    'query += "update mkpi set calctype='" & IIf(KPICalcTypeH.SelectedValue = "0", KPICalcTypeD.SelectedValue, IIf(KPICalcTypeC.SelectedValue = "4", "2", KPICalcTypeC.SelectedValue)) & "' where ID in (select idchild from KPIRELATION where IDParent='" & KPIID.Value & "' and includecount=1);"
                    '---------------------------------------------------------
                    query += "; END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
                End If

                cmd = New SqlCommand(query, conn)
                conn.Open()
                result = cmd.ExecuteScalar()
                If IsNumeric(result) Then
                    If KPIReady.SelectedValue="1" then 'Non Aktif
                        btnNewChild.Disabled = True
                        searchChild.Disabled = True
                    else
                        btnNewChild.disabled=false
                        searchChild.disabled=false
                    End If
                    If result = 0 Then
                        'If KPIFormula.SelectedValue <> 0 Then
                        cmd = New SqlCommand()
                        cmd.CommandText = "sp_recount"
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Connection = conn
                        cmd.Parameters.AddWithValue("kpiid", KPIID.Value)
                        cmd.ExecuteScalar()
                        cmd = New SqlCommand()
                        cmd.CommandText = "sp_recount_parent"
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Connection = conn
                        cmd.Parameters.AddWithValue("@kpiid", KPIID.Value)
                        cmd.ExecuteScalar()
                        cmd = New SqlCommand()
                        cmd.CommandText = "sp_recount_grandparent"
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Connection = conn
                        cmd.Parameters.AddWithValue("@kpiid", KPIID.Value)
                        cmd.ExecuteScalar()
                        'End If
                        lblMessage.Text = "**Update Berhasil**"
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes2", "alert('Update Berhasil');", True)
                    Else
                        KPIID.Value = result
                        If Trim(addQuery) = "" Then
                            If IsNumeric(result) Then
                                lblMessage.Text = "**Save Berhasil**ID KPI :" & CStr(KPIID.Value)
                                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes2", "alert('Save Berhasil');", True)
                            End If
                        Else
                            addQuery = Replace(addQuery, "$%^", KPIID.Value)
                            addQuery = "BEGIN TRANSACTION; BEGIN TRY " & addQuery
                            addQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
                            cmd = New SqlCommand(addQuery, conn)
                            result = cmd.ExecuteScalar()
                            If IsNumeric(result) Then
                                lblMessage.Text = "**Save Berhasil**ID KPI :" & CStr(KPIID.Value)
                                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes2", "alert('Save Berhasil');", True)
                            End If
                        End If
                    End If
                Else
                    lblMessage.Text = result
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes2", "alert('Save Gagal');", True)
                End If
                End If
            GenerateState()
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes4", "disablenewchild(" & KPIReady.SelectedValue & ")", True)
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes", "retrieveChild()", True)
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes3", "retrieveFormula()", True)
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes1", "changeText()", True)
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes5", "onchangekpicalch();setdisabled(true);", True)
            If Trim(addQuery) = "" Then
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes5", "addRowToFTable();", True)
            End If

        Catch ex As Exception
            lblMessage.Text = ex.Message
        Finally
            conn.Close()
        End Try
    End Sub

    ''' <summary>
    ''' Value="0" Text=BUMA
    ''' Value="1" Text="Function"
    ''' Value="2" Text="Section"
    ''' Value="3" Text="Jobsite"
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GenerateFunction() As String
        Try
            Dim result As String = ""
            Select Case KPILevel.SelectedValue
                Case 0
                    Return ""
                Case 1
                    For Each c As ListItem In KPIFunc.Items
                        If c.Selected Then
                            result += "1"
                        Else
                            result += "0"
                        End If
                    Next
                    Return result
                Case 2
                    Return ""
                Case 3
                    Return ""
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
        Return ""
    End Function

    ''' <summary>
    ''' Value="0" Text=BUMA
    ''' Value="1" Text="Function"
    ''' Value="2" Text="Section"
    ''' Value="3" Text="Jobsite"
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GenerateSection() As String
        Try
            Dim result As String = ""
            Select Case KPILevel.SelectedValue
                Case 0
                    Return ""
                Case 1
                    Return ""
                Case 2
                    For Each c As ListItem In KPISection.Items
                        If c.Selected Then
                            result += "1"
                        Else
                            result += "0"
                        End If
                    Next
                    Return result
                Case 3
                    Return ""
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
        Return ""
    End Function

    ''' <summary>
    ''' Value="0" Text=BUMA
    ''' Value="1" Text="Function"
    ''' Value="2" Text="Section"
    ''' Value="3" Text="Jobsite"
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GenerateJobsite() As String
        Try
            Dim result As String = ""
            Select Case KPILevel.SelectedValue
                Case 0
                    Return ""
                Case 1
                    Return ""
                Case 2
                    For Each c As ListItem In KPIJobsite.Items
                        If c.Selected Then
                            result += "1"
                        Else
                            result += "0"
                        End If
                    Next
                    Return result
                Case 3
                    For Each c As ListItem In KPIJobsite.Items
                        If c.Selected Then
                            result += "1"
                        Else
                            result += "0"
                        End If
                    Next
                    Return result
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
        Return ""
    End Function

    Sub GenerateKPILevel(ByVal type As String)
        Try
            Select Case type
                Case "0" 'BUMA
                    For Each l As ListItem In KPILevel.Items
                        If l.Value = "0" Or l.Value = "3" Then
                            l.Enabled = False
                        End If
                    Next
                    KPIFunc.Visible = True
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                Case "1" 'Function
                    For Each l As ListItem In KPILevel.Items
                        If l.Value = "0" Or l.Value = "1" Or l.Value = "3" Then
                            l.Enabled = False
                        End If
                    Next
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = False
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Sub GenerateKPILevelByPermission(ByVal permission As String)
        Try
            Select Case permission
                Case "1" 'MDV
                    For Each l As ListItem In KPILevel.Items
                        If l.Value = "0" Then
                            l.Selected = True
                        End If
                        'Dicomment berdasarkan email
                        'If l.Value = "2" Or l.Value = "3" Then
                        '    l.Enabled = False
                        'End If
                    Next
                Case "B" 'MDV Section Head
                    For Each l As ListItem In KPILevel.Items
                        If l.Value = "0" Then
                            l.Selected = True
                        End If
                        'Dicomment berdasarkan email
                        'If l.Value = "2" Or l.Value = "3" Then
                        '    l.Enabled = False
                        'End If
                    Next
                Case "2" 'SKEPRO
                    For Each l As ListItem In KPILevel.Items
                        If l.Value = "0" Or l.Value = "1" Then
                            l.Enabled = False
                        End If
                        If l.Value = "3" Then
                            l.Selected = True
                        End If
                    Next
                Case "7" 'S HEAD
                    For Each l As ListItem In KPILevel.Items
                        If l.Value = "0" Or l.Value = "1" Or l.Value = "3" Then
                            l.Enabled = False
                        End If
                        If l.Value = "2" Then
                            l.Selected = True
                        End If
                    Next
                Case "A" ' S Head Admin
                     For Each l As ListItem In KPILevel.Items
                        If l.Value = "0" Or l.Value = "1" Or l.Value = "3" Then
                            l.Enabled = False
                        End If
                        If l.Value = "2" Then
                            l.Selected = True
                        End If
                    Next
                Case "8" 'PIC PDCA
                    For Each l As ListItem In KPILevel.Items
                        If l.Value = "0" Or l.Value = "3" Then
                            l.Enabled = False
                        End If
                        If l.Value = "1" Then
                            l.Selected = True
                        End If
                    Next
                Case "5" 'Manager
                    For Each l As ListItem In KPILevel.Items
                        If l.Value = "0" Or l.Value = "3" Then
                            l.Enabled = False
                        End If
                        If l.Value = "1" Then
                            l.Selected = True
                        End If
                    Next
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Function Validasi() As Boolean
        Try
            If Trim(KPIName.Text) = "" Then
                lblMessage.Text = "***Nama KPI Wajib Diisi"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                Return False
            ElseIf Trim(KPIUOM.Text) = "" Then
                lblMessage.Text = "***Unit Of Measurement KPI Wajib Diisi"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                Return False
            ElseIf Trim(KPIBobot.Text) = "" Then
                lblMessage.Text = "***Bobot KPI Wajib Diisi"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                Return False
            ElseIf Trim(KPIOwner.Text) = "" Then
                lblMessage.Text = "***KPI Owner Wajib Diisi"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                Return False
            ElseIf Trim(KPIRed.Text) = "" Then
                lblMessage.Text = "***Red Parameter Wajib Diisi"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                Return False
            ElseIf Trim(KPIGreen.Text) = "" Then
                lblMessage.Text = "***Green Parameter Wajib Diisi"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                Return False
            ElseIf Trim(KPIDesc.Text) = "" Then
                lblMessage.Text = "***Deskripsi KPI Wajib Diisi"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                Return False
            ElseIf Not IsNumeric(KPIRed.Text) Then
                lblMessage.Text = "***Nilai Red Parameter Harus Berupa Numerik"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                Return False
            ElseIf Not IsNumeric(KPIGreen.Text) Then
                lblMessage.Text = "***Nilai Green Parameter Harus Berupa Numerik"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                Return False
            ElseIf Not IsNumeric(KPIBobot.Text) Then
                lblMessage.Text = "***Bobot KPI Harus Berupa Numerik"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                Return False
            End If
            KPIName.Text = KPIName.Text.Replace("'", "`")
            KPIUOM.Text = KPIUOM.Text.Replace("'", "`")
            KPIOwner.Text = KPIOwner.Text.Replace("'", "`")
            KPIDesc.Text = KPIDesc.Text.Replace("'", "`")
            KPIFDesc.Text = KPIFDesc.Text.Replace("'", "`")
            KPIBobot.Text = KPIBobot.Text.replace(",",".")
            If Len(KPIDesc.Text) > 1000 Then
                lblMessage.Text = "***Panjang Karakter KPI Description Maks. 1000"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                Return False
            ElseIf Len(KPIFDesc.Text) > 500 Then
                lblMessage.Text = "***Panjang Karakter KPI F Description Maks. 500"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                Return False
            End If
            If KPIID.Value <> "[AUTO]" Then
                Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
                Try
                    Dim sqlQuery As String = "select count(*) from calccontent where kpiid='" & KPIID.Value & "' and CalcType='" & KPICalcTypeC.SelectedValue & "'"
                    Dim cmd As New SqlCommand(sqlQuery, conn)
                    Dim result As Integer = 0
                    If KPICalcTypeH.SelectedValue = "1" Then
                        conn.Open()
                        result = cmd.ExecuteScalar
                        conn.Close()
                        If result = 0 Then
                            lblMessage.Text = "***Calculation Type yang bersifat DISCRETE harus melakukan SET DATA terlebih dahulu"
                            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                            Return False
                        End If
                    End If
                    sqlQuery = "select IsNull(Max(StEdit),0) as StEdit from MKPI where id in (select id from mkpi where id in (select idparent from KPIRELATION where idchild='" & KPIID.Value & "') and level <> '3') and level <> '3'"
                    cmd = New SqlCommand(sqlQuery, conn)
                    result = 0
                    conn.Open()
                    result = cmd.ExecuteScalar
                    If result = 1 And KPIReady.SelectedValue = 0 Then
                        lblMessage.Text = "***KPI Parent dalam keadaan OFF"
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                        Return False
                    End If
                    conn.Close()
                Catch ex As Exception
                    lblMessage.Text = ex.Message
                Finally
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                End Try
            End If

            Return True
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Function

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            If KPIID.Value = "[AUTO]" Then
                lblMessage.Text = "Harap Memilih KPI yang akan dihapus"
            Else
                Dim query As String = ""
                Dim result As String = ""
                Dim stedit As String = ""
                Dim cmd As SqlCommand
                cmd = New SqlCommand("select stedit from mkpi where id='" & KPIID.Value & "'", conn)
                conn.Open()
                stedit = cmd.ExecuteScalar()
                If stedit = "0" Then
                    lblMessage.Text = "***Harap Me-Non Aktifkan terlebih dahulu KPI yang ingin dihapus"
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes", "retrieveChild()", True)
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes3", "retrieveFormula()", True)
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes1", "changeText()", True)
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes4", "onchangekpicalch();", True)
                Else
                    If KPILevel.SelectedValue = "3" Then 'HAPUS JOBSITE
                        query = "BEGIN TRANSACTION; BEGIN TRY "
                        query += "INSERT INTO DMKPI SELECT * FROM MKPI WHERE MKPI.ID='" & KPIID.Value & "';"
                        query += "UPDATE DMKPI SET DELETEDBY='" & Session("NikUser") & "',DELETEDTIME=GETDATE(),DELETEDIN='" & Request.UserHostName & "' WHERE ID='" & KPIID.Value & "';"
                        query += "INSERT INTO  DTBSC SELECT * FROM TBSC WHERE TBSC.KPIID='" & KPIID.Value & "';"
                        query += "INSERT INTO  DKPIRELATION  SELECT * FROM KPIRELATION WHERE KPIRELATION.IDPARENT='" & KPIID.Value & "' OR KPIRELATION.IDCHILD='" & KPIID.Value & "';"
                        query += "DELETE MKPI WHERE ID='" & KPIID.Value & "';"
                        query += "DELETE TBSC WHERE KPIID='" & KPIID.Value & "';"
                        query += "DELETE KPIRELATION WHERE IDPARENT='" & KPIID.Value & "' OR IDCHILD='" & KPIID.Value & "';"
                        query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
                    Else
                        query = "BEGIN TRANSACTION; BEGIN TRY "
                        query += "INSERT INTO DMKPI SELECT * FROM MKPI WHERE MKPI.ID='" & KPIID.Value & "';"
                        query += "UPDATE DMKPI SET DELETEDBY='" & Session("NikUser") & "',DELETEDTIME=GETDATE(),DELETEDIN='" & Request.UserHostName & "' WHERE ID='" & KPIID.Value & "';"
                        query += "INSERT INTO DTBSC SELECT * FROM TBSC WHERE TBSC.KPIID='" & KPIID.Value & "';"
                        query += "INSERT INTO DMKPI SELECT * FROM MKPI WHERE MKPI.ID in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT='" & KPIID.Value & "');"
                        query += "UPDATE DMKPI SET DELETEDBY='" & Session("NikUser") & "',DELETEDTIME=GETDATE(),DELETEDIN='" & Request.UserHostName & "' WHERE ID in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT='" & KPIID.Value & "');"
                        query += "INSERT INTO DTBSC SELECT * FROM TBSC WHERE TBSC.KPIID in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT='" & KPIID.Value & "');"
                        query += "INSERT INTO DMKPI SELECT * FROM MKPI WHERE MKPI.ID in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT='" & KPIID.Value & "'));"
                        query += "UPDATE DMKPI SET DELETEDBY='" & Session("NikUser") & "',DELETEDTIME=GETDATE(),DELETEDIN='" & Request.UserHostName & "' WHERE ID in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT='" & KPIID.Value & "'));"
                        query += "INSERT INTO DTBSC SELECT * FROM TBSC WHERE TBSC.KPIID in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT='" & KPIID.Value & "'));"
                        query += "INSERT INTO DKPIRELATION SELECT * FROM KPIRELATION WHERE KPIRELATION.IDPARENT='" & KPIID.Value & "' OR KPIRELATION.IDCHILD='" & KPIID.Value & "';"
                        query += "INSERT INTO DKPIRELATION SELECT * FROM KPIRELATION WHERE KPIRELATION.IDPARENT in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT='" & KPIID.Value & "');"
                        query += "DELETE MKPI WHERE ID='" & KPIID.Value & "';"
                        query += "DELETE TBSC WHERE KPIID='" & KPIID.Value & "';"
                        query += "DELETE MKPI WHERE ID in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT='" & KPIID.Value & "');"
                        query += "DELETE TBSC WHERE KPIID in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT='" & KPIID.Value & "');"
                        query += "DELETE MKPI WHERE ID in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT='" & KPIID.Value & "'));"
                        query += "DELETE TBSC WHERE KPIID in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT='" & KPIID.Value & "'));"
                        query += "DELETE KPIRELATION WHERE IDPARENT='" & KPIID.Value & "' OR IDCHILD='" & KPIID.Value & "';"
                        query += "DELETE KPIRELATION WHERE IDPARENT in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT='" & KPIID.Value & "');"
                        query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
                    End If
                    cmd = New SqlCommand(query, conn)

                    result = cmd.ExecuteScalar()
                    If result = 0 Then
                        cmd = New SqlCommand()
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.CommandText = "sp_recount_parent"
                        cmd.Parameters.AddWithValue("@kpiid", KPIID.Value)
                        cmd.Connection = conn
                        cmd.ExecuteScalar()
                        cmd = New SqlCommand()
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.CommandText = "sp_recount_grandparent"
                        cmd.Parameters.AddWithValue("@kpiid", KPIID.Value)
                        cmd.Connection = conn
                        cmd.ExecuteScalar()
                        conn.Close()
                        lblMessage.Text = "Hapus Berhasil"
                        Response.Redirect("masterkpibuma.aspx")
                    Else
                        conn.Close()
                        lblMessage.Text = result
                    End If
                End If
            End If
        Catch ex As Exception
            lblMessage.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        Response.Redirect("masterkpibuma.aspx")
    End Sub

    Sub ClearField()
        KPIName.Text = ""
        KPIUOM.Text = ""
        KPIOwner.Text = ""
        KPIDesc.Text = ""
        KPIPeriod.SelectedIndex = 0
        KPIFDesc.Text = ""
        KPIFormula.SelectedIndex = 0
        KPIBased.SelectedIndex = 0
        KPICalcTypeH.SelectedIndex = 0
        KPILeads.Text = ""
        KPIProcess.Text = ""
    End Sub

    Sub GenerateState()
        If KPIID.Value = "[AUTO]" Then
            lblState.Text = "Kondisi : Data Baru"
        Else
            lblState.Text = "Kondisi : Edit Data"
        End If
    End Sub

    Private Sub KPICalcTypeH_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles KPICalcTypeH.SelectedIndexChanged
        Try
            Select Case KPICalcTypeH.SelectedValue
                Case "0" 'Discrete
                    For i As Integer = 0 To KPICalcTypeH.Items.Count - 1
                        If i <> 0 And i <> 1 Then
                            KPICalcTypeH.Items(i).Enabled = False
                        End If
                    Next
                Case "1" 'Continur
                    For i As Integer = 0 To KPICalcTypeH.Items.Count - 1
                        If i = 0 And i = 1 Then
                            KPICalcTypeH.Items(i).Enabled = False
                        End If
                    Next
            End Select
        Catch ex As Exception

        End Try
    End Sub

    Function Calculate(ByVal CalcType As String, ByVal kpiid As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim sqlquery As String = "Select calctype from mkpi where id='" & kpiid & "'"
            Dim oricalctype As String = ""
            Dim cmd As New SqlCommand(sqlquery, conn)
            conn.Open()
            oricalctype = cmd.ExecuteScalar()
            If oricalctype <> CalcType Then
                Dim dt As New DataTable()
                If CalcType = "2" Or CalcType = "3" Or CalcType = "4" Then
                    sqlquery = "Select * from dbo.CALCCONTENT where kpiid='" & kpiid & "' and CalcType='" & CalcType & "'"
                    Dim da As New SqlDataAdapter(sqlquery, conn)
                    da.Fill(dt)
                End If
                sqlquery = "select * from TBSC where kpiid='" & kpiid & "' and approved='0'"
                Dim reader As SqlDataReader
                cmd = New SqlCommand(sqlquery, conn)
                reader = cmd.ExecuteReader()
                sqlquery = ""
                If reader.HasRows Then
                    Dim returnQuery As String = ""
                    While reader.Read
                        If IsDBNull(reader("target")) And IsDBNull(reader("Actual")) Then
                        ElseIf CalcType = "0" Or CalcType = "1" Then 'MIN OR MAX
                            If IsDBNull(reader("target")) Or IsDBNull(reader("Actual")) Then
                            Else
                                If CalcType = "0" Then 'MIN
                                    If CDec(reader("actual")) = 0 Then
                                        sqlquery += "update TBSC set achievement='100' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    Else
                                        sqlquery += "update TBSC set achievement='" & IIf(Math.Round(CDec(reader("target")) / CDec(reader("actual")) * 100, 2) > 105, 105, Math.Round(CDec(reader("target")) / CDec(reader("actual")) * 100, 2)) & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    End If
                                Else
                                    If CDec(reader("target")) = 0 And CDec(reader("actual")) = 0 Then 'MAX
                                        sqlquery += "update TBSC set achievement='100' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    ElseIf CDec(reader("target")) = 0 Then
                                        sqlquery += "update TBSC set achievement='0' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    Else
                                        sqlquery += "update TBSC set achievement='" & IIf(Math.Round(CDec(reader("actual")) / CDec(reader("target")) * 100, 2) > 105, 105, Math.Round(CDec(reader("actual")) / CDec(reader("target")) * 100, 2)) & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    End If
                                End If
                            End If
                        Else
                            Select Case CalcType
                                Case 2
                                    If (reader("actual") <= dt.Rows(0)("e2") And reader("actual") >= dt.Rows(0)("e1")) Then
                                        sqlquery += "update TBSC set achievement='" & dt.Rows(0)("ea") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    ElseIf (reader("actual") <= dt.Rows(0)("mr2") And reader("actual") >= dt.Rows(0)("mr1")) Then
                                        sqlquery += "update TBSC set achievement='" & dt.Rows(0)("mra") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    ElseIf (reader("actual") <= dt.Rows(0)("bl2") And reader("actual") >= dt.Rows(0)("bl1")) Then
                                        sqlquery += "update TBSC set achievement='" & dt.Rows(0)("bla") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    ElseIf (reader("actual") > dt.Rows(0)("p2")) Then
                                        sqlquery += "update TBSC set achievement='" & dt.Rows(0)("pa") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    Else
                                        sqlquery += "update TBSC set achievement='" & 100 & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    End If
                                Case 3
                                    If (reader("actual") <= dt.Rows(0)("e2") And reader("actual") >= dt.Rows(0)("e1")) Then
                                        sqlquery += "update TBSC set achievement='" & dt.Rows(0)("ea") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    ElseIf (reader("actual") <= dt.Rows(0)("mr2") And reader("actual") >= dt.Rows(0)("mr1")) Or (reader("actual") <= dt.Rows(0)("mr4") And reader("actual") >= dt.Rows(0)("mr3")) Then
                                        sqlquery += "update TBSC set achievement='" & dt.Rows(0)("mra") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    ElseIf (reader("actual") <= dt.Rows(0)("bl2") And reader("actual") >= dt.Rows(0)("bl1")) Or (reader("actual") <= dt.Rows(0)("bl4") And reader("actual") >= dt.Rows(0)("bl3")) Then
                                        sqlquery += "update TBSC set achievement='" & dt.Rows(0)("bla") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    ElseIf (reader("actual") > dt.Rows(0)("p2") Or reader("actual") < dt.Rows(0)("p1")) Then
                                        sqlquery += "update TBSC set achievement='" & dt.Rows(0)("pa") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    Else
                                        sqlquery += "update TBSC set achievement='" & 100 & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    End If
                                Case 4
                                    If (reader("actual") <= dt.Rows(0)("e2") And reader("actual") >= dt.Rows(0)("e1")) Then
                                        sqlquery += "update TBSC set achievement='" & dt.Rows(0)("ea") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    ElseIf (reader("actual") <= dt.Rows(0)("mr2") And reader("actual") >= dt.Rows(0)("mr1")) Then
                                        sqlquery += "update TBSC set achievement='" & dt.Rows(0)("mra") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    ElseIf (reader("actual") <= dt.Rows(0)("bl2") And reader("actual") >= dt.Rows(0)("bl1")) Then
                                        sqlquery += "update TBSC set achievement='" & dt.Rows(0)("bla") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    ElseIf (reader("actual") <= dt.Rows(0)("p2")) Then
                                        sqlquery += "update TBSC set achievement='" & dt.Rows(0)("pa") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    Else
                                        sqlquery += "update TBSC set achievement='" & 100 & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                    End If
                            End Select
                        End If
                    End While
                    Return sqlquery
                Else
                    conn.Close()
                    Return ""
                End If
            Else
                conn.Close()
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            Return ""
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    Function CalculateFormulaKonsolidasi(ByVal kpiid As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim sqlquery As String = "select a.month,a.year,a.kpiid,a.actual,b.formula,case b.Formula when '1' then MAX(child.actual) when '2' then SUM(child.actual)when '3' then AVG(child.actual) end as datakonsolidasi from TBSC a left join MKPI b on a.kpiid=b.ID left join KPIRELATION rel on a.kpiid = rel.IDParent and rel.IncludeCount=1 left join TBSC child on child.kpiid=rel.IDChild and a.month=child.month and a.year=child.year where a.kpiid='" & kpiid & "' and a.approved='0' group by a.month,a.year,a.kpiid,a.actual,b.formula"
            Dim sql As String = ""
            Dim cmd As New SqlCommand(sqlquery, conn)
            Dim dr As SqlDataReader
            conn.Open()
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    sql += "UPDATE TBSC SET ACTUAL=" & dr("datakonsolidasi")
                End While
            End If
            conn.Close()
            Return sqlquery
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            Return ""
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function
End Class