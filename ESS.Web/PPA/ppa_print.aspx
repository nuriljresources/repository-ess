﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ppa_print.aspx.vb" Inherits="EXCELLENT.ppa_print" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PPA</title>
    <style type="text/css">


body{
  padding: 1em;
  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAAQElEQVQIW2P89OvDfwYo+PHjJ4zJwMHBzsAIk0SXAKkCS2KTAEu++vQSbizIKGQAl0SXAJkGlsQmAbcT2Shk+wH0sCzAEOZW1AAAAABJRU5ErkJggg==);
}
a{
  color: #739931;
}
.page{
  max-width: 70em;
  margin: 0 auto;
}
table th,
table td{
  text-align: left;
}
table.layout{
  width: 100%;
  border-collapse: collapse;
}
table.display{
  margin: 1em 0;
}
table.display th,
table.display td{
  border: 1px solid #B3BFAA;
  padding: .5em 1em;
}

table.display th{ background: #D5E0CC; }
table.display td{ background: #fff; }

table.responsive-table{
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
}

.center{
  text-align:center;
  }
.none{
	border-style:none;
    text-align:center;
  }
  
  /* Accordion */
.accordionHeader
{
    border: 1px solid #2F4F4F;
    color: white;
    background-color: #2E4d7B;
	font-family: Arial, Sans-Serif;
	font-size: 12px;
	font-weight: bold;
    padding: 5px;
    margin-top: 5px;
    cursor: pointer;
}

#master_content .accordionHeader a
{
	color: #FFFFFF;
	background: none;
	text-decoration: none;
}

#master_content .accordionHeader a:hover
{
	background: none;
	text-decoration: underline;
}

.accordionHeaderSelected
{
    border: 1px solid #2F4F4F;
    color: white;
    background-color: #5078B3;
	font-family: Arial, Sans-Serif;
	font-size: 12px;
	font-weight: bold;
    padding: 5px;
    margin-top: 5px;
    cursor: pointer;
}

#master_content .accordionHeaderSelected a
{
	color: #FFFFFF;
	background: none;
	text-decoration: none;
}

#master_content .accordionHeaderSelected a:hover
{
	background: none;
	text-decoration: underline;
}

.accordionContent
{
    background-color: #D3DEEF;
    border: 1px dashed #2F4F4F;
    border-top: none;
    padding: 5px;
    padding-top: 10px;
}
/*end accordion*/
  
  #footer {
	background:#373737;
	clear:both;
	bottom: 0;
	position: relative;
	margin-top: -50px; /* negative value of footer height */
	height: 20px;
	clear:both;
	}
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 
	 FOOTER
	 	 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

.nav-footer
{
	position: relative;
	text-align: center;
	clear: both;
}

.nav-footer ul{
	padding:0;
	margin:0;
	text-align:center;
	}

.nav-footer li{
	background:none;
	display:inline;
	border-right:1px dotted #686868;
	padding:0 10px;
	}

.nav-footer li.first {
	border-left:1px dotted #686868;
	}
	
.copyright {
	color:#999;
	font-size:.8em;
	clear:both;
	}

.nav-footer a:link,
.nav-footer a:visited {
	color:#CCC;
	}

@media (max-width: 30em){
    table.responsive-table{
      box-shadow: none;  
    }
    table.responsive-table thead{
      display: none; 
    }
  table.display th,
  table.display td{
    padding: .5em;
  }
    
  table.responsive-table td:nth-child(1):before{
    content: 'Number';
  }
  table.responsive-table td:nth-child(2):before{
    content: 'Name';
  }
  table.responsive-table td:nth-child(1),
  table.responsive-table td:nth-child(2){
    padding-left: 25%;
  }
  table.responsive-table td:nth-child(1):before,
  table.responsive-table td:nth-child(2):before{
    position: absolute;
    left: .5em;
    font-weight: bold;
  }
  
    table.responsive-table tr,
    table.responsive-table td{
        display: block;
    }
    table.responsive-table tr{
        position: relative;
        margin-bottom: 1em;
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
    }
    table.responsive-table td{
        border-top: none;
    }
    table.responsive-table td.organisationnumber{
        background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    table.responsive-table td.actions{
        position: absolute;
        top: 0;
        right: 0;
        border: none;
        background: none;
    }
    table.responsive-table td.top{
    	background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
}

</style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="page">
    <div id="div2" style="float:right;">
    <label><a href="ppa_entry.aspx">Home</a></label>
    
    <label><a href="ppa_logout.aspx">LogOut</a></label>
    
    </div>
    <img src="../images/logo.png" />
       
       
       <br /> <br /> 
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="801px" 
            Width="100%" Font-Names="Verdana" Font-Size="8pt">
            <LocalReport ReportPath="PPA\report\cetak_ppa.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="dbppa_DataTable1" />
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource2" Name="dbppa_DataTable2" />
                </DataSources>
            </LocalReport>
        
        </rsweb:ReportViewer>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:localdev %>" 
            SelectCommand="SELECT A.nobuk, A.nik, (SELECT NIKSITE FROM JRN_TEST.dbo.H_A101 WHERE (Nik = A.nik)) AS niksite, (SELECT NmJabat FROM JRN_TEST.dbo.H_A150 WHERE (KdJabat = A.kdjabatan)) AS nmjabat, (SELECT NmDepar FROM JRN_TEST.dbo.H_A130 WHERE (KdDepar = A.kddept)) AS nmdepar, A.nama, A.kdjabatan, A.kddept, A.kdsite, A.periode, B.genid, B.t_title, B.t_task, B.t_target, B.t_actual, B.t_score FROM tr_ppa_h AS A INNER JOIN tr_ppa_task AS B ON A.nobuk = B.nobuk WHERE (B.nobuk = @nobuk) AND (A.nik = @nik) ORDER BY B.genid">
            <SelectParameters>
                <asp:SessionParameter Name="nobuk" SessionField="nobuk" />
                <asp:SessionParameter Name="nik" SessionField="niksite" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:localdev %>" 
            SelectCommand="SELECT TOP (1) A.nobuk, A.nik, (SELECT NIKSITE FROM JRN_TEST.dbo.H_A101 WHERE (Nik = A.nik)) AS niksite, (SELECT NmJabat FROM JRN_TEST.dbo.H_A150 WHERE (KdJabat = A.kdjabatan)) AS nmjabat, (SELECT NmDepar FROM JRN_TEST.dbo.H_A130 WHERE (KdDepar = A.kddept)) AS nmdepar, A.nama, A.kdjabatan, A.kddept, A.kdsite, A.periode, A.stedit, A.status, (SELECT score_isi FROM tr_ppa_d WHERE (nobuk = B.nobuk) AND (ms_id = 1)) AS question_1, (SELECT score_isi FROM tr_ppa_d AS tr_ppa_d_8 WHERE (nobuk = A.nobuk) AND (ms_id = 2)) AS question_2, (SELECT score_isi FROM tr_ppa_d AS tr_ppa_d_7 WHERE (nobuk = A.nobuk) AND (ms_id = 3)) AS question_3, (SELECT score_isi FROM tr_ppa_d AS tr_ppa_d_6 WHERE (nobuk = A.nobuk) AND (ms_id = 4)) AS question_4, (SELECT score_isi FROM tr_ppa_d AS tr_ppa_d_5 WHERE (nobuk = A.nobuk) AND (ms_id = 5)) AS question_5, (SELECT score_isi FROM tr_ppa_d AS tr_ppa_d_4 WHERE (nobuk = A.nobuk) AND (ms_id = 6)) AS question_6, (SELECT score_isi FROM tr_ppa_d AS tr_ppa_d_3 WHERE (nobuk = A.nobuk) AND (ms_id = 7)) AS question_7, (SELECT score_isi FROM tr_ppa_d AS tr_ppa_d_2 WHERE (nobuk = A.nobuk) AND (ms_id = 8)) AS question_8, (SELECT score_isi FROM tr_ppa_d AS tr_ppa_d_1 WHERE (nobuk = A.nobuk) AND (ms_id = 9)) AS question_9 FROM tr_ppa_h AS A INNER JOIN tr_ppa_d AS B ON A.nobuk = B.nobuk WHERE (B.nobuk = @nobuk) AND (A.nik = @nik)">
            <SelectParameters>
                <asp:SessionParameter Name="nobuk" SessionField="nobuk" />
                <asp:SessionParameter Name="nik" SessionField="niksite" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            TypeName="EXCELLENT.dbppaTableAdapters.DataTable2TableAdapter">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="201311/PPA/000005" Name="nobuk" 
                    SessionField="nobuk" Type="String" />
                <asp:SessionParameter DefaultValue="0001098" Name="nik" SessionField="niksite" 
                    Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            TypeName="EXCELLENT.dbppaTableAdapters.DataTable1TableAdapter">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="201311/PPA/000005" Name="nobuk" 
                    SessionField="nobuk" Type="String" />
                <asp:SessionParameter DefaultValue="0001098" Name="nik" SessionField="niksite" 
                    Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
    </form>
</body>
</html>
