﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports System.Xml.Serialization
Imports System.Diagnostics
Imports EXCELLENT.Excellent
Imports System.Data.SqlClient

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class MyList
    Inherits System.Web.Services.WebService

    ' squerymax argumen utk maxrecord
    Dim squerymax As String = ""
    ' Samakan nilainya dengan di JavaScript
    Private nbatas As Int16 = 10
    Private cekstn As String = " 00:00:00.000"
    Dim pgawa As Integer
    Dim pgakh As Integer

    <WebMethod()> _
    <ScriptMethod()> _
    Public Function GetMasterList(ByVal _page As Int16, ByVal _iduser As String) As MyIssueWrap
        Dim _myIssueWrap As New MyIssueWrap
        pgawa = ((_page - 1) * nbatas) + 1
        pgakh = (_page) * nbatas

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        conn.Open()

        Dim skon As String = ""
        Dim sgroup As String = GroupUser(_iduser)

        Dim s As String = ""
        Dim upgrup As String = GetUpGroup(_iduser)
        For i As Integer = 1 To upgrup.Length
            If i = Len(upgrup) Then
                If Mid(upgrup, i, 1) = 1 Then
                    s = s + "'" + i.ToString + "'"
                End If
            Else
                If Mid(upgrup, i, 1) = 1 Then
                    s = s + "'" + i.ToString + "',"
                End If
            End If
        Next

        If Right(s, 1) = "," Then
            s = s.Substring(0, s.Length - 1)
        End If

        If s = "" Then
            s = "'0'"
        End If

        skon = " Where jenisRap in (" + s + ")"

        If sgroup = "S" Or sgroup = "1" Or sgroup = "3" Then
            skon = skon
        Else
            ' Jika GM cuma bisa liat divisinya
            If sgroup = "4" Then
                skon = skon

                If skon.IndexOf("'3',") > -1 Then
                    skon = skon.Replace("'3',", "")
                    skon = skon + " OR (JenisRap = '3' AND KdDepar = '" + GetFunction(GetCodeFunction(_iduser)) + "')"
                ElseIf skon.IndexOf("and JenisRap = '3'") > -1 Then
                    skon = skon.Replace("and JenisRap = '3'", "AND (JenisRap = '3' and KdDepar = '" + GetFunction(GetCodeFunction(_iduser)) + "')")
                End If
            Else
                ' PM/DPM, SekPro, Sect. Head
                If sgroup = "2" Or sgroup = "6" Or sgroup = "7" Or sgroup = "A" Then
                    skon = skon + " and JenisRap in (" + s + ") and KdCabang = '" + GetJobSite(_iduser) + "'"
                Else
                    ' PIC PDCA bisa lihat meeting cross function
                    skon = skon + " and JenisRap in (" + s + ")"

                    If skon.IndexOf("'3',") > -1 Then
                        skon = skon.Replace("'3',", "")
                        skon = skon + " OR (JenisRap = '3' AND KdDepar = '" + GetFunction(GetCodeFunction(_iduser)) + "')"
                    ElseIf skon.IndexOf("and JenisRap = '3'") > -1 Then
                        skon = skon.Replace("and JenisRap = '3'", "AND (JenisRap = '3' and KdDepar = '" + GetFunction(GetCodeFunction(_iduser)) + "')")
                    End If
                End If
            End If
        End If

        Dim squery As String = ""

        ' RCKY - S
        squery = squery + "select *, ROW_NUMBER() OVER(ORDER BY TglRap DESC, IdRap) AS Indeks from( "
        squery = squery + "select b.IdRap,a.IdIssue,a.NmIssue,a.NoAction,b.kdcabang,b.kddepar,b.jenisrap,b.tglrap, "
        squery = squery + "(SELECT MAX(DueDate) FROM DRAPAT b WHERE b.IdRap+b.IdIssue = a.IdRap+a.IdIssue) as DueDate,isnull(d.stissue,'0') as stissue, "
        ' Tambahan kolom TglClosed sebagai pembanding
        squery = squery + "ISNULL((SELECT MAX(ISNULL(TglClosed,'12/31/3000')) FROM TRISSUE b WHERE b.IdRap+b.IdIssue = a.IdRap+a.IdIssue),'12/31/3000') as TglClosed, "
        squery = squery + "Flag = CASE WHEN (DueDate > GetDate() + 3) Then 'B' WHEN DueDate < GetDate() THEN 'R' ELSE 'Y' END "
        squery = squery + "from hrapat b left join drapat a on b.idrap=a.idrap "
        squery = squery + "left join ( "
        squery = squery + "select idrap,idissue,min(stissue) as stissue from (select idrap,idissue,noaction,sum(cast(stissue as integer)) as stissue from TRISSUE "
        squery = squery + "group by idrap,idissue,noaction)g "
        squery = squery + "group by idrap,idissue)d "
        squery = squery + "on a.idrap=d.idrap and a.idissue=d.idissue "
        squery = squery + "where a.nikpic is not null and a.nikpic != '' and b.stedit='0' and a.idissue=a.noaction "
        squery = squery + ")FIN  "
        If skon.Length > 0 Then
            squery = squery + skon
        End If
        squerymax = squery

        squery = "select * from (" + squery + ")a where indeks BETWEEN " + pgawa.ToString + " AND " + pgakh.ToString

        ' RCKY - E

        Dim scom2 As SqlCommand = New SqlCommand(squery, conn)
        Dim sdr As SqlDataReader = scom2.ExecuteReader()

        Dim njumlah As Int16 = GetMaxRecord(squerymax)

        Dim _lmyTask As New List(Of MyTaskList)
        Dim _myTask As MyTaskList
        Do While sdr.Read()
            _myTask = New MyTaskList
            _myTask.Indeks = sdr("Indeks")
            _myTask.IDMeet = IIf(IsDBNull(sdr("IdRap")), "", sdr("IdRap"))
            _myTask.NoIssue = IIf(IsDBNull(sdr("IdIssue")), "", sdr("IdIssue"))
            _myTask.NmIssue = IIf(IsDBNull(sdr("NmIssue")), "", sdr("NmIssue"))
            _myTask.due = IIf(IsDBNull(sdr("DueDate")), "", Format(sdr("DueDate"), "dd/MM/yyy"))
            _myTask.closed = IIf(IsDBNull(sdr("TglClosed")), "", Format(sdr("TglClosed"), "dd/MM/yyy"))
            _myTask.status = IIf(IsDBNull(sdr("StIssue")), "0", sdr("StIssue"))
            _myTask.Nmpic = GetPIC(_myTask.IDMeet, _myTask.NoIssue)
            _myTask.NmInfo = GetInfoTo(_myTask.IDMeet, _myTask.NoIssue)
            _myTask.MaxRecord = njumlah.ToString

            'Memanfaatkan property Output untuk menampung Last Progress, hal ini dilakukan 
            'daripada membuat property baru serta memanfaatkan property yang nganggur
            _myTask.Output = GetLastProgress(_myTask.IDMeet, _myTask.NoIssue, "")
            _myTask.Flag = sdr("Flag")

            Select Case sdr("JenisRap")
                Case 1
                    _myTask.jenis = "Weekly Review Jobsite"
                Case 2
                    _myTask.jenis = "Monthly Review Jobsite"
                Case 3
                    _myTask.jenis = "Weekly Internal Functional"
                Case 4
                    _myTask.jenis = "Weekly Management Meeting"
                Case 5
                    _myTask.jenis = "Monthly Management Meeting"
                Case 6
                    _myTask.jenis = "Others"
            End Select
            _lmyTask.Add(_myTask)
        Loop
        _myIssueWrap.MyTaskList = _lmyTask
        conn.Close()

        Return _myIssueWrap
    End Function

    Function GenQuery(ByVal q As String) As String
        If q = "" Then
            q = q & " "
            Return q
        Else
            q = q & " AND "
            Return q
        End If
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Public Function GetMasterListOpsi(ByVal _page As Int16, ByVal _opsi As Opsi, ByVal _iduser As String, ByVal _jnstgl As String) As MyIssueWrap
        pgawa = ((_page - 1) * nbatas) + 1
        pgakh = (_page) * nbatas

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        conn.Open()

        Dim sgroup As String = GroupUser(_iduser)
        Dim scabang As String = GetJobSite(_iduser)
        Dim sekuel As String = ""

        ' Tanggal -- COND 1
        If (_opsi.Tgl1 <> "" And _opsi.Tgl2 <> "") And (_opsi.Tgl1 <> "null" Or _opsi.Tgl2 <> "null") Then
            If _jnstgl = "rap" Then
                sekuel = " TglRap BETWEEN '" + DateTime.ParseExact(_opsi.Tgl1, "dd/MM/yyyy", Nothing).ToString("MM/dd/yyyy") + "' AND '" + Format(DateTime.ParseExact(_opsi.Tgl2, "dd/MM/yyyy", Nothing), "MM/dd/yyyy") + "'"
            Else
                sekuel = " DueDate BETWEEN '" + Format(DateTime.ParseExact(_opsi.Tgl1, "dd/MM/yyyy", Nothing), "MM/dd/yyyy") + "' AND '" + Format(DateTime.ParseExact(_opsi.Tgl2, "dd/MM/yyyy", Nothing), "MM/dd/yyyy") + "'"
            End If
        End If

        ' GROUP PERMISSION
        Select Case sgroup
            Case "2" 'SekPro
                sekuel = GenQuery(sekuel) & " KdCabang like '%" + GetJobSite(_iduser) + "%'"
            Case "3" 'BOD
                If _opsi.KdCab <> "" And _opsi.KdCab <> "null" Then
                    sekuel = GenQuery(sekuel) & " KdCabang like '%" + _opsi.KdCab & "%' "
                End If
            Case "4" 'GM
                If _opsi.KdCab <> "" And _opsi.KdCab <> "null" Then
                    sekuel = GenQuery(sekuel) & " KdCabang like '%" + _opsi.KdCab & "%' "
                End If
            Case "6" 'PM/DPM
                sekuel = GenQuery(sekuel) & " KdCabang like '%" + GetJobSite(_iduser) + "%' "
            Case "7" 'Sect Head
                sekuel = GenQuery(sekuel) & " KdCabang like '%" + GetJobSite(_iduser) + "%' "
            Case "A" 'Sect Head Admin
                sekuel = GenQuery(sekuel) & " KdCabang like '%" + GetJobSite(_iduser) + "%' "
            Case "8" 'PIC PDCA
                If _opsi.KdCab <> "" And _opsi.KdCab <> "null" Then
                    sekuel = GenQuery(sekuel) & " KdCabang like '%" + _opsi.KdCab & "%' "
                End If
            Case Else
                If _opsi.KdCab <> "" And _opsi.KdCab <> "null" Then
                    sekuel = GenQuery(sekuel) & " KdCabang like '%" + _opsi.KdCab & "%' "
                End If
        End Select

        Dim s As String = ""
        Dim upgrup As String = GetUpGroup(_iduser)
        For i As Integer = 1 To upgrup.Length
            If i = Len(upgrup) Then
                If Mid(upgrup, i, 1) = 1 Then
                    s = s + "'" + i.ToString + "'"
                End If
            Else
                If Mid(upgrup, i, 1) = 1 Then
                    s = s + "'" + i.ToString + "',"
                End If
            End If
        Next

        If Right(s, 1) = "," Then
            s = s.Substring(0, s.Length - 1)
        End If

        If s = "" Then
            s = "'0'"
        End If

        Dim strToReplace As String
        If sekuel <> "" Then
            sekuel = sekuel + " and (JenisRap in (" + s + ") "
            strToReplace = " and (JenisRap in (" + s + ") "
        Else
            sekuel = sekuel + " (JenisRap in (" + s + ") "
            strToReplace = " (JenisRap in (" + s + ") "
        End If

        'Jika punya akses S,1,3 (Admin,MDV,BOD)
        'Kode Departemen dapat disesuaikan dengan filter
        If (_opsi.Fungsi <> "") And (_opsi.Fungsi <> "null") Then
            If sekuel.IndexOf("'3',") > -1 Then
                If strToReplace <> "" Then
                    sekuel = sekuel.Replace(strToReplace, "")
                    If strToReplace.IndexOf(" AND ") > -1 Then
                        sekuel = sekuel + " AND (JenisRap = '3' AND KdDepar = '" + _opsi.Fungsi + "') "
                    Else
                        sekuel = IIf(sekuel = "", sekuel + " (JenisRap = '3' AND KdDepar = '" + _opsi.Fungsi + "') ", sekuel + " AND (JenisRap = '3' AND KdDepar = '" + _opsi.Fungsi + "') ")
                    End If
                Else
                    sekuel = sekuel.Replace("'3',", "")
                    sekuel = sekuel + " OR (JenisRap = '3' AND KdDepar = '" + _opsi.Fungsi + "')) "
                End If
            Else
                sekuel = sekuel + ") "
            End If
        Else
            Select Case sgroup
                Case "4" 'GM
                    If sekuel.IndexOf("'3',") > -1 Then
                        sekuel = sekuel.Replace("'3',", "")
                        sekuel = sekuel + " OR (JenisRap = '3' AND KdDepar = '" + GetFunction(GetCodeFunction(_iduser)) + "' )) "
                    Else
                        sekuel = sekuel + ") "
                    End If
                Case "5", "7", "8" 'PICPDCA
                    If sekuel.IndexOf("'3',") > -1 Then
                        sekuel = sekuel.Replace("'3',", "")
                        sekuel = sekuel + " OR (JenisRap = '3' and KdDepar = '" & GetFunction(GetCodeFunction(_iduser)) & "')) "
                    Else
                        sekuel = sekuel + ") "
                    End If
                Case Else
                    sekuel = sekuel + ") "
            End Select
        End If

        ' Opsi Pilihan
        ' ------------
        ' Status Issue
        If _opsi.Status <> "" And _opsi.Status <> "null" Then
            If _opsi.Status = "Open" Then
                If sekuel = "" Then
                    sekuel = " StIssue = '0' "
                Else
                    sekuel = sekuel + " AND StIssue = '0' "
                End If
            Else
                If sekuel = "" Then
                    sekuel = " StIssue = '0' "
                Else
                    sekuel = sekuel + " AND StIssue = '1'"
                End If
            End If
        End If

        'overdue or not
        If _opsi.Over <> "" And _opsi.Over <> "null" Then
            If _opsi.Over = "due" Then
                If sekuel = "" Then
                    sekuel = " Flag='R' "
                Else
                    sekuel = sekuel + " AND Flag='R' "
                End If
            Else
                If sekuel = "" Then
                    sekuel = " Flag='Y' "
                Else
                    sekuel = sekuel + " AND Flag='Y' "
                End If
            End If
        End If

        Dim squery As String = ""

        ' RCKY - S
        squery = squery + "select *, ROW_NUMBER() OVER(ORDER BY TglRap DESC, IdRap) AS Indeks from( "
        squery = squery + "select b.IdRap,a.IdIssue,a.NmIssue,a.NoAction,b.kdcabang,b.kddepar,b.jenisrap,b.tglrap, "
        squery = squery + "(SELECT MAX(DueDate) FROM DRAPAT b WHERE b.IdRap+b.IdIssue = a.IdRap+a.IdIssue) as DueDate,isnull(d.stissue,'0') as stissue, "
        ' Tambahan kolom TglClosed sebagai pembanding
        squery = squery + "ISNULL((SELECT MAX(ISNULL(TglClosed,'12/31/3000')) FROM TRISSUE b WHERE b.IdRap+b.IdIssue = a.IdRap+a.IdIssue),'12/31/3000') as TglClosed, "
        squery = squery + "Flag = CASE WHEN (DueDate > GetDate() + 3) Then 'B' WHEN DueDate < GetDate() THEN 'R' ELSE 'Y' END "
        squery = squery + "from hrapat b left join drapat a on b.idrap=a.idrap "
        squery = squery + "left join ( "
        squery = squery + "select idrap,idissue,min(stissue) as stissue from (select idrap,idissue,noaction,sum(cast(stissue as integer)) as stissue from TRISSUE "
        squery = squery + "group by idrap,idissue,noaction)g "
        squery = squery + "group by idrap,idissue)d "
        squery = squery + "on a.idrap=d.idrap and a.idissue=d.idissue "
        squery = squery + "where a.nikpic is not null and a.nikpic != '' and b.stedit='0' and a.idissue=a.noaction "
        squery = squery + ")FIN  "
        If sekuel.Length > 0 Then
            squery = squery + " where " + sekuel
        End If
        squerymax = squery

        'jenis meeting
        If _opsi.Jenis <> "0" Then
            If sekuel = "" Then
                squery = squery
            Else
                squery = "select * from (select Idrap,IdIssue,NmIssue,NoAction,kdcabang,kddepar,jenisrap,tglrap,DueDate,stissue,Flag,TglClosed,ROW_NUMBER() OVER(ORDER BY TglRap DESC, IdRap) AS Indeks from (" + squery + ")b where jenisrap = '" + _opsi.Jenis + "')c "
            End If
            squerymax = squery
        End If

        squery = "select * from (" + squery + ")a where indeks BETWEEN " + pgawa.ToString + " AND " + pgakh.ToString

        ' RCKY - E

        Dim scom2 As SqlCommand = New SqlCommand(squery, conn)
        Dim sdr As SqlDataReader = scom2.ExecuteReader()
        Dim njumlah As Int16 = GetMaxRecord(squerymax)

        Dim _lmyTask As New List(Of MyTaskList)
        Dim _myTask As MyTaskList
        Do While sdr.Read()
            _myTask = New MyTaskList
            _myTask.Indeks = sdr("Indeks")
            _myTask.IDMeet = IIf(IsDBNull(sdr("IdRap")), "", sdr("IdRap"))
            _myTask.NoIssue = IIf(IsDBNull(sdr("IdIssue")), "", sdr("IdIssue"))
            _myTask.NmIssue = IIf(IsDBNull(sdr("NmIssue")), "", sdr("NmIssue"))
            _myTask.due = Format(IIf(IsDBNull(sdr("DueDate")), "", sdr("DueDate")), "dd/MM/yyyy").ToString
            _myTask.closed = Format(IIf(IsDBNull(sdr("TglClosed")), "", sdr("TglClosed")), "dd/MM/yyyy").ToString
            _myTask.status = IIf(IsDBNull(sdr("StIssue")), "0", sdr("StIssue"))
            _myTask.Nmpic = GetPIC(_myTask.IDMeet, _myTask.NoIssue)
            _myTask.NmInfo = GetInfoTo(_myTask.IDMeet, _myTask.NoIssue)
            _myTask.MaxRecord = njumlah.ToString

            'Memanfaatkan property Output untuk menampung Last Progress, hal ini dilakukan 
            'daripada membuat property baru serta memanfaatkan property yang nganggur
            _myTask.Output = GetLastProgress(_myTask.IDMeet, _myTask.NoIssue, _myTask.NoRef)
            _myTask.Flag = sdr("Flag")

            Select Case sdr("JenisRap")
                Case 1
                    _myTask.jenis = "Weekly Review Jobsite"
                Case 2
                    _myTask.jenis = "Monthly Review Jobsite"
                Case 3
                    _myTask.jenis = "Weekly Internal Functional"
                Case 4
                    _myTask.jenis = "Weekly Management Meeting"
                Case 5
                    _myTask.jenis = "Monthly Management Meeting"
                Case 6
                    _myTask.jenis = "Others"
            End Select

            _lmyTask.Add(_myTask)
        Loop
        Dim _myIssueWrap As New MyIssueWrap
        _myIssueWrap.MyTaskList = _lmyTask

        conn.Close()

        Return _myIssueWrap
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Public Function GetMasterTindakan(ByVal _IdRap As String, ByVal _IdIssue As String) As MyIssueWrap
        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        conn.Open()

        Dim squery As String = ""
        squery = squery + "SELECT a.IdRap, a.IdIssue, a.NmIssue, a.NoAction, a.Action, a.Deliverable, convert(char(12),a.DueDate,103) as duedate, a.NmPIC, a.NmInfo, "
        squery = squery + "(SELECT b.StIssue FROM TRISSUE b WHERE b.IdRap+b.IdIssue+b.NoAction = a.IdRap+a.IdIssue+a.NoAction) as StIssue, "
        squery = squery + "convert(char(12),(SELECT c.TglClosed FROM TRISSUE c WHERE c.IdRap+c.IdIssue+c.NoAction = a.IdRap+a.IdIssue+a.NoAction),103) as TglClosed "
        squery = squery + "FROM DRAPAT a WHERE a.IdRap = '" + _IdRap + "' AND IdIssue = '" + _IdIssue + "' and a.StEdit = '0'"

        Dim scom2 As SqlCommand = New SqlCommand(squery, conn)
        Dim sdr As SqlDataReader = scom2.ExecuteReader()

        Dim _lmyTask As New List(Of MyTaskList)
        Dim _myTask As MyTaskList
        Do While sdr.Read()
            _myTask = New MyTaskList
            _myTask.IDMeet = sdr("IdRap")
            _myTask.NoIssue = sdr("IdIssue")
            _myTask.NmIssue = sdr("NmIssue")
            _myTask.NoRef = sdr("NoAction")
            _myTask.Action = IIf(IsDBNull(sdr("Action")), "", sdr("Action"))
            _myTask.status = IIf(IsDBNull(sdr("StIssue")), "0", sdr("StIssue"))
            _myTask.closed = IIf(IsDBNull(sdr("TglClosed")), "", sdr("TglClosed"))
            _myTask.due = IIf(IsDBNull(sdr("DueDate")), "", sdr("DueDate"))
            _myTask.Nmpic = IIf(IsDBNull(sdr("NmPIC")), "", sdr("NmPIC"))
            _myTask.NmInfo = IIf(IsDBNull(sdr("NmInfo")), "", sdr("NmInfo"))
            _myTask.Deliverable = IIf(IsDBNull(sdr("Deliverable")), "", sdr("Deliverable"))

            'Memanfaatkan property Output untuk menampung Last Progress,
            'hal ini dilakukan daripada membuat property baru serta memanfaatkan
            'property yang nganggur
            _myTask.Output = GetLastProgress(_myTask.IDMeet, _myTask.NoIssue, _myTask.NoRef)

            _lmyTask.Add(_myTask)
        Loop
        Dim _myIssueWrap As New MyIssueWrap
        _myIssueWrap.MyTaskList = _lmyTask

        conn.Close()

        Return _myIssueWrap
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Public Function GetList(ByVal _id As String, ByVal _page As Int16, ByVal flagshowmytask As Int16) As MyIssueWrap
        pgawa = ((_page - 1) * nbatas) + 1
        pgakh = (_page) * nbatas

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        conn.Open()

        Dim squery As String = ""

        'ADD ON RICKY
        Dim Grup As String = ""
        Dim divisi As String = ""
        Dim temp As String = ""
        squery = "select a.grup+ ';' + isnull(b.divisi,'') from MUSER a left join mappdep b on a.Departemen = b.Kode  where a.NikUser='" & _id & "'"
        Dim cmd As New SqlCommand(squery, conn)
        temp = cmd.ExecuteScalar
        Grup = temp.Split(";")(0)

        'divisi = temp.Split(";")(1)
        divisi = GetFunction(GetCodeFunction(_id))

        If divisi = "MDV" Then
            divisi = "MSD"
        End If
        '---------------------

        squery = ""
        squery = squery + "SELECT *,Flag = CASE WHEN (DueDate > GetDate() + 3) Then 'B' WHEN DueDate < GetDate() THEN 'R' ELSE 'Y' END FROM "
        squery = squery + "(SELECT ROW_NUMBER() OVER(ORDER BY a.DueDate) AS Indeks, a.IdRap, a.IdIssue, a.NmIssue, NoAction, Action , DueDate, Deliverable,a.NikPIC, NmPIC, NmInfo, "
        squery = squery + "(SELECT MAX(CAST(StIssue as INT)) FROM TRISSUE c WHERE c.IdRap+c.IdIssue+c.NoAction = a.IdRap+a.IdIssue+a.NoAction) as StIssue, "
        squery = squery + "(SELECT c.TglClosed FROM TRISSUE c WHERE c.IdRap+c.IdIssue+c.NoAction = a.IdRap+a.IdIssue+a.NoAction) as TglClosed, "
        squery = squery + "(SELECT COUNT(IdRap) FROM TRKOMENTAR d WHERE d.IdRap+d.IdIssue+d.NoAction = a.IdRap+a.IdIssue+a.NoAction) as JCom, "
        squery = squery + "(SELECT MAX(StComen) FROM TRKOMENTAR g WHERE g.IdRap+g.IdIssue+g.NoAction = a.IdRap+a.IdIssue+a.NoAction) as Marked, "
        squery = squery + "(SELECT NoReferensi FROM TRISSUE e WHERE e.IdRap+e.IdIssue+e.NoAction = a.IdRap+a.IdIssue+a.NoAction) as NoRef, "
        squery = squery + "(SELECT Informasi FROM TRISSUE f WHERE f.IdRap+f.IdIssue+f.NoAction = a.IdRap+a.IdIssue+a.NoAction) as Inform "
        If Grup = "1" And flagshowmytask = 0 Then 'MSD Officer HO
            squery = squery + "FROM DRAPAT a INNER JOIN HRAPAT b on a.IdRap = b.IdRap WHERE (b.jenisrap in ('4','5') or a.NikPIC='" + _id + "') and a.StEdit = '0' and b.Mail = '1')TABEL "
        ElseIf Grup = "2" And flagshowmytask = 0 Then 'MSD Officer Site
            squery = squery + "FROM DRAPAT a INNER JOIN HRAPAT b on a.IdRap = b.IdRap WHERE ((b.jenisrap in ('1','2') and b.kdcabang='" & GetJobsite(_id) & "') or a.NikPIC='" + _id + "') and a.StEdit = '0' and b.Mail = '1')TABEL "
        ElseIf Grup = "8" And flagshowmytask = 0 Then 'PADCA
            squery = squery + "FROM DRAPAT a INNER JOIN HRAPAT b on a.IdRap = b.IdRap WHERE ((b.jenisrap in ('3') and b.kddepar='" & divisi & "') or a.NikPIC='" + _id + "') and a.StEdit = '0' and b.Mail = '1')TABEL "
        Else
            squery = squery + "FROM DRAPAT a INNER JOIN HRAPAT b on a.IdRap = b.IdRap WHERE a.NikPIC = '" + _id + "' and a.StEdit = '0' and b.Mail = '1')TABEL "
        End If
        squerymax = squery
        squery = squery + "WHERE indeks BETWEEN " + pgawa.ToString + " AND " + pgakh.ToString

        Dim scom2 As SqlCommand = New SqlCommand(squery, conn)
        Dim sdr As SqlDataReader = scom2.ExecuteReader()

        Dim njumlah As Int16 = GetMaxRecord(squerymax)

        Dim _lmyTask As New List(Of MyTaskList)
        Dim _myTask As MyTaskList
        Do While sdr.Read()
            _myTask = New MyTaskList
            _myTask.Indeks = sdr("Indeks")
            _myTask.IDMeet = sdr("IdRap")
            _myTask.NoIssue = sdr("IdIssue")
            _myTask.NmIssue = sdr("NmIssue")
            _myTask.NoRef = sdr("NoAction")
            _myTask.Action = IIf(IsDBNull(sdr("Action")), "", sdr("Action"))
            _myTask.status = IIf(IsDBNull(sdr("StIssue")), "0", sdr("StIssue"))
            _myTask.Referensi = IIf(IsDBNull(sdr("NoRef")), "", sdr("NoRef"))
            _myTask.Informasi = IIf(IsDBNull(sdr("Inform")), "", sdr("Inform"))
            If IsDBNull(sdr("TglClosed")) Then
                _myTask.closed = ""
            Else
                _myTask.closed = Format(sdr("TglClosed"), "dd/MM/yyyy")
            End If
            _myTask.due = IIf(IsDBNull(sdr("DueDate")), "", Format(sdr("DueDate"), "dd/MM/yyyy"))
            _myTask.Nmpic = IIf(IsDBNull(sdr("NmPIC")), "", sdr("NmPIC"))
            _myTask.NmInfo = IIf(IsDBNull(sdr("NmInfo")), "", sdr("NmInfo"))
            _myTask.JmlCom = sdr("JCom")
            _myTask.Flag = sdr("Flag")
            _myTask.Deliverable = IIf(IsDBNull(sdr("Deliverable")), "", sdr("Deliverable"))
            _myTask.MaxRecord = njumlah.ToString
            _myTask.hasRead = IIf(IsDBNull(sdr("Marked")), "0", sdr("Marked")).ToString.Trim

            'Memanfaatkan property Output untuk menampung Last Progress, hal ini dilakukan 
            'daripada membuat property baru serta memanfaatkan property yang nganggur
            _myTask.Output = GetLastProgress(_myTask.IDMeet, _myTask.NoIssue, _myTask.NoRef)

            'Add on RICKY
            _myTask.PIC = IIf(IsDBNull(sdr("NikPIC")), "", sdr("NikPIC"))
            _lmyTask.Add(_myTask)
        Loop
        Dim _myIssueWrap As New MyIssueWrap
        _myIssueWrap.MyTaskList = _lmyTask

        conn.Close()

        Return _myIssueWrap
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Public Function GetListByDate(ByVal _id As String, ByVal _tgl1 As String, ByVal _tgl2 As String, ByVal _page As Int16, ByVal _jnstgl As String, ByVal flagshowmytask As Int16) As MyIssueWrap
        Dim dtgl1 As String = _tgl1.Substring(3, 2) + "-" + _tgl1.Substring(0, 2) + "-" + _tgl1.Substring(6, 4)
        dtgl1 = dtgl1 + cekstn
        Dim dtgl2 As String = _tgl2.Substring(3, 2) + "-" + _tgl2.Substring(0, 2) + "-" + _tgl2.Substring(6, 4)
        dtgl2 = dtgl2 + cekstn

        pgawa = ((_page - 1) * nbatas) + 1
        pgakh = (_page) * nbatas

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        conn.Open()

        Dim squery As String = ""

        'ADD ON RICKY
        Dim Grup As String = ""
        Dim divisi As String = ""
        Dim temp As String = ""
        squery = "select a.grup+ ';' + isnull(b.divisi,'') from MUSER a left join mappdep b on a.Departemen = b.Kode  where a.NikUser='" & _id & "'"
        Dim cmd As New SqlCommand(squery, conn)
        temp = cmd.ExecuteScalar
        Grup = temp.Split(";")(0)
        'divisi = temp.Split(";")(1)
        divisi = GetFunction(GetCodeFunction(_id))
        If divisi = "MDV" Then
            divisi = "MSD"
        End If
        '---------------------
        squery = ""
        squery = squery + "SELECT *,Flag = CASE WHEN (DueDate > GetDate() + 3) Then 'B' WHEN DueDate < GetDate() THEN 'R' ELSE 'Y' END FROM "
        squery = squery + "(SELECT ROW_NUMBER() OVER(ORDER BY a.DueDate) AS Indeks, a.IdRap, a.IdIssue, a.NmIssue, NoAction, Action, DueDate, Deliverable,a.NikPIC, NmPIC, NmInfo, "
        squery = squery + "(SELECT MAX(CAST(StIssue as INT)) FROM TRISSUE c WHERE c.IdRap+c.IdIssue+c.NoAction = a.IdRap+a.IdIssue+a.NoAction) as StIssue, "
        squery = squery + "(SELECT c.TglClosed FROM TRISSUE c WHERE c.IdRap+c.IdIssue+c.NoAction = a.IdRap+a.IdIssue+a.NoAction) as TglClosed, "
        squery = squery + "(SELECT COUNT(IdRap) FROM TRKOMENTAR d WHERE d.IdRap+d.IdIssue+d.NoAction = a.IdRap+a.IdIssue+a.NoAction) as JCom, "
        squery = squery + "(SELECT MAX(StComen) FROM TRKOMENTAR g WHERE g.IdRap+g.IdIssue+g.NoAction = a.IdRap+a.IdIssue+a.NoAction) as Marked, "
        squery = squery + "(SELECT NoReferensi FROM TRISSUE e WHERE e.IdRap+e.IdIssue+e.NoAction = a.IdRap+a.IdIssue+a.NoAction) as NoRef, "
        squery = squery + "(SELECT Informasi FROM TRISSUE f WHERE f.IdRap+f.IdIssue+f.NoAction = a.IdRap+a.IdIssue+a.NoAction) as Inform "
        If _jnstgl = "rap" Then
            If Grup = "1" And flagshowmytask = 0 Then 'MSD Officer HO
                squery = squery + "FROM DRAPAT a INNER JOIN HRAPAT f on a.IdRap = f.IdRap WHERE (f.jenisrap in ('4','5') or a.NikPIC='" + _id + "') and f.TglRap BETWEEN '" + dtgl1 + "' and '" + dtgl2 + "' and a.StEdit = '0' and f.Mail = '1')TABEL "
            ElseIf Grup = "2" And flagshowmytask = 0 Then 'MSD Officer Site 
                squery = squery + "FROM DRAPAT a INNER JOIN HRAPAT f on a.IdRap = f.IdRap WHERE ((f.jenisrap in ('1','2') and f.kdcabang='" & GetJobsite(_id) & "') or a.NikPIC='" + _id + "') and f.TglRap BETWEEN '" + dtgl1 + "' and '" + dtgl2 + "' and a.StEdit = '0' and f.Mail = '1')TABEL "
            ElseIf Grup = "8" And flagshowmytask = 0 Then 'PADCA
                squery = squery + "FROM DRAPAT a INNER JOIN HRAPAT f on a.IdRap = f.IdRap WHERE ((f.jenisrap in ('3') and f.kddepar='" & divisi & "') or a.NikPIC='" + _id + "')  and f.TglRap BETWEEN '" + dtgl1 + "' and '" + dtgl2 + "' and a.StEdit = '0' and f.Mail = '1')TABEL "
            Else
                squery = squery + "FROM DRAPAT a LEFT JOIN HRAPAT f on a.IdRap = f.IdRap WHERE a.NikPIC = '" + _id + "' and f.TglRap BETWEEN '" + dtgl1 + "' and '" + dtgl2 + "' and a.StEdit = '0' and f.Mail = '1')TABEL "
            End If
        Else
            If Grup = "1" And flagshowmytask = 0 Then 'MSD Officer HO
                squery = squery + "FROM DRAPAT a INNER JOIN HRAPAT f on a.IdRap = f.IdRap WHERE (f.jenisrap in ('4','5') or a.NikPIC='" + _id + "') and a.DueDate BETWEEN '" + dtgl1 + "' and '" + dtgl2 + "' and a.StEdit = '0' and f.Mail = '1')TABEL "
            ElseIf Grup = "2" And flagshowmytask = 0 Then 'MSD Officer Site
                squery = squery + "FROM DRAPAT a INNER JOIN HRAPAT f on a.IdRap = f.IdRap WHERE ((f.jenisrap in ('1','2') and f.kdcabang='" & GetJobsite(_id) & "') or a.NikPIC='" + _id + "') and a.DueDate BETWEEN '" + dtgl1 + "' and '" + dtgl2 + "' and a.StEdit = '0' and f.Mail = '1')TABEL "
            ElseIf Grup = "8" And flagshowmytask = 0 Then 'PADCA
                squery = squery + "FROM DRAPAT a INNER JOIN HRAPAT f on a.IdRap = f.IdRap WHERE ((f.jenisrap in ('3') and f.kddepar='" & divisi & "') or a.NikPIC='" + _id + "') and a.DueDate BETWEEN '" + dtgl1 + "' and '" + dtgl2 + "' and a.StEdit = '0' and f.Mail = '1')TABEL "
            Else
                squery = squery + "FROM DRAPAT a LEFT JOIN HRAPAT f on a.IdRap = f.IdRap WHERE a.NikPIC = '" + _id + "' and a.DueDate BETWEEN '" + dtgl1 + "' and '" + dtgl2 + "' and a.StEdit = '0' and f.Mail = '1')TABEL "
            End If
            'squery = squery + "FROM DRAPAT a LEFT JOIN HRAPAT f on a.IdRap = f.IdRap WHERE a.NikPIC = '" + _id + "' and a.DueDate BETWEEN '" + dtgl1 + "' and '" + dtgl2 + "' and a.StEdit = '0' and f.Mail = '1')TABEL "
        End If
        squerymax = squery
        squery = squery + "WHERE indeks BETWEEN " + pgawa.ToString + " AND " + pgakh.ToString

        Dim scom As SqlCommand = New SqlCommand()
        scom.Connection = conn
        scom.CommandText = squery
        Dim sdr As SqlDataReader = scom.ExecuteReader()

        Dim njumlah As Int16 = GetMaxRecord(squerymax)

        Dim _lmyTask As New List(Of MyTaskList)
        Dim _myTask As MyTaskList
        Do While sdr.Read()
            _myTask = New MyTaskList
            _myTask.Indeks = sdr("Indeks")
            _myTask.IDMeet = sdr("IdRap")
            _myTask.NoIssue = sdr("IdIssue")
            _myTask.NmIssue = sdr("NmIssue")
            _myTask.NoRef = sdr("NoAction")
            _myTask.Action = sdr("Action")
            _myTask.status = IIf(IsDBNull(sdr("StIssue")), "0", sdr("StIssue"))
            _myTask.Referensi = IIf(IsDBNull(sdr("NoRef")), "", sdr("NoRef"))
            _myTask.Informasi = IIf(IsDBNull(sdr("Inform")), "", sdr("Inform"))
            If IsDBNull(sdr("TglClosed")) Then
                _myTask.closed = ""
            Else
                _myTask.closed = Format(IIf(IsDBNull(sdr("TglClosed")), "", sdr("TglClosed")), "dd/MM/yyyy").ToString
            End If
            _myTask.due = Format(IIf(IsDBNull(sdr("DueDate")), "", sdr("DueDate")), "dd/MM/yyyy").ToString
            _myTask.Nmpic = IIf(IsDBNull(sdr("NmPIC")), "", sdr("NmPIC"))
            _myTask.NmInfo = IIf(IsDBNull(sdr("NmInfo")), "", sdr("NmInfo"))
            _myTask.JmlCom = sdr("JCom")
            _myTask.Flag = sdr("Flag")
            _myTask.Deliverable = IIf(IsDBNull(sdr("Deliverable")), "", sdr("Deliverable"))
            _myTask.MaxRecord = njumlah.ToString
            _myTask.hasRead = IIf(IsDBNull(sdr("Marked")), "0", sdr("Marked")).ToString.Trim

            'Memanfaatkan property Output untuk menampung Last Progress, hal ini dilakukan 
            'daripada membuat property baru serta memanfaatkan property yang nganggur
            _myTask.Output = GetLastProgress(_myTask.IDMeet, _myTask.NoIssue, _myTask.NoRef)
            _myTask.PIC = IIf(IsDBNull(sdr("NikPIC")), "", sdr("NikPIC"))
            _lmyTask.Add(_myTask)
        Loop
        Dim _myIssueWrap As New MyIssueWrap
        _myIssueWrap.MyTaskList = _lmyTask

        conn.Close()

        Return _myIssueWrap
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Public Function GetListByStatus(ByVal _id As String, ByVal _status As String, ByVal _page As Int16, ByVal flagshowmytask As Int16) As MyIssueWrap
        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        conn.Open()

        pgawa = ((_page - 1) * nbatas) + 1
        pgakh = (_page) * nbatas

        If _status = "Open" Then
            _status = "0"
        Else
            _status = "1"
        End If


        Dim squery As String = ""

        'ADD ON RICKY
        Dim Grup As String = ""
        Dim divisi As String = ""
        Dim temp As String = ""
        squery = "select a.grup+ ';' + isnull(b.divisi,'') from MUSER a left join mappdep b on a.Departemen = b.Kode  where a.NikUser='" & _id & "'"
        Dim cmd As New SqlCommand(squery, conn)
        temp = cmd.ExecuteScalar
        Grup = temp.Split(";")(0)
        'divisi = temp.Split(";")(1)
        divisi = GetFunction(GetCodeFunction(_id))
        If divisi = "MDV" Then
            divisi = "MSD"
        End If
        '---------------------

        squery = ""
        squery = "SELECT * FROM (SELECT *, ROW_NUMBER() OVER(ORDER BY DueDate) AS Indeks, "
        squery = squery + "Flag = CASE WHEN (DueDate > GetDate() + 3) Then 'B' WHEN DueDate < GetDate() THEN 'R' ELSE 'Y' END FROM "
        squery = squery + "(SELECT a.IdRap, a.IdIssue, a.NmIssue, a.NoAction, "
        squery = squery + "Action, a.DueDate, a.Deliverable,a.NikPIC, a.NmPIC, a.NmInfo, ISNULL(h.StIssue,'0') as StIssue, "
        squery = squery + "(SELECT c.TglClosed FROM TRISSUE c WHERE c.IdRap+c.IdIssue+c.NoAction = a.IdRap+a.IdIssue+a.NoAction) as TglClosed, "
        squery = squery + "(SELECT COUNT(IdRap) FROM TRKOMENTAR d WHERE d.IdRap+d.IdIssue+d.NoAction = a.IdRap+a.IdIssue+a.NoAction) as JCom, "
        squery = squery + "(SELECT MAX(StComen) FROM TRKOMENTAR g WHERE g.IdRap+g.IdIssue+g.NoAction = a.IdRap+a.IdIssue+a.NoAction) as Marked, "
        squery = squery + "(SELECT NoReferensi FROM TRISSUE e WHERE e.IdRap+e.IdIssue+e.NoAction = a.IdRap+a.IdIssue+a.NoAction) as NoRef, "
        squery = squery + "(SELECT Informasi FROM TRISSUE f WHERE f.IdRap+f.IdIssue+f.NoAction = a.IdRap+a.IdIssue+a.NoAction) as Inform "
        squery = squery + "FROM DRAPAT a LEFT JOIN TRISSUE h on a.IdRap+a.IdIssue+a.NoAction = h.IdRap+h.IdIssue+h.NoAction "
        squery = squery + "INNER JOIN HRAPAT zz on zz.IdRap = a.IdRap "
        If Grup = "1" And flagshowmytask = 0 Then 'MSD Officer HO
            'squery = squery + "FROM DRAPAT a INNER JOIN HRAPAT b on a.IdRap = b.IdRap WHERE (b.jenisrap in ('4','5') or a.NikPIC='" + _id + "') and a.StEdit = '0' and b.Mail = '1')TABEL "
            squery = squery + "WHERE (zz.jenisrap in ('4','5') or a.NikPIC='" + _id + "') and a.StEdit = '0' and zz.Mail = '1') TABEL WHERE StIssue = '" + _status + "')TABEL "
        ElseIf Grup = "2" And flagshowmytask = 0 Then 'MSD Officer Site
            'squery = squery + "FROM DRAPAT a INNER JOIN HRAPAT b on a.IdRap = b.IdRap WHERE (b.jenisrap in ('1','2') or a.NikPIC='" + _id + "') and a.StEdit = '0' and b.Mail = '1')TABEL "
            squery = squery + "WHERE ((zz.jenisrap in ('1','2') and zz.kdcabang='" & GetJobsite(_id) & "') or a.NikPIC='" + _id + "') and a.StEdit = '0' and zz.Mail = '1') TABEL WHERE StIssue = '" + _status + "')TABEL "
        ElseIf Grup = "8" And flagshowmytask = 0 Then 'PADCA
            'squery = squery + "FROM DRAPAT a INNER JOIN HRAPAT b on a.IdRap = b.IdRap WHERE (b.jenisrap in ('3') or a.NikPIC='" + _id + "') and a.StEdit = '0' and b.Mail = '1')TABEL "
            squery = squery + "WHERE ((zz.jenisrap in ('3') and zz.kddepar='" & divisi & "') or a.NikPIC='" + _id + "') and a.StEdit = '0' and zz.Mail = '1') TABEL WHERE StIssue = '" + _status + "')TABEL "
        Else
            squery = squery + "WHERE a.NikPIC = '" + _id + "' and a.StEdit = '0' and zz.Mail = '1') TABEL WHERE StIssue = '" + _status + "')TABEL "
        End If
        'squery = squery + "WHERE a.NikPIC = '" + _id + "' and a.StEdit = '0' and zz.Mail = '1') TABEL WHERE StIssue = '" + _status + "')TABEL "
        squerymax = squery
        squery = squery + "WHERE indeks BETWEEN " + pgawa.ToString + " AND " + pgakh.ToString

        Dim scom As SqlCommand = New SqlCommand()
        scom.Connection = conn
        scom.CommandText = squery
        Dim sdr As SqlDataReader = scom.ExecuteReader()

        Dim njumlah As Int16 = GetMaxRecord(squerymax)

        Dim _lmyTask As New List(Of MyTaskList)
        Dim _myTask As MyTaskList
        Do While sdr.Read()
            _myTask = New MyTaskList
            _myTask.Indeks = sdr("Indeks")
            _myTask.IDMeet = sdr("IdRap")
            _myTask.NoIssue = sdr("IdIssue")
            _myTask.NmIssue = sdr("NmIssue")
            _myTask.NoRef = sdr("NoAction")
            _myTask.Action = sdr("Action")
            _myTask.status = IIf(IsDBNull(sdr("StIssue")), "0", sdr("StIssue"))
            _myTask.Referensi = IIf(IsDBNull(sdr("NoRef")), "", sdr("NoRef"))
            _myTask.Informasi = IIf(IsDBNull(sdr("Inform")), "", sdr("Inform"))
            If IsDBNull(sdr("TglClosed")) Then
                _myTask.closed = ""
            Else
                _myTask.closed = Format(IIf(IsDBNull(sdr("TglClosed")), "", sdr("TglClosed")), "dd/MM/yyyy").ToString
            End If
            _myTask.due = Format(IIf(IsDBNull(sdr("DueDate")), "", sdr("DueDate")), "dd/MM/yyyy").ToString
            _myTask.Nmpic = IIf(IsDBNull(sdr("NmPIC")), "", sdr("NmPIC"))
            _myTask.NmInfo = IIf(IsDBNull(sdr("NmInfo")), "", sdr("NmInfo"))
            _myTask.JmlCom = sdr("JCom")
            _myTask.Flag = sdr("Flag")
            _myTask.Deliverable = IIf(IsDBNull(sdr("Deliverable")), "", sdr("Deliverable"))
            _myTask.MaxRecord = njumlah.ToString
            _myTask.hasRead = IIf(IsDBNull(sdr("Marked")), "0", sdr("Marked")).ToString.Trim

            'Memanfaatkan property Output untuk menampung Last Progress, hal ini dilakukan 
            'daripada membuat property baru serta memanfaatkan property yang nganggur
            _myTask.Output = GetLastProgress(_myTask.IDMeet, _myTask.NoIssue, _myTask.NoRef)
            _myTask.PIC = IIf(IsDBNull(sdr("NikPIC")), "", sdr("NikPIC"))
            _lmyTask.Add(_myTask)
        Loop
        Dim _myIssueWrap As New MyIssueWrap
        _myIssueWrap.MyTaskList = _lmyTask

        conn.Close()

        Return _myIssueWrap
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Public Function GetInfo(ByVal _id As String, ByVal _page As Int16) As MyIssueWrap
        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        conn.Open()

        pgawa = ((_page - 1) * nbatas) + 1
        pgakh = (_page) * nbatas

        Dim squery As String = ""
        squery = "SELECT * FROM (SELECT row_number() over(ORDER BY tglrap desc) as Indeks,"
        squery = squery + "a.IdRap,a.IdIssue,a.NoAction,a.DueDate,a.NmIssue,a.Action,a.NmPIC,a.NikInfo,a.NmInfo,"
        squery = squery + "b.StIssue,b.NoReferensi,b.TglClosed,b.Informasi,b.Output,b.Others,"
        squery = squery + "b.ModifiedBy,TglRap FROM DRAPAT a LEFT JOIN TRISSUE b ON "
        squery = squery + "a.IdRap+a.IdIssue+a.NoAction = b.IdRap+b.IdIssue+b.NoAction LEFT JOIN HRAPAT h on h.idrap=a.idrap WHERE a.NikInfo = '" + _id + "' AND a.StEdit = '0' AND h.Mail = '1') TABEL "
        squerymax = squery
        squery = squery + "WHERE indeks BETWEEN " + pgawa.ToString + " AND " + pgakh.ToString + ""

        Dim scom As SqlCommand = New SqlCommand()
        scom.Connection = conn
        scom.CommandText = squery
        Dim sdr As SqlDataReader = scom.ExecuteReader()

        Dim njumlah As Int16 = GetMaxRecord(squerymax)

        Dim _lmyTask As New List(Of MyTaskList)
        Dim _myTask As MyTaskList
        Do While sdr.Read()
            _myTask = New MyTaskList
            _myTask.Indeks = sdr("Indeks")
            _myTask.IDMeet = sdr("IdRap")
            _myTask.NoIssue = sdr("IdIssue")
            _myTask.NmIssue = IIf(IsDBNull(sdr("NmIssue")), "", sdr("NmIssue"))
            _myTask.NoRef = sdr("NoAction")
            _myTask.Nmpic = IIf(IsDBNull(sdr("NmPIC")), "", sdr("NmPIC"))
            _myTask.Action = IIf(IsDBNull(sdr("Action")), "", sdr("Action"))
            _myTask.status = IIf(IsDBNull(sdr("StIssue")), "0", sdr("StIssue"))
            _myTask.Referensi = IIf(IsDBNull(sdr("NoReferensi")), "", sdr("NoReferensi"))
            _myTask.Output = IIf(IsDBNull(sdr("Output")), "", sdr("Output"))
            _myTask.Informasi = IIf(IsDBNull(sdr("Informasi")), "", sdr("Informasi"))
            _myTask.NikInfo = IIf(IsDBNull(sdr("NikInfo")), "", sdr("NikInfo"))
            _myTask.NmInfo = IIf(IsDBNull(sdr("NmInfo")), "", sdr("NmInfo"))
            _myTask.closed = IIf(IsDBNull(sdr("TglClosed")), "", sdr("TglClosed"))
            _myTask.due = Format(IIf(IsDBNull(sdr("DueDate")), "", sdr("DueDate")), "dd/MM/yyyy").ToString
            _myTask.MaxRecord = njumlah.ToString
            _lmyTask.Add(_myTask)
        Loop
        Dim _myIssueWrap As New MyIssueWrap
        _myIssueWrap.MyTaskList = _lmyTask

        conn.Close()

        Return _myIssueWrap
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Public Function UpdateList(ByVal _argumen As MyUpdateList)
        Dim shasil = ""

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()
            Dim squery As String = "UPDATE TRISSUE SET "

            squery = squery + "StIssue = '" + _argumen.Status
            squery = squery + "', Informasi = '" + _argumen.Informasi
            squery = squery + "', Output = '" + _argumen.Output.Replace("'", "`")
            ' Jika Closed, Isi TglClosed
            ' Jika Open, Kosongi Tanggal Closed
            If (_argumen.Status = "0") Then
                squery = squery + "', TglClosed = null"
                squery = squery + ", NoReferensi = '" + (IIf(_argumen.Referensi = "null", "", _argumen.Referensi))
            Else
                squery = squery + "', TglClosed = '" + Format(Date.Today, "MM/dd/yyyy")
                squery = squery + "', NoReferensi = '" + (IIf(_argumen.Referensi = "null", "", _argumen.Referensi))
            End If
            squery = squery + "', ModifiedIn = '" + HttpContext.Current.Request.UserHostName
            squery = squery + "', ModifiedTime = '" + Format(System.DateTime.Now, "MM/dd/yyyy HH:mm:ss")
            squery = squery + "' WHERE "
            squery = squery + "IdRap = '" + _argumen.IdRap + "' AND IdIssue = '" + _argumen.IdIssue
            squery = squery + "' AND NoAction = '" + _argumen.NoAction + "'"

            Dim scom As SqlCommand = New SqlCommand()
            scom.Connection = conn
            scom.CommandText = squery
            scom.ExecuteScalar()

            conn.Close()
            shasil = "success"

        Catch ex As SqlClient.SqlException
            conn.Close()
            shasil = "failed;" + ex.Message
        Catch ex As Exception
            conn.Close()
            shasil = "failed;" + ex.Message
            Throw
        Finally
            conn.Close()
        End Try

        Return shasil
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Public Function GetKomentar(ByVal _idRap As String, ByVal _idIssue As String, ByVal _noAction As String) As List(Of Komentar)
        Dim alis As New List(Of Komentar)
        Dim Komen As New Komentar()

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            ' Komen Issue
            If _noAction = "" Then
                squery = "SELECT a.idComen,a.idIssue,a.noAction,a.nikFeeder,a.komentar,a.others,a.CreatedTime, "
                squery = squery + "(SELECT TOP 1 b.NmInfo FROM DRAPAT b WHERE a.nikFeeder = b.NikInfo) as NmInfo FROM TRKOMENTAR a "
                squery = squery + "WHERE a.IdRap = '" + _idRap + "' AND a.idIssue = '" + _idIssue + "' AND a.NoAction = '' ORDER BY a.idIssue,a.noAction,a.CreatedTime"
            Else
                ' Komen Action
                squery = "SELECT a.idComen,a.idIssue,a.noAction,a.nikFeeder,a.komentar,a.others,a.CreatedTime, "
                squery = squery + "(SELECT TOP 1 b.NmInfo FROM DRAPAT b WHERE a.nikFeeder = b.NikInfo) as NmInfo FROM TRKOMENTAR a "
                squery = squery + "WHERE a.IdRap = '" + _idRap + "' AND a.idIssue = '" + _idIssue + "' AND a.NoAction = '" + _noAction + "' ORDER BY a.idIssue,a.noAction,a.CreatedTime"
            End If

            Dim scom As SqlCommand = New SqlCommand()
            scom.Connection = conn
            scom.CommandText = squery
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            Dim objek As Komentar
            Do While sdr.Read
                objek = New Komentar()
                objek.IdIssue = sdr("idIssue")
                objek.NoAction = sdr("noAction")
                objek.IdComen = sdr("idComen")
                objek.NikFeeder = IIf(IsDBNull(sdr("nikFeeder")) = True, "", sdr("nikFeeder"))
                objek.NmInfo = IIf(IsDBNull(sdr("NmInfo")) = True, "", sdr("NmInfo"))
                objek.TglKomentar = IIf(IsDBNull(sdr("CreatedTime")) = True, "", Format(sdr("CreatedTime"), "dd/MM/yyyy"))
                objek.Komentar = IIf(IsDBNull(sdr("Komentar")), "", sdr("Komentar"))
                objek.Other = IIf(IsDBNull(sdr("Others")) = True, "", sdr("Others"))
                alis.Add(objek)
            Loop

            ' Tandai yang sudah dibaca
            If sdr.HasRows Then
                MarkKomentar(_idRap, _idIssue, _noAction)
            End If

            conn.Close()
        Catch ex As Exception
            conn.Close()
        End Try

        Return alis
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Public Function InsertKomentar(ByVal _komen As Komentar, ByVal _idkom As String) As String
        Dim shasil = ""

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            ' Buat No, pake CAST coz idcomen adalah varchar
            ' ---------------------------------------------
            'Dim smax As String = "SELECT ISNULL(MAX(CAST(idcomen AS int)),0) AS nomor FROM TRKOMENTAR WHERE "
            'smax = smax + "IdRap = '" + _komen.IdRap + "' AND IdIssue = '" + _komen.IdIssue + "' "
            'smax = smax + "AND NikFeeder = '" + _komen.NikFeeder + "'"

            Dim smax As String = "SELECT ISNULL(MAX(CAST(idcomen AS int)),0) AS nomor FROM TRKOMENTAR WHERE "
            smax = smax + "IdRap = '" + _komen.IdRap + "' AND IdIssue = '" + _komen.IdIssue + "' AND NoAction = '" + _komen.NoAction + "'"

            Dim scmax As SqlCommand = New SqlCommand()
            scmax.Connection = conn
            scmax.CommandText = smax
            Dim noCom As Object = scmax.ExecuteScalar()

            Dim squery As String = ""

            If _idkom = "1" Then
                ' Proses Input Komentar Issue Maka kolom action dikosongi !!
                ' NOT USED ______ !!!
                squery = "INSERT INTO TRKOMENTAR (idcomen,idrap,idissue,noaction,nikfeeder,komentar,CreatedTime,CreatedIn) "
                squery = squery + "values('" + (noCom + 1).ToString + "','" + _komen.IdRap + "','" + _komen.IdIssue + "','','"
                squery = squery + _komen.NikFeeder + "','" + _komen.Komentar.Replace("'", "`") + "','" + Format(Date.Today, "MM/dd/yyyy") + "','" + HttpContext.Current.Request.UserHostName + "')"
            Else
                ' Proses Input Komentar Action Maka kolom action diisi !!
                squery = "INSERT INTO TRKOMENTAR (idcomen,idrap,idissue,noaction,nikfeeder,komentar,CreatedTime,CreatedIn) "
                squery = squery + "values('" + (noCom + 1).ToString + "','" + _komen.IdRap + "','" + _komen.IdIssue + "','" + _komen.NoAction + "','"
                squery = squery + _komen.NikFeeder + "','" + _komen.Komentar.Replace("'", "`") + "','" + Format(Date.Today, "MM/dd/yyyy") + "','" + HttpContext.Current.Request.UserHostName + "')"
            End If
            Dim scom As SqlCommand = New SqlCommand()
            scom.Connection = conn
            scom.CommandText = squery
            scom.ExecuteNonQuery()

            conn.Close()
            shasil = "success"
        Catch ex As Exception
            shasil = "failed"
            conn.Close()
        End Try

        Return shasil
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Public Function InsertProgress(ByVal _progress As Progress, ByVal _nikpic As String) As String
        Dim shasil = ""

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            ' Proses Input Progress Tindakan
            Dim squery As String = ""
            squery = "INSERT INTO TRPROGRESS(NoProgres,IdRap,IdIssue,NoAction,NmProgres,NikPIC,TglProgres,CreatedBy,CreatedTime,CreatedIn) "
            squery = squery + "values(" + GetNoProgress().ToString + ",'" + _progress.IdRap + "','" + _progress.IdIssue + "','" + _progress.NoAction + "','" + _progress.NmProgress.Replace("'", "`") + "','" + _progress.NikPIC + "','"
            squery = squery + Format(System.DateTime.Now, "MM/dd/yyyy HH:mm:ss") + "','" + _nikpic + "','" + Format(System.DateTime.Now, "MM/dd/yyyy HH:mm:ss") + "','" + HttpContext.Current.Request.UserHostName + "')"

            Dim scom As SqlCommand = New SqlCommand()
            scom.Connection = conn
            scom.CommandText = squery
            scom.ExecuteScalar()
            conn.Close()

            shasil = "success"
        Catch ex As Exception
            shasil = "failed"
            conn.Close()
        End Try

        Return shasil
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Public Sub DeleteProgress(ByVal _noProgres As String)
        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            ' Proses Input Progress Tindakan
            Dim squery As String = ""
            squery = "DELETE FROM TRPROGRESS WHERE NoProgres = " + _noProgres

            Dim scom As SqlCommand = New SqlCommand()
            scom.Connection = conn
            scom.CommandText = squery
            scom.ExecuteScalar()
            conn.Close()

            ' Setelah menghapus progress, pastikan bahwa 
            ' lampiran utk progress tersebut juga dihapus
            DeleteLampiran(_noProgres)
        Catch ex As Exception
            conn.Close()
        End Try
    End Sub

    <WebMethod()> _
    <ScriptMethod()> _
    Public Function ShowProgress(ByVal _progress As Progress, ByVal _nikpic As String) As List(Of Progress)
        Dim alis As New List(Of Progress)
        Dim ohasil As Progress

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            ' Proses Input Progress Tindakan
            Dim squery As String = ""
            squery = "SELECT a.NoProgres,a.TglProgres,a.NmProgres FROM TRPROGRESS a WHERE a.IdRap = '" + _progress.IdRap + "' AND a.IdIssue = '" + _progress.IdIssue + "' AND "
            'squery = squery + "a.NoAction = '" + _progress.NoAction + "' AND a.NikPIC = '" + _nikpic + "' ORDER BY CreatedTime desc"
            'Add ON Ricky
            squery = squery + "a.NoAction = '" + _progress.NoAction + "' ORDER BY CreatedTime desc"

            Dim scom As SqlCommand = New SqlCommand()
            scom.Connection = conn
            scom.CommandText = squery
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            Do While sdr.Read()
                ohasil = New Progress()
                ohasil.TglProgress = Format(sdr("TglProgres"), "dd/MM/yyyy hh:mm")
                ohasil.NmProgress = sdr("NmProgres")
                ohasil.NoProgres = sdr("NoProgres")
                alis.Add(ohasil)
            Loop
            conn.Close()
        Catch ex As Exception
            conn.Close()
        End Try

        Return alis
    End Function

    ' Melakukan Transfer DRAPAT yang belum
    ' terdata ke Tabel TRISSUE, dipanggil saat di JS akan melakukan EditAction
    <WebMethod()> _
    <ScriptMethod()> _
     Sub TransferIssue(ByVal _nikPic As String)
        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()
            'Dim squery As String = "INSERT INTO TRISSUE(IdRap,IdIssue,NoAction,ModifiedBy,StIssue,DueDate,CreatedIn,CreatedTime) "
            'squery = squery + "(SELECT IdRap,IdIssue,NoAction,NikPIC,StIssue,DueDate,CreatedIn,CreatedTime FROM DRAPAT WHERE "
            'squery = squery + "(IdIssue+IdRap+NoAction) NOT IN (SELECT (IdIssue+IdRap+NoAction) FROM TRISSUE) AND NikPIC = '" + _nikPic + "')"

            Dim squery As String = "INSERT INTO TRISSUE(IdRap,IdIssue,NoAction,ModifiedBy,StIssue,DueDate,CreatedIn,CreatedTime) "
            squery = squery + "(SELECT IdRap,IdIssue,NoAction,NikPIC,StIssue,DueDate,CreatedIn,CreatedTime FROM DRAPAT WHERE "
            squery = squery + "(IdIssue+IdRap+NoAction) NOT IN (SELECT (IdIssue+IdRap+NoAction) FROM TRISSUE))"

            Dim scom As SqlCommand = New SqlCommand()
            scom.Connection = conn
            scom.CommandText = squery
            scom.ExecuteScalar()
            conn.Close()
        Catch ex As Exception
            conn.Close()
        End Try
    End Sub

    ' Tandai Komentar yang sudah dibaca
    ' digunakan utk merubah warna di user
    Public Sub MarkKomentar(ByVal _idRap As String, ByVal _idIssue As String, ByVal _noAction As String)
        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()
            Dim squery As String = ""

            squery = "UPDATE TRKOMENTAR SET StComen = '1' WHERE "
            squery = squery + "IdRap = '" + _idRap + "' AND idIssue = '" + _idIssue + "' AND NoAction = '" + _noAction + "'"

            Dim scom As SqlCommand = New SqlCommand()
            scom.Connection = conn
            scom.CommandText = squery
            scom.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            conn.Close()
        End Try
    End Sub

    ' Mendapatkan Jumlah Record Maximal
    Public Function GetMaxRecord(ByVal _sekuel As String) As Int16
        Dim nRecord As Int16 = 0
        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        conn.Open()

        _sekuel = "select top 1 indeks from (" + _sekuel + ") b order by indeks desc"

        Dim scom2 As SqlCommand = New SqlCommand(_sekuel, conn)
        nRecord = scom2.ExecuteScalar()

        conn.Close()
        Return nRecord
    End Function

    ' Mendapatkan Nama PIC per Issue
    Function GetPIC(ByVal IdRap As String, ByVal IdIssue As String) As String
        Dim sPIC As String = ""

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            'squery = "SELECT DISTINCT(NmPIC) as NmPIC FROM DRAPAT WHERE IdRap = '" + IdRap + "' AND IdIssue = '" + IdIssue + "'"

            squery = "SELECT DISTINCT(NmPIC + ' ' + ISNULL((SELECT CASE WHEN stIssue=1 THEN '[Close]' ELSE '[Open]' END STAT FROM trissue "
            squery = squery + "WHERE IdRap = DRAPAT.IDRAP AND IdIssue = DRAPAT.IDISSUE AND NOACTION=DRAPAT.NOACTION),'[Open]') ) as NmPIC,IdRap,IdIssue,NoAction "
            squery = squery + "FROM DRAPAT WHERE IdRap = '" + IdRap + "' AND IdIssue = '" + IdIssue + "' ORDER BY IdRap, IdIssue, NoAction "

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()
            Do While sdr.Read()
                sPIC = sPIC + IIf(IsDBNull(sdr("NmPIC")), "", sdr("NmPIC")) + ","
            Loop
            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return sPIC
    End Function

    ' Mendapatkan Nama Info To per Issue
    Function GetInfoTo(ByVal IdRap As String, ByVal IdIssue As String) As String
        Dim sInfo As String = ""

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT DISTINCT(NmInfo) as NmInfo,IdRap,IdIssue,NoAction FROM DRAPAT WHERE IdRap = '" + IdRap + "' AND IdIssue = '" + IdIssue + "' ORDER BY IdRap, IdIssue, NoAction "

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()
            Do While sdr.Read()
                sInfo = sInfo + IIf(IsDBNull(sdr("NmInfo")), "", sdr("NmInfo")) + ","
            Loop
            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return sInfo
    End Function

    ' Mendapatkan Progress Terakhir dari Update Issue
    Function GetLastProgress(ByVal IdRap As String, ByVal IdIssue As String, ByVal IdAction As String) As String
        Dim sProgress As String = ""

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()
            Dim squery As String = ""
            If IdAction = "" Then
                squery = "SELECT TOP 1 nmprogres + (case when CreatedBy <> NikPIC then ' [Updated By ' + (select nmuser from MUSER where NikUser=TRPROGRESS.CreatedBy) + ' ' + cast(TRPROGRESS.CreatedTime as varchar(25)) + ']' else '' end) as nmprogres FROM TRPROGRESS WHERE tglprogres = "
                squery = squery + "(SELECT MAX(tglprogres) FROM trprogress WHERE IdRap = '" + IdRap + "' "
                squery = squery + "AND IdIssue = '" + IdIssue + "') AND "
                squery = squery + "IdRap = '" + IdRap + "' AND IdIssue = '" + IdIssue + "'"
            Else
                squery = "SELECT TOP 1 nmprogres + (case when CreatedBy <> NikPIC then ' [Updated By ' + (select nmuser from MUSER where NikUser=TRPROGRESS.CreatedBy) + ' ' + cast(TRPROGRESS.CreatedTime as varchar(25)) + ']' else '' end) as nmprogres  FROM TRPROGRESS WHERE tglprogres = "
                squery = squery + "(SELECT MAX(tglprogres) FROM trprogress WHERE IdRap = '" + IdRap + "' "
                squery = squery + "AND IdIssue = '" + IdIssue + "' AND NoAction = '" + IdAction + "') AND "
                squery = squery + "IdRap = '" + IdRap + "' AND IdIssue = '" + IdIssue + "' AND NoAction = '" + IdAction + "'"
            End If

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()
            Do While sdr.Read()
                sProgress = sProgress + IIf(IsDBNull(sdr("nmprogres")), "", sdr("nmprogres"))
            Loop
            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return sProgress
    End Function

    Function GetNoProgress() As Int16
        Dim noProgres As Int16

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT MAX(NOPROGRES) AS Nomor  FROM TRPROGRESS"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()
            Do While sdr.Read()
                noProgres = sdr("Nomor") + 1
            Loop

            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return noProgres
    End Function

    Sub DeleteLampiran(ByVal NoPro As String)
        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "DELETE FROM TRLAMPIRAN WHERE NoProgres = " + NoPro

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            scom.ExecuteScalar()
            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try
    End Sub

    Function GroupUser(ByVal _iduser As String) As String
        Dim sgroup As String = "3"

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT Grup FROM MUSER WHERE IdUser = '" + _iduser + "'"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            While sdr.Read()
                sgroup = IIf(IsDBNull(sdr("Grup")), "", sdr("Grup"))
            End While

            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return sgroup
    End Function

    Function GetJobSite(ByVal _iduser As String) As String
        Dim slokasi As String = ""

        ''KODE JOBSITE TIDAK AMBIL LAGI DARI HRIS KARENA LEVEL SCTN HEAD UP
        ''KODE JOBSITE DIANGGAP HO - MAKA AMBIL DARI TABEL MUSER

        'KODE JOBSITE AMBIL LAGI DARI HRIS - BASED RAPAT DI RUANG 3B TGL 1/6/2010

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            'JIKA LEVEL E-Up MAKA AMBIL KODE LOKASI DARI H_A101 HRIS
            'SEDANGKAN D-Down AMBIL KODE JOBSITE DARI H_A101 HRIS

            Dim squery As String = ""
            'squery = "SELECT Nik, KdLokasi = case KdLevel when 5 Then KdLokasi when 6 Then KdLokasi When 7 Then KdLokasi else KdSite end "
            'squery = squery + "FROM H_A101 WHERE Nik = '" + _iduser + "'"

            'squery = "SELECT SITE as KdLokasi FROM MAPSITESAP a inner join OPENROWSET('SQLNCLI','SERVER=172.16.0.7\hris;UID=IICS;PWD=$IICS-123-ONLINE$', 'SELECT WERKS,BTRTL from BERP_CENTRAL.dbo.H_A101_SAP where pernr=''" & _iduser & "''') x on a.WERKS=x.WERKS and a.BTRTL=x.BTRTL"
            squery = "SELECT SITE as KdLokasi FROM MAPSITESAP a inner join OPENROWSET('SQLNCLI','SERVER=20.110.1.5;UID=dev;PWD=hrishris', 'SELECT kdsite  from JRN_TEST.dbo.H_A101 where nik=''" & _iduser & "''') x on a.SITE=x.KDSITE  "

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            If sdr.HasRows Then
                While sdr.Read()
                    slokasi = IIf(IsDBNull(sdr("KdLokasi")), "NFO", sdr("KdLokasi"))
                End While
            Else
                slokasi = "NFO"
            End If

            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return slokasi
    End Function

    Function GetDepar(ByVal _iduser As String) As String
        Dim slokasi As String = ""

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT Departemen FROM MUSER WHERE IdUser = '" + _iduser + "'"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            While sdr.Read()
                slokasi = IIf(IsDBNull(sdr("Departemen")), "", sdr("Departemen"))
            End While

            conn.Close()
            Return slokasi
        Catch eks As Exception
            conn.Close()
        End Try

        Return slokasi
    End Function

    Function GetUpGroup(ByVal _iduser As String) As String
        Dim slokasi As String = ""

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT UpGrup FROM MUSER WHERE IdUser = '" + _iduser + "'"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            While sdr.Read()
                slokasi = IIf(IsDBNull(sdr("UpGrup")), "", sdr("UpGrup"))
            End While

            conn.Close()
            Return slokasi
        Catch eks As Exception
            conn.Close()
        End Try

        Return slokasi
    End Function

    Function ListDepartemen(ByVal _iduser As String) As String
        Dim conn As String = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim SQLConn As New SqlConnection(conn)
        Dim SQlCmd As SqlCommand = New SqlCommand()
        SQlCmd.CommandText = "SELECT kode FROM mappdep INNER JOIN muser on mappdep.divisi = muser.divisi where muser.nikuser = '" + _iduser + "'"
        SQlCmd.Connection = SQLConn
        SQLConn.Open()

        Dim SQLDR As SqlDataReader = SQlCmd.ExecuteReader()

        Dim slist As String = "("
        While SQLDR.Read
            slist = slist + "'" + SQLDR("Kode").ToString + "',"
        End While
        slist = slist.Substring(0, slist.Length - 1)
        slist = slist + ")"

        SQLDR.Close()
        SQLDR = Nothing

        Return slist
    End Function

    Function GetFunction(ByVal _kode As String) As String
        Dim sfungsi As String = _kode

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString)
        Try
            conn.Open()

            Dim squery As String = ""
            'squery = "SELECT abbr FROM mappdep WHERE kode = '" + _kode + "'"
            squery = "SELECT abbr FROM mappdep WHERE NmDepar = '" + _kode + "'"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            While sdr.Read()
                sfungsi = IIf(IsDBNull(sdr("abbr")), "", sdr("abbr"))
            End While

            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return sfungsi
    End Function

    Function GetCodeFunction(ByVal _iduser As String) As String
        Dim sfungsi As String = ""

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
        Try
            If _iduser.IndexOf("10000306") > -1 Or _iduser.IndexOf("10002040") > -1 Or _iduser.IndexOf("10006574") > -1 Or _iduser.IndexOf("10010546") > -1 Or _iduser.IndexOf("10011006") > -1 Or _iduser.IndexOf("10011038") > -1 Then
                sfungsi = "Human Resource & General Affair"
            ElseIf _iduser.IndexOf("10002373") > -1 Or _iduser.IndexOf("10002609") > -1 Or _iduser.IndexOf("10002779") > -1 Or _iduser.IndexOf("10009167") > -1 Or _iduser.IndexOf("10010394") > -1 Or _iduser.IndexOf("10008053") > -1 or  _iduser.IndexOf("10010983") > -1 or _iduser.IndexOf("10000973") > -1 Then
                sfungsi = "Operation"
            ElseIf _iduser.IndexOf("10007237") > -1 Or _iduser.IndexOf("10011034") > -1 Then
                sfungsi = "Plant"
            ElseIf _iduser.IndexOf("10007792") > -1 Then
                sfungsi = "Finance & Accounting"
            ElseIf _iduser.IndexOf("10010983") > -1 Then
                sfungsi = "None Function"
            ElseIf _iduser.IndexOf("10010982") > -1 Then
                sfungsi = "Engineering"
            Else
                conn.Open()

                Dim squery As String = ""
                squery = "SELECT KdDepar as ZFUN FROM H_A101 WHERE Nik = '" + _iduser + "'"
                'squery = "SELECT ZFUN FROM H_A101_SAP WHERE PERNR = '" + _iduser + "' OR PNALT='" + _iduser + "'"

                Dim scom As SqlCommand = New SqlCommand(squery, conn)
                Dim sdr As SqlDataReader = scom.ExecuteReader()

                While sdr.Read()
                    sfungsi = IIf(IsDBNull(sdr("ZFUN")), "", sdr("ZFUN"))
                End While

                conn.Close()
            End If
        Catch eks As Exception
            conn.Close()
        End Try

        Return sfungsi
    End Function
End Class

Public Class MyTaskList
    Private _Indeks As String
    Private _idMeet As String
    Private _noMeet As String
    Private _noIssue As String
    Private _nmIssue As String
    Private _noRef As String
    Private _action As String
    Private _pic As String
    Private _nmpic As String
    Private _due As String
    Private _closed As String
    Private _jenis As String
    Private _status As String
    Private _Referensi As String
    Private _Output As String
    Private _Informasi As String
    Private _NikInfo As String
    Private _NmInfo As String
    Private _JmlCom As String
    Private _NmProgres As String
    Private _Flag As String
    Private _Deliverable As String
    Private _deleted As String
    Private _MaxRecord As String
    Private _hasRead As String

    Public Property Indeks() As String
        Get
            Return _Indeks
        End Get
        Set(ByVal value As String)
            _Indeks = value
        End Set
    End Property
    Public Property IDMeet() As String
        Get
            Return _idMeet
        End Get
        Set(ByVal value As String)
            _idMeet = value
        End Set
    End Property
    Public Property NoMeet() As String
        Get
            Return _noMeet
        End Get
        Set(ByVal value As String)
            _noMeet = value
        End Set
    End Property
    Public Property NoIssue() As String
        Get
            Return _noIssue
        End Get
        Set(ByVal value As String)
            _noIssue = value
        End Set
    End Property
    Public Property NmIssue() As String
        Get
            Return _nmIssue
        End Get
        Set(ByVal value As String)
            _nmIssue = value
        End Set
    End Property
    Public Property NoRef() As String
        Get
            Return _noRef
        End Get
        Set(ByVal value As String)
            _noRef = value
        End Set
    End Property
    Public Property Action() As String
        Get
            Return _action
        End Get
        Set(ByVal value As String)
            _action = value
        End Set
    End Property
    Public Property PIC() As String
        Get
            Return _pic
        End Get
        Set(ByVal value As String)
            _pic = value
        End Set
    End Property
    Public Property Nmpic() As String
        Get
            Return _nmpic
        End Get
        Set(ByVal value As String)
            _nmpic = value
        End Set
    End Property
    Public Property due() As String
        Get
            Return _due
        End Get
        Set(ByVal value As String)
            _due = value
        End Set
    End Property
    Public Property closed() As String
        Get
            Return _closed
        End Get
        Set(ByVal value As String)
            _closed = value
        End Set
    End Property
    Public Property jenis() As String
        Get
            Return _jenis
        End Get
        Set(ByVal value As String)
            _jenis = value
        End Set
    End Property
    Public Property status() As String
        Get
            Return _status
        End Get
        Set(ByVal value As String)
            _status = value
        End Set
    End Property
    Public Property Referensi() As String
        Get
            Return _Referensi
        End Get
        Set(ByVal value As String)
            _Referensi = value
        End Set
    End Property
    Public Property Output() As String
        Get
            Return _Output
        End Get
        Set(ByVal value As String)
            _Output = value
        End Set
    End Property
    Public Property Informasi() As String
        Get
            Return _Informasi
        End Get
        Set(ByVal value As String)
            _Informasi = value
        End Set
    End Property
    Public Property NikInfo() As String
        Get
            Return _NikInfo
        End Get
        Set(ByVal value As String)
            _NikInfo = value
        End Set
    End Property
    Public Property NmInfo() As String
        Get
            Return _NmInfo
        End Get
        Set(ByVal value As String)
            _NmInfo = value
        End Set
    End Property
    Public Property JmlCom() As String
        Get
            Return _JmlCom
        End Get
        Set(ByVal value As String)
            _JmlCom = value
        End Set
    End Property
    Public Property NmProgres() As String
        Get
            Return _NmProgres
        End Get
        Set(ByVal value As String)
            _NmProgres = value
        End Set
    End Property
    Public Property Flag() As String
        Get
            Return _Flag
        End Get
        Set(ByVal value As String)
            _Flag = value
        End Set
    End Property
    Public Property Deliverable() As String
        Get
            Return _Deliverable
        End Get
        Set(ByVal value As String)
            _Deliverable = value
        End Set
    End Property
    Public Property Deleted() As String
        Get
            Return _deleted
        End Get
        Set(ByVal value As String)
            _deleted = value
        End Set
    End Property
    Public Property MaxRecord() As String
        Get
            Return _MaxRecord
        End Get
        Set(ByVal value As String)
            _MaxRecord = value
        End Set
    End Property
    Public Property hasRead() As String
        Get
            Return _hasRead
        End Get
        Set(ByVal value As String)
            _hasRead = value
        End Set
    End Property
End Class

Public Class MyIssueWrap
    Private _MyTaskList As List(Of MyTaskList)

    Public Property MyTaskList() As List(Of MyTaskList)
        Get
            Return _MyTaskList
        End Get
        Set(ByVal value As List(Of MyTaskList))
            _MyTaskList = value
        End Set
    End Property
End Class

Public Class MyUpdateList
    Private _IdRap As String
    Private _IdIssue As String
    Private _NoAction As String
    Private _Status As String
    Private _Hasil As String
    Private _Informasi As String
    Private _Output As String
    Private _TglClosed As String
    Private _Referensi As String

    Public Property IdRap() As String
        Get
            Return _IdRap
        End Get
        Set(ByVal value As String)
            _IdRap = value
        End Set
    End Property
    Public Property IdIssue() As String
        Get
            Return _IdIssue
        End Get
        Set(ByVal value As String)
            _IdIssue = value
        End Set
    End Property
    Public Property NoAction() As String
        Get
            Return _NoAction
        End Get
        Set(ByVal value As String)
            _NoAction = value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _Status
        End Get
        Set(ByVal value As String)
            _Status = value
        End Set
    End Property
    Public Property Hasil() As String
        Get
            Return _Hasil
        End Get
        Set(ByVal value As String)
            _Hasil = value
        End Set
    End Property
    Public Property Informasi() As String
        Get
            Return _Informasi
        End Get
        Set(ByVal value As String)
            _Informasi = value
        End Set
    End Property
    Public Property Output() As String
        Get
            Return _Output
        End Get
        Set(ByVal value As String)
            _Output = value
        End Set
    End Property
    Public Property TglClosed() As String
        Get
            Return _TglClosed
        End Get
        Set(ByVal value As String)
            _TglClosed = value
        End Set
    End Property
    Public Property Referensi() As String
        Get
            Return _Referensi
        End Get
        Set(ByVal value As String)
            _Referensi = value
        End Set
    End Property
End Class

Public Class Komentar
    Private _IdComen As String
    Private _IdRap As String
    Private _IdIssue As String
    Private _NoAction As String
    Private _NikFeeder As String
    Private _NmInfo As String
    Private _Komentar As String
    Private _TglKomentar As String
    Private _Other As String

    Public Property IdComen() As String
        Get
            Return _IdComen
        End Get
        Set(ByVal value As String)
            _IdComen = value
        End Set
    End Property
    Public Property IdRap() As String
        Get
            Return _IdRap
        End Get
        Set(ByVal value As String)
            _IdRap = value
        End Set
    End Property
    Public Property IdIssue() As String
        Get
            Return _IdIssue
        End Get
        Set(ByVal value As String)
            _IdIssue = value
        End Set
    End Property
    Public Property NoAction() As String
        Get
            Return _NoAction
        End Get
        Set(ByVal value As String)
            _NoAction = value
        End Set
    End Property
    Public Property NikFeeder() As String
        Get
            Return _NikFeeder
        End Get
        Set(ByVal value As String)
            _NikFeeder = value
        End Set
    End Property
    Public Property NmInfo() As String
        Get
            Return _NmInfo
        End Get
        Set(ByVal value As String)
            _NmInfo = value
        End Set
    End Property
    Public Property Komentar() As String
        Get
            Return _Komentar
        End Get
        Set(ByVal value As String)
            _Komentar = value
        End Set
    End Property
    Public Property TglKomentar() As String
        Get
            Return _TglKomentar
        End Get
        Set(ByVal value As String)
            _TglKomentar = value
        End Set
    End Property
    Public Property Other() As String
        Get
            Return _Other
        End Get
        Set(ByVal value As String)
            _Other = value
        End Set
    End Property
End Class

Public Class Progress
    Private _IdRap As String
    Private _IdIssue As String
    Private _NoAction As String
    Private _TglProgress As String
    Private _NmProgress As String
    Private _NikPIC As String
    Private _NoProgres As String

    Public Property IdRap() As String
        Get
            Return _IdRap
        End Get
        Set(ByVal value As String)
            _IdRap = value
        End Set
    End Property
    Public Property IdIssue() As String
        Get
            Return _IdIssue
        End Get
        Set(ByVal value As String)
            _IdIssue = value
        End Set
    End Property
    Public Property NoAction() As String
        Get
            Return _NoAction
        End Get
        Set(ByVal value As String)
            _NoAction = value
        End Set
    End Property
    Public Property TglProgress() As String
        Get
            Return _TglProgress
        End Get
        Set(ByVal value As String)
            _TglProgress = value
        End Set
    End Property
    Public Property NmProgress() As String
        Get
            Return _NmProgress
        End Get
        Set(ByVal value As String)
            _NmProgress = value
        End Set
    End Property
    Public Property NikPIC() As String
        Get
            Return _NikPIC
        End Get
        Set(ByVal value As String)
            _NikPIC = value
        End Set
    End Property
    Public Property NoProgres() As String
        Get
            Return _NoProgres
        End Get
        Set(ByVal value As String)
            _NoProgres = value
        End Set
    End Property
End Class

Public Class Opsi
    Private _Tgl1 As String
    Private _Tgl2 As String
    Private _KdCab As String
    Private _Status As String
    Private _Over As String
    Private _Fungsi As String
    Private _JnsMeeting As String

    Public Property Tgl1() As String
        Get
            Return _Tgl1
        End Get
        Set(ByVal value As String)
            _Tgl1 = value
        End Set
    End Property
    Public Property Tgl2() As String
        Get
            Return _Tgl2
        End Get
        Set(ByVal value As String)
            _Tgl2 = value
        End Set
    End Property
    Public Property KdCab() As String
        Get
            Return _KdCab
        End Get
        Set(ByVal value As String)
            _KdCab = value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return _Status
        End Get
        Set(ByVal value As String)
            _Status = value
        End Set
    End Property
    Public Property Over() As String
        Get
            Return _Over
        End Get
        Set(ByVal value As String)
            _Over = value
        End Set
    End Property
    Public Property Fungsi() As String
        Get
            Return _Fungsi
        End Get
        Set(ByVal value As String)
            _Fungsi = value
        End Set
    End Property
    Public Property Jenis() As String
        Get
            Return _JnsMeeting
        End Get
        Set(ByVal value As String)
            _JnsMeeting = value
        End Set
    End Property
End Class

