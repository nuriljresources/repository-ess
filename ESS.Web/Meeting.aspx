<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="Meeting.aspx.vb" Inherits="EXCELLENT.Meeting"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script src="Scripts/calendar_us.js" type="text/javascript"></script>
<script src="Scripts/jsm01.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/calendar-c.css"/> 
 <style type="text/css">
        ul#navigation li a.m2
        {        	
	        background-color:#00CC00;color: #000000;
        }
     .style1
     {
         text-align: center;
     }
     .style2
     {
         height: 24px;
     }
     .style4
     {
         height: 26px;
     }
     ul.dropdown *.dir3 {
          padding-right: 20px;
          background-image: url("/memo/css/dropdown/themes/default/images/nav-arrow-down.png");
          background-position: 100% 50%;
          background-repeat: no-repeat;
          background-color: #FFA500;
         }
   </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ajaxToolkit:ToolkitScriptManager  ID="ToolkitScriptManager1" runat="server" ScriptMode="Release" CombineScripts="true"/>
<asp:UpdatePanel id="MyUpdatePanel" runat="server">
    <ContentTemplate>
     <div id="divmeeting">
        <input type="hidden" id="ctrlToFind" />
<%--         <% If Session("Permission") Is Nothing Then
               Response.Redirect("login.aspx")
               End If%>--%>
                 
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
         <caption style="text-align:center;font-size:1.5em">MINUTES OF MEETING</caption> 
<%--            <input type="hidden" id="KdDepar" value="<%= Session("departemen") %>" />
            <input type="hidden" id="KdCabang" value="<%= Session("jobsite") %>" />--%>
            
            <tr class="row-alternating">
                <td>No. Meeting</td>
                <td colspan="7"><asp:TextBox ID="txtIdRap" runat="Server" value="NEW" Enabled="False" Size="25"></asp:TextBox></td>
            </tr>
            <tr><td class="required">Title</td>
                <td colspan="7"><asp:TextBox ID="txtNmRap" Width="99%" runat="Server"></asp:TextBox></td></tr>
            <tr >
                <td class="required">Date</td>
                <td><asp:TextBox ID="txtTglRap" runat="Server" width="100px" ReadOnly="true" ></asp:TextBox>
                     <script type="text/javascript">
	                     new tcal ({
		                     // form name
	                     'formname': 'aspnetForm',
		                     // input name
	                     'controlname': 'ctl00$ContentPlaceHolder1$txtTglRap'
	                     });
	                  </script>
                </td>
                <td class="required">Time</td>
                <td>
                    <asp:TextBox ID="txtTime" runat="Server" Width="40px"></asp:TextBox> 
                    <ajaxToolkit:MaskedEditExtender ID="txtTime_MaskedEditExtender" runat="server" 
                       Enabled="True" MaskType="Time" Mask="99:99" OnInvalidCssClass="MaskedEditError"
                       TargetControlID="txtTime">
                    </ajaxToolkit:MaskedEditExtender>
                    -
                    <asp:TextBox ID="txtAkhir" runat="server" Width="40px"></asp:TextBox>
                    <ajaxToolkit:MaskedEditExtender ID="txtAkhir_MaskedEditExtender" runat="server" 
                       Enabled="True" MaskType="Time" Mask="99:99" OnInvalidCssClass="MaskedEditError"
                       TargetControlID="txtAkhir">
                    </ajaxToolkit:MaskedEditExtender>
                </td>
                <td class="required">Room</td>
                <td><asp:TextBox ID="txtTmpRap" runat="Server"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="required">Chairman</td>
                <td><input type="hidden" id="nikPimRap" name="nikPimRap"/>
                    <input type="text" id="namaPimRap" name="namaPimRap" size="30" onclick="OpenPopup('PimRap')" readonly=readonly/>
                    <img alt="add" src="images/Search.gif" align="absmiddle" style="cursor:pointer" onclick="OpenPopup('PimRap')" />
                </td>
                <td class="required">Note Taker</td>
                <td colspan="5">
<%--                    <input type="hidden" id="nikNotRap" name="nikNotRap" value="<%= Session("NikUser") %>"/>
                    <input type="text" id="namaNotRap" name="namaNotRap" size="30"   value="<%= Session("NmUser") %>" disabled="disabled"/>
--%>                    <!--<img alt="add" src="images/Search.gif"  align="absmiddle" style="cursor:pointer" onclick="OpenPopup('NotRap')" />-->
                </td>
            </tr>
<%--            <%  If Session("permission") = "" Or Session("permission") = Nothing Or Session("jnsmeeting") = Nothing Then
                    Response.Redirect("Login.aspx", False)
                End If
            %>--%>
            <tr class="row-alternating">
               <td class="style4">Type Meeting</td>
               <td colspan="6" class="style4"><select name="jnsmeeting" id="jnsmeeting">     
               <option value="0"></option>
<%--               <% Response.Write(IIf(Mid(Session("jnsmeeting"), 1, 1) = "1" And Session("permission") <> "1" And Session("permission") <> "8", "<option value=""1"">Weekly Review Jobsite</option>", ""))%>
               <% Response.Write(IIf(Mid(Session("jnsmeeting"), 2, 1) = "1" And Session("permission") <> "1" And Session("permission") <> "8", "<option value=""2"">Monthly Review Jobsite</option>", ""))%>
               <% Response.Write(IIf(Mid(Session("jnsmeeting"), 3, 1) = "1", "<option value=""3"">Weekly Internal Functional</option>", ""))%>
               <% Response.Write(IIf(Mid(Session("jnsmeeting"), 4, 1) = "1", "<option value=""4"">Weekly Management Meeting</option>", ""))%>
               <% Response.Write(IIf(Mid(Session("jnsmeeting"), 5, 1) = "1", "<option value=""5"">Monthly Management Meeting</option>", ""))%>
               <% Response.Write(IIf(Mid(Session("jnsmeeting"), 6, 1) = "1", "<option value=""6"">Others</option>", ""))%>
               --%></select>
               </td>
            </tr>
        </table>
        <br />                       
                              <%--<table width="100%" class="data-table" cellpadding="0" cellspacing="0" id="tblPeserta">--%>
                              <table width="100%" class="data-table-1" cellpadding="0" cellspacing="0" id="tblPeserta">
                                  <caption>
                                      PARTICIPANT LIST
                                      <%--<span id="dtblPeserta" onclick="displayRow('tblPeserta')" 
                                              style="cursor:pointer">Hide</span>--%>
                                  </caption>                                  
                   
                                  <thead>
                                      <tr>
                                          <th width="2%">
                                              No.</th>
                                          <th width="15%">
                                              NIK</th>
                                          <th width="50%">
                                              Name</th>
                                          <th width="10%">
                                              Attendance</th>
                                          <th width="2%">
                                              Add</th>
                                          <th width="2%">
                                              Delete</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                  </tbody>
                             </table>  
        <br />   
        <b>PARTICIPANT ( NON JRN ) :</b>
        <br />
         <asp:TextBox ID="txtNonBuma" runat="server" Height="30px" Width="100%" 
             TextMode="MultiLine" class="tb10"></asp:TextBox>
        <br />                         
        <br />           

        <b>AGENDA :</b>
        <br />
         <asp:TextBox ID="txtAgenda" runat="server" Height="60px" Width="100%" 
             TextMode="MultiLine" class="tb10"></asp:TextBox>
        <br />                         
        <br />
        <table width="100%" class="data-table-1" cellpadding="0" cellspacing="0" id="tblIssue">      
             <caption>LIST ISSUE</caption>      
         <thead>
         <tr>
            <th width="1%">No.</th>
            <th>Issue</th>
            <th>Action</th>
            <th>Delivarable</th>
            <th>Information 1</th>
            <th>Information 2</th>
            <th width="3%">Delete</th>
         </tr>  
         </thead>
         <tbody></tbody>      
        </table>
        <br />        
    
        <div id="divinfo">
        <b>INFORMATION :</b>
        <asp:TextBox ID="TextInformasi" runat="server" Width="100%" Rows="20"
            TextMode="MultiLine" class="tb10" MaxLength="5000" ></asp:TextBox>
            <span id='lblsisainfo' style="color: #FF3300">(Max 5000 Character)</span>
        </div>
        <br />
        <div id="divkebijakan">
        <b>DECISION :</b>
        
        <asp:TextBox ID="TextKebijakan" runat="server" Height="75px" Width="100%" 
            TextMode="MultiLine" class="tb10" MaxLength="1500" onkeyup="limiter2();" onkeydown="limiter2();"></asp:TextBox>
            <span id='lblsisa' style="color: #FF3300">(Max 1500 Character)</span> 
        </div>
        <br />
        
        <br />            
        <div id="menu" style="text-align:center">
        <img id="saveimg"  alt="Save" src="images/save-icon.png" style="cursor:pointer" onclick="saveheader();" title="Simpan" /> <img id="printimg" alt="Print" src="images/print-icon.png" style="cursor:pointer" onclick="OpenPrint()" title="Cetak" /> <img id="mailimg" alt="Mail" src="images/email-icon.png" style="cursor:pointer" onclick="doMail()" title="Send Email" /> </div><div id="log" style="color:red;font:Trebechuet;"></div>
        <script type="text/javascript">
            textboxstyle();
        </script> </div>
    </ContentTemplate>    
</asp:UpdatePanel>
</asp:Content>
