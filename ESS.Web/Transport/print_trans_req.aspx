﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="print_trans_req.aspx.vb" Inherits="EXCELLENT.print_trans_req" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" Height="483px" Width="1135px">
            <LocalReport ReportPath="Laporan\cetak_trans_req.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                        Name="transport_T_H003" />
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" 
                        Name="transport_T_H00301" />
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource3" 
                        Name="transport_T_H00302" />
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource4" 
                        Name="transport_T_H003021" />
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource5" 
                        Name="transport_T_H003022" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="ObjectDataSource5" runat="server" 
            DeleteMethod="Delete" InsertMethod="Insert" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            TypeName="EXCELLENT.transportTableAdapters.T_H003022TableAdapter" 
            UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_GenID" Type="String" />
                <asp:Parameter Name="Original_TransportNo" Type="String" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="pes_nik" Type="String" />
                <asp:Parameter Name="pes_name" Type="String" />
                <asp:Parameter Name="pes_type" Type="String" />
                <asp:Parameter Name="Original_GenID" Type="String" />
                <asp:Parameter Name="Original_TransportNo" Type="String" />
            </UpdateParameters>
            <SelectParameters>
                <asp:SessionParameter Name="arg" SessionField="transno" Type="String" />
            </SelectParameters>
            <InsertParameters>
                <asp:Parameter Name="GenID" Type="String" />
                <asp:Parameter Name="TransportNo" Type="String" />
                <asp:Parameter Name="pes_nik" Type="String" />
                <asp:Parameter Name="pes_name" Type="String" />
                <asp:Parameter Name="pes_type" Type="String" />
            </InsertParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource4" runat="server" 
            DeleteMethod="Delete" InsertMethod="Insert" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            TypeName="EXCELLENT.transportTableAdapters.T_H003021TableAdapter" 
            UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_GenID" Type="String" />
                <asp:Parameter Name="Original_TransportNo" Type="String" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="pes_nik" Type="String" />
                <asp:Parameter Name="pes_name" Type="String" />
                <asp:Parameter Name="pes_type" Type="String" />
                <asp:Parameter Name="Original_GenID" Type="String" />
                <asp:Parameter Name="Original_TransportNo" Type="String" />
            </UpdateParameters>
            <SelectParameters>
                <asp:SessionParameter Name="arg" SessionField="transno" Type="String" />
            </SelectParameters>
            <InsertParameters>
                <asp:Parameter Name="GenID" Type="String" />
                <asp:Parameter Name="TransportNo" Type="String" />
                <asp:Parameter Name="pes_nik" Type="String" />
                <asp:Parameter Name="pes_name" Type="String" />
                <asp:Parameter Name="pes_type" Type="String" />
            </InsertParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" 
            DeleteMethod="Delete" InsertMethod="Insert" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            TypeName="EXCELLENT.transportTableAdapters.T_H00302TableAdapter" 
            UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_GenID" Type="String" />
                <asp:Parameter Name="Original_TransportNo" Type="String" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="pes_nik" Type="String" />
                <asp:Parameter Name="pes_name" Type="String" />
                <asp:Parameter Name="pes_type" Type="String" />
                <asp:Parameter Name="Original_GenID" Type="String" />
                <asp:Parameter Name="Original_TransportNo" Type="String" />
            </UpdateParameters>
            <SelectParameters>
                <asp:SessionParameter Name="arg" SessionField="transno" Type="String" />
            </SelectParameters>
            <InsertParameters>
                <asp:Parameter Name="GenID" Type="String" />
                <asp:Parameter Name="TransportNo" Type="String" />
                <asp:Parameter Name="pes_nik" Type="String" />
                <asp:Parameter Name="pes_name" Type="String" />
                <asp:Parameter Name="pes_type" Type="String" />
            </InsertParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
            DeleteMethod="Delete" InsertMethod="Insert" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            TypeName="EXCELLENT.transportTableAdapters.T_H00301TableAdapter" 
            UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_GenID" Type="String" />
                <asp:Parameter Name="Original_TransportNo" Type="String" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Timedes" Type="String" />
                <asp:Parameter Name="des_date" Type="DateTime" />
                <asp:Parameter Name="dep_from" Type="String" />
                <asp:Parameter Name="timeto" Type="String" />
                <asp:Parameter Name="to_date" Type="DateTime" />
                <asp:Parameter Name="des_to" Type="String" />
                <asp:Parameter Name="purpose" Type="String" />
                <asp:Parameter Name="Original_GenID" Type="String" />
                <asp:Parameter Name="Original_TransportNo" Type="String" />
            </UpdateParameters>
            <SelectParameters>
                <asp:SessionParameter DefaultValue="transno" Name="arg" SessionField="transno" 
                    Type="String" />
            </SelectParameters>
            <InsertParameters>
                <asp:Parameter Name="GenID" Type="String" />
                <asp:Parameter Name="TransportNo" Type="String" />
                <asp:Parameter Name="Timedes" Type="String" />
                <asp:Parameter Name="des_date" Type="DateTime" />
                <asp:Parameter Name="dep_from" Type="String" />
                <asp:Parameter Name="timeto" Type="String" />
                <asp:Parameter Name="to_date" Type="DateTime" />
                <asp:Parameter Name="des_to" Type="String" />
                <asp:Parameter Name="purpose" Type="String" />
            </InsertParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            DeleteMethod="Delete" InsertMethod="Insert" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            TypeName="EXCELLENT.transportTableAdapters.T_H003TableAdapter" 
            UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_TransportNo" Type="String" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="nik" Type="String" />
                <asp:Parameter Name="kddepar" Type="String" />
                <asp:Parameter Name="kddivisi" Type="String" />
                <asp:Parameter Name="costcode" Type="String" />
                <asp:Parameter Name="sup_nik" Type="String" />
                <asp:Parameter Name="vehicle_type" Type="String" />
                <asp:Parameter Name="reqby" Type="String" />
                <asp:Parameter Name="reqdate" Type="DateTime" />
                <asp:Parameter Name="apprby" Type="String" />
                <asp:Parameter Name="apprdate" Type="DateTime" />
                <asp:Parameter Name="reviewby" Type="String" />
                <asp:Parameter Name="reviewdate" Type="DateTime" />
                <asp:Parameter Name="proceedby" Type="String" />
                <asp:Parameter Name="proceeddate" Type="DateTime" />
                <asp:Parameter Name="CreatedBy" Type="String" />
                <asp:Parameter Name="CreatedIn" Type="String" />
                <asp:Parameter Name="CreatedTime" Type="DateTime" />
                <asp:Parameter Name="ModifiedBy" Type="String" />
                <asp:Parameter Name="ModifiedIn" Type="String" />
                <asp:Parameter Name="ModifiedTime" Type="DateTime" />
                <asp:Parameter Name="StEdit" Type="String" />
                <asp:Parameter Name="DeleteBy" Type="String" />
                <asp:Parameter Name="DeleteTime" Type="DateTime" />
                <asp:Parameter Name="fstatus" Type="Byte" />
                <asp:Parameter Name="Original_TransportNo" Type="String" />
            </UpdateParameters>
            <SelectParameters>
                <asp:SessionParameter Name="arg" SessionField="transno" Type="String" />
            </SelectParameters>
            <InsertParameters>
                <asp:Parameter Name="TransportNo" Type="String" />
                <asp:Parameter Name="nik" Type="String" />
                <asp:Parameter Name="kddepar" Type="String" />
                <asp:Parameter Name="kddivisi" Type="String" />
                <asp:Parameter Name="costcode" Type="String" />
                <asp:Parameter Name="sup_nik" Type="String" />
                <asp:Parameter Name="vehicle_type" Type="String" />
                <asp:Parameter Name="reqby" Type="String" />
                <asp:Parameter Name="reqdate" Type="DateTime" />
                <asp:Parameter Name="apprby" Type="String" />
                <asp:Parameter Name="apprdate" Type="DateTime" />
                <asp:Parameter Name="reviewby" Type="String" />
                <asp:Parameter Name="reviewdate" Type="DateTime" />
                <asp:Parameter Name="proceedby" Type="String" />
                <asp:Parameter Name="proceeddate" Type="DateTime" />
                <asp:Parameter Name="CreatedBy" Type="String" />
                <asp:Parameter Name="CreatedIn" Type="String" />
                <asp:Parameter Name="CreatedTime" Type="DateTime" />
                <asp:Parameter Name="ModifiedBy" Type="String" />
                <asp:Parameter Name="ModifiedIn" Type="String" />
                <asp:Parameter Name="ModifiedTime" Type="DateTime" />
                <asp:Parameter Name="StEdit" Type="String" />
                <asp:Parameter Name="DeleteBy" Type="String" />
                <asp:Parameter Name="DeleteTime" Type="DateTime" />
                <asp:Parameter Name="fstatus" Type="Byte" />
            </InsertParameters>
        </asp:ObjectDataSource>
    </div>
    </form>
</body>
</html>
