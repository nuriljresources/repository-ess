﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Security.Cryptography
Imports System.IO
Imports System.Text
Imports System.Configuration
Imports System.Collections.Generic
Imports System.Linq
Imports System.DirectoryServices  'Note dicek perlu ditick pada add reference
Imports System.Net.Mail
Partial Public Class login_ppa
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dtnow As Date
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("localdev").ConnectionString)
        sqlConn.Open()
        Dim dtb As DataTable = New DataTable()
        Dim strcon As String = "select dtopen, dtclose from ms_system"
        Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
        sda.Fill(dtb)

        dtnow = Today.Date

        If dtb.Rows.Count > 0 Then
            If dtnow < dtb.Rows(0)!dtopen Or dtnow > dtb.Rows(0)!dtclose Then
                Response.Redirect("notactive.aspx")
            End If
        End If

    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLogin.Click
        Dim msg As String
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        If Trim(Me.TextBox2.Text) = "" Then
            Me.lblConfirm.Text = "Password Should Not Empty Please Fill Your Password"
            Exit Sub
        End If

        Dim ls_user, ls_pass, lsErr As String
        Dim lsnik As String
        Dim lbl_login As Boolean = False
        Dim Gsuser As String

        Dim index As Integer = TextBox1.Text.IndexOf("\")
        Dim dm As String
        Dim nm As String
        Dim count As Integer
        If index > 0 Then
            count = TextBox1.Text.Length
            dm = TextBox1.Text.Substring(0, index)
            nm = TextBox1.Text.Substring(index, count - index)
            nm = Replace(nm, "\", "")
        End If

        ls_pass = TextBox2.Text

        Dim domain As String = dm

        lbl_login = ValidateActiveDirectoryLogin(domain, nm, ls_pass, lsnik, lsErr)

        If lbl_login = True Then
            'Check login id.
            If lsnik = "" Then
                'MsgBox("Nik user name : " + ls_user + " not found,Please inform SysAdmin Check user domain setting.", MsgBoxStyle.Exclamation, "Confirmation")
                Me.lblConfirm.Text = "Nik user name : " + ls_user + " not found,Please inform SysAdmin Check user domain setting."
                Return
            Else
                Gsuser = lsnik  ' GsUser yg dipakai untuk tampung NIK user dlm transaksi
                'MsgBox("Sukses login masuk", MsgBoxStyle.Exclamation, Gsuser)
                'Masuk Form Login
                Me.lblConfirm.Text = "Login Success"
            End If
        Else
            'MsgBox(lsErr, MsgBoxStyle.Exclamation, "Confirmation")
            'Me.lblConfirm.Text = lsErr + " " + "user : " + ls_user + " " + "password : " + ls_pass + " " + "nik : " + lsnik + " domain: " + Domain
            Me.lblConfirm.Text = lsErr
            Return
        End If

        'cek nik user didatabase
        Try
            'lsnik = "JRN0319"
            'lsnik = "JRN0167"
            'lsnik = "JRN0150"
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()
            'HashString
            'Dim strcon As String = "select kduser,nmuser,kdsite,kddivisi,kddepar, kdlevel from B_C012 where kduser = '" + Replace(Me.TextBox1.Text, "'", "") + "' and password = '" + SecureIt.Secure.Encrypt(Replace(Me.TextBox2.Text, "'", "")) + "' and stedit in ('0','1')"
            'Dim strcon As String = "select kduser,nmuser,kdsite,kddivisi,kddepar, kdlevel from B_C012 where kduser = '" + Replace(Me.TextBox1.Text, "'", "") + "' and password = '" + HashString(Replace(Me.TextBox2.Text, "'", "")) + "' and stedit in ('0','1')"
            Dim strcon As String = "select kduser,nmuser,kdsite,kddivisi,kddepar, (select KdLevel from H_A101 where niksite = B_C012.kduser) as kdlevel from B_C012 where kduser = '" + Replace(lsnik, "'", "") + "' and stedit in ('0','1')"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            If dtb.Rows.Count = 0 Then
                Me.lblConfirm.Text = "Please Check Again your login ..."
                sqlConn.Close()
                Exit Sub
            End If

            If Session("ppa_otorisasi") = "" Then

                Session("ppa_otorisasi") = dtb.Rows(0)("kduser").ToString
                Session("ppa_permission") = "1;B"
                Session("ppa_NmUser") = dtb.Rows(0)("nmuser").ToString
                Session("ppa_level") = dtb.Rows(0)("kdlevel").ToString
                'Response.Redirect("dw_trip_header.aspx")
            End If

            If Session("ppa_level") = "0L" Then
                Me.lblConfirm.Text = "Currently Not Available"
                Session("ppa_permission") = ""
                Session("ppa_otorisasi") = ""
                Session.Clear()
                Session.Abandon()
                Exit Sub
            End If

        Catch ex As Exception
            Me.lblConfirm.Text = ex.Message
        End Try

        Try

            Dim sqlConn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

            Dim dtb2 As DataTable = New DataTable()
            Dim strcon2 As String = "select nik from H_A101 where niksite='" + Session("ppa_otorisasi") + "'"
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn)
            sda2.Fill(dtb2)

            Session("ppa_niksite") = dtb2.Rows(0)("nik").ToString

            Response.Redirect("ppa_entry.aspx")
        Catch ex As Exception
            Me.lblConfirm.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Session("ppa_otorisasi") = ""
        Session("ppa_departemen") = ""
        Session("ppa_jobsite") = ""
        Session.Abandon()
        Response.Redirect("Logout.aspx")
    End Sub

    Public Function HashString(ByVal Text As String) As String
        Dim hashedPassword As String = ""
        Dim hashProvider As SHA256Managed
        Try
            Dim passwordBytes() As Byte
            'Dim hashBytes() As Byte
            passwordBytes = System.Text.Encoding.Unicode.GetBytes(Text)
            hashProvider = New SHA256Managed()
            passwordBytes = hashProvider.ComputeHash(passwordBytes)
            hashedPassword = Convert.ToBase64String(passwordBytes)
        Catch ex As Exception
            'logger.LogRequest("At HashString: " + ex.Message)
            MsgBox(ex.Message)
        Finally
            If Not hashProvider Is Nothing Then
                hashProvider.Clear()
                hashProvider = Nothing
            End If
        End Try
        Return hashedPassword
    End Function

    Private Function ValidateActiveDirectoryLogin(ByVal Domain As String, ByVal Username As String, ByVal Password As String, ByRef NIK As String, ByRef msgErr As String) As Boolean
        Dim Success As Boolean = False
        Dim Entry As New System.DirectoryServices.DirectoryEntry("LDAP://" & Domain, Username, Password)
        Dim Searcher As New System.DirectoryServices.DirectorySearcher(Entry)
        Dim RetArray As New Hashtable()
        Dim oResult As SearchResult
        Dim oResults As SearchResultCollection
        Dim lsnik As String
        Dim msg As String

        Searcher.PropertiesToLoad.Add("SAMAccountName")
        Searcher.PropertiesToLoad.Add("givenname")
        Searcher.PropertiesToLoad.Add("employeeid")
        Searcher.PropertiesToLoad.Add("mail")

        Searcher.SearchScope = DirectoryServices.SearchScope.Subtree


        Try
            Dim Results As System.DirectoryServices.SearchResult = Searcher.FindOne
            Success = Not (Results Is Nothing)

            If Success = True Then
                Searcher.Filter = "(samAccountName=" & Username & ")"

                Dim Resultsxx As System.DirectoryServices.SearchResultCollection = Searcher.FindAll

                For Each oResult In Resultsxx
                    Dim de As System.DirectoryServices.DirectoryEntry = oResult.GetDirectoryEntry()

                    If de.Properties("GivenName").Value() Is Nothing Then
                    Else
                        lsnik = de.Properties("GivenName").Value().ToString()
                    End If


                    If de.Properties("employeeid").Value() Is Nothing Then
                        lsnik = ""
                    Else
                        lsnik = de.Properties("employeeid").Value().ToString()
                    End If

                Next

                NIK = lsnik

            End If


        Catch ex As Exception
            msgErr = ex.Message
            Success = False

        End Try
        Return Success
    End Function
End Class