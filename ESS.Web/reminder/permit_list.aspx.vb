﻿Imports System.Data
Imports System.Data.SqlClient
Partial Public Class permit_list
    Inherits System.Web.UI.Page
    Public msginfo As String
    Public listex As String = ""
    Public mnentry As String
    Public mnview As String
    Public mnmgmt As String
    Public mncontract As String
    Dim llp As New lp

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim reqstr As String
        Dim i As Integer
        'If Request.QueryString("reqid") <> "" Then

        'End If

        'If Trim(Session("permission_permit")) <> "3" Then
        '    uploaddoc.Enabled = True
        'Else
        '    uploaddoc.Enabled = False
        'End If

        If Session("otorisasi_permit") = Nothing Or Session("otorisasi_permit") = "" Then
            Response.Redirect("signin_permit.aspx")
        End If

        If Trim(Session("permission_permit")) = "1" Or Trim(Session("permission_permit")) = "2" Then
            mnentry = "<div id='mnentry'><img alt='entry document' src='../images/open_doc_small.png' /> <a href='doc_center_entry.aspx'>&nbsp;Set Document</a></div>"
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"

            If Session("dept").ToString().ToLower = "contract" Or Session("dept").ToString().ToLower() = "contract management system" Then
                mncontract = "<div id='mncontract'><img alt='view document' src='../images/open_doc_small.png' /> <a href='supp_permit_lisense_entry.aspx'>&nbsp;Set Permit & License</a></div>"
            End If
        End If

        If Trim(Session("permission_permit")) = "1" Then
            mnmgmt = "<div id='mnmgmt'> <img alt='Management User' src='../images/open_doc_small.png' /> <a href='mgmnt_list.aspx'>&nbsp;Management User</a></div>"
        End If

        If Trim(Session("permission_permit")) = "3" Or Trim(Session("permission_permit")) = "4" Then
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
        End If

        If Not IsPostBack Then


            Dim div As String = Session("divisi")

            Try
                'Dim strcon As String = "select id,substring(f_name,1,80) as f_name,ftype,fsubject,kdcom, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10), createdtime, 101) as createdtime, convert(char(10),cms_start, 101) as cms_start, convert(char(10),cms_end, 101) as cms_end, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic1) as pic1, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic2) as pic2, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic3) as pic3, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic4) as pic4, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic5) as pic5, kddiv from docpermit_trans where stedit <> 2 and modul = '" + Session("modul_permit").ToString + "' and (pic1 = '" + Session("otorisasi_permit") + "' or pic2 = '" + Session("otorisasi_permit") + "' or pic3 = '" + Session("otorisasi_permit") + "' or pic4 = '" + Session("otorisasi_permit") + "' or pic5 = '" + Session("otorisasi_permit") + "')"
                Dim strcon As String = "select id,substring(f_name,1,80) as f_name,ftype,fsubject,kdcom, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10), createdtime, 101) as createdtime, convert(char(10),cms_start, 101) as cms_start, convert(char(10),cms_end, 101) as cms_end, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic1) as pic1, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic2) as pic2, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic3) as pic3, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic4) as pic4, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic5) as pic5, kddiv, vendor from docpermit_trans where stedit <> 2 and modul = '" + Session("modul_permit").ToString + "'"
                'strcon = strcon + " where month(reqdate) = '" + imonth.ToString + "' and YEAR(reqdate) = '" + th + "' and nik = '1231313' and stedit <> 2"
                Dim dtb As DataTable = New DataTable
                Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)

                sda.Fill(dtb)
                GridView1.DataSource = dtb
                GridView1.DataBind()

                Dim strcon2 As String = "select top 10(f_name) from docpermit_trans where (cms_end between GETDATE() and GETDATE() + 30 OR GETDATE() > cms_end) AND StEdit <> '2' and modul = '" + Session("modul_permit").ToString + "'"
                Dim dtb2 As DataTable = New DataTable
                Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn)

                sda2.Fill(dtb2)

                For i = 1 To dtb2.Rows.Count
                    listex = listex + "- " + Left(dtb2.Rows(i - 1)!f_name, 20) + ".. <br> "
                Next

                Dim ssqlpl As String = "select * from docpermit_lisense_trans where stedit = 1"
                Dim dtbpl As DataTable = New DataTable
                Dim sdapl As SqlDataAdapter = New SqlDataAdapter(ssqlpl, sqlConn)

                sdapl.Fill(dtbpl)
                gvlistlp.DataSource = dtbpl
                gvlistlp.DataBind()
            Catch ex As Exception

            End Try
        End If
        
    End Sub

    'Protected Sub uploaddoc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles uploaddoc.Click
    '    Response.Redirect("entry_permit_doc.aspx")
    'End Sub

    Private Sub GridView1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridView1.RowDeleting
        'If GridView1.SelectedRow Is Nothing Then Exit Sub
        If Trim(Session("permission_permit")) <> "3" Or Trim(Session("permission_permit")) <> "4" Then

            Dim row As GridViewRow = GridView1.SelectedRow
            Dim pid As String
            Dim i As Integer

            i = e.RowIndex
            pid = GridView1.DataKeys(i).Value.ToString

            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)

            conn.Open()
            Try
                Dim sqlquery As String = "update docpermit_trans set stedit = '2' where id='" + pid + "'"
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sqlquery, conn)
                cmd.ExecuteScalar()
            Catch ex As Exception

            Finally
                conn.Close()
                Response.Redirect("permit_list.aspx")
            End Try
        Else
            msginfo = "<script language='javascript' type='text/javascript'> alert('You dont have access to modify document information') </script>"
            Exit Sub
        End If
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        If Trim(Session("permission_permit")) <> "3" And Trim(Session("permission_permit")) <> "4" Then
            If GridView1.SelectedRow Is Nothing Then Exit Sub

            Dim row As GridViewRow = GridView1.SelectedRow
            Session("pid") = GridView1.DataKeys(row.RowIndex).Value.ToString
            'Response.Redirect("view_trip.aspx?ID=" & GridView1.DataKeys(row.RowIndex).Value.ToString)
            Response.Redirect("edit_permit_doc.aspx")
        Else
            'lblinfo.Text = "You dont have access to modify document information"
            msginfo = "<script language='javascript' type='text/javascript'> alert('You dont have access to modify document information') </script>"
            Exit Sub
        End If
        
    End Sub

    Protected Sub btnscrh_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnscrh.Click
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim strcon As String
        If ddlsearch.SelectedValue = "01" Then
            strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where (f_name like '%" + find.Text + "%' or ftype like '%" + find.Text + "%' or fsubject like '%" + find.Text + "%' or company_name like '%" + find.Text + "%' or site_name like '%" + find.Text + "%' or fyear like '%" + find.Text + "%' or fcategory like '%" + find.Text + "%' or f_path like '%" + find.Text + "%' or vendor like '%" + find.Text + "%') and  stedit <> 2 and modul = '" + Session("modul_permit") + "'"
        ElseIf ddlsearch.SelectedValue = "02" Then
            strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where company_name like '%" + find.Text + "%' and stedit <> 2 and modul = '" + Session("modul_permit") + "'"
        ElseIf ddlsearch.SelectedValue = "03" Then
            strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where site_name like '%" + find.Text + "%' and stedit <> 2 and modul = '" + Session("modul_permit") + "'"
        ElseIf ddlsearch.SelectedValue = "04" Then
            strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where department like '%" + find.Text + "%' and stedit <> 2 and modul = '" + Session("modul_permit") + "'"
        ElseIf ddlsearch.SelectedValue = "05" Then
            strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where subdepartment like '%" + find.Text + "%' and stedit <> 2 and modul = '" + Session("modul_permit") + "'"
        ElseIf ddlsearch.SelectedValue = "06" Then
            strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where site_name like '%" + find.Text + "%' and stedit <> 2 and modul = '" + Session("modul_permit") + "'"
        ElseIf ddlsearch.SelectedValue = "07" Then
            strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where f_name like '%" + find.Text + "%' and stedit <> 2 and modul = '" + Session("modul_permit") + "'"
        ElseIf ddlsearch.SelectedValue = "08" Then
            strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where fyear like '%" + find.Text + "%' and stedit <> 2 and modul = '" + Session("modul_permit") + "'"
        ElseIf ddlsearch.SelectedValue = "09" Then
            strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where fcategory like '%" + find.Text + "%' and stedit <> 2 and modul = '" + Session("modul_permit") + "'"

        End If

        'strcon = strcon + " where month(reqdate) = '" + imonth.ToString + "' and YEAR(reqdate) = '" + th + "' and nik = '1231313' and stedit <> 2"
        Dim dtb As DataTable = New DataTable
        Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)

        sda.Fill(dtb)
        GridView1.DataSource = dtb
        GridView1.DataBind()
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Try
            Dim strcon As String

            If find.Text = "" Then
                strcon = "select id,substring(f_name,1,80) as f_name,ftype,fsubject,kdcom, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10), createdtime, 101) as createdtime, convert(char(10),cms_start, 101) as cms_start, convert(char(10),cms_end, 101) as cms_end, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic1) as pic1, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic2) as pic2, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic3) as pic3, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic4) as pic4, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic5) as pic5, kddiv, vendor from docpermit_trans where stedit <> 2 and modul = '" + Session("modul_permit").ToString + "'"
            Else
                If ddlsearch.SelectedValue = "01" Then
                    strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where (f_name like '%" + find.Text + "%' or ftype like '%" + find.Text + "%' or fsubject like '%" + find.Text + "%' or company_name like '%" + find.Text + "%' or site_name like '%" + find.Text + "%' or fyear like '%" + find.Text + "%' or fcategory like '%" + find.Text + "%' or f_path like '%" + find.Text + "%' or vendor like '%" + find.Text + "%') and  stedit <> 2 and modul = '" + Session("modul_permit") + "'"
                ElseIf ddlsearch.SelectedValue = "02" Then
                    strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where company_name like '%" + find.Text + "%' and stedit <> 2 and modul = '" + Session("modul_permit") + "'"
                ElseIf ddlsearch.SelectedValue = "03" Then
                    strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where site_name like '%" + find.Text + "%' and stedit <> 2 and modul = '" + Session("modul_permit") + "'"
                ElseIf ddlsearch.SelectedValue = "04" Then
                    strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where department like '%" + find.Text + "%' and stedit <> 2 and modul = '" + Session("modul_permit") + "'"
                ElseIf ddlsearch.SelectedValue = "05" Then
                    strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where subdepartment like '%" + find.Text + "%' and stedit <> 2 and modul = '" + Session("modul_permit") + "'"
                ElseIf ddlsearch.SelectedValue = "06" Then
                    strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where site_name like '%" + find.Text + "%' and stedit <> 2 and modul = '" + Session("modul_permit") + "'"
                ElseIf ddlsearch.SelectedValue = "07" Then
                    strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where f_name like '%" + find.Text + "%' and stedit <> 2 and modul = '" + Session("modul_permit") + "'"
                ElseIf ddlsearch.SelectedValue = "08" Then
                    strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where fyear like '%" + find.Text + "%' and stedit <> 2 and modul = '" + Session("modul_permit") + "'"
                ElseIf ddlsearch.SelectedValue = "09" Then
                    strcon = "select id,f_name,ftype,fsubject,kdcom, vendor, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10),cms_end, 101) as cms_end, convert(char(10),cms_start, 101) as cms_start, convert(char(10),createdtime, 101) as createdtime from docpermit_trans where fcategory like '%" + find.Text + "%' and stedit <> 2 and modul = '" + Session("modul_permit") + "'"
                End If
            End If

            'strcon = strcon + " where month(reqdate) = '" + imonth.ToString + "' and YEAR(reqdate) = '" + th + "' and nik = '1231313' and stedit <> 2"
            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)

            sda.Fill(dtb)
            GridView1.DataSource = dtb
            GridView1.PageIndex = e.NewPageIndex
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvlistlp_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvlistlp.SelectedIndexChanged
                If Trim(Session("permission_permit")) <> "3" And Trim(Session("permission_permit")) <> "4" Then
            If gvlistlp.SelectedRow Is Nothing Then Exit Sub

            Dim row As GridViewRow = gvlistlp.SelectedRow
            Dim rowc As Integer = gvlistlp.SelectedRow.RowIndex
            Session("pid") = gvlistlp.DataKeys(row.RowIndex).Value.ToString
            llp.lpsubject = gvlistlp.Rows(rowc).Cells(1).Text.Trim()
            llp.lpsupplier = gvlistlp.Rows(rowc).Cells(2).Text.Trim()
            llp.lptyp = gvlistlp.Rows(rowc).Cells(3).Text.Trim()
            llp.lpentrydt = gvlistlp.Rows(rowc).Cells(4).Text.Trim()
            llp.lpremday = gvlistlp.Rows(rowc).Cells(5).Text.Trim()

            'Response.Redirect("view_trip.aspx?ID=" & GridView1.DataKeys(row.RowIndex).Value.ToString)
            Response.Redirect("supp_permit_lisense_entry.aspx")
        Else
            'lblinfo.Text = "You dont have access to modify document information"
            msginfo = "<script language='javascript' type='text/javascript'> alert('You dont have access to modify document information') </script>"
            Exit Sub
        End If
    End Sub
End Class