﻿Imports System.Data.SqlClient

Partial Public Class trip_history
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            fillgrid()
        End If

    End Sub

    Private Sub fillgrid()
        Dim sqleditdetail As String = <![CDATA[
        SELECT
            --A.transid AS 'LeaveNo',
            A.datex AS 'Date',
            CASE 
                WHEN A.StatusWF = 'COMPLT' THEN 'Complete'
                WHEN A.StatusWF = 'RVS' THEN 'Revise'
                WHEN A.StatusWF = 'INP' THEN 'In Progress'
                WHEN A.StatusWF = 'INT' THEN 'Initial'
            END AS 'Status Work Flow',
            B.Nik AS 'PIC User NIK',
            B.Nama AS 'PIC User Name',
            CASE 
                WHEN A.picUserRole = 'DEL' THEN 'Delegator'
                WHEN A.picUserRole = 'MGR' THEN 'Manager'
                WHEN A.picUserRole = 'HRD' THEN 'In HRD'
            END AS 'PIC User Role',
            CASE 
                WHEN A.picAction = '0' THEN '-'
                WHEN A.picAction = '1' THEN 'Approved'
                WHEN A.picAction = '2' THEN 'Reject'
            END AS 'PIC ACTION',
            A.remarks AS 'Remarks'
        FROM V_H001_logWF A
            INNER JOIN H_A101 B ON A.picUser = B.Nik
        WHERE A.transid = '{0}'
        ORDER BY A.datex ASC
        ]]>.Value

        Dim no As String = Request.QueryString("no")
        sqleditdetail = String.Format(sqleditdetail, no)


        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim sdaeditdetail As SqlDataAdapter = New SqlDataAdapter(sqleditdetail, sqlConn)
        Dim dtbeditmedicd As DataTable = New DataTable()
        sdaeditdetail.Fill(dtbeditmedicd)
        GridView1.DataSource = dtbeditmedicd
        GridView1.DataBind()

    End Sub


End Class