﻿Imports System.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Partial Public Class transport_entry
    Inherits System.Web.UI.Page
    Public myDt As DataTable
    Public PessengerDt As DataTable
    Dim customer As New CustomersCls()
    Public js As String
    Public nik As String
    Public kddepar As String
    Public kddivisi As String
    Public kdspv As String
    Public jsmask As String
    Public msg As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("niksite") = "" Or Session("niksite") = Nothing Then
            Response.Redirect("startpage.aspx")
        End If

        If Not IsPostBack Then
            Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()

            Dim strcon As String = "SELECT Nik, niksite, Nama,KdSite, kddepar, (select kddivisi from H_A130 where kddepar = H_A101.kddepar) as kddivisi, (select nmdivisi from H_A140 where kddivisi = (select kddivisi from H_A130 where kddepar = H_A101.kddepar)) as nmdivisi, (select nmdepar from h_a130 where kddepar= H_a101.kddepar) as deparname,(select nmdepar from h_a130 where kddepar= H_a101.kddepar) + ' [' + (select nmsec from H_A209 ,H_A150 where H_A209.kdsec = H_A150.kdsec and H_A150.kdjabat = H_A101.kdjabatan )  + ']' as NmDepar, (select nmjabat from H_A209 ,H_A150 where H_A209.kdsec = H_A150.kdsec and H_A150.kdjabat = H_A101.kdjabatan ) as nmjabat, (SELECT kdjabat from H_A209 ,H_A150 where H_A209.kdsec = H_A150.kdsec and H_A150.kdjabat = H_A101.kdjabatan) as kdjabat, (select top 1 kddepar from h_a130 where kddepar= H_a101.kddepar) as kddepart, (select top 1 nik from H_A101 a where a.KdJabatan = (select top 1 kdjabatsup from H_A150 where KdJabat = H_A101.KdJabatan )) as kdsupv, (select top 1 Nama from H_A101 a where a.KdJabatan = (select top 1 kdjabatsup from H_A150 where KdJabat = H_A101.KdJabatan )) as supv, (SELECT  ISNULL(GLCostLS, '') FROM h_a150 WHERE  kdjabat = h_a101.kdjabatan) as costcode FROM H_A101 WHERE active = '1' AND stedit <> '2' AND nik = '" + Session("niksite") + "'"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            If dtb.Rows.Count > 0 Then
                txtreq.Text = dtb.Rows(0)!Nama.ToString
                hname.Value = dtb.Rows(0)!Nama.ToString
                txtdepar.Text = dtb.Rows(0)!deparname.ToString
                txtdivision.Text = dtb.Rows(0)!nmdivisi.ToString
                txtspv.Text = dtb.Rows(0)!supv.ToString
                txtcostcode.Text = dtb.Rows(0)!costcode.ToString
                hcostcode.Value = dtb.Rows(0)!costcode.ToString
                nik = dtb.Rows(0)!nik.ToString
                hnik.Value = dtb.Rows(0)!nik.ToString
                kddepar = dtb.Rows(0)!kddepar.ToString
                hkdepar.Value = dtb.Rows(0)!kddepar.ToString
                kddivisi = dtb.Rows(0)!kddivisi.ToString
                hkddivisi.Value = dtb.Rows(0)!kddivisi.ToString
                kdspv = dtb.Rows(0)!kdsupv.ToString
                hkdspv.Value = dtb.Rows(0)!kdsupv.ToString
            End If

            myDt = New DataTable()
            myDt = customer.Fetch()
            PessengerDt = New DataTable()
            PessengerDt = customer.Fetchpes()

            Session("myDatatable") = myDt
            Session("pesDatatable") = PessengerDt
            FillCustomerInGrid()
            FillpassengerGrid()

        End If

        'If IsPostBack Then
        '    txtreq.Text = hname.Value
        'End If
        'Me.GridView2.DataSource = (CType(Session("myDatatable"), DataTable)).DefaultView
        'Me.GridView2.DataBind()

        'AddDataToTable("1", Date.Today.TimeOfDay.ToString, Date.Today.Date, "Office", "17.00", Date.Today.Date, "Bandara Soeta", "Menjemput Tamu", CType(Session("myDatatable2"), DataTable))

        'js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn2', 'ctl00_ContentPlaceHolder1_GridView1_ctl02_txtDate', '%m/%d/%Y');</script>"
        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewDateto").ClientID + "', '%m/%d/%Y');</script>"
        'jsmask = "<script type='text/javascript'> $(document).ready(function() { $('input[type=text][id*=" + GridView1.FooterRow.FindControl("txtNewtm").ClientID + "]').mask('99:99'); $('input[type=text][id*=" + GridView1.FooterRow.FindControl("txtNewTimeto").ClientID + "]').mask('99:99'); }); </script>"

        'txtNewdt
        Dim textnewdt As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewdt"), TextBox)
        Dim textnewdateto As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewDateto"), TextBox)
        Dim txtNewNik As TextBox = DirectCast(Gridview2.FooterRow.FindControl("txtNewNik"), TextBox)

        textnewdt.Attributes.Add("readonly", "readonly")
        textnewdateto.Attributes.Add("readonly", "readonly")
        txtNewNik.Attributes.Add("readonly", "readonly")

        cnt.Value = Gridview2.FooterRow.FindControl("txtNewPName").ClientID
        cnt2.Value = Gridview2.FooterRow.FindControl("txtNewNik").ClientID

    End Sub

    Private Sub FillCustomerInGrid()

        'Dim dtCustomer As DataTable = customer.Fetch()
        Dim dtCustomer As DataTable = Session("myDatatable")

        If dtCustomer.Rows.Count > 0 Then
            GridView1.DataSource = dtCustomer
            GridView1.DataBind()
            'Session("myDatatable") = myDt
        Else
            dtCustomer.Rows.Add(dtCustomer.NewRow())
            GridView1.DataSource = dtCustomer
            GridView1.DataBind()

            Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
            GridView1.Rows(0).Cells.Clear()
            GridView1.Rows(0).Cells.Add(New TableCell())
            GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
            GridView1.Rows(0).Cells(0).Text = "No Record Found"
            Session("myDatatable") = dtCustomer
        End If

        'If Session("pesDatatable").ToString <> "" Then
        Dim dtPessenger As DataTable = Session("pesDatatable")
        If dtPessenger.Rows.Count > 0 Then
            If Gridview2.Rows(0).Cells(0).Text = "No Record Found" Then
                Dim TotalColumns As Integer = Gridview2.Rows(0).Cells.Count
                Gridview2.Rows(0).Cells.Clear()
                Gridview2.Rows(0).Cells.Add(New TableCell())
                Gridview2.Rows(0).Cells(0).ColumnSpan = TotalColumns
                Gridview2.Rows(0).Cells(0).Text = "No Record Found"
                Session("pesDatatable") = dtPessenger
            Else
                Gridview2.DataSource = dtPessenger
                Gridview2.DataBind()
                'Session("myDatatable") = myDt
                cnt.Value = Gridview2.FooterRow.FindControl("txtNewPName").ClientID
                cnt2.Value = Gridview2.FooterRow.FindControl("txtNewNik").ClientID
            End If
        Else
            dtPessenger.Rows.Add(dtPessenger.NewRow())
            Gridview2.DataSource = dtPessenger
            Gridview2.DataBind()

            Dim TotalColumns As Integer = Gridview2.Rows(0).Cells.Count
            Gridview2.Rows(0).Cells.Clear()
            Gridview2.Rows(0).Cells.Add(New TableCell())
            Gridview2.Rows(0).Cells(0).ColumnSpan = TotalColumns
            Gridview2.Rows(0).Cells(0).Text = "No Record Found"
            Session("pesDatatable") = dtPessenger
        End If
        ' End If

        Dim textnewdt As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewdt"), TextBox)
        Dim textnewdateto As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewDateto"), TextBox)
        Dim txtNewNik As TextBox = DirectCast(Gridview2.FooterRow.FindControl("txtNewNik"), TextBox)

        textnewdt.Attributes.Add("readonly", "readonly")
        textnewdateto.Attributes.Add("readonly", "readonly")
        txtNewNik.Attributes.Add("readonly", "readonly")
    End Sub

    Private Function CreateDataTable() As DataTable
        Dim myDataTable As DataTable = New DataTable()

        Dim myDataColumn As DataColumn

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "Time"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "Date"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "From"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "Timeto"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "Dateto"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "To"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "Purpose"
        myDataTable.Columns.Add(myDataColumn)

        Return myDataTable
    End Function

    Private Sub AddDataToTable(ByVal num As String, ByVal time As String, ByVal dt As String, ByVal from As String, ByVal timeto As String, ByVal dtto As String, ByVal toke As String, ByVal purpose As String, ByVal myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("No") = num
        row("Time") = time
        row("Date") = dt
        row("From") = from
        row("Timeto") = timeto
        row("Dateto") = dtto
        row("To") = toke
        row("Purpose") = purpose

        myTable.Rows.Add(row)

    End Sub

    'Private Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadd.Click
    'AddDataToTable("1", "11.00", Date.Today.Date, "Office", "17.00", Date.Today.Date, "Bandara Soeta", "Menjemput Tamu", CType(Session("myDatatable"), DataTable))
    'Me.GridView2.DataSource = (CType(Session("myDatatable"), DataTable)).DefaultView
    'Me.GridView2.DataBind()
    'End Sub

    'Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
    'If e.Row.RowType = DataControlRowType.DataRow Then
    '    Dim cmbType As DropDownList = DirectCast(e.Row.FindControl("cmbType"), DropDownList)

    '    If cmbType IsNot Nothing Then
    '        cmbType.DataSource = customer.FetchCustomerType()
    '        cmbType.DataBind()
    '        cmbType.SelectedValue = GridView1.DataKeys(e.Row.RowIndex).Values(1).ToString()
    '    End If
    'End If

    'If e.Row.RowType = DataControlRowType.Footer Then
    '    Dim cmbNewType As DropDownList = DirectCast(e.Row.FindControl("cmbNewType"), DropDownList)
    '    cmbNewType.DataSource = customer.FetchCustomerType()
    '    cmbNewType.DataBind()
    'End If
    'End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)

        If e.CommandName.Equals("AddNew") Then
            Dim txtNewTime As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewTm"), TextBox)
            Dim txtNewdt As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewdt"), TextBox)
            Dim txtNewFrom As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewFrom"), TextBox)
            Dim txtNewTimeto As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewTimeto"), TextBox)
            Dim txtNewDateto As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewDateto"), TextBox)
            Dim txtNewTo As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewTo"), TextBox)
            Dim txtNewPurpose As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewPurpose"), TextBox)

            If txtNewTime.Text = "" Then
                msg = "<script type ='text/javascript' > alert('Please entry departure time') </script>"
                If Gridview2.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = Gridview2.Rows(0).Cells.Count
                    Gridview2.Rows(0).Cells.Clear()
                    Gridview2.Rows(0).Cells.Add(New TableCell())
                    Gridview2.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    Gridview2.Rows(0).Cells(0).Text = "No Record Found"
                End If

                If GridView1.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                    GridView1.Rows(0).Cells.Clear()
                    GridView1.Rows(0).Cells.Add(New TableCell())
                    If TotalColumns = 1 Then TotalColumns = 10
                    GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    GridView1.Rows(0).Cells(0).Text = "No Record Found"
                End If
                Return
            End If

            If txtNewdt.Text = "" Then
                msg = "<script type ='text/javascript' > alert('Please entry departure date') </script>"
                If Gridview2.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = Gridview2.Rows(0).Cells.Count
                    Gridview2.Rows(0).Cells.Clear()
                    Gridview2.Rows(0).Cells.Add(New TableCell())
                    Gridview2.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    Gridview2.Rows(0).Cells(0).Text = "No Record Found"
                End If

                If GridView1.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                    GridView1.Rows(0).Cells.Clear()
                    GridView1.Rows(0).Cells.Add(New TableCell())
                    If TotalColumns = 1 Then TotalColumns = 10
                    GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    GridView1.Rows(0).Cells(0).Text = "No Record Found"
                End If
                Return
            End If

            If txtNewFrom.Text = "" Then
                msg = "<script type ='text/javascript' > alert('Please entry departure location') </script>"
                If Gridview2.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = Gridview2.Rows(0).Cells.Count
                    Gridview2.Rows(0).Cells.Clear()
                    Gridview2.Rows(0).Cells.Add(New TableCell())
                    Gridview2.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    Gridview2.Rows(0).Cells(0).Text = "No Record Found"
                End If

                If GridView1.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                    GridView1.Rows(0).Cells.Clear()
                    GridView1.Rows(0).Cells.Add(New TableCell())
                    If TotalColumns = 1 Then TotalColumns = 10
                    GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    GridView1.Rows(0).Cells(0).Text = "No Record Found"
                End If
                Return
            End If

            If txtNewTimeto.Text = "" Then
                msg = "<script type ='text/javascript' > alert('Please entry arrival time') </script>"
                If Gridview2.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = Gridview2.Rows(0).Cells.Count
                    Gridview2.Rows(0).Cells.Clear()
                    Gridview2.Rows(0).Cells.Add(New TableCell())
                    Gridview2.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    Gridview2.Rows(0).Cells(0).Text = "No Record Found"
                End If

                If GridView1.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                    GridView1.Rows(0).Cells.Clear()
                    GridView1.Rows(0).Cells.Add(New TableCell())
                    If TotalColumns = 1 Then TotalColumns = 10
                    GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    GridView1.Rows(0).Cells(0).Text = "No Record Found"
                End If
                Return
            End If

            If txtNewDateto.Text = "" Then
                msg = "<script type ='text/javascript' > alert('Please entry arrival date') </script>"
                If Gridview2.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = Gridview2.Rows(0).Cells.Count
                    Gridview2.Rows(0).Cells.Clear()
                    Gridview2.Rows(0).Cells.Add(New TableCell())
                    Gridview2.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    Gridview2.Rows(0).Cells(0).Text = "No Record Found"
                End If

                If GridView1.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                    GridView1.Rows(0).Cells.Clear()
                    GridView1.Rows(0).Cells.Add(New TableCell())
                    If TotalColumns = 1 Then TotalColumns = 10
                    GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    GridView1.Rows(0).Cells(0).Text = "No Record Found"
                End If
                Return
            End If

            If txtNewTo.Text = "" Then
                msg = "<script type ='text/javascript' > alert('Please entry arrival location') </script>"
                If Gridview2.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = Gridview2.Rows(0).Cells.Count
                    Gridview2.Rows(0).Cells.Clear()
                    Gridview2.Rows(0).Cells.Add(New TableCell())
                    Gridview2.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    Gridview2.Rows(0).Cells(0).Text = "No Record Found"
                End If

                If GridView1.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                    GridView1.Rows(0).Cells.Clear()
                    GridView1.Rows(0).Cells.Add(New TableCell())
                    If TotalColumns = 1 Then TotalColumns = 10
                    GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    GridView1.Rows(0).Cells(0).Text = "No Record Found"
                End If
                Return
            End If

            If txtNewPurpose.Text = "" Then
                msg = "<script type ='text/javascript' > alert('Please entry purpose') </script>"
                If Gridview2.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = Gridview2.Rows(0).Cells.Count
                    Gridview2.Rows(0).Cells.Clear()
                    Gridview2.Rows(0).Cells.Add(New TableCell())
                    Gridview2.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    Gridview2.Rows(0).Cells(0).Text = "No Record Found"
                End If

                If GridView1.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                    GridView1.Rows(0).Cells.Clear()
                    GridView1.Rows(0).Cells.Add(New TableCell())
                    If TotalColumns = 1 Then TotalColumns = 10
                    GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    GridView1.Rows(0).Cells(0).Text = "No Record Found"
                End If
                Return
            End If

            If DateTime.Parse(txtNewdt.Text) = DateTime.Parse(txtNewDateto.Text) Then
                If DateTime.Parse(txtNewTime.Text) > DateTime.Parse(txtNewTimeto.Text) Then
                    msg = "<script type ='text/javascript' > alert('time from cant not > time to') </script>"
                    If Gridview2.Rows(0).Cells(0).Text = "No Record Found" Then
                        Dim TotalColumns As Integer = Gridview2.Rows(0).Cells.Count
                        Gridview2.Rows(0).Cells.Clear()
                        Gridview2.Rows(0).Cells.Add(New TableCell())
                        Gridview2.Rows(0).Cells(0).ColumnSpan = TotalColumns
                        Gridview2.Rows(0).Cells(0).Text = "No Record Found"
                    End If

                    If GridView1.Rows(0).Cells(0).Text = "No Record Found" Then
                        Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                        GridView1.Rows(0).Cells.Clear()
                        GridView1.Rows(0).Cells.Add(New TableCell())
                        If TotalColumns = 1 Then TotalColumns = 10
                        GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                        GridView1.Rows(0).Cells(0).Text = "No Record Found"
                    End If
                    Return
                End If
            Else
                If DateTime.Parse(txtNewdt.Text) < DateTime.Parse(txtNewDateto.Text) Then
                Else
                    If DateTime.Parse(txtNewdt.Text) > DateTime.Parse(txtNewDateto.Text) Then
                        msg = "<script type ='text/javascript' > alert('date from cant not > date to') </script>"
                        If Gridview2.Rows(0).Cells(0).Text = "No Record Found" Then
                            Dim TotalColumns As Integer = Gridview2.Rows(0).Cells.Count
                            Gridview2.Rows(0).Cells.Clear()
                            Gridview2.Rows(0).Cells.Add(New TableCell())
                            Gridview2.Rows(0).Cells(0).ColumnSpan = TotalColumns
                            Gridview2.Rows(0).Cells(0).Text = "No Record Found"
                        End If

                        If GridView1.Rows(0).Cells(0).Text = "No Record Found" Then
                            Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                            GridView1.Rows(0).Cells.Clear()
                            GridView1.Rows(0).Cells.Add(New TableCell())
                            If TotalColumns = 1 Then TotalColumns = 10
                            GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                            GridView1.Rows(0).Cells(0).Text = "No Record Found"
                        End If
                        Return
                    End If
                End If

            End If

            If GridView1.Rows(0).Cells(0).Text = "No Record Found" Then
                GridView1.Rows(0).Cells.Clear()
                GridView1.Rows(0).Cells.Remove(New TableCell())
            End If

            customer.Insert(txtNewTime.Text, txtNewdt.Text, txtNewFrom.Text, txtNewTimeto.Text, txtNewDateto.Text, txtNewTo.Text, txtNewPurpose.Text, CType(Session("myDatatable"), DataTable))
            'Me.GridView1.DataSource = (CType(Session("myDatatable"), DataTable)).DefaultView
            myDt = Session("myDatatable")
            FillCustomerInGrid()
            'js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewDateto").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn2', '" + GridView1.Rows(0).ClientID + "_txtDate', '%m/%d/%Y'); cal.manageFields('btn4', '" + GridView1.Rows(0).ClientID + "_txtdateto', '%m/%d/%Y');</script>"
            js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewDateto").ClientID + "', '%m/%d/%Y');</script>"
            'jsmask = "<script type='text/javascript'> $(document).ready(function() { $('input[type=text][id*=" + GridView1.FooterRow.FindControl("txtNewtm").ClientID + "]').mask('99:99'); $('input[type=text][id*=" + GridView1.FooterRow.FindControl("txtNewTimeto").ClientID + "]').mask('99:99'); }); </script>"
        End If
    End Sub

    Protected Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As GridViewEditEventArgs)
        GridView1.EditIndex = e.NewEditIndex
        FillCustomerInGrid()

        Dim txtdate As TextBox = DirectCast(GridView1.Rows(e.NewEditIndex).FindControl("txtdate"), TextBox)
        Dim txtdateto As TextBox = DirectCast(GridView1.Rows(e.NewEditIndex).FindControl("txtdateto"), TextBox)
        txtdate.Attributes.Add("readonly", "readonly")
        txtdateto.Attributes.Add("readonly", "readonly")

        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewDateto").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn2', '" + GridView1.Rows(e.NewEditIndex).ClientID + "_txtDate', '%m/%d/%Y'); cal.manageFields('btn4', '" + GridView1.Rows(e.NewEditIndex).ClientID + "_txtdateto', '%m/%d/%Y');</script>"
        'js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewDateto").ClientID + "', '%m/%d/%Y');</script>"
        'jsmask = "<script type='text/javascript'> $(document).ready(function() { $('input[type=text][id*=" + GridView1.FooterRow.FindControl("txtNewtm").ClientID + "]').mask('99:99'); $('input[type=text][id*=" + GridView1.FooterRow.FindControl("txtNewTimeto").ClientID + "]').mask('99:99'); $('input[type=text][id*=" + GridView1.Rows(e.NewEditIndex).ClientID + "_txtTime]').mask('99:99'); $('input[type=text][id*=" + GridView1.Rows(e.NewEditIndex).ClientID + "_txttimeto]').mask('99:99'); }); </script>"
    End Sub

    Protected Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As GridViewCancelEditEventArgs)
        GridView1.EditIndex = -1
        FillCustomerInGrid()
        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewDateto").ClientID + "', '%m/%d/%Y');</script>"

    End Sub

    Protected Sub GridView1_RowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs)
        Dim txtTime As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtTime"), TextBox)
        Dim txtDate As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtDate"), TextBox)
        Dim txtfrom As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtfrom"), TextBox)
        Dim txttimeto As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txttimeto"), TextBox)
        Dim txtdateto As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtdateto"), TextBox)
        Dim txtto As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtto"), TextBox)
        Dim txtpurpose As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtpurpose"), TextBox)
        'Dim cmbType As DropDownList = DirectCast(GridView1.Rows(e.RowIndex).FindControl("cmbType"), DropDownList)

        If txtTime.Text = "" Then
            msg = "<script type ='text/javascript' > alert('Please entry departure time') </script>"
            Return
        End If

        If txtDate.Text = "" Then
            msg = "<script type ='text/javascript' > alert('Please entry departure date') </script>"
            Return
        End If

        If txtfrom.Text = "" Then
            msg = "<script type ='text/javascript' > alert('Please entry departure location') </script>"
            Return
        End If

        If txttimeto.Text = "" Then
            msg = "<script type ='text/javascript' > alert('Please entry arrival time') </script>"
            Return
        End If

        If txtdateto.Text = "" Then
            msg = "<script type ='text/javascript' > alert('Please entry arrival date') </script>"
            Return
        End If

        If txtto.Text = "" Then
            msg = "<script type ='text/javascript' > alert('Please entry arrival location') </script>"
            Return
        End If

        If txtpurpose.Text = "" Then
            msg = "<script type ='text/javascript' > alert('Please entry purpose') </script>"
            Return
        End If

        If DateTime.Parse(txtDate.Text) = DateTime.Parse(txtdateto.Text) Then
            If DateTime.Parse(txtTime.Text) > DateTime.Parse(txttimeto.Text) Then
                msg = "<script type ='text/javascript' > alert('time from cant not > time to') </script>"
                Return
            End If
        Else
            If DateTime.Parse(txtDate.Text) < DateTime.Parse(txtdateto.Text) Then
            Else
                If DateTime.Parse(txtDate.Text) > DateTime.Parse(txtdateto.Text) Then
                    msg = "<script type ='text/javascript' > alert('date from cant not > date to') </script>"
                    Return
                End If
            End If

        End If

        'customer.Update(GridView1.DataKeys(e.RowIndex).Values(0).ToString(), txtName.Text, cmbGender.SelectedValue, txtCity.Text, CustomerType.SelectedValue)
        customer.Update(GridView1.DataKeys(e.RowIndex).Values(0).ToString(), txtTime.Text, txtDate.Text, txtfrom.Text, txttimeto.Text, txtdateto.Text, txtto.Text, txtpurpose.Text, CType(Session("myDatatable"), DataTable), e.RowIndex)

        GridView1.EditIndex = -1
        FillCustomerInGrid()

    End Sub

    Protected Sub GridView1_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)
        customer.Delete(GridView1.DataKeys(e.RowIndex).Values(0).ToString(), CType(Session("myDatatable"), DataTable), e.RowIndex)
        FillCustomerInGrid()
        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewDateto").ClientID + "', '%m/%d/%Y');</script>"
        'jsmask = "<script type='text/javascript'> $(document).ready(function() { $('input[type=text][id*=" + GridView1.FooterRow.FindControl("txtNewtm").ClientID + "]').mask('99:99'); $('input[type=text][id*=" + GridView1.FooterRow.FindControl("txtNewTimeto").ClientID + "]').mask('99:99'); }); </script>"
    End Sub

    Protected Sub GridView2_RowCancelingEdit(ByVal sender As Object, ByVal e As GridViewCancelEditEventArgs)
        Gridview2.EditIndex = -1
        FillpassengerGrid()
        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewDateto").ClientID + "', '%m/%d/%Y');</script>"
    End Sub

    Protected Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)

    End Sub

    Protected Sub GridView2_RowEditing(ByVal sender As Object, ByVal e As GridViewEditEventArgs)
        Gridview2.EditIndex = e.NewEditIndex
        FillpassengerGrid()
        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewDateto").ClientID + "', '%m/%d/%Y');</script>"
        cnt.Value = Gridview2.Rows(e.NewEditIndex).FindControl("txtPName").ClientID
        cnt2.Value = Gridview2.Rows(e.NewEditIndex).FindControl("txtNik").ClientID

        Dim txtNik As TextBox = DirectCast(Gridview2.Rows(e.NewEditIndex).FindControl("txtNik"), TextBox)
        txtNik.Attributes.Add("readonly", "readonly")
    End Sub

    Protected Sub GridView2_RowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs)
        'Dim txtNewNo As TextBox = DirectCast(Gridview2.Rows(e.RowIndex).FindControl("txtNo"), TextBox)
        Dim txtNewNik As TextBox = DirectCast(Gridview2.Rows(e.RowIndex).FindControl("txtNik"), TextBox)
        Dim txtNewPName As TextBox = DirectCast(Gridview2.Rows(e.RowIndex).FindControl("txtPName"), TextBox)
        Dim cmbNewType As DropDownList = DirectCast(Gridview2.Rows(e.RowIndex).FindControl("cmbType"), DropDownList)

        If txtNewPName.Text = "" Then
            msg = "<script type ='text/javascript' > alert('Please entry Passengger Name') </script>"
            Return
        End If
        Dim pno As String = ""
        customer.passangerupdate(Gridview2.DataKeys(e.RowIndex).Values(0).ToString(), txtNewNik.Text, txtNewPName.Text, cmbNewType.SelectedValue, CType(Session("pesDatatable"), DataTable), e.RowIndex)
        'PessengerDt = Session("pesDatatable")
        Gridview2.EditIndex = -1
        FillpassengerGrid()
    End Sub

    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
        If e.CommandName.Equals("AddNew") Then
            'Dim txtNewNo As TextBox = DirectCast(Gridview2.FooterRow.FindControl("txtNewNo"), TextBox)
            Dim txtNewNik As TextBox = DirectCast(Gridview2.FooterRow.FindControl("txtNewNik"), TextBox)
            Dim txtNewPName As TextBox = DirectCast(Gridview2.FooterRow.FindControl("txtNewPName"), TextBox)
            Dim cmbNewType As DropDownList = DirectCast(Gridview2.FooterRow.FindControl("cmbNewType"), DropDownList)

            If Gridview2.Rows(0).Cells(0).Text = "No Record Found" Then
                Gridview2.Rows(0).Cells.Clear()
                Gridview2.Rows(0).Cells.Remove(New TableCell())
            End If

            If txtNewPName.Text = "" Then
                msg = "<script type ='text/javascript' > alert('Please entry Passengger Name') </script>"
                Return
            End If

            'txtNewNo.Text = PessengerDt.Rows.Count
            Dim pno As String = ""
            customer.passangerinsert(pno, txtNewNik.Text, txtNewPName.Text, cmbNewType.SelectedValue, CType(Session("pesDatatable"), DataTable))
            PessengerDt = Session("pesDatatable")
            FillpassengerGrid()
        End If
    End Sub

    Protected Sub GridView2_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)
        customer.PDelete(Gridview2.DataKeys(e.RowIndex).Values(0).ToString(), CType(Session("pesDatatable"), DataTable), e.RowIndex)
        FillpassengerGrid()
    End Sub

    Private Sub FillpassengerGrid()
        'Dim dtCustomer As DataTable = customer.Fetch()
        Dim dtPessenger As DataTable = Session("pesDatatable")
        If dtPessenger.Rows.Count > 0 Then
            If Gridview2.Rows(0).Cells.Count <> 0 Then
                If Gridview2.Rows(0).Cells(0).Text = "No Record Found" Then
                Else
                    Gridview2.DataSource = dtPessenger
                    Gridview2.DataBind()
                    'Session("myDatatable") = myDt
                    cnt.Value = Gridview2.FooterRow.FindControl("txtNewPName").ClientID
                    cnt2.Value = Gridview2.FooterRow.FindControl("txtNewNik").ClientID
                End If
            Else
                Gridview2.DataSource = dtPessenger
                Gridview2.DataBind()
                'Session("myDatatable") = myDt
                cnt.Value = Gridview2.FooterRow.FindControl("txtNewPName").ClientID
                cnt2.Value = Gridview2.FooterRow.FindControl("txtNewNik").ClientID
            End If
        Else
            dtPessenger.Rows.Add(dtPessenger.NewRow())
            Gridview2.DataSource = dtPessenger
            Gridview2.DataBind()

            Dim TotalColumns As Integer = Gridview2.Rows(0).Cells.Count
            Gridview2.Rows(0).Cells.Clear()
            Gridview2.Rows(0).Cells.Add(New TableCell())
            Gridview2.Rows(0).Cells(0).ColumnSpan = TotalColumns
            Gridview2.Rows(0).Cells(0).Text = "No Record Found"
            Session("pesDatatable") = dtPessenger
        End If

        Dim dtCustomer As DataTable = Session("myDatatable")
        If dtCustomer.Rows.Count > 0 Then

            If GridView1.Rows(0).Cells(0).Text = "No Record Found" Then
                Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                GridView1.Rows(0).Cells.Clear()
                GridView1.Rows(0).Cells.Add(New TableCell())
                If TotalColumns = 1 Then TotalColumns = 10
                GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                GridView1.Rows(0).Cells(0).Text = "No Record Found"
                Session("myDatatable") = dtCustomer
            Else
                GridView1.DataSource = dtCustomer
                GridView1.DataBind()
            End If

        Else
            dtCustomer.Rows.Add(dtCustomer.NewRow())
            GridView1.DataSource = dtCustomer
            GridView1.DataBind()

            Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
            GridView1.Rows(0).Cells.Clear()
            GridView1.Rows(0).Cells.Add(New TableCell())
            GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
            GridView1.Rows(0).Cells(0).Text = "No Record Found"
            Session("myDatatable") = dtCustomer
        End If

        Dim textnewdt As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewdt"), TextBox)
        Dim textnewdateto As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewDateto"), TextBox)
        Dim txtNewNik As TextBox = DirectCast(Gridview2.FooterRow.FindControl("txtNewNik"), TextBox)

        textnewdt.Attributes.Add("readonly", "readonly")
        textnewdateto.Attributes.Add("readonly", "readonly")
        txtNewNik.Attributes.Add("readonly", "readonly")
    End Sub

    Private Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim sqlQuery As String
        Dim cmd As SqlCommand
        Dim dtmnow As String
        Dim dtynow As String
        Dim stransno As String
        Dim dtb As DataTable = New DataTable()
        Dim transno As String
        Dim li_count As Integer
        Dim li_count2 As Integer
        Dim ls_sqlinsert_T_H00301 As String
        Dim ls_sqlinsert_T_H00302 As String
        Dim cmd2 As SqlCommand
        Dim cmd3 As SqlCommand

        Dim mydtb As DataTable = Session("myDatatable")
        Dim mydtb2 As DataTable = Session("pesDatatable")

        'If txtspreq.Text = "" Then
        '    msg = "<script type ='text/javascript' > alert('Please entry your Superior Name') </script>"
        '    Return
        'End If

        If mydtb.Rows.Count > 0 Then
            If mydtb.Rows(0)!no.ToString = "" Then
                msg = "<script type ='text/javascript' > alert('Please entry your departure and arrival data') </script>"
                If Gridview2.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = Gridview2.Rows(0).Cells.Count
                    Gridview2.Rows(0).Cells.Clear()
                    Gridview2.Rows(0).Cells.Add(New TableCell())
                    Gridview2.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    Gridview2.Rows(0).Cells(0).Text = "No Record Found"
                End If

                If GridView1.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                    GridView1.Rows(0).Cells.Clear()
                    GridView1.Rows(0).Cells.Add(New TableCell())
                    If TotalColumns = 1 Then TotalColumns = 10
                    GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    GridView1.Rows(0).Cells(0).Text = "No Record Found"
                End If
                Return
            End If
        End If

        If mydtb2.Rows.Count > 0 Then
            If mydtb2.Rows(0)!PNo.ToString = "" Then
                msg = "<script type ='text/javascript' > alert('Please entry your Passenger data') </script>"
                If Gridview2.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = Gridview2.Rows(0).Cells.Count
                    Gridview2.Rows(0).Cells.Clear()
                    Gridview2.Rows(0).Cells.Add(New TableCell())
                    Gridview2.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    Gridview2.Rows(0).Cells(0).Text = "No Record Found"
                End If

                If GridView1.Rows(0).Cells(0).Text = "No Record Found" Then
                    Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                    GridView1.Rows(0).Cells.Clear()
                    GridView1.Rows(0).Cells.Add(New TableCell())
                    If TotalColumns = 1 Then TotalColumns = 10
                    GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                    GridView1.Rows(0).Cells(0).Text = "No Record Found"
                End If
                Return
            End If
        End If

        Try
            dtmnow = DateTime.Now.ToString("MM")
            If dtmnow = "01" Then dtmnow = "I"
            If dtmnow = "02" Then dtmnow = "II"
            If dtmnow = "03" Then dtmnow = "III"
            If dtmnow = "04" Then dtmnow = "IV"
            If dtmnow = "05" Then dtmnow = "V"
            If dtmnow = "06" Then dtmnow = "VI"
            If dtmnow = "07" Then dtmnow = "VII"
            If dtmnow = "08" Then dtmnow = "VIII"
            If dtmnow = "09" Then dtmnow = "IX"
            If dtmnow = "10" Then dtmnow = "X"
            If dtmnow = "11" Then dtmnow = "XI"
            If dtmnow = "12" Then dtmnow = "XII"

            dtynow = Date.Now.Year.ToString
            stransno = "/TRF-JRN/" + dtmnow + "/" + dtynow

            Dim strcon As String = "select TransportNo from T_H003 where TransportNo like '%" + stransno + "%'"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, conn)
            sda.Fill(dtb)

            If dtb.Rows.Count = 0 Then
                transno = "000001" + stransno
            ElseIf dtb.Rows.Count > 0 Then
                I = 0
                I = dtb.Rows.Count + 1
                If dtb.Rows.Count > 0 And dtb.Rows.Count < 9 Then
                    transno = "00000" + I.ToString + stransno
                ElseIf dtb.Rows.Count >= 9 And dtb.Rows.Count < 99 Then
                    transno = "0000" + I.ToString + stransno
                ElseIf dtb.Rows.Count >= 99 And dtb.Rows.Count < 999 Then
                    transno = "000" + I.ToString + stransno
                ElseIf dtb.Rows.Count >= 999 And dtb.Rows.Count < 9999 Then
                    transno = "00" + I.ToString + stransno
                ElseIf dtb.Rows.Count >= 9999 And dtb.Rows.Count < 99999 Then
                    transno = "0" + I.ToString + stransno
                ElseIf dtb.Rows.Count >= 99999 And dtb.Rows.Count < 999999 Then
                    transno = I.ToString + stransno
                ElseIf dtb.Rows.Count >= 999999 Then
                    transno = "Error on generate Tripnum"
                End If
            End If

            conn.Open()

            sqlQuery = "insert into T_H003 (TransportNo, nik, kddepar, kddivisi, costcode, sup_nik, special_req, reqby, reqdate, CreatedBy, CreatedIn, CreatedTime, StEdit, fstatus) values ('" + transno + "', '" + hnik.Value + "', '" + hkdepar.Value + "', '" + hkddivisi.Value + "', '" + hcostcode.Value + "', '" + hkdspv.Value + "', '" + txtspreq.Text + "', '" + hnik.Value + "', '" + Today.ToString + "', '" + Session("niksite").ToString + "', '" + HttpContext.Current.Request.UserHostName.ToString + "', '" + DateTime.Now + "', '0', '0' )"
            cmd = New SqlCommand(sqlQuery, conn)
            cmd.ExecuteScalar()

            For li_count = 0 To mydtb.Rows.Count - 1
                ls_sqlinsert_T_H00301 = "insert into T_H00301 (GenID, TransportNo, Timedes, des_date, dep_from, timeto, to_date, des_to, purpose) values ('" + mydtb.Rows(li_count)!No.ToString + "','" + transno + "','" + mydtb.Rows(li_count)!Time.ToString + "','" + mydtb.Rows(li_count)!Date.ToString + "','" + mydtb.Rows(li_count)!From.ToString + "','" + mydtb.Rows(li_count)!Timeto.ToString + "','" + mydtb.Rows(li_count)!Dateto.ToString + "','" + mydtb.Rows(li_count)!To.ToString + "','" + mydtb.Rows(li_count)!Purpose.ToString + "')"
                cmd2 = New SqlCommand(ls_sqlinsert_T_H00301, conn)
                cmd2.ExecuteScalar()
            Next

            For li_count2 = 0 To mydtb2.Rows.Count - 1
                ls_sqlinsert_T_H00302 = "insert into T_H00302 (GenID, TransportNo, pes_nik, pes_name, pes_type) values ('" + mydtb2.Rows(li_count2)!PNo.ToString + "', '" + transno + "', '" + mydtb2.Rows(li_count2)!PNik.ToString + "', '" + mydtb2.Rows(li_count2)!PName.ToString + "', '" + mydtb2.Rows(li_count2)!PType.ToString + "')"
                cmd3 = New SqlCommand(ls_sqlinsert_T_H00302, conn)
                cmd3.ExecuteScalar()
            Next

            btnsave.Enabled = False
            btnprint.Enabled = True
            Session("vtrans") = transno

            Response.Redirect("transport_edit.aspx?save=1")
        Catch ex As Exception

        Finally
            conn.Close()
        End Try

    End Sub

    Private Sub btnprint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnprint.Click
        Response.Redirect("print_trans_req.aspx")
    End Sub
End Class