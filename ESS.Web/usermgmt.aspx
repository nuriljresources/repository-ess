﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="usermgmt.aspx.vb" Inherits="EXCELLENT.usermgmt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="css/calendar-c.css"/> 
 <style type="text/css">
        ul#navigation li a.m6
        {        	
	        background-color:#00CC00;color: #000000;
        }
         ul.dropdown *.dir4 {
          padding-right: 20px;
          background-image: url("/memo/css/dropdown/themes/default/images/nav-arrow-down.png");
          background-position: 100% 50%;
          background-repeat: no-repeat;
          background-color: #FFA500;
         }
   </style>
<script type="text/javascript">
   function save() {
      if (document.getElementById("newpass1").value != document.getElementById("newpass2").value){
         alert("Password baru tidak sama")
         return false;
      }else{
         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/UserMgmt.asmx/ChangePass",
            data: "{'_nik':" + JSON.stringify(document.getElementById("username").value) + ",'_oldpass':" + JSON.stringify(document.getElementById("oldpass").value) + ",'_newpass':" + JSON.stringify(document.getElementById("newpass1").value) +  "}",
            dataType: "json",
            success: function(res) {
                document.getElementById('log').innerHTML = res.d
            },
            error: function(err) {
                alert("error: " + err.responseText);
            }
        });
      }  
   }
</script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<br />
<div id="cpassword">
   <center>
     <table class="data-table" style="border: 1px solid #00CC66">        
        <tr>
        <td colspan="2" bgcolor="#33CC33" 
                style="font-size: large; font-family: calibri; font-weight: bold;">
            CHANGE PASSWORD
        </td>
        </tr>
         <tr><td style="text-align:right">User Name </td><td><input type="text" id="username" disabled="disabled" value="<%= Session("otorisasi") %>" class="tb10"/></td></tr>
         <tr><td style="text-align:right">Old Password </td><td><input type="password" id="oldpass" class="tb10"/></td></tr>
         <tr><td style="text-align:right">New Password </td><td><input type="password" id="newpass1" class="tb10"/></td></tr>
         <tr><td style="text-align:right">Re-entry New Password </td><td><input type="password" id="newpass2" class="tb10"/></td></tr>
         <tr><td colspan="2" style="text-align:right"><input type="button" value="Change" onclick="save()" style="width: 41%; font-family: calibri;" /></td></tr>
         <tr><td colspan="2" style="text-align:center"><div id="log" style="color:red;font:Trebechuet;"></div></td></tr>
   </table>
   </center>
</div>

</asp:Content>
