﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="spl_dec_entry.aspx.vb" Inherits="EXCELLENT.spl_dec_entry" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Overtime Declaration
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content> 

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
<caption style="text-align: center; font-size: 1.5em; color:White;">Overtime Declaration Entry</caption>
<tr>
<td style="width:5%;">Periode</td>
<td style="width:8%;">
<asp:DropDownList ID="ddlbulan" runat="server">
    <asp:ListItem Value = "1">January</asp:ListItem>
    <asp:ListItem Value = "2">February</asp:ListItem>
    <asp:ListItem Value = "3">March</asp:ListItem>
    <asp:ListItem Value = "4">April</asp:ListItem>
    <asp:ListItem Value = "5">May</asp:ListItem>
    <asp:ListItem Value = "6">June</asp:ListItem>
    <asp:ListItem Value = "7">July</asp:ListItem>
    <asp:ListItem Value = "8">August</asp:ListItem>
    <asp:ListItem Value = "9">September</asp:ListItem>
    <asp:ListItem Value = "10">October</asp:ListItem>
    <asp:ListItem Value = "11">November</asp:ListItem>
    <asp:ListItem Value = "12">December</asp:ListItem>
</asp:DropDownList>
<asp:TextBox ID="tripfrom" style="width:70px;" runat="server" width="90px" ReadOnly="true" Visible ="false"></asp:TextBox></td>
<td style="width:5%;"><asp:DropDownList ID="ddltahun" runat="server"></asp:DropDownList></td>

<td style="width:5%;"><asp:Button ID="btnSearch" runat="server" Text="Search"/></td>
<%--<td style="width:5%;"><input type="button" visible="false" id="cetak" value="Print" onclick="frmcetak()" style="width: 80px; font-family: Calibri; " /></td>--%>
<td>
 <asp:TextBox ID="tripto" style="width:70px;" runat="server" ReadOnly="true" visible="false"></asp:TextBox>
</td>
<td></td>
</tr></table>
<table><tr><td style="vertical-align:top;">
<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="noreg" AllowPaging="true" PageSize="100" Width="600px">
    <RowStyle Font-Size="Smaller" />
<Columns>

<asp:BoundField DataField="noreg" HeaderText="Trans Number" Visible="true" 
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
 </asp:BoundField>
<asp:BoundField DataField="Periode" HeaderText="Period" Visible = "false"  
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
 </asp:BoundField>
    <asp:BoundField DataField="niksite" HeaderText="NIK" HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="90px"></HeaderStyle>
<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    
     <asp:BoundField DataField="nama" HeaderText="Name" HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="200px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>

<asp:BoundField DataField="jam_start" HeaderText="Time Start" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="jam_end" HeaderText="Time end" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>  

<asp:BoundField DataField="status" HeaderText="Status" Visible = "false" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:CommandField ShowSelectButton="True" SelectText="Select" ButtonType="Button"  HeaderStyle-Width="40px" ItemStyle-VerticalAlign="top" >
<HeaderStyle Width="40px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:CommandField>
</Columns> 
    <SelectedRowStyle BackColor="#99CCFF" />
    <HeaderStyle BackColor="#0099FF" />
</asp:GridView>
</td>
<td style="vertical-align:top; padding-left: 20px;">
<table>
<tr>
<td>No</td><td><asp:TextBox ID="txtno" runat="server"></asp:TextBox></td>
</tr>
<tr>
<td>Name</td><td><asp:TextBox ID="txtname" runat="server"></asp:TextBox></td>
</tr>
<tr>
<td>Time Start</td><td><asp:TextBox ID="txttimestart" runat="server"></asp:TextBox></td>
</tr>
<tr>
<td>Time End</td><td><asp:TextBox ID="txttimefinish" runat="server"></asp:TextBox></td>
</tr>
<tr>
<td style="vertical-align:top;">Task Description</td><td><asp:TextBox ID="txtdec" runat="server" TextMode="MultiLine" Rows="10" Columns="50"></asp:TextBox></td>
</tr>
<tr>
<td>
<asp:Button ID="btnsavedec" runat="server" Text="Save Declaration"/>
</td>
</tr>
</table>
</td>
</tr></table>

<%=msg%>
</asp:Content>