﻿Public Partial Class RedirectDOA
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim strGUID As String() = Guid.NewGuid().ToString().Split("-")
        If (Session("otorisasi") <> Nothing) Then

            Dim encriptID As String = ESS.Common.Rijndael.Encrypt(Session("otorisasi").ToString())

            Response.Redirect(ESS.Common.SITE_SETTINGS.DOA_URL + encriptID)
        End If

    End Sub

End Class