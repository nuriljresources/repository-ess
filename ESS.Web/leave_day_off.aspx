﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="leave_day_off.aspx.vb"
    Inherits="EXCELLENT.leave_day_off" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/site.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            function UseSelected(transid, dateex, usage) {
                var pw = window.opener;
                var inputFrm = pw.document.forms['aspnetForm'];
                inputFrm.elements['ctl00_ContentPlaceHolder1_txtdayoff'].value = transid;
                inputFrm.elements['ctl00_ContentPlaceHolder1_dayoff_tripid'].value = transid;
                inputFrm.elements['ctl00_ContentPlaceHolder1_dayoff_dateex'].value = dateex;
                inputFrm.elements['ctl00_ContentPlaceHolder1_dayoff_usage'].value = usage;
                
                window.close();
            }
        </script>
</head>
<body>
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" />
    <asp:UpdatePanel ID="MyUpdatePanel" runat="server">
        <ContentTemplate>
            <div>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
                    <caption style="text-align: center">
                        Search Employee</caption>
                    <tr>
                        <td style="padding: 5px; width: 20%">
                        </td>
                        <td style="width: 15%">
                            <asp:DropDownList ID="SearchKey" runat="server" class="tb10" Visible="False">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <input type="text" id="hide" style="display: none;" /><asp:Button ID="btnSearch"
                                runat="server" Text="Cari Nama" Font-Bold="False" ForeColor="#0033CC" OnClientClick="cekArgumen()"
                                Height="26px" Width="90px" Visible="False" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Repeater ID="rptPages" runat="server">
                                <HeaderTemplate>
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="vertical-align: top">
                                                <b>Page:</b>&nbsp;
                                            </td>
                                            <td>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                        CssClass="text" runat="server" Visible='<%# (CInt(Container.DataItem) <> (PageNumber+1)).ToString() %>'>
                         <%#Container.DataItem%> 
                                    </asp:LinkButton>
                                    <asp:Label ID="lblPage" runat="server" Visible='<%# (CInt(Container.DataItem) = (PageNumber+1)).ToString()%>'
                                        Text="<%#Container.DataItem%>" ForeColor="black" Font-Bold="true">
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </td> </tr> </td> </table>
                                </FooterTemplate>
                            </asp:Repeater>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Repeater ID="rptResult" runat="server">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table-inside">
                                        <tr>
                                            <th>
                                                No.
                                            </th>
                                            <th>
                                                Trans Id
                                            </th>
                                            <th>
                                                Expired Date
                                            </th>
                                            <th>
                                                Total Leave
                                            </th>
                                            <th>
                                                Total Usage
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <%# Container.ItemIndex + 1 %>
                                        </td>
                                        <td>
                                            <a href="#" onclick="UseSelected('<%#DataBinder.Eval(Container.DataItem, "reference")%>', '<%#DataBinder.Eval(Container.DataItem, "expiredDate")%>', '<%#DataBinder.Eval(Container.DataItem, "totBalance")%>');">
                                                <%#DataBinder.Eval(Container.DataItem, "reference")%></a>
                                        </td>
                                        <td>
                                            <%#DataBinder.Eval(Container.DataItem, "expiredDate")%>
                                        </td>
                                        <td>
                                            <%#DataBinder.Eval(Container.DataItem, "totleave")%>
                                        </td>
                                        <td>
                                            <%#DataBinder.Eval(Container.DataItem, "totUsage")%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="progress1" runat="server" DisplayAfter="50">
        <ProgressTemplate>
            <asp:Panel ID="pnlProgressBar" runat="server">
                <div>
                    <asp:Image ID="Image1" runat="Server" ImageUrl="images/ajax-loader.gif" />Please
                    Wait...</div>
            </asp:Panel>
        </ProgressTemplate>
    </asp:UpdateProgress>
    </form>
</body>
</html>

