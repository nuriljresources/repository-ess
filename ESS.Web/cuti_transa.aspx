<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="cuti_transa.aspx.vb" Inherits="EXCELLENT.cuti_transa" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    cuti_transa
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script src="Scripts/calendar_us.js" type="text/javascript"></script>
<script src="Scripts/jsm01.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/calendar-c.css"/> 

<script type="text/javascript">

</script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
      <caption style="text-align: center; font-size: 1.5em">Leave</caption>
      <tr style="width:300px;">
         <td style="width:200px;">Nomor Bukti</td>
         <td style="width:70px;">
         <input type="text" id="transid" /></td><td style="width :119px;"></td>
         <td><input style="width:58px;" type="text" id="kdsite" /></td>
      </tr>
      <tr>
         <td>Nik*</td>
         <td colspan ="3">
            <input type="text" id="nik"/><img alt="add" id="Img1" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopupfield('nik')" /><input style="width:159px;" type="text" id="nik_1"/>
          </td>
      </tr>
     <tr style="width:200px;">
      <td style="width:200px;">Departement*</td>
      <td colspan="2"><input type="text" id="department"/></td>
      </tr>
       <tr><td>Position*</td>
      <td colspan="2"><input type="text" id="position"/></td>
      </tr>
      <tr><td>Type Leave*</td>
      <td colspan="3">
            <input type="text" id="TypeLeave"/><img alt="add" id="Img2" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopupfield('bidang')" /><input style="width:159px;" type="text" id="Text2"/>
      </td>
      </tr>
      <tr><td>Date Request*</td>
      <td colspan ="3">
            <asp:TextBox ID="requestdt" runat="Server" width="100px" ReadOnly="true" ></asp:TextBox> 
                  <script type="text/javascript">
                          new tcal({
                              // form name
                              'formname': 'aspnetForm',
                              // input name
                              'controlname': 'ctl00$ContentPlaceHolder1$requestdt'
                          });
                  </script> 
                  Please Re-Calculate date leave changed
      </td>
      </tr>
            <tr><td>Leave From*</td>
            <td colspan = "3">
            <asp:TextBox ID="Leavefromdt" runat="Server" width="100px" ReadOnly="true" ></asp:TextBox>
                  <script type="text/javascript">
                      new tcal({
                          // form name
                          'formname': 'aspnetForm',
                          // input name
                          'controlname': 'ctl00$ContentPlaceHolder1$Leavefromdt'
                      });
                  </script>
            To <asp:TextBox ID="Leavetodt" runat="Server" width="100px" ReadOnly="true" ></asp:TextBox>
                 <script type="text/javascript">
                                  new tcal({
                                      // form name
                                      'formname': 'aspnetForm',
                                      // input name
                                      'controlname': 'ctl00$ContentPlaceHolder1$Leavetodt'
                                  });
                 </script>
            &nbsp;&nbsp;Holiday&nbsp;<input type="text" id="Text6"/> Days
      </td>
      </tr>  
      <tr><td>Remaining Leave</td>
      <td colspan="3">
            <input type="text" id="remainleave"/>
      Days <input type="button" value="Calculate" onclick="save()" style="width: 80px; font-family: Calibri;" /> 
      Total Leave <input style="width:135px" type="text" id="Text8"/> Days</td>
      </tr>
      <tr><td>Leave Eligible</td>
      <td colspan = "3">
            <input type="text" id="leaveeligible"/> Days Cut of Absent without Permission <input type="text" id="Text10"/>Days
      </td>
      </tr>  
      <tr><td>Remarks</td>
      <td colspan ="3">
            <asp:TextBox ID="remarks" runat="server" Height="60px" Width="50%" 
             TextMode="MultiLine" class="tb10"></asp:TextBox>
      </td>
      </tr>
      <tr>  
       <td colspan="3">
        <div id="Div1" style="color: red; font: Trebechuet;">During Leave, duties will be act by</div>
       </td></tr>
      <tr>
         <td>Nik*</td>
         <td colspan = "3">
            <input type="text" id="nik2"/>
            <img alt="add" id="Img3" src="images/Search.gif" align="absmiddle"style="cursor: pointer" onclick="OpenPopupfield('bidang')" />               
            <input style="width:165px;" type="text" id="pic_nik2"/></td>
      </tr>
      <tr><td>Position*</td>
      <td colspan = "3">
            <input style="width:335px;" type="text" id="position"/>
      </td>
      </tr>
      <tr>  
       <td>
        <div id="Div2" style="color: red; font: Trebechuet;">Veryfied By
        </div>   
       </td>
       <td colspan= "0"><div id="Div3" style="color: red; font: Trebechuet; margin-left:170px;">Approved By</div></td>
       </tr>      
        <tr>
         <td>Nik*</td>
         <td colspan="3">
            <input type="text" id="verivynik"/><img alt="add" id="Img4" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopupfield('bidang')" /> 
          Nik*<input style="width:159px;" type="text" id="approvenik"/></td>
      </tr>
       <tr>
         <td>Position*</td>
         <td colspan="3">
            <input type="text" id="verivy_jabat"/><img alt="add" id="Img5" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopupfield('bidang')" /> 
          Position* <input style="width:129px;" type="text" id="approvejabat"/></td>
      </tr>
      <tr>
       <td>
        <div id="Div4" style="color: red; font: Trebechuet;">Note :
        </div> 
       </td></tr>
       <tr>
            <td colspan ="3">
        <div id="Div5" style="color: red; font: Trebechuet;">Please Calculate after changing date from & date to and holiday between range date from to leave 
        </div> 
       </td></tr>
       <tr>
              <td colspan ="3">
        <div id="Div6" style="color: red; font: Trebechuet;">before saving data ( especially for Annual Leave)
        </div> 
       </td></tr>
      <!-- Tombol Save-->
      <tr>
         <td colspan="5">
            <input type="button" value="Save" onclick="save()" style="width: 80px; font-family: Calibri;" />
         </td>
         
      </tr>
      <tr>
         <td colspan="7">
            <div id="log" style="color: red; font: Trebechuet;">
            </div>
         </td>
      </tr>
   </table>
</asp:Content>

