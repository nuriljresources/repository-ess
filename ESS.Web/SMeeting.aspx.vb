﻿Imports System.Data.SqlClient

Partial Public Class SMeeting
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") = "" Or Session("permission") = "M" Then
            Response.Redirect("Login.aspx", False)
            Exit Sub
        Else
            If Not Page.IsPostBack() Then
                ddlJenis.Items.Add(New ListItem("--SEMUA MEETING--", "0"))
                If (Mid(Session("jnsmeeting"), 1, 1) = 1) Then
                    ddlJenis.Items.Add(New ListItem("Weekly Review Jobsite", "1"))
                End If
                If (Mid(Session("jnsmeeting"), 2, 1) = 1) Then
                    ddlJenis.Items.Add(New ListItem("Monthly Review Jobsite", "2"))
                End If
                If (Mid(Session("jnsmeeting"), 3, 1) = 1) Then
                    ddlJenis.Items.Add(New ListItem("Weekly Internal Functional", "3"))
                End If
                If (Mid(Session("jnsmeeting"), 4, 1) = 1) Then
                    ddlJenis.Items.Add(New ListItem("Weekly Management Meeting", "4"))
                End If
                If (Mid(Session("jnsmeeting"), 5, 1) = 1) Then
                    ddlJenis.Items.Add(New ListItem("Monthly Management Meetin", "5"))
                End If
                If (Mid(Session("jnsmeeting"), 6, 1) = 1) Then
                    ddlJenis.Items.Add(New ListItem("Others", "6"))
                End If
                TglAwal.Text = Now.ToString("dd/MM/yyyy")
                TglAkhir.Text = Now.ToString("dd/MM/yyyy")
            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        Dim dt As DateTime
        If DateTime.TryParseExact(Request(TglAwal.UniqueID), "dd/MM/yyyy", Nothing, Globalization.DateTimeStyles.None, dt) = False Then
            log.InnerHtml = "**Tanggal Awal Belum Dimasukkan / Salah"
        ElseIf DateTime.TryParseExact(Request(TglAkhir.UniqueID), "dd/MM/yyyy", Nothing, Globalization.DateTimeStyles.None, dt) = False Then
            log.InnerHtml = "**Tanggal Akhir Belum Dimasukkan / Salah"
        Else
            log.InnerHtml = ""
            SearchMeeting()
        End If
    End Sub

    Public Function FormatDate(ByVal dt As DateTime) As String
        Return dt.ToString("dd/MM/yyyy")
    End Function

    Private Sub rptSearchResult_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSearchResult.ItemCommand
        Try
            Dim str As String = e.CommandArgument.ToString()
            If e.CommandName = "Delete" Then
                If GetOtority(str) = True Then
                    Dim _adapter As New ExcellentTableAdapters.HRAPATTableAdapter
                    _adapter.DeleteRapat("2", Request.UserHostAddress, Request.UserHostName, Now, str)

                    'Jika ubah status edit di Header Rapat, ubah juga di Detail Rapat
                    DeleteDetail(str)
                    SearchMeeting()
                Else
                    log.InnerHtml = "**Anda Tidak Berhak Menghapus"
                End If
            ElseIf e.CommandName = "Edit" Then
                If GetOtority(str) = True Then
                    Response.Redirect("Meeting.aspx?id=" & str, False)
                Else
                    log.InnerHtml = "**Anda Tidak Berhak Mengedit"
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message + ex.StackTrace)
        End Try
    End Sub

    Sub SearchMeeting()
        Dim conn As String = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim SQLConn As New SqlConnection(conn)
        Try
            Dim SQLDBDataReader As SqlClient.SqlDataReader
            Dim SQLDataTable As New DataTable
            Dim SQLCmd As New SqlCommand()

            Dim sgrup As String = GroupUser(Session("NikUser"))
            'Dim ssite As String = GetJobSite(Session("NikUser"))
            Dim ssite As String = Session("jobsite")
            Dim tglaw As String = Format(DateTime.ParseExact(Request(TglAwal.UniqueID), "dd/MM/yyyy", Nothing), "MM/dd/yyyy")
            Dim tglak As String = Format(DateTime.ParseExact(Request(TglAkhir.UniqueID), "dd/MM/yyyy", Nothing), "MM/dd/yyyy")

            Dim skuery0 As String = ""
            Dim skuery1 As String = ""
            Dim skuery2 As String = ""

            Dim s As String = ""
            For i As Integer = 1 To Len(Session("jnsmeeting"))
                If i = Len(Session("jnsmeeting")) Then
                    If Mid(Session("jnsmeeting"), i, 1) = 1 Then
                        s = s + "'" + i.ToString + "'"
                    End If
                Else
                    If Mid(Session("jnsmeeting"), i, 1) = 1 Then
                        s = s + "'" + i.ToString + "',"
                    End If
                End If
            Next

            If Right(s, 1) = "," Then
                s = s.Substring(0, s.Length - 1)
            End If

            skuery0 = "SELECT idRap,NmRap,KdCabang,KdDepar,TglRap,TmpRap,NmPimpinan,NmNotulis,Notulis,(Case JenisRap When '1' then 'Weekly Review Jobsite' When '2' then 'Monthly Review Jobsite' when '3' then 'Weekly Internal Functional' when '4' then 'Weekly Management Meeting' when '5' then 'Monthly Management Meeting' when '6' then 'Others' end) as JenisRap, (Case ISNULL(Mail,0) when '0' Then 'No' Else 'Yes' end) as Mail,Abbr FROM HRAPAT LEFT JOIN MAPPDEP ON HRAPAT.KdDepar = MAPPDEP.Abbr "
            skuery1 = "WHERE (stedit='0' and TglRap between "
            skuery2 = "and stedit='0' and TglRap between "

            If s = "" Then
                s = "'0'"
            End If

            If ddlJenis.SelectedValue = "0" Then
                ' Jika Super User, MDV, BOD, bisa liat semua
                If sgrup = "S" Or sgrup = "1" Or sgrup = "3" Then
                    skuery1 = skuery1 + "'" + tglaw + "' and '" + tglak + "' and JenisRap in (" + s + ")"
                Else
                    ' Jika GM cuma bisa liat divisinya
                    If sgrup = "4" Then
                        skuery1 = skuery1 + "'" + tglaw + "' and '" + tglak + "' and (JenisRap in (" + s + ")"

                        If skuery1.IndexOf("'3',") > -1 Then
                            skuery1 = skuery1.Replace("'3',", "")
                            skuery1 = skuery1 + " OR (JenisRap = '3' AND KdDepar = '" + GetFunction(GetCodeFunction(Session("NikUser"))) + "'))"
                        ElseIf skuery1.IndexOf("and JenisRap = '3'") > -1 Then
                            skuery1 = skuery1.Replace("and JenisRap = '3'", "AND (JenisRap = '3' and KdDepar = '" + GetFunction(GetCodeFunction(Session("NikUser"))) + "' ))")
                        End If
                    Else
                        ' PM/DPM, SekPro, Sect. Head, S. Head admin
                        If sgrup = "2" Or sgrup = "6" Or sgrup = "7" Or sgrup = "A" Then
                            skuery1 = skuery1 + "'" + tglaw + "' and '" + tglak + "' and JenisRap in (" + s + ") and KdCabang = '" + ssite + "'"
                        Else
                            ' PIC PDCA bisa lihat meeting cross function
                            skuery1 = skuery1 + "'" + tglaw + "' and '" + tglak + "' and (JenisRap in (" + s + ")"

                            If skuery1.IndexOf("'3',") > -1 Then
                                skuery1 = skuery1.Replace("'3',", "")
                                skuery1 = skuery1 + " OR (JenisRap = '3' AND KdDepar = '" + GetFunction(GetCodeFunction(Session("NikUser"))) + "'))"
                            ElseIf skuery1.IndexOf("and JenisRap = '3'") > -1 Then
                                skuery1 = skuery1.Replace("and JenisRap = '3'", "AND (JenisRap = '3' and KdDepar = '" + GetFunction(GetCodeFunction(Session("NikUser"))) + "'))")
                            End If
                        End If
                    End If
                End If
            Else
                If sgrup = "S" Or sgrup = "1" Or sgrup = "3" Then
                    skuery1 = skuery1 + "'" + tglaw + "' and '" + tglak + "' and JenisRap = '" + ddlJenis.SelectedValue + "'"
                Else
                    ' Jika GM cuma bisa liat divisinya
                    If sgrup = "4" Then
                        If ddlJenis.SelectedValue = "3" Then
                            skuery1 = skuery1 + "'" + tglaw + "' and '" + tglak + "' and JenisRap = '" + ddlJenis.SelectedValue + "' and KdDepar = '" + GetFunction(GetCodeFunction(Session("NikUser"))) + "'"
                        Else
                            skuery1 = skuery1 + "'" + tglaw + "' and '" + tglak + "' and JenisRap = '" + ddlJenis.SelectedValue + "'"
                        End If
                    Else
                        ' PM/DPM, SekPro, Sect. Head, Sect. Head Admin
                        If sgrup = "2" Or sgrup = "6" Or sgrup = "7" Or sgrup = "A" Then
                            skuery1 = skuery1 + "'" + tglaw + "' and '" + tglak + "' and JenisRap = '" + ddlJenis.SelectedValue + "' and KdCabang = '" + ssite + "'"
                        Else
                            ' PIC PDCA bisa lihat meeting cross function
                            If ddlJenis.SelectedValue = "3" Then
                                skuery1 = skuery1 + "'" + tglaw + "' and '" + tglak + "' and JenisRap = '" + ddlJenis.SelectedValue + "' and KdCabang = '" + ssite + "' and KdDepar = '" + GetFunction(GetCodeFunction(Session("NikUser"))) + "'"
                            Else
                                skuery1 = skuery1 + "'" + tglaw + "' and '" + tglak + "' and JenisRap = '" + ddlJenis.SelectedValue + "'"
                            End If
                        End If
                    End If
                End If
            End If

            'SQLCmd.CommandText = skuery0 + skuery1 + ") OR (Notulis = '" + Session("NikUser") + "' and TglRap between '" + tglaw + "' and '" + tglak + "') order by TglRap Desc"
            SQLCmd.CommandText = skuery0 + skuery1 + ") OR (Notulis = '" + Session("NikUser") + "' and JenisRap = '" + ddlJenis.SelectedValue + "' and TglRap between '" + tglaw + "' and '" + tglak + "') order by TglRap Desc"
            SQLCmd.Connection = SQLConn
            SQLConn.Open()

            SQLDBDataReader = SQLCmd.ExecuteReader()
            rptSearchResult.DataSource = SQLDBDataReader
            rptSearchResult.DataBind()
            SQLDBDataReader.Close()
            SQLDBDataReader = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            If SQLConn.State = ConnectionState.Open Then
                SQLConn.Close()
            End If
            SQLConn.Dispose()
            SQLConn = Nothing
        End Try
    End Sub

    Function ListDepartemen() As String
        Dim conn As String = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim SQLConn As New SqlConnection(conn)
        Dim SQlCmd As SqlCommand = New SqlCommand()
        SQlCmd.CommandText = "SELECT Kode FROM MappDep WHERE Divisi = '" + Session("Divisi").ToString + "'"
        SQLCmd.Connection = SQLConn
        SQLConn.Open()

        Dim SQLDR As SqlDataReader = SQlCmd.ExecuteReader()

        Dim slist As String = "("
        While SQLDR.Read
            slist = slist + "'" + SQLDR("Kode").ToString + "',"
        End While
        slist = slist.Substring(0, slist.Length - 1)
        slist = slist + ")"

        SQLDR.Close()
        SQLDR = Nothing

        Return slist
    End Function

    Public ReadOnly Property STglAwal() As String
        Get
            Dim ctanggal As String = ""
            If TglAwal.Text <> "" Then
                ctanggal = Format(CDate(TglAwal.Text), "MM/dd/yyy")
            End If
            Return ctanggal
        End Get
    End Property

    Public ReadOnly Property STglAkhir() As String
        Get
            Dim ctanggal As String = ""
            If TglAwal.Text <> "" Then
                ctanggal = Format(CDate(TglAkhir.Text), "MM/dd/yyy")
            End If
            Return ctanggal
        End Get
    End Property

    Public ReadOnly Property Jenis() As String
        Get
            Return ddlJenis.SelectedValue.ToString()
        End Get
    End Property

    Sub DeleteDetail(ByVal _idRap As String)
        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "UPDATE DRAPAT SET StEdit = '2',DeletedBy = '" + Session("NikUser") + "',DeletedIn = '" + Request.UserHostAddress
            squery = squery + "',DeletedTime = '" + Format(System.DateTime.Now, "MM/dd/yyyy HH:mm:ss") + "' WHERE IdRap = '" + _idRap + "'"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            scom.ExecuteNonQuery()
            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try
    End Sub

    Function GetOtority(ByVal _idRap As String) As Boolean
        Dim lakses As Boolean = False

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT Notulis FROM HRAPAt WHERE IdRap = '" + _idRap + "' and (mail is null or mail='0')"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            While sdr.Read()
                If sdr("Notulis") = Session("NikUser") Then
                    lakses = True
                Else
                    lakses = False
                End If
            End While

            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return lakses
    End Function

    Function GetJobSite(ByVal _iduser As String) As String
        Dim slokasi As String = ""

        ''KODE JOBSITE TIDAK AMBIL LAGI DARI HRIS KARENA LEVEL SCTN HEAD UP
        ''KODE JOBSITE DIANGGAP HO - MAKA AMBIL DARI TABEL MUSER

        'KODE JOBSITE AMBIL LAGI DARI HRIS - BASED RAPAT DI RUANG 3B TGL 1/6/2010

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            'JIKA LEVEL E-Up MAKA AMBIL KODE LOKASI DARI H_A101 HRIS
            'SEDANGKAN D-Down AMBIL KODE JOBSITE DARI H_A101 HRIS

            Dim squery As String = ""
            'squery = "SELECT Nik, KdLokasi = case KdLevel when 5 Then KdLokasi when 6 Then KdLokasi When 7 Then KdLokasi else KdSite end "
            'squery = squery + "FROM H_A101 WHERE Nik = '" + _iduser + "'"

            'squery = "SELECT SITE as KdLokasi FROM MAPSITESAP a inner join OPENROWSET('SQLNCLI','SERVER=172.16.0.7\hris;UID=IICS;PWD=$IICS-123-ONLINE$', 'SELECT WERKS,BTRTL from BERP_CENTRAL.dbo.H_A101_SAP where pernr=''" & _iduser & "''') x on a.WERKS=x.WERKS and a.BTRTL=x.BTRTL"
            squery = "SELECT SITE as KdLokasi FROM MAPSITESAP a inner join OPENROWSET('SQLNCLI','SERVER=20.110.1.5\hris;UID=dev;PWD=hrishris', 'SELECT kdsite, '' as BTRTL from JRN_TEST.dbo.H_A101 where nik=''" & _iduser & "''') x on a.site=x.KDSITE"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            If sdr.HasRows Then
                While sdr.Read()
                    slokasi = IIf(IsDBNull(sdr("KdLokasi")), "NFO", sdr("KdLokasi"))
                End While
            Else
                slokasi = "NFO"
            End If

            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return slokasi
    End Function

    Function GroupUser(ByVal _iduser As String) As String
        Dim sgroup As String = "3"

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT Grup FROM MUSER WHERE IdUser = '" + _iduser + "'"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            While sdr.Read()
                sgroup = IIf(IsDBNull(sdr("Grup")), "", sdr("Grup"))
            End While

            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return sgroup
    End Function

    Function GetFunction(ByVal _kode As String) As String
        Dim sfungsi As String = _kode

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT abbr FROM mappdep WHERE NmDepar = '" + _kode + "'"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            If sdr.HasRows Then
                While sdr.Read()
                    sfungsi = IIf(IsDBNull(sdr("abbr")), "", sdr("abbr"))
                End While
            Else
                sfungsi = "UNK"
            End If
            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return sfungsi
    End Function

    Function GetCodeFunction(ByVal _iduser As String) As String
        Dim sfungsi As String = ""

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
        Try
           If _iduser.IndexOf("10000306") > -1 Or _iduser.IndexOf("10002040") > -1 Or _iduser.IndexOf("10006574") > -1 Or _iduser.IndexOf("10010546") > -1 Or _iduser.IndexOf("10011006") > -1 Or _iduser.IndexOf("10011038") > -1 Then
                sfungsi = "Human Resource & General Affair"
            ElseIf _iduser.IndexOf("10002373") > -1 Or _iduser.IndexOf("10002609") > -1 Or _iduser.IndexOf("10002779") > -1 Or _iduser.IndexOf("10009167") > -1 Or _iduser.IndexOf("10010394") > -1 Or _iduser.IndexOf("10008053") > -1 or  _iduser.IndexOf("10010983") > -1 or _iduser.IndexOf("10000973") > -1  Then
                sfungsi = "Operation"
            ElseIf _iduser.IndexOf("10007237") > -1 Or _iduser.IndexOf("10011034") > -1 Then
                sfungsi = "Plant"
            ElseIf _iduser.IndexOf("10007792") > -1 Then
                sfungsi = "Finance & Accounting"
            ElseIf _iduser.IndexOf("10010983") > -1 Then
                sfungsi = "None Function"
            ElseIf _iduser.IndexOf("10010982") > -1 Then
                sfungsi = "Engineering"
            Else
                conn.Open()

                Dim squery As String = ""
                squery = "SELECT KdDepar FROM H_A101 WHERE Nik = '" + _iduser + "'"
                'squery = "SELECT ZFUN FROM H_A101_SAP WHERE PERNR = '" + _iduser + "' OR PNALT='" + _iduser + "'"

                Dim scom As SqlCommand = New SqlCommand(squery, conn)
                Dim sdr As SqlDataReader = scom.ExecuteReader()

                While sdr.Read()
                    'sfungsi = IIf(IsDBNull(sdr("ZFUN")), "", sdr("ZFUN"))
                    sfungsi = IIf(IsDBNull(sdr("KdDepar")), "", sdr("KdDepar"))
                End While

                conn.Close()
            End If
        Catch eks As Exception
            conn.Close()
        End Try

        Return sfungsi
    End Function
   

End Class