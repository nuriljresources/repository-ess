﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="popupexcel.aspx.vb" Inherits="EXCELLENT.popupexcel" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
        <link href="css/transaction.css" rel="stylesheet" type="text/css" />    
    <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/json2.js" type="text/javascript"></script>
    <script src="scripts/transaction.js" type="text/javascript"></script> 
    <style type="text/css">
       input.print{
         border-style : none;
       }
       
       input[type=button]{
         visibility:hidden;
       }
    
     table.bsc {
	      border-width: 1px;
	      border-spacing: 0px;
	      border-style: groove;
	      border-color: #C0C0C0;
	      border-collapse: collapse;
	      background-color: white;
         }
      table.bsc thead {
	      border-width: 1px;
	      padding: 1px;
	      border-style: inset;
	      border-color: #C0C0C0;
      }
      table.bsc td {
	      border-width: 1px;
	      padding: 1px;
	      border-style: inset;
	      border-color: #C0C0C0;
      }
      .headbsctable{
         font-size:12px;
         background-color:#449A50;
         color:white;
         font-weight:bold;
      }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="header1" align="center" style="font-size:20px;font-weight:bold;" runat="server">
    </div>
    <div id="result" align="center" class="result" runat="server">
    </div>
    </form>
     <script type="text/javascript">
        fnTest();
    </script>
</body>
</html>
