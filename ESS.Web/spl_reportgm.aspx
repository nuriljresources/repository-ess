<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="spl_reportgm.aspx.vb" Inherits="EXCELLENT.spl_reportgm" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Overtime Report</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    
    <script src="Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="Scripts/jscal2.js" type ="text/javascript"></script>
    <script src="Scripts/en.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="css/win2k/win2k.css" />
</asp:Content>

    <asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <center>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
    <caption style="text-align: center; font-size: 1.5em; color:White;">Overtime Report</caption>
    
    </table>
    <asp:HiddenField ID="hkdsite" runat="server" />
    <table>
    <tr>
    <td>
        From
    </td>
    <td><asp:TextBox ID="txtdate1" runat="server"></asp:TextBox> <button id="btndt1">...</button></td>
    <td> To </td>
    <td><asp:TextBox ID="txtdate2" runat="server"></asp:TextBox> <button id="btndt2">...</button></td>
    <td><asp:Button ID="btnok" runat="server" text="OK"/></td>
    </tr>
    </table>
    <br />
    
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" Height="461px" Width="678px" ShowFindControls="False" 
                HyperlinkTarget="_blank">
            <LocalReport ReportPath="Laporan\cetak_splGM.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                        Name="SPLDataSet_H_H1101" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            SelectMethod="GetData" 
            TypeName="EXCELLENT.SPLDataSetTableAdapters.H_H1101TableAdapter" 
            DeleteMethod="Delete" OldValuesParameterFormatString="original_{0}" 
                UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_NIK" Type="String" />
                <asp:Parameter Name="Original_Jam_start" Type="DateTime" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Kdsite" Type="String" />
                <asp:Parameter Name="KdDepar" Type="String" />
                <asp:Parameter Name="Jam_end" Type="DateTime" />
                <asp:Parameter Name="Remarks" Type="String" />
                <asp:Parameter Name="Original_NIK" Type="String" />
                <asp:Parameter Name="Original_Jam_start" Type="DateTime" />
            </UpdateParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="txtdate1" Name="date1" PropertyName="Text" 
                    Type="DateTime" />
                <asp:ControlParameter ControlID="txtdate2" Name="date2" PropertyName="Text" 
                    Type="DateTime" />
                <asp:ControlParameter ControlID="hkdsite" Name="kdsite" PropertyName="Value" 
                    Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
  </center>
    
    <script type = "text/javascript" >
        var today = new Date();
        var cal = Calendar.setup({
            onSelect: function(cal) { cal.hide() },
            showTime: false
        });


        cal.manageFields("btndt1", "ctl00_ContentPlaceHolder1_txtdate1", "%m/%d/%Y");
        cal.manageFields("btndt2", "ctl00_ContentPlaceHolder1_txtdate2", "%m/%d/%Y");
 </script>
</asp:Content>