﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master"
   CodeBehind="ractivityplan.aspx.vb" Inherits="EXCELLENT.ractivityplan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
   <link href="css/jsgantt.css" rel="stylesheet" type="text/css" />

   <script src="scripts/jsgantt.js" type="text/javascript"></script>

   <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>

   <script src="../Scripts/json2.js" type="text/javascript"></script>

   <script src="scripts/floating-1.7.js" type="text/javascript"></script>

   <style type="text/css">
      body
      {
         font-family: Calibri;
      }
      #divprogress
      {
         z-index: 1000;
         position: absolute;
         top: 0;
         bottom: 0;
         left: 0;
         width: 100%;
         background: #000;
         opacity: 0.55;
         -moz-opacity: 0.55;
         filter: alpha(opacity=55);
         visibility: hidden;
      }
      td.styleHeader
      {
         background-color: #00CC00;
         color: black;
         font-weight: bold;
         font-size: medium;
      }
   </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div id="divprogress">
      <center>
         <img id="imgloading" alt="" src="/remo/images/tloader.gif" style="margin-top: 100px" /></center>
   </div>
   <asp:HiddenField ID="Grup" runat="server" />
   <asp:HiddenField ID="Jobsite" runat="server" />
   <asp:HiddenField ID="Divisi" runat="server" />
   <asp:HiddenField ID="Section" runat="server" />
   <asp:HiddenField ID="userid" runat="server" />
   <asp:HiddenField ID="userip" runat="server" />
   <asp:HiddenField ID="hid" runat="server" />
   <asp:HiddenField ID="myhtml" runat="server" />
   <table style="width: 100%">
      <tr>
         <td colspan="2" class="styleHeader">
            Review Activity Plan
         </td>
      </tr>
      <tr>
         <td valign="top">
            Tahun
         </td>
         <td valign="top">
            <asp:DropDownList ID="ddlYear" runat="server">
               <asp:ListItem>2010</asp:ListItem>
               <asp:ListItem>2011</asp:ListItem>
               <asp:ListItem>2012</asp:ListItem>
            </asp:DropDownList>
         </td>
      </tr>
      <tr>
         <td style="vertical-align: top">
            Level
         </td>
         <td>
            <asp:DropDownList ID="KPILevel" runat="server" AutoPostBack="true" CssClass="plains">
               <asp:ListItem Value="0" Text="BUMA"></asp:ListItem>
               <asp:ListItem Value="1" Text="Function"></asp:ListItem>
               <asp:ListItem Value="2" Text="Section"></asp:ListItem>
               <asp:ListItem Value="3" Text="Jobsite"></asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="KPIFunc" runat="server" Visible="False" CssClass="plains">
            </asp:DropDownList>
            <asp:DropDownList ID="KPISection" runat="server" Visible="False" CssClass="plains">
            </asp:DropDownList>
            <asp:DropDownList ID="KPIJobsite" runat="server" Visible="False" CssClass="plains">
            </asp:DropDownList>
            <br />
            <input id="Radio1" type="radio" name="opsi" value="0" checked />Sesuai Function
            <input id="Radio2" type="radio" name="opsi" value="1" />Sesuai Function dan include
            cross function
            <br />
            <input type="button" value="View" onclick="retrieve();" />
         </td>
      </tr>
   </table>
   <div style="position: relative" class="gantt" id="GanttChartDIV">
   </div>
   <%--<asp:Button runat="server" Text="PRINT" ID="export" CausesValidation="false" OnClientClick="document.getElementById('ctl00_ContentPlaceHolder1_myhtml').value=escape(document.getElementById('GanttChartDIV').innerHTML);" />--%>

   <script type="text/javascript">
      var g = new JSGantt.GanttChart('g', document.getElementById('GanttChartDIV'), 'day');
      g.setShowRes(1); // Show/Hide Responsible (0/1)
      g.setShowDur(0); // Show/Hide Duration (0/1)
      g.setShowComp(0); // Show/Hide % Complete(0/1)
      g.setCaptionType('Resource');  // Set to Show Caption

      function retrieve() {

         var selectedRadio = '';
         var myurl = '';
         var radio = document.getElementsByName('opsi');
         for (var j = 0; j < radio.length; j++) {
            if (radio[j].checked)
               selectedRadio = radio[j].value;
         }

         document.getElementById("divprogress").style.visibility = 'visible';
         var funcid = '0';
         var param = '';
         var noproject = 0;
         var mySArray = new Array("A.", "B.", "C.", "D.", "E.", "F.", "G.", "H.", "I.", "J.", "K.", "L.", "M.", "N.", "O.", "P.", "Q.", "R.", "S.", "T.", "U.", "V.", "W.", "X.", "Y.", "Z.");
         switch (document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').options[document.getElementById('ctl00_ContentPlaceHolder1_KPILevel').selectedIndex].value) {
            case '1': //FUNCTION
               {
                  param = '1#' + document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').selectedIndex].value;
                  break;
               }
            case '2': //SECTION
               {
                  param = '2#' + document.getElementById('ctl00_ContentPlaceHolder1_KPISection').options[document.getElementById('ctl00_ContentPlaceHolder1_KPISection').selectedIndex].value + '#' + +document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').selectedIndex].value;
                  break;
               }
         }

         if (selectedRadio == "0")
            myurl = "WS/BSCService.asmx/GetGantt";
         else
            myurl = "WS/BSCService.asmx/GetGanttFull";


         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: myurl,
            data: "{'param':" + JSON.stringify(param) + ",'nik':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_userid").value) + ",'year':" + document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').selectedIndex].value + "}",
            dataType: "json",
            success: function(res) {
               //document.getElementById("resultchild").innerHTML = res.d;
               if (res.d.length > 0) {
                  g = new JSGantt.GanttChart('g', document.getElementById('GanttChartDIV'), 'day');
                  g.setShowRes(1); // Show/Hide Responsible (0/1)
                  g.setShowDur(0); // Show/Hide Duration (0/1)
                  g.setShowComp(0); // Show/Hide % Complete(0/1)
                  g.setShowStartDate(0);
                  g.setShowEndDate(0);
                  g.setCaptionType('Resource');  // Set to Show Caption
                  if (g) {
                     var j = 1;
                     var no = 0;
                     var subno = 0;
                     for (var i = 0; i < res.d.length; i++) {
                        if (res.d[i].Jenis == 'PROJECT') {
                           if (parseInt(noproject) > 25) {
                              g.AddTaskItem(new JSGantt.TaskItem(noproject + 1, '<b>' + res.d[i].Activity + '<b>', res.d[i].Ptglstart, res.d[i].Ptglend, 'FF8356', 'popupgantt.aspx?id=' + res.d[i].No, 0, res.d[i].Pic, 0, 0, 0, 1, '', '', (res.d[i].Jenis == 'TASK') ? '' : res.d[i].Kpi, '', 'P', res.d[i].Atglstart, res.d[i].Atglend, res.d[i].Deliverables, '<b>' + mySArray[parseInt((parseInt(noproject) / parseInt(26)) - parseInt(1))] + mySArray[parseInt(noproject) % 26] + '</b>', unescape(res.d[i].Logprogress)));
                           } else {
                              g.AddTaskItem(new JSGantt.TaskItem(noproject + 1, '<b>' + res.d[i].Activity + '<b>', res.d[i].Ptglstart, res.d[i].Ptglend, 'FF8356', 'popupgantt.aspx?id=' + res.d[i].No, 0, res.d[i].Pic, 0, 0, 0, 1, '', '', (res.d[i].Jenis == 'TASK') ? '' : res.d[i].Kpi, '', 'P', res.d[i].Atglstart, res.d[i].Atglend, res.d[i].Deliverables, '<b>' + mySArray[parseInt(noproject) % 26] + '</b>', unescape(res.d[i].Logprogress)));
                           }
                           no = 0; subno = 0; noproject += 1;
                        } else if (res.d[i].Jenis == 'PROGRAM') {
                           no += 1;
                           subno = 0;
                           g.AddTaskItem(new JSGantt.TaskItem(noproject + "." + j, res.d[i].Activity, res.d[i].Ptglstart, res.d[i].Ptglend, 'FF8356', 'popupgantt.aspx?id=' + res.d[i].No, 0, res.d[i].Pic, 0, 0, 0, 1, '', '', (res.d[i].Jenis == 'TASK') ? '' : res.d[i].Kpi, res.d[i].Status, 'P', res.d[i].Atglstart, res.d[i].Atglend, res.d[i].Deliverables, no + '.', unescape(res.d[i].Logprogress)));
                        } else {
                           subno += 1;
                           g.AddTaskItem(new JSGantt.TaskItem(noproject + "." + j, res.d[i].Activity, res.d[i].Ptglstart, res.d[i].Ptglend, 'FF8356', 'popupgantt.aspx?id=' + res.d[i].No, 0, res.d[i].Pic, 0, 0, 0, 1, '', '', (res.d[i].Jenis == 'TASK') ? '' : res.d[i].Kpi, res.d[i].Status, 'P', res.d[i].Atglstart, res.d[i].Atglend, res.d[i].Deliverables, no + '.' + subno + '. ', unescape(res.d[i].Logprogress)));
                        }
                        //g.AddTaskItem(new JSGantt.TaskItem(j, res.d[i].Activity, res.d[i].Ptglstart, res.d[i].Ptglend, 'FF8356', 'popupgantt.aspx?id=' + res.d[i].No, 0, res.d[i].Pic, 0, 0, 0, 1, '', '', (res.d[i].Jenis == 'TASK') ? '' : res.d[i].Kpi, res.d[i].Status, 'P', res.d[i].Atglstart, res.d[i].Atglend, res.d[i].Deliverables, (res.d[i].Jenis == 'TASK') ? (no + '.' + subno + '. ') : no + '. ', unescape(res.d[i].Logprogress)));
                        j += 1;
                        //                        if (res.d[i].Atglstart != '' && res.d[i].Atglend != '') {
                        //                           g.AddTaskItem(new JSGantt.TaskItem(j, '', res.d[i].Atglstart, res.d[i].Atglend, '0094FF', '', 0, res.d[i].Pic, 0, 0, 0, 1, '', '', (res.d[i].Jenis == 'TASK') ? '' : res.d[i].Kpi, res.d[i].Status, 'A'));
                        //                           j += 1;
                        //                        }

                     }
                     g.Draw();
                     g.DrawDependencies();
                     document.getElementById("divprogress").style.visibility = 'hidden';
                  }
               }
               else {
                  document.getElementById("GanttChartDIV").innerHTML = "";
                  document.getElementById("divprogress").style.visibility = 'hidden';
               }

            },
            error: function(err) {
               alert("error: " + err.responseText);
            }
         });
      }
      retrieve();

      function hide(id) {
         var rows = document.getElementById('LEFTTAB').getElementsByTagName('tbody')[0].getElementsByTagName('tr');
         for (i = 0; i < rows.length; i++) {
            if (rows[i].id.split(".")[0] == id && rows[i].id.split(".").length == 2) {
               if (rows[i].style.display == "none")
                  rows[i].style.display = "";
               else
                  rows[i].style.display = "none";
            }
         }

         rows = document.getElementById('rightside').getElementsByTagName('div');
         var y = '';
         for (i = 0; i < rows.length; i++) {
            y += rows[i].id;
            if ((rows[i].id.split(".")[0] == ('childgrid_' + id.split("_")[1])) && rows[i].id.split(".").length == 2) {
               if (rows[i].style.display == "none")
                  rows[i].style.display = "";
               else
                  rows[i].style.display = "none";
            }
         }
      }
   </script>

</asp:Content>
