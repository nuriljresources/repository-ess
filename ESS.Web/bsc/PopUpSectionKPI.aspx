﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PopUpSectionKPI.aspx.vb" Inherits="EXCELLENT.PopUpSectionKPI" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Popup</title>
    <script type="text/javascript">
       function executeClose(id) {
          window.returnValue = id;
          window.close();
       }    
    </script>
    <style type="text/css">
      body{
       font-family: arial;
       font-size:14px;
      }
      
      .plain
      {
      border: 1px solid #000000;
      }
      
      .grid{
          cursor:hand;
          border: 1px solid #C0C0C0;
      }
      
       .grid2 td{
          border: 1px solid #C0C0C0;
      }
      
      .gridHeader th{
          border: 1px solid #C0C0C0;
          font-weight:bold;
          background:#00CC00;
      }
    </style>
</head>
<body>

    <form id="form1" runat="server">
    <div>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
       <table>
         <tr>
            <td colspan="3"><asp:Label ID="lblKet" runat="server" Text=""></asp:Label></td>
         </tr>
         <tr>
            <td>Nama KPI</td>
            <td><asp:TextBox ID="srchTextBox" runat="server"></asp:TextBox></td>
            <td><asp:Button ID="btnSearch" runat="server" Text="Search" /></td>
         </tr>
         <tr>
            <td colspan="3">
               <asp:GridView ID="srchResult" runat="server" AllowPaging="True" Width="100%" CssClass="grid">
                  <PagerSettings Position="Top"/>
                  <RowStyle CssClass="grid2"/>
                  <PagerStyle BorderStyle="None" />
                  <HeaderStyle CssClass="gridHeader" BackColor="#00CC00" />
                  <AlternatingRowStyle BackColor="silver" />
               </asp:GridView> 
            </td>
         </tr>
    </table>
    </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
