﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PopUpAddKPI.aspx.vb" Inherits="EXCELLENT.PopUpAddKPI" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Popup</title>
    <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/json2.js" type="text/javascript"></script>
    <script type="text/javascript">
       function recount(id) {
          $.ajax({
             type: "POST",
             contentType: "application/json; charset=utf-8",
             url: "WS/BSCService.asmx/RecountKPI",
             data: "{'kpiid':" + JSON.stringify(id) + "}",
             dataType: "json",
             success: function(res) {
             },
             error: function(err) {
                alert("error: " + err.responseText);
             }
          });
       }
       function executeClose(id) {
          window.returnValue = id;
          window.close();
       }    
    </script>
    <style type="text/css">
      body{
         font-family:"trebuchet MS", verdana, sans-serif;
	      font-size:12px;
      }
      
      .plain
      {
      border: 1px solid #000000;
      }
      
      .grid{
          cursor:hand;
          border: 1px solid #C0C0C0;
      }
      
       .grid2 td{
          border: 1px solid #C0C0C0;
      }
      
      .gridHeader th{
          border: 1px solid #C0C0C0;
          font-weight:bold;
          background:#00CC00;
      }
      input.btn{
        color:#050;
        font: bold 90% 'trebuchet ms',helvetica,sans-serif;
        background-color:#fed;
        border: 1px solid;
        border-color: #696 #363 #363 #696;
        filter:progid:DXImageTransform.Microsoft.Gradient
        (GradientType=0,StartColorStr='#ffffffff',EndColorStr='#ffeeddaa');
        padding:0 2px 0 2px;
        cursor:hand;
       }
    </style>
</head>
<body>

    <form id="form1" runat="server">
    <div>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
       <table style="width:100%">
         <tr>
            <td colspan="3"><asp:Label ID="lblKet" runat="server" Text=""></asp:Label></td>
         </tr>
         <tr>
            <td>Nama KPI</td>
            <td><asp:TextBox ID="srchTextBox" runat="server"></asp:TextBox>&nbsp;&nbsp;<asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn"/></td>
            <td></td>
         </tr>
         <tr>
            <td colspan="3">
               <asp:GridView ID="srchResult" runat="server" AllowPaging="True" Width="100%" CssClass="grid" enableviewstate="True" DataKeyNames="ID">
                  <Columns>
                  <asp:TemplateField>
                  <ItemTemplate>
                   <asp:CheckBox runat="server" ID="cbItem" />
                  </ItemTemplate>
                 </asp:TemplateField> 
                  </Columns>
                  <PagerSettings Position="Top"/>
                  <RowStyle CssClass="grid2"/>
                  <PagerStyle BorderStyle="None" />
                  <HeaderStyle CssClass="gridHeader" BackColor="#00CC00" />
                  <AlternatingRowStyle BackColor="silver" />
               </asp:GridView> 
            </td>
         </tr>
    </table>
    <asp:button id="btnOk" runat="Server" Text="Add as child" />
    </ContentTemplate>    
    </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
