﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="printkpi.aspx.vb" Inherits="EXCELLENT.printkpi" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
      body{
         font-weight:bold;
         font-size:11px;
      }
      table {
	      border-width: 1px;
	      border-spacing: 0px;
	      border-color: #C0C0C0;
	      border-collapse: collapse;
	      background-color: white;
         }
      table thead {
	      border-width: 1px;
	      padding: 1px;
	      border-color: #C0C0C0;
      }
      table td {
	      border-width: 1px;
	      padding: 1px;
	      border-color: #C0C0C0;
      }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
       <table style="width:100%" border="1">
         <tr><td align="center" style="background-color:black;color:white">Code</td><td rowspan="4" align="center" style="width:75%;font-size:22px">MEASUREMENT FORM</td></tr>
         <tr><td><br /><br /></td></tr>
         <tr><td align="center" style="background-color:black;color:white">KPI Level</td></tr>
         <tr><td align="center"><asp:Label ID="KPILevel" runat="server" Text=""></asp:Label></td></tr>
       </table>
       <table style="width:100%" border="1">
         <tr><td style="background-color:black;color:white" colspan="4">KPI Definition</td></tr>
         <tr>
            <td style="width:20%;background-color:#D9D9D9">Name Of KPI</td>
            <td style="width:45%"><asp:Label ID="KPIName" runat="server" Text=""></asp:Label></td>
            <td style="width:15%;background-color:#D9D9D9">Unit Of Measure</td>
            <td style="width:20%"><asp:Label ID="KPIUom" runat="server" Text=""></asp:Label></td>
         </tr>
         <tr>
            <td style="width:20%;background-color:#D9D9D9">Name Of KPI Owner</td>
            <td style="width:45%"><asp:Label ID="KPIOwner" runat="server" Text=""></asp:Label></td>
            <td style="width:15%;background-color:#D9D9D9">Type Of KPI</td>
            <td style="width:20%">Lag <input type="text" id="KPILag" runat="server" style="width:10px;border:solid 1px black"/>&nbsp;Lead <input type="text" id="KPILead" runat="server" style="width:10px;border:solid 1px black"/></td>           
         </tr>
         <tr>
            <td style="width:20%;background-color:#D9D9D9">Description</td><td colspan="3"><asp:Label ID="KPIDesc" runat="server" Text=""></asp:Label></td>
         </tr>
         <tr>
            <td style="width:20%;background-color:#D9D9D9">Period of Measurement</td>
            <td colspan="3">
            Monthly <input type="text" id="Monthly" runat="server" style="width:10px;border:solid 1px black"/>&nbsp;
            Quarterly <input type="text" id="Quarterly" runat="server" style="width:10px;border:solid 1px black"/>&nbsp;
            Semester <input type="text" id="Semester" runat="server" style="width:10px;border:solid 1px black"/>&nbsp;
            Yearly <input type="text" id="Yearly" runat="server" style="width:10px;border:solid 1px black"/>&nbsp;
            </td>
         </tr>
         <tr>
            <td style="width:20%;background-color:#D9D9D9">KPI Leads To</td><td colspan="3"><asp:Label ID="KPILeads" runat="server" Text=""/></td>
         </tr>
         <tr>
            <td style="width:20%;background-color:#D9D9D9" valign="top">KPI Supported By (Child KPI)</td><td colspan="3" valign="top"><div id="childkpi" runat="server"></div></td>
         </tr>
       </table>
       <table style="width:100%" border="1">
         <tr><td style="background-color:black;color:white" colspan="2">KPI Formulation</td></tr>
         <tr><td style="width:20%;background-color:#D9D9D9">Formula</td><td><asp:Label ID="KPIFormulation" runat="server" Text=""></asp:Label></td></tr>
         <tr>
         <td colspan="2">
            <div id="omg" runat="server">
            </div>
            <%--<table style="width:100%" border="1">
               <tr><td style="background-color:#D9D9D9">Name Of Data</td><td style="background-color:#D9D9D9">Source Data (Form No.)</td><td style="background-color:#D9D9D9">Data Responsibility</td><td style="background-color:#D9D9D9">Data Prepared By</td><td style="background-color:#D9D9D9">Cut Of Date</td></tr>
               <tr><td><br /></td><td></td><td></td><td></td><td></td></tr>
               <tr><td><br /></td><td></td><td></td><td></td><td></td></tr>
               <tr><td><br /></td><td></td><td></td><td></td><td></td></tr>
            </table>--%>
         </td>
         </tr>
       </table>
       <table style="width:100%" border="1">
         <tr><td style="background-color:black;color:white" colspan="2">KPI Achievement Monitoring</td></tr>
         <tr><td style="width:55%;background-color:#D9D9D9">Process To Setting Target</td><td style="width:45%;background-color:#D9D9D9">Targetting Behaviour</td></tr>
         <tr><td style="width:55%" rowspan="2" valign="top">
         <asp:Label ID="KPIProcess" runat="server" Text=""/>
         </td><td style="width:45%;font-weight:normal"><b>Flat</b> <input type="text" runat="server" id="kpiflat" style="width:10px;border:solid 1px black"/> &nbsp; tetap/dibagi rata dalam setahun</td></tr>
         <tr><td style="width:45%;font-weight:normal"><b>Moving target</b> <input type="text" runat="server" id="kpimoving" style="width:10px;border:solid 1px black"/> &nbsp; fluktuatif/variatif tergantung kebutuhan</td></tr>
       </table>
       <br />
       <table style="width:100%" border="1">
         <tr><td style="width:20%;background-color:#F2F2F2">Target Setting</td></tr>
         <tr><td style="background-color:#F2F2F2">UoM</td><td><asp:Label ID="KPIUoM1" runat="server" Text=""></asp:Label></td></tr>
         <tr><td style="background-color:#D9D9D9">Period</td><td style="background-color:#D9D9D9">Past Year Actual</td><td style="background-color:#D9D9D9">Current Year Target</td><td style="background-color:#D9D9D9">Year 2</td><td style="background-color:#D9D9D9">Year 3</td><td>&nbsp;</td><td colspan="2" style="background-color:#D9D9D9">Flag Achievement Status</td></tr>
         <tr><td rowspan="3" style="background-color:#F2F2F2">Full Year</td><td rowspan="3"></td><td rowspan="3"><asp:Label ID="KPICYTarget" runat="server" Text=""></asp:Label></td><td rowspan="3"></td><td rowspan="3"></td><td rowspan="3">&nbsp;</td><td style="width:5%;background-color:red">Red</td><td align="center" style="background-color:red">90 %</td></tr>
         <tr style="background-color:yellow"><td >Yellow</td><td align="center">Between</td></tr>
         <tr style="background-color:green"><td>Green</td><td align="center">95 %</td></tr>
       </table>
       <br />
       <table style="width:100%" border="1">
         <tr><td>Date:</td><td>Date:</td><td>Date:</td></tr>
         <tr>
            <td align="center" style="width:33%;background-color:#D9D9D9">Prepared by : <br /> &lt;PaDCA&gt;</td>
            <td align="center" style="width:33%;background-color:#D9D9D9">Acknowledged by : <br /> &lt;PIC KPI&gt;</td>
            <td align="center" style="width:33%;background-color:#D9D9D9">Approved by : <br /> &lt;Direct Report of PIC KPI&gt;</td>
         </tr>
         <tr>
            <td align="center" style="width:33%"><br /><br /><br /></td>
            <td align="center" style="width:33%"><br /><br /><br /></td>
            <td align="center" style="width:33%"><br /><br /><br /></td>
         </tr>
         <tr>
            <td align="center" style="width:33%"></td>
            <td align="center" style="width:33%"></td>
            <td align="center" style="width:33%"></td>
         </tr>
       </table>
    </div>
    </form>
</body>
</html>
