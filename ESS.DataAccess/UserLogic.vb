﻿Imports ESS.DataAccess
Imports System.Linq
Imports ESS.DataAccess.ESSData
Imports ESS.DataAccess.ESSDataTableAdapters



Public Class UserLogic

    Public Shared Function GetUserByNIK(ByVal nik As String) As EmployeeModel
        Dim model As EmployeeModel = New EmployeeModel()

        Dim EmployeeAdapter As EmployeeTableAdapter = New EmployeeTableAdapter
        Dim Employee As EmployeeDataTable = EmployeeAdapter.GetEmployeeByNik(nik)
        If Employee.Count > 0 Then
            model.Nik = Employee(0).Nik
            model.NikSite = Employee(0).NikSite
            model.Nama = Employee(0).Nama
            model.Company = Employee(0).Company
            model.Jabatan = Employee(0).Jabatan
            model.Divisi = Employee(0).Divisi
            model.Site = Employee(0).Site
            model.KodeSite = Employee(0).KodeSite
        End If

        Return model
    End Function

    Public Shared Function GetUserByNIKSite(ByVal nik As String) As EmployeeModel
        Dim model As EmployeeModel = New EmployeeModel()

        Dim EmployeeAdapter As EmployeeTableAdapter = New EmployeeTableAdapter
        Dim Employee As EmployeeDataTable = EmployeeAdapter.GetEmployeeByNikSite(nik)
        If Employee.Count > 0 Then
            model.Nik = Employee(0).Nik
            model.NikSite = Employee(0).NikSite
            model.Nama = Employee(0).Nama
            model.Company = Employee(0).Company
            model.Jabatan = Employee(0).Jabatan
            model.Divisi = Employee(0).Divisi
            model.Site = Employee(0).Site
            model.KodeSite = Employee(0).KodeSite
        End If

        Return model
    End Function

End Class
