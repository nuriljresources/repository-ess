﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="PrintNotulen.aspx.vb" Inherits="EXCELLENT.PrintNotulen" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        ul#simple-menu li a.m1
        {        	
	        color:#fff;background:#4A6867;
        }
        ul#simple-menu li a.m2
        {
	        color:#2E4560;background:#fff;
        }
        ul#simple-menu li a.m3
        {        	
	        color:#fff;background:#4A6867;
        }
        ul#simple-menu li a.m4
        {        	
	        color:#fff;background:#4A6867;
        }
        ul#simple-menu li a.m5
        {
            color:#fff;background:#4A6867;
        }
        #divcetak
        {
            height: 676px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="divhead">
    <table  width="100%" class="data-table-1" cellpadding="0" cellspacing="0" >
        <caption> PRINT MINUTES OF MEETING</caption>
    </table>
    </div>
    
    <div id="divcetak" style="border: 1px solid #000000"><center>
        
        <asp:SqlDataSource ID="SqlDataSourceDR" runat="server" 
            ConnectionString="<%$ ConnectionStrings:EXCELLENTConnectionString %>" 
            
            SelectCommand="SELECT [NmIssue], [NoAction], [Action], [NmPIC], [DueDate], [StIssue] FROM [DRAPAT]">
        </asp:SqlDataSource>
        
        <asp:SqlDataSource ID="SqlDataSourceHR" runat="server" 
            ConnectionString="<%$ ConnectionStrings:EXCELLENTConnectionString %>" 
            
            SelectCommand="SELECT [IdRap], [NmRap], [TmpRap], [WkRap], [SdWkRap], [TglRap], [Agenda], [Pimpinan], [NmPimpinan], [Notulis], [NmNotulis], [JenisRap] FROM [HRAPAT]">
        </asp:SqlDataSource>
        
        <asp:SqlDataSource ID="SqlDataSourceDP" runat="server" 
            ConnectionString="<%$ ConnectionStrings:EXCELLENTConnectionString %>" 
            
            SelectCommand="SELECT [IdRap], [NikMember], [NmMember], [Departemen], [StHadir] FROM [DPESERTA]">
        </asp:SqlDataSource>
        
        <asp:SqlDataSource ID="SqlDataSourceTEMP" runat="server" 
            ConnectionString="<%$ ConnectionStrings:EXCELLENTConnectionString %>" 
            
            SelectCommand="SELECT * FROM [TEMPTABLE]">
        </asp:SqlDataSource>
        <br />
        <rsweb:ReportViewer ID="ReportViewer" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" Height="563px" Width="100%" DocumentMapWidth="100%">
            <LocalReport ReportPath="Laporan\Notulen.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="SqlDataSourceDP" 
                        Name="Excellent_DPESERTA" />
                    <rsweb:ReportDataSource DataSourceId="SqlDataSourceDR" 
                        Name="Excellent_DRAPAT" />
                    <rsweb:ReportDataSource DataSourceId="SqlDataSourceHR" 
                        Name="Excellent_HRAPAT" />
                    <rsweb:ReportDataSource DataSourceId="SqlDataSourceTEMP" 
                        Name="Excellent_TEMPTABLE" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        </center>
    </div>
</asp:Content>
