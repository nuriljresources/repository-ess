﻿Imports System.Data.SqlClient
Imports ESS.Common
Imports System.IO

Partial Public Class emp_permit
    Inherits System.Web.UI.Page

    Partial Public Class PermitData
        Public TransID As String
        Public Site As String
        Public EmployeeNIK As String
        Public EmployeeNIKSITE As String
        Public EmployeeName As String
        Public TypePermit As String
        Public TypeSubPermit As String
        Public DatePermit As DateTime
        Public DatePermitTo As DateTime
        Public TotalDay As Integer
        Public TimePermit As TimeSpan
        Public Remarks As String
        Public SuhuTubuh As String
        Public StatusEdit As Integer
        Public verify_nik As String
        Public verify_jabat As String
        Public SupNikttd As String
        Public Supjabatttd As String


    End Class
    Public alertMessage As String
    Public functionBackEnd As String
    Public typePermitSelected As String
    Public typePermitSubSelected As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") = "" Or Session("permission") = Nothing Then
            Response.Redirect("Login.aspx", False)
            Exit Sub
        End If

        If Not IsPostBack Then
            BindData()
        End If
    End Sub

    Public Sub BindData()

        Dim conn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        conn2.Open()
        Dim dt As DataTable = New DataTable()

        Dim strcon2 As String = "select '' AS kode, '-- select type --' AS remark UNION ALL "
        strcon2 = strcon2 + "select kode, remark from l_a006 where kdsite != 'PEN' AND remark != 'Others'"
        Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, conn2)
        sda2.Fill(dt)

        TypePermit.DataSource = dt
        TypePermit.DataBind()
        TypePermit.DataTextField = "remark"
        TypePermit.DataValueField = "kode"
        TypePermit.DataBind()

        conn2.Close()

        Dim strTransId As String = Request.QueryString("Id")
        typePermitSelected = String.Empty

        If Not String.IsNullOrEmpty(strTransId) Then
            Dim conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
            conn.Open()
            Dim strQueryDocument As String
            strQueryDocument = <![CDATA[SELECT
                PERMIT.[transid], 
                PERMIT.[nik], 
                EMP.Nama,
                EMP.NIKSITE,
                ISNULL(PERMIT.[LType], '') AS [LType], 
                ISNULL(PERMIT.[ltypec], '') AS [ltypec], 
                PERMIT.[trans_date], 
                PERMIT.[trans_date2], 
                PERMIT.[totleave], 
                ISNULL(PERMIT.[remarks], '') AS [remarks], 
                ISNULL(PERMIT.[SuhuTubuh], '') AS [SuhuTubuh], 
                PERMIT.[fstatus], 
                PERMIT.[kdsite], 
                PERMIT.[StEdit],
                PERMIT.[CreatedBy],
                ISNULL(PERMIT.[StatusWF], '') AS [StatusWF]
                FROM [L_H006] PERMIT 
                INNER JOIN [H_A101] EMP ON PERMIT.nik = EMP.NIK
                WHERE PERMIT.transid = '{0}']]>.Value
            strQueryDocument = String.Format(strQueryDocument, strTransId)
            Dim dtRow As DataTable = New DataTable
            Dim sqlTotalAdapter As SqlDataAdapter = New SqlDataAdapter(strQueryDocument, conn)
            sqlTotalAdapter.Fill(dtRow)
            conn.Close()

            If dtRow.Rows.Count > 0 Then
                Dim Item As System.Data.DataRow = dtRow.Rows(0)

                If Item("CreatedBy") = Session("niksite") Or Item("Nik") = Session("niksite") Then

                    TransID.Text = Item("transid")
                    site.Text = Item("kdsite")
                    niksite.Text = Item("NIKSITE")
                    nik.Value = Item("nik")

                    nama.Text = Item("Nama")
                    Dim _dateTime As Date = Date.Parse(Item("trans_date"))
                    Dim _dateTimeTo As Date = Date.Parse(Item("trans_date2"))

                    txtDateTime.Text = _dateTime.ToString("MM/dd/yyyy")
                    txtDateTimeTo.Text = _dateTimeTo.ToString("MM/dd/yyyy")
                    txtTimePermit.Text = Date.Parse(Item("trans_date2")).ToString("H:mm")



                    txtTotalDays.Text = Item("totleave")
                    status.Value = Item("StEdit")
                    txtRemarks.Text = Item("remarks")
                    txtSuhuTubuh.Text = Item("SuhuTubuh")

                    typePermitSelected = Item("LType")
                    TypePermit.SelectedValue = Item("LType")
                    TypePermitSub.SelectedValue = Item("ltypec")
                    typePermitSubSelected = Item("ltypec")

                    If TypePermit.SelectedValue = "02" Or TypePermit.SelectedValue = "03" Then
                        txtTimePermit.Visible = True
                        txtTimePermit.Enabled = True
                    Else
                        txtTimePermit.Visible = False
                    End If

                    BindSubType(TypePermit.SelectedValue, typePermitSubSelected)
                    btnSubmit.Visible = True

                    If Integer.Parse(status.Value) = 2 Then
                        ButtonControl("Deleted")
                    ElseIf Integer.Parse(status.Value) = 1 Then
                        ButtonControl("Disabled")
                    Else
                        ButtonControl("Edit")
                    End If

                    If Item("StatusWF") = "INP" Or Item("StatusWF") = "COMPLT" Then
                        btnSubmit.Enabled = False
                        btnEdit.Enabled = False
                    End If

                    If status.Value <> 2 And Item("kdsite") = "JKT" And (Item("StatusWF") = "" Or Item("StatusWF") = "RVS") Then
                        btnSubmit.Enabled = True
                    End If

                    If Not String.IsNullOrEmpty(Item("StatusWF")) And Item("StatusWF") = "RVS" Then
                        btnEdit.Enabled = False
                    End If

                    FileUpload1.Visible = True
                    btnUpload.Visible = True
                    lblInfoUpload.Text = "List File"
                    GridView1.Visible = True

                    BindFileUpload()


                Else
                    alertMessage = "<script type ='text/javascript' > alert('Document " + strTransId + " User Not Authorized') </script>"
                End If

            Else
                alertMessage = "<script type ='text/javascript' > alert('Document " + strTransId + " Not Found') </script>"
            End If
        End If



    End Sub

    Public Function PopulateToObject() As PermitData
        Dim Model As PermitData = New PermitData

        Model.TransID = Request.Form(TransID.UniqueID)
        Model.Site = Request.Form(site.UniqueID)
        Model.EmployeeNIKSITE = Request.Form(niksite.UniqueID)
        Model.EmployeeNIK = Request.Form(nik.UniqueID)
        Model.EmployeeName = Request.Form(nama.UniqueID)
        Model.TypePermit = Request.Form(TypePermit.UniqueID)
        Model.TypeSubPermit = Request.Form(TypePermitSub.UniqueID)
        Model.Remarks = Request.Form(txtRemarks.UniqueID)
        Model.SuhuTubuh = Request.Form(txtSuhuTubuh.UniqueID)

        Dim datePermit As Date = DateTime.MinValue
        Dim datePermitTo As Date = DateTime.MinValue
        Dim timePermit As TimeSpan = TimeSpan.MinValue


        If String.IsNullOrEmpty(Model.Remarks) Then
            Model.Remarks = String.Empty
        End If

        Dim strDatePermit As String = Request.Form(txtDateTime.UniqueID)
        If Not String.IsNullOrEmpty(strDatePermit) Then
            DateTime.TryParse(strDatePermit, datePermit)
        End If

        Dim strDatePermitTo As String = Request.Form(Me.txtDateTimeTo.UniqueID)
        If Not String.IsNullOrEmpty(strDatePermitTo) Then
            DateTime.TryParse(strDatePermitTo, datePermitTo)
        End If

        Dim strTimePermit As String = Request.Form(txtTimePermit.UniqueID)
        If Not String.IsNullOrEmpty(strTimePermit) Then
            TimeSpan.TryParse(strTimePermit, timePermit)
        End If

        If Model.TypePermit = "02" Or Model.TypePermit = "03" Then
            Model.TotalDay = 1
        Else
            Model.TotalDay = (datePermitTo.Subtract(datePermit).TotalDays) + 1
        End If





        Model.DatePermit = datePermit
        Model.DatePermitTo = datePermitTo
        Model.TimePermit = timePermit

        Return Model
    End Function

    Public Sub BindToControl(ByVal Model As PermitData)
        TransID.Text = Model.TransID
        site.Text = Model.Site
        nik.Value = Model.EmployeeNIK
        niksite.Text = Model.EmployeeNIKSITE
        nama.Text = Model.EmployeeName

        If Not Model.DatePermit = DateTime.MinValue Then
            txtDateTime.Text = Model.DatePermit.ToString("MM/dd/yyyy")
        End If

        If Not Model.DatePermitTo = DateTime.MinValue Then
            txtDateTimeTo.Text = Model.DatePermitTo.ToString("MM/dd/yyyy")
        End If

        If Not Model.TimePermit = TimeSpan.MinValue Then
            txtTimePermit.Text = String.Format("{0}:{1}", Model.TimePermit.Hours, Model.TimePermit.Minutes)
        End If


        txtTotalDays.Text = 1
        txtRemarks.Text = Model.Remarks
        txtSuhuTubuh.Text = Model.SuhuTubuh
        typePermitSelected = Model.TypePermit
        typePermitSubSelected = Model.TypeSubPermit


    End Sub

    Public Function GetDocumentNumber() As String
        Dim DocumentNumber As String = String.Empty
        Dim StripNo As String = String.Format("{0}{1}/S/", DateTime.Now.Year, DateTime.Now.ToString("MM"))
        Dim RunningNumber As Integer = 0

        Dim conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim dt As New DataTable

        conn.Open()
        Dim strQueryTotal As String = "select MAX(right(transid,6)) from l_h006 where transid like '" + StripNo + "%'"

        Dim dtTotal As DataTable = New DataTable
        Dim sqlTotalAdapter As SqlDataAdapter = New SqlDataAdapter(strQueryTotal, conn)
        sqlTotalAdapter.Fill(dtTotal)
        conn.Close()

        If (dtTotal.Rows.Count > 0 And Not String.IsNullOrEmpty(dtTotal.Rows(0).Item(0).ToString())) Then
            RunningNumber = Integer.Parse(dtTotal.Rows(0).Item(0))
        End If

        DocumentNumber = StripNo + (RunningNumber + 1).ToString("D6")

        Return DocumentNumber
    End Function

    Public Function ValidationForm(ByVal Model As PermitData) As Boolean
        Dim flag As Boolean
        Dim str As String = String.Empty
        SetApproval(Model)

        If ((Model.TypePermit = "02") Or (Model.TypePermit = "03")) Then
            txtTimePermit.Enabled = True
        Else
            txtTimePermit.Enabled = False
        End If

        If Not String.IsNullOrEmpty(Model.EmployeeNIK) Then
            If Not Model.DatePermit = DateTime.MinValue Then
                If Not String.IsNullOrEmpty(Model.TypePermit) Then
                    If (Model.Remarks.Length > 100) Then
                        str = "Remarks input can not be longer than 100 characters"
                    ElseIf ((Model.TypePermit = "02") Or (Model.TypePermit = "03")) Then
                        If Not (Model.TimePermit = TimeSpan.MinValue) Then
                            flag = True
                        Else
                            str = "Time Permit Cannot Be Null"
                        End If
                    ElseIf Not Model.DatePermit = DateTime.MinValue Then
                        If (Model.DatePermitTo.Subtract(Model.DatePermit).Days >= 0) Then
                            flag = True
                        Else
                            str = "Date From & Date To Error"
                        End If
                    Else
                        str = "Date To Cannot Be Null"
                    End If
                Else
                    str = "Type Permit Must Be Select"
                End If
            Else
                str = "Date Cannot Be Null"
            End If
        Else
            str = "Employee NIK Cannot Be Null"
        End If
        If ((String.IsNullOrEmpty(str) And ((TypePermit.SelectedValue = "02") Or (TypePermit.SelectedValue = "03"))) AndAlso String.IsNullOrEmpty(TypePermitSub.SelectedValue)) Then
            str = "Sub Type Permit Must Be Select"
            flag = False
        End If
        If (String.IsNullOrEmpty(str) And String.IsNullOrEmpty(Model.Remarks)) Then
            str = "Remarks Cannot Be Null"
            flag = False
        End If
        If (String.IsNullOrEmpty(str) And String.IsNullOrEmpty(Model.verify_nik)) Then
            str = "HR Employee for Approval Not Found"
            flag = False
        End If
        If (String.IsNullOrEmpty(str) And String.IsNullOrEmpty(Model.SupNikttd)) Then
            str = "Manager Employee for Approval Not Found"
            flag = False
        End If
        'If (String.IsNullOrEmpty(str) And Model.TypePermit = "05" And String.IsNullOrEmpty(Model.SuhuTubuh)) Then
        '    str = "Body Temperatur cannot be null"
        '    flag = False
        'End If
        If Not String.IsNullOrEmpty(str) Then
            alertMessage = ("<script type='text/javascript'> alert('" & str & "') </script>")
        End If
        Return flag
    End Function




    Public Function ValidationForm() As Boolean
        Dim IsValid As Boolean
        Dim strMessage As String = String.Empty
        Dim Model = PopulateToObject()
        SetApproval(Model)

        If Model.TypePermit = "02" Or Model.TypePermit = "03" Then
            txtTimePermit.Enabled = True
        Else
            txtTimePermit.Enabled = False
        End If

        If Not String.IsNullOrEmpty(Model.EmployeeNIK) Then
            If Not Model.DatePermit = DateTime.MinValue Then
                If Not String.IsNullOrEmpty(Model.TypePermit) Then

                    If Model.Remarks.Length > 100 Then
                        strMessage = "Remarks input can not be longer than 100 characters"
                    Else

                        If Model.TypePermit = "02" Or Model.TypePermit = "03" Then
                            If Not Model.TimePermit = TimeSpan.MinValue Then
                                IsValid = True
                            Else
                                strMessage = "Time Permit Cannot Be Null"
                            End If
                        Else
                            IsValid = True
                        End If

                    End If

                Else
                    strMessage = "Type Permit Must Be Select"
                End If
            Else
                strMessage = "Date Cannot Be Null"
            End If
        Else
            strMessage = "Employee NIK Cannot Be Null"
        End If

        If Not String.IsNullOrEmpty(strMessage) Then
            alertMessage = "<script type='text/javascript'> alert('" + strMessage + "') </script>"
        End If

        Return IsValid
    End Function

    Public Sub SaveData()
        Dim Model = PopulateToObject()
        Dim TimePermit As DateTime = Model.DatePermit

        If ((Model.TypePermit = "02") Or (Model.TypePermit = "03")) Then
            If Not (Model.TimePermit = TimeSpan.MinValue) Then
                TimePermit = TimePermit.AddHours(CDbl(Model.TimePermit.Hours)).AddMinutes(CDbl(Model.TimePermit.Minutes))
            End If
        Else
            TimePermit = Model.DatePermitTo
        End If



        Dim sqlQuery As String

        If ValidationForm(Model) Then

            If String.IsNullOrEmpty(Model.TransID) Then
                sqlQuery = <![CDATA[INSERT INTO [dbo].[L_H006]
                ([transid], [nik], [LType], [trans_date], [trans_date2], [totleave], [remarks], [fstatus], [CreatedBy], [CreatedIn], [CreatedTime], [StEdit], [kdsite], [ltypec], [verify_nik], [verify_jabat], [SupNikttd], [Supjabatttd], [SuhuTubuh])
                VALUES 
                ('{0}', '{1}', '{2}', '{3}', '{4}', {5}, '{6}', {7}, '{8}', '{9}', '{10}', {11}, '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}')
                ]]>.Value

                Dim DocNumber As String = GetDocumentNumber()

                If Model.Remarks.Length > 100 Then
                    Model.Remarks = Model.Remarks.Substring(0, 100)
                End If

                sqlQuery = String.Format(sqlQuery, DocNumber, Model.EmployeeNIK, Model.TypePermit, Model.DatePermit.ToString(), TimePermit.ToString(), Model.TotalDay, Model.Remarks, 0, Session("niksite"), System.Net.Dns.GetHostName(), DateTime.Now.ToString(), 0, Model.Site, Model.TypeSubPermit, Model.verify_nik, Model.verify_jabat, Model.SupNikttd, Model.Supjabatttd, Model.SuhuTubuh)
                Model.TransID = DocNumber
            Else
                sqlQuery = <![CDATA[UPDATE [dbo].[L_H006]
                           SET [nik] = '{0}'
                              ,[LType] = '{1}'
                              ,[ltypec] = '{2}'
                              ,[trans_date] = '{3}'
                              ,[trans_date2] = '{4}'
                              ,[remarks] = '{5}'
                              ,[ModifiedBy] = '{6}'
                              ,[ModifiedIn] = '{7}'
                              ,[ModifiedTime] = '{8}'
                              ,[kdsite] = '{9}'
                              ,[totleave] = '{10}'
                              ,[SuhuTubuh] = '{11}'

                         WHERE [transid] = '{12}'
                ]]>.Value

                sqlQuery = String.Format(sqlQuery, Model.EmployeeNIK, Model.TypePermit, Model.TypeSubPermit, Model.DatePermit.ToString(), TimePermit.ToString(), Model.Remarks, Session("niksite"), System.Net.Dns.GetHostName(), DateTime.Now.ToString(), Model.Site, Model.TotalDay, Model.SuhuTubuh, Model.TransID)
            End If


            Dim conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
            conn.Open()
            Dim cmd As SqlCommand = New SqlCommand(sqlQuery, conn)
            cmd.ExecuteScalar()
            conn.Close()

            Dim redirectStr As String = "window.location.href = 'emp_permit.aspx?id=" + Model.TransID + "';"

            alertMessage = "<script type ='text/javascript' > alert('Document has been saved');  " + redirectStr + "</script>"
        Else
            BindToControl(Model)
        End If
    End Sub

    Public Sub ButtonControl(ByVal Status As String)

        Select Case Status
            Case "New"
                btnSave.Visible = True
                btnEdit.Visible = False
                btnPrint.Visible = False
                Exit Select
            Case "Edit"
                btnSave.Visible = False
                btnEdit.Visible = True
                btnPrint.Visible = True
                btnDelete.Visible = True
                Exit Select
            Case "Disabled"
                btnSave.Visible = False
                btnEdit.Visible = True
                btnEdit.Enabled = False
                btnPrint.Visible = True
                txtTimePermit.Enabled = False
                TypePermit.Enabled = False
                txtRemarks.Enabled = False
                Exit Select
            Case "Deleted"
                btnSave.Visible = False
                btnEdit.Visible = False
                btnPrint.Visible = False
                btnDelete.Visible = False
                txtTimePermit.Enabled = False
                TypePermit.Enabled = False
                txtRemarks.Enabled = False
                btnSubmit.Enabled = False
                btnSubmit.Visible = False
                Exit Select
        End Select


    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        SaveData()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEdit.Click
        SaveData()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrint.Click
        Dim Model As PermitData = PopulateToObject()
        Response.Redirect(String.Format("emp_permit_form.aspx?id={0}", Model.TransID))
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        Dim Model As PermitData = PopulateToObject()

        Dim conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        conn.Open()

        Dim sqlQuery As String = <![CDATA[UPDATE [dbo].[L_H006]
                           SET [StEdit] = 2
                         WHERE [transid] = '{0}'
                ]]>.Value

        sqlQuery = String.Format(sqlQuery, Model.TransID)
        Dim cmd As SqlCommand = New SqlCommand(sqlQuery, conn)
        cmd.ExecuteScalar()
        conn.Close()

        alertMessage = "<script type ='text/javascript' > alert('Document has been deleted') </script>"
        Response.Redirect("emp_permit.aspx?id=" + Model.TransID, True)
    End Sub

    Protected Sub TypePermit_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TypePermit.SelectedIndexChanged

        TypePermitSub.Items.Clear()
        TypePermitSub.Enabled = False
        TypePermitSub.Visible = False
        txtDateTime.Text = String.Empty
        txtDateTimeTo.Text = String.Empty

        Dim Model = PopulateToObject()
        BindToControl(Model)

        If Model.TypePermit = "02" Or Model.TypePermit = "03" Then
            txtTimePermit.Enabled = True
        Else
            txtTimePermit.Enabled = False
        End If

        BindSubType(Model.TypePermit, String.Empty)
    End Sub

    Public Sub BindSubType(ByVal PermitType As String, ByVal PermitTypeSub As String)
        If (PermitType = "02") Or (PermitType = "03") Then

            Dim conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
            conn.Open()

            Dim dataTable As New DataTable
            Dim str As String = "select '' AS CatCode, '-- select sub --' AS CatRemark UNION ALL "

            Dim strQueryDocument As String
            strQueryDocument = <![CDATA[select CatCode, CatRemark from L_A006C where code = '{0}' order by CatRemark]]>.Value
            strQueryDocument = String.Format((str + strQueryDocument), PermitType)
            Dim sqlTotalAdapter As SqlDataAdapter = New SqlDataAdapter(strQueryDocument, conn)
            sqlTotalAdapter.Fill(dataTable)
            conn.Close()

            TypePermitSub.DataSource = dataTable
            TypePermitSub.DataTextField = "CatRemark"
            TypePermitSub.DataValueField = "CatCode"
            TypePermitSub.DataBind()
            If (Not TypePermitSub.Items.FindByValue(PermitTypeSub) Is Nothing) Then
                Me.TypePermitSub.SelectedValue = PermitTypeSub
            End If

            lblTo.Visible = False
            txtDateTimeTo.Visible = False
            btDateTo.Visible = False
            txtTimePermit.Visible = True

            TypePermitSub.Enabled = True
            TypePermitSub.Visible = True
        Else
            lblTo.Visible = True
            txtDateTimeTo.Visible = True
            btDateTo.Visible = True
            txtTimePermit.Visible = False
            functionBackEnd = "<script type='text/javascript'> $( document ).ready(function() {    cal.manageFields('ctl00_ContentPlaceHolder1_btDateTo', 'ctl00_ContentPlaceHolder1_txtDateTimeTo', '%m/%d/%Y'); }); </script>"
        End If

        If (PermitType = "05") Then
            txtSuhuTubuh.Enabled = True
        Else
            txtSuhuTubuh.Text = ""
            txtSuhuTubuh.Enabled = False
        End If

    End Sub



    Protected Sub UploadFile(ByVal sender As Object, ByVal e As EventArgs)
        Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings.Item("EMPLOYEEConn").ConnectionString)
        Dim flag As Boolean = True
        Dim strName As String = TransID.Text.Replace("-", "_").Replace("/", "_")
        Dim strPathFile As String = (SITE_SETTINGS.PATH_UPLOAD & "/" & strName)
        Dim pathServer As String = Me.Server.MapPath(("~/" & strPathFile & "/"))

        If Not Directory.Exists(pathServer) Then
            Directory.CreateDirectory(pathServer)
        End If

        If (Me.FileUpload1.PostedFile.ContentLength > 0) Then
            Dim fileName As String = Path.GetFileName(FileUpload1.PostedFile.FileName)
            FileUpload1.PostedFile.SaveAs((pathServer & fileName))

            Try

                Dim conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
                conn.Open()

                Dim sqlQuery As String = <![CDATA[INSERT INTO [dbo].[FileUpload]
                                ([ID], [DocumentNo], [PathFile], [FileName], [UploadDate], [UploadBy]) 
                                VALUES
                                ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')
                        ]]>.Value

                sqlQuery = String.Format(sqlQuery, Guid.NewGuid(), TransID.Text, strPathFile.Replace("/", "\"), fileName, DateTime.Now.ToString, Session("niksite"))

                Dim cmd As SqlCommand = New SqlCommand(sqlQuery, conn)
                cmd.ExecuteScalar()
                conn.Close()

                BindFileUpload()

            Catch exception1 As Exception

                Dim exception As Exception = exception1
                flag = False

            Finally
                If flag Then
                    Me.alertMessage = ("<script type ='text/javascript' > alert('" & fileName & "has been uploaded') </script>")
                Else
                    Me.alertMessage = "<script type ='text/javascript' > alert('data upload failed') </script>"
                End If
                connection.Close()
            End Try

        End If
        Me.Response.Redirect(Me.Request.Url.AbsoluteUri)
    End Sub

    Public Function BindFileUpload()
        If Not String.IsNullOrEmpty(Me.TransID.Text) Then
            Dim strFileName As String = Me.TransID.Text.Replace("-", "_").Replace("/", "_")
            Dim pathFile As String = Me.Server.MapPath(("~/" & SITE_SETTINGS.PATH_UPLOAD & "/" & strFileName))

            If Not Directory.Exists(pathFile) Then
                Directory.CreateDirectory(pathFile)
            End If

            Dim files As String() = Directory.GetFiles(pathFile)
            Dim list As New List(Of ListItem)
            Dim str3 As String
            For Each str3 In files
                list.Add(New ListItem(Path.GetFileName(str3), str3))
            Next
            Me.GridView1.DataSource = list
            Me.GridView1.DataBind()
        End If
    End Function

    Protected Sub DownloadFile(ByVal sender As Object, ByVal e As EventArgs)
        Dim commandArgument As String = DirectCast(sender, LinkButton).CommandArgument
        Me.Response.ContentType = Me.ContentType
        Me.Response.AppendHeader("Content-Disposition", ("attachment; filename=" & Path.GetFileName(commandArgument)))
        Me.Response.WriteFile(commandArgument)
        Me.Response.End()
    End Sub


    Protected Sub DeleteFile(ByVal sender As Object, ByVal e As EventArgs)
        Dim commandArgument As String = DirectCast(sender, LinkButton).CommandArgument
        Dim fileName As String = Path.GetFileName(commandArgument)




        Dim conn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
        conn2.Open()
        Dim dataTable As DataTable = New DataTable()

        Dim strcon2 As String = "SELECT [ID], [PathFile], [FileName] FROM [dbo].[FileUpload] WHERE [DocumentNo] = '{0}' AND [FileName] = '{1}'"
        strcon2 = String.Format(strcon2, Me.TransID.Text, fileName)
        Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, conn2)
        sda2.Fill(dataTable)





        Dim str As String = ""
        If (dataTable.Rows.Count > 0) Then
            Dim str6 As String = dataTable.Rows.Item(0).Item("PathFile").ToString
            fileName = dataTable.Rows.Item(0).Item("FileName").ToString
            str = dataTable.Rows.Item(0).Item("ID").ToString
            File.Delete(commandArgument)
        End If
        Dim flag As Boolean = True


        Try

            Dim conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
            conn.Open()

            Dim sqlQuery As String = <![CDATA[DELETE FROM [dbo].[FileUpload] WHERE [ID] = '{0}']]>.Value

            sqlQuery = String.Format(sqlQuery, str)

            Dim cmd As SqlCommand = New SqlCommand(sqlQuery, conn)
            cmd.ExecuteScalar()
            conn.Close()



            BindFileUpload()


        Catch exception1 As Exception

            Dim exception As Exception = exception1
            flag = False

        Finally
            If flag Then
                alertMessage = ("<script type ='text/javascript' > alert('" & fileName & "has been deleted') </script>")
                btnSubmit.Enabled = False
                btnEdit.Enabled = False
                btnSubmit.Visible = False
            Else
                alertMessage = "<script type ='text/javascript' > alert('data deleted failed') </script>"
            End If

        End Try
        Me.Response.Redirect(Me.Request.Url.AbsoluteUri)
    End Sub


    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Dim flag As Boolean = True
        Dim conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

        Try

            conn.Open()

            Dim sqlQuery As String = <![CDATA[UPDATE L_H006 SET StatusWF = 'INT' WHERE [transid] = '{0}']]>.Value

            sqlQuery = String.Format(sqlQuery, TransID.Text)

            Dim cmd As SqlCommand = New SqlCommand(sqlQuery, conn)
            cmd.ExecuteScalar()
            conn.Close()

        Catch exception1 As Exception

            Dim exception As Exception = exception1
            flag = False

        Finally
            If flag Then
                Me.alertMessage = "<script type ='text/javascript' > alert('Data has been updated') </script>"
                Me.btnSubmit.Enabled = False
                Me.btnEdit.Enabled = False
            Else
                Me.alertMessage = "<script type ='text/javascript' > alert('data updated failed') </script>"
            End If
            conn.Close()
        End Try
    End Sub


    Public Function SetApproval(ByVal _permitData As PermitData) As PermitData

        Dim conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        conn.Open()
        Dim str As String = "JRN0107"
        Dim strQueryDocument As String = "SELECT NIKSITE, kdjabatan, Nik FROM H_A101 WHERE NIKSITE = '{0}'"
        strQueryDocument = String.Format(strQueryDocument, str)


        Dim dataTable As DataTable = New DataTable
        Dim sqlTotalAdapter As SqlDataAdapter = New SqlDataAdapter(strQueryDocument, conn)
        sqlTotalAdapter.Fill(dataTable)
        conn.Close()


        If (dataTable.Rows.Count > 0) Then
            _permitData.verify_nik = dataTable.Rows.Item(0).Item("Nik").ToString
            _permitData.verify_jabat = dataTable.Rows.Item(0).Item("kdjabatan").ToString
        End If


        conn = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        conn.Open()
        Dim table2 As New DataTable
        strQueryDocument = "SELECT NIKSITE, kdjabatan, Nik FROM H_A101 WHERE NIKSITE = (SELECT TOP 1 Manager FROM V_HRMS_EMP_PERMIT WHERE nik = '{0}')"
        strQueryDocument = String.Format(strQueryDocument, _permitData.EmployeeNIK)
        sqlTotalAdapter = New SqlDataAdapter(strQueryDocument, conn)
        sqlTotalAdapter.Fill(table2)
        conn.Close()

        If (table2.Rows.Count > 0) Then
            _permitData.SupNikttd = table2.Rows.Item(0).Item("Nik").ToString
            _permitData.Supjabatttd = table2.Rows.Item(0).Item("kdjabatan").ToString
        End If

        Return _permitData



    End Function

End Class