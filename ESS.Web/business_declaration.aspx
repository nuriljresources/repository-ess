﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="business_declaration.aspx.vb" Inherits="EXCELLENT.business_declaration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Business Declaration Trip
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script src="Scripts/jsm01.js" type="text/javascript"></script>
<script src="Scripts/tcal.js" type="text/javascript"></script>
<script src="Scripts/jquery.min.js" type="text/javascript" ></script>
<link rel="stylesheet" href="css/tcal.css" type="text/css" />
<script src="Scripts/jscal2.js" type ="text/javascript"></script>
    <script src="Scripts/en.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="css/gold/gold.css" />
<style type="text/css">
        .style4
        {
            width: 62%;
        }
        .style6
        {
            width: 353px;
        }
        .style7
        {
            width: 309px;
        }
        .style8
        {
            width: 186px;
        }
        .style9
        {
            width: 603px;
        }
        .style10
        {
            width: 133px;
        }
        .entri
        {
            font-size :1.0em;
            height:16px; 
        }
    </style>
        <script type="text/javascript" >
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode < 45 || charCode > 57 || charCode == 47 || charCode == 45)
                    return false;

                return true;
            }

            function chkchar(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 39 || charCode == 43)
                    return false;

                return true;
            }
            
            function OpenPopup(key) {
                document.getElementById("ctrlToFind").value = key;
                window.open("SearchEmployee.aspx", "List", "scrollbars=no,resizable=no,width=500,height=400");
                return false;
            }

            function OpenPopupfieldcostcd(key) {
                var d = document;
                var jml = d.getElementById("jml").value;
                document.getElementById("ctrlToFind").value = key;
                window.open("searchcostcode.aspx?M" + jml, "List", "scrollbars=no,resizable=no,width=500,height=400");
                return false;
            }

            function OpenPopuptrip(key) {
                document.getElementById("ctrlToFind").value = key;
                window.open("Searchtrip.aspx", "List", "scrollbars=yes,resizable=yes,width=1000,height=400");
                return false;
            }

            function frmcetak() {
                var d = document;
                window.location = "business_dec_trip_report.aspx?id=" + d.getElementById("decno").value
            }
            
            function save() {
                var d = document;
                var genid = '<%= genid %>'

                if (d.getElementById("tripno").value.length == 0) {
                    alert("Please Choose Trip ID");
                    return false;
                }

                if (d.getElementById("costcode").value.length == 0) {
                    alert("Please Entry Costcode");
                    return false;
                }

                if (d.getElementById("snik").value.length == 0) {
                    alert("Please Entry Supervisor");
                    return false;
                }
                
                var i = 1;
                var jml = d.getElementById("jml").value;
                var qry = "";
                for (i = 1; i <= jml; i++) {
                    if (i <= jml) {
                        genid = ((genid * 1) + 1)
                    }
                    qry = qry + " (" + genid + ","
                    qry = qry + "'||',"
                    qry = qry + "'" + d.getElementById("decdate" + i).value + "',"
                    qry = qry + "'" + d.getElementById("expensetype" + i).value + "',"
                    qry = qry + "'" + d.getElementById("currency" + i).value + "',"
                    qry = qry + "'" + d.getElementById("subtotal" + i).value + "',"
                    qry = qry + "'" + d.getElementById("remarks" + i).value + "')";
                    if (i != jml) {
                        qry = qry + "+";
                    }
                }

                if (jml == 1 && d.getElementById("decdate1").value == "") {
                    qry = "null"
                }
                
                $.ajax({

                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "WS/UserMgmt.asmx/Adddec",
                    //_decno, _site, _costcode, _tripno, _kddepar, _snik, _nik, _nmjabat, _fstatus, _nama, _decdate, _remark, _reqby, _reqbydt, _apprby, _apprdate, _reviewby, _reviewdate, _proceedby, _proceeddate, _qry
                    data: "{'_decno':" + JSON.stringify(document.getElementById("decno").value) + ",'_site':" + JSON.stringify(document.getElementById("site").value) + ",'_costcode':" + JSON.stringify(document.getElementById("costcode").value) + ",'_tripno':" + JSON.stringify(document.getElementById("tripno").value) + ",'_kddepar':" + JSON.stringify(document.getElementById("kddepar").value) + ",'_snik':" + JSON.stringify(document.getElementById("snik").value) + ",'_nik':" + JSON.stringify(document.getElementById("nik").value) + ",'_nmjabat':" + JSON.stringify(document.getElementById("nmjabat").value) + ",'_fstatus':" + JSON.stringify(document.getElementById("fstatus").value) + ",'_nama':" + JSON.stringify(document.getElementById("nama").value) + ",'_decdate':" + JSON.stringify(document.getElementById('<%=decdate.clientid%>').value) + ",'_remark':" + JSON.stringify(document.getElementById("remark").value) + ",'_reqby':" + JSON.stringify(document.getElementById("reqby").value) + ",'_reqbydt':" + JSON.stringify(document.getElementById("reqbydt").value) + ",'_apprby':" + JSON.stringify(document.getElementById("apprby").value) + ",'_apprdate':" + JSON.stringify(document.getElementById("apprdate").value) + ",'_reviewby':" + JSON.stringify(document.getElementById("reviewby").value) + ",'_reviewdate':" + JSON.stringify(document.getElementById("reviewdate").value) + ",'_proceedby':" + JSON.stringify(document.getElementById("proceedby").value) + ",'_proceeddate':" + JSON.stringify(document.getElementById("proceeddate").value) + ",'_qry':" + JSON.stringify(qry) + "}",
                    dataType: "json",
                    success: function(res) {
                    d.getElementById('<%=cetak.clientid%>').disabled = false;
                    d.getElementById('<%=btnPaymentUsage.clientid%>').disabled = false;

                    d.getElementById("btnsavedec").disabled = true;
                    alert("Save Data Success ...!");

                    },
                    error: function(err) {
                        window.location = "home.aspx";
                    }
                });
            }
        </script>
        
</asp:Content> 

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <input type="hidden" id="ctrlToFind" />
    <input style="width:90px;" class="entri" type="hidden" id="decno" value='<%=decno %>' />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
    <caption style="text-align: center; font-size: 1.5em; color:White;">Business Declaration</caption>
        <tr>
             <%--<td style ="width:10%;">Declaration Number</td>
             <td style ="width:15%;"> </td>--%>
             <td style ="width:10%;">Trip ID</td>
            <td style ="width:15%;"><input style="width:90px;" class="entri" disabled="disabled"  type="text" id="tripno" value="" />
              <img alt="add" id="Img4" src="images/Search.gif" align="absmiddle" style="cursor: pointer" onclick="OpenPopuptrip('dec')" /></td>
             <td style ="width:10%;"> Site</td>
             <td style ="width:15%;"><input style="width:90px;" class="entri" disabled="disabled" type="text" id="site" /></td>
             <td style ="width:10%;">Cost Code</td>
             <td style ="width:15%;"><input type="text" id="costcode" style="width:90px;" class="entri"  disabled= "disabled"/> <input type="hidden" id="costcd" />
             <%--<img alt="add" id="Img1" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopupfieldcostcd('cc')" />--%> 
             </td>
             <td></td>
        </tr>
        <tr>
            
            <td>Department</td>
            <td><input type="hidden" id="kddepar" /> <input type="text" id="dep" disabled= "disabled" style="width:90px;" class="entri"/> </td> 
            <td>Superior Name </td>
            <td><input type="hidden" id="snik" /> <input type="text" id="superior" disabled= "disabled" style="width:90px;" class="entri"/>
                          <%--<img alt="add" id="Img5" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopup('suser')" />--%> </td>
             <td>Status</td>
            <td><input style="width:90px;" class="entri" disabled="disabled" type="text" id="fstatus"/></td>   
            <td></td><td class="style8"></td>
        </tr>
        <tr>
            <td>NIK</td>
            <td colspan="0"><input style="width:90px;" class="entri" disabled="disabled" type="text" id="nik"/> 
            </td>
            <td>Position</td>
            <td><input type="hidden" id="nmjabat" /> <input type="text" id="position" disabled= "disabled" style="width:90px;" class="entri" /></td>
            <td>Type</td><td><input type="text" id="type" disabled= "disabled" style="width:90px;" class="entri" /></td>
        </tr>
        <tr>
        <td>Name</td>
        <td><input style="width:90px;" class="entri" disabled="disabled" type="text" id="nama" /> </td>
        </tr>
        <tr>
        <td>Declare Date</td>
        <td><asp:TextBox ID="decdate" runat="Server" width="90px" Enabled="false" class="entri"></asp:TextBox><%--<button id="bdecdt">...</button>--%></td>
        </tr>
        <tr>
          <td>Remark</td>
          <td colspan="2"><textarea id="remark" style="width:80%;Height:100px" onkeypress="return chkchar(event)" class="entri" cols="5" rows="5"></textarea></td>
          <td colspan="4"> 
              <%--<div style="float: left; position: absolute; top: 105px; left: 507px;">Checked by direct Outcome Report<br />
              &nbsp;&nbsp;&nbsp;<input type="checkbox" id="chktripoutcrpt" /> Business Trip Outcome Report </div>--%>         
              <table>
              <tr>
              <td></td><td style="width:200px"><input type="hidden" id="reqby" visible="false" class="entri"/>
<%--              <img alt="add" id="Img2" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopup('reqby')" />--%>
              </td>
              <td></td><td><input type="hidden" id="reqbydt" /> <%--<button id="breqbydt">...</button>--%>
                                                                                            </td>
              </tr>
              <tr>
              <td></td><td><input type="hidden" id="apprby" visible="false" class="entri"/>
<%--              <img alt="add" id="Img3" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopup('supappr')" />--%>
              </td>
              <td></td><td><input type ="hidden" id="apprdate" /> <%--<button id="bapprdt">...</button>--%></td>
              </tr>
              <tr>
              <td></td><td><input type="hidden" id="reviewby" visible="false" class="entri"/>
<%--              <img alt="add" id="Img6" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopup('revby')" />--%>
              </td>
              <td></td><td><input type="hidden" id="reviewdate" /> <%--<button id="breviewdt" >...</button>--%></td>
              </tr>
              <tr>
              <td></td><td><input type="hidden" id="proceedby" visible="false" class="entri"/>
<%--              <img alt="add" id="Img7" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopup('finappr')" />--%>
              </td>
              <td></td><td><input type="hidden" id="proceeddate" /> <%--<button id="bproceeddt">...</button>--%></td>
              <td><input type="hidden" id="jml" value="1" /></td>
              </tr>
              <tr>
              <td>
                      
              </td>
              </tr>
              </table>
          </td> 
        </tr>
        </table> 
        
                                              <table width="80%" class="data-table-1" cellpadding="0" cellspacing="0" id="tb">
                                  <caption style="color:White;">
                                      Detail Claim
                                      <%--<span id="dtblPeserta" onclick="displayRow('tblPeserta')" 
                                              style="cursor:pointer">Hide</span>--%>
                                  </caption>                                  
                   
                                  <thead>
                                      <tr>
                                          <th width="3%">
                                              No.</th>
                                          <th width="19%">
                                              Date</th>
                                          <th width="9%">
                                              Type</th>
                                          <th width="6%">
                                              Currency</th>
                                          <th width="14%">
                                              Sub Total</th>
                                          <th width="55%">
                                              Remarks</th>
                                      </tr>
                                      <tr>
                                           <td colspan="6">
                                                <div id="input1" style="margin-bottom:2px; margin-right:0px; width:100%;" class="clonedInput">
		                                            <input type="text" name="no1" id="no1" value="1" style="width:3%;" class="entri" disabled="disabled" />
		                                            <input type="text" name="decdate1" id="decdate1" readonly="readonly" style="width:15%;" class="entri" /><button id="bdecdetdt1">...</button>
		                                            <select id="expensetype1" name="expensetype1" style="width:10%;">
		                                            <option value="01">Tickets</option>
		                                            <option value="02">Hotel</option>
		                                            <option value="03">Transports</option>
		                                            <option value="04">Others</option>
		                                            <option value="05">Foods</option>
		                                            </select>
		                                            <select id="currency1" name="currency1" style="width:6%;">
		                                            <option value="IDR">IDR</option>
		                                            <option value="USD">USD</option>
		                                            <option value="MYR">MYR</option>
		                                            <option value="SGD">SGD</option>
		                                            </select>
		                                            <input type="text" name="subtotal1" id="subtotal1" onkeypress="return isNumberKey(event)" class="entri" style="width:14%;"/>
		                                            <input type="text" name="remarks1" id="remarks1" onkeypress="return chkchar(event)" class="entri" style="width:46%;" />
	                                            </div>
                                           </td>
                                      </tr>
                                  </thead>
                                  <tbody>
                                  </tbody>
                             </table>
                             
                             
     
	        <div style="position:absolute;">
		        <input type="button" id="btnAdd" value="add" />
		        <input type="button" id="btnDel" value="remove" />
	        </div>
	        <script type ="text/javascript" >

	            //<![CDATA[

	            var cal = Calendar.setup({
	                onSelect: function(cal) { cal.hide() },
	                showTime: false
	            });

	            cal.manageFields("bdecdetdt1", "decdate1", "%m/%d/%Y");

	            //]]

	            $(document).ready(function() {
	                $('#btnAdd').click(function() {
	                    var num = $('.clonedInput').length;
	                    var newNum = new Number(num + 1);

	                    var newElem = $('#input' + num).clone().attr('id', 'input' + newNum);

	                    newElem.children(':eq(0)').attr('id', 'no' + newNum).attr('name', 'no' + newNum).attr('value', newNum);
	                    newElem.children(':eq(1)').attr('id', 'decdate' + newNum).attr('name', 'decdate' + newNum);
	                    newElem.children(':eq(2)').attr('id', 'bdecdetdt' + newNum).attr('name', 'bdecdetdt' + newNum);
	                    newElem.children(':eq(3)').attr('id', 'expensetype' + newNum).attr('name', 'expensetype' + newNum);
	                    newElem.children(':eq(4)').attr('id', 'currency' + newNum).attr('name', 'currency' + newNum);
	                    newElem.children(':eq(5)').attr('id', 'subtotal' + newNum).attr('name', 'subtotal' + newNum);
	                    newElem.children(':eq(6)').attr('id', 'remarks' + newNum).attr('name', 'remarks' + newNum);



	                    $('#input' + num).after(newElem);

	                    $('#btnDel').attr('disabled', '');

	                    var d = document;
	                    (d.getElementById("jml").value = newNum)

	                    if (newNum == 100)
	                        $('#btnAdd').attr('disabled', 'disabled');

	                    cal.manageFields("bdecdetdt1", "decdate1", "%m/%d/%Y");
	                    if (newNum == 2)
	                        cal.manageFields("bdecdetdt2", "decdate2", "%m/%d/%Y");
	                    if (newNum == 3)
	                        cal.manageFields("bdecdetdt3", "decdate3", "%m/%d/%Y");
	                    if (newNum == 4)
	                        cal.manageFields("bdecdetdt4", "decdate4", "%m/%d/%Y");
	                    if (newNum == 5)
	                        cal.manageFields("bdecdetdt5", "decdate5", "%m/%d/%Y");
	                    if (newNum == 6)
	                        cal.manageFields("bdecdetdt6", "decdate6", "%m/%d/%Y");
	                    if (newNum == 7)
	                        cal.manageFields("bdecdetdt7", "decdate7", "%m/%d/%Y");
	                    if (newNum == 8)
	                        cal.manageFields("bdecdetdt8", "decdate8", "%m/%d/%Y");
	                    if (newNum == 9)
	                        cal.manageFields("bdecdetdt9", "decdate9", "%m/%d/%Y");
	                    if (newNum == 10)
	                        cal.manageFields("bdecdetdt10", "decdate10", "%m/%d/%Y");
	                    if (newNum == 11)
	                        cal.manageFields("bdecdetdt11", "decdate11", "%m/%d/%Y");
	                    if (newNum == 12)
	                        cal.manageFields("bdecdetdt12", "decdate12", "%m/%d/%Y");
	                    if (newNum == 13)
	                        cal.manageFields("bdecdetdt13", "decdate13", "%m/%d/%Y");
	                    if (newNum == 14)
	                        cal.manageFields("bdecdetdt14", "decdate14", "%m/%d/%Y");
	                    if (newNum == 15)
	                        cal.manageFields("bdecdetdt15", "decdate15", "%m/%d/%Y");
	                    if (newNum == 16)
	                        cal.manageFields("bdecdetdt16", "decdate16", "%m/%d/%Y");
	                    if (newNum == 17)
	                        cal.manageFields("bdecdetdt17", "decdate17", "%m/%d/%Y");
	                    if (newNum == 18)
	                        cal.manageFields("bdecdetdt18", "decdate18", "%m/%d/%Y");
	                    if (newNum == 19)
	                        cal.manageFields("bdecdetdt19", "decdate19", "%m/%d/%Y");
	                    if (newNum == 20)
	                        cal.manageFields("bdecdetdt20", "decdate20", "%m/%d/%Y");
	                    if (newNum == 21)
	                        cal.manageFields("bdecdetdt21", "decdate21", "%m/%d/%Y");
	                    if (newNum == 22)
	                        cal.manageFields("bdecdetdt22", "decdate22", "%m/%d/%Y");
	                    if (newNum == 23)
	                        cal.manageFields("bdecdetdt23", "decdate23", "%m/%d/%Y");
	                    if (newNum == 24)
	                        cal.manageFields("bdecdetdt24", "decdate24", "%m/%d/%Y");
	                    if (newNum == 25)
	                        cal.manageFields("bdecdetdt25", "decdate25", "%m/%d/%Y");
	                    if (newNum == 26)
	                        cal.manageFields("bdecdetdt26", "decdate26", "%m/%d/%Y");
	                    if (newNum == 27)
	                        cal.manageFields("bdecdetdt27", "decdate27", "%m/%d/%Y");
	                    if (newNum == 28)
	                        cal.manageFields("bdecdetdt28", "decdate28", "%m/%d/%Y");
	                    if (newNum == 29)
	                        cal.manageFields("bdecdetdt29", "decdate29", "%m/%d/%Y");
	                    if (newNum == 30)
	                        cal.manageFields("bdecdetdt30", "decdate30", "%m/%d/%Y");
	                    if (newNum == 31)
	                        cal.manageFields("bdecdetdt31", "decdate31", "%m/%d/%Y");
	                    if (newNum == 32)
	                        cal.manageFields("bdecdetdt32", "decdate32", "%m/%d/%Y");
	                    if (newNum == 33)
	                        cal.manageFields("bdecdetdt33", "decdate33", "%m/%d/%Y");
	                    if (newNum == 34)
	                        cal.manageFields("bdecdetdt34", "decdate34", "%m/%d/%Y");
	                    if (newNum == 35)
	                        cal.manageFields("bdecdetdt35", "decdate35", "%m/%d/%Y");
	                    if (newNum == 36)
	                        cal.manageFields("bdecdetdt36", "decdate36", "%m/%d/%Y");
	                    if (newNum == 37)
	                        cal.manageFields("bdecdetdt37", "decdate37", "%m/%d/%Y");
	                    if (newNum == 38)
	                        cal.manageFields("bdecdetdt38", "decdate38", "%m/%d/%Y");
	                    if (newNum == 39)
	                        cal.manageFields("bdecdetdt39", "decdate39", "%m/%d/%Y");
	                    if (newNum == 40)
	                        cal.manageFields("bdecdetdt40", "decdate40", "%m/%d/%Y");
	                    if (newNum == 41)
	                        cal.manageFields("bdecdetdt41", "decdate41", "%m/%d/%Y");
	                    if (newNum == 42)
	                        cal.manageFields("bdecdetdt42", "decdate42", "%m/%d/%Y");    
	                });

	                $('#btnDel').click(function() {
	                    var num = $('.clonedInput').length;

	                    $('#input' + num).remove();
	                    $('#btnAdd').attr('disabled', '');

	                    var d = document;
	                    (d.getElementById("jml").value = d.getElementById("jml").value - 1)

	                    if (num - 1 == 1)
	                        $('#btnDel').attr('disabled', 'disabled');
	                });

	                $('#btnDel').attr('disabled', 'disabled');
	            });  
                                 
</script>
    <br />
    <br />
    <table>
    <tr>
    <td>
        <input type="button" id="btnsavedec" value="save" onclick="save()" style="width: 80px; height:20px; font-family: Calibri;" />
        
    </td>

    <td>
        <%--<input type="button" id="cetak" value="Print" onclick="frmcetak()" style="width: 80px; height:20px; font-family: Calibri;"/>--%>
        <asp:Button ID="cetak" Text="print" runat="server" OnClick="cetak_Click" OnClientClick="aspnetForm.target ='_blank';" style="width: 80px; height:20px; font-family: Calibri;"/>
        
        <asp:Button ID="btnPaymentUsage" Text="print pa" runat="server" OnClick="cetak_Click_Payment" OnClientClick="aspnetForm.target ='_blank';" style="width: 80px; height:20px; font-family: Calibri;" />
        
        
        
    </td>
    </tr>   
    </table>
   
    
    <script type ="text/javascript" >
            
        document.getElementById('<%=cetak.clientid%>').disabled = true;
        document.getElementById('<%=btnPaymentUsage.clientid%>').disabled = true;
        //<![CDATA[

        var cal = Calendar.setup({
            onSelect: function(cal) { cal.hide() },
            showTime: false
        });

        
        //]]
        </script>
</asp:Content>