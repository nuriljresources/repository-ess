﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="default.aspx.vb" Inherits="EXCELLENT._default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<script src="Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>

    <style type="text/css" >
        #form1
        {
            font-family:Tahoma,Arial;
        }
        #form1 h1 
        {
            color: #DEDEDE;
            font-size:x-large;
        }
        .title
        {
            color: blue;
            font-size: small;
            margin-bottom:10px;
        }
        .line
        {
            width: 100%;
            height: 20px;
        }
        .general
        {
            font-size:0.8em;
            color:#33ADFF;
            font-weight:bold;
            border-style:solid;
            border-width:1px;
            border-color:#33ADFF;
            padding-left:5px;
        }
        
        .general a
        {
            text-decoration:none;
            color:#33ADFF;
        }
        
        .general a:hover
        {
            text-decoration:underline;
            color:#33ADFF;
        }
    </style>
    <title></title>

    <script language="javascript" type="text/javascript">
// <!CDATA[

        function btnfind_onclick() {

        }

// ]]>
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="top_content" style="background-color:#F0F0FF; border-bottom:solid 1px blue">
    <div id="welcome" style="width:450px; left:100px; text-align:left; position:absolute;">
            <h1>Welcome to the Document Center</h1>
    </div>
        <div id="logo" style="height:80px; width:80px;"><img alt="Permit Folder" src="../images/Folder_img.png" style="height:80px; width:80px;" /></div>
        
        <div id="button" style="width:450px; left:100px; text-align:left; position:absolute; top:60px;">
            <asp:Button ID="uploaddoc" Text="Upload Document" runat="server" />
        </div>
    </div>
    <div class="line"></div>
    <div class="title">
        <strong>Newest Document</strong>
    </div>
    <div id="search" class="general" style="width:200px; font-size:0.6em; margin-bottom:5px; padding-bottom:5px; padding-top:5px;"> Search <input type="text" id="find" style="width:130px;" /> <img src="../images/Search.gif" id="btnfind" runat="server" style="vertical-align:bottom;" onclick="return btnfind_onclick()" /></div>
    <div id="list_newest" class="general" style="width:200px;">
        <div id="file1"> <img alt="open document" src="../images/open_doc_small.png" /> <a href="#">&nbsp;2012</a></div>
        <div id="Div1"> <img alt="open document" src="../images/open_doc_small.png" /> <a href="#">&nbsp;2011</a></div>
        <div id="Div2"> <img alt="open document" src="../images/open_doc_small.png" /> <a href="#">&nbsp;2010</a></div>
        <div id="Div3"> <img alt="open document" src="../images/open_doc_small.png" /> <a href="#">&nbsp;2009</a></div>
        <div id="Div4"> <img alt="open document" src="../images/open_doc_small.png" /> <a href="#">&nbsp;2008</a></div>
        <div id="Div5"> <img alt="open document" src="../images/open_doc_small.png" /> <a href="#">&nbsp;2007</a></div>
    </div>
    
    <div id="list_permit" class="general" style="width:1000px; border-style:none; height:400px; left:220px; top:135px; position:absolute; font-size:0.6em;">
       
        <asp:GridView ID="GridView1" runat="server" DataKeyNames="id" AutoGenerateColumns="False"
            AllowPaging="True" PageSize="50" Width="800px">
            <Columns>
            <asp:CommandField ShowSelectButton="True" SelectText="Select" ButtonType="Link" HeaderStyle-Width="40px" ItemStyle-VerticalAlign="top" >
                <HeaderStyle Width="40px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:CommandField>
            <asp:CommandField ShowDeleteButton="true" DeleteText="Delete" ButtonType="Link" HeaderStyle-Width="40px" ItemStyle-VerticalAlign ="Top">
                <HeaderStyle Width="40px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:CommandField>
            
            <asp:BoundField DataField="ftype" HeaderText="Type" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="fsubject" HeaderText="Subject" HeaderStyle-Width="200px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="200px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="company_name" HeaderText="Company" HeaderStyle-Width="200px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="200px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="site_name" HeaderText="Site" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="fyear" HeaderText="Year" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="fcategory" HeaderText="Category" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            
            <asp:TemplateField HeaderText="File Name">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" Text='<%# Bind("f_name") %>' NavigateUrl='<%#"~/view_doc.aspx?ID=" + eval("f_name") %>' Target="_blank" runat="server"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField> 
            </Columns>  
        </asp:GridView>
        
    </div>
    
    </form>
</body>
</html>
