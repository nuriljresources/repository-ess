﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="spl_approval2_list.aspx.vb" Inherits="EXCELLENT.spl_approval2_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Overtime Request Approval List
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content> 

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
<caption style="text-align: center; font-size: 1.5em; color:White;">Overtime Request Approval List</caption>
<tr>
<td style="width:5%;">Periode</td>
<td style="width:8%;">
<asp:DropDownList ID="ddlbulan" runat="server">
    <asp:ListItem Value = "1">January</asp:ListItem>
    <asp:ListItem Value = "2">February</asp:ListItem>
    <asp:ListItem Value = "3">March</asp:ListItem>
    <asp:ListItem Value = "4">April</asp:ListItem>
    <asp:ListItem Value = "5">May</asp:ListItem>
    <asp:ListItem Value = "6">June</asp:ListItem>
    <asp:ListItem Value = "7">July</asp:ListItem>
    <asp:ListItem Value = "8">August</asp:ListItem>
    <asp:ListItem Value = "9">September</asp:ListItem>
    <asp:ListItem Value = "10">October</asp:ListItem>
    <asp:ListItem Value = "11">November</asp:ListItem>
    <asp:ListItem Value = "12">December</asp:ListItem>
</asp:DropDownList>
<asp:TextBox ID="tripfrom" style="width:70px;" runat="server" width="90px" ReadOnly="true" Visible ="false"></asp:TextBox></td>
<td style="width:5%;"><asp:DropDownList ID="ddltahun" runat="server"></asp:DropDownList></td>

<td><asp:Button ID="btnSearch" runat="server" Text="Search"/> <asp:Button ID="btncomplist" runat="server" Text="Approval list" /></td>

<%--<td style="width:5%;"><input type="button" visible="false" id="cetak" value="Print" onclick="frmcetak()" style="width: 80px; font-family: Calibri; " /></td>--%>
<td>
 <asp:TextBox ID="tripto" style="width:70px;" runat="server" ReadOnly="true" visible="false"></asp:TextBox>
</td>
<td></td>
</tr></table>

<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="noreg" AllowPaging="true" PageSize="100" Width="900px">
    <RowStyle Font-Size="Smaller" />
<Columns>

<asp:BoundField DataField="noreg" HeaderText="Trans Number" Visible="False" 
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
 </asp:BoundField>
<asp:BoundField DataField="Periode" HeaderText="Period" Visible= "false"
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
 </asp:BoundField>
    <asp:BoundField DataField="niksite" HeaderText="NIK" HeaderStyle-Width="50px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="50px"></HeaderStyle>
<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    
     <asp:BoundField DataField="nama" HeaderText="Name" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>

<asp:BoundField DataField="jam_start" HeaderText="Time Start" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="jam_end" HeaderText="Time end" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>  

<asp:BoundField DataField="remarks" HeaderText="Task" HeaderStyle-Width="150px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="status" HeaderText="Status" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

</Columns> 
    <SelectedRowStyle BackColor="#99CCFF" />
    <HeaderStyle BackColor="#0099FF" />
</asp:GridView>
<%=msg%>
</asp:Content>