﻿Imports System.Data.SqlClient
Partial Public Class medic_frameslist
    Inherits System.Web.UI.Page
    Public tglaw As String
    Public th As String
    Public v1 As String
    Public v2 As String
    Public imonth As Integer
    Public js As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("niksite") = Nothing Or Session("niksite") = "" Then Response.Redirect("login.aspx")
        If Not IsPostBack Then
            Dim a As String

            tglaw = Request(ddlbulan.UniqueID)
            th = Request(ddltahun.UniqueID)
            Dim i As Integer

            Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

            sqlConn.Open()
            Dim strcon As String = "select min(tgltrans) as min_date ,max(tgltrans) as max_date from H_H21502"
            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            v1 = dtb.Rows(0)!min_date.ToString
            If v1.ToString = "" Then
                v1 = Today.Year.ToString
            Else
                v1 = CDate(v1).ToString("yyyy")
            End If

            v2 = dtb.Rows(0)!max_date.ToString
            If v2.ToString = "" Then
                v2 = Today.Year.ToString
            Else
                v2 = CDate(v2).ToString("yyyy")
            End If

            ddltahun.Items.Clear()
            For i = v1 To v2
                ddltahun.Items.Add(i)
            Next

            ddltahun.SelectedValue = (i - 1).ToString

            Select Case tglaw
                Case "January"
                    imonth = 1
                Case "February"
                    imonth = 2
                Case "March"
                    imonth = 3
                Case "April"
                    imonth = 4
                Case "May"
                    imonth = 5
                Case "June"
                    imonth = 6
                Case "July"
                    imonth = 7
                Case "August"
                    imonth = 8
                Case "September"
                    imonth = 9
                Case "October"
                    imonth = 10
                Case "November"
                    imonth = 11
                Case "December"
                    imonth = 12
                Case Else
                    imonth = 1
            End Select

            Dim sqlConn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
            Try

                Dim dtb2 As DataTable = New DataTable()

                Dim strcon2 As String = "select Noreg, Kdsite, (convert(char(11),tgltrans,106)) as tgltrans, nik, (select niksite from H_A101 where nik= H_H21502.Nik) as niksite, (select nama from H_A101 where nik = H_H21502.nik) as nama, (case fstatus when 0 then 'Unauthorize' when 1 then 'Authorize' end) as fstatus, keterangan from H_H21502 "
                'strcon2 = strcon2 + " where month(reqdate) = " + Today.Month.ToString + " and YEAR(reqdate) = " + Today.Year.ToString + " and createdby = '" + Session("niksite") + "' and stedit <> '2'"
                strcon2 = strcon2 + "where nik = '" + Session("niksite") + "' and StEdit <> 2 and month(TglTrans) = " + Today.Month.ToString + " and YEAR(TglTrans) = " + Today.Year.ToString + ""
                strcon2 = strcon2 + " union "
                strcon2 = strcon2 + " select Noreg, Kdsite, (convert(char(11),tgltrans,106)) as tgltrans, nik, (select niksite from H_A101 where nik= H_H21502.Nik) as niksite, (select nama from H_A101 where nik = H_H21502.nik) as nama, (case fstatus when 0 then 'Unauthorize' when 1 then 'Authorize' end) as fstatus, keterangan from H_H21502, V_A004 where H_H21502.nik in (V_A004.nikb) and V_A004.nika = '" + Session("niksite") + "' and V_A004.module = 'MEDIC' and StEdit <> 2 and month(TglTrans) = " + Today.Month.ToString + " and YEAR(TglTrans) = " + Today.Year.ToString + ""
                Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn2)
                sda2.Fill(dtb2)

                GridView1.DataSource = dtb2
                GridView1.DataBind()
                ddlbulan.SelectedValue = Today.Month

            Catch ex As Exception
            Finally
                sqlConn.Close()
            End Try
        End If
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()

            Dim strcon As String = "select Noreg, Kdsite, (convert(char(11),tgltrans,106)) as tgltrans, nik, (select niksite from H_A101 where nik= H_H21502.Nik) as niksite, (select nama from H_A101 where nik = H_H21502.nik) as nama, (case fstatus when 0 then 'Unauthorize' when 1 then 'Authorize' end) as fstatus, keterangan from H_H21502 "
            strcon = strcon + "where nik = '" + Session("niksite") + "' and StEdit <> 2 and month(TglTrans) = " + ddlbulan.SelectedValue.ToString + " and YEAR(TglTrans) = " + ddltahun.SelectedValue.ToString
            strcon = strcon + " union "
            strcon = strcon + " select Noreg, Kdsite, (convert(char(11),tgltrans,106)) as tgltrans, nik, (select niksite from H_A101 where nik= H_H21502.Nik) as niksite, (select nama from H_A101 where nik = H_H21502.nik) as nama, (case fstatus when 0 then 'Unauthorize' when 1 then 'Authorize' end) as fstatus, keterangan from H_H21502, V_A004 where H_H21502.nik in (V_A004.nikb) and V_A004.nika = '" + Session("niksite") + "' and V_A004.module = 'MEDIC' and StEdit <> 2 and month(TglTrans) = " + ddlbulan.SelectedValue.ToString + " and YEAR(TglTrans) = " + ddltahun.SelectedValue.ToString
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            GridView1.DataSource = dtb
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging

        GridView1.PageIndex = e.NewPageIndex
        GridView1.DataBind()

    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        If GridView1.SelectedRow Is Nothing Then Exit Sub

        Dim row As GridViewRow = GridView1.SelectedRow

        Session("framenoreg") = GridView1.DataKeys(row.RowIndex).Value.ToString
        Session("framenoregst") = "EditFrame"
        Response.Redirect("medic_frames.aspx")
    End Sub
End Class