﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="business_dec_trip_report.aspx.vb" Inherits="EXCELLENT.business_dec_trip_report" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

            <asp:Button ID="btnsavepdf" runat="server" BorderStyle="None" Text="Save to PDF" 
                Font-Underline="True" ForeColor="#3333FF" BackColor="White" /><br />
    <asp:TextBox ID="arg1" runat="server" Visible ="false"></asp:TextBox>
    <asp:TextBox ID="arg2" runat="server" Visible ="false"></asp:TextBox>
    <asp:TextBox ID="arg3" runat="server" Visible ="false"></asp:TextBox>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" Height="586px" Width="876px" ShowExportControls="False" 
            ShowPrintButton="False">
            <LocalReport ReportPath="Laporan\business_declatation.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="DataSet6_V_H002" />
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource2" Name="DataSet6_V_H00105" />
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource3" Name="DataSet6_V_H001051" />
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource4" Name="DataSet6_BTRGroupPrice" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:jrn_citrixcon %>" 
            
            
                SelectCommand="SELECT Decno, GenId, decdate, expensetype, currency, subtotal, remarks, remarks2, (SELECT currcode FROM V_H001 WHERE (TripNo IN (SELECT TripNo FROM V_H002 WHERE (DecNo = V_H00105.Decno)))) AS CurrAdvan, (SELECT cashadvan FROM V_H001 AS V_H001_1 WHERE (TripNo IN (SELECT TripNo FROM V_H002 AS V_H002_1 WHERE (DecNo = V_H00105.Decno)))) AS CashAdvan FROM V_H00105 WHERE (Decno = @trip) ORDER BY currency">
            <SelectParameters>
                <asp:ControlParameter ControlID="arg3" Name="trip" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:jrn_citrixcon %>" 
            
            
                SelectCommand="SELECT decno, genid, decdate, expensetype, currency, subtotal, remarks, remarks2 FROM V_H00105 WHERE (decno = @trip) ORDER BY decdate">
            <SelectParameters>
                <asp:ControlParameter ControlID="arg2" Name="trip" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:jrn_citrixcon %>" 
            
            
            
                SelectCommand="SELECT DecNo, TripNo, TransDate, Remarks, reqby, reqdate, apprby, apprdate, reviewby, reviewdate, proceedby, proceeddate, check_sup, decTotal, currcode, CreatedBy, CreatedIn, CreatedTime, ModifiedBy, ModifiedIn, ModifiedTime, StEdit, DeleteBy, DeleteTime, fstatus, nik, (SELECT Nama FROM H_A101 WHERE (Nik = V_H002.nik)) AS nama, (SELECT NmDepar FROM H_A130 WHERE (KdDepar = (SELECT kddepar FROM V_H001 WHERE (TripNo = V_H002.TripNo)))) AS nmdepar, (SELECT NmJabat FROM H_A150 WHERE (KdJabat = (SELECT kdjabat FROM V_H001 AS V_H001_4 WHERE (TripNo = V_H002.TripNo)))) AS nmjabat, (SELECT costcode FROM V_H001 AS V_H001_3 WHERE (TripNo = V_H002.TripNo)) AS costcode, (SELECT cashadvan FROM V_H001 AS V_H001_2 WHERE (TripNo = V_H002.TripNo)) AS cashadvan, (SELECT Nama FROM H_A101 AS H_A101_5 WHERE (Nik = (SELECT sup_nik FROM V_H001 AS V_H001_1 WHERE (TripNo = V_H002.TripNo)))) AS superior, (SELECT Nama FROM H_A101 AS H_A101_4 WHERE (Nik = V_H002.reqby)) AS req, (SELECT Nama FROM H_A101 AS H_A101_3 WHERE (Nik = V_H002.apprby)) AS appr, (SELECT Nama FROM H_A101 AS H_A101_2 WHERE (Nik = V_H002.reviewby)) AS review, (SELECT Nama FROM H_A101 AS H_A101_1 WHERE (Nik = V_H002.proceedby)) AS proceed, (SELECT detailTrip FROM V_H001 AS V_H001_5 WHERE (TripNo = V_H002.TripNo)) AS detailtrip, (SELECT costName FROM H_A221 WHERE (costcode = (SELECT costcode FROM V_H001 AS V_H001_5 WHERE (TripNo = V_H002.TripNo)))) AS costname, (SELECT NIKSITE FROM H_A101 AS H_A101_6 WHERE (Nik = V_H002.reqby)) AS niksite FROM V_H002 WHERE (DecNo = @argReg)">
            <SelectParameters>
                <asp:ControlParameter ControlID="arg1" Name="argReg" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
            ConnectionString="<%$ ConnectionStrings:jrn_citrixcon %>" SelectCommand="SELECT currency, SUM(Tickets) AS Tickets, SUM(Hotel) AS Hotel, SUM(Transport) AS Transport, SUM(Others) AS Others, SUM(Food) AS Food FROM (
SELECT
currency,
CASE  
  WHEN expensetype = '01' THEN SUM(subtotal)
  ELSE 0
END as Tickets,
CASE  
  WHEN expensetype = '02' THEN SUM(subtotal)
  ELSE 0
END as Hotel,
CASE  
  WHEN expensetype = '03' THEN SUM(subtotal)
  ELSE 0
END as Transport,
CASE  
  WHEN expensetype = '04' THEN SUM(subtotal)
  ELSE 0
END as Others,
CASE  
  WHEN expensetype = '05' THEN SUM(subtotal)
  ELSE 0
END as Food


FROM V_H00105 WHERE Decno = @trip
GROUP BY currency, expensetype 

) AL GROUP BY currency 
ORDER BY currency">
            <SelectParameters>
                <asp:ControlParameter ControlID="arg1" Name="trip" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        
        <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" 
            SelectMethod="GetData" 
            TypeName="EXCELLENT.DataSet6TableAdapters.V_H001051TableAdapter">
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
            SelectMethod="GetData" 
            TypeName="EXCELLENT.DataSet6TableAdapters.V_H00105TableAdapter">
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            SelectMethod="GetData" 
            TypeName="EXCELLENT.DataSet6TableAdapters.V_H002TableAdapter">
        </asp:ObjectDataSource>
        
        <asp:ObjectDataSource ID="ObjectDataSource4" runat="server" 
            SelectMethod="GetData" 
            TypeName="EXCELLENT.DataSet6TableAdapters.BTRGroupPriceTableAdapter">
        </asp:ObjectDataSource>
    
    
    </div>
    </form>
</body>
</html>
