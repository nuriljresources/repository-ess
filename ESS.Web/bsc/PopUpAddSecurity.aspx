﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PopUpAddSecurity.aspx.vb"
   Inherits="EXCELLENT.PopUpAddSecurity" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title></title>
   <link href="../css/site.css" rel="stylesheet" type="text/css" />

   <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>

   <script type="text/javascript">
      var pw = window.opener;
      var inputFrm = pw.document.forms['aspnetForm'];
      //inputFrm.elements['ctl00_ContentPlaceHolder1_hid'].value

      function UseSelected(nopeg, nama, kddepar, kdcabang) {
         var myForm = document.form1; //Form Name
         var mySel = myForm.lbRegistered; // Listbox Name
         var myOption;

         for (var i = 0; i < mySel.options.length; ++i) {
            if (mySel.options[i].value == nopeg) {
               alert('Nik sudah terdaftar');
               return;
            }
         }

         myOption = document.createElement("Option");
         myOption.text = nama; //Textbox's value
         myOption.value = nopeg; //Textbox's value
         mySel.add(myOption);
      }

      function Left(str, n) {
         if (n <= 0)
            return "";
         else if (n > String(str).length)
            return str;
         else
            return String(str).substring(0, n);
      }

      function Right(str, n) {
         if (n <= 0)
            return "";
         else if (n > String(str).length)
            return str;
         else {
            var iLen = String(str).length;
            return String(str).substring(iLen, iLen - n);
         }
      }

      function keyPressed(e) {
         switch (e.keyCode) {
            case 13:
               {
                  document.getElementById("btnSearch").click();
                  break;
               }
            default:
               {
                  break;
               }
         }
      }

      function cekArgumen() {
         var d = document;
         if ((d.getElementById("SearchKey").value.length < 3) || (d.getElementById("SearchKey").value.length == 0)) {
            alert("Berikan nama yang lebih spesifik (minimal 3 karakter)");
            return false;
         }
      }
   </script>

</head>
<body>
   <form id="form1" runat="server">
   <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" />
   <asp:UpdatePanel ID="MyUpdatePanel" runat="server">
      <ContentTemplate>
         <div>
            <table border="0" cellpadding="0" cellspacing="0" class="data-table">
               <caption style="text-align: center">
                  Security Setup</caption>
               <tr style="vertical-align: top">
                  <td style="padding: 5px; width: 10%; vertical-align: top">
                     Nama
                  </td>
                  <td style="width: 15%; vertical-align: top">
                     <asp:TextBox ID="SearchKey" runat="server" class="tb10" onkeydown="keyPressed(event);" />
                  </td>
                  <td style="vertical-align: top">
                     <input type="text" id="hide" style="display: none;" /><asp:Button ID="btnSearch"
                        runat="server" Text="Cari Nama" Font-Bold="False" ForeColor="#0033CC" OnClientClick="cekArgumen()"
                        Height="26px" Width="90px" />
                  </td>
               </tr>
               <tr>
                  <td>
                  </td>
               </tr>
               <tr>
                  <td colspan="3">
                     <asp:Repeater ID="rptPages" runat="server">
                        <HeaderTemplate>
                           <table cellpadding="0" cellspacing="0" border="0">
                              <tr>
                                 <td style="vertical-align: top">
                                    <b>Page:</b>&nbsp;
                                 </td>
                                 <td>
                        </HeaderTemplate>
                        <ItemTemplate>
                           <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                              CssClass="text" runat="server" Visible='<%# (CInt(Container.DataItem) <> (PageNumber+1)).ToString() %>'>
                         <%#Container.DataItem%> 
                           </asp:LinkButton>
                           <asp:Label ID="lblPage" runat="server" Visible='<%# (CInt(Container.DataItem) = (PageNumber+1)).ToString()%>'
                              Text="<%#Container.DataItem%>" ForeColor="black" Font-Bold="true">
                           </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                           </td> </tr> </td> </table>
                        </FooterTemplate>
                     </asp:Repeater>
               </tr>
               <tr>
                  <td colspan="3">
                     <asp:Repeater ID="rptResult" runat="server">
                        <HeaderTemplate>
                           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table-inside">
                              <tr>
                                 <th>
                                    No.
                                 </th>
                                 <th>
                                    Nik
                                 </th>
                                 <th>
                                    Nama
                                 </th>
                                 <%--<th>Kd. Departemen</th>--%>
                                 <th>
                                    Departemen
                                 </th>
                                 <th>
                                    Jobsite
                                 </th>
                              </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                           <tr>
                              <td>
                                 <%# Container.ItemIndex + 1 %>
                              </td>
                              <td>
                                 <a href="#" onclick="UseSelected('<%#DataBinder.Eval(Container.DataItem, "Nik")%>','<%#DataBinder.Eval(Container.DataItem, "Nama")%>','<%#DataBinder.Eval(Container.DataItem, "NmDepar")%>','<%#DataBinder.Eval(Container.DataItem, "KdSite")%>');">
                                    <%#DataBinder.Eval(Container.DataItem, "Nik")%></a>
                              </td>
                              <td>
                                 <%#DataBinder.Eval(Container.DataItem, "Nama")%>
                              </td>
                              <%--<td align="center"><%#DataBinder.Eval(Container.DataItem, "KdDepar")%></td>--%>
                              <td>
                                 <%#DataBinder.Eval(Container.DataItem, "NmDepar")%>
                              </td>
                              <td align="center">
                                 <%#DataBinder.Eval(Container.DataItem, "KdSite")%>
                              </td>
                           </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                           </table>
                        </FooterTemplate>
                     </asp:Repeater>
                  </td>
               </tr>
            </table>
         </div>
      </ContentTemplate>
   </asp:UpdatePanel>
   <asp:UpdateProgress ID="progress1" runat="server" DisplayAfter="50">
      <ProgressTemplate>
         <asp:Panel ID="pnlProgressBar" runat="server">
            <div>
               <asp:Image ID="Image1" runat="Server" ImageUrl="../images/ajax-loader.gif" />Harap
               Sabar Menunggu...</div>
         </asp:Panel>
      </ProgressTemplate>
   </asp:UpdateProgress>
   <table style="width: 100%">
      <tr>
         <td>
            <b>Registered User</b>
         </td>
      </tr>
      <tr>
         <td>
            <asp:ListBox ID="lbRegistered" runat="Server" Style="width: 90%; align: left"></asp:ListBox>
            <img title="Delete Row" style="cursor: hand" onclick="delCheck()" src="../images/delete.gif" />
         </td>
      </tr>
      <tr>
         <td>
            <input type="button" id="btnSave" value="Save" onclick="save();" />
         </td>
      </tr>
   </table>
   </form>

   <script type="text/javascript">
      document.getElementById("SearchKey").focus();

      function delCheck() {
         var list = document.getElementById('lbRegistered');
         for (i = list.options.length - 1; i >= 0; i--) {
            if (list.options[i].selected)
               list.remove(i);
         }
      }

      function save() {
         var list = document.getElementById('lbRegistered');
         var nik = "";
         for (var i = 0; i < list.options.length; ++i)
            nik = nik + list.options[i].value + ";";
         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/DoSecureActivity",
            data: "{'id':" + inputFrm.elements['ctl00_ContentPlaceHolder1_hid'].value + ",'nik':" + JSON.stringify(nik) + "}",
            dataType: "json",
            success: function(res) {
               if (res.d == "0") {
                  if (nik == "") {
                     pw.changepic(inputFrm.elements['ctl00_ContentPlaceHolder1_imgid'].value, "0");
                  } else {
                     pw.changepic(inputFrm.elements['ctl00_ContentPlaceHolder1_imgid'].value, "1");
                  }
                  alert("Save Berhasil");
                  window.close();
               } else {
                  alert(res.d);
               }
            },
            error: function(err) {
               document.getElementById("divprogress").style.visibility = 'hidden';
               alert("error: " + err.responseText);
            }
         });
      }

      function retrieve() {
         var list = document.getElementById('lbRegistered');
         var nik = "";
         for (var i = 0; i < list.options.length; ++i)
            nik = nik + list.options[i].value + ";";

         var myForm = document.form1; //Form Name
         var mySel = myForm.lbRegistered; // Listbox Name
         var myOption;

         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/RetSecureActivity",
            data: "{'id':" + inputFrm.elements['ctl00_ContentPlaceHolder1_hid'].value + "}",
            dataType: "json",
            success: function(res) {
               for (var i = 0; i < res.d.length; i++) {
                  myOption = document.createElement("Option");
                  myOption.text = res.d[i].Name;
                  myOption.value = res.d[i].Nik;
                  mySel.add(myOption);
               }
            },
            error: function(err) {
               document.getElementById("divprogress").style.visibility = 'hidden';
               alert("error: " + err.responseText);
            }
         });
      }

      retrieve();
   </script>

</body>
</html>
