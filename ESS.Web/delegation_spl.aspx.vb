﻿Imports System.Data.SqlClient
Partial Public Class delegation_spl
    Inherits System.Web.UI.Page
    Public tglaw As String
    Public th As String
    Public v1 As String
    Public v2 As String
    Public imonth As Integer
    Public js As String
    Public msg As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("niksite") = Nothing Or Session("niksite") = "" Then Response.Redirect("login.aspx")

        Try
            If Not IsPostBack Then
                Dim a As String
                
                tglaw = Request(ddlbulan.UniqueID)
                th = Request(ddltahun.UniqueID)
                Dim i As Integer

                Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
                sqlConn.Open()
                Dim strcon As String = "select min(Tanggal) as min_date ,max(Tanggal) as max_date from H_H110"
                Dim dtb As DataTable = New DataTable
                Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
                sda.Fill(dtb)

                v1 = dtb.Rows(0)!min_date.ToString
                If v1.ToString = "" Then
                    v1 = Today.Year.ToString
                Else
                    v1 = CDate(v1).ToString("yyyy")
                End If

                v2 = dtb.Rows(0)!max_date.ToString
                If v2.ToString = "" Then
                    v2 = Today.Year.ToString
                Else
                    v2 = CDate(v2).ToString("yyyy")
                End If

                ddltahun.Items.Clear()
                For i = v1 To v2
                    ddltahun.Items.Add(i)
                Next

                ddltahun.SelectedValue = (i - 1).ToString

                Select Case tglaw
                    Case "January"
                        imonth = 1
                    Case "February"
                        imonth = 2
                    Case "March"
                        imonth = 3
                    Case "April"
                        imonth = 4
                    Case "May"
                        imonth = 5
                    Case "June"
                        imonth = 6
                    Case "July"
                        imonth = 7
                    Case "August"
                        imonth = 8
                    Case "September"
                        imonth = 9
                    Case "October"
                        imonth = 10
                    Case "November"
                        imonth = 11
                    Case "December"
                        imonth = 12
                    Case Else
                        imonth = 1
                End Select

                ddlbulan.SelectedValue = Today.Month
                ddldept.Visible = False
                lbldept.Visible = False

                'delegasi khusus GM
                If (Left((Session("kdlevel").ToString), 1) = "4") Then
                    Dim strdepar As String = "select top 1 '0' kddepar, 'All Department' nmdepar from h_A130 union select kddepar, nmdepar from h_A130 where KdDivisi = (select left(KdJabatan,2) from h_A101 where kdlevel like '%4%' and nik = '" + Session("niksite") + "')"
                    Dim dtbdepar As DataTable = New DataTable
                    Dim sdadepar As SqlDataAdapter = New SqlDataAdapter(strdepar, sqlConn)

                    ddldept.Visible = True
                    lbldept.Visible = True
                    sdadepar.Fill(dtbdepar)
                    ddldept.DataSource = dtbdepar
                    ddldept.DataValueField = "kddepar"
                    ddldept.DataTextField = "nmdepar"
                    ddldept.DataBind()
                End If

                Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
                Dim qrychkhr As String = "select 1 usr from V_A004 where module = 'DELOT' and nika = (select nik from h_A101 where niksite = '" + Session("otorisasi") + "')"
                Dim dtbchkhr As DataTable = New DataTable

                Dim sdas As SqlDataAdapter = New SqlDataAdapter(qrychkhr, conn)

                sdas.Fill(dtbchkhr)

                If dtbchkhr.Rows.Count > 0 Then
                    pdelhr.Visible = True
                Else
                    pdelhr.Visible = False
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsave.Click
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim sqlinsert As String
        Dim cmdinsert As SqlCommand
        Dim ss As String
        Dim skdlevel As String

        If txtnikreq.Text = "" Or txtnamareq.Text = "" Or txtdtfrom.Text = "" Or Txtdtto.Text = "" Then
            msg = "<script type='text/javascript'> alert('Please complete your data'); </script>"
            Return
        End If

        Try
            sqlConn.Open()

            'Dim strdepar As String = "select kdlevel from h_A101 where nik = '" + hnik.Value.ToString() + "'"
            'Dim dtbdepar As DataTable = New DataTable
            'Dim sdadepar As SqlDataAdapter = New SqlDataAdapter(strdepar, sqlConn)
            'sdadepar.Fill(dtbdepar)

            'If dtbdepar.Rows.Count > 0 Then
            '    skdlevel = dtbdepar.Rows(0)!kdlevel
            'End If

            If hid.Value = "" Then
                If hnik.Value.ToString() = "" Then
                    sqlinsert = "insert into H_H11002 (nikdel, date_from, date_to, nikreq, stedit, createdby) values ('" + hnikreq.Value + "', '" + txtdtfrom.Text + "', '" + Txtdtto.Text + "', '" + Session("niksite").ToString + "', '0', '" + Session("otorisasi").ToString() + "')"
                Else
                    If hkddepar.Value.ToString() = "" Then
                        sqlinsert = "insert into H_H11002 (nikdel, date_from, date_to, nikreq, stedit, createdby) values ('" + hnikreq.Value + "', '" + txtdtfrom.Text + "', '" + Txtdtto.Text + "', '" + hnik.Value.ToString() + "', '0', '" + Session("otorisasi").ToString() + "')"
                    Else
                        sqlinsert = "insert into H_H11002 (nikdel, date_from, date_to, nikreq, stedit, kddepar, createdby) values ('" + hnikreq.Value + "', '" + txtdtfrom.Text + "', '" + Txtdtto.Text + "', '" + hnik.Value.ToString() + "', '0', '" + hkddepar.Value.ToString() + "', '" + Session("otorisasi").ToString() + "')"
                    End If
                End If

                cmdinsert = New SqlCommand(sqlinsert, sqlConn)
                cmdinsert.ExecuteScalar()
                'recordid.Value = transno

                msg = "<script type='text/javascript'> alert('Data has been save'); </script>"
            Else
                If hnikreq.Value = "" Then
                Else
                    If hnik.Value.ToString() = "" Then
                        sqlinsert = "update H_H11002 set nikdel = '" + hnikreq.Value + "', date_from = '" + txtdtfrom.Text + "', date_to = '" + Txtdtto.Text + "', nikreq = '" + Session("niksite").ToString + "' where delegation_id = '" + hid.Value + "' "
                    Else
                        If hkddepar.Value.ToString() = "" Then
                            sqlinsert = "update H_H11002 set nikdel = '" + hnikreq.Value + "', date_from = '" + txtdtfrom.Text + "', date_to = '" + Txtdtto.Text + "', nikreq = '" + hnik.Value.ToString() + "' where delegation_id = '" + hid.Value + "' "
                        Else
                            sqlinsert = "update H_H11002 set nikdel = '" + hnikreq.Value + "', date_from = '" + txtdtfrom.Text + "', date_to = '" + Txtdtto.Text + "', nikreq = '" + hnik.Value.ToString() + "', kddepar = '" + hkddepar.Value.ToString() + "' where delegation_id = '" + hid.Value + "'"
                        End If

                    End If

                    cmdinsert = New SqlCommand(sqlinsert, sqlConn)
                    cmdinsert.ExecuteScalar()
                    'recordid.Value = transno
                End If

                msg = "<script type='text/javascript'> alert('Data has been save'); </script>"

            End If

            Dim strcon As String = "select delegation_id, (select nama from H_A101 where nik = H_H11002.nikdel) as nama, convert(varchar(20),date_from, 101) as date_from,  convert(varchar(20),date_to,101) as date_to, nikdel, nikreq, (select nama from H_A101 where nik = H_H11002.nikreq) as namareq, kddepar,(select nmdepar from h_A130 where KdDepar = h_H11002.kddepar) nmdepar, (select niksite from H_A101 where nik = H_H11002.nikdel) as niksite from H_H11002 where (month(date_from) = " + ddlbulan.SelectedValue + " or month(date_to) = " + ddlbulan.SelectedValue + ") and (nikreq = '" + Session("niksite").ToString + "' or createdby = '" + Session("otorisasi").ToString() + "') and stedit <> 2"
            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            GridView1.DataSource = dtb
            GridView1.DataBind()

            If hkddepar.Value.ToString() <> "" Then
                Dim Script As String = "document.getElementById('trh').style.visibility = 'visible'"
                txtdepar.Style.Item("visibility") = "visible"
                If Not Page.ClientScript.IsStartupScriptRegistered(Me.GetType(), "alertscript") Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "alertscript", Script, True)
                End If
            Else
                Dim Script As String = "document.getElementById('trh').style.visibility = 'hidden'"
                txtdepar.Style.Item("visibility") = "hidden"
                If Not Page.ClientScript.IsStartupScriptRegistered(Me.GetType(), "alertscript") Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "alertscript", Script, True)
                End If
            End If

        Catch ex As Exception

            ss = ex.Message.Replace("'", "")
            ss = ss.Replace(Environment.NewLine, "")
            msg = "<script type='text/javascript'> alert('Save Failed : " + ss + "'); </script>"

        Finally
            sqlConn.Close()
        End Try
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

        sqlConn.Open()
        Try
            Dim strcon As String = "select delegation_id, (select nama from H_A101 where nik = H_H11002.nikdel) as nama, convert(varchar(20),date_from, 101) as date_from,  convert(varchar(20),date_to,101) as date_to, nikdel, nikreq, (select nama from H_A101 where nik = H_H11002.nikreq) as namareq, kddepar,(select nmdepar from h_A130 where KdDepar = h_H11002.kddepar) nmdepar, (select niksite from H_A101 where nik = H_H11002.nikdel) as niksite from H_H11002 where (month(date_from) = " + ddlbulan.SelectedValue + " or month(date_to) = " + ddlbulan.SelectedValue + ") and (nikreq = '" + Session("niksite").ToString + "' or createdby = '" + Session("otorisasi").ToString() + "') and stedit <> 2"
            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            GridView1.DataSource = dtb
            GridView1.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged

        If GridView1.SelectedRow Is Nothing Then Exit Sub

        Dim row As GridViewRow = GridView1.SelectedRow

        'Session("splrecordid") = GridView1.DataKeys(row.RowIndex).Value.ToString
        hnikreq.Value = DirectCast(GridView1.SelectedRow.Cells(1).FindControl("HiddenField3"), HiddenField).Value
        txtnikreq.Text = DirectCast(GridView1.SelectedRow.Cells(1).FindControl("HiddenField4"), HiddenField).Value
        txtnamareq.Text = row.Cells(3).Text
        txtdtfrom.Text = row.Cells(4).Text
        Txtdtto.Text = row.Cells(5).Text
        hid.Value = GridView1.DataKeys(row.RowIndex).Value.ToString
        hkddepar.Value = DirectCast(GridView1.SelectedRow.Cells(9).FindControl("HiddenField1"), HiddenField).Value
        txtdepar.Text = row.Cells(6).Text
        txnamefrom.Text = row.Cells(7).Text
        hnik.Value = DirectCast(GridView1.SelectedRow.Cells(9).FindControl("HiddenField2"), HiddenField).Value

        If hkddepar.Value.ToString() <> "" Then
            Dim Script As String = "document.getElementById('trh').style.visibility = 'visible'"
            txtdepar.Style.Item("visibility") = "visible"
            If Not Page.ClientScript.IsStartupScriptRegistered(Me.GetType(), "alertscript") Then
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "alertscript", Script, True)
            End If
        Else
            Dim Script As String = "document.getElementById('trh').style.visibility = 'hidden'"
            txtdepar.Style.Item("visibility") = "hidden"
            If Not Page.ClientScript.IsStartupScriptRegistered(Me.GetType(), "alertscript") Then
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "alertscript", Script, True)
            End If
        End If
    End Sub

    Private Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim sqlinsert As String
        Dim cmdinsert As SqlCommand
        Try
            sqlConn.Open()
            sqlinsert = "update H_H11002 set stedit = '2' where delegation_id = '" + hid.Value + "' "
            cmdinsert = New SqlCommand(sqlinsert, sqlConn)
            cmdinsert.ExecuteScalar()

            'Dim strcon As String = "select delegation_id, (select nama from H_A101 where nik = H_H11002.nikdel) as nama, convert(varchar(20),date_from, 113) as date_from,  convert(varchar(20),date_to,113) as date_to, (select niksite from H_A101 where nik = H_H11002.nikdel) as niksite from H_H11002 where (month(date_from) = " + ddlbulan.SelectedValue + " or month(date_to) = " + ddlbulan.SelectedValue + ") and (nikreq = '" + Session("niksite").ToString + "' or createdby = '" + Session("otorisasi").ToString() + "') and stedit <> 2"
            Dim strcon As String = "select delegation_id, (select nama from H_A101 where nik = H_H11002.nikdel) as nama, convert(varchar(20),date_from, 101) as date_from,  convert(varchar(20),date_to,101) as date_to, nikdel, nikreq, (select nama from H_A101 where nik = H_H11002.nikreq) as namareq, kddepar,(select nmdepar from h_A130 where KdDepar = h_H11002.kddepar) nmdepar, (select niksite from H_A101 where nik = H_H11002.nikdel) as niksite from H_H11002 where (month(date_from) = " + ddlbulan.SelectedValue + " or month(date_to) = " + ddlbulan.SelectedValue + ") and (nikreq = '" + Session("niksite").ToString + "' or createdby = '" + Session("otorisasi").ToString() + "') and stedit <> 2"
            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            GridView1.DataSource = dtb
            GridView1.DataBind()

            msg = "<script type='text/javascript'> alert('Data has been deleted'); </script>"
        Catch ex As Exception

        Finally
            sqlConn.Close()
        End Try

    End Sub

    Protected Sub btnnew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnnew.Click
        Response.Redirect("delegation_spl.aspx")
    End Sub
End Class