﻿Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports System.IO
Imports System.Net.Mail

Partial Public Class PrintNotulen
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim id As String
            'id = Request.QueryString("id")

            If id = "NEW" Then
                '    Response.Redirect("SMeeting.aspx", False)
            ElseIf id IsNot Nothing Then
                Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
                Dim conn As New SqlConnection(arg)
                Dim ob1 As SqlDataAdapter = New SqlDataAdapter("SELECT [NmPimpinan], [NmNotulis] FROM HRAPAT WHERE IdRap = '" + id + "'", arg)
                Dim DTabel As New DataTable()
                ob1.Fill(DTabel)

                Dim scom As SqlCommand = New SqlCommand("sp_printnotulen", conn)
                scom.CommandType = CommandType.StoredProcedure
                Dim myParm As SqlParameter = scom.Parameters.Add("@noRapat", SqlDbType.VarChar, 30)
                myParm.Value = id
                conn.Open()
                scom.ExecuteNonQuery()

                Me.SqlDataSourceHR.SelectCommand = "SELECT [IdRap], [NmRap], [TmpRap], [WkRap], [SdWkRap], [TglRap], [Agenda], [Pimpinan], [NmPimpinan], [Notulis], [NmNotulis], [JenisRap], [Informasi], [MemberEx],[Kebijakan] FROM HRAPAT WHERE IdRap = '" + id + "'"
                'Me.SqlDataSourceDR.SelectCommand = "SELECT (Case When IdIssue=NoAction Then [NmIssue] Else '' End) as [NmIssue], [NoAction], [Action], [NmPIC], [DueDate], [StIssue] FROM DRAPAT WHERE IdRap = '" + id + "'"
                Me.SqlDataSourceDR.SelectCommand = "SELECT [NmIssue], [NoAction], [Action], [NmPIC], [DueDate], [StIssue] FROM DRAPAT WHERE IdRap = '" + id + "'"
                Me.SqlDataSourceDR.SelectCommand = "SELECT * FROM TEMPTABLE"
                Me.SqlDataSourceDP.SelectCommand = "SELECT [IdRap], [NikMember], [NmMember], [Departemen], [StHadir] FROM [DPESERTA] WHERE IdRap = '" + id + "'"

                Dim nbrs As Int32
                nbrs = DTabel.Rows.Count()

                Dim plist1 As New Generic.List(Of ReportParameter)
                Dim plist2 As New Generic.List(Of ReportParameter)
                Dim plist3 As New Generic.List(Of ReportParameter)
                Dim plist4 As New Generic.List(Of ReportParameter)

                Dim param1 As ReportParameter
                Dim param2 As ReportParameter
                Dim param3 As ReportParameter
                Dim param4 As ReportParameter

                param1 = New ReportParameter("prm_Notulis", DTabel.Rows(0)("NmNotulis").ToString)
                plist1.Add(param1)
                param2 = New ReportParameter("prm_Pimpinan", DTabel.Rows(0)("NmPimpinan").ToString)
                plist2.Add(param2)

                ' Peserta Rapat Hadir
                Dim ob2 As SqlDataAdapter = New SqlDataAdapter("SELECT [NmMember] FROM [DPESERTA] WHERE IdRap = '" + id + "' and StHadir = '0'", arg)
                Dim DTabel2 As New DataTable()
                ob2.Fill(DTabel2)

                Dim nbrs2 As Int32
                nbrs2 = DTabel2.Rows.Count()

                Dim cp As String = ""
                If nbrs2 = 0 Then
                    cp = ""
                Else
                    For i = 0 To nbrs2 - 1
                        If i = 0 Then
                            cp = cp + DTabel2.Rows(i)("NmMember").ToString
                        Else
                            cp = cp + ", " + DTabel2.Rows(i)("NmMember").ToString
                        End If
                    Next
                End If

                param3 = New ReportParameter("prm_Peserta", cp)
                plist3.Add(param3)

                ' Peserta Rapat Hadir
                Dim ob3 As SqlDataAdapter = New SqlDataAdapter("SELECT [NmMember] FROM [DPESERTA] WHERE IdRap = '" + id + "' and StHadir = '1'", arg)
                Dim DTabel3 As New DataTable()
                ob3.Fill(DTabel3)

                Dim nbrs3 As Int32
                nbrs3 = DTabel3.Rows.Count()

                Dim cpa As String = ""
                If nbrs3 = 0 Then
                    cpa = ""
                Else
                    For i = 0 To nbrs3 - 1
                        If i = 0 Then
                            cpa = cpa + DTabel3.Rows(i)("NmMember").ToString
                        Else
                            cpa = cpa + ", " + DTabel3.Rows(i)("NmMember").ToString
                        End If
                    Next
                End If

                param4 = New ReportParameter("prm_Absen", cpa)
                plist4.Add(param4)

                ReportViewer.LocalReport.SetParameters(plist1)
                ReportViewer.LocalReport.SetParameters(plist2)
                ReportViewer.LocalReport.SetParameters(plist3)
                ReportViewer.LocalReport.SetParameters(plist4)
                ReportViewer.ShowPrintButton = True

                conn.Close()
                conn.Dispose()
            Else
                'Response.Redirect("SMeeting.aspx", False)
            End If

        Catch ex As Exception
            Response.Write(ex.Message + ex.StackTrace)
        End Try
    End Sub

    Function SendMail(ByVal rv As ReportViewer)
        Try
            Dim id As String
            id = Request.QueryString("id")
            Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
            Dim conn As New SqlConnection(arg)
            Dim cmd As New SqlCommand("select isnull(mail,0) as mail from hrapat where idrap='" & id & "'", conn)
            Dim result As String
            'Dim smtp As New SmtpClient()
            conn.Open()
            result = cmd.ExecuteScalar()
            conn.Close()
            If result = 0 Then
                Dim bytes As Byte()
                Dim warnings As Warning()
                Dim streamids As String()
                Dim mimeType As String
                Dim encoding As String
                Dim extension As String

                Dim squery As String
                squery = "select * from "
                squery = squery + "( select IdRap,(Case When IdIssue=NoAction Then NmIssue Else (select NmIssue from drapat dd where dd.idrap=a.idrap and dd.idissue=a.idissue and dd.noaction=a.idissue) end) NmIssue,Action,DueDate,NikPIC,eMail,'PIC' as Jenis,NmInfo  from drapat a left join mail b on a.nikpic = b.nik"
                squery = squery + " where idRap = '" & id & "' union "
                squery = squery + " select IdRap,(Case When IdIssue=NoAction Then NmIssue Else (select NmIssue from drapat dd where dd.idrap=a.idrap and dd.idissue=a.idissue and dd.noaction=a.idissue) end) NmIssue,Action,DueDate,Nikinfo,eMail,'Info' as Jenis,NmPIC  from drapat a "
                squery = squery + " left join mail b on a.Nikinfo = b.nik where idRap = '" & id & "' "
                squery = squery + " )b where (nikpic <> '' and nikpic is not null)  and email is not null order by nikpic, jenis desc, duedate asc "

                Dim dt As New DataTable()
                Dim da As New SqlDataAdapter(squery, conn)
                conn.Open()
                da.Fill(dt)
                conn.Close()


                'Add penambahan automail peserta MOM''''''''''''''''''''''''''''''''''''''''''
                Dim squeryP As String
                squeryP = "SELECT  a.nikMember, b.email ,'LIST' as jenis, StHadir  FROM  DPESERTA a left join Mail b on a.nikMember = b.nik WHERE IdRap = '" & id & "'"
                Dim dtP As New DataTable()
                Dim daP As New SqlDataAdapter(squeryP, conn)
                conn.Open()
                daP.Fill(dtP)
                conn.Close()
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                Dim message As New MailMessage()
                Dim tampung As String = ""
                Dim mail As String = ""
                Dim jenis As String = ""
                Dim counter As Integer = 0
                Dim jmlrow As Integer = 0
                Dim f As Boolean
                For Each dr In dt.Rows
                    jmlrow += 1
                    'MsgBox(tampung)
                    If tampung = "" Then
                        message = New MailMessage()

                        message.From = New MailAddress("memo@jresources.com", "MEMO")
                        'message.Subject = "My Issue according to Minutes of Meeting No. " & id
                        message.Subject = "Minutes Of Meeting No : " & id
                        message.IsBodyHtml = True
                        tampung = dr("NikPIC")
                        mail = dr("eMail")
                        message.Body = "Dear,<br>Good Day!<br><br>"
                        If dr(6).ToString() = "PIC" Then
                            If counter = 0 Then
                                message.Body = message.Body + "As reminder, following are list of my issue needed action and follow up:<br><br>"
                                message.Body = message.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:30%'>Issue</th><th style='width:60%'>Action</th><th style='width:5%'>Due To</th>"
                                counter += 1
                            End If
                            message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" & dr(1).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(2).ToString() & "</td><td style='text-align:center;border:solid 1px'>" & Format(dr(3), "dd/MM/yyyy") & "</td></tr>"
                            f = True
                            jenis = "PIC"
                        ElseIf jenis = "" Then
                            If counter = 0 Then
                                message.Body = message.Body + "You get information as follow, <br><br>"
                            Else
                                message.Body = message.Body + "</table><br><br>and You get information as follow, <br><br>"
                            End If
                            message.Body = message.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:20%'>Information From</th><th style='width:20%'>Isu</th><th style='width:50%'>Action</th><th style='width:5%'>Due To</th>"
                            message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" & dr(7).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(1).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(2).ToString() & "</td><td style='text-align:center;border:solid 1px'>" & Format(dr(3), "dd/MM/yyyy") & "</td></tr>"
                            f = False
                        Else
                            If f Then
                                message.Body = message.Body + "</table><br><br>You get information as follow, <br><br>"
                                message.Body = message.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:20%'>Information From</th><th style='width:20%'>Isu</th><th style='width:50%'>Action</th><th style='width:5%'>Due To</th>"
                            End If
                            message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" & dr(7).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(1).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(2).ToString() & "</td><td style='text-align:center;border:solid 1px'>" & Format(dr(3), "dd/MM/yyyy") & "</td></tr>"
                            f = False
                        End If
                        counter += 1
                        If jmlrow = dt.Rows.Count Then
                            'start gabung email dengan atachment
                            Dim warn As Warning() = Nothing
                            Dim byteviewer() As Byte
                            Dim deviceinfo As String = "<DeviceInfo><OutputFormat>PDF</OutputFormat><PageWidth>8.5in</PageWidth><PageHeight>11in</PageHeight><MarginTop>0in</MarginTop><MarginLeft>0in</MarginLeft><MarginRight>0in</MarginRight><MarginBottom>0in</MarginBottom></DeviceInfo>"

                            byteviewer = rv.LocalReport.Render("PDF", deviceinfo, mimeType, encoding, extension, streamids, warn)
                            Response.Buffer = True

                            Dim ms As MemoryStream = New MemoryStream(byteviewer)
                            ms.Write(byteviewer, 0, byteviewer.Length)
                            ms.Seek(0, SeekOrigin.Begin)

                            'message.Body = message.Body + "Dear,<br>Good day Is A Beutiful Day! 4<br><br>"

                            message.Body = message.Body + "<br>Following attachment is the summary for Minutes of Meeting .<br>"
                            message.Body = message.Body + "<br><br> Please visit link below: http://moss.jresources.com:8080/memo/Login.aspx, for detailed information and further action.<br><Br>Thank You.<Br>"
                            'message.Body = message.Body + "</table> <br><br> Please visit link below: http://moss.jresources.com:8080/memo/Login.aspx, for detailed information.<br>"

                            message.Attachments.Add(New Attachment(ms, "MinutesMeeting-" & id & ".pdf"))

                            'end gabung email

                            'message.Body = message.Body + "</table> <br><br> Please visit link below: http://moss.jresources.com:8080/memo/Login.aspx, for detailed information and further action.<br><Br>Thank You."
                            message.To.Add(mail)
                            'message.Bcc.Add("ricky.tudjuka@bukitmakmur.com")
                            'Dim smtp As New SmtpClient("192.168.100.15")
                            'Dim smtp As New SmtpClient("172.16.0.40")
                            'Dim smtp As New SmtpClient("smtp.scbd.net.id")

                            'smtp.Host = "smtp.scbd.net.id"

                            'smtpClient.Credentials = new System.Net.CredentialCache NetworkCredential(strFrom, strPass);
                            'smtp.Credentials = CredentialCache.DefaultNetworkCredentials

                            ' ///SmtpClient smtp = new SmtpClient();
                            '//smtpClient.Host = "smtp.gmail.com";
                            '//smtpClient.Port = 587;
                            '//smtpClient.EnableSsl = true;
                            '//smtpClient.Credentials = new System.Net.NetworkCredential(strFrom, strPass

                            Dim smtp As New SmtpClient()
                            smtp.Host = "smtp.gmail.com"
                            smtp.Port = 587
                            smtp.EnableSsl = True
                            smtp.Credentials = New System.Net.NetworkCredential("memo@jresources.com", "jrec54321")

                            smtp.Send(message)
                            ms.Close()
                        End If


                    ElseIf tampung = dr("NikPIC") And counter <> dt.Rows.Count Then
                        'MsgBox(tampung)
                        If dr(6).ToString() = "PIC" Then
                            message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" & dr(1).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(2).ToString() & "</td><td style='text-align:center;border:solid 1px'>" & Format(dr(3), "dd/MM/yyyy") & "</td></tr>"
                            f = True
                        Else
                            If f Then
                                message.Body = message.Body + "</table><br><br>You get information as follow, <br><br>"
                                message.Body = message.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:20%'>Information From</th><th style='width:20%'>Issue</th><th style='width:50%'>Action</th><th style='width:5%'>Due To</th>"
                            End If
                            message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" & dr(7).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(1).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(2).ToString() & "</td><td style='text-align:center;border:solid 1px'>" & Format(dr(3), "dd/MM/yyyy") & "</td></tr>"
                            f = False
                        End If
                        counter += 1
                        If jmlrow = dt.Rows.Count Then

                            'start gabung email dengan atachment

                            Dim warn As Warning() = Nothing
                            Dim byteviewer() As Byte
                            Dim deviceinfo As String = "<DeviceInfo><OutputFormat>PDF</OutputFormat><PageWidth>8.5in</PageWidth><PageHeight>11in</PageHeight><MarginTop>0in</MarginTop><MarginLeft>0in</MarginLeft><MarginRight>0in</MarginRight><MarginBottom>0in</MarginBottom></DeviceInfo>"

                            byteviewer = rv.LocalReport.Render("PDF", deviceinfo, mimeType, encoding, extension, streamids, warn)
                            Response.Buffer = True

                            Dim ms As MemoryStream = New MemoryStream(byteviewer)
                            ms.Write(byteviewer, 0, byteviewer.Length)
                            ms.Seek(0, SeekOrigin.Begin)

                            'message.Body = message.Body + "Dear,<br>Good day Is A Beutiful Day! 4<br><br>"

                            message.Body = message.Body + "<br>Following attachment is the summary for Minutes of Meeting .<br>"
                            message.Body = message.Body + "<br><br> Please visit link below: http://moss.jresources.com:8080/memo/Login.aspx, for detailed information and further action.<br><Br>Thank You.<Br>"
                            'message.Body = message.Body + "</table> <br><br> Please visit link below: http://moss.jresources.com:8080/memo/Login.aspx, for detailed information.<br>"

                            message.Attachments.Add(New Attachment(ms, "MinutesMeeting-" & id & ".pdf"))

                            'end gabung email

                            'message.Body = message.Body + "</table> <br><br> Please visit link below: http://moss.jresources.com:8080/memo/Login.aspx, for detailed information and further action.<br><Br>Thank You."
                            message.To.Add(mail)

                            Dim smtp As New SmtpClient()
                            smtp.Host = "smtp.gmail.com"
                            smtp.Port = 587
                            smtp.EnableSsl = True
                            smtp.Credentials = New System.Net.NetworkCredential("memo@jresources.com", "jrec54321")

                            smtp.Send(message)
                            ms.Close()
                        End If
                    ElseIf tampung = dr("NikPIC") And counter = dt.Rows.Count Then
                        'MsgBox(tampung)
                        If dr(6).ToString() = "PIC" Then
                            message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" & dr(1).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(2).ToString() & "</td><td style='text-align:center;border:solid 1px'>" & Format(dr(3), "dd/MM/yyyy") & "</td></tr>"
                            f = True
                        Else
                            If f Then
                                message.Body = message.Body + "</table><br><br>You get information as follow, <br><br>"
                                message.Body = message.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:20%'>Information From</th><th style='width:20%'>Issue</th><th style='width:50%'>Action</th><th style='width:5%'>Due To</th>"
                            End If
                            message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" & dr(7).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(1).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(2).ToString() & "</td><td style='text-align:center;border:solid 1px'>" & Format(dr(3), "dd/MM/yyyy") & "</td></tr></table>"
                            f = False
                        End If

                        message.To.Add(dr(5).ToString())
                        tampung = ""
                        jenis = ""
                        counter = 0

                        'start gabung email dengan atachment
                        Dim warn As Warning() = Nothing
                        Dim byteviewer() As Byte
                        Dim deviceinfo As String = "<DeviceInfo><OutputFormat>PDF</OutputFormat><PageWidth>8.5in</PageWidth><PageHeight>11in</PageHeight><MarginTop>0in</MarginTop><MarginLeft>0in</MarginLeft><MarginRight>0in</MarginRight><MarginBottom>0in</MarginBottom></DeviceInfo>"

                        byteviewer = rv.LocalReport.Render("PDF", deviceinfo, mimeType, encoding, extension, streamids, warn)
                        Response.Buffer = True

                        Dim ms As MemoryStream = New MemoryStream(byteviewer)
                        ms.Write(byteviewer, 0, byteviewer.Length)
                        ms.Seek(0, SeekOrigin.Begin)

                        'message.Body = message.Body + "Dear,<br>Good day Is A Beutiful Day! 4<br><br>"

                        message.Body = message.Body + "<br>Following attachment is the summary for Minutes of Meeting .<br>"
                        message.Body = message.Body + "<br><br> Please visit link below: http://moss.jresources.com:8080/memo/Login.aspx, for detailed information and further action.<br><Br>Thank You.<Br>"
                        'message.Body = message.Body + "</table> <br><br> Please visit link below: http://moss.jresources.com:8080/memo/Login.aspx, for detailed information.<br>"

                        message.Attachments.Add(New Attachment(ms, "MinutesMeeting-" & id & ".pdf"))

                        'end gabung email

                        Dim smtp As New SmtpClient()
                        smtp.Host = "smtp.gmail.com"
                        smtp.Port = 587
                        smtp.EnableSsl = True
                        smtp.Credentials = New System.Net.NetworkCredential("memo@jresources.com", "jrec54321")
                        smtp.Send(message)
                        ms.Close()

                        cmd = New SqlCommand("update hrapat set mail='1' where idrap='" & id & "'", conn)
                        conn.Open()
                        result = cmd.ExecuteScalar()
                        conn.Close()

                    ElseIf tampung <> dr("NikPIC") And counter = dt.Rows.Count Then
                        'MsgBox(tampung)
                        message.Body = message.Body + "</table> <br><br> Please visit link below: http://moss.jresources.com:8080/memo/Login.aspx, for detailed information and further action.<br><Br>Thank You."
                        message.To.Add(mail)

                        Dim smtp As New SmtpClient("smtp.scbd.net.id")
                        smtp.Send(message)
                        jenis = ""
                        counter = 0
                        message = New MailMessage()

                        message.From = New MailAddress("memo@jresources.com", "MEMO")
                        'message.Subject = "My Issue according to Minutes of Meeting No. " & id
                        message.Subject = "Minutes Of Meeting No : " & id
                        message.IsBodyHtml = True
                        tampung = dr("NikPIC")
                        mail = dr("eMail")
                        message.Body = "Dear,<br>Good Day!<br><br>"
                        If dr(6).ToString() = "PIC" Then
                            If counter = 0 Then
                                message.Body = message.Body + "As reminder, following are list of my issue needed action and follow up:<br><br>"
                                message.Body = message.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:30%'>Issue</th><th style='width:60%'>Action</th><th style='width:5%'>Due To</th>"
                                counter += 1
                            End If
                            message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" & dr(1).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(2).ToString() & "</td><td style='text-align:center;border:solid 1px'>" & Format(dr(3), "dd/MM/yyyy") & "</td></tr>"
                            f = True
                            jenis = "PIC"
                        ElseIf jenis = "" Then
                            If counter = 0 Then
                                message.Body = message.Body + "You get information as follow, <br><br>"
                            Else
                                message.Body = message.Body + "</table><br><br>and You get information as follow, <br><br>"
                            End If
                            message.Body = message.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:20%'>Information From</th><th style='width:20%'>Issue</th><th style='width:50%'>Action</th><th style='width:5%'>Due To</th>"
                            message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" & dr(7).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(1).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(2).ToString() & "</td><td style='text-align:center;border:solid 1px'>" & Format(dr(3), "dd/MM/yyyy") & "</td></tr>"
                            f = False
                        Else
                            If f Then
                                message.Body = message.Body + "</table><br><br>You get information as follow, <br><br>"
                                message.Body = message.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:20%'>Information From</th><th style='width:20%'>Issue</th><th style='width:50%'>Action</th><th style='width:5%'>Due To</th>"
                            End If
                            message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" & dr(7).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(1).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(2).ToString() & "</td><td style='text-align:center;border:solid 1px'>" & Format(dr(3), "dd/MM/yyyy") & "</td></tr>"
                            f = False
                        End If

                        'start gabung email dengan atachment

                        Dim warn As Warning() = Nothing
                        Dim byteviewer() As Byte
                        Dim deviceinfo As String = "<DeviceInfo><OutputFormat>PDF</OutputFormat><PageWidth>8.5in</PageWidth><PageHeight>11in</PageHeight><MarginTop>0in</MarginTop><MarginLeft>0in</MarginLeft><MarginRight>0in</MarginRight><MarginBottom>0in</MarginBottom></DeviceInfo>"

                        byteviewer = rv.LocalReport.Render("PDF", deviceinfo, mimeType, encoding, extension, streamids, warn)
                        Response.Buffer = True

                        Dim ms As MemoryStream = New MemoryStream(byteviewer)
                        ms.Write(byteviewer, 0, byteviewer.Length)
                        ms.Seek(0, SeekOrigin.Begin)

                        'message.Body = message.Body + "Dear,<br>Good day Is A Beutiful Day! 4<br><br>"

                        message.Body = message.Body + "<br>Following attachment is the summary for Minutes of Meeting .<br>"
                        message.Body = message.Body + "<br><br> Please visit link below: http://moss.jresources.com:8080/memo/Login.aspx, for detailed information and further action.<br><Br>Thank You.<Br>"
                        'message.Body = message.Body + "</table> <br><br> Please visit link below: http://moss.jresources.com:8080/memo/Login.aspx, for detailed information.<br>"

                        message.Attachments.Add(New Attachment(ms, "MinutesMeeting-" & id & ".pdf"))

                        'end gabung email

                        'message.Body = message.Body + "</table> <br><br> Please visit link below: http://moss.jresources.com:8080/memo/Login.aspx, for detailed information and further action.<br><Br>Thank You."
                        message.To.Add(mail)

                        smtp = New SmtpClient()
                        smtp.Host = "smtp.gmail.com"
                        smtp.Port = 587
                        smtp.EnableSsl = True
                        smtp.Credentials = New System.Net.NetworkCredential("memo@jresources.com", "jrec54321")
                        smtp.Send(message)
                        ms.Close()
                    Else
                        message.Body = message.Body + "</table> <br><br> Please visit link below: http://moss.jresources.com:8080/memo/Login.aspx, for detailed information and further action.<br><Br>Thank You."
                        message.To.Add(mail)

                        Dim smtp As New SmtpClient()
                        smtp.Host = "smtp.gmail.com"
                        smtp.Port = 587
                        smtp.EnableSsl = True
                        smtp.Credentials = New System.Net.NetworkCredential("memo@jresources.com", "jrec54321")

                        smtp.Send(message)
                        jenis = ""
                        counter = 0
                        tampung = dr("NikPIC")
                        mail = dr("eMail")
                        message = New MailMessage()

                        message.From = New MailAddress("memo@jresources.com", "MEMO")
                        'message.Subject = "My Issue according to Minutes of Meeting No. " & id
                        message.Subject = "Minutes Of Meeting No : " & id
                        message.IsBodyHtml = True
                        message.Body = "<br>Dear,<br>Good Day!<br><br>"
                        If dr(6).ToString() = "PIC" Then
                            If counter = 0 Then
                                message.Body = message.Body + "As reminder, following are list of my issue needed action and follow up:<br><br>"
                                message.Body = message.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:30%'>Issue</th><th style='width:60%'>Action</th><th style='width:5%'>Due To</th>"
                                counter += 1
                            End If
                            message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" & dr(1).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(2).ToString() & "</td><td style='text-align:center;border:solid 1px'>" & Format(dr(3), "dd/MM/yyyy") & "</td></tr>"
                            f = True
                            jenis = "PIC"
                        ElseIf jenis = "" Then
                            If counter = 0 Then
                                message.Body = message.Body + "You get information as follow, <br><br>"
                            Else
                                message.Body = message.Body + "</table><br><br>and You get information as follow, <br><br>"
                            End If
                            message.Body = message.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:20%'>Information From</th><th style='width:20%'>Issue</th><th style='width:50%'>Action</th><th style='width:5%'>Due To</th>"
                            message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" & dr(7).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(1).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(2).ToString() & "</td><td style='text-align:center;border:solid 1px'>" & Format(dr(3), "dd/MM/yyyy") & "</td></tr>"
                            f = False
                        Else
                            If f Then
                                message.Body = message.Body + "</table><br><br>You get information as follow, <br><br>"
                                message.Body = message.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:20%'>Information From</th><th style='width:20%'>Issue</th><th style='width:50%'>Action</th><th style='width:5%'>Due To</th>"
                            End If
                            message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" & dr(7).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(1).ToString() & "</td><td style='text-align:left;border:solid 1px'>" & dr(2).ToString() & "</td><td style='text-align:center;border:solid 1px'>" & Format(dr(3), "dd/MM/yyyy") & "</td></tr>"
                            f = False
                        End If
                        counter += 1
                        If jmlrow = dt.Rows.Count Then
                            message.Body = message.Body + "</table> <br><br> Please visit link below: http://moss.jresources.com:8080/memo/Login.aspx, for detailed information and further action.<br><Br>Thank You."
                            message.To.Add(mail)

                            'Dim smtp = New SmtpClient()
                            smtp.Host = "smtp.gmail.com"
                            smtp.Port = 587
                            smtp.EnableSsl = True
                            smtp.Credentials = New System.Net.NetworkCredential("memo@jresources.com", "jrec54321")
                            smtp.Send(message)
                        End If
                    End If
                Next

                Dim warn2 As Warning() = Nothing
                Dim byteviewer2() As Byte
                Dim deviceinfo2 As String = "<DeviceInfo><OutputFormat>PDF</OutputFormat><PageWidth>8.5in</PageWidth><PageHeight>11in</PageHeight><MarginTop>0in</MarginTop><MarginLeft>0in</MarginLeft><MarginRight>0in</MarginRight><MarginBottom>0in</MarginBottom></DeviceInfo>"

                byteviewer2 = rv.LocalReport.Render("PDF", deviceinfo2, mimeType, encoding, extension, streamids, warn2)
                Response.Buffer = True

                Dim ms2 As MemoryStream = New MemoryStream(byteviewer2)
                ms2.Write(byteviewer2, 0, byteviewer2.Length)
                ms2.Seek(0, SeekOrigin.Begin)

                '===Saving locally first===================================================
                'Dim lfFile As File
                'If lfFile.Exists("c:\temp\myReport.pdf") Then
                'lfFile.Delete("c:\temp\myReport.pdf")
                'End If

                'Dim fs As New FileStream("c:\temp\myReport.pdf", FileMode.Create)
                'fs.Write(byteviewer, 0, byteviewer.Length)
                'fs.Close()

                'message = New MailMessage()
                'rv.LocalReport.Render()
                'message.Attachments()

                'message.Attachments("d:\test.pdf")

                'fs.Seek(0, SeekOrigin.Begin)
                'message.Attachments.Add(New Attachment(fs, "Notulen.pdf"))
                '==========================================================================

                message.From = New MailAddress("memo@jresources.com", "MEMO")
                message.Subject = "Minutes Of Meeting No : " & id
                message.IsBodyHtml = True

                message.Body = "Dear,<br><br><br>"

                message.Body = message.Body + "Following attachment is the summary for Minutes of Meeting .<br><br>"
                message.Body = message.Body + "</table> <br><br> Please visit link below: http://moss.jresources.com:8080/memo/Login.aspx, for detailed information.<br><Br>Thank you.."

                For Each drp In dtP.Rows
                    mail = drp("eMail")
                    message.To.Add(mail)
                Next

                message.Attachments.Add(New Attachment(ms2, "MinutesMeeting-" & id & ".pdf"))

                Dim smtpx As New SmtpClient()
                smtpx.Host = "smtp.gmail.com"
                smtpx.Port = 587
                smtpx.EnableSsl = True
                smtpx.Credentials = New System.Net.NetworkCredential("memo@jresources.com", "jrec54321")

                smtpx.Send(message)
                ms2.Close()

                cmd = New SqlCommand("update hrapat set mail='1' where idrap='" & id & "'", conn)
                conn.Open()
                result = cmd.ExecuteScalar()
                conn.Close()

                Response.Redirect("SMeeting.aspx")

                'cmd = New SqlCommand("update hrapat set mail='1' where idrap='" & id & "'", conn)
                'conn.Open()
                'result = cmd.ExecuteScalar()
                'conn.Close()

                'Response.Redirect("SMeeting.aspx")
            End If
        Catch ex As Exception
            Response.Write(ex.Message + ex.StackTrace)
        Finally
            Response.Redirect("SMeeting.aspx")
        End Try
    End Function

    Function GetMailList(ByVal idRap As String) As DataTable
        Try
            Dim s As String
            Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
            Dim conn As New SqlConnection(arg)
            Dim dt As New DataTable()
            s = "select nik,email from "
            s = s + "(select nikpic from drapat where idrap='" + idRap + "'"
            s = s + "union select nikinfo from drapat where idrap='" + idRap + "' )d left join MAIL m "
            s = s + "on d.nikpic = m.nik where nik is not null "
            Dim cmd As New SqlCommand(s, conn)
            Dim da As New SqlDataAdapter(s, conn)
            conn.Open()
            da.Fill(dt)
            conn.Close()
            Return dt
        Catch ex As Exception
            Response.Write(ex.Message + ex.StackTrace)
        End Try
    End Function

    Private Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
        Dim m As String
        m = Request.QueryString("m")
        If m IsNot Nothing Then
            ReportViewer.Visible = False
        End If
        If m = "t" Then
            SendMail(ReportViewer)
        End If
    End Sub
End Class
