﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="emp_permit.aspx.vb" Inherits="EXCELLENT.emp_permit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Employee Permit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script src="Scripts/jsm01.js" type="text/javascript"></script>
    <script src="Scripts/tcal.js" type="text/javascript"></script>
    <script src="Scripts/jquery.min.js" type="text/javascript" ></script>
    <link rel="stylesheet" href="css/tcal.css" type="text/css" />
    <script src="Scripts/jscal2.js" type ="text/javascript"></script>
    <script src="Scripts/en.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="css/gold/gold.css" />
    
    <link rel="stylesheet" type="text/css" href="css/jquery.timepicker.min.css"> 
    <script type="text/javascript" src="Scripts/jquery.plugin.js"></script> 
    <script type="text/javascript" src="Scripts/jquery.timeentry.js"></script>
    
    <style>
        #table-form tbody tr td 
        {
        	padding-left: 50px;
        }
        
        input[readonly] { 
            background: #CCC; 
            color: #333; 
            border: 1px solid #a9a9a9 
        }
        .style4
        {
            height: 70px;
        }
    .style5
    {
        height: 22px;
    }
        .style6
        {
            height: 28px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <table id="table-form" width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
            <caption style="text-align: center; font-size: 1.5em; color:White;">
                Employee Permit
            </caption>
            <tbody>
                <tr>
                    <td colspan="2"><br /></td>
                </tr>
                <tr>
                    <td style="width: 15%;">
                        Trans. ID
                    </td>
                    <td>
                        <asp:TextBox ID="TransID" runat="server" readonly></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Site
                    </td>
                    <td>
                        <asp:TextBox ID="site" runat="server" readonly style="width:260px;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Employee Number
                    </td>
                    <td>
                        <asp:TextBox ID="niksite" runat="server" readonly style="width:150px;"></asp:TextBox>
                        <asp:HiddenField ID="nik" runat = "server" /> 
                        <img alt="add" id="Img2" src="images/Search.gif" align="absmiddle" style="cursor: pointer;" onclick="OpenPopup('permit')" /> 
                    </td>
                </tr>
                <tr>
                    <td class="style5">
                        Employee Name
                    </td>
                    <td class="style5">
                        <asp:TextBox ID="nama" runat="server" readonly style="width:260px;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style6">
                        Date Permit
                    </td>
                    <td class="style6">
                        <asp:TextBox ID="txtDateTime" runat="server" readonly style="width:132px;"></asp:TextBox>
                        <button id="btDate" >...</button> 
                        
                        <asp:TextBox ID="txtTimePermit" class="txtTimePermit" runat="server" style="width:70px;" placeholder="HH:MM" Enabled="false" ></asp:TextBox>
                        
                        <asp:Label ID="lblTo" runat="server" Text=" To " Visible="false" />
                        
                        <asp:TextBox ID="txtDateTimeTo" runat="server" readonly style="width:132px;" Visible="false" />
                        <asp:Button runat="server" UseSubmitBehavior="false" onclientclick="Javascript:return false;" ID="btDateTo" Text="..." Visible="false" />
                        
                        
                    </td>
                </tr>
                <tr>
                    <td class="style5">
                        Type
                    </td>
                    <td class="style5">
                        <asp:DropDownList ID="TypePermit" runat="server" OnSelectedIndexChanged="TypePermit_SelectedIndexChanged" AutoPostBack="true" >
                        </asp:DropDownList>
                                               
                        <asp:DropDownList ID="TypePermitSub" runat="server" Visible="false"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Total Days
                    </td>
                    <td>
                        <asp:TextBox ID="txtTotalDays" runat="server" readonly style="width:50px; font-size: 1.0em"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style4">
                        Remarks
                    </td>
                    <td class="style4">
                        <asp:TextBox ID="txtRemarks" runat="server" Rows="4" TextMode="MultiLine" Width="270px" MaxLength="100"></asp:TextBox>
                    </td>
                </tr>
                <tr style="display:none;">
                    <td class="style4">
                        Suhu Tubuh
                    </td>
                    <td class="style4">
                        <asp:TextBox ID="txtSuhuTubuh" runat="server" Width=40 style="width:40px;></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        File Upload
                    </td>
                    <td>
                        <asp:FileUpload ID="FileUpload1" runat="server" visible="false" />          
                        <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="UploadFile" visible="false" />                        
                    </td>
                </tr>
                
                <tr>
                    <td></td>
                    <td>
                        <asp:Label ID="lblInfoUpload" runat="server" Text="save the document first"></asp:Label>
                        <br />
                        <asp:GridView ID="GridView1" Visible="false" runat="server" AutoGenerateColumns="false" EmptyDataText = "No files uploaded">
                            <Columns>
                                <asp:BoundField DataField="Text" HeaderText="File Name" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDownload" Text = "Download" CommandArgument = '<%# Eval("Value") %>' runat="server" OnClick = "DownloadFile"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID = "lnkDelete" Text = "Delete" CommandArgument = '<%# Eval("Value") %>' runat = "server" OnClick = "DeleteFile" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>


                    </td>
                </tr>
                
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnSave" Visible=true Text="Save" runat="server" 
                            style="width: 80px; height:20px; font-family: Calibri;"/>
                        
                        <asp:Button ID="btnEdit" Visible=false Text="Save & Edit" runat="server" 
                            style="width: 80px; height:20px; font-family: Calibri;"/>
                        
                        <asp:Button ID="btnPrint" Visible=false Text="Print" runat="server" 
                            style="width: 80px; font-family: Calibri;"/>
                            
                        <asp:Button ID="btnSubmit" Visible=false Text="Submit" runat="server" Enabled="false"
                            style="width: 80px; height:20px; font-family: Calibri;"/>
                        
                        <asp:Button ID="btnDelete" Visible=false Text="Delete" runat="server" 
                            style="width: 80px; font-family: Calibri;"/>

                    </td>
                </tr>
                
            </tbody>
        </table>
        
        <input type="hidden" id="ctrlToFind" /> 
        <input type="hidden" id="ctnm" />
        <input type="hidden" id = "jml" value = "1" />
        <asp:HiddenField ID="status" runat = "server" /> 
        <asp:HiddenField ID="cnt" runat = "server" /> 
        <asp:HiddenField ID="cnt2" runat = "server" />
        
<script type ="text/javascript" >


    function OpenPopup(key) {
        document.getElementById("ctrlToFind").value = key;
        window.open("Searchempdel.aspx", "List", "scrollbars=yes,resizable=no,width=500,height=400");
        return false;
    }

    var cal = Calendar.setup({
        onSelect: function(cal) {
            cal.hide();
            $('#ctl00_ContentPlaceHolder1_txtTotalDays').val(1);
        },
        showTime: false
    });

    cal.manageFields("btDate", "ctl00_ContentPlaceHolder1_txtDateTime", "%m/%d/%Y");
    //    cal.manageFields("btDateTo", "ctl00_ContentPlaceHolder1_txtDateTimeTo", "%m/%d/%Y");


    $('#ctl00_ContentPlaceHolder1_txtTimePermit').timeEntry(
        {
            show24Hours: true,
            spinnerImage: ''
        }
    );

    var typePermitSelect = '<%=typePermitSelected%>';
    var dataTypePermit = [];
    $("#ctl00_ContentPlaceHolder1_TypePermit option").each(function() {
        var txtLabel = $(this).text();
        var kdSite = txtLabel.split("|");

        var obj = {
            kode: $(this).val(),
            kdsite: kdSite[0],
            remark: kdSite[1]
        };

        dataTypePermit.push(obj);
    });

    console.log(dataTypePermit);

    function BindSelectType() {
        $('#ctl00_ContentPlaceHolder1_TypePermit').empty();
        var site = 'ALL';
        var objSite = $('#site').val();
        if (objSite != undefined && objSite != '' && objSite == 'PEN') {
            site = objSite;
        }

        for (i = 0; i < dataTypePermit.length; i++) {
            var obj = dataTypePermit[i];
            if (obj.kdsite == site) {
                var selected = '';
                if (obj.kode == typePermitSelect) {
                    selected = 'selected';
                }
                $('#ctl00_ContentPlaceHolder1_TypePermit').append('<option ' + selected + ' value="' + obj.kode + '">' + obj.remark + '</option>');
            }
        }

    }
    

    //    $('#ctl00_ContentPlaceHolder1_site').click(function(){
    //        BindSelectType();
    //    });

    //    BindSelectType();
    
</script>

<%=alertMessage%>   
<%=functionBackEnd%>  

</asp:Content>