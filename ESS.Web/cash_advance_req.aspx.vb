﻿Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports System.IO
Imports System.Reflection
Imports System.Net

Partial Public Class cash_advance_req
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim tripno As String

        tripno = Request.QueryString("ID")
        tripnum.Text = tripno



        'Dim mimeType As String
        'Dim encoding As String
        'Dim fileNameExtension As string
        'Dim warnings As Warning()
        'Dim streams As String()

        'Dim exportBytes As Array = ReportViewer1.LocalReport.Render("PDF", Nothing, mimeType, encoding, fileNameExtension, streams, warnings)

    End Sub

    'Protected Sub ReportViewer1_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReportViewer1.PreRender


    '    ' Disable EXCEL format   
    '    Call DisableFormat(Me.ReportViewer1, "Excel")


    'End Sub


    'Protected Sub DisableFormat(ByRef viewer As ReportViewer, ByVal formatName As String)


    '    Const Flags As System.Reflection.BindingFlags = System.Reflection.BindingFlags.NonPublic + System.Reflection.BindingFlags.Public + System.Reflection.BindingFlags.Instance
    '    Dim m_previewService As System.Reflection.FieldInfo = viewer.LocalReport.GetType().GetField("m_previewService", Flags)
    '    Dim ListRenderingExtensions As System.Reflection.MethodInfo = m_previewService.FieldType.GetMethod("ListRenderingExtensions", Flags)
    '    Dim previewServiceInstance As Object = m_previewService.GetValue(viewer.LocalReport)
    '    Dim extensions As IList = ListRenderingExtensions.Invoke(previewServiceInstance, Nothing)
    '    Dim name As System.Reflection.PropertyInfo = extensions(0).GetType().GetProperty("Name", Flags)
    '    Dim extension As Object
    '    For Each extension In extensions

    '        If (String.Compare(name.GetValue(extension, Nothing).ToString(), formatName, True) = 0) Then
    '            Dim m_isVisible As System.Reflection.FieldInfo = extension.GetType().GetField("m_isVisible", System.Reflection.BindingFlags.NonPublic + System.Reflection.BindingFlags.Instance)
    '            '    m_isVisible.SetValue(extension, Dim m_isExposedExternally As System.Reflection.FieldInfo = extension.GetType().GetField("m_isExposedExternally", System.Reflection.BindingFlags.NonPublic + System.Reflection.BindingFlags.Instance)False) m_isExposedExternally.SetValue(extension, False)


    '            Exit For

    '        End If
    '    Next extension


    'End Sub

    'Public NotInheritable Class ReportViewerExtensions
    '    Private Sub New()
    '    End Sub
    '    '<System.Runtime.CompilerServices.Extension()> _
    '    Public Shared Sub SetExportFormatVisibility(ByVal viewer As ReportViewer, ByVal format As ReportViewerExportFormat, ByVal isVisible As Boolean)

    '        Dim formatName As String = format.ToString()

    '        Const Flags As System.Reflection.BindingFlags = System.Reflection.BindingFlags.NonPublic Or System.Reflection.BindingFlags.[Public] Or System.Reflection.BindingFlags.Instance
    '        Dim m_previewService As System.Reflection.FieldInfo = viewer.LocalReport.[GetType]().GetField("m_previewService", Flags)

    '        Dim ListRenderingExtensions As System.Reflection.MethodInfo = m_previewService.FieldType.GetMethod("ListRenderingExtensions", Flags)
    '        Dim previewServiceInstance As Object = m_previewService.GetValue(viewer.LocalReport)

    '        Dim extensions As IList = DirectCast(ListRenderingExtensions.Invoke(previewServiceInstance, Nothing), IList)
    '        Dim name As System.Reflection.PropertyInfo = extensions(0).[GetType]().GetProperty("Name", Flags)

    '        'object extension = null;
    '        For Each ext As Object In extensions

    '            If (String.Compare(name.GetValue(ext, Nothing).ToString(), formatName, True) = 0) Then
    '                Dim m_isVisible As System.Reflection.FieldInfo = ext.[GetType]().GetField("m_isVisible", System.Reflection.BindingFlags.NonPublic Or System.Reflection.BindingFlags.Instance)

    '                Dim m_isExposedExternally As System.Reflection.FieldInfo = ext.[GetType]().GetField("m_isExposedExternally", System.Reflection.BindingFlags.NonPublic Or System.Reflection.BindingFlags.Instance)
    '                m_isVisible.SetValue(ext, isVisible)
    '                m_isExposedExternally.SetValue(ext, isVisible)

    '                Exit For

    '            End If
    '        Next

    '        'ReportViewer1.SetExportFormatVisibility(ReportViewerExportFormat.PDF, False)
    '    End Sub

    'End Class

    'Public Enum ReportViewerExportFormat
    '    Excel
    '    PDF
    'End Enum

    'Public Sub DisableUnwantedExportFormat(ByVal ReportViewerID As ReportViewer, ByVal strFormatName As String)


    '    Dim info As FieldInfo

    '    For Each extension As RenderingExtension In ReportViewerID.ServerReport.ListRenderingExtensions()


    '        If extension.Name = strFormatName Then


    '            info = extension.[GetType]().GetField("m_isVisible", BindingFlags.Instance Or BindingFlags.NonPublic)


    '            info.SetValue(extension, False)



    '        End If
    '    Next

    'End Sub
    'Private Sub ReportViewer1_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReportViewer1.PreRender


    '    DisableUnwantedExportFormat(DirectCast(sender, ReportViewer), "PDF")


    'End Sub

    Protected Sub btnsavepdf_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsavepdf.Click
        Dim mimeType As String
        Dim Encoding As String
        Dim fileNameExtension As String
        Dim streams As String()
        Dim warnings As Microsoft.Reporting.WebForms.Warning()

        Dim pdfContent As Byte() = ReportViewer1.LocalReport.Render("PDF", Nothing, mimeType, Encoding, fileNameExtension, streams, warnings)
        'Creatr PDF file on disk
        Dim pdfPath As String = Server.MapPath("~/export/")
        pdfPath = Path.Combine(pdfPath, String.Format("cash_adv_req{0}.pdf", Session("otorisasi")))
        Dim pdfFile As New System.IO.FileStream(pdfPath, System.IO.FileMode.Create)
        pdfFile.Write(pdfContent, 0, pdfContent.Length)
        pdfFile.Close()


        Response.Redirect("export\cash_adv_req" + Session("otorisasi") + ".pdf")




    End Sub
End Class