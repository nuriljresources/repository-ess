﻿Imports System.Data.SqlClient

Partial Public Class tactivityplan
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            If Not Page.IsPostBack Then
                If Session("permission") = "" Or Session("permission") = Nothing Then
                    Response.Redirect("../Login.aspx", False)
                    Exit Sub
                ElseIf ("1;2;3;7;8;4;5;6").IndexOf(Session("permission")) = -1 Then
                    Response.Redirect("../default.aspx", False)
                    Exit Sub
                End If

                Grup.Value = Session("Permission")
                Section.Value = Session("bscdepar")
                Divisi.Value = Session("bscdivisi")
                Jobsite.Value = Session("jobsite")
                userid.Value = Session("NikUser")
                userip.Value = HttpContext.Current.Request.UserHostAddress

                If Not Page.IsPostBack Then
                    For Each x As ListItem In ddlYear.Items
                        If x.Text = Date.Now.Year Then
                            x.Selected = True
                        End If
                    Next

                    Dim cmd As SqlCommand
                    Dim dr As SqlDataReader = Nothing
                    Dim sqlQuery As String = ""
                    'Initialize Function
                    sqlQuery = "select ID,FunctionName,active from MFunction where active='1' order by ID"
                    cmd = New SqlCommand(sqlQuery, conn)
                    conn.Open()
                    dr = cmd.ExecuteReader
                    While dr.Read
                        If Session("permission") = "X" Then 'GRUP BOD dan MSD
                            KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                        ElseIf Session("permission") = "8" Or Session("permission") = "1" Or Session("permission") = "B" Or Session("permission") = "4" Or Session("permission") = "5" Then 'PIC PDCA dan GM dan Manager
                            If Session("bscdivisi") = dr(0) Then
                                KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                            End If
                        End If
                    End While
                    dr.Close()
                    dr = Nothing
                    'Initialize Section
                    sqlQuery = "select ID,SectionName,active,funcid from MSection order by ID"
                    cmd = New SqlCommand(sqlQuery, conn)
                    dr = cmd.ExecuteReader
                    While dr.Read
                        If Session("permission") = "1" Or Session("permission") = "B" Or Session("permission") = "3" Or Session("permission") = "6" Then 'GRUP MDV atau sekpro atau BOD
                            If Session("bscdepar") = dr(0) Then
                                KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                            End If
                        ElseIf Session("permission") = "7" Or Session("permission") = "2" Or Session("permission") = "A" Then 'GRUP S HEAD & S HEAD ADMIN
                            If Session("bscdepar") = dr(0) Then
                                KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                            End If
                        ElseIf Session("permission") = "8" Or Session("permission") = "4" Or Session("permission") = "5" Then 'PIC PDCA dan GM dan Manager
                            If Session("bscdivisi") = dr(3) Then
                                KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                            End If
                        End If
                    End While
                    dr.Close()
                    dr = Nothing
                    'Initialize Jobsite
                    sqlQuery = "select id,kdsite,active from site order by id"
                    cmd = New SqlCommand(sqlQuery, conn)
                    dr = cmd.ExecuteReader
                    While dr.Read
                        If Session("permission") = "1" Or Session("permission") = "B" Or Session("permission") = "8" Or Session("permission") = "3" Or Session("permission") = "4" Or Session("permission") = "5" Then 'GRUP MDV & PIC PDCA & BOD & GM & MANAGER
                            KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                        ElseIf Session("permission") = "7" Or Session("permission") = "2" Or Session("permission") = "6" Or Session("permission") = "A" Then 'GRUP S HEAD & SEKPRO & PM & S HEAD ADMIN
                            If Session("jobsite") = dr(1) Then
                                KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                            End If
                        End If
                        'If dr(2) = 0 Then
                        '    KPIJobsite.Items(KPIJobsite.Items.Count - 1).Enabled = False
                        'End If
                    End While
                    dr.Close()
                    dr = Nothing
                    conn.Close()
                    'Isi KPI Level
                    KPILevel.Items.Clear()
                    Select Case Session("permission")
                        Case "1" 'MSD
                            KPILevel.Items.Add(New ListItem("Function", "1"))
                            KPILevel.Items.Add(New ListItem("Section", "2"))
                        Case "B" 'MSD Func. Head
                            KPILevel.Items.Add(New ListItem("Function", "1"))
                            KPILevel.Items.Add(New ListItem("Section", "2"))
                        Case "2" 'Sek Pro
                            KPILevel.Items.Add(New ListItem("Section", "2"))
                        Case "7" 'S.Head
                            KPILevel.Items.Add(New ListItem("Section", "2"))
                        Case "8" 'PIC PDCA
                            KPILevel.Items.Add(New ListItem("Function", "1"))
                            KPILevel.Items.Add(New ListItem("Section", "2"))
                    End Select
                    'Selected Index Change
                    Select Case KPILevel.SelectedValue
                        Case 0
                            KPIFunc.Visible = False
                            KPIJobsite.Visible = False
                            KPISection.Visible = False
                        Case 1
                            KPIFunc.Visible = True
                            KPIJobsite.Visible = False
                            KPISection.Visible = False
                        Case 2
                            KPIFunc.Visible = False
                            KPIJobsite.Visible = True
                            KPISection.Visible = True
                        Case 3
                            KPIFunc.Visible = False
                            KPIJobsite.Visible = True
                            KPISection.Visible = False
                    End Select
                End If
                Retrieve()
            End If
        Catch ex As Exception
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Private Sub KPILevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles KPILevel.SelectedIndexChanged
        Try
            Select Case KPILevel.SelectedValue
                Case 0
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                Case 1
                    KPIFunc.Visible = True
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                Case 2
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = True
                Case 3
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = False
            End Select
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Retrieve()
    End Sub

    Sub Retrieve()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim tipe as string=""
        Try
            ddlPIC.Items.Clear()
            ddlStatus.Items.Clear()
            chkPIC.Checked = False
            chkStatus.Checked = False
            Dim myNumber() As String
            myNumber = New String() {"A.", "B.", "C.", "D.", "E.", "F.", "G.", "H.", "I.", "J.", "K.", "L.", "M.", "N.", "O.", "P.", "Q.", "R.", "S.", "T.", "U.", "V.", "W.", "X.", "Y.", "Z."}
            Dim cmd As New SqlCommand
            Dim dr As SqlDataReader
            Dim lock As Boolean = False
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn
            cmd.CommandText = "dbo.sp_getActivityPlan"
            Select Case KPILevel.SelectedValue
                Case "1"
                    cmd.Parameters.AddWithValue("@param", "1#" + KPIFunc.SelectedValue)
                Case "2"
                    cmd.Parameters.AddWithValue("@param", "2#" + KPISection.SelectedValue + "#" + KPIJobsite.SelectedValue)
            End Select
            cmd.Parameters.AddWithValue("@nik", Session("NikUser"))
            cmd.Parameters.AddWithValue("@year", ddlYear.SelectedValue)
            conn.Open()
            dr = cmd.ExecuteReader()
            If dr.HasRows Then
                result.InnerHtml = ""
                result.InnerHtml += "<table class='act' width='100%' id='activity'><thead>"
                result.InnerHtml += "<tr style='background-color:#00CC00;font-weight:bold'><td style='width:1%;text-align:center'>No.</td><td style='width:15%;text-align:center'>Action Plan</td><td style='width:10%;text-align:center'>KPI</td><td style='width:10%;text-align:center'>Start - End</td><td style='width:25%;text-align:center'>Log Progress</td><td style='width:30%;text-align:center'>Update Progress</td><td style='width:5%;text-align:center'>Status</td><td></td></tr></thead><tbody>"
                Dim noproject As Integer = 1
                Dim no As Integer = 1
                Dim subno As Integer = 1
                Dim counter As Integer = 1
                While dr.Read
                    '--------------------
                    If Not dr("pic") = "" Then
                        If ddlPIC.Items.IndexOf(New ListItem(dr("nama"), dr("pic"))) = -1 Then
                            ddlPIC.Items.Add(New ListItem(dr("nama"), dr("pic")))
                        End If
                    End If
                    If Not dr("status") = "" Then
                        Select Case dr("status")
                            Case "0"
                                If ddlStatus.Items.IndexOf(New ListItem("IDLE", dr("status"))) = -1 Then
                                    ddlStatus.Items.Add(New ListItem("IDLE", dr("status")))
                                End If
                            Case "1"
                                If ddlStatus.Items.IndexOf(New ListItem("ONP", dr("status"))) = -1 Then
                                    ddlStatus.Items.Add(New ListItem("ONP", dr("status")))
                                End If
                            Case "2"
                                If ddlStatus.Items.IndexOf(New ListItem("DONE", dr("status"))) = -1 Then
                                    ddlStatus.Items.Add(New ListItem("DONE", dr("status")))
                                End If
                        End Select
                    End If
                    '--------------
                    If (dr("jenis") = "PROGRAM" And dr("securecount") > 0 And dr("nik") = Session("NikUser")) Then
                        lock = False
                    ElseIf (dr("jenis") = "PROGRAM" And dr("securecount") = 0) Then
                        lock = False
                    ElseIf dr("jenis") = "TASK" Then
                    ElseIf dr("jenis") = "PROJECT" Then
                        lock = False
                    Else
                        lock = True
                    End If
                    If Not lock Then
                        result.InnerHtml += "<tr>"
                        If dr("jenis") = "PROJECT" Then
                            if tipe="" then
                                tipe = dr("tipe")
                            else if tipe <> dr("tipe") and dr("tipe")="2" then
                                tipe = dr("tipe")
                                 result.InnerHtml += "<td colspan='8' style='font-size:20px;vertical-align:top;border-bottom:thin solid #969696;background-color:#A9F2A9;'>CROSS FUNCTION</td></tr>"
                            End If
                            result.InnerHtml += "<tr>"
                            If noproject > 26 Then
                                result.InnerHtml += "<td style='font-size:20px;vertical-align:top;border-bottom:thin solid #A9F2A9;background-color:#A9F2A9;'>" & myNumber(CInt(noproject / 26) - 1) & myNumber(CInt(noproject Mod 26) - 1) & "</td>"
                            Else
                                result.InnerHtml += "<td style='font-size:20px;vertical-align:top;border-bottom:thin solid #A9F2A9;background-color:#A9F2A9;'>" & myNumber(noproject - 1) & "</td>"
                            End If
                            result.InnerHtml += "<td colspan='7' style='font-size:20px;vertical-align:top;border-bottom:thin solid #969696;background-color:#A9F2A9;'>" & dr("activity") & "</td></tr>"
                            noproject += 1
                            no = 1
                            subno = 1
                        Else
                            If dr("jenis") = "PROGRAM" Then
                                result.InnerHtml += "<td rowspan='2' style='vertical-align:top;border-bottom:thin solid #D2EFD2;'>" & no & "</td>"
                                no += 1
                                subno = 1
                            Else
                                result.InnerHtml += "<td rowspan='2' style='vertical-align:top;border-bottom:thin solid #969696;'>" & no - 1 & "." & subno & "</td>"
                                subno += 1
                            End If
                            result.InnerHtml += "<td rowspan='2' style='vertical-align:top;border-bottom:thin solid #969696;" & IIf(dr("jenis") = "TASK", "", "background-color:#D2EFD2;") & "'>" & dr("activity") & "</td>"
                            result.InnerHtml += "<td rowspan='2' style='vertical-align:top;border-bottom:thin solid #969696;'>" & dr("name") & "</td>"
                            If IsDBNull(dr("startdate")) Then
                                result.InnerHtml += "<td style='vertical-align:top;text-align:center'>-"
                            Else
                                result.InnerHtml += "<td style='vertical-align:top;text-align:center'>" & Format(dr("startdate"), "dd MMM yy") & ""
                            End If
                            If IsDBNull(dr("enddate")) Then
                                result.InnerHtml += "-</td>"
                            Else
                                result.InnerHtml += "<br>" & Format(dr("enddate"), "dd MMM yy") & "</td>"
                            End If
                            result.InnerHtml += "<td rowspan='2' style='vertical-align:top;border-bottom:thin solid #969696;'><div id='logprogress" & dr("id") & "' style='height:50px;overflow:auto;'><script type='text/javascript'>document.getElementById('logprogress" & dr("id") & "').innerHTML = unescape('" & Replace(Replace(dr("logprogress"), "&lt;br&gt;", "<br>"), "%0D%0A", "<br>") & "')</script></div></td>"
                            result.InnerHtml += "<td rowspan='2' style='vertical-align:top;border-bottom:thin solid #969696;'>" & IIf(dr("pic") = Session("NikUser"), "<textarea style='width:99%;' class='plain' onkeypress='return imposeMaxLength(event,this, 500)' rows=3 id='myprogress" & dr("id") & "'></textarea>", "")
                            If dr("pic") = Session("NikUser") Then
                                'removed based on req
                                'result.InnerHtml += "<img src='../images/save-icon-small.png' style='cursor:hand' height='20px' width='20px' onclick='saveprogress(" & dr("nomor") & "," & dr("id") & ");'/>"
                                '-------------------------------------------
                                result.InnerHtml += "<input type='hidden' value='" & dr("id") & "'>"
                            End If
                            result.InnerHtml += "</td>"
                            result.InnerHtml += "<td rowspan='2' style='vertical-align:top;border-bottom:thin solid #969696;text-align:center'>" & IIf(dr("pic") = Session("NikUser"), "<input type='button' style='width:100%' id='btnStatus" & dr("id") & "' onclick='updateStatus(" & dr("id") & "," & dr("id") & ",""" & dr("activity") & """)' value='" & IIf(dr("status") = "0", "IDLE", IIf(dr("status") = "1", "ONP", "DONE")) & "'/>", "<input type='button' style='width:100%' disabled id='btnStatus" & dr("id") & "'  value='" & IIf(dr("status") = "0", "IDLE", IIf(dr("status") = "1", "ONP", "DONE")) & "'/>") & "</td>"
                            If dr("pic") = Session("NikUser") Then
                                result.InnerHtml += "<td rowspan='2' id='upload" & counter & "' style='vertical-align:top;border-bottom:thin solid #969696;text-align:center'>" & "<input type='button' id='btnUpload' Style='width:10px;height:25px;background-image: url(../images/attachment_icon.gif); background-repeat: no-repeat;cursor:hand;background-color:transparent;border:0' onclick='javascript:openupload(this,""" & dr("id") & """,""" & dr("activity") & """)'/>"
                                If dr("total") > 0 Then
                                    result.InnerHtml += "<img src='../images/book.jpg' alt='attachment' height='30px' width='30px'/></td>"
                                Else
                                    result.InnerHtml += "&nbsp;</td>"
                                End If
                            Else
                                result.InnerHtml += "<td rowspan='2' style='vertical-align:top;border-bottom:thin solid #969696;text-align:center'>"
                                If dr("total") > 0 Then
                                    result.InnerHtml += "<img src='../images/book.jpg' alt='attachment' height='30px' width='30px' onclick='javascript:openview(this,""" & dr("id") & """,""" & dr("activity") & """)'/></td>"
                                Else
                                    result.InnerHtml += "&nbsp;</td>"
                                End If
                            End If
                            result.InnerHtml += "</tr>"
                            result.InnerHtml += "<tr><td style='text-align:left;border-bottom:thin solid #969696;vertical-align:top;'><b>PIC : " & dr("nama") & "</b></td></tr>"
                            'result.InnerHtml += "<tr><td colspan='2' style='vertical-align:top;text-align:center'>" & dr("nama") & "</td></tr>"
                            'no += 1
                        End If
                    End If
                    counter += 1
                End While
                result.InnerHtml += "</tbody></table><script src='scripts/table_floating_header.js' type='text/javascript'></script>"
                'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes7123", "", True)
                'ddlPIC.DataSource = picList
                'ddlPIC.DataBind()
                'ddlStatus.DataSource = statusList
                'ddlStatus.DataBind()
            Else
                result.InnerHtml = "No Data Found"
            End If
            conn.Close()
        Catch ex As Exception
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Private Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        Try
            Dim pic As String = ""
            Dim status As String = ""
            If chkPIC.Checked Then
                pic = ddlPIC.SelectedItem.Value
            End If
            If chkStatus.Checked Then
                status = ddlStatus.SelectedItem.Value
            End If
            If chkPIC.Checked Or chkStatus.Checked Then
                Retrieve(pic, status)
            Else
                Retrieve()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub Retrieve(ByVal pic As String, ByVal status As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim tipe As String = ""
        Try
            Dim myNumber() As String
            myNumber = New String() {"A.", "B.", "C.", "D.", "E.", "F.", "G.", "H.", "I.", "J.", "K.", "L.", "M.", "N.", "O.", "P.", "Q.", "R.", "S.", "T.", "U.", "V.", "W.", "X.", "Y.", "Z."}
            Dim cmd As New SqlCommand
            Dim dr As SqlDataReader
            Dim lock As Boolean = False
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = conn
            cmd.CommandText = "dbo.sp_getActivityPlan"
            Select Case KPILevel.SelectedValue
                Case "1"
                    cmd.Parameters.AddWithValue("@param", "1#" + KPIFunc.SelectedValue)
                Case "2"
                    cmd.Parameters.AddWithValue("@param", "2#" + KPISection.SelectedValue + "#" + KPIJobsite.SelectedValue)
            End Select
            cmd.Parameters.AddWithValue("@nik", Session("NikUser"))
            cmd.Parameters.AddWithValue("@year", ddlYear.SelectedValue)
            conn.Open()
            dr = cmd.ExecuteReader()
            If dr.HasRows Then
                result.InnerHtml = ""
                result.InnerHtml += "<table class='act' width='100%' id='activity'><thead>"
                result.InnerHtml += "<tr style='background-color:#00CC00;font-weight:bold'><td style='width:1%;text-align:center'>No.</td><td style='width:15%;text-align:center'>Action Plan</td><td style='width:10%;text-align:center'>KPI</td><td style='width:10%;text-align:center'>Start - End</td><td style='width:25%;text-align:center'>Log Progress</td><td style='width:30%;text-align:center'>Update Progress</td><td style='width:5%;text-align:center'>Status</td><td></td></tr></thead><tbody>"
                Dim noproject As Integer = 1
                Dim no As Integer = 1
                Dim subno As Integer = 1
                Dim counter As Integer = 1
                'ddlPIC.Items.Clear()
                'ddlStatus.Items.Clear()
                While dr.Read
                    '--------------------
                    'If Not dr("pic") = "" Then
                    '    If ddlPIC.Items.IndexOf(New ListItem(dr("nama"), dr("pic"))) = -1 Then
                    '        ddlPIC.Items.Add(New ListItem(dr("nama"), dr("pic")))
                    '    End If
                    'End If
                    'If Not dr("status") = "" Then
                    '    Select Case dr("status")
                    '        Case "0"
                    '            If ddlStatus.Items.IndexOf(New ListItem("IDLE", dr("status"))) = -1 Then
                    '                ddlStatus.Items.Add(New ListItem("IDLE", dr("status")))
                    '            End If
                    '        Case "1"
                    '            If ddlStatus.Items.IndexOf(New ListItem("ONP", dr("status"))) = -1 Then
                    '                ddlStatus.Items.Add(New ListItem("ONP", dr("status")))
                    '            End If
                    '        Case "2"
                    '            If ddlStatus.Items.IndexOf(New ListItem("DONE", dr("status"))) = -1 Then
                    '                ddlStatus.Items.Add(New ListItem("DONE", dr("status")))
                    '            End If
                    '    End Select
                    'End If
                    '--------------
                    If pic <> "" Then
                        If pic <> dr("pic") And dr("jenis") <> "PROJECT" Then
                            Continue While
                        End If
                    End If
                    If status <> "" Then
                        If status <> dr("status") And dr("jenis") <> "PROJECT" Then
                            Continue While
                        End If
                    End If
                    '--------------
                    If (dr("jenis") = "PROGRAM" And dr("securecount") > 0 And dr("nik") = Session("NikUser")) Then
                        lock = False
                    ElseIf (dr("jenis") = "PROGRAM" And dr("securecount") = 0) Then
                        lock = False
                    ElseIf dr("jenis") = "TASK" Then
                    ElseIf dr("jenis") = "PROJECT" Then
                        lock = False
                    Else
                        lock = True
                    End If
                    If Not lock Then
                        result.InnerHtml += "<tr>"
                        If dr("jenis") = "PROJECT" Then
                            If tipe = "" Then
                                tipe = dr("tipe")
                            ElseIf tipe <> dr("tipe") And dr("tipe") = "2" Then
                                tipe = dr("tipe")
                                result.InnerHtml += "<td colspan='8' style='font-size:20px;vertical-align:top;border-bottom:thin solid #969696;background-color:#A9F2A9;'>CROSS FUNCTION</td></tr>"
                            End If
                            If noproject > 26 Then
                                result.InnerHtml += "<td style='font-size:20px;vertical-align:top;border-bottom:thin solid #A9F2A9;'>" & myNumber(CInt(noproject / 26) - 1) & myNumber(CInt(noproject Mod 26) - 1) & "</td>"
                            Else
                                result.InnerHtml += "<td style='font-size:20px;vertical-align:top;border-bottom:thin solid #A9F2A9;'>" & myNumber(noproject - 1) & "</td>"
                            End If
                            result.InnerHtml += "<td colspan='7' style='font-size:20px;vertical-align:top;border-bottom:thin solid #969696;background-color:#A9F2A9;'>" & dr("activity") & "</td></tr>"
                            noproject += 1
                            no = 1
                            subno = 1
                        Else
                            If dr("jenis") = "PROGRAM" Then
                                result.InnerHtml += "<td rowspan='2' style='vertical-align:top;border-bottom:thin solid #D2EFD2;'>" & no & "</td>"
                                no += 1
                                subno = 1
                            Else
                                result.InnerHtml += "<td rowspan='2' style='vertical-align:top;border-bottom:thin solid #969696;'>" & IIf(no - 1 = 0, 1, no - 1) & "." & subno & "</td>"
                                subno += 1
                            End If
                            result.InnerHtml += "<td rowspan='2' style='vertical-align:top;border-bottom:thin solid #969696;" & IIf(dr("jenis") = "TASK", "", "background-color:#D2EFD2;") & "'>" & dr("activity") & "</td>"
                            result.InnerHtml += "<td rowspan='2' style='vertical-align:top;border-bottom:thin solid #969696;'>" & dr("name") & "</td>"
                            If IsDBNull(dr("startdate")) Then
                                result.InnerHtml += "<td style='vertical-align:top;text-align:center'>-"
                            Else
                                result.InnerHtml += "<td style='vertical-align:top;text-align:center'>" & Format(dr("startdate"), "dd MMM yy") & ""
                            End If
                            If IsDBNull(dr("enddate")) Then
                                result.InnerHtml += "-</td>"
                            Else
                                result.InnerHtml += "<br>" & Format(dr("enddate"), "dd MMM yy") & "</td>"
                            End If
                            result.InnerHtml += "<td rowspan='2' style='vertical-align:top;border-bottom:thin solid #969696;'><div id='logprogress" & dr("id") & "' style='height:50px;overflow:auto;'><script type='text/javascript'>document.getElementById('logprogress" & dr("id") & "').innerHTML = unescape('" & Replace(Replace(dr("logprogress"), "&lt;br&gt;", "<br>"), "%0D%0A", "<br>") & "')</script></div></td>"
                            result.InnerHtml += "<td rowspan='2' style='vertical-align:top;border-bottom:thin solid #969696;'>" & IIf(dr("pic") = Session("NikUser"), "<textarea style='width:99%;' class='plain' onkeypress='return imposeMaxLength(event,this, 500)' rows=3 id='myprogress" & dr("id") & "'></textarea>", "")
                            If dr("pic") = Session("NikUser") Then
                                'removed based on req
                                'result.InnerHtml += "<img src='../images/save-icon-small.png' style='cursor:hand' height='20px' width='20px' onclick='saveprogress(" & dr("nomor") & "," & dr("id") & ");'/>"
                                '-------------------------------------------
                                result.InnerHtml += "<input type='hidden' value='" & dr("id") & "'>"
                            End If
                            result.InnerHtml += "</td>"
                            result.InnerHtml += "<td rowspan='2' style='vertical-align:top;border-bottom:thin solid #969696;text-align:center'>" & IIf(dr("pic") = Session("NikUser"), "<input type='button' style='width:100%' id='btnStatus" & dr("id") & "' onclick='updateStatus(" & dr("id") & "," & dr("id") & ",""" & dr("activity") & """)' value='" & IIf(dr("status") = "0", "IDLE", IIf(dr("status") = "1", "ONP", "DONE")) & "'/>", "<input type='button' style='width:100%' disabled id='btnStatus" & dr("id") & "'  value='" & IIf(dr("status") = "0", "IDLE", IIf(dr("status") = "1", "ONP", "DONE")) & "'/>") & "</td>"
                            If dr("pic") = Session("NikUser") Then
                                result.InnerHtml += "<td rowspan='2' id='upload" & counter & "' style='vertical-align:top;border-bottom:thin solid #969696;text-align:center'>" & "<input type='button' id='btnUpload' Style='width:10px;height:25px;background-image: url(../images/attachment_icon.gif); background-repeat: no-repeat;cursor:hand;background-color:transparent;border:0' onclick='javascript:openupload(this,""" & dr("id") & """,""" & dr("activity") & """)'/>"
                                If dr("total") > 0 Then
                                    result.InnerHtml += "<img src='../images/book.jpg' alt='attachment' height='30px' width='30px'/></td>"
                                Else
                                    result.InnerHtml += "&nbsp;</td>"
                                End If
                            Else
                                result.InnerHtml += "<td rowspan='2' style='vertical-align:top;border-bottom:thin solid #969696;text-align:center'>"
                                If dr("total") > 0 Then
                                    result.InnerHtml += "<img src='../images/book.jpg' alt='attachment' height='30px' width='30px' onclick='javascript:openview(this,""" & dr("id") & """,""" & dr("activity") & """)'/></td>"
                                Else
                                    result.InnerHtml += "&nbsp;</td>"
                                End If
                            End If
                            result.InnerHtml += "</tr>"
                            result.InnerHtml += "<tr><td style='text-align:left;border-bottom:thin solid #969696;vertical-align:top;'><b>PIC : " & dr("nama") & "</b></td></tr>"
                            'result.InnerHtml += "<tr><td colspan='2' style='vertical-align:top;text-align:center'>" & dr("nama") & "</td></tr>"
                            'no += 1
                        End If
                    End If
                    counter += 1
                End While
                result.InnerHtml += "</tbody></table><script src='scripts/table_floating_header.js' type='text/javascript'></script>"
                'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes7123", "", True)                
            Else
                result.InnerHtml = "No Data Found"
            End If
            conn.Close()
        Catch ex As Exception
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub
End Class