﻿Imports System.Data.SqlClient

Partial Public Class emp_permit_list
    Inherits System.Web.UI.Page

    Public tglaw As String
    Public th As String
    Public v1 As String
    Public v2 As String
    Public imonth As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") = "" Or Session("permission") = Nothing Then
            Response.Redirect("Login.aspx", False)
            Exit Sub
        End If

        If Not IsPostBack Then
            SetControlData()
        End If

        BindData()


    End Sub

    Public Sub BindData()
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

        Dim dtb As DataTable = New DataTable
        sqlConn.Open()
        dtb = New DataTable()

        Dim strcon As String = <![CDATA[SELECT
                            PERMIT.[transid], 
                            PERMIT.[nik], 
                            EMP.Nama,
                            PERMIT.[LType], 
                            TYPEPERMIT.[remark] AS TypePermit, 
                            PERMIT.[trans_date], 
                            PERMIT.[trans_date2], 
                            PERMIT.[totleave], 
                            PERMIT.[remarks], 
                            PERMIT.[fstatus], 
                            PERMIT.[kdsite], 
                            PERMIT.[StEdit] 
                        FROM [L_H006] PERMIT 
                            INNER JOIN [H_A101] EMP ON PERMIT.nik = EMP.NIK
                            INNER JOIN [L_A006] TYPEPERMIT ON PERMIT.[LType] = TYPEPERMIT.kode
                        WHERE 
                            EMP.nik = '{0}' AND
                            YEAR(PERMIT.[trans_date]) = '{1}' AND MONTH(PERMIT.[trans_date]) = '{2}' AND PERMIT.[StEdit] != 2 
                        ORDER BY PERMIT.[CreatedTime]]]>.Value

        strcon = String.Format(strcon, Session("niksite"), dYear.SelectedValue, dMonth.SelectedValue)
        Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
        sda.Fill(dtb)

        GridView1.DataSource = dtb
        GridView1.DataBind()
    End Sub

    Public Sub SetControlData()
        v1 = DateTime.Now.AddYears(-3).Year.ToString()
        v2 = DateTime.Now.Year.ToString()

        dYear.Items.Clear()
        For I = v1 To v2
            dYear.Items.Add(I)
        Next
        dYear.SelectedValue = (I - 1).ToString
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        BindData()
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GridView1.SelectedIndexChanged
        If GridView1.SelectedRow Is Nothing Then Exit Sub

        Dim row As GridViewRow = GridView1.SelectedRow
        Dim TransId As String = GridView1.DataKeys(row.RowIndex).Value.ToString

        Response.Redirect("emp_permit.aspx?id=" + TransId, True)
    End Sub
End Class