﻿Imports System.Data.SqlClient
Imports System.Data
Partial Public Class index
    Inherits System.Web.UI.Page
    Dim myDt As DataTable
    Public jrnnum As String
    Public nmuser As String
    Public level As String
    Public mnentry As String
    Public mnview As String
    Public mnmgmt As String
    Public depar As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        'If Me.IsPostBack = False Then
        '    myDt = New DataTable()
        '    myDt = CreateDataTable()
        '    Session("myDatatable") = myDt

        '    Me.GridView1.DataSource = (CType(Session("myDatatable"), DataTable)).DefaultView
        '    Me.GridView1.DataBind()
        'End If
        If Session("otorisasi_permit") = Nothing Or Session("otorisasi_permit") = "" Then
            Response.Redirect("signin_permit.aspx")
        End If

        jrnnum = Session("otorisasi_permit")
        nmuser = Session("NmUser_permit")
        level = Session("permission_permit")

        If Trim(level) = "1" Then level = "Owner"
        If Trim(level) = "2" Then level = "Doc Admin"
        If Trim(level) = "4" Then level = "PIC"
        If Trim(level) = "3" Then level = "User"

        If Trim(Session("permission_permit")) = "1" Or Trim(Session("permission_permit")) = "2" Then
            mnentry = "<div id='mnentry'><img alt='entry document' src='../images/open_doc_small.png' /> <a href='doc_center_entry.aspx'>&nbsp;Set Document</a></div>"
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
        End If

        If Trim(Session("permission_permit")) = "1" Then
            mnmgmt = "<div id='mnmgmt'> <img alt='Management User' src='../images/open_doc_small.png' /> <a href='mgmnt_list.aspx'>&nbsp;Management User</a></div>"
        End If

        If Trim(Session("permission_permit")) = "3" Or Trim(Session("permission_permit")) = "4" Then
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
        End If

        Dim div As String = Session("divisi")        

        Try
            Dim strcon As String = "select top 5 id,substring(f_name,1,80) as f_name,ftype,fsubject,kdcom, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10), createdtime, 101) as createdtime, convert(char(10),cms_start, 101) as cms_start, convert(char(10),cms_end, 101) as cms_end, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic1) as pic1, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic2) as pic2, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic3) as pic3, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic4) as pic4, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic5) as pic5 from docpermit_trans where stedit <> 2 and (cms_end BETWEEN GETDATE() AND GETDATE() + 30 OR GETDATE() > cms_end) and modul = '" + Session("modul_permit").ToString + "'"
            'strcon = strcon + " where month(reqdate) = '" + imonth.ToString + "' and YEAR(reqdate) = '" + th + "' and nik = '1231313' and stedit <> 2"
            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)

            sda.Fill(dtb)
            GridView1.DataSource = dtb
            GridView1.DataBind()

            'Dim strcon2 As String = "select top 5 id,substring(f_name,1,80) as f_name,ftype,fsubject,kdcom, company_name, kdsite, site_name,fyear,fcategory,f_path,doctype, convert(char(10), createdtime, 101) as createdtime, convert(char(10),cms_start, 101) as cms_start, convert(char(10),cms_end, 101) as cms_end, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic1) as pic1, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic2) as pic2, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic3) as pic3, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic4) as pic4, (select nama from jrn_test.dbo.H_A101 where niksite = docpermit_trans.pic5) as pic5 from docpermit_trans where stedit <> 2 and (cms_end BETWEEN GETDATE() AND GETDATE() + 30)"
            'Dim dtb2 As DataTable = New DataTable
            'Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn)

            'sda2.Fill(dtb2)

        Catch ex As Exception

        End Try

        depar = Trim(Session("dept"))
    End Sub

    'Private Function CreateDataTable() As DataTable
    '    Dim myDataTable As DataTable = New DataTable()

    '    Dim myDataColumn As DataColumn

    '    myDataColumn = New DataColumn()
    '    myDataColumn.DataType = Type.GetType("System.String")
    '    myDataColumn.ColumnName = "id"
    '    myDataTable.Columns.Add(myDataColumn)

    '    myDataColumn = New DataColumn()
    '    myDataColumn.DataType = Type.GetType("System.String")
    '    myDataColumn.ColumnName = "username"
    '    myDataTable.Columns.Add(myDataColumn)

    '    myDataColumn = New DataColumn()
    '    myDataColumn.DataType = Type.GetType("System.String")
    '    myDataColumn.ColumnName = "firstname"
    '    myDataTable.Columns.Add(myDataColumn)

    '    myDataColumn = New DataColumn()
    '    myDataColumn.DataType = Type.GetType("System.String")
    '    myDataColumn.ColumnName = "lastname"
    '    myDataTable.Columns.Add(myDataColumn)

    '    myDataColumn = New DataColumn()
    '    myDataColumn.DataType = Type.GetType("System.Int32")
    '    myDataColumn.ColumnName = "point"
    '    myDataTable.Columns.Add(myDataColumn)
    '    Return myDataTable
    'End Function

    'Private Function AddDataToTable(ByVal username As String, ByVal firstname As String, ByVal lastname As String, ByVal point As String, ByVal myTable As DataTable)
    '    Dim row As DataRow

    '    row = myTable.NewRow()

    '    row("id") = Guid.NewGuid().ToString()
    '    row("username") = username
    '    row("firstname") = firstname
    '    row("lastname") = lastname
    '    row("point") = point

    '    myTable.Rows.Add(row)
    'End Function


    'Protected Sub btnAdd_Click_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd_Click.Click
    'If txtUserName.Text.Trim() = "" Then
    '    Me.lblTips.Text = "You must fill a username."
    '    Return
    'Else
    '    AddDataToTable(Me.txtUserName.Text.Trim(), Me.txtFirstName.Text.Trim(), Me.txtLastName.Text.Trim(), Me.txtpoint.Text.Trim(), CType(Session("myDatatable"), DataTable))

    '    Me.GridView1.DataSource = CType(Session("myDatatable"), DataTable).DefaultView

    '    Me.GridView1.DataBind()

    '    Me.txtFirstName.Text = ""
    '    Me.txtLastName.Text = ""
    '    Me.txtUserName.Text = ""
    '    Me.lblTips.Text = ""
    '    Me.txtpoint.Text = ""

    'End If

    'End Sub

    'Protected Sub btnsum_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsum.Click
    '    Dim table As DataTable
    '    'table = DataSet.Tables("YourTableName")
    '    'table = myDt.DataSet.Tables("myDt")
    '    table = CType(Session("myDatatable"), DataTable)

    '    ' Declare an object variable.
    '    Dim sumObject As Object
    '    sumObject = table.Compute("Sum(point)", "")

    '    Dim dicSum As New Dictionary(Of String, Integer)()
    '    For Each row As DataRow In table.Rows
    '        Dim group As String = row("username").ToString()
    '        Dim rate As Integer = Convert.ToInt32(row("point"))
    '        If dicSum.ContainsKey(group) Then
    '            dicSum(group) += rate
    '        Else
    '            dicSum.Add(group, rate)
    '        End If
    '    Next
    '    For Each g As String In dicSum.Keys
    '        Console.WriteLine("SUM({0})={1}", g, dicSum(g))
    '        lblTips.Text = lblTips.Text + g + ": " + dicSum(g).ToString + ", "
    '    Next

    '    'lblTips.Text = sumObject.ToString()
    'End Sub
End Class