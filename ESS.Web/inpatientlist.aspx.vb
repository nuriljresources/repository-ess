﻿Imports System.Data
Imports System.Data.SqlClient
Partial Public Class inpatientlist
    Inherits System.Web.UI.Page
    Public tglaw As String
    Public th As String
    Public v1 As String
    Public v2 As String
    Public imonth As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("permission") = "" Or Session("permission") = Nothing Then
        '    Response.Redirect("Login.aspx", False)
        '    Exit Sub
        'End If

        Dim a As String

        tglaw = Request(ddlbulan.UniqueID)
        th = Request(ddltahun.UniqueID)
        Dim i As Integer
        ddlbulan.Items.Clear()
        ddlbulan.Items.Add("January")
        ddlbulan.Items.Add("February")
        ddlbulan.Items.Add("March")
        ddlbulan.Items.Add("April")
        ddlbulan.Items.Add("May")
        ddlbulan.Items.Add("June")
        ddlbulan.Items.Add("July")
        ddlbulan.Items.Add("August")
        ddlbulan.Items.Add("September")
        ddlbulan.Items.Add("October")
        ddlbulan.Items.Add("November")
        ddlbulan.Items.Add("December")

        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

        sqlConn.Open()
        Dim strcon As String = "select min(reqdate) as min_date ,max(reqdate) as max_date from V_H001"
        Dim dtb As DataTable = New DataTable
        Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
        sda.Fill(dtb)

        v1 = dtb.Rows(0)!min_date.ToString
        v1 = CDate(v1).ToString("yyyy")

        v2 = dtb.Rows(0)!max_date.ToString
        v2 = CDate(v2).ToString("yyyy")

        ddltahun.Items.Clear()
        For i = v1 To v2
            ddltahun.Items.Add(i)
        Next

        ddltahun.SelectedValue = (i - 1).ToString

        If Not IsPostBack Then
            Dim strmedin As String = "SELECT convert(char(10),H_H215.TglTrans,101) as TglTrans,H_H215.Keterangan,H_H215.NoReg,H_H215.KdSite,H_H215.Tipe,H_H215.Nik,H_A101.Nama,   H_H215.fstatus,   H_H215.CreatedBy,   H_H215.CreatedIn,   H_H215.CreatedTime,   H_H215.ModifiedBy,   H_H215.ModifiedIn,   H_H215.ModifiedTime,   H_H215.StEdit,   H_H215.DeleteBy,   H_H215.DeleteTime,   H_A130.NmDepar,H_A101.kdlevel,H_H215.pycostcode, " & _
                                     "( SELECT  H_A21501.amount1  FROM     H_A101,  H_A21501, H_A21502 WHERE   ( H_A21501.nobuk = H_A21502.nobuk ) and ( H_A101.kdlevel = H_A21502.kdlevel ) and ( H_A21501.stedit<>'2' ) AND ( H_A21501.TipeOutPatient='1') AND ( H_A101.nik =  H_H215.Nik) AND H_A21501.kdsite = H_A101.kdsite  ) as 'Max' FROM H_H215, H_A101, H_A130 WHERE ( H_H215.Nik = H_A101.Nik ) and  ( H_A101.KdDepar = H_A130.KdDepar ) and ( H_H215.Tipe = '1' )  AND  ( H_H215.StEdit <> '2' )"
            Dim dtbmedin As DataTable = New DataTable
            Dim sdamedin As SqlDataAdapter = New SqlDataAdapter(strmedin, sqlConn)
            sdamedin.Fill(dtbmedin)

            GridView1.DataSource = dtbmedin
            GridView1.DataBind()
        End If
    End Sub

End Class