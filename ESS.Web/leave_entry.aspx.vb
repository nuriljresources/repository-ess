﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Drawing.Color


Partial Public Class leave_entry
    Inherits System.Web.UI.Page
    Public dtnow As String
    Public alert As String
    Public remain, yeara, yearb, atot, btot As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("niksite") = Nothing Or Session("niksite") = "" Then Response.Redirect("login.aspx")

        If Not Session("kdsite") = "JKT" Then
            btnSubmit.Enabled = False
        End If

        If Not IsPostBack Then
            Dim snik As String = Session("niksite")
            txtnik.Attributes.Add("readonly", "readonly")
            txtnama.Attributes.Add("readonly", "readonly")
            txtdepar.Attributes.Add("readonly", "readonly")
            txtposition.Attributes.Add("readonly", "readonly")
            txtkdsite.Attributes.Add("readonly", "readonly")
            txtleavetype.Attributes.Add("readonly", "readonly")
            txtpaidleave.Attributes.Add("readonly", "readonly")
            txtleafrom.Attributes.Add("readonly", "readonly")
            txtleato.Attributes.Add("readonly", "readonly")
            'txtleato.Attributes.Add("readonly", "readonly")
            txttotleave.Attributes.Add("readonly", "readonly")
            txtcutabs.Attributes.Add("readonly", "readonly")
            txtremleave.Attributes.Add("readonly", "readonly")
            txtleaeli.Attributes.Add("readonly", "readonly")
            txtnikdut.Attributes.Add("readonly", "readonly")
            txtnmdut.Attributes.Add("readonly", "readonly")
            txtposdut.Attributes.Add("readonly", "readonly")
            'txtverby.Attributes.Add("readonly", "readonly")
            'txtposverby.Attributes.Add("readonly", "readonly")
            txtnikappby.Attributes.Add("readonly", "readonly")
            txtposappby.Attributes.Add("readonly", "readonly")
            dayoff_dateex.Attributes.Add("readonly", "readonly")
            dayoff_usage.Attributes.Add("readonly", "readonly")

            'txtholiday.Enabled = False

            'btnedit.Enabled = False
            'btnprint.Enabled = False

            txtcutabs.Visible = False
            txtcutabs.Text = "0"
            txtleaeli.Visible = False
            txtleaeli.Text = "0"

        End If

        If Not dayoff_tripid.Value = "" Then
            txtdayoff.Text = dayoff_tripid.Value
            txtdayoff.Enabled = True
        End If

        dtnow = Date.Today
        txtdtreq.Text = dtnow
    End Sub

    Protected Sub btncalc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btncalc.Click
        'Dim dtstart As DateTime = DateTime.ParseExact(txtleafrom.Text, "dd/MM/yyyy", Nothing)
        'Dim dtend As DateTime = DateTime.ParseExact(txtleato.Text, "dd/MM/yyyy", Nothing)
        atot = 0
        btot = 0

        Dim yr As String = Year(Date.Now).ToString
        Dim YearLeave, YearLeaveBack, TotalLeaveDay, litotb, liTotNew, TotalQuotaRemainRef, TotalQuotaRemain, TotalQuotaRemain2, liUsed, liUsedb, TotalDeduct, liSisa, TotalAbsen As Decimal
        Dim DateQuotaLeave As Date
        Dim lastUsed, LastBurn As Double

        TotalLeaveDay = 0
        litotb = 0
        liTotNew = 0

        If txtnikx.Value = Nothing Or txtnikx.Value = "" Then
            alert = "<script type ='text/javascript' > alert('Nik can not empty') </script>"
            Return
        ElseIf txtleavetype.Text = Nothing Or txtleavetype.Text = "" Then
            alert = "<script type ='text/javascript' > alert('Leave Type can not empty') </script>"
            Return
        ElseIf txtholiday.Text = Nothing Or txtholiday.Text = "" Then
            alert = "<script type ='text/javascript' > alert('Holiday can not empty') </script>"
            Return
        Else

            If txtholiday.Text = Nothing Then txtholiday.Text = "0"
            If txtcutabs.Text = Nothing Then txtcutabs.Text = "0"

            'Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("test_con").ConnectionString)
            Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)

            Try
                If txtholiday.Text < 0 Then
                    alert = "<script type ='text/javascript' > alert('Holiday value can not minus') </script>"
                    Return
                End If

                sqlConn.Open()
                Dim dtb As DataTable = New DataTable()
                Dim dtbx As DataTable = New DataTable()
                Dim dtblinfo As DataTable = New DataTable()
                Dim dtbyrend As DataTable = New DataTable()

                'Dim strlinfo As String = " SELECT  H_A101.Nik , H_A101.tglmasuk , H_A101.kdsite, H_A101.kdlokasi, " & _
                '"(SELECT DATEPOP FROM h_h217 WHERE NIK = '" + txtnikx.Value + "' AND YEAR = " + yr + ") AS 'DateLeavePop', " & _
                '"isnull((SELECT TOTlEAVE FROM h_h217 WHERE NIK = '" + txtnikx.Value + "' AND YEAR = " + yr + "),0) AS 'Totleave', " & _
                '"(SELECT DATEEXPIRED FROM h_h217 WHERE NIK = '" + txtnikx.Value + "' AND YEAR = " + yr + ") AS 'ExpiredLeave', " & _
                '"isnull((select SUM(atot) from L_H001 where stedit <> '2' and fstatus=1 " & _
                '"and typeleave='00' and nik = '" + txtnikx.Value + "' and yeara =   " + yr + " ),0) + " & _
                '"isnull((select SUM(btot) from L_H001 where stedit <> '2' and fstatus=1 " & _
                '"and typeleave='00' and nik =  '" + txtnikx.Value + "' and yearb =  " + yr + " ),0) as 'usedLeave', " & _
                '"isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='00' " & _
                '"and nik = '" + txtnikx.Value + "' and YEAR(trans_date) = " + yr + "),0) as 'burnLeave', " & _
                '"isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='01' " & _
                '"and nik = '" + txtnikx.Value + "' and YEAR(trans_date) = " + yr + "),0) as 'AddLeave', " & _
                '"isnull((select sum(TOTlEAVE) from H_H217 where nik = '" + txtnikx.Value + "' " & _
                '"and year < " + yr + "  and dateExpired  >= getdate()  ),0) as 'LastTot', " & _
                '"isnull((select SUM(atot) from L_H001 where stedit <> '2' and fstatus=1 " & _
                '"and typeleave='00' and nik = '" + txtnikx.Value + "' and yeara =   " + yr + " -1 ),0)  + " & _
                '"isnull((select SUM(btot) from L_H001 where stedit <> '2' and fstatus=1 " & _
                '"and typeleave='00' and nik =  '" + txtnikx.Value + "' and yearb =  " + yr + " - 1 ),0) as 'LastusedLeave', " & _
                '"isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='00' " & _
                '"and nik = '" + txtnikx.Value + "' and YEAR(trans_date) = " + yr + " -1 ),0) as 'LastburnLeave' , " & _
                '"isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='01' " & _
                '"and nik = '" + txtnikx.Value + "' and YEAR(trans_date) = " + yr + " -1 ),0) as 'LastAddLeave' " & _
                '"FROM H_A101 WHERE H_A101.Nik = '" + txtnikx.Value + "'"

                Dim strlinfo As String = String.Empty




                Dim sptotLeave As Integer = 0

                'If txtleavetype.Text = "Long Service Leave" Then
                '    strlinfo = <![CDATA[SELECT h_a101.nik, h_a101.tglmasuk, h_a101.kdsite, h_a101.kdlokasi, 
                '           (SELECT datepop FROM   h_h217L WHERE  nik = '@NIK' AND year = @YEAR) AS 'DateLeavePop', ISNULL((SELECT totleave FROM   h_h217L WHERE  nik = '@NIK'AND year = @YEAR), 0) AS 'Totleave',

                '            0 AS 'LastTotEx',
                '           --Isnull((select sum(TOTlEAVE) from H_H217L where nik =  @NIK and dateExpired < getdate() and year < @YEAR   ),0) as 'LastTotEx',

                '           --isnull((select SUM(atot) from L_H001 where stedit <> '2' and fstatus=1 and typeleave='00' and nik = '@NIK' and yeara in (select year from H_H217L where nik = '@NIK'  AND dateExpired < getdate() and year < @YEAR 
                '                --union select YEAR from l_A099 where year < (select min(year) from H_H217L where nik = '@NIK'  AND dateExpired < getdate() and year < @YEAR) 
                '                --union select year(h_a101.tglmasuk) )       ),0)  + 
                '                --isnull((select SUM(btot) from L_H001 where stedit <> '2' and fstatus=1 and typeleave='00' and nik =  '@NIK' and yearb  in (select year from H_H217L where nik = '@NIK'  AND dateExpired < getdate() and year < @YEAR
                '                --union select YEAR from l_A099 where year < (select min(year) from H_H217L where nik = '@NIK'  AND dateExpired < getdate() and year < @YEAR)
                '                --union select year(h_a101.tglmasuk)  )    ),0) as 'LastusedLeaveEx',
                '            0 AS 'LastusedLeaveEx',

                '           (SELECT dateexpired FROM h_h217L WHERE  nik = '@NIK' AND year = @YEAR) AS 'ExpiredLeave', 
                '           (SELECT dateexpired FROM h_h217L WHERE  nik = '@NIK' AND year = @YEAR - 1) AS 'LastExpiredLeave',
                '           ISNULL((SELECT Sum(atot) FROM l_h001 WHERE stedit <> '2' AND fstatus=1 AND typeleave='01' AND nik = '@NIK' AND yeara = @YEAR ), 0) + Isnull((SELECT Sum(btot) FROM l_h001 WHERE stedit <> '2' AND fstatus=1 AND typeleave='01' AND nik = '@NIK' AND yearb = @YEAR ), 0) AS 'usedLeave', 
                '           ISNULL((SELECT Sum(totleave) FROM   l_h002L WHERE  stedit <> '2' AND fstatus = 1 AND ltype = '00' AND nik = '@NIK' AND Year(trans_date) = @YEAR), 0) AS 'burnLeave', 
                '           ISNULL((SELECT Sum(totleave) FROM   l_h002L WHERE  stedit <> '2' AND fstatus = 1 AND ltype = '01' AND nik = '@NIK' AND Year(trans_date) = @YEAR), 0) AS 'AddLeave', 
                '           ISNULL((SELECT Sum(totleave) FROM   h_h217L WHERE  nik = '@NIK' AND dateexpired >= Getdate() AND year < @YEAR), 0) AS  'LastTot', 
                '           ISNULL((SELECT Sum(atot) FROM l_h001 WHERE stedit <> '2' AND fstatus=1 AND typeleave='01' AND nik = '@NIK' AND yeara IN (SELECT year FROM h_h217L WHERE nik = h_a101.nik AND dateexpired >= Getdate() AND year < @YEAR) ), 0) + ISNULL((SELECT Sum(btot) FROM l_h001 WHERE stedit <> '2' AND fstatus=1 AND typeleave='01' AND nik = '@NIK' AND yearb IN (SELECT year FROM h_h217L WHERE nik = h_a101.nik AND dateexpired >= Getdate() AND year < @YEAR) ), 0) AS 'LastusedLeave', 
                '           ISNULL((SELECT Sum(totleave) FROM   l_h002L WHERE  stedit <> '2' AND fstatus = 1 AND ltype = '00' AND nik = '@NIK' AND Year(trans_date) < @YEAR), 0) AS 'LastburnLeave', 
                '           ISNULL((SELECT Sum(totleave) FROM   l_h002L WHERE  stedit <> '2' AND fstatus = 1 AND ltype = '01' AND nik = '@NIK' AND Year(trans_date) < @YEAR), 0) AS 'LastAddLeave', 
                '           ISNULL((SELECT Sum(totleave) FROM   h_h217L WHERE  nik = h_a101.nik AND dateexpired < Getdate() AND year < @YEAR), 0) AS 'LastTotEx', 
                '           ISNULL((SELECT Sum(atot) FROM l_h001 WHERE stedit <> '2' AND fstatus=1 AND typeleave='01' AND nik = h_a101.nik AND yeara IN (SELECT year FROM h_h217L WHERE nik = h_a101.nik AND dateexpired < Getdate() AND year < @YEAR UNION SELECT year FROM l_a099 WHERE year < (SELECT Min(year) FROM h_h217L WHERE nik = h_a101.nik AND dateexpired < Getdate() AND year < @YEAR) UNION SELECT Year(h_a101.tglmasuk) ) ), 0) + Isnull((SELECT Sum(btot) FROM l_h001 WHERE stedit <> '2' AND fstatus=1 AND typeleave='01' AND nik = h_a101.nik AND yearb IN (SELECT year FROM h_h217L WHERE nik = h_a101.nik AND dateexpired < Getdate() AND year < @YEAR UNION SELECT year FROM l_a099 WHERE year < (SELECT Min(year) FROM h_h217L WHERE nik = h_a101.nik AND dateexpired < Getdate() AND year < @YEAR) UNION SELECT Year(h_a101.tglmasuk) ) ), 0) AS  'LastusedLeaveEx'
                '    FROM   h_a101 
                '    WHERE  h_a101.nik = '@NIK' 
                '    ]]>.Value

                'Else
                '    strlinfo = <![CDATA[SELECT h_a101.nik, 
                '           h_a101.tglmasuk, 
                '           h_a101.kdsite, 
                '           h_a101.kdlokasi, 
                '           (SELECT datepop FROM   h_h217 WHERE  nik = '@NIK' AND year = @YEAR) AS 'DateLeavePop',
                '           Isnull((SELECT totleave FROM   h_h217 WHERE  nik = '@NIK' AND year = @YEAR), 0) AS 'Totleave', 
                '           Isnull((select sum(TOTlEAVE) from H_H217 where nik =  @NIK and dateExpired < getdate() and year < @YEAR   ),0) as 'LastTotEx',
                '           isnull((select SUM(atot) from L_H001 where stedit <> '2' and fstatus=1 and typeleave='00' and nik = '@NIK' and yeara in (select year from H_H217 where nik = '@NIK'  AND dateExpired < getdate() and year < @YEAR 
                '                union select YEAR from l_A099 where year < (select min(year) from H_H217 where nik = '@NIK'  AND dateExpired < getdate() and year < @YEAR) 
                '                union select year(h_a101.tglmasuk) )       ),0)  + 
                '                isnull((select SUM(btot) from L_H001 where stedit <> '2' and fstatus=1 and typeleave='00' and nik =  '@NIK' and yearb  in (select year from H_H217 where nik = '@NIK'  AND dateExpired < getdate() and year < @YEAR
                '                union select YEAR from l_A099 where year < (select min(year) from H_H217 where nik = '@NIK'  AND dateExpired < getdate() and year < @YEAR)
                '                union select year(h_a101.tglmasuk)  )    ),0) as 'LastusedLeaveEx',

                '           (SELECT dateexpired FROM h_h217 WHERE  nik = '@NIK' AND year = @YEAR) AS 'ExpiredLeave', 
                '           (SELECT dateexpired FROM   h_h217 WHERE  nik = '@NIK' AND year = @YEAR - 1) AS 'LastExpiredLeave', 
                '           Isnull((SELECT Sum(atot) FROM l_h001 WHERE stedit <> '2' AND fstatus=1 AND typeleave='00' AND nik = '@NIK' AND yeara = @YEAR ), 0) + Isnull((SELECT Sum(btot) FROM l_h001 WHERE stedit <> '2' AND fstatus=1 AND typeleave='00' AND nik = '@NIK' AND yearb = @YEAR ), 0) AS 'usedLeave', 
                '           Isnull((SELECT Sum(totleave) FROM   l_h002 WHERE  stedit <> '2' AND fstatus = 1 AND ltype = '00' AND nik = '@NIK' AND Year(trans_date) = @YEAR), 0) AS 'burnLeave', 
                '           Isnull((SELECT Sum(totleave) FROM   l_h002 WHERE  stedit <> '2' AND fstatus = 1 AND ltype = '01' AND nik = '@NIK' AND Year(trans_date) = @YEAR), 0)                      AS 'AddLeave', 
                '           Isnull((SELECT Sum(totleave) FROM   h_h217 WHERE  nik = '@NIK' AND dateexpired >= Getdate() AND year < @YEAR), 0) AS 'LastTot', Isnull((SELECT Sum(atot) FROM l_h001 WHERE stedit <> '2' AND fstatus=1 AND typeleave='00' AND nik = '@NIK' AND yeara IN (SELECT year FROM h_h217 WHERE nik = h_a101.nik AND dateexpired >= Getdate() AND year < @YEAR) ), 0) + Isnull((SELECT Sum(btot) FROM l_h001 WHERE stedit <> '2' AND fstatus=1 AND typeleave='00' AND nik = '@NIK' AND yearb IN (SELECT year FROM h_h217 WHERE nik = h_a101.nik AND dateexpired >= Getdate() AND year < @YEAR) ), 0) AS 'LastusedLeave', 
                '           Isnull((SELECT Sum(totleave) FROM   l_h002 WHERE  stedit <> '2' AND fstatus = 1 AND ltype = '00' AND nik = '@NIK' AND Year(trans_date) < @YEAR), 0) AS 'LastburnLeave', Isnull((SELECT Sum(totleave) FROM   l_h002 WHERE  stedit <> '2' AND fstatus = 1 AND ltype = '01' AND nik = '@NIK' AND Year(trans_date) < @YEAR), 0) AS 'LastAddLeave', 
                '           Isnull((SELECT Sum(totleave) FROM   h_h217 WHERE  nik = h_a101.nik AND dateexpired < Getdate() AND year < @YEAR), 0) AS 'LastTotEx',
                '           Isnull((SELECT Sum(atot) FROM l_h001 WHERE stedit <> '2' AND fstatus=1 AND typeleave='00' AND nik = h_a101.nik AND yeara IN (SELECT year FROM h_h217 WHERE nik = h_a101.nik AND dateexpired < Getdate() AND year < @YEARUNION SELECT year FROM l_a099 WHERE year < (SELECT Min(year) FROM h_h217 WHERE nik = h_a101.nik AND dateexpired < Getdate() AND year < @YEAR) UNION SELECT Year(h_a101.tglmasuk) ) ), 0) + Isnull((SELECT Sum(btot) FROM l_h001 WHERE stedit <> '2' AND fstatus=1 AND typeleave='00' AND nik = h_a101.nik AND yearb IN (SELECT year FROM h_h217 WHERE nik = h_a101.nik AND dateexpired < Getdate() AND year < @YEAR UNION SELECT year FROM l_a099 WHERE year < (SELECT Min(year) FROM h_h217 WHERE nik = h_a101.nik AND dateexpired < Getdate() AND year < @YEAR) UNION SELECT Year(h_a101.tglmasuk) ) ), 0) AS 'LastusedLeaveEx' 
                '           FROM   h_a101 
                '    WHERE  h_a101.nik = '@NIK'
                '    ]]>.Value

                'End If

                Dim CMD As New SqlCommand("sp_ESS_LEAVE_CALC")
                CMD.Parameters.AddWithValue("@nik", txtnikx.Value)
                CMD.Parameters.AddWithValue("@argThn", yr)

                'strlinfo = strlinfo.Replace("@NIK", txtnikx.Value)
                'strlinfo = strlinfo.Replace("@YEAR", yr)
                'Dim CMD As New SqlCommand(strlinfo)

                Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
                CMD.Connection = connection
                CMD.CommandType = CommandType.StoredProcedure

                Dim sdalinfo As SqlDataAdapter = New SqlDataAdapter(CMD)
                sdalinfo.Fill(dtblinfo)
                Dim rDB As System.Data.DataRow = dtblinfo.Rows(0)


                Dim strcon As String = "select nik, year, datepop, totleave, remainleave, fhangusbig, fhangussick, fextend, fadd, dateexpired, dateextend, extendref, hangusBref, hangusSref, addref from h_h217 where nik = '" + txtnikx.Value + "'"
                'Dim strcon As String = "select nik, year, datepop, totleave, remainleave, fhangusbig, fhangussick, fextend, fadd, dateexpired, dateextend, extendref, hangusBref, hangusSref, addref from h_h217 where nik = '0000002'"
                Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
                sda.Fill(dtb)

                Dim stryrend As String = "select [year], totleave, "
                stryrend = stryrend + "(totleave - ISNULL((select sum(atot) total from L_H001 where stedit <> 2 and fstatus = 1 and typeleave = '00' and (yeara=h_h217.year) and nik = '" + txtnikx.Value + "') + "
                stryrend = stryrend + "(select sum(btot) total from L_H001 where stedit <> 2 and fstatus = 1 and typeleave = '00' and (yearb=h_h217.year) and nik = '" + txtnikx.Value + "'),0)) remainL"
                stryrend = stryrend + " from h_h217 where nik = '" + txtnikx.Value + "' and year(dateexpired) >= " + yr + " order by year asc"
                Dim sdayrend As SqlDataAdapter = New SqlDataAdapter(stryrend, sqlConn)
                sdayrend.Fill(dtbyrend)

                If txtleafrom.Text <> Nothing Then
                    Dim dtstart As DateTime = txtleafrom.Text
                    Dim dtend As DateTime = txtleato.Text


                    'Dim ts As String = (dtend - dtstart).Days
                    'ts = ts - txtholiday.Text

                    'txttotleave.Text = ts - txtholiday.Text

                    'litotDeduct = ts + dtblinfo.Rows(0)!LastTot 'total potong cuti
                    TotalAbsen = txtcutabs.Text

                    TotalLeaveDay = CDec(DateDiff(DateInterval.Day, dtstart, dtend)) - CDec(txtholiday.Text) + 1  'hrs cek hari libur?

                    If dtend < dtstart Then
                        alert = "<script type ='text/javascript' > alert('Date from smaller than date to, Please check date leave.') </script>"
                        Return
                    End If

                    If TotalLeaveDay <= 0 Then
                        alert = "<script type ='text/javascript' > alert('Total Deduct Leave = 0, Cannot Recalculate, Please check date leave.') </script>"
                        Return
                    End If

                    If TotalAbsen < 0 Then
                        alert = "<script type ='text/javascript' > alert('Value cut from absent must not minus') </script>"
                        Return
                    End If



                    TotalDeduct = TotalLeaveDay + TotalAbsen

                    'REWRITE
                    'If rDB!dateleavepop.ToString = "" Or rDB!dateleavepop.ToString = Nothing Then
                    '    DateQuotaLeave = Today
                    'Else
                    '    DateQuotaLeave = rDB!dateleavepop.ToString
                    'End If



                    YearLeave = Year(DateQuotaLeave)
                    YearLeaveBack = YearLeave - 1

                    'cek date cuti bilamana dibawah date pop maka jatah cuti tahun ini = 0
                    'If txtleafrom.Text < DateQuotaLeave Then
                    '    TotalQuotaRemain = CDec(rDB!CarryFRemain.ToString) - CDec(rDB!usedleave.ToString)
                    'Else
                    '    TotalQuotaRemain = CDec(rDB!LeaveRemain.ToString) + CDec(rDB!CarryFRemain.ToString)
                    'End If

                    'TotalQuotaRemain2 = rDB!LastTot

                    'If rDB!lastexpiredleave.ToString = "" Then rDB!lastexpiredleave = "1/1/1990"


                    'If TotalQuotaRemain2 > 0 Then
                    '    lastUsed = rDB!LastusedLeave
                    '    LastBurn = rDB!LastburnLeave
                    '    'liRemainb = (liRemainb + dtblinfo.Rows(0)!LastAddLeave) - (lastUsed + LastBurn)

                    '    If rDB!LastTotEx - rDB!LastusedLeaveEx < 0 Then
                    '        TotalQuotaRemain2 = (rDB!LastTotEx - rDB!LastusedLeaveEx) + (TotalQuotaRemain2 + rDB!lastaddleave) - (rDB!lastusedleave + rDB!lastburnleave)
                    '    Else
                    '        TotalQuotaRemain2 = (TotalQuotaRemain2 + rDB!lastaddleave) - (rDB!lastusedleave + rDB!lastburnleave)
                    '    End If

                    '    'diganti dengan rumus diatas
                    '    'If dtblinfo.Rows(0)!lastexpiredleave < txtleafrom.Text Then
                    '    '    If ((liRemainb + dtblinfo.Rows(0)!lastaddleave) - (dtblinfo.Rows(0)!lastusedleave + dtblinfo.Rows(0)!lastburnleave) < 0) Then
                    '    '        liRemainb = (liRemainb + dtblinfo.Rows(0)!lastaddleave) - (dtblinfo.Rows(0)!lastusedleave + dtblinfo.Rows(0)!lastburnleave)
                    '    '    Else
                    '    '        liRemainb = 0
                    '    '    End If
                    '    'Else
                    '    '    liRemainb = (liRemainb + dtblinfo.Rows(0)!lastaddleave) - (dtblinfo.Rows(0)!lastusedleave + dtblinfo.Rows(0)!lastburnleave)
                    '    'End If
                    'Else
                    '    TotalQuotaRemain2 = 0 'sdh abis expired
                    '    'ambil remain minus , akalu ada positiop nolkan krn da expired
                    '    lastUsed = rDB!LastusedLeave
                    '    LastBurn = rDB!LastburnLeave
                    '    'liRemainb = (liRemainb + dtblinfo.Rows(0)!LastAddLeave) - (lastUsed + LastBurn)
                    '    If rDB!lastexpiredleave < txtleafrom.Text Then
                    '        If ((TotalQuotaRemain2 + rDB!lastaddleave) - (rDB!lastusedleave + rDB!lastburnleave) < 0) Then
                    '            TotalQuotaRemain2 = (TotalQuotaRemain2 + rDB!lastaddleave) - (rDB!lastusedleave + rDB!lastburnleave)
                    '        Else
                    '            TotalQuotaRemain2 = 0
                    '        End If
                    '    Else
                    '        TotalQuotaRemain2 = (TotalQuotaRemain2 + rDB!lastaddleave) - (rDB!lastusedleave + rDB!lastburnleave)
                    '    End If

                    '    If TotalQuotaRemain2 < 0 Then
                    '    Else
                    '        TotalQuotaRemain2 = 0
                    '    End If

                    'End If

                    'If TotalQuotaRemain2 > 0 Then
                    '    If TotalDeduct > TotalQuotaRemain2 Then
                    '        litotb = TotalQuotaRemain2
                    '        liSisa = TotalDeduct - litotb
                    '        If liSisa > 0 Then
                    '            'Cek sisa cuti thn ini
                    '            If liSisa > TotalQuotaRemain Then
                    '                'tambahan bole cuti minus
                    '                liTotNew = liSisa
                    '                liSisa = 0
                    '            Else
                    '                liTotNew = liSisa
                    '                liSisa = 0
                    '            End If
                    '        End If
                    '    Else
                    '        litotb = TotalDeduct
                    '        liSisa = 0
                    '    End If
                    'Else 'cek klo sisa cuti ga ada cek cuti thn ini
                    '    If TotalDeduct > TotalQuotaRemain Then
                    '        'tambahan bole cuti minus
                    '        liTotNew = TotalDeduct
                    '        liSisa = 0
                    '    Else
                    '        liTotNew = TotalDeduct
                    '        liSisa = 0
                    '    End If
                    'End If

                    'TotalQuotaRemainRef = (TotalQuotaRemain + TotalQuotaRemain2) - TotalDeduct 'tambahan syam remain leave bisa minus
                    'remain = TotalQuotaRemainRef

                    'perbaikan tahun cuti syam 27 July 2016
                    'Dim iyr As Integer
                    'If dtbyrend.Rows.Count > 0 Then
                    '    If dtbyrend.Rows.Count >= 2 Then
                    '        YearLeave = dtbyrend.Rows(1)!year.ToString()
                    '        YearLeaveBack = dtbyrend.Rows(0)!year.ToString()

                    '        For iyr = 0 To dtbyrend.Rows.Count - 1
                    '            If dtbyrend.Rows(iyr)!remainL > 0 Then
                    '                If dtbyrend.Rows(iyr)!remainL - TotalDeduct > 0 Then
                    '                    YearLeave = (Convert.ToInt32(dtbyrend.Rows(iyr)!year) + 1).ToString()
                    '                    YearLeaveBack = dtbyrend.Rows(iyr)!year.ToString()
                    '                    liTotNew = 0
                    '                    litotb = TotalDeduct
                    '                    iyr = dtbyrend.Rows.Count
                    '                Else
                    '                    YearLeave = (Convert.ToInt32(dtbyrend.Rows(iyr)!year) + 1).ToString()
                    '                    YearLeaveBack = dtbyrend.Rows(iyr)!year.ToString()
                    '                    litotb = TotalDeduct - dtbyrend.Rows(iyr)!remainL
                    '                    liTotNew = dtbyrend.Rows(iyr)!remainL
                    '                    iyr = dtbyrend.Rows.Count
                    '                End If
                    '                'Else
                    '                '    iyr = iyr + 1
                    '            End If
                    '        Next
                    '    ElseIf dtbyrend.Rows.Count = 1 Then
                    '        If dtbyrend.Rows(0)!year = yr Then
                    '            YearLeave = dtbyrend.Rows(0)!year.ToString()
                    '            YearLeaveBack = (dtbyrend.Rows(0)!year - 1).ToString()

                    '            For iyr = 0 To dtbyrend.Rows.Count - 1
                    '                If dtbyrend.Rows(iyr)!remainL > 0 Then
                    '                    If dtbyrend.Rows(iyr)!remainL - TotalDeduct > 0 Then
                    '                        YearLeave = (Convert.ToInt32(dtbyrend.Rows(iyr)!year) + 1).ToString()
                    '                        YearLeaveBack = dtbyrend.Rows(iyr)!year.ToString()
                    '                        liTotNew = 0
                    '                        litotb = TotalDeduct
                    '                        iyr = dtbyrend.Rows.Count
                    '                    Else
                    '                        YearLeave = (Convert.ToInt32(dtbyrend.Rows(iyr)!year) + 1).ToString()
                    '                        YearLeaveBack = dtbyrend.Rows(iyr)!year.ToString()
                    '                        litotb = TotalDeduct - dtbyrend.Rows(iyr)!remainL
                    '                        liTotNew = dtbyrend.Rows(iyr)!remainL
                    '                        iyr = dtbyrend.Rows.Count
                    '                    End If
                    '                    'Else
                    '                    '    iyr = iyr + 1
                    '                End If
                    '            Next
                    '        ElseIf dtbyrend.Rows(0)!year < yr Then
                    '            YearLeave = (dtbyrend.Rows(0)!year + 1).ToString()
                    '            YearLeaveBack = (dtbyrend.Rows(0)!year).ToString()

                    '            For iyr = 0 To dtbyrend.Rows.Count - 1
                    '                If dtbyrend.Rows(iyr)!remainL > 0 Then
                    '                    If dtbyrend.Rows(iyr)!remainL - TotalDeduct > 0 Then
                    '                        YearLeave = (Convert.ToInt32(dtbyrend.Rows(iyr)!year) + 1).ToString()
                    '                        YearLeaveBack = dtbyrend.Rows(iyr)!year.ToString()
                    '                        liTotNew = 0
                    '                        litotb = TotalDeduct
                    '                        iyr = dtbyrend.Rows.Count
                    '                    Else
                    '                        YearLeave = (Convert.ToInt32(dtbyrend.Rows(iyr)!year) + 1).ToString()
                    '                        YearLeaveBack = dtbyrend.Rows(iyr)!year.ToString()
                    '                        litotb = TotalDeduct - dtbyrend.Rows(iyr)!remainL
                    '                        liTotNew = dtbyrend.Rows(iyr)!remainL
                    '                        iyr = dtbyrend.Rows.Count
                    '                    End If
                    '                    'Else
                    '                    '    iyr = iyr + 1
                    '                End If
                    '            Next
                    '        End If
                    '    End If
                    'Else
                    '    alert = "<script type ='text/javascript'> alert('Pop Date not found, Leave Eligible hasn't Generated') </script>"
                    'End If

                    yeara = YearLeave
                    yearb = YearLeaveBack


                    'end perbaikan tahun cuti
                    atot = liTotNew
                    btot = litotb

                    Dim TotLeave As Decimal = 0
                    Dim LeaveRemain As Decimal = 0
                    Dim CarryFRemain As Decimal = 0
                    Dim usedLeave As Decimal = 0

                    If txtleavetype.Text = "Long Service Leave" Then
                        TotLeave = CDec(rDB!ltotleave.ToString)
                        LeaveRemain = CDec(rDB!lLeaveRemain.ToString)
                        CarryFRemain = CDec(rDB!lCarryFRemain.ToString)
                        usedLeave = CDec(rDB!lusedLeave.ToString)
                    Else
                        TotLeave = CDec(rDB!Totleave.ToString)
                        LeaveRemain = CDec(rDB!LeaveRemain.ToString)
                        CarryFRemain = CDec(rDB!CarryFRemain.ToString)
                        usedLeave = CDec(rDB!usedLeave.ToString)
                    End If


                    Dim sisaLeave As Decimal = 0
                    If (CarryFRemain < 0) Then
                        CarryFRemain = 0
                    End If

                    If dtstart < CDate(rDB!DateLeavePop.ToString) Then
                        TotalQuotaRemain = CarryFRemain - usedLeave
                    Else
                        TotalQuotaRemain = LeaveRemain + CarryFRemain
                    End If

                    txtremleave.Text = CDec(TotalQuotaRemain - TotalDeduct).ToString("#.##")
                    txtremleaveact.Value = CDec(TotalQuotaRemain - TotalDeduct).ToString("#.##")
                    If txtremleave.Text = "" Then txtremleave.Text = "0"
                    If txtremleaveact.Value = "" Then txtremleaveact.Value = "0"

                    txttotleave.Text = CDec(TotalDeduct).ToString("#.##")
                    If txttotleave.Text = "" Then txttotleave.Text = "0"
                    txtcutabs.Text = 0
                    txtleaeli.Text = CDec(TotalQuotaRemain + TotalQuotaRemain2).ToString("#.##")
                    If txtleaeli.Text = "" Then txtleaeli.Text = "0"

                    Dim Liyeara As Integer = Year(dtstart)
                    Dim Liyearb As Integer = Liyeara - 1
                    yeara = Liyeara
                    yearb = Liyearb

                    If CarryFRemain > 0 Then

                        If CarryFRemain > TotalDeduct Then
                            btot = TotalDeduct
                            TotalDeduct = 0
                        Else
                            btot = CarryFRemain
                            TotalDeduct = TotalDeduct - CarryFRemain
                        End If

                    End If

                    atot = TotalDeduct

                    hyeara.Value = yeara
                    hyearb.Value = yearb
                    htota.Value = atot
                    htotb.Value = btot

                End If

                If rDB!DateLeavePop.ToString = Nothing Or rDB!DateLeavePop.ToString = "" Then
                    alert = "<script type ='text/javascript' > alert('Nik Leave Pop Date not found, Leave Eligible hasn't Generated') </script>"
                End If


                If dtb.Rows.Count = Nothing Then
                    alert = "<script type ='text/javascript' > alert(''Nik Leave Pop Date not found, Leave Eligible hasn't Generated.) </script>"
                ElseIf dtb.Rows.Count > 0 Then
                    Dim dtleafrom As DateTime = CDate(txtleafrom.Text)
                    Dim dtpop As DateTime = CDate(dtb.Rows(0)!datepop)
                    'If dtleafrom < dtpop Then
                    '    'txtremleave.Text = "0"
                    '    txtleaeli.Text = "0"
                    '    txtremleave.Text = Format(dtb.Rows(0)!totleave, "0.00")
                    '    txtcutabs.Text = "0"
                    'Else
                    '    txtremleave.Text = Format(dtb.Rows(0)!totleave, "0.00")
                    '    txtleaeli.Text = dtb.Rows(0)!remainleave
                    '    txtcutabs.Text = "0"
                    'End If
                End If
            Catch ex As Exception
                If ex.Message.Contains("Conversion from string") Then
                    txtholiday.Text = "0"
                End If
            End Try
            'alert('test');
        End If

    End Sub

    Private Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        'Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("test_con").ConnectionString)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)

        Dim sqlQuery As String
        Dim cmd As SqlCommand
        Dim dtmnow As String
        Dim dtynow As String
        Dim stripno As String

        Dim permitid As String
        Dim createdin As String
        Dim dttm As String
        Dim tes As Boolean
        Dim cb(10) As String
        Dim bcheck As Boolean = True

        validasilentry(cb)

        If cb(2) = "False" Then
            alert = "<script type ='text/javascript' > alert('" + cb(1) + "') </script>"
            Return
        End If

        'createdin = HttpContext.Current.Request.UserHostName
        createdin = System.Net.Dns.GetHostName()
        dttm = Date.Now.ToString

        If leavetype.Value = "03" And (txtpaidleave.Text = "" Or txtpaidleave.Text = Nothing) Then
            alert = "<script type ='text/javascript' > alert('Please fill paid leave') </script>"
            txtpaidleave.BackColor = System.Drawing.ColorTranslator.FromHtml("#FADEE8")
            Return
        End If

        If txtappby.Value = "" Then
            alert = "<script type ='text/javascript' > alert('Please fill approved by') </script>"
            txtnikappby.BackColor = System.Drawing.ColorTranslator.FromHtml("#FADEE8")
            Return
        End If

        If leavetype.Value = "07" Then
            If (dayoff_tripid.Value = "" Or dayoff_tripid.Value = Nothing) Then
                alert = "<script type ='text/javascript' > alert('Please fill businness trip leave') </script>"
                txtdayoff.BackColor = System.Drawing.ColorTranslator.FromHtml("#FADEE8")
                Return
            Else
                Dim dtExpired As DateTime = dayoff_dateex.Text
                If String.IsNullOrEmpty(txtleafrom.Text) Or String.IsNullOrEmpty(txtleato.Text) Then
                    alert = "<script type ='text/javascript' > alert('Leave From & Leave muse be selected ') </script>"
                    Return
                End If

                Dim dtstart As DateTime = txtleafrom.Text
                Dim dtend As DateTime = txtleato.Text

                If (dtstart > dtExpired) Or (dtend > dtExpired) Then
                    alert = "<script type ='text/javascript' > alert('Leave From & Leave To not in range date business trip ') </script>"
                    Return
                End If

                If Integer.Parse(txttotleave.Text) > Integer.Parse(dayoff_usage.Text.Replace(".00", "")) Then
                    alert = "<script type ='text/javascript' > alert('Total leave cannot more from Total Balance ') </script>"
                    Return
                End If

            End If
        End If

        Try
            conn.Open()

            dtmnow = DateTime.Now.ToString("MM")
            dtynow = Date.Now.Year.ToString
            stripno = dtynow + dtmnow + "/" + "L" + "/"

            Dim strlh001 As String = "SELECT CONVERT(INT, ISNULL(MAX(NOLAST), 0)) AS LastNo  FROM (select REPLACE(transid, '" + stripno + "','') AS NOLAST, transid from L_H001 where transid like '%" + stripno + "%') DOCLAST"

            'Dim strlh001 As String = "select REPLACE(transid, '', '') from L_H001 where transid like '%" + stripno + "%'"

            Dim dtblh001 As DataTable = New DataTable
            Dim sdalh001 As SqlDataAdapter = New SqlDataAdapter(strlh001, conn)

            sdalh001.Fill(dtblh001)

            Dim lastNo As Integer = dtblh001.Rows(0)!LastNo + 1
            permitid = stripno + lastNo.ToString("000000")


            'If dtblh001.Rows.Count = 0 Then
            '    permitid = stripno + "000001"
            'ElseIf dtblh001.Rows.Count > 0 Then
            '    I = 0
            '    I = dtblh001.Rows.Count + 1
            '    If dtblh001.Rows.Count > 0 And dtblh001.Rows.Count < 9 Then
            '        permitid = stripno + "00000" + I.ToString
            '    ElseIf dtblh001.Rows.Count >= 9 And dtblh001.Rows.Count < 99 Then
            '        permitid = stripno + "0000" + I.ToString
            '    ElseIf dtblh001.Rows.Count >= 99 And dtblh001.Rows.Count < 999 Then
            '        permitid = stripno + "000" + I.ToString
            '    ElseIf dtblh001.Rows.Count >= 999 And dtblh001.Rows.Count < 9999 Then
            '        permitid = stripno + "00" + I.ToString
            '    ElseIf dtblh001.Rows.Count >= 9999 And dtblh001.Rows.Count < 99999 Then
            '        permitid = stripno + "0" + I.ToString
            '    ElseIf dtblh001.Rows.Count >= 99999 And dtblh001.Rows.Count < 999999 Then
            '        permitid = stripno + I.ToString
            '    ElseIf dtblh001.Rows.Count >= 999999 Then
            '        permitid = "Error on generate Tripnum"
            '    End If
            'End If

            Dim qryha101 As String = "select Nik,Nama,TglMasuk,tglEfektif,EmpType,TglAwalKontrak,TglAkhirKontrak,KontrakKe,TglAwalTetap,Active,TglResign,KdSite,KdDepar,KdJabatan,KdLokasi,KdLevel,KdSubLevel,PointHire,Gender,TmpLahir,TglLahir,Agama,StSipil,StMarital,GolDarah,Alamat,Kota,KdPos,Propinsi,kdtelp,Telp,NoHP,Email,NoKTP,TglExpiredKTP,AlamatID,KotaID,PropinsiID,KdPosID,TypeSIM,NoSIM,TglExpiredSIM,ContactPerson,AlmPerson,TelpPerson,HubPerson,UkuranBaju,UkuranSepatu,UkuranCelana,Foto,NoMutasi,NoPromosi,NoDemosi,FExpatriate,FHousing,FFlexible,FSPL,FHitungOT,FSchedulingShift,Shift,FNotRP,TglTblCuti,TglExpLeave,FHangus,CutiLalu,CutiSkr,CutiCallIn,CutiTahunIni,Jatah,ELeaveDate,ELeaveExpDate,CutiPanjangLalu,CutiPanjang,CutiPanjangDiambil,SisaCutiPanjang,TglInitPL,PLeaveDate,PLeaveExpDate,PLeaveStatus,PLeaveReturnDate,PLeavePrevBalance,PLeaveEmerge,PLeaveClaimed,PLeaveRemaining,ClaimPercentage,PeriodeTimbulClaim,PeriodeLastClaimFrame,PeriodeLastClaimLens,OutpatientMax,OutpatientClaim,OutpatientSisa,InpatientMax,InpatientClaim,InpatientSisa,GlassesMonoNormalMax,GlassesMonoNormalClaim,GlassesMonoNormalSisa,GlassesMonoSilinderMax,GlassesMonoSilinderClaim,GlassesMonoSilinderSisa,GlassesBiNormalMax,GlassesBiNormalClaim,GlassesBiNormalSisa,GlassesBiSilinderMax,GlassesBiSilinderClaim,GlassesBiSilinderSisa,GlassesFrameMax,GlassesFrameClaim,GlassesFrameSisa,MaternityMax,MaternityClaim,MaternitySisa,nMaternity,CreatedBy,CreatedIn,CreatedTime,ModifiedBy,ModifiedIn,ModifiedTime,StEdit,DeleteBy,DeleteTime,FCover,FKirim,KdSubLevelRit,pos_code,old_nik,pycostcode,NIKSITE,heirPerson,heiralm,heirtelp,heirhub,email2 from H_A101 where nik = '" + txtnikx.Value + "'"
            Dim dtbha101 As DataTable = New DataTable
            Dim sdaha101 As SqlDataAdapter = New SqlDataAdapter(qryha101, conn)

            sdaha101.Fill(dtbha101)

            'NIK Nya Mba Nining 0001458
            ' Diganti ke Wibowo 0006319
            Dim Nikverby As String = "0001458"
            If Not dtbha101.Rows(0)!KdSite = "JKT" Then
                Nikverby = txtnikverby.Value
            End If


            sqlQuery = "insert into L_H001 (L_H001.transid , L_H001.kdsite, L_H001.nik, L_H001.trans_date, L_H001.kddepar, L_H001.kdjabatan," & _
                   "L_H001.datefrom, L_H001.dateto, L_H001.totleave, L_H001.remainleave, L_H001.eligiLeave, L_H001.cutabsent, " & _
                   "L_H001.typeLeave, L_H001.paidltype, L_H001.remarks, L_H001.pic_nik, L_H001.pic_jabat, L_H001.verify_nik, " & _
                   "L_H001.verify_jabat, L_H001.approve_nik, L_H001.approve_jabat, L_H001.fstatus, L_H001.CreatedBy, L_H001.CreatedIn, " & _
                   "L_H001.CreatedTime, L_H001.StEdit, " & _
                   "L_H001.yeara, L_H001.yearb, L_H001.atot, L_H001.btot, L_H001.holiday, L_H001.ref_fieldbreak, L_H001.StatusWF) Values " & _
                   "('" + permitid + "', '" + dtbha101.Rows(0)!KdSite + "', '" + dtbha101.Rows(0)!nik + "', " & _
                   "'" + txtdtreq.Text + "', '" + dtbha101.Rows(0)!KdDepar + "', '" + dtbha101.Rows(0)!KdJabatan + "'," & _
                   "'" + txtleafrom.Text + "', '" + txtleato.Text + "', '" + txttotleave.Text + "', '" + txtremleaveact.Value.ToString + "', " & _
                   "'" + txtleaeli.Text + "', '" + txtcutabs.Text + "', '" + leavetype.Value + "', " & _
                   "'" + paidleave.Value + "', '" + txarem.Text + "', '" + txtniksitedut.Value + "', '" + posdut.Value + "', " & _
                   "'" + Nikverby + "', '" + posverby.Value + "', '" + txtappby.Value + "', '" + posappby.Value + "', " & _
                   "'0', '" + Session("otorisasi") + "', '" + createdin + "', '" + dttm + "', '0', '" + hyeara.Value + "', '" + hyearb.Value + "', '" + htota.Value + "', '" + htotb.Value + "', '" + txtholiday.Text + "', '" + dayoff_tripid.Value + "', '')"

            cmd = New SqlCommand(sqlQuery, conn)
            cmd.ExecuteScalar()

            'alert = "<script type ='text/javascript' > alert('Data has been saved') </script>"
            Session("leaveid") = permitid
            Session("vleave") = permitid

            'Response.Redirect("leave_edit.aspx")
        Catch ex As Exception
            bcheck = False
        Finally

            If bcheck = True Then
                alert = "<script type ='text/javascript' > alert('Data has been saved') </script>"
                btnsave.Enabled = False
            Else
                alert = "<script type ='text/javascript' > alert('data save failed') </script>"
            End If

            conn.Close()
        End Try

        If bcheck = True Then
            Response.Redirect("leave_edit.aspx?save=1")
        End If
    End Sub

    Private Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        Response.Redirect("leave_entry.aspx")
    End Sub

    'Public Sub btnprint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnprint.Click
    '    Response.Redirect("cetak_leave.aspx?id=" + Session("leaveid"), False)
    'End Sub

    Private Sub validasilentry(ByVal pesan() As String)
        'If txtnik.Text <> "" Or txtnik.Text <> Nothing Or txtnama.Text <> "" Or txtnama.Text <> Nothing Or txtnama.Text <> "" _
        'Or txtdepar.Text <> "" Or txtdepar.Text <> Nothing Or txtposition.Text <> "" Or txtposition.Text <> Nothing _
        'Or txtleavetype.Text <> "" Or txtleavetype.Text <> Nothing Or txtdtreq.Text <> "" Or txtdtreq.Text <> Nothing _
        'Or txtleafrom.Text <> "" Or txtleafrom.Text <> Nothing Or txtleato.Text <> "" Or txtleato.Text <> Nothing _
        'Then
        '    bchek = True
        'Else
        '    bchek = False
        'End If

        If txtnik.Text <> "" Or txtnik.Text <> Nothing Then
            pesan(2) = "True"
        Else
            pesan(2) = "False"
            pesan(1) = "Please fill nik"
        End If

        If leavetype.Value = Nothing Then
            pesan(2) = "False"
            pesan(1) = "Please fill leave type"
        Else
            pesan(2) = "True"
        End If

        If Not leavetype.Value = "" Then
            If leavetype.Value = "03" And paidleave.Value = Nothing Then
                pesan(2) = "False"
                pesan(1) = "Please fill paid leave"
            Else
                pesan(2) = "True"
            End If

            If leavetype.Value <> "03" And paidleave.Value <> Nothing Then
                pesan(2) = "False"
                pesan(1) = "Please delete paid leave"
            Else
                pesan(2) = "True"
            End If
        End If

    End Sub

    Private Sub txtholiday_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtholiday.TextChanged

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click

    End Sub

End Class