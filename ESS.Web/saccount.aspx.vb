﻿Imports System.Data.SqlClient

Partial Public Class saccount
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            SearchUser()
        Catch ex As Exception

        End Try
    End Sub

    Sub SearchUser()
        Dim conn As String = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim SQLConn As New SqlConnection(conn)
        Try
            Dim SQLDBDataReader As SqlClient.SqlDataReader
            Dim SQLDataTable As New DataTable
            Dim SQLCmd As New SqlCommand()
            SQLCmd.CommandText = "SELECT IdUser,NmUser,Abbr as Departemen,Jobsite,Grup FROM muser LEFT JOIN mappdep on muser.Departemen = mappdep.Kode WHERE stedit='0'"
            SQLCmd.CommandType = CommandType.Text
            SQLCmd.Connection = SQLConn
            SQLConn.Open()
            SQLDBDataReader = SQLCmd.ExecuteReader()
            rptSearchResult.DataSource = SQLDBDataReader
            rptSearchResult.DataBind()
            SQLDBDataReader.Close()
            SQLDBDataReader = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            If SQLConn.State = ConnectionState.Open Then
                SQLConn.Close()
            End If
            SQLConn.Dispose()
            SQLConn = Nothing
        End Try
    End Sub

    Private Sub rptSearchResult_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSearchResult.ItemCommand
        Try
            Dim str As String = e.CommandArgument.ToString()
            If e.CommandName = "Delete" Then
                Dim _adapter As New ExcellentTableAdapters.MUSERTableAdapter
                _adapter.DeleteUser(Request.UserHostAddress, Request.UserHostName, Now, "2", str)
                SearchUser()
            ElseIf e.CommandName = "Edit" Then
                Response.Redirect("account.aspx?id=" & str, False)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

End Class