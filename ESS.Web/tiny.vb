﻿Namespace mywebsite
    Public Class tiny

    End Class

    Public Class TextEditor
        Inherits TextBox
        Protected Overrides Sub OnPreRender(ByVal e As EventArgs)
            Dim tinyMceIncludeKey As String = "TinyMCEInclude"
            Dim tinyMceIncludeScript As String = "<script type=""text/javascript"" src=""/tiny_mce/tiny_mce.js""></script>"

            If Not Page.ClientScript.IsStartupScriptRegistered(tinyMceIncludeKey) Then
                Page.ClientScript.RegisterStartupScript(Me.[GetType](), tinyMceIncludeKey, tinyMceIncludeScript)
            End If

            If Not Page.ClientScript.IsStartupScriptRegistered(GetInitKey()) Then
                Page.ClientScript.RegisterStartupScript(Me.[GetType](), GetInitKey(), GetInitScript())
            End If

            If Not CssClass.Contains(GetEditorClass()) Then
                'probably this is not the best way how to add the css class but I do not know any beter way
                If CssClass.Length > 0 Then
                    CssClass += " "
                End If
                CssClass += GetEditorClass()
            End If
            MyBase.OnPreRender(e)
        End Sub

        Private Function GetInitKey() As String
            Dim simpleKey As String = "TinyMCESimple"
            Dim fullKey As String = "TinyMCEFull"

            Select Case Mode
                Case TextEditorMode.Simple
                    Return simpleKey
                Case TextEditorMode.Full
                    Return fullKey
                Case Else
                    'goto case TextEditorMode.Simple
                    Return simpleKey
            End Select
        End Function

        Private Function GetEditorClass() As String
            Return GetEditorClass(Mode)
        End Function

        Private Function GetEditorClass(ByVal mode As TextEditorMode) As String
            Dim simpleClass As String = "SimpleTextEditor"
            Dim fullClass As String = "FullTextEditor"

            Select Case mode
                Case TextEditorMode.Simple
                    Return simpleClass
                Case TextEditorMode.Full
                    Return fullClass
                Case Else
                    'goto case TextEditorMode.Simple
                    Return simpleClass
            End Select
        End Function

        Private Function GetInitScript() As String
            Dim simpleScript As String = "<script language=""javascript"" type=""text/javascript"">tinyMCE.init({{mode : ""textareas"",theme : ""simple"",editor_selector : ""{0}""}});</script>"
            Dim fullScript As String = "<script language=""javascript"" type=""text/javascript"">tinyMCE.init({{mode : ""textareas"",theme : ""advanced"",editor_selector : ""{0}""}});</script>"

            Select Case Mode
                Case TextEditorMode.Simple
                    Return String.Format(simpleScript, GetEditorClass(TextEditorMode.Simple))
                Case TextEditorMode.Full
                    Return String.Format(fullScript, GetEditorClass(TextEditorMode.Full))
                Case Else
                    'goto case TextEditorMode.Simple
                    Return String.Format(simpleScript, GetEditorClass(TextEditorMode.Simple))
            End Select
        End Function

        Public ReadOnly Property TextMode() As TextBoxMode
            Get
                Return TextBoxMode.MultiLine
            End Get
        End Property

        Public Property Mode() As TextEditorMode
            Get
                Dim obj As [Object] = ViewState("Mode")
                If obj Is Nothing Then
                    Return TextEditorMode.Simple
                End If
                Return CType(obj, TextEditorMode)
            End Get
            Set(ByVal value As TextEditorMode)
                ViewState("Mode") = value
            End Set
        End Property

        Public Enum TextEditorMode
            Simple
            Full
        End Enum
    End Class
End Namespace