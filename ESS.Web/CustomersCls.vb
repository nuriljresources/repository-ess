﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Public Class CustomersCls
    Public Sub Insert(ByVal TransportTime As String, ByVal TransportDate As String, ByVal Fromx As String, ByVal TimeTo As String, ByVal DateTo As String, ByVal Toy As String, ByVal Purpose As String, ByVal myTable As DataTable)
        ' Write your own Insert statement blocks
        Dim row As DataRow
        row = myTable.NewRow()

        Dim no As String

        If myTable.Rows.Count = "1" And myTable.Rows(0)!No.ToString = "1" Then
            no = myTable.Rows.Count + 1
        ElseIf myTable.Rows.Count > "1" Then
            no = myTable.Rows.Count + 1
        Else
            no = myTable.Rows.Count
        End If

        row("No") = no
        row("Time") = TransportTime
        row("Date") = TransportDate
        row("From") = Fromx
        row("Timeto") = TimeTo
        row("Dateto") = DateTo
        row("To") = Toy
        row("Purpose") = Purpose

        myTable.Rows.Add(row)

        If Convert.IsDBNull(myTable.Rows(0)!Time) Then
            myTable.Rows.RemoveAt(0)
        End If
    End Sub
    Public Function Fetch() As DataTable
        ' Write your own Fetch statement blocks, this method should return a DataTable

        Dim myDataTable As DataTable = New DataTable()

        Dim myDataColumn As DataColumn

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "Time"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "Date"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "From"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "Timeto"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "Dateto"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "To"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "Purpose"
        myDataTable.Columns.Add(myDataColumn)

        Return myDataTable
    End Function
    Public Function FetchCustomerType() As DataTable
        ' Write your own Fetch statement blocks to fetch Customer Type from its master table and this method
        'should return a DataTable
    End Function
    Public Sub Update(ByVal no As Integer, ByVal TransportTime As String, ByVal TransportDate As String, ByVal fromx As String, ByVal transporttimeto As String, ByVal transportdateto As String, ByVal toy As String, ByVal purpose As String, ByVal myTable As DataTable, ByVal baris As String)
        ' Write your own Update statement blocks.
        Dim row As DataRow

        myTable.Rows(baris)!Time = TransportTime
        myTable.Rows(baris)!Date = TransportDate
        myTable.Rows(baris)!from = fromx
        myTable.Rows(baris)!timeto = transporttimeto
        myTable.Rows(baris)!dateto = transportdateto
        myTable.Rows(baris)!To = toy
        myTable.Rows(baris)!Purpose = purpose

    End Sub
    Public Sub Delete(ByVal CustomerCode As Integer, ByVal myTable As DataTable, ByVal baris As String)
        ' Write your own Delete statement blocks.
        Dim i As Integer

        myTable.Rows(baris).Delete()

        For i = 1 To myTable.Rows.Count - 1
            myTable.Rows(i)!No = i + 1
        Next

    End Sub

    Public Sub passangerinsert(ByVal pno As String, ByVal pnik As String, ByVal pname As String, ByVal ptype As String, ByVal myTable2 As DataTable)
        ' Write your own Insert statement blocks
        Dim row As DataRow
        row = myTable2.NewRow()

        If myTable2.Rows.Count = "1" And myTable2.Rows(0)!PNo.ToString = "1" Then
            pno = myTable2.Rows.Count + 1
        ElseIf myTable2.Rows.Count > "1" Then
            pno = myTable2.Rows.Count + 1
        Else
            pno = myTable2.Rows.Count
        End If

        row("PNo") = pno
        row("PNik") = pnik
        row("PName") = pname
        row("PType") = ptype

        myTable2.Rows.Add(row)

        If Convert.IsDBNull(myTable2.Rows(0)!Pno) Then
            myTable2.Rows.RemoveAt(0)
        End If
    End Sub

    Public Sub passangerupdate(ByVal pno As String, ByVal pnik As String, ByVal pname As String, ByVal ptype As String, ByVal myTable2 As DataTable, ByVal baris As String)
        myTable2.Rows(baris)!PNo = pno
        myTable2.Rows(baris)!PNik = pnik
        myTable2.Rows(baris)!PName = pname
        myTable2.Rows(baris)!PType = ptype

    End Sub

    Public Function Fetchpes() As DataTable
        Dim myDataTable As DataTable = New DataTable()

        Dim myDataColumn As DataColumn

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.Int32")
        myDataColumn.ColumnName = "PNo"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "PNik"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "PName"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "PType"
        myDataTable.Columns.Add(myDataColumn)

        Return myDataTable
    End Function

    Public Sub PDelete(ByVal CustomerCode As Integer, ByVal myTable2 As DataTable, ByVal baris As String)
        ' Write your own Delete statement blocks.
        Dim i As Integer

        myTable2.Rows(baris).Delete()

        For i = 1 To myTable2.Rows.Count - 1
            myTable2.Rows(i)!PNo = i + 1
        Next

    End Sub

    Public Sub EditInsert(ByVal TransportTime As String, ByVal TransportDate As String, ByVal Fromx As String, ByVal TimeTo As String, ByVal DateTo As String, ByVal Toy As String, ByVal Purpose As String, ByVal myTable As DataTable)
        ' Write your own Insert statement blocks
        Dim row As DataRow
        row = myTable.NewRow()

        Dim no As String

        If myTable.Rows.Count = "1" And myTable.Rows(0)!Genid.ToString = "1" Then
            no = myTable.Rows.Count + 1
        ElseIf myTable.Rows.Count > "1" Then
            no = myTable.Rows.Count + 1
        Else
            no = myTable.Rows.Count
        End If

        row("Genid") = no
        row("Timedes") = TransportTime
        row("des_Date") = TransportDate
        row("dep_From") = Fromx
        row("Timeto") = TimeTo
        row("to_Date") = DateTo
        row("des_to") = Toy
        row("Purpose") = Purpose

        myTable.Rows.Add(row)

        If Convert.IsDBNull(myTable.Rows(0)!Timedes) Then
            myTable.Rows.RemoveAt(0)
        End If
    End Sub

    Public Sub EditUpdate(ByVal no As Integer, ByVal TransportTime As String, ByVal TransportDate As String, ByVal fromx As String, ByVal transporttimeto As String, ByVal transportdateto As String, ByVal toy As String, ByVal purpose As String, ByVal myTable As DataTable, ByVal baris As String)
        ' Write your own Update statement blocks.
        Dim row As DataRow

        myTable.Rows(baris)!Timedes = TransportTime
        myTable.Rows(baris)!des_Date = TransportDate
        myTable.Rows(baris)!dep_From = fromx
        myTable.Rows(baris)!Timeto = transporttimeto
        myTable.Rows(baris)!to_Date = transportdateto
        myTable.Rows(baris)!des_to = toy
        myTable.Rows(baris)!Purpose = purpose

    End Sub

    Public Sub EditDelete(ByVal CustomerCode As Integer, ByVal myTable As DataTable, ByVal baris As String)
        ' Write your own Delete statement blocks.
        Dim i As Integer

        myTable.Rows(baris).Delete()
        myTable.AcceptChanges()

        For i = 1 To myTable.Rows.Count - 1
            myTable.Rows(i)!Genid = i + 1
        Next

    End Sub

    Public Sub editpassangerinsert(ByVal pno As String, ByVal pnik As String, ByVal pname As String, ByVal ptype As String, ByVal myTable2 As DataTable)
        ' Write your own Insert statement blocks
        Dim row As DataRow
        row = myTable2.NewRow()

        If myTable2.Rows.Count = "1" And Convert.ToString(myTable2.Rows(0)!Genid.ToString) = "1" Then
            pno = myTable2.Rows.Count + 1
        ElseIf myTable2.Rows.Count > "1" Then
            pno = myTable2.Rows.Count + 1
        Else
            pno = myTable2.Rows.Count
        End If

        row("Genid") = pno
        row("pes_nik") = pnik
        row("pes_name") = pname
        row("pes_type") = ptype

        myTable2.Rows.Add(row)

        If Convert.IsDBNull(myTable2.Rows(0)!Genid) Then
            myTable2.Rows.RemoveAt(0)
        End If
    End Sub

    Public Sub editpassangerupdate(ByVal pno As String, ByVal pnik As String, ByVal pname As String, ByVal ptype As String, ByVal myTable2 As DataTable, ByVal baris As String)
        myTable2.Rows(baris)!Genid = pno
        myTable2.Rows(baris)!pes_nik = pnik
        myTable2.Rows(baris)!pes_name = pname
        myTable2.Rows(baris)!pes_type = ptype
    End Sub

    Public Sub editPDelete(ByVal CustomerCode As Integer, ByVal myTableeditdelete As DataTable, ByVal baris As String)
        ' Write your own Delete statement blocks.
        Dim i As Integer

        myTableeditdelete.Rows(baris).Delete()
        myTableeditdelete.AcceptChanges()

        For i = 1 To myTableeditdelete.Rows.Count - 1
            myTableeditdelete.Rows(i)!Genid = i + 1
        Next

    End Sub
End Class
