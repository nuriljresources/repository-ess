﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="popupprint.aspx.vb" Inherits="EXCELLENT.popupprint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <script type="text/javascript">
       function fnTest() {
          document.getElementById("header1").innerHTML = window.opener.document.getElementById("ctl00_ContentPlaceHolder1_header1").innerHTML;
          document.getElementById("result").innerHTML = window.opener.document.getElementById("ctl00_ContentPlaceHolder1_result").innerHTML;
       for (var i = 0; i < document.getElementById("BSCTABLE").rows.length; i++) {
          oneRow = document.getElementById("BSCTABLE").rows[i]
          if (oneRow.cells.length != 1) {
             oneRow.deleteCell(oneRow.cells.length - 1);
          }
       }
       for (var i = 0; i < document.getElementById("BSCTABLE").rows.length; i++) {
          oneRow = document.getElementById("BSCTABLE").rows[i]
          if (oneRow.cells.length != 1) {
             oneRow.deleteCell(oneRow.cells.length - 1);
          }
       }
    }    
    </script>
    <style type="text/css">
       body{
         font-family: Calibri, Verdana, Ariel, sans-serif;
         font-size:20px;
       }
       input.print{
         border-style : none;
         font-size:14px;
         font-family: Calibri, Verdana, Ariel, sans-serif;
       }
       input{
         border-style : none;
         font-size:14px;
         font-family: Calibri, Verdana, Ariel, sans-serif;
       }
       
       input[type=button]{
         visibility:hidden;
       }
    
     table.bsc {
	      border-width: 1px;
	      border-spacing: 0px;
	      border-style: groove;
	      border-color: #C0C0C0;
	      border-collapse: collapse;
	      background-color: white;
	      font-family: Calibri, Verdana, Ariel, sans-serif;
	      font-size:14px;
         }
      table.bsc thead {
	      border-width: 1px;
	      padding: 1px;
	      border-style: inset;
	      border-color: #C0C0C0;
	       font-family: Calibri, Verdana, Ariel, sans-serif;
	      font-size:14px;
      }
      table.bsc td {
	      border-width: 1px;
	      padding: 1px;
	      border-style: inset;
	      border-color: #C0C0C0;
	       font-family: Calibri, Verdana, Ariel, sans-serif;
	      font-size:14px;
      }
      .headbsctable{
         font-size:16px;
         background-color:#449A50;
         color:white;
         font-weight:bold;
         font-family: Calibri, Verdana, Ariel, sans-serif;
      }
      #totalach{
         font-size:64px;
      }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="header1" align="center" style="font-size:20px;font-weight:bold;">
    </div>
    <div id="result" align="center" class="result">
    </div>
    </form>
     <script type="text/javascript">
        fnTest();
    </script>
</body>
</html>
