﻿Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms

Partial Public Class reportmaster
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") = "" Or Session("permission") = Nothing Then
            Response.Redirect("../Login.aspx", False)
            Exit Sub
        ElseIf ("1;2;3;4;5;6;7;8;A;B").IndexOf(Session("permission")) = -1 Then
            Response.Redirect("../default.aspx", False)
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim dr As SqlDataReader = Nothing
            Try
                Dim l As String = Request.QueryString("l")
                Dim y As String = Request.QueryString("y")
                Dim sqlQuery As String = ""
                Dim cmd As SqlCommand

                conn.Open()
                'Initialize Function
                sqlQuery = "select ID,FunctionName,active from MFunction  where active='1' order by ID"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "3" Or Session("permission") = "1" Or Session("permission") = "B" Then 'GRUP BOD
                        KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "8" Or Session("permission") = "4" Or Session("permission") = "5" Then 'PIC PDCA dan GM dan Manager dan MSD
                        If Session("bscdivisi") = dr(0) Then
                            KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Direktorat
                sqlQuery = "select ID,DirektoratName,active from MDirektorat where active='1' order by ID"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Then 'GRUP MDV
                        KPIDirektorat1.Items.Add(New ListItem(dr(1), dr(0)))
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Section
                sqlQuery = "select ID,SectionName,active,funcid from MSection order by ID"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Or Session("permission") = "2" Or Session("permission") = "3" Or Session("permission") = "6" Then 'GRUP MDV atau sekpro atau BOD
                        KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "7" Or Session("permission") = "A" Then 'GRUP S HEAD
                        If Session("bscdepar") = dr(0) Then
                            KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    ElseIf Session("permission") = "8" Or Session("permission") = "4" Or Session("permission") = "5" Then 'PIC PDCA dan GM dan Manager
                        If Session("bscdivisi") = dr(3) Then
                            KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Jobsite
                sqlQuery = "select id,kdsite,active from site order by id"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Or Session("permission") = "8" Or Session("permission") = "3" Or Session("permission") = "4" Or Session("permission") = "5" Then 'GRUP MDV & PIC PDCA & BOD & GM & MANAGER
                        KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "7" Or Session("permission") = "A" Or Session("permission") = "2" Or Session("permission") = "6" Then 'GRUP S HEAD & SEKPRO & PM
                        If Session("jobsite") = dr(1) Then
                            KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                End While
                dr.Close()
                dr = Nothing
                GenerateMenu()
                Select Case KPILevel.SelectedValue
                    Case 0
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                        KPIDirektorat1.Visible = False
                    Case 1
                        KPIFunc.Visible = True
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                        KPIDirektorat1.Visible = False
                    Case 2
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = True
                        KPISection.Visible = True
                        KPIDirektorat1.Visible = False
                    Case 3
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = True
                        KPISection.Visible = False
                    Case 4
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                        KPIDirektorat1.Visible = True
                End Select
                KPIYear.SelectedValue = Now.Year
            Catch ex As Exception
                lblMessage.Text = ex.Message
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        End If
    End Sub

    Protected Sub KPILevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles KPILevel.SelectedIndexChanged
        Try
            Select Case KPILevel.SelectedValue
                Case 0
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                    KPIDirektorat1.Visible = False
                Case 1
                    KPIFunc.Visible = True
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                    KPIDirektorat1.Visible = False
                Case 2
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = True
                    KPIDirektorat1.Visible = False
                Case 3
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = False
                    KPIDirektorat1.Visible = False
                Case 4
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                    KPIDirektorat1.Visible = True
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnView.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim sqlQuery As String = ""
            Select Case KPILevel.SelectedValue
                Case "0" 'Level BUMA
                    sqlQuery = "select perspective,level,'JRN' as slevel,'' as workunit,c.stid,c.stname as sst,isnull(e.jabatan,'') as soleader,a.name as kpi,a.owner as kpiowner,a.uom,b.so,a.year,versi,(case when jan = '' then null else jan end) as jan,(case when feb = '' then null else feb end) as feb,(case when mar = '' then null else mar end) as mar,(case when apr = '' then null else apr end) as apr,(case when mei = '' then null else mei end) as mei,(case when jun = '' then null else jun end) as jun,(case when jul = '' then null else jul end) as jul,(case when agu = '' then null else agu end) as agu,(case when sep = '' then null else sep end) as sep,(case when okt = '' then null else okt end) as okt,(case when nov = '' then null else nov end) as nov,(case when des = '' then null else des end) as des,annual,stedit " & _
                               "from MKPI a inner join MSO b on a.soid=b.id " & _
                               "inner join mst c on b.stid=c.stid " & _
                               "inner join mperspective d on c.prid=d.id " & _
                               "left join mjabatan e on b.bumaleader=e.id " & _
                               "left join (select kpiid,year,versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from( " & _
                               "select row_number() over(partition by kpiid order by year desc,kpiid,versi desc) as row,* from TTARGET " & _
                               ")a where row='1') tt on a.id=tt.kpiid " & _
                               "where level='0' and a.year='" & KPIYear.SelectedValue & "' " & _
                               "order by d.id,c.stid,b.id,a.id"
                Case "1" 'Level Function
                    sqlQuery = "select perspective,level,'JRN' as slevel,'' as workunit,c.stid,c.stname as sst,isnull(e.jabatan,'') as soleader,a.name as kpi,a.owner as kpiowner,a.uom,b.so,a.year,versi,(case when jan = '' then null else jan end) as jan,(case when feb = '' then null else feb end) as feb,(case when mar = '' then null else mar end) as mar,(case when apr = '' then null else apr end) as apr,(case when mei = '' then null else mei end) as mei,(case when jun = '' then null else jun end) as jun,(case when jul = '' then null else jul end) as jul,(case when agu = '' then null else agu end) as agu,(case when sep = '' then null else sep end) as sep,(case when okt = '' then null else okt end) as okt,(case when nov = '' then null else nov end) as nov,(case when des = '' then null else des end) as des,annual,stedit " & _
                               "from MKPI a inner join MSO b on a.soid=b.id " & _
                               "inner join mst c on b.stid=c.stid " & _
                               "inner join mperspective d on c.prid=d.id " & _
                               "left join mjabatan e on b.bumaleader=e.id " & _
                               "left join (select kpiid,year,versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from( " & _
                               "select row_number() over(partition by kpiid order by year desc,kpiid,versi desc) as row,* from TTARGET " & _
                               ")a where row='1') tt on a.id=tt.kpiid " & _
                               "where level='1' and a.func='" & KPIFunc.SelectedValue & "' and a.year='" & KPIYear.SelectedValue & "' " & _
                               "order by d.id,c.stid,b.id,a.id"
                Case "2" 'Level Section
                    sqlQuery = "select perspective,level,'JRN' as slevel,'' as workunit,c.stid,c.stname as sst,isnull(e.jabatan,'') as soleader,a.name as kpi,a.owner as kpiowner,a.uom,b.so,a.year,versi,(case when jan = '' then null else jan end) as jan,(case when feb = '' then null else feb end) as feb,(case when mar = '' then null else mar end) as mar,(case when apr = '' then null else apr end) as apr,(case when mei = '' then null else mei end) as mei,(case when jun = '' then null else jun end) as jun,(case when jul = '' then null else jul end) as jul,(case when agu = '' then null else agu end) as agu,(case when sep = '' then null else sep end) as sep,(case when okt = '' then null else okt end) as okt,(case when nov = '' then null else nov end) as nov,(case when des = '' then null else des end) as des,annual,stedit " & _
                               "from MKPI a inner join MSO b on a.soid=b.id " & _
                               "inner join mst c on b.stid=c.stid " & _
                               "inner join mperspective d on c.prid=d.id " & _
                               "left join mjabatan e on b.bumaleader=e.id " & _
                               "left join (select kpiid,year,versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from( " & _
                               "select row_number() over(partition by kpiid order by year desc,kpiid,versi desc) as row,* from TTARGET " & _
                               ")a where row='1') tt on a.id=tt.kpiid " & _
                               "where level='2' and a.jobsite='" & KPIJobsite.SelectedValue & "' and a.section='" & KPISection.SelectedValue & "'  and a.year='" & KPIYear.SelectedValue & "' " & _
                               "order by d.id,c.stid,b.id,a.id"
                Case "3" 'Level Jobsite
                    sqlQuery = "select perspective,level,'JRN' as slevel,'' as workunit,c.stid,c.stname as sst,isnull(e.jabatan,'') as soleader,a.name as kpi,a.owner as kpiowner,a.uom,b.so,a.year,versi,(case when jan = '' then null else jan end) as jan,(case when feb = '' then null else feb end) as feb,(case when mar = '' then null else mar end) as mar,(case when apr = '' then null else apr end) as apr,(case when mei = '' then null else mei end) as mei,(case when jun = '' then null else jun end) as jun,(case when jul = '' then null else jul end) as jul,(case when agu = '' then null else agu end) as agu,(case when sep = '' then null else sep end) as sep,(case when okt = '' then null else okt end) as okt,(case when nov = '' then null else nov end) as nov,(case when des = '' then null else des end) as des,annual,stedit " & _
                               "from MKPI a inner join MSO b on a.soid=b.id " & _
                               "inner join mst c on b.stid=c.stid " & _
                               "inner join mperspective d on c.prid=d.id " & _
                               "left join mjabatan e on b.bumaleader=e.id " & _
                               "left join (select kpiid,year,versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from( " & _
                               "select row_number() over(partition by kpiid order by year desc,kpiid,versi desc) as row,* from TTARGET " & _
                               ")a where row='1') tt on a.id=tt.kpiid " & _
                               "where level='3' and a.jobsite='" & KPIJobsite.SelectedValue & "' and a.year='" & KPIYear.SelectedValue & "' " & _
                               "order by d.id,c.stid,b.id,a.id"
                Case "4"
                    sqlQuery = "select perspective,level,'JRN' as slevel,'' as workunit,c.stid,c.stname as sst,isnull(e.jabatan,'') as soleader,a.name as kpi,a.owner as kpiowner,a.uom,b.so,a.year,versi,(case when jan = '' then null else jan end) as jan,(case when feb = '' then null else feb end) as feb,(case when mar = '' then null else mar end) as mar,(case when apr = '' then null else apr end) as apr,(case when mei = '' then null else mei end) as mei,(case when jun = '' then null else jun end) as jun,(case when jul = '' then null else jul end) as jul,(case when agu = '' then null else agu end) as agu,(case when sep = '' then null else sep end) as sep,(case when okt = '' then null else okt end) as okt,(case when nov = '' then null else nov end) as nov,(case when des = '' then null else des end) as des,annual,stedit " & _
                               "from MKPI a inner join MSO b on a.soid=b.id " & _
                               "inner join mst c on b.stid=c.stid " & _
                               "inner join mperspective d on c.prid=d.id " & _
                               "left join mjabatan e on b.bumaleader=e.id " & _
                               "left join (select kpiid,year,versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from( " & _
                               "select row_number() over(partition by kpiid order by year desc,kpiid,versi desc) as row,* from TTARGET " & _
                               ")a where row='1') tt on a.id=tt.kpiid " & _
                               "where level='4' and a.dir='" & KPIDirektorat1.SelectedValue & "' and a.year='" & KPIYear.SelectedValue & "' " & _
                               "order by d.id,c.stid,b.id,a.id"
            End Select

            Dim da As New SqlDataAdapter(sqlQuery, conn)
            Dim dt As New bscds.masterbscDataTable()
            da.Fill(dt)
            Dim plist As New Generic.List(Of ReportParameter)
            Dim param1 As ReportParameter = Nothing
            Dim result As String = ""
            Select Case KPILevel.SelectedValue
                Case "0"
                    param1 = New ReportParameter("judul", "BSC JRN")
                Case "1"
                    result = "BSC Function " & KPIFunc.SelectedItem.Text & ""
                    param1 = New ReportParameter("judul", result)
                Case "2"
                    result = "BSC Jobsite " & KPIJobsite.SelectedItem.Text & " Section " & KPISection.SelectedItem.Text & ""
                    param1 = New ReportParameter("judul", "BSC Jobsite")
                Case "3"
                    result = "BSC Jobsite " & KPIJobsite.SelectedItem.Text & ""
                    param1 = New ReportParameter("judul", result)
                Case "4"
                    result = "BSC Direktorat " & KPIDirektorat1.SelectedItem.Text & ""
                    param1 = New ReportParameter("judul", result)
            End Select
            plist.Add(param1)
            rpt.LocalReport.ReportPath = "Laporan\rptMasterBSC.rdlc"
            rpt.LocalReport.DataSources.Clear()
            rpt.LocalReport.SetParameters(plist)
            rpt.LocalReport.DataSources.Add(New ReportDataSource("bscds_masterbsc", dt))
            rpt.LocalReport.Refresh()

        Catch ex As Exception
            lblMessage.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Sub GenerateMenu()
        Select Case Session("permission")
            Case "1" 'MSD
            Case "B" 'MSD
            Case "2" 'Sekpro
                For Each x As ListItem In KPILevel.Items
                    If x.Value <> "2" And x.Value <> "3" Then
                        x.Enabled = False
                    ElseIf x.Value = "3" Then
                        x.Selected = True
                    End If
                Next
            Case "3" 'BOD
            Case "4" 'GM
                For Each x As ListItem In KPILevel.Items
                    If x.Value <> "1" And x.Value <> "2" And x.Value <> "3" Then
                        x.Enabled = False
                    ElseIf x.Value = "1" Then
                        x.Selected = True
                    End If
                Next
            Case "5" 'MGR
                For Each x As ListItem In KPILevel.Items
                    If x.Value <> "1" And x.Value <> "2" And x.Value <> "3" Then
                        x.Enabled = False
                    ElseIf x.Value = "1" Then
                        x.Selected = True
                    End If
                Next
            Case "6" 'PM
                For Each x As ListItem In KPILevel.Items
                    If x.Value <> "2" And x.Value <> "3" Then
                        x.Enabled = False
                    ElseIf x.Value = "2" Then
                        x.Selected = True
                    End If
                Next
            Case "7" 'SHEAD
                For Each x As ListItem In KPILevel.Items
                    If x.Value <> "2" And x.Value <> "3" Then
                        x.Enabled = False
                    ElseIf x.Value = "2" Then
                        x.Selected = True
                    End If
                Next
            Case "A" 'SHEAD Admin
                For Each x As ListItem In KPILevel.Items
                    If x.Value <> "2" And x.Value <> "3" Then
                        x.Enabled = False
                    ElseIf x.Value = "2" Then
                        x.Selected = True
                    End If
                Next
            Case "8"
                For Each x As ListItem In KPILevel.Items
                    If x.Value <> "1" And x.Value <> "2" Then
                        x.Enabled = False
                    ElseIf x.Value = "1" Then
                        x.Selected = True
                    End If
                Next
        End Select
    End Sub
End Class