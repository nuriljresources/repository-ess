﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Services
Imports System.Security.Cryptography

Partial Public Class dw_trip_header
    Inherits System.Web.UI.Page
    Public tripno As String
    Public genid As String
    Public genid2 As String
    Public alert As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") = "" Or Session("permission") = Nothing Then
            Response.Redirect("Login.aspx", False)
            Exit Sub
        End If

        'Dim dtmnow As String
        'Dim dtynow As String
        'Dim stripno As String
        'Dim i As Integer

        'Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        'Try
        '    If Session("permission") = "" Or Session("permission") = "M" Or Session("permission") = Nothing Then
        '        Response.Redirect("Login.aspx", False)
        '        Exit Sub
        '    End If
        '    conn.Open()

        '    dtmnow = DateTime.Now.ToString("MM")
        '    dtynow = Date.Now.Year.ToString
        '    stripno = dtynow + dtmnow + "/" + "T" + "/"

        '    Dim dtb As DataTable = New DataTable()
        '    Dim strcon As String = "select TripNo from V_H001 where tripno like '%" + stripno + "%'"
        '    Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, conn)
        '    sda.Fill(dtb)

        '    If dtb.Rows.Count = 0 Then
        '        tripno = stripno + "000001"
        '    ElseIf dtb.Rows.Count > 0 Then
        '        i = 0
        '        i = dtb.Rows.Count + 1
        '        If dtb.Rows.Count > 0 And dtb.Rows.Count < 9 Then
        '            tripno = stripno + "00000" + i.ToString
        '        ElseIf dtb.Rows.Count >= 9 And dtb.Rows.Count < 99 Then
        '            tripno = stripno + "0000" + i.ToString
        '        ElseIf dtb.Rows.Count >= 99 And dtb.Rows.Count < 999 Then
        '            tripno = stripno + "000" + i.ToString
        '        ElseIf dtb.Rows.Count >= 999 And dtb.Rows.Count < 9999 Then
        '            tripno = stripno + "00" + i.ToString
        '        ElseIf dtb.Rows.Count >= 9999 And dtb.Rows.Count < 99999 Then
        '            tripno = stripno + "0" + i.ToString
        '        ElseIf dtb.Rows.Count >= 99999 And dtb.Rows.Count < 999999 Then
        '            tripno = stripno + i.ToString
        '        ElseIf dtb.Rows.Count >= 999999 Then
        '            tripno = "Error on generate Tripnum"
        '        End If
        '    End If
        '    conn.Close()
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try

        'tripno = Session("tripno")
        reqdate.Text = Date.Today().ToString("MM/dd/yyyy")
        hcreatedby.Value = Session("otorisasi").ToString
        Try
            Dim conn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
            conn2.Open()
            Dim dtb2 As DataTable = New DataTable()
            Dim strcon2 As String = "Select max(genid) as genid from V_H00102"
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, conn2)
            sda2.Fill(dtb2)

            If dtb2.Rows.Count = 1 Then
                genid = dtb2.Rows(0)!genid
            Else
                genid = 0
            End If

            conn2.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Try
            Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
            con.Open()
            Dim dtb As DataTable = New DataTable()
            Dim strcon As String = "Select max(genid) as genid from V_H00106"

            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, con)
            sda.Fill(dtb)

            If dtb.Rows.Count = 1 Then
                genid2 = dtb.Rows(0)!genid
            Else
                genid2 = 0
            End If
        Catch ex As Exception

        End Try

        'Try
        '    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        '    con.Open()
        '    Dim dtb2 As DataTable = New DataTable()
        '    Dim strcon As String = "select curr_code, curr_name from V_A002"
        '    Dim i As Integer
        '    Dim selcurr As String = ""

        '    Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, con)
        '    sda.Fill(dtb2)

        '    If dtb2.Rows.Count = 1 Then

        '        For i = 0 To dtb2.Rows.Count
        '            selcurr = selcurr + "<option value='" + dtb2.Rows(i)!curr_code + "'>" + dtb2.Rows(i)!curr_name + "</option>"
        '        Next
        '    Else

        '    End If

        'Catch ex As Exception

        'End Try

        'If Not Page.IsPostBack Then
        '    Me.Page.Form.Enctype = "multipart/form-data"
        '    If Request.QueryString("id") IsNot Nothing Then
        '        ClientScript.RegisterStartupScript(GetType(Page), "ff", "<script>getrapat('" & Request.QueryString("id") & "');</script>")
        '    Else
        '        ClientScript.RegisterStartupScript(GetType(Page), "ff", "<script>addPToTable();addRowToTable();</script>")
        '    End If
        'End If
    End Sub

    Public Sub cetak_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cetak.Click
        Dim strnik As String
        strnik = Request.Form("nik")
        Response.Redirect("bussiness_trip_report_.aspx?id=" + Session("tripno"), False)
        'If Trim(strnik) <> "" Then

        'Else
        '    ScriptManager.RegisterStartupScript(Me, [GetType](), "showalert", "alert('Please Entry Your Data');", True)
        'End If

    End Sub

    'Public Sub cetak2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cetak2.Click
    '   Response.Redirect("cash_advance_req.aspx?ID=" + Session("tripno"))
    'tripno = Session("tripno")
    'Response.Write("<script language='javascript'>window.open(cash_advance_req.aspx?ID=<%=tripno%>, '_blank');</script>")
    'End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
        Dim conn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)

        Dim sqlQuery As String
        Dim cmd As SqlCommand
        Dim bcheck As Boolean = True

        Try
            conn.Open()

            'Review By (ex: jakarta= Andreas Saragih (JRN0324) nik 0001807)
            Dim ReviewBy As String = "0001807"

            'Proceed By (ex : jakarta= Dessy (JRN0071) nik 0001296)
            Dim ProceedBy As String = "0001296"

            'VP Approval
            Dim VPApproval As String = String.Empty
            Dim sqllh001 As String = "SELECT TOP 1 nik from H_A101 where NIKSITE in (SELECT vpniksite FROM V_ESS_BTR_APPROVAL_VP  where nik=(SELECT TOP 1 nik FROM V_H001 WHERE tripNo = '{0}'))"
            sqllh001 = String.Format(sqllh001, Session("tripno"))

            Dim dtblh001 As DataTable = New DataTable
            Dim sdalh001 As SqlDataAdapter = New SqlDataAdapter(sqllh001, conn)

            sdalh001.Fill(dtblh001)

            If dtblh001.Rows.Count > 0 Then
                VPApproval = dtblh001.Rows(0)!nik.ToString
            End If
            conn.Close()


            conn2.Open()
            sqlQuery = "update V_H001 set StatusWF = 'INT', reviewby = '{0}', proceedby = '{1}', apprby = '{2}' where tripNo = '{3}'"
            sqlQuery = String.Format(sqlQuery, ReviewBy, ProceedBy, VPApproval, Session("tripno"))

            cmd = New SqlCommand(sqlQuery, conn2)
            cmd.ExecuteScalar()
            conn2.Close()

        Catch ex As Exception
            bcheck = False
        Finally

            If bcheck = True Then
                alert = "<script type ='text/javascript' > alert('Data has been updated') </script>"
                btnSubmit.Enabled = False
            Else
                alert = "<script type ='text/javascript' > alert('data updated failed') </script>"
            End If

        End Try

    End Sub


End Class