﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="rpt_expired_1month.aspx.vb" Inherits="EXCELLENT.rpt_expired_1month" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" Height="697px" Width="770px">
            <LocalReport ReportPath="permit\permit_report\report_expired_doc_1month.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource1" 
                        Name="Dspermit_docpermit_trans" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:jrn_permit %>" 
            SelectCommand="SELECT id, f_name, vendor_code, ftype, fsubject, kdcom, company_name, kdsite, site_name, fyear, fcategory, doctype, department, Subdepartment, Number, must_report, cms_start, cms_end, cms_actual, terms_code, rem_code, status_reminder, status_contract, pic1, pic1_mail, pic2, pic2_mail, pic3, pic3_mail, pic4, pic4_mail, pic5, pic5_mail, remarks, f_path, CreatedBy, CreatedIn, CreatedTime, ModifiedBy, ModifiedIn, ModifiedTime, StEdit, DeleteBy, DeleteTime, jobcode, costcode FROM docpermit_trans WHERE (cms_end BETWEEN GETDATE() AND GETDATE() + 30 OR GETDATE() > cms_end) AND (StEdit &lt;&gt; '2') AND (modul = @Param2)">
            <SelectParameters>
                <asp:SessionParameter Name="Param2" SessionField="modul_permit" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            DeleteMethod="Delete" InsertMethod="Insert" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            TypeName="EXCELLENT.DspermitTableAdapters.docpermit_transTableAdapter" 
            UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_id" Type="String" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="f_name" Type="String" />
                <asp:Parameter Name="vendor_code" Type="String" />
                <asp:Parameter Name="ftype" Type="String" />
                <asp:Parameter Name="fsubject" Type="String" />
                <asp:Parameter Name="kdcom" Type="String" />
                <asp:Parameter Name="company_name" Type="String" />
                <asp:Parameter Name="kdsite" Type="String" />
                <asp:Parameter Name="site_name" Type="String" />
                <asp:Parameter Name="fyear" Type="String" />
                <asp:Parameter Name="fcategory" Type="String" />
                <asp:Parameter Name="doctype" Type="String" />
                <asp:Parameter Name="department" Type="String" />
                <asp:Parameter Name="Subdepartment" Type="String" />
                <asp:Parameter Name="Number" Type="Int32" />
                <asp:Parameter Name="must_report" Type="Int32" />
                <asp:Parameter Name="cms_start" Type="DateTime" />
                <asp:Parameter Name="cms_end" Type="DateTime" />
                <asp:Parameter Name="cms_actual" Type="DateTime" />
                <asp:Parameter Name="terms_code" Type="String" />
                <asp:Parameter Name="rem_code" Type="String" />
                <asp:Parameter Name="status_reminder" Type="String" />
                <asp:Parameter Name="status_contract" Type="String" />
                <asp:Parameter Name="pic1" Type="String" />
                <asp:Parameter Name="pic1_mail" Type="String" />
                <asp:Parameter Name="pic2" Type="String" />
                <asp:Parameter Name="pic2_mail" Type="String" />
                <asp:Parameter Name="pic3" Type="String" />
                <asp:Parameter Name="pic3_mail" Type="String" />
                <asp:Parameter Name="pic4" Type="String" />
                <asp:Parameter Name="pic4_mail" Type="String" />
                <asp:Parameter Name="pic5" Type="String" />
                <asp:Parameter Name="pic5_mail" Type="String" />
                <asp:Parameter Name="remarks" Type="String" />
                <asp:Parameter Name="f_path" Type="String" />
                <asp:Parameter Name="CreatedBy" Type="String" />
                <asp:Parameter Name="CreatedIn" Type="String" />
                <asp:Parameter Name="CreatedTime" Type="DateTime" />
                <asp:Parameter Name="ModifiedBy" Type="String" />
                <asp:Parameter Name="ModifiedIn" Type="String" />
                <asp:Parameter Name="ModifiedTime" Type="DateTime" />
                <asp:Parameter Name="StEdit" Type="String" />
                <asp:Parameter Name="DeleteBy" Type="String" />
                <asp:Parameter Name="DeleteTime" Type="DateTime" />
                <asp:Parameter Name="jobcode" Type="String" />
                <asp:Parameter Name="costcode" Type="String" />
                <asp:Parameter Name="Original_id" Type="String" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="id" Type="String" />
                <asp:Parameter Name="f_name" Type="String" />
                <asp:Parameter Name="vendor_code" Type="String" />
                <asp:Parameter Name="ftype" Type="String" />
                <asp:Parameter Name="fsubject" Type="String" />
                <asp:Parameter Name="kdcom" Type="String" />
                <asp:Parameter Name="company_name" Type="String" />
                <asp:Parameter Name="kdsite" Type="String" />
                <asp:Parameter Name="site_name" Type="String" />
                <asp:Parameter Name="fyear" Type="String" />
                <asp:Parameter Name="fcategory" Type="String" />
                <asp:Parameter Name="doctype" Type="String" />
                <asp:Parameter Name="department" Type="String" />
                <asp:Parameter Name="Subdepartment" Type="String" />
                <asp:Parameter Name="Number" Type="Int32" />
                <asp:Parameter Name="must_report" Type="Int32" />
                <asp:Parameter Name="cms_start" Type="DateTime" />
                <asp:Parameter Name="cms_end" Type="DateTime" />
                <asp:Parameter Name="cms_actual" Type="DateTime" />
                <asp:Parameter Name="terms_code" Type="String" />
                <asp:Parameter Name="rem_code" Type="String" />
                <asp:Parameter Name="status_reminder" Type="String" />
                <asp:Parameter Name="status_contract" Type="String" />
                <asp:Parameter Name="pic1" Type="String" />
                <asp:Parameter Name="pic1_mail" Type="String" />
                <asp:Parameter Name="pic2" Type="String" />
                <asp:Parameter Name="pic2_mail" Type="String" />
                <asp:Parameter Name="pic3" Type="String" />
                <asp:Parameter Name="pic3_mail" Type="String" />
                <asp:Parameter Name="pic4" Type="String" />
                <asp:Parameter Name="pic4_mail" Type="String" />
                <asp:Parameter Name="pic5" Type="String" />
                <asp:Parameter Name="pic5_mail" Type="String" />
                <asp:Parameter Name="remarks" Type="String" />
                <asp:Parameter Name="f_path" Type="String" />
                <asp:Parameter Name="CreatedBy" Type="String" />
                <asp:Parameter Name="CreatedIn" Type="String" />
                <asp:Parameter Name="CreatedTime" Type="DateTime" />
                <asp:Parameter Name="ModifiedBy" Type="String" />
                <asp:Parameter Name="ModifiedIn" Type="String" />
                <asp:Parameter Name="ModifiedTime" Type="DateTime" />
                <asp:Parameter Name="StEdit" Type="String" />
                <asp:Parameter Name="DeleteBy" Type="String" />
                <asp:Parameter Name="DeleteTime" Type="DateTime" />
                <asp:Parameter Name="jobcode" Type="String" />
                <asp:Parameter Name="costcode" Type="String" />
            </InsertParameters>
        </asp:ObjectDataSource>
    
    </div>
    </form>
</body>
</html>
