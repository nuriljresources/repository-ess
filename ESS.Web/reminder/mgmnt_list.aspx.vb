﻿Imports System.Data.SqlClient
Partial Public Class mgmnt_list
    Inherits System.Web.UI.Page
    Public mnentry As String
    Public mnview As String
    Public mnmgmt As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim dept As String
        Dim strcon As String

        If Session("otorisasi_permit") = Nothing Or Session("otorisasi_permit") = "" Then
            Response.Redirect("signin_permit.aspx")
        End If

        If Trim(Session("permission_permit")) = "1" Or Trim(Session("permission_permit")) = "2" Or Trim(Session("permission_permit")) = "4" Then
            mnentry = "<div id='mnentry'><img alt='entry document' src='../images/open_doc_small.png' /> <a href='doc_center_entry.aspx'>&nbsp;Set Document</a></div>"
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
        End If

        If Trim(Session("permission_permit")) = "1" Then
            mnmgmt = "<div id='mnmgmt'> <img alt='Management User' src='../images/open_doc_small.png' /> <a href='mgmnt_list.aspx'>&nbsp;Management User</a></div>"
        End If

        If Trim(Session("permission_permit")) = "3" Or Trim(Session("permission_permit")) = "4" Then
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View List</a></div>"
        End If

        Try
            Dim strcon2 As String = "Select * from ms_user where stedit <> '2' and emp_code = '" + Session("otorisasi_permit") + "' and modul = '" + Session("modul_permit") + "'"
            Dim dtb2 As DataTable = New DataTable
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn)

            sda2.Fill(dtb2)

            If dtb2.Rows(0)!modul.ToString = "IT" Then
                'strcon = "select emp_code, name, modul, (case lvl when '1' then 'Owner' when '2' then 'Doc Admin' when '3' then 'User' when '4' then 'PIC' end) as lvl from ms_user order by modul"
                strcon = "select emp_code, name, modul, (case lvl when '1' then 'Owner' when '2' then 'Doc Admin' when '3' then 'Cross' when '4' then 'PIC' end) as lvl, case schmail when '01' then 'Daily' when '02' then 'Weekly' when '03' then 'Monthly' end as schmail from ms_user where stedit <> '2' and modul = '" + dtb2.Rows(0)!modul.ToString + "' order by modul"
            Else
                strcon = "select emp_code, name, modul, (case lvl when '1' then 'Owner' when '2' then 'Doc Admin' when '3' then 'Cross' when '4' then 'PIC' end) as lvl, case schmail when '01' then 'Daily' when '02' then 'Weekly' when '03' then 'Monthly' end as schmail from ms_user where stedit <> '2' and modul = '" + dtb2.Rows(0)!modul.ToString + "' order by modul"
            End If

            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)

            sda.Fill(dtb)
            GridView1.DataSource = dtb
            GridView1.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged

        If GridView1.SelectedRow Is Nothing Then Exit Sub

        Dim row As GridViewRow = GridView1.SelectedRow
        Session("docedituser") = GridView1.DataKeys(row.RowIndex).Value.ToString
        'Response.Redirect("view_trip.aspx?ID=" & GridView1.DataKeys(row.RowIndex).Value.ToString)
        Response.Redirect("mgmnt_user_edit.aspx")

    End Sub
End Class