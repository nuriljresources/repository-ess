﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="searchtrip.aspx.vb" Inherits="EXCELLENT.searchtrip" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head  id= "head1" runat="server">
    <title>Search Trip</title>
    <link href="css/site.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            function UseSelected(Tripno, nik, nama, kddepar, kdjabat, kdsite, costcode, sup_nik, fstatus, superior, nmjabat, nmdepar, costname, detailtrip) {
          var pw = window.opener;
          var inputFrm = pw.document.forms['aspnetForm'];
          if (inputFrm.elements['ctrlToFind'].value == 'Originator') {
             inputFrm.elements['ctl00_ContentPlaceHolder1_Originator'].value = nopeg;
             inputFrm.elements['ctl00_ContentPlaceHolder1_OriginatorName'].value = nama;
          } else if (inputFrm.elements['ctrlToFind'].value == 'user') {
               inputFrm.elements['nik'].value = nopeg;
               inputFrm.elements['nama'].value = nama;
//               inputFrm.elements['kddepar'].value = kddepar;
               inputFrm.elements['kdcabang'].value = kdcabang;
           } else if (inputFrm.elements['ctrlToFind'].value == 'bidang') {
               inputFrm.elements['kddepar'].value = bidang_code;
           } else if (inputFrm.elements['ctrlToFind'].value == 'dec') {
           inputFrm.elements['tripno'].value = Tripno;
           inputFrm.elements['nik'].value = nik;
           inputFrm.elements['nama'].value = nama;
           inputFrm.elements['kddepar'].value = kddepar;
           inputFrm.elements['nmjabat'].value = kdjabat;
           inputFrm.elements['site'].value = kdsite;
           inputFrm.elements['costcode'].value = costcode;
           inputFrm.elements['snik'].value = sup_nik;
           inputFrm.elements['fstatus'].value = fstatus;
           inputFrm.elements['dep'].value = nmdepar;
           inputFrm.elements['superior'].value = superior;
           inputFrm.elements['position'].value = nmjabat;
           inputFrm.elements['costcd'].value = costname;
           inputFrm.elements['type'].value = detailtrip;
           } else {
            inputFrm.elements[inputFrm.elements['ctrlToFind'].value].value = nopeg;
            inputFrm.elements[inputFrm.elements['ctrlToFind'].value + 'name'].value = nama;   
          }
          window.close();
       }
       function Left(str, n) {
          if (n <= 0)
             return "";
          else if (n > String(str).length)
             return str;
          else
             return String(str).substring(0, n);
       }

       function Right(str, n) {
          if (n <= 0)
             return "";
          else if (n > String(str).length)
             return str;
          else {
             var iLen = String(str).length;
             return String(str).substring(iLen, iLen - n);
          }
      }

      function keyPressed(e) {
         switch (e.keyCode) {
            case 13:
               {
                  document.getElementById("btnSearch").click();
                  break;
               }
             default:
               {
                  break;
               }
         }
     }

     function cekArgumen() {
        var d = document;
        if ((d.getElementById("SearchKey").value.length < 3) || (d.getElementById("SearchKey").value.length == 0)){
            alert("Please Entry Specific NIK / Name (minimal 3 Character)");
            return false;
        }
     }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager  ID="ToolkitScriptManager1" runat="server"/>
   <asp:UpdatePanel id="MyUpdatePanel" runat="server">
    <ContentTemplate>
      <div>           

      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
         <caption style="text-align:center">Search Field Code</caption>
         <tr>
         <td>
         </td>
         </tr>
         <tr>
         <td>
         <asp:Repeater ID="rptPages" Runat="server">
      <HeaderTemplate>
      <table cellpadding="0" cellspacing="0" border="0">
      <tr>
         <td style="vertical-align:top"><b>Page:</b>&nbsp;</td>
         <td>
      </HeaderTemplate>
      <ItemTemplate>
                        <asp:LinkButton ID="btnPage" CommandName="Page" 
                         CommandArgument="<%# Container.DataItem %>" 
                         CssClass="text" 
                         Runat="server" 
                         Visible='<%# (CInt(Container.DataItem) <> (PageNumber+1)).ToString()%>'>
                         <%#Container.DataItem%> 
                         </asp:LinkButton>
                         
                         <asp:Label ID="lblPage" runat="server" Visible='<%# (CInt(Container.DataItem) = (PageNumber+1)).ToString()%>' 
                         Text="<%#Container.DataItem%>" 
                         ForeColor="black" Font-Bold="true">
                         </asp:Label>
      </ItemTemplate>
      <FooterTemplate>
         </td>
      </tr>
      </td>
      </table>
      </FooterTemplate>
      </asp:Repeater>
         </tr>
         <tr>
            <td>
               <asp:Repeater ID="rptResult" runat="server">
               <HeaderTemplate>
                           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table-inside">
                           <tr>               
                                
                                <th>No</th>
                                <th>Trip No.</th>
                                <th>Name</th>
                                <th>Purpose</th>
                                <%--
                                
                                <th>Department</th>
                                <th>Jobsite</th>--%>
                           </tr>
                        </HeaderTemplate>  
                        <ItemTemplate>
                       <tr>
                        <td><%#Container.ItemIndex + 1%></td>
                        <td><a href="#" onclick="UseSelected('<%#DataBinder.Eval(Container.DataItem, "Tripno")%>','<%#DataBinder.Eval(Container.DataItem, "nik")%>','<%#DataBinder.Eval(Container.DataItem, "nama")%>','<%#DataBinder.Eval(Container.DataItem, "kddepar")%>','<%#DataBinder.Eval(Container.DataItem, "kdjabat")%>','<%#DataBinder.Eval(Container.DataItem, "kdsite")%>','<%#DataBinder.Eval(Container.DataItem, "costcode")%>','<%#DataBinder.Eval(Container.DataItem, "sup_nik")%>','<%#DataBinder.Eval(Container.DataItem, "fstatus")%>','<%#DataBinder.Eval(Container.DataItem, "superior")%>','<%#DataBinder.Eval(Container.DataItem, "nmjabat")%>','<%#DataBinder.Eval(Container.DataItem, "nmdepar")%>','<%#DataBinder.Eval(Container.DataItem, "costname")%>','<%#DataBinder.Eval(Container.DataItem, "detailtrip")%>');"><%#DataBinder.Eval(Container.DataItem, "Tripno")%></a></td>
                        <td><%#DataBinder.Eval(Container.DataItem, "nama")%></td>
                        <td><%#DataBinder.Eval(Container.DataItem, "purpose")%></a></td>
                        <%--<td align="center"><%#DataBinder.Eval(Container.DataItem, "bidang_name")%></td>--%>
                        <%--<td><%#DataBinder.Eval(Container.DataItem, "NmDepar")%></td>--%>
                        </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                           </table>
                        </FooterTemplate>
               </asp:Repeater>             
            </td>
         </tr>
      
      </table>  
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
     <asp:UpdateProgress ID="progress1" runat="server" DisplayAfter="50">    
                  <ProgressTemplate>
                  <asp:Panel ID="pnlProgressBar" runat="server">    
                     <div><asp:Image ID="Image1" runat="Server" ImageUrl="images/ajax-loader.gif"/>Please 
                         Wait...</div>    
                  </asp:Panel>   
                  </ProgressTemplate> 
                  </asp:UpdateProgress>
    </form>
</body>
</html>
