﻿Imports System.Data.SqlClient

Partial Public Class performance
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim dr As SqlDataReader = Nothing
            Dim cmd As SqlCommand
            Dim query As String = "select id,kdsite,active from site order by id"
            cmd = New SqlCommand(query, conn)
            Try
                If Session("permission") = "" Or Session("permission") = Nothing Then
                    Response.Redirect("../Login.aspx", False)
                    Exit Sub
                ElseIf ("1;2;3;4;5;6;8;7;B").IndexOf(Session("permission")) = -1 Then
                    Response.Redirect("../default.aspx", False)
                    Exit Sub
                End If
                For Each x As ListItem In ddlYear.Items
                    If x.Text = Date.Now.Year Then
                        x.Selected = True
                    End If
                Next
                GenerateMenu()
                'Initialize Jobsite
                conn.Open()
                dr = cmd.ExecuteReader
                KPIJobsite.Items.Clear()
                While dr.Read
                    'KPIJobsite.Items.Add(dr(1))
                    'If dr(2) = 0 Then
                    '    KPIJobsite.Items(KPIJobsite.Items.Count - 1).Enabled = False
                    'End If
                    If Session("permission") = "1" Or Session("permission") = "B" Or Session("permission") = "8" Or Session("permission") = "2" Or Session("permission") = "7" Or Session("permission") = "6" Then 'GRUP MDV & PIC PDCA
                        KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                        'ElseIf Session("permission") = "2" Or Session("permission") = "7" Or Session("permission") = "6" Then 'GRUP SEK PRO & S.Head
                        '    If Session("jobsite") = dr(1) Then
                        '        KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                        '    End If
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Direktorat
                query = "select ID,DirektoratName,active from MDirektorat where active='1' order by ID"
                cmd = New SqlCommand(query, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Then 'GRUP MDV
                        KPIDirektorat.Items.Add(New ListItem(dr(1), dr(0)))
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Function
                query = "select ID,FunctionName,active from MFunction where active='1' order by dirid,ID"
                cmd = New SqlCommand(query, conn)
                dr = cmd.ExecuteReader
                KPIFunc.Items.Clear()
                While dr.Read
                    If Session("permission") = "3" Or Session("permission") = "1" Or Session("permission") = "2" Then 'GRUP BOD
                        KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "8" Or Session("permission") = "4" Or Session("permission") = "5" Or Session("permission") = "1" Or Session("permission") = "7" Then 'PIC PDCA dan GM dan Manager dan MSD
                        If Session("bscdivisi") = dr(0) Then
                            KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                End While
                dr.Close()
                dr = Nothing
                conn.Close()
                Select Case ddlJenis.SelectedValue
                    Case "2"
                        KPIJobsite.Visible = True
                        KPIFunc.Visible = False
                    Case "3"
                        KPIJobsite.Visible = False
                        KPIFunc.Visible = True
                    Case Else
                        KPIJobsite.Visible = False
                        KPIFunc.Visible = False
                End Select
            Catch ex As Exception
                lblMessage.Text = ex.Message
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try

        End If
    End Sub

    Sub GenerateMenu()
        Try
            Select Case Session("permission")
                Case "1" 'MSD
                Case "B" 'MSD
                Case "2" 'SekPro
                    For Each x As ListItem In ddlJenis.Items
                        If x.Value <> 2 And x.Value <> 0 Then
                            x.Enabled = False
                        End If
                    Next
                Case "3" 'BOD
                    For Each x As ListItem In ddlJenis.Items
                        If x.Value <> 0 And x.Value <> 1 Then
                            x.Enabled = False
                        End If
                    Next
                Case "4" 'GM
                    For Each x As ListItem In ddlJenis.Items
                        If x.Value <> 3 Then
                            x.Enabled = False
                        Else
                            x.Selected = True
                        End If
                    Next
                Case "5" 'GM
                    For Each x As ListItem In ddlJenis.Items
                        If x.Value <> 3 Then
                            x.Enabled = False
                        Else
                            x.Selected = True
                        End If
                    Next
                Case "6" 'PM
                    For Each x As ListItem In ddlJenis.Items
                        If x.Value <> 2 And x.Value <> 0 Then
                            x.Enabled = False
                        End If
                    Next
                Case "7" 'S. Head
                    For Each x As ListItem In ddlJenis.Items
                        If x.Value <> 3 Then
                            x.Enabled = False
                        Else
                            x.Selected = True
                        End If
                    Next
                Case "8" 'PIC PDCA
                    For Each x As ListItem In ddlJenis.Items
                        If x.Value <> 3 Then
                            x.Enabled = False
                        Else
                            x.Selected = True
                        End If
                    Next
            End Select
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim cmd As SqlCommand
            Dim dr As SqlDataReader
            Select Case ddlJenis.SelectedValue
                Case 0 'Performance All Jobsite
                    cmd = New SqlCommand
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "dbo.sp_summarylevel0"
                    cmd.Parameters.Add(New SqlParameter("@year", ddlYear.SelectedValue))
                    cmd.Connection = conn
                    conn.Open()
                    dr = cmd.ExecuteReader
                    result.InnerHtml = "Unit Of Measurement : %"
                    result.InnerHtml += "<table border='1' class='act' width='100%'>"
                    result.InnerHtml += "<tr align='center'><td style='background-color:#D7E4BC;font-weight:bold' colspan='13'>PERFORMANCE JRN & ALL JOBSITE" & " " & ddlYear.SelectedValue & "</td></tr>"
                    result.InnerHtml += "<tr style='background-color:#92D050' align='center'><td>" & ddlYear.SelectedValue & "</td><td>JAN</td><td>FEB</td><td>MAR</td><td>APR</td><td>MEI</td><td>JUN</td><td>JUL</td><td>AGU</td><td>SEP</td><td>OKT</td><td>NOV</td><td>DES</td></tr>"
                    result.InnerHtml += "<tr align='center'><td style='background-color:#D7E4BC'>JRN</td>"
                    If dr.HasRows Then
                        While dr.Read
                            result.InnerHtml += "<td style='background-color:#F2F2F2'>" & Format(System.Math.Round(IIf(IsDBNull(dr("ACH")), 0, dr("ACH")), 2), "#,##0.00") & "</td>"
                        End While
                    End If
                    result.InnerHtml += "</tr>"

                    dr.Close()
                    dr = Nothing
                    cmd = New SqlCommand()
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "dbo.sp_summarylevel3"
                    cmd.Parameters.Add(New SqlParameter("@year", ddlYear.SelectedValue))
                    cmd.Connection = conn
                    dr = cmd.ExecuteReader
                    Dim jobsite As String = ""
                    If dr.HasRows Then
                        While dr.Read
                            If jobsite = "" Or jobsite <> dr("kdsite") Then
                                jobsite = dr("kdsite")
                                result.InnerHtml += "</tr><tr align='center'><td style='background-color:#EAF1DD'>" & dr("kdsite") & "</td>"
                            End If
                            result.InnerHtml += "<td>" & Format(System.Math.Round(IIf(IsDBNull(dr("ACH")), 0, dr("ACH")), 2), "#,##0.00") & "</td>"
                        End While
                    End If
                    result.InnerHtml += "</tr>"
                    result.InnerHtml += "</table>"
                    result.InnerHtml += "<div align='right'><input type='button' value='Print' class='btn' onclick='javascipt:fnprint()' id='btnprint'/></div>"
                    conn.Close()
                Case 1 'Performance All Function
                    cmd = New SqlCommand
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "dbo.sp_summarylevel0"
                    cmd.Parameters.Add(New SqlParameter("@year", ddlYear.SelectedValue))
                    cmd.Connection = conn
                    conn.Open()
                    dr = cmd.ExecuteReader
                    result.InnerHtml = "Unit Of Measurement : %"
                    result.InnerHtml += "<table border='1' class='act' width='100%'>"
                    result.InnerHtml += "<tr align='center'><td style='background-color:#D7E4BC;font-weight:bold' colspan='13'>PERFORMANCE ALL FUNCTION" & " " & ddlYear.SelectedValue & "</td></tr>"
                    result.InnerHtml += "<tr style='background-color:#92D050' align='center'><td>" & ddlYear.SelectedValue & "</td><td>JAN</td><td>FEB</td><td>MAR</td><td>APR</td><td>MEI</td><td>JUN</td><td>JUL</td><td>AGU</td><td>SEP</td><td>OKT</td><td>NOV</td><td>DES</td></tr>"
                    'result.InnerHtml += "<tr align='center'><td style='background-color:#D7E4BC'>BUMA</td>"
                    'If dr.HasRows Then
                    '    While dr.Read
                    '        result.InnerHtml += "<td  style='background-color:#F2F2F2'>" & System.Math.Round(IIf(IsDBNull(dr("ACH")), 0, dr("ACH")), 1) & "</td>"
                    '    End While
                    'End If
                    'result.InnerHtml += "</tr>"

                    dr.Close()
                    dr = Nothing
                    cmd = New SqlCommand()
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "dbo.sp_summarylevel1"
                    cmd.Parameters.Add(New SqlParameter("@year", ddlYear.SelectedValue))
                    cmd.Connection = conn
                    dr = cmd.ExecuteReader
                    Dim functionname As String = ""
                    If dr.HasRows Then
                        While dr.Read
                            If functionname = "" Or functionname <> dr("FunctionName") Then
                                functionname = dr("FunctionName")
                                result.InnerHtml += "</tr><tr align='center'><td style='background-color:#EAF1DD'>" & dr("FunctionName") & "</td>"
                            End If
                            result.InnerHtml += "<td>" & Format(System.Math.Round(IIf(IsDBNull(dr("ACH")), 0, dr("ACH")), 2), "#,##0.00") & "</td>"
                        End While
                    End If
                    result.InnerHtml += "</tr>"
                    result.InnerHtml += "</table>"
                    result.InnerHtml += "<div align='right'><input type='button' value='Print' class='btn' onclick='javascipt:fnprint()' id='btnprint'/></div>"

                    conn.Close()
                Case 2 'Performance Jobsite
                    cmd = New SqlCommand
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "dbo.sp_summarylevel3persite"
                    cmd.Parameters.Add(New SqlParameter("@year", ddlYear.SelectedValue))
                    cmd.Parameters.Add(New SqlParameter("@jobsite", KPIJobsite.SelectedValue))
                    cmd.Connection = conn
                    conn.Open()
                    dr = cmd.ExecuteReader
                    result.InnerHtml = "Unit Of Measurement : %"
                    result.InnerHtml += "<table border='1' class='act' width='100%'>"
                    result.InnerHtml += "<tr align='center'><td style='background-color:#D7E4BC;font-weight:bold' colspan='13'>PERFORMANCE JOBSITE " & KPIJobsite.SelectedItem.Text & " " & ddlYear.SelectedValue & "</td></tr>"
                    result.InnerHtml += "<tr style='background-color:#92D050' align='center'><td>" & ddlYear.SelectedValue & "</td><td>JAN</td><td>FEB</td><td>MAR</td><td>APR</td><td>MEI</td><td>JUN</td><td>JUL</td><td>AGU</td><td>SEP</td><td>OKT</td><td>NOV</td><td>DES</td></tr>"
                    result.InnerHtml += "<tr align='center'><td style='background-color:#D7E4BC'>" & KPIJobsite.SelectedItem.Text & "</td>"
                    If dr.HasRows Then
                        While dr.Read
                            result.InnerHtml += "<td  style='background-color:#F2F2F2'>" & Format(System.Math.Round(IIf(IsDBNull(dr("ACH")), 0, dr("ACH")), 2), "#,##0.00") & "</td>"
                        End While
                    End If
                    result.InnerHtml += "</tr>"

                    dr.Close()
                    dr = Nothing
                    cmd = New SqlCommand()
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "dbo.sp_summarylevel2"
                    cmd.Parameters.Add(New SqlParameter("@year", ddlYear.SelectedValue))
                    cmd.Parameters.Add(New SqlParameter("@jobsite", KPIJobsite.SelectedValue))
                    cmd.Connection = conn
                    dr = cmd.ExecuteReader
                    Dim sectionname As String = ""
                    If dr.HasRows Then
                        While dr.Read
                            If sectionname = "" Or sectionname <> dr("sectionname") Then
                                sectionname = dr("sectionname")
                                result.InnerHtml += "</tr><tr align='center'><td style='background-color:#EAF1DD'>" & dr("sectionname") & "</td>"
                            End If
                            result.InnerHtml += "<td>" & System.Math.Round(IIf(IsDBNull(dr("ACH")), 0, dr("ACH")), 2) & "</td>"
                        End While
                    End If
                    result.InnerHtml += "</tr>"
                    result.InnerHtml += "</table>"
                    result.InnerHtml += "<div align='right'><input type='button' value='Print' class='btn' onclick='javascipt:fnprint()' id='btnprint'/></div>"

                    conn.Close()
                Case 3 'Performance Function
                    cmd = New SqlCommand
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "dbo.sp_summarylevel1perfunc"
                    cmd.Parameters.Add(New SqlParameter("@year", ddlYear.SelectedValue))
                    cmd.Parameters.Add(New SqlParameter("@func", KPIFunc.SelectedValue))
                    cmd.Connection = conn
                    conn.Open()
                    dr = cmd.ExecuteReader
                    result.InnerHtml = "Unit Of Measurement : %"
                    result.InnerHtml += "<table border='1' class='act' width='100%'>"
                    result.InnerHtml += "<tr align='center'><td style='background-color:#D7E4BC;font-weight:bold' colspan='13'>PERFORMANCE FUNCTION " & KPIFunc.SelectedItem.Text & " " & ddlYear.SelectedValue & "</td></tr>"
                    result.InnerHtml += "<tr style='background-color:#92D050' align='center'><td>" & ddlYear.SelectedValue & "</td><td>JAN</td><td>FEB</td><td>MAR</td><td>APR</td><td>MEI</td><td>JUN</td><td>JUL</td><td>AGU</td><td>SEP</td><td>OKT</td><td>NOV</td><td>DES</td></tr>"

                    If Session("permission") <> "7" Then
                        result.InnerHtml += "<tr align='center'><td style='background-color:#D7E4BC'><a href='#' onclick=""window.showModalDialog('showgraph.aspx?y=" & ddlYear.SelectedValue & "&c=1&f=" & KPIFunc.SelectedValue & "', '', 'dialogWidth=' + screen.width + 'px;dialogHeight=' + screen.height + 'px;resizable=yes;help=no;unadorned=yes')"">" & KPIFunc.SelectedItem.Text & "</a></td>"
                        If dr.HasRows Then
                            While dr.Read
                                result.InnerHtml += "<td style='background-color:#F2F2F2'>" & Format(System.Math.Round(IIf(IsDBNull(dr("ACH")), 0, dr("ACH")), 2), "#,##0.00") & "</td>"
                            End While
                        End If
                        result.InnerHtml += "</tr>"
                    End If

                    dr.Close()
                    dr = Nothing
                    cmd = New SqlCommand()
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "dbo.sp_summarylevel2perfunc"
                    cmd.Parameters.Add(New SqlParameter("@year", ddlYear.SelectedValue))
                    cmd.Parameters.Add(New SqlParameter("@func", KPIFunc.SelectedValue))
                    cmd.Connection = conn
                    dr = cmd.ExecuteReader
                    Dim jobsite As String = ""
                    If dr.HasRows Then
                        While dr.Read
                            If jobsite = "" Or jobsite <> dr("kdsite") Then
                                jobsite = dr("kdsite")
                                result.InnerHtml += "</tr><tr align='center'><td style='background-color:#EAF1DD'><a href='#' onclick=""window.showModalDialog('showgraph.aspx?y=" & ddlYear.SelectedValue & "&js=" & dr("kdsite") & "&c=11&f=" & KPIFunc.SelectedValue & "&s=" & dr("sectionname") & "', '', 'dialogWidth=' + screen.width + 'px;dialogHeight=' + screen.height + 'px;resizable=yes;help=no;unadorned=yes')"">" & dr("sectionname") & " - " & dr("kdsite") & "</a></td>"
                            End If
                            result.InnerHtml += "<td>" & Format(System.Math.Round(IIf(IsDBNull(dr("ACH")), 0, dr("ACH")), 2), "#,##0.00") & "</td>"
                        End While
                    End If
                    result.InnerHtml += "</tr>"
                    result.InnerHtml += "</table>"
                    result.InnerHtml += "<div align='right'><input type='button' value='Print' class='btn' onclick='javascipt:fnprint()' id='btnprint'/></div>"

                    conn.Close()
                Case 4 'Performance Direktorat
                    cmd = New SqlCommand
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "dbo.sp_summarylevel1perdir"
                    cmd.Parameters.Add(New SqlParameter("@year", ddlYear.SelectedValue))
                    cmd.Parameters.Add(New SqlParameter("@dir", KPIDirektorat.SelectedValue))
                    cmd.Connection = conn
                    conn.Open()
                    dr = cmd.ExecuteReader
                    result.InnerHtml = "Unit Of Measurement : %"
                    result.InnerHtml += "<table border='1' class='act' width='100%'>"
                    result.InnerHtml += "<tr align='center'><td style='background-color:#D7E4BC;font-weight:bold' colspan='13'>PERFORMANCE DIREKTORAT " & KPIDirektorat.SelectedItem.Text & " " & ddlYear.SelectedValue & "</td></tr>"
                    result.InnerHtml += "<tr style='background-color:#92D050' align='center'><td>" & ddlYear.SelectedValue & "</td><td>JAN</td><td>FEB</td><td>MAR</td><td>APR</td><td>MEI</td><td>JUN</td><td>JUL</td><td>AGU</td><td>SEP</td><td>OKT</td><td>NOV</td><td>DES</td></tr>"

                    If Session("permission") <> "7" Then
                        result.InnerHtml += "<tr align='center'><td style='background-color:#D7E4BC'>" & KPIDirektorat.SelectedItem.Text & "</td>"
                        If dr.HasRows Then
                            While dr.Read
                                result.InnerHtml += "<td style='background-color:#F2F2F2'>" & Format(System.Math.Round(IIf(IsDBNull(dr("ACH")), 0, dr("ACH")), 2), "#,##0.00") & "</td>"
                            End While
                        End If
                        result.InnerHtml += "</tr>"
                    End If

                    dr.Close()
                    dr = Nothing
                    cmd = New SqlCommand()
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "dbo.sp_summarylevel2perdir"
                    cmd.Parameters.Add(New SqlParameter("@year", ddlYear.SelectedValue))
                    cmd.Parameters.Add(New SqlParameter("@dir", KPIDirektorat.SelectedValue))
                    cmd.Connection = conn
                    dr = cmd.ExecuteReader
                    Dim func As String = ""
                    If dr.HasRows Then
                        While dr.Read
                            If func = "" Or func <> dr("functionname") Then
                                func = dr("functionname")
                                result.InnerHtml += "</tr><tr align='center'><td style='background-color:#EAF1DD'>" & KPIDirektorat.SelectedItem.Text & " - " & dr("functionname") & "</td>"
                            End If
                            result.InnerHtml += "<td>" & Format(System.Math.Round(IIf(IsDBNull(dr("ACH")), 0, dr("ACH")), 2), "#,##0.00") & "</td>"
                        End While
                    End If
                    result.InnerHtml += "</tr>"
                    result.InnerHtml += "</table>"
                    result.InnerHtml += "<div align='right'><input type='button' value='Print' class='btn' onclick='javascipt:fnprint()' id='btnprint'/></div>"

                    conn.Close()
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        Finally
            Select Case conn.State
                Case ConnectionState.Open
                    conn.Close()
            End Select
        End Try
    End Sub

    Protected Sub ddlJenis_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlJenis.SelectedIndexChanged
        Try
            Select Case ddlJenis.SelectedValue
                Case "2"
                    KPIJobsite.Visible = True
                    KPIFunc.Visible = False
                    KPIDirektorat.visible=False
                Case "3"
                    KPIJobsite.Visible = False
                    KPIFunc.Visible = True
                    KPIDirektorat.visible=False
                Case "4"
                    KPIDirektorat.visible=true
                    KPIJobsite.Visible = False
                    KPIFunc.Visible = False
                Case Else
                    KPIJobsite.Visible = False
                    KPIFunc.Visible = False
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class