﻿Imports System.Data.SqlClient

Partial Public Class PopUpSearch
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") Is Nothing Then
            Response.Redirect("../Login.aspx", False)
            Exit Sub
        ElseIf Session("permission") = "" Then
            Response.Redirect("../Login.aspx", False)
            Exit Sub
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        BindGrid()
    End Sub

    Private Sub srchResult_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles srchResult.PageIndexChanging
        BindGrid()
        srchResult.PageIndex = e.NewPageIndex
        srchResult.DataBind()
    End Sub

    Sub BindGrid()
        Try
            lblKet.Text = ""
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim query As String = ""

            Select Case Request.QueryString("type")
                Case "0"
                    query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI from MKPI a left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='0'  and year='" & Request.QueryString("y") & "'  " & IIf(Request.QueryString("vt") = "1", " and a.stedit='0' ", "") & " order by p.id,createdtime desc"
                Case "1"
                    If Request.QueryString("wtr") IsNot Nothing Then
                        If (Session("permission") = "3" Or Session("permission") = "8" Or Session("permission") = "1") And Request.QueryString("wtr") = "mf" Then 'BOD, MSD
                            query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI ,c.functionname as [Function] from MKPI a inner join MFUNCTION c on a.Func=c.ID left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='1'  and year='" & Request.QueryString("y") & "'  and Func='" & Request.QueryString("func") & "' " & IIf(Request.QueryString("vt") = "1", " and a.stedit='0' ", "") & "  order by p.id,createdtime desc"
                        Else
                            query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI ,c.functionname as [Function] from MKPI a inner join MFUNCTION c on a.Func=c.ID left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='1'  and year='" & Request.QueryString("y") & "'  and Func='" & Session("bscdivisi") & "' " & IIf(Request.QueryString("vt") = "1", " and a.stedit='0' ", "") & "  order by p.id,createdtime desc"
                        End If
                    Else
                        If Session("permission") = "1" Then
                            query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI ,c.functionname as [Function] from MKPI a inner join MFUNCTION c on a.Func=c.ID left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='1' and year='" & Request.QueryString("y") & "' and Func='" & Request.QueryString("func") & "' " & IIf(Request.QueryString("vt") = "1", " and a.stedit='0' ", "") & "  order by p.id,createdtime desc"
                        Else
                            query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI ,c.functionname as [Function] from MKPI a inner join MFUNCTION c on a.Func=c.ID left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='1' and year='" & Request.QueryString("y") & "' and Func='" & Session("bscdivisi") & "' " & IIf(Request.QueryString("vt") = "1", " and a.stedit='0' ", "") & "  order by p.id,createdtime desc"
                        End If
                    End If
                Case "2"
                    If Request.QueryString("wtr") IsNot Nothing Then
                        If (Session("permission") = "6" Or Session("permission") = "2" Or Session("permission") = "3" Or Session("permission") = "4" Or Session("permission") = "5" Or Session("permission") = "8" Or Session("permission") = "1") And Request.QueryString("wtr") = "mf" Then
                            query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI ,d.kdsite,c.sectionname as Section from MKPI a inner join MSECTION c on a.section=c.ID inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='2'  and year='" & Request.QueryString("y") & "'  and jobsite='" & Request.QueryString("jobsite") & "' and Section='" & Request.QueryString("sec") & "' " & IIf(Request.QueryString("vt") = "1", " and a.stedit='0' ", "") & "  order by p.id,createdtime desc"
                        ElseIf Session("permission") = "2" Then
                            query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI ,d.kdsite,c.sectionname as Section from MKPI a inner join MSECTION c on a.section=c.ID inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='2'  and year='" & Request.QueryString("y") & "'  and kdsite='" & Session("jobsite") & "' and Section='9' " & IIf(Request.QueryString("vt") = "1", " and a.stedit='0' ", "") & "  order by p.id,createdtime desc"
                        Else
                            query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI ,d.kdsite,c.sectionname as Section from MKPI a inner join MSECTION c on a.section=c.ID inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='2'  and year='" & Request.QueryString("y") & "'  and kdsite='" & Session("jobsite") & "' and Section='" & Session("bscdepar") & "' " & IIf(Request.QueryString("vt") = "1", " and a.stedit='0' ", "") & "  order by p.id,createdtime desc"
                        End If
                    Else
                        If Session("permission") = "7" Then
                            query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI ,d.kdsite as Site ,c.sectionname as Section from MKPI a inner join MSECTION c on a.section=c.ID inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='2' and year='" & Request.QueryString("y") & "' and kdsite='" & Session("jobsite") & "' and Section='" & Session("bscdepar") & "' " & IIf(Request.QueryString("vt") = "1", " and a.stedit='0' ", "") & "  order by p.id,createdtime desc"
                        ElseIf Session("permission") = "5" Then
                            query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI ,d.kdsite as Site ,c.sectionname as Section from MKPI a inner join MSECTION c on a.section=c.ID inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='2' and year='" & Request.QueryString("y") & "' and jobsite='" & Request.QueryString("jobsite") & "' and Section='" & Session("bscdepar") & "' " & IIf(Request.QueryString("vt") = "1", " and a.stedit='0' ", "") & "  order by p.id,createdtime desc"
                        ElseIf Session("permission") = "8" Then
                            query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI ,d.kdsite as Site ,c.sectionname as Section from MKPI a inner join MSECTION c on a.section=c.ID inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='2' and year='" & Request.QueryString("y") & "' and jobsite='" & Request.QueryString("jobsite") & "' and Section='" & Request.QueryString("sec") & "' " & IIf(Request.QueryString("vt") = "1", " and a.stedit='0' ", "") & "  order by p.id,createdtime desc"
                        ElseIf Session("permission") = "2" Or Session("permission") = "A" Then
                            query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI ,d.kdsite as Site ,c.sectionname as Section from MKPI a inner join MSECTION c on a.section=c.ID inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='2' and year='" & Request.QueryString("y") & "' and kdsite='" & Session("jobsite") & "' and Section='" & Request.QueryString("sec") & "' " & IIf(Request.QueryString("vt") = "1", " and a.stedit='0' ", "") & "  order by p.id,createdtime desc"
                        ElseIf Session("permission") = "1" Then
                            query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI ,d.kdsite as Site ,c.sectionname as Section from MKPI a inner join MSECTION c on a.section=c.ID inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='2' and year='" & Request.QueryString("y") & "' and jobsite='" & Request.QueryString("jobsite") & "' and Section='" & Request.QueryString("sec") & "' order by p.id,createdtime desc"
                        End If
                    End If
                Case "3"
                    If (Session("permission") = "6" Or Session("permission") = "2" Or Session("permission") = "3" Or Session("permission") = "4" Or Session("permission") = "5" Or Session("permission") = "1") And Request.QueryString("wtr") = "mf" Then
                        query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI,d.kdsite as Site  from MKPI a inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='3'  and year='" & Request.QueryString("y") & "'  and jobsite='" & Request.QueryString("jobsite") & "' " & IIf(Request.QueryString("vt") = "1", " and a.stedit='0' ", "") & "  order by p.id,createdtime desc"
                    ElseIf Session("permission") = "1" Then
                        query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI,d.kdsite as Site  from MKPI a inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='3'  and year='" & Request.QueryString("y") & "'  and jobsite='" & Request.QueryString("jobsite") & "' " & IIf(Request.QueryString("vt") = "1", " and a.stedit='0' ", "") & "  order by p.id,createdtime desc"
                    Else
                        query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI,d.kdsite as Site  from MKPI a inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='3'  and year='" & Request.QueryString("y") & "'  and d.kdsite='" & Session("jobsite") & "' " & IIf(Request.QueryString("vt") = "1", " and a.stedit='0' ", "") & "  order by p.id,createdtime desc"
                    End If
                Case "4"
                    query = "select a.year as Tahun,p.abbr as Perspective,a.ID,a.Name as KPI ,c.direktoratname as Direktorat from MKPI a inner join MDirektorat c on a.Dir=c.ID left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='4' and year='" & Request.QueryString("y") & "' and Dir='" & Request.QueryString("dir") & "' order by p.id,createdtime desc"
            End Select
            Dim cmd As SqlCommand
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(query, conn)
            cmd = New SqlCommand(query, conn)
            conn.Open()
            da.Fill(dt)
            conn.Close()
            srchResult.DataSource = dt
            srchResult.DataBind()
            If dt.Rows.Count = 0 Then
                lblKet.Text = "Data Tidak Ditemukan"
            End If
        Catch ex As Exception
            lblKet.Text = ex.Message
        End Try
    End Sub

    Private Sub srchResult_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles srchResult.RowDataBound
        Try
            If Request.QueryString("wtr") IsNot Nothing Then
                If e.Row.RowIndex <> -1 Then
                    e.Row.Attributes.Add("ondblclick", "javascript:executeCloseCmbn('" & e.Row.Cells(2).Text & "|" & e.Row.Cells(3).Text & "')")
                End If
            Else
                If e.Row.RowIndex <> -1 Then
                    e.Row.Attributes.Add("ondblclick", "javascript:executeClose(" & e.Row.Cells(2).Text & ")")
                End If
            End If
        Catch ex As Exception
            lblKet.Text = ex.Message
        End Try
    End Sub


End Class