﻿Imports System.Data.SqlClient
Imports System.Net
Imports System.IO

Partial Public Class entry_permit_doc
    Inherits System.Web.UI.Page
    Public tyear As String
    Public lbl As New Label
    Public mnentry As String
    Public mnview As String
    Public mnmgmt As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim skdcom As String
        Dim snmcom As String
        Dim ikdcom As Integer
        Dim inmcom As Integer
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim strcon As String = "select Kdcompany, Nmcompany from jrn_test.dbo.H_A110"
        'strcon = strcon + " where month(reqdate) = '" + imonth.ToString + "' and YEAR(reqdate) = '" + th + "' and nik = '1231313' and stedit <> 2"
        Dim dtb As DataTable = New DataTable
        Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
        sda.Fill(dtb)

        For ikdcom = 0 To dtb.Rows.Count

        Next
        tyear = Date.Now.Year.ToString

        txtyear.Value = tyear

        If Session("otorisasi_permit") = Nothing Or Session("otorisasi_permit") = "" Then
            Response.Redirect("signin_permit.aspx")
        End If

        If Trim(Session("permission_permit")) = "1" Or Trim(Session("permission_permit")) = "2" Then
            mnentry = "<div id='mnentry'><img alt='entry document' src='../images/open_doc_small.png' /> <a href='entry_permit_doc.aspx'>&nbsp;Upload Document</a></div>"
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Document</a></div>"
        End If

        If Trim(Session("permission_permit")) = "1" Then
            mnmgmt = "<div id='mnmgmt'> <img alt='Management User' src='../images/open_doc_small.png' /> <a href='mgmnt_list.aspx'>&nbsp;Management User</a></div>"
        End If

        If Trim(Session("permission_permit")) = "3" Or Trim(Session("permission_permit")) = "4" Then
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Document</a></div>"
        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim conn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim sqlQuery As String
        Dim cmd As SqlCommand
        Dim dttm As String
        Dim dtmnow As String
        Dim dtynow As String
        Dim stripno As String
        Dim permitid As String
        Dim _comp As String = ddlcomp.SelectedValue
        Dim nmcomp As String = ddlcomp.Text
        Dim _site As String = ddlsite.SelectedValue
        Dim _subject As String = subject.Value
        Dim nmsite As String = ddlsite.Text
        Dim _year As String = txtyear.Value
        Dim _category As String = category.Value
        Dim _doctype As String = doctp.SelectedValue
        Dim _department As String = Department.Value
        Dim _subdept As String = subdept.Value
        Dim _number As Integer = Number.Value
        Dim _report As String
        Dim _dtissue As String = dtissue.Value
        Dim _dtexp As String = dtexp.Value
        Dim _FileUpload1 As String
        Dim _filename As String
        Dim _remcode As String = rem_days.SelectedValue
        Dim i As Integer
        Dim strcon As String = "select id from docpermit_trans"
        Dim dtb As DataTable = New DataTable
        Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, conn)
        Dim _nmpic1 As String
        Dim _nmpic2 As String
        Dim _nmpic3 As String
        Dim _nmpic4 As String
        Dim _nmpic5 As String
        Dim _mailpic1 As String
        Dim _mailpic2 As String
        Dim _mailpic3 As String
        Dim _mailpic4 As String
        Dim _mailpic5 As String
        Dim _piccd1 As String
        Dim _piccd2 As String
        Dim _piccd3 As String
        Dim _piccd4 As String
        Dim _piccd5 As String

        If FileUpload1.HasFile Then
            Dim fileExt As String
            Dim s As String
            fileExt = System.IO.Path.GetExtension(FileUpload1.FileName)

            If (fileExt = ".pdf") Then
                Dim docty As String = doctype2.Value

                uploadFile("ftp://20.110.1.6/syam", FileUpload1.FileName, "syam", "r3d0.678", FileUpload1.PostedFile.FileName)
                RenameFileOnServer("ftp://20.110.1.6/syam/", FileUpload1.FileName, _doctype + "_" + subject.Value + "_" + ddlcomp.SelectedValue + "_" + ddlsite.SelectedValue + "_" + tyear + "_" + FileUpload1.FileName, "syam", "r3d0.678")

            Else
                label1.Text = "Only .pdf files allowed!"
            End If
        Else
            label1.Text = "You have not specified a file."
        End If
        'label2.Text = lbl.Text

        Try
            dtmnow = DateTime.Now.ToString("MM")
            dtynow = Date.Now.Year.ToString
            stripno = dtynow + dtmnow + "/"

            sda.Fill(dtb)

            If dtb.Rows.Count = 0 Then
                permitid = stripno + "000001"
            ElseIf dtb.Rows.Count > 0 Then
                i = 0
                i = dtb.Rows.Count + 1
                If dtb.Rows.Count > 0 And dtb.Rows.Count < 9 Then
                    permitid = stripno + "00000" + i.ToString
                ElseIf dtb.Rows.Count >= 9 And dtb.Rows.Count < 99 Then
                    permitid = stripno + "0000" + i.ToString
                ElseIf dtb.Rows.Count >= 99 And dtb.Rows.Count < 999 Then
                    permitid = stripno + "000" + i.ToString
                ElseIf dtb.Rows.Count >= 999 And dtb.Rows.Count < 9999 Then
                    permitid = stripno + "00" + i.ToString
                ElseIf dtb.Rows.Count >= 9999 And dtb.Rows.Count < 99999 Then
                    permitid = stripno + "0" + i.ToString
                ElseIf dtb.Rows.Count >= 99999 And dtb.Rows.Count < 999999 Then
                    permitid = stripno + i.ToString
                ElseIf dtb.Rows.Count >= 999999 Then
                    permitid = "Error on generate Tripnum"
                End If
            End If
            conn2.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        conn.Open()

        Try
            Dim strcon2 As String = "select Nmcompany from jrn_test.dbo.H_A110 where kdcompany = '" + _comp + "'"
            Dim dtb2 As DataTable = New DataTable
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, conn)
            sda2.Fill(dtb2)

            nmcomp = dtb2.Rows(0)!Nmcompany.ToString
        Catch ex As Exception
        End Try


        Try
            Dim strcon3 As String = "select Nmsite from jrn_test.dbo.H_A120 where kdsite = '" + _site + "'"
            Dim dtb3 As DataTable = New DataTable
            Dim sda3 As SqlDataAdapter = New SqlDataAdapter(strcon3, conn)
            sda3.Fill(dtb3)

            nmsite = dtb3.Rows(0)!Nmsite.ToString
        Catch ex As Exception

        End Try

        If rpt.Checked = True Then
            _report = "1"
        Else
            _report = "0"
            _remcode = "0"
        End If
        dttm = Date.Now.ToString
        _FileUpload1 = doctp.SelectedValue + "_" + subject.Value + "_" + ddlcomp.SelectedValue + "_" + ddlsite.SelectedValue + "_" + txtyear.Value + "_" + FileUpload1.FileName
        _filename = FileUpload1.FileName
        If chkpic1.Checked = True Then
            _nmpic1 = txtpic1.SelectedItem.ToString
            _mailpic1 = txtpic1.SelectedValue
            Try
                Dim sqlpic As String = "select pic_code from docpermit_pic where pic_email = '" + _mailpic1 + "'"
                Dim dtbpic As DataTable = New DataTable
                Dim sdapic As SqlDataAdapter = New SqlDataAdapter(sqlpic, conn)
                sdapic.Fill(dtbpic)

                If dtbpic.Rows.Count > 0 Then
                    _piccd1 = dtbpic.Rows(0)!pic_code
                Else
                    _piccd1 = " "
                End If
            Catch ex As Exception

            End Try
        Else
            _nmpic1 = " "
            _mailpic1 = " "
        End If
        If chkpic2.Checked = True Then
            _nmpic2 = txtpic2.SelectedItem.ToString
            _mailpic2 = txtpic2.SelectedValue

            Try
                Dim sqlpic As String = "select pic_code from docpermit_pic where pic_email = '" + _mailpic2 + "'"
                Dim dtbpic As DataTable = New DataTable
                Dim sdapic As SqlDataAdapter = New SqlDataAdapter(sqlpic, conn)
                sdapic.Fill(dtbpic)

                If dtbpic.Rows.Count > 0 Then
                    _piccd2 = dtbpic.Rows(0)!pic_code
                Else
                    _piccd2 = " "
                End If
            Catch ex As Exception

            End Try
        Else
            _nmpic2 = " "
            _mailpic2 = " "
        End If
        If chkpic3.Checked = True Then
            _nmpic3 = txtpic3.SelectedItem.ToString
            _mailpic3 = txtpic3.SelectedValue

            Try
                Dim sqlpic As String = "select pic_code from docpermit_pic where pic_email = '" + _mailpic3 + "'"
                Dim dtbpic As DataTable = New DataTable
                Dim sdapic As SqlDataAdapter = New SqlDataAdapter(sqlpic, conn)
                sdapic.Fill(dtbpic)

                If dtbpic.Rows.Count > 0 Then
                    _piccd3 = dtbpic.Rows(0)!pic_code
                Else
                    _piccd3 = " "
                End If
            Catch ex As Exception

            End Try
        Else
            _nmpic3 = " "
            _mailpic3 = " "
        End If
        If chkpic4.Checked = True Then
            _nmpic4 = txtpic4.SelectedItem.ToString
            _mailpic4 = txtpic4.SelectedValue

            Try
                Dim sqlpic As String = "select pic_code from docpermit_pic where pic_email = '" + _mailpic4 + "'"
                Dim dtbpic As DataTable = New DataTable
                Dim sdapic As SqlDataAdapter = New SqlDataAdapter(sqlpic, conn)
                sdapic.Fill(dtbpic)

                If dtbpic.Rows.Count > 0 Then
                    _piccd4 = dtbpic.Rows(0)!pic_code
                Else
                    _piccd4 = " "
                End If
            Catch ex As Exception

            End Try
        Else
            _nmpic4 = " "
            _mailpic4 = " "
        End If
        If chkpic5.Checked = True Then
            _nmpic5 = txtpic5.SelectedItem.ToString
            _mailpic5 = txtpic5.SelectedValue

            Try
                Dim sqlpic As String = "select pic_code from docpermit_pic where pic_email = '" + _mailpic5 + "'"
                Dim dtbpic As DataTable = New DataTable
                Dim sdapic As SqlDataAdapter = New SqlDataAdapter(sqlpic, conn)
                sdapic.Fill(dtbpic)

                If dtbpic.Rows.Count > 0 Then
                    _piccd5 = dtbpic.Rows(0)!pic_code
                Else
                    _piccd5 = " "
                End If
            Catch ex As Exception

            End Try
        Else
            _nmpic5 = " "
            _mailpic5 = " "
        End If
        Try
            'sqlQuery = "Insert into docpermit_trans (id,ftype,fsubject,kdcom,company_name,kdsite,site_name,fyear,fcategory,doctype,department,subdepartment,number,must_report,cms_start,cms_end, f_path, f_name, stedit,createdtime) values ('" + permitid.ToString + "' , ' ', '" + _subject.ToString + "', '" + _comp.ToString + "', '" + nmcomp.ToString + "', '" + _site.ToString + "', '" + nmsite.ToString + "', '" + _year.ToString + "', '" + _category.ToString + "', '" + _doctype.ToString + "', '" + _department.ToString + "', '" + _subdept.ToString + "', '" + _number.ToString + "', '" + _report.ToString + "', '" + _dtissue.ToString + "', '" + _dtexp.ToString + "', '\\20.110.1.6\syam\" + _FileUpload1.ToString + "', '" + _filename.ToString + "', '1','" + dttm.ToString + "' )"
            'sqlQuery = "Insert into docpermit_trans (id,ftype,fsubject,kdcom,company_name,kdsite,site_name,fyear,fcategory,doctype,department,subdepartment,number,must_report,cms_start,cms_end, f_path, f_name, stedit) values ('" + permitid + "' , '" + _type + "', '" + _subject + "', '" + _comp + "', '" + nmcomp + "', '" + _site + "', '" + nmsite + "', '" + _year + "', '" + _category + "', '" + _doctype + "', '" + _department + "', '" + _subdept + "', '" + _number + "', '" + _report + "', '" + _dtissue + "', '" + _dtexp + "', 'C:\uploads\" + _fileupload1 + "', '" + _filename + "', '1' )"
            sqlQuery = "Insert into docpermit_trans (id,ftype,fsubject,kdcom,company_name,kdsite,site_name,fyear,fcategory,doctype,department,subdepartment,number,must_report,cms_start,cms_end, f_path, f_name, stedit,createdtime,rem_code,pic1, pic1_mail, pic2, pic2_mail,pic3, pic3_mail,pic4, pic4_mail,pic5, pic5_mail) values ('" + permitid.ToString + "' , ' ', '" + _subject.ToString + "', '" + _comp.ToString + "', '" + nmcomp.ToString + "', '" + _site.ToString + "', '" + nmsite.ToString + "', '" + _year.ToString + "', '" + _category.ToString + "', '" + _doctype.ToString + "', '" + _department.ToString + "', '" + _subdept.ToString + "', '" + _number.ToString + "', '" + _report.ToString + "', '" + _dtissue.ToString + "', '" + _dtexp.ToString + "', '\\20.110.1.6\syam\" + _FileUpload1.ToString + "', '" + _filename.ToString + "', '1','" + dttm.ToString + "','" + _remcode + "','" + _piccd1 + "','" + _mailpic1 + "','" + _piccd2 + "','" + _mailpic2 + "','" + _piccd3 + "','" + _mailpic3 + "','" + _piccd4 + "','" + _mailpic4 + "','" + _piccd5 + "','" + _mailpic5 + "')"
            cmd = New SqlCommand(sqlQuery, conn)
            cmd.ExecuteScalar()

            'Return "success"
        Catch ex As Exception

            'Return "failed"
        Finally
            conn.Close()
        End Try
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        Response.Redirect("permit_list.aspx")
    End Sub

    Private Sub uploadFile(ByVal FTPAddress As String, ByVal filePath As String, ByVal username As String, ByVal password As String, ByVal fullpath As String)
        Try
            Dim request As FtpWebRequest = DirectCast(FtpWebRequest.Create(FTPAddress & "/" & Path.GetFileName(filePath)), FtpWebRequest)

            request.Method = WebRequestMethods.Ftp.UploadFile
            request.Credentials = New NetworkCredential(username, password)
            request.UsePassive = True
            request.UseBinary = True
            request.KeepAlive = False

            'Load the file
            'Dim stream As FileStream = File.OpenRead(filePath)
            Dim fStream As Stream = FileUpload1.PostedFile.InputStream
            'MsgBox("test " + File.OpenRead(fullpath).ToString())
            Dim buffer As Byte() = New Byte(CInt(fStream.Length - 1)) {}

            fStream.Read(buffer, 0, buffer.Length)
            fStream.Close()

            'Upload file
            Dim reqStream As Stream = request.GetRequestStream()
            reqStream.Write(buffer, 0, buffer.Length)
            reqStream.Close()

            'MsgBox("Uploaded Successfully", MsgBoxStyle.Information)
        Catch e As Exception
            label1.Text = "ERROR: " & e.Message.ToString()
            'MsgBox("Failed to upload.Please check the ftp settings" + "Reason: " + e.Message, MsgBoxStyle.Critical)
            'MessageBox(e.Message.ToString())
        End Try
    End Sub

    Private Sub RenameFileOnServer(ByVal TheServerURL As String, ByVal OldFile As String, ByVal NewName As String, ByVal username As String, ByVal password As String)
        Dim MyFtpWebRequest As System.Net.FtpWebRequest = CType(System.Net.FtpWebRequest.Create(TheServerURL & OldFile), FtpWebRequest)
        MyFtpWebRequest.Credentials = New System.Net.NetworkCredential(username, password)
        MyFtpWebRequest.Method = System.Net.WebRequestMethods.Ftp.Rename
        MyFtpWebRequest.RenameTo() = NewName
        Dim MyResponse As System.Net.FtpWebResponse

        Try
            MyResponse = CType(MyFtpWebRequest.GetResponse, FtpWebResponse)

            Dim MyStatusStr As String = MyResponse.StatusDescription
            Dim MyStatusCode As System.Net.FtpStatusCode = MyResponse.StatusCode

            If MyStatusCode <> Net.FtpStatusCode.FileActionOK Then
                'MessageBox("*** Rename " & OldFile & " to " & NewName & " failed.  Returned status = " & MyStatusStr)
                'label2.Text = "*** Rename " & OldFile & " to " & NewName & " failed.  Returned status = " & MyStatusStr
                label2.Text = "Upload Failed.  Returned status = " & MyStatusStr
            Else
                'MessageBox("Rename " & OldFile & " to " & NewName & " ok at " & Now.ToString)
                'label2.Text = "Rename " & OldFile & " to " & NewName & " ok at " & Now.ToString
                label2.Text = "Upload Success at " & Now.ToString
            End If
        Catch ex As Exception
            'MessageBox("*** Rename " & OldFile & " to " & NewName & " failed due to the following error: " & ex.Message)

        End Try
    End Sub

    Private Sub MessageBox(ByVal msg As String)

        lbl.Text = "<script language='javascript'>" & Environment.NewLine & _
               "window.alert('" + msg + "')</script>"
        Page.Controls.Add(lbl)
    End Sub

    Function Terbilang(ByVal myNum As Double) As String
        Dim myString As String() = {"", "satu", "dua", "tiga", "empat", "lima", _
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"}
        Dim myTmpString As String = ""
        Select Case myNum
            Case Is < 12
                myTmpString = myString(myNum)
            Case Is < 20
                myTmpString = Terbilang(myNum - 10) + " " + "belas"
            Case Is < 100
                If Microsoft.VisualBasic.Right(myNum, 1) > 0 Then
                    myTmpString = Terbilang(Int(myNum / 10)) + " " + "puluh" + " " + Terbilang(myNum Mod 10)
                Else
                    myTmpString = Terbilang(Int(myNum / 10)) + " " + "puluh"
                End If
            Case Is < 200
                myTmpString = "seratus" + " " + Terbilang(myNum - 100)
            Case Is < 1000
                myTmpString = Terbilang(Int(myNum / 100)) + " " + "ratus" + " " + Terbilang(myNum Mod 100)
            Case Is < 2000
                myTmpString = "seribu" + " " + Terbilang(myNum - 1000)
            Case Is < 1000000
                myTmpString = Terbilang(Int(myNum / 1000)) + " " + "ribu" + " " + Terbilang(myNum Mod 1000)
            Case Is < 1000000000
                myTmpString = Terbilang(Int(myNum / 1000000)) + " " + "juta" + " " + Terbilang(myNum Mod 1000000)
            Case Is < 1000000000000
                myTmpString = Terbilang(Int(myNum / 1000000000)) + " " + "milyar" + " " + Terbilang(myNum Mod 1000000000)
            Case Is < 1000000000000000
                myTmpString = Terbilang(Int(myNum / 1000000000000)) + " " + "trilyun" + " " + Terbilang(myNum Mod 1000000000000)
        End Select
        Return myTmpString
    End Function
End Class
'Dim type As String
'Dim w As New System.Net.WebClient
'w.Credentials = New System.Net.NetworkCredential("syam.kharisman", "sysyam", "jresources")

'Server.ScriptTimeout = 3600
'FileUpload1.SaveAs("\\files.jresources.com\Permit Documents\" + tipe.Value + "_" + subject.Value + "_" + ddlcomp.SelectedValue + "_" + ddlsite.SelectedValue + "_" + tyear + "_" & _
'FileUpload1.FileName)
'FileUpload1.SaveAs("C:\uploads\" + tipe.Value + "_" + subject.Value + "_" + ddlcomp.SelectedValue + "_" + ddlsite.SelectedValue + "_" + tyear + "_" & _
'FileUpload1.FileName)
'Dim LogonCred As NetworkCredential = New NetworkCredential("syam.kharisman", "sysyam", "jresources.com")
'Dim DestUri As Uri = New Uri("\\files.jresources.com\Permit Documents\" + doctype2.Value + "_" + subject.Value + "_" + ddlcomp.SelectedValue + "_" + ddlsite.SelectedValue + "_" + tyear + "_" + FileUpload1.FileName)
'Dim DestUri As Uri = New Uri("\\files.jresources.com\Permit Documents\" + FileUpload1.FileName)
'My.Computer.Network.UploadFile(FileUpload1.PostedFile.FileName, DestUri, LogonCred, False, 3500)

'Dim filepath As String = "\\files.jresources.com\Permit Documents\" + FileUpload1.FileName
'Dim filepath_new As String = docty + "_" + subject.Value + "_" + ddlcomp.SelectedValue + "_" + ddlsite.SelectedValue + "_" + tyear + "_" + FileUpload1.FileName
'filepath_new = Replace(filepath_new, "/", "_")


'If File.Exists(filepath) Then
' Give a new name
'My.Computer.FileSystem.RenameFile(filepath, filepath_new)

'Else
' Use existing name
'End If

'Dim myFtpWebRequest As FtpWebRequest
'Dim myFtpWebResponse As FtpWebResponse
'Dim myStreamWriter As StreamWriter

'myFtpWebRequest = WebRequest.Create("ftp://syam@20.110.1.6/syam/" + FileUpload1.FileName)

'myFtpWebRequest.Credentials = New NetworkCredential("syam", "r3d0.678")

'myFtpWebRequest.Method = WebRequestMethods.Ftp.UploadFile
'myFtpWebRequest.UseBinary = True

'myStreamWriter = New StreamWriter(myFtpWebRequest.GetRequestStream())
'myStreamWriter.Write(New StreamReader(Server.MapPath(FileUpload1.FileName)).ReadToEnd)
'myStreamWriter.Write(New StreamReader(FileUpload1.PostedFile.FileName).ReadToEnd)
'myStreamWriter.Close()

'myFtpWebResponse = myFtpWebRequest.GetResponse()

'label1.Text = myFtpWebResponse.StatusDescription



'label1.Text = "File name: " & _
'  FileUpload1.PostedFile.FileName & "<br>" & _
'  "File Size: " & _
'  FileUpload1.PostedFile.ContentLength & " kb<br>" & _
'  "Content type: " & _
'  FileUpload1.PostedFile.ContentType & " <br>" & _
'  myFtpWebResponse.StatusDescription

'myFtpWebResponse.Close()
'Dim fStream As Stream = FileUpload1.PostedFile.InputStream

'Dim reqObj As FtpWebRequest = DirectCast(WebRequest.Create("ftp://syam@20.110.1.6/syam/" + FileUpload1.FileName), FtpWebRequest)
'Dim reqObj As FtpWebRequest = DirectCast(WebRequest.Create("ftp://20.110.1.6/syam/btr.pdf"), FtpWebRequest)
's = "1"

''Call A FileUpload Method of FTP Request Object
'reqObj.Method = WebRequestMethods.Ftp.UploadFile

's = "2"

''If you want to access Resourse Protected,give UserName and PWD
'reqObj.Credentials = New NetworkCredential("syam", "r3d0.678")

's = "3" + FileUpload1.FileName

'' Copy the contents of the file to the byte array.
'Dim fileContents As Byte() = File.ReadAllBytes(FileUpload1.PostedFile.FileName)
'reqObj.ContentLength = fileContents.Length

's = "4"

''Upload File to FTPServer
'Dim requestStream As Stream = reqObj.GetRequestStream()
'requestStream.Write(fileContents, 0, fileContents.Length)
'requestStream.Close()
'Dim response As FtpWebResponse = DirectCast(reqObj.GetResponse(), FtpWebResponse)
'response.Close()
'label2.Text = lbl.Text