﻿Imports System.Data.SqlClient

Partial Public Class printkpi
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("permission") = "" Or Session("permission") = Nothing Then
                Response.Redirect("../Login.aspx", False)
                Exit Sub
            ElseIf ("1;2;3;4;5;7;8;A").IndexOf(Session("permission")) = -1 Then 'yang bisa akses cuman mdv,sekpro,shead,picpdca, SH Admin
                Response.Redirect("../default.aspx", False)
                Exit Sub
            ElseIf Request.QueryString("id") Is Nothing Then
                Response.Redirect("../default.aspx", False)
                Exit Sub
            End If

            If Not Page.IsPostBack Then
                Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
                Dim dr As SqlDataReader = Nothing
                Dim sqlQuery As String
                Dim level As String = ""
                sqlQuery = "select a.*,dir.DirektoratName,f.FunctionName,s.kdsite,ss.SectionName from MKPI a " & _
                           "left join MFUNCTION f on a.Func=f.ID " & _
                           "left join SITE s on a.Jobsite = s.id " & _
                           "left join MSECTION ss on a.Section=ss.ID " & _
                           "left join MDIREKTORAT dir on f.dirid=dir.ID " & _
                           "where a.ID='" & Request.QueryString("id") & "'"
                Try
                    Dim cmd As New SqlCommand(sqlQuery, conn)
                    conn.Open()
                    dr = cmd.ExecuteReader
                    If dr.HasRows Then
                        While dr.Read
                            level = dr("level")
                            Select Case dr("level")
                                Case "0"
                                    KPILevel.Text = "JRN"
                                Case "1"
                                    KPILevel.Text = "Function " & dr("FunctionName")
                                Case "2"
                                    KPILevel.Text = "Section " & dr("SectionName") & " " & dr("kdsite")
                                Case "3"
                                    KPILevel.Text = "Jobsite " & dr("kdsite")
                                Case "4"
                                    KPILevel.Text = "Direktorat " & dr("DirektoratName")
                            End Select
                            KPIName.Text = dr("Name")
                            KPIOwner.Text = dr("Owner")
                            KPIUom.Text = dr("UoM")
                            KPIUoM1.Text = dr("UoM")
                            KPIDesc.Text = dr("Desc")
                            Select Case dr("period")
                                Case "2"
                                    Monthly.Value = "X"
                                Case "3"
                                    Quarterly.Value = "X"
                                Case "4"
                                    Semester.Value = "X"
                                Case "5"
                                    Yearly.Value = "X"
                            End Select
                            KPIFormulation.Text = dr("FormulaDesc")
                            If Not IsDBNull(dr("KPIType")) Then
                                Select Case dr("KPIType")
                                    Case "0"
                                        KPILag.Value = "X"
                                    Case "1"
                                        KPILead.Value = "X"
                                End Select
                            End If
                            If Not IsDBNull(dr("KPILeads")) Then
                                KPILeads.Text = dr("KPILeads")
                            End If
                            If Not IsDBNull(dr("Process")) Then
                                KPIProcess.Text = dr("Process")
                            End If
                            If Not IsDBNull(dr("Targetting")) Then
                                Select Case dr("Targetting")
                                    Case "0"
                                        kpiflat.Value = "X"
                                    Case "1"
                                        kpimoving.Value = "X"
                                End Select
                            End If
                        End While
                    End If
                    dr.Close()
                    dr = Nothing

                    sqlQuery = "select * from mkpiformula where kpiid='" & Request.QueryString("id") & "'"
                    cmd = New SqlCommand(sqlQuery, conn)
                    dr = cmd.ExecuteReader
                    If dr.HasRows Then
                        omg.InnerHtml = "<table style=""width:100%"" border=""1"">"
                        omg.InnerHtml += "<tr><td style=""background-color:#D9D9D9"">Name Of Data</td><td style=""background-color:#D9D9D9"">Source Data (Form No.)</td><td style=""background-color:#D9D9D9"">Data Responsibility</td><td style=""background-color:#D9D9D9"">Data Prepared By</td><td style=""background-color:#D9D9D9"">Cut Of Date</td></tr>"
                        While dr.Read
                            omg.InnerHtml += "<tr><td>" & dr("Name") & "</td><td>" & dr("source") & "</td><td>" & dr("responsibility") & "</td><td>" & dr("prepared") & "</td><td>" & dr("cutofdate") & "</td></tr>"
                        End While
                        omg.InnerHtml += "</table>"
                    End If
                    dr.Close()
                    dr = Nothing

                    Select Case level
                        Case "0"
                            sqlQuery = "select distinct a.Name, a.owner  from MKPI a " _
                            & "inner join kpirelation b on a.ID = b.idchild " _
                            & "inner join MDIREKTORAT c on a.dir=c.ID " _
                            & "where b.idparent='" & Request.QueryString("id") & "'"
                        Case "1"
                            sqlQuery = "select distinct a.Name, a.owner  from MKPI a " _
                            & "inner join kpirelation b on a.ID = b.idchild " _
                            & "inner join SITE c on a.jobsite=c.ID " _
                            & "where b.idparent='" & Request.QueryString("id") & "'"
                        Case "3"
                            sqlQuery = "select distinct a.Name, a.owner  from MKPI a " _
                           & "inner join kpirelation b on a.ID = b.idchild " _
                           & "inner join MSection c on a.Section=c.ID " _
                           & "where b.idparent='" & Request.QueryString("id") & "'"
                        Case "4"
                            sqlQuery = "select distinct a.Name, a.owner  from MKPI a " _
                            & "inner join kpirelation b on a.ID = b.idchild " _
                            & "inner join MFUNCTION c on a.Func=c.ID inner join MDIREKTORAT dir on dir.id=c.dirid " _
                            & "where b.idparent='" & Request.QueryString("id") & "'"
                    End Select
                    cmd = New SqlCommand(sqlQuery, conn)
                    dr = cmd.ExecuteReader
                    If dr.HasRows Then
                        childkpi.InnerHtml = "<table border='1' style='width:100%;border-top:0;border-bottom:0;border-right:0;border-left:0'><tr><td style='width:60%;border-left:0;border-top:0'>Name of KPI</td><td style='width:40%;border-top:0;border-right:0'>KPIOwner</td></tr>"
                        While dr.Read
                            'childkpi.InnerHtml = Replace(childkpi.InnerHtml, ";border-bottom:0", "")
                            childkpi.InnerHtml += "<tr><td style='border-left:0'>" & dr("name") & "</td><td style='border-right:0'>" & dr("owner") & "</td></tr>"
                        End While
                        childkpi.InnerHtml += "</table>"
                    End If
                    dr.Close()
                    dr = Nothing
                    sqlQuery = "select top 1 * from ttarget where kpiid=" & Request.QueryString("id") & " order by versi desc"
                    cmd = New SqlCommand(sqlQuery, conn)
                    dr = cmd.ExecuteReader
                    If dr.HasRows Then
                        While dr.Read
                            Select Case dr("CalcType")
                                Case "0" 'SUM
                                    KPICYTarget.Text = CDec(IIf(dr("JAN") = "", 0, dr("JAN"))) + CDec(IIf(dr("FEB") = "", 0, dr("FEB"))) + CDec(IIf(dr("MAR") = "", 0, dr("MAR"))) + CDec(IIf(dr("APR") = "", 0, dr("APR"))) + CDec(IIf(dr("MEI") = "", 0, dr("MEI"))) + CDec(IIf(dr("JUN") = "", 0, dr("JUN"))) + CDec(IIf(dr("JUL") = "", 0, dr("JUL"))) + CDec(IIf(dr("AGU") = "", 0, dr("AGU"))) + CDec(IIf(dr("SEP") = "", 0, dr("SEP"))) + CDec(IIf(dr("OKT") = "", 0, dr("OKT"))) + CDec(IIf(dr("NOV") = "", 0, dr("NOV"))) + CDec(IIf(dr("DES") = "", 0, dr("DES")))
                                Case "1" 'AVG
                                    Dim myresult As Decimal
                                    Dim divider As Decimal = 12
                                    myresult = CDec(IIf(dr("JAN") = "", 0, dr("JAN"))) + CDec(IIf(dr("FEB") = "", 0, dr("FEB"))) + CDec(IIf(dr("MAR") = "", 0, dr("MAR"))) + CDec(IIf(dr("APR") = "", 0, dr("APR"))) + CDec(IIf(dr("MEI") = "", 0, dr("MEI"))) + CDec(IIf(dr("JUN") = "", 0, dr("JUN"))) + CDec(IIf(dr("JUL") = "", 0, dr("JUL"))) + CDec(IIf(dr("AGU") = "", 0, dr("AGU"))) + CDec(IIf(dr("SEP") = "", 0, dr("SEP"))) + CDec(IIf(dr("OKT") = "", 0, dr("OKT"))) + CDec(IIf(dr("NOV") = "", 0, dr("NOV"))) + CDec(IIf(dr("DES") = "", 0, dr("DES")))
                                    If dr("JAN") = "" Then divider = divider - 1
                                    If dr("FEB") = "" Then divider = divider - 1
                                    If dr("MAR") = "" Then divider = divider - 1
                                    If dr("APR") = "" Then divider = divider - 1
                                    If dr("MEI") = "" Then divider = divider - 1
                                    If dr("JUN") = "" Then divider = divider - 1
                                    If dr("JUL") = "" Then divider = divider - 1
                                    If dr("AGU") = "" Then divider = divider - 1
                                    If dr("SEP") = "" Then divider = divider - 1
                                    If dr("OKT") = "" Then divider = divider - 1
                                    If dr("NOV") = "" Then divider = divider - 1
                                    If dr("DES") = "" Then divider = divider - 1
                                    KPICYTarget.Text = CDec(myresult / divider)
                                Case "2" 'LAST VAL
                                    KPICYTarget.Text = CDec(dr("annual"))
                                Case "3" 'OTHERS
                                    If IsDBNull(dr("annual")) Then
                                        KPICYTarget.Text = 0
                                    Else
                                        KPICYTarget.Text = CDec(dr("annual"))
                                    End If
                            End Select

                            'Select Case dr("calctype")
                            '    Case "0" 'SUM
                            '        KPICYTarget.Text = CDbl(dr("jan")) + CDbl(dr("feb")) + CDbl(dr("mar")) + CDbl(dr("apr")) + CDbl(dr("mei")) + CDbl(dr("jun")) + CDbl(dr("jul")) + CDbl(dr("agu")) + CDbl(dr("sep")) + CDbl(dr("okt")) + CDbl(dr("nov")) + CDbl(dr("des"))
                            '    Case "1"
                            '        KPICYTarget.Text = (CDbl(dr("jan")) + CDbl(dr("feb")) + CDbl(dr("mar")) + CDbl(dr("apr")) + CDbl(dr("mei")) + CDbl(dr("jun")) + CDbl(dr("jul")) + CDbl(dr("agu")) + CDbl(dr("sep")) + CDbl(dr("okt")) + CDbl(dr("nov")) + CDbl(dr("des"))) / 12
                            '    Case "2"
                            '        KPICYTarget.Text = dr("des")
                            'End Select
                        End While
                    End If
                    dr.Close()
                    dr = Nothing
                    conn.Close()
                Catch ex As Exception
                    lblMessage.Text = ex.Message
                Finally
                    If conn.State = ConnectionState.Open Then
                        conn.Close()
                    End If
                End Try
            End If
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub

End Class