﻿Imports System.Web.Services
Imports System.Security.Cryptography

Partial Public Class Meeting
    Inherits System.Web.UI.Page

    Shared _ldrapat As List(Of DRAPAT)
    Shared _ldpeserta As List(Of DPESERTA)
    Dim supportedFormats() As String = New String() {"dd/MM/yyyy"}
    Dim numOfRows As Integer = 1

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Try
        '    If Session("permission") = "" Or Session("permission") = "M" Or Session("permission") = Nothing Then
        '        Response.Redirect("Login.aspx", False)
        '        Exit Sub
        '    ElseIf ("S;1;2;7;8;A;B").IndexOf(Session("permission")) = -1 Then
        '        Response.Redirect("default.aspx", False)
        '        Exit Sub
        '    Else
        If Not Page.IsPostBack Then
            Me.Page.Form.Enctype = "multipart/form-data"
            'Me.txtTglRap.Text = DateTime.Now.ToString("dd/MM/yyyy")
            If Request.QueryString("id") IsNot Nothing Then
                ClientScript.RegisterStartupScript(GetType(Page), "ff", "<script>getrapat('" & Request.QueryString("id") & "');</script>")
            Else
                ClientScript.RegisterStartupScript(GetType(Page), "ff", "<script>addPToTable();addRowToTable();</script>")
            End If
        End If
        '    End If
        'Catch ex As Exception
        '    Response.Write(ex.Message + ex.StackTrace)
        'End Try
    End Sub

    Public Function FormatDate(ByVal dt As DateTime) As String
        Return dt.ToString("dd/MM/yyyy")
    End Function
End Class
