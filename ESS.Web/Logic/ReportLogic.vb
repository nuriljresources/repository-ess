﻿Imports System.Data.SqlClient

Public Class ReportLogic

    Public Shared Function GetPaymentApplicationUsage(ByVal UserId As String, ByVal PaymentNo As String, ByVal PaymentType As String, ByVal Amount As String, ByVal DateCreated As String, ByVal ToPerson As String) As DataTable
        Dim dtPaymentUsage As DataTable = New DataTable("PaymentApplicationInternal")
        dtPaymentUsage.Columns.Add("NikSite")
        dtPaymentUsage.Columns.Add("Nik")
        dtPaymentUsage.Columns.Add("Nama")
        dtPaymentUsage.Columns.Add("Position")
        dtPaymentUsage.Columns.Add("Division")
        dtPaymentUsage.Columns.Add("Site")
        dtPaymentUsage.Columns.Add("PaymentNo")
        dtPaymentUsage.Columns.Add("CostCenter")
        dtPaymentUsage.Columns.Add("Amount")
        dtPaymentUsage.Columns.Add("To")
        dtPaymentUsage.Columns.Add("Usage")
        dtPaymentUsage.Columns.Add("DateCreated")
        dtPaymentUsage.Columns.Add("DateRequest")
        dtPaymentUsage.Columns.Add("Company")
        dtPaymentUsage.Columns.Add("NameBank")
        dtPaymentUsage.Columns.Add("AddressBank")
        dtPaymentUsage.Columns.Add("BankBranch")
        dtPaymentUsage.Columns.Add("NoRekening")
        dtPaymentUsage.Columns.Add("NameRekening")

        Dim conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        conn.Open()
        Dim strQueryDocument As String
        strQueryDocument = <![CDATA[SELECT TOP 1
                            Employee.Nik,
                            Employee.NIKSITE AS NikSite,
                            Employee.Nama AS Nama,
                            ISNULL(Company.NmCompany, '') AS Company,
                            ISNULL(Jabatan.NmJabat, '') AS Jabatan, 
                            ISNULL(Divisi.NmDivisi, '') AS Divisi,
                            ISNULL(Location.NmSite, '') AS [Site],
                            ISNULL(Location.KdSite, '') AS [KodeSite],
                            ISNULL(H_A103.kdbank, '') AS KodeBank,
	                        ISNULL(H_A103.norekening, '') AS NoRekening,
	                        ISNULL(H_A103.nmrekening, '') AS NameRekening,
	                        ISNULL(H_A106.NmBank, '') AS NameBank,
	                        ISNULL(H_A106.AlmBank, '') AS AddressBank,
	                        ISNULL(H_A106.branch, '') AS BankBranch,
	                        ISNULL(Jabatan.GLCostLS, '') AS CostCenter

                        FROM H_A101 Employee
                            LEFT JOIN H_A150 Jabatan ON Employee.KdJabatan = Jabatan.KdJabat
                            LEFT JOIN H_A140 Divisi ON Jabatan.KdDivisi = Divisi.KdDivisi
                            LEFT JOIN H_A120 Location ON Employee.pycostcode = Location.KdSite
                            LEFT JOIN H_A110 Company ON Location.KdCompany = Company.KdCompany
                            
                            LEFT JOIN H_A103 ON H_A103.Nik = Employee.Nik
                            LEFT JOIN H_A106 ON H_A106.KdBank = H_A103.kdbank
                            
                            
                        WHERE Employee.nik = '{0}' AND Jabatan.StEdit <> '2' and Jabatan.fLS=0]]>.Value

        strQueryDocument = String.Format(strQueryDocument, UserId)
        Dim dtRow As DataTable = New DataTable
        Dim sqlTotalAdapter As SqlDataAdapter = New SqlDataAdapter(strQueryDocument, conn)
        sqlTotalAdapter.Fill(dtRow)
        conn.Close()

        If dtRow.Rows.Count > 0 Then
            Dim Item As System.Data.DataRow = dtRow.Rows(0)

            dtPaymentUsage.Rows.Add(Item("KodeSite"), Item("Nik"), Item("Nama"), Item("Jabatan"), Item("Divisi"), Item("Site"), PaymentNo, Item("CostCenter"), Amount, ToPerson, PaymentType, DateCreated, DateTime.Now.ToString("dd MMM yyyy"), Item("Company"), Item("NameBank"), Item("AddressBank"), Item("BankBranch"), Item("NoRekening"), Item("NameRekening"))

        End If


        Return dtPaymentUsage
    End Function

End Class
