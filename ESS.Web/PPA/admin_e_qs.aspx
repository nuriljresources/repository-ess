﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="admin_e_qs.aspx.vb" ValidateRequest="false" Inherits="EXCELLENT.admin_e_qs" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <style type="text/css">


body{
  padding: 1em;
  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAAQElEQVQIW2P89OvDfwYo+PHjJ4zJwMHBzsAIk0SXAKkCS2KTAEu++vQSbizIKGQAl0SXAJkGlsQmAbcT2Shk+wH0sCzAEOZW1AAAAABJRU5ErkJggg==);
}
a{
  color: #739931;
}
.page{
  max-width: 70em;
  margin: 0 auto;
}
table th,
table td{
  text-align: left;
}
table.layout{
  width: 100%;
  border-collapse: collapse;
}
table.display{
  margin: 1em 0;
}
table.display th,
table.display td{
  border: 1px solid #B3BFAA;
  padding: .5em 1em;
}

table.display th{ background: #D5E0CC; }
table.display td{ background: #fff; }

table.responsive-table{
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
}

.center{
  text-align:center;
  }
.none{
	border-style:none;
    text-align:center;
  }
  
    /* Accordion */
.accordionHeader
{
    border: 1px solid #2F4F4F;
    color: white;
    background-color: #2E4d7B;
	font-family: Arial, Sans-Serif;
	font-size: 12px;
	font-weight: bold;
    padding: 5px;
    margin-top: 5px;
    cursor: pointer;
}

#master_content .accordionHeader a
{
	color: #2F4F4F;
	background: none;
	text-decoration: none;
}

#master_content .accordionHeader a:hover
{
	background: none;
	text-decoration: underline;
}

.accordionHeaderSelected
{
    border: 1px solid #2F4F4F;
    color: white;
    background-color: #5078B3;
	font-family: Arial, Sans-Serif;
	font-size: 12px;
	font-weight: bold;
    padding: 5px;
    margin-top: 5px;
    cursor: pointer;
}

#master_content .accordionHeaderSelected a
{
	color: #FFFFFF;
	background: none;
	text-decoration: none;
}

#master_content .accordionHeaderSelected a:hover
{
	background: none;
	text-decoration: underline;
}

.accordionContent
{
    background-color: #D3DEEF;
    border: 1px dashed #2F4F4F;
    border-top: none;
    padding: 5px;
    padding-top: 10px;
}
/*end accordion*/
  
  #footer {
	background:#373737;
	clear:both;
	bottom: 0;
	position: relative;
	margin-top: -50px; /* negative value of footer height */
	height: 20px;
	clear:both;
	}
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 
	 FOOTER
	 	 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

.nav-footer
{
	position: relative;
	text-align: center;
	clear: both;
}

.nav-footer ul{
	padding:0;
	margin:0;
	text-align:center;
	}

.nav-footer li{
	background:none;
	display:inline;
	border-right:1px dotted #686868;
	padding:0 10px;
	}

.nav-footer li.first {
	border-left:1px dotted #686868;
	}
	
.copyright {
	color:#999;
	font-size:.8em;
	clear:both;
	}

.nav-footer a:link,
.nav-footer a:visited {
	color:#CCC;
	}

@media (max-width: 30em){
    table.responsive-table{
      box-shadow: none;  
    }
    table.responsive-table thead{
      display: none; 
    }
  table.display th,
  table.display td{
    padding: .5em;
  }
    
  table.responsive-table td:nth-child(1):before{
    content: 'Number';
  }
  table.responsive-table td:nth-child(2):before{
    content: 'Name';
  }
  table.responsive-table td:nth-child(1),
  table.responsive-table td:nth-child(2){
    padding-left: 25%;
  }
  table.responsive-table td:nth-child(1):before,
  table.responsive-table td:nth-child(2):before{
    position: absolute;
    left: .5em;
    font-weight: bold;
  }
  
    table.responsive-table tr,
    table.responsive-table td{
        display: block;
    }
    table.responsive-table tr{
        position: relative;
        margin-bottom: 1em;
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
    }
    table.responsive-table td{
        border-top: none;
    }
    table.responsive-table td.organisationnumber{
        background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    table.responsive-table td.actions{
        position: absolute;
        top: 0;
        right: 0;
        border: none;
        background: none;
    }
    table.responsive-table td.top{
    	background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
}

</style>
    
    <!-- TinyMCE -->
<script type="text/javascript" src="../tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "insertdatetime,preview,fullscreen",
		//style,advimage,advlink,emotions,iespell,save,advhr,layer,table,media,searchreplace,print,contextmenu,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave,directionality,paste,pagebreak
		// Theme options
		
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
		//theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
		//theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		//theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		//theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing: true,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",
        
		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<!-- /TinyMCE -->

</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr>
    <td>Question ID</td>
    <td>
     <asp:DropDownList ID="ddlqs" runat="server" AutoPostBack="True" 
                        DataSourceID="sqlquestions" DataTextField="ms_id" DataValueField="ms_id">
                        <asp:ListItem Value =""> </asp:ListItem>
                        </asp:DropDownList>
                    <asp:SqlDataSource ID="sqlquestions" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:localdev %>" 
                        SelectCommand="SELECT [ms_id] FROM [ms_questions] ORDER BY [ms_id]">
                    </asp:SqlDataSource>
                    <asp:HiddenField ID="hstsedit" runat ="server" />
    </td>
    </tr>
    </table>
    <div id="content" style="width:650px;">
    <ajaxToolkit:ToolkitScriptManager runat="server" ID="ScriptManager1">
        <ControlBundles>
            <ajaxToolkit:ControlBundle Name="Accordion"/>
        </ControlBundles>
    </ajaxToolkit:ToolkitScriptManager>
    <ajaxToolkit:Accordion ID="MyAccordion" runat="server" SelectedIndex="0"
            HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
            ContentCssClass="accordionContent" FadeTransitions="false" FramesPerSecond="40" 
            TransitionDuration="250" AutoSize="None" RequireOpenedPane="false" SuppressHeaderPostbacks="true">
           <Panes>
            <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                <Header><a href="" class="accordionLink">Header</a></Header>
                <Content>
                   <asp:TextBox ID="txtheader" runat="server" TextMode="MultiLine"></asp:TextBox> 
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">
                <Header><a href="" class="accordionLink">Question 1</a></Header>
                <Content>
                    <asp:TextBox ID="txtq1" runat="server" TextMode="MultiLine"></asp:TextBox> 
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPane3" runat="server">
                <Header><a href="" class="accordionLink">Question 2</a></Header>
                <Content>
                    <asp:TextBox ID="txtq2" runat="server" TextMode="MultiLine"></asp:TextBox> 
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPane4" runat="server">
                <Header><a href="" class="accordionLink">Question 3</a></Header>
                <Content>
                    <asp:TextBox ID="txtq3" runat="server" TextMode="MultiLine"></asp:TextBox> 
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPane5" runat="server">
                <Header><a href="" class="accordionLink">Question 4</a></Header>
                <Content>
                    <asp:TextBox ID="txtq4" runat="server" TextMode="MultiLine"></asp:TextBox>
                </Content>
            </ajaxToolkit:AccordionPane>
            <ajaxToolkit:AccordionPane ID="AccordionPane6" runat="server">
                <Header><a href="" class="accordionLink">Question 5</a></Header>
                <Content>
                    <asp:TextBox ID="txtq5" runat="server" TextMode="MultiLine" ></asp:TextBox> 
                </Content>
            </ajaxToolkit:AccordionPane>
            </Panes>
        </ajaxToolkit:Accordion>
        </div>
        <table>
            <tr>
                <td>
                    <asp:Button ID="btnsave" runat="server" Text="Save" />
                </td>
            </tr>
        </table>
        
        
    </div>
    </form>
</body>
</html>
