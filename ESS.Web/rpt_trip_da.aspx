﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="rpt_trip_da.aspx.vb" Inherits="EXCELLENT.rpt_trip_da" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Trip Report</title>
    <script src="Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="Scripts/jscal2.js" type ="text/javascript"></script>
    <script src="Scripts/en.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="css/win2k/win2k.css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hkdsite" runat="server" />
    <table>
    <tr>
    <td>
        From
    </td>
    <td><asp:TextBox ID="txtdate1" runat="server"></asp:TextBox> <button id="btndt1">...</button></td>
    <td> To </td>
    <td><asp:TextBox ID="txtdate2" runat="server"></asp:TextBox> <button id="btndt2">...</button></td>
    <td><asp:Button ID="btnok" runat="server" text="OK"/></td>
    <td><asp:DropDownList ID="ddlrptlist" runat="server" /></td>
    </tr>
    </table>
    
     <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
        Font-Size="8pt" Height="579px" Width="1160px">
        <LocalReport ReportPath="Laporan\rptbtrda.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="hr_dttripda" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    
        <rsweb:ReportViewer ID="ReportViewer2" runat="server" Font-Names="Verdana" 
        Font-Size="8pt" Height="579px" Width="1160px">
        <LocalReport ReportPath="laporan\rptbtraco.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="hr_dttripaco" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    
    
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="EXCELLENT.hrTableAdapters.dttripdaTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="hkdsite" Name="kdsite" PropertyName="Value" Type="String" />
            <asp:ControlParameter ControlID="txtdate1" Name="date1" PropertyName="Text" Type="DateTime" />
            <asp:ControlParameter ControlID="txtdate2" Name="date2" PropertyName="Text" Type="DateTime" />
        </SelectParameters>
    </asp:ObjectDataSource>
    
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="EXCELLENT.hrTableAdapters.dttripacoTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="hkdsite" Name="kdsite" PropertyName="Value" Type="String" />
            <asp:ControlParameter ControlID="txtdate1" Name="date1" PropertyName="Text" Type="DateTime" />
            <asp:ControlParameter ControlID="txtdate2" Name="date2" PropertyName="Text" Type="DateTime" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <div>
    
    </div>
    </form>
</body>
 <script type = "text/javascript" >
        var today = new Date();
        var cal = Calendar.setup({
            onSelect: function(cal) { cal.hide() },
            showTime: false
        });


        cal.manageFields("btndt1", "txtdate1", "%m/%d/%Y");
        cal.manageFields("btndt2", "txtdate2", "%m/%d/%Y");
 </script>
</html>
