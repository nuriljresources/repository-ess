﻿Imports System.Data.SqlClient

Partial Public Class popupexcel
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.ContentType = "application/vnd.ms-excel"
        '------------------
        Dim sqlQuery As String = ""
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim beforeyear As String
        Dim beforemonth As String
        Dim cmd As SqlCommand
        Dim approved As Integer = 0
        Dim month As Integer = Request.QueryString("m")
        Dim year As Integer = Request.QueryString("y")
        Dim KPILevel As String = Request.QueryString("lvl")
        Dim KPIFunc As String = Request.QueryString("f")
        Dim KPIJobsite As String = Request.QueryString("j")
        Dim KPISection As String = Request.QueryString("s")

        If month = 1 Then
            beforeyear = year - 1
            beforemonth = 12
        Else
            beforeyear = year
            beforemonth = month - 1
        End If

        Select Case KPILevel
            Case "0"
                sqlQuery = "Select count(*) from TBSCAPPR where level='0' and month ='" & month & "' and year='" & year & "'"
            Case "1"
                sqlQuery = "Select count(*) from TBSCAPPR where level='1' and month ='" & month & "' and year='" & year & "' and func='" & KPIFunc & "'"
            Case "2"
                sqlQuery = "Select count(*) from TBSCAPPR where level='2' and month ='" & month & "' and year='" & year & "' and jobsite='" & KPIJobsite & "' and section='" & KPISection & "'"
            Case "3"
                sqlQuery = "Select count(*) from TBSCAPPR where level='3' and month ='" & month & "' and year='" & year & "' and jobsite='" & KPIJobsite & "'"
        End Select
        cmd = New SqlCommand(sqlQuery, conn)
        conn.Open()
        approved = cmd.ExecuteScalar
        conn.Close()

        sqlQuery = "select a.year,a.month,a.kpiid as id,b.Level,'' as workunit,d.STName as st,c.SO,isnull(e.jabatan,'-') as soleader," & _
                    "b.Name as kpi,b.Owner as kpiowner,b.UoM,b.Formula,b.RedParam,b.GreenParam," & IIf(approved = 0, "isnull(dd." & GetMonth(month) & ",0)", "isnull(a.target,0)") & " as target,isnull(a.actual,0) as actual,isnull(a.achievement,0) as achievement, " & _
                    "ff.kpiid,isnull(ff.achievement,-9999) as oldach,p.perspective,isnull(ff.actual,-9999) as oldact " & _
                    ",isnull(b.RedCond,'0') as RedCond,isnull(b.GreenCond,'0') as GreenCond,b.calctype,b.basedon, isnull(tp.value,0) as bobot " & _
                    "from tbsc a inner join MKPI b on a.kpiid = b.id " & _
                    "inner join MSO c on b.SOID =c.ID  " & _
                    "inner join MST d on c.STID = d.STID  " & _
                    "left join MJABATAN e on e.id = c.BUMALeader  " & _
                    "left join " & _
                    "(select kpiid,achievement,actual from TBSC t where t.year='" & beforeyear & "' and t.month='" & beforemonth & "') ff on ff.kpiid =a.kpiid  left join mperspective p on d.PRID=p.id " & _
                    "left join " & _
                    "(select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & year & "' group by kpiid,year) b on a.versi=b.versi and a.kpiid = b.kpiid where a.year='" & year & "')dd on a.kpiid=dd.kpiid  " & _
                    "left join tperspective tp on p.id=tp.pid and tp.year=a.year and tp.level=" & getMapLevel(KPILevel) & IIf(KPILevel = 0 Or KPILevel = 3, "", IIf(KPILevel = "1", " and tp.funcid=b.func ", " and tp.funcid= (select funcid from msection where id=b.section) ")) & _
                    "where a.year='" & year & "' and a.month='" & month & "' and b.Level='" & KPILevel & "' "

        Select Case KPILevel
            Case "0"
                sqlQuery += " order by p.id,d.stid,c.ID"
            Case "1"
                sqlQuery += " and b.func='" & KPIFunc & "' order by p.id,d.stid,c.ID"
            Case "2"
                sqlQuery += " and b.jobsite='" & KPIJobsite & "' and b.section='" & KPISection & "' order by p.id,d.stid,c.ID"
            Case "3"
                sqlQuery += " and b.jobsite='" & KPIJobsite & "' order by p.id,d.stid,c.ID"
        End Select

        Dim da As New SqlDataAdapter(sqlQuery, conn)
        Dim dt As New DataTable()
        da.Fill(dt)

        '-----------------------------------
        Dim mytableresult As String = ""
        Dim perspective As String = ""
        Dim so As String = ""
        Dim socount As Integer = 1
        Dim row As Integer = 1
        Dim ach As Decimal = 0
        If dt.Rows.Count = 0 Then
            header1.InnerHtml = "BSC TRANSACTION"
        Else
            Select Case KPILevel
                Case "0"
                    header1.InnerHtml = "BUMA PERFORMANCE " & GetMonth(month).ToUpper() & " " & year & getApproval(approved)
                Case "1"
                    header1.InnerHtml = KPIFunc & " PERFORMANCE " & GetMonth(month).ToUpper() & " " & year & getApproval(approved)
                Case "2"
                    header1.InnerHtml = KPISection & " " & KPIJobsite & " PERFORMANCE " & GetMonth(month).ToUpper() & " " & year & getApproval(approved)
                Case "3"
                    header1.InnerHtml = KPIJobsite & " PERFORMANCE " & GetMonth(month).ToUpper() & " " & year & getApproval(approved)
            End Select
        End If
        'style='align:center;font-size:12px;background-color:#449A50;color:white;font-weight:bold;'
        mytableresult += "<div id='totalach' style='font-size:16px;font-weight:bold;'></div>"
        mytableresult += "<table border='1' id='BSCTABLE' class='bsc' style='width:100%;font-size:11px'>"
        mytableresult += "<thead class='headbsctable'><td style='width:18%' align='center'>Strategic Objective</td><td align='center'>Achievement SO</td><td style='width:30%' align='center'>KPI</td><td align='center' style='width:7%'>KPI Owner</td><td align='center'>UoM</td>" & _
                         "<td align='center'>Target</td><td  align='center'>Actual</td><td  align='center'>Achievement <br> (Act Vs Target)</td><td align='center'>Trend</td><td>Attach</td><td align='center'>Details</td></thead>"
        For Each dr As DataRow In dt.Rows
            If perspective <> dr("perspective") Then
                perspective = dr("perspective") '<div id='p" & row & "'>VAL</div>
                mytableresult += "<tr><td colspan='12' align='center' style='background-color:#EACB92;'>" & perspective & "<br><input type='text' id='p" & row & "' style='width:3%;border-style:none;background:transparent;' READONLY size='5'/>% X <input type='text' style='width:3%;border-style:none;background:transparent;' READONLY size='5' id='tp" & row & "' value='" & System.Math.Round(dr("bobot"), 1) & "'/>% &nbsp; = <input type='text' style='font-size:24px;font-weight:bold;width:4%;border-style:none;background:transparent;' READONLY size='5' id='hp" & row & "'/>%</td></tr>"
            End If
            mytableresult += "<tr>"
            If so <> dr("SO") Then
                mytableresult = mytableresult.Replace("x&x", socount)
                so = dr("SO")
                socount = 1
                mytableresult += "<td rowspan='x&x'>" & dr("SO") & "</td><td rowspan='x&x' align='center'><div id='so" & row & "'>VAL</div></td>"
            Else
                'mytableresult += "<td>" & dr("SO") & "</td>"
                socount += 1
            End If
            mytableresult += "<td align='left'>" & dr("kpi")
            mytableresult += "<input type='hidden' id='kpiid" & row & "' value='" & dr("id") & "'/>"
            mytableresult += "<input type='hidden' id='year" & row & "' value='" & dr("year") & "'/>"
            mytableresult += "<input type='hidden' id='month" & row & "' value='" & dr("month") & "'/>"
            mytableresult += "<input type='hidden' id='calctype" & row & "' value='" & dr("calctype") & "'/>"
            mytableresult += "</td><td>" & dr("kpiowner") & "</td><td align='center'>" & dr("UoM") & "</td>"
            mytableresult += "<td><input type='text' style='border-style:none' size=6 onChange='calc();' id='kpit" & row & "' value='" & System.Math.Round(CDec(dr("target")), 1) & "' DISABLED/></td>"
            mytableresult += "<td><input type='text' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='" & System.Math.Round(dr("actual"), 1) & "' " & "" & "/></td><td><input type='text' size=3  onChange='calc();' style='border-style:none;text-align:right' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 1) & "' DISABLED/>%</td>"
            If dr("calctype") = "0" Then 'MIN
                If dr("actual") = 0 Then
                    ach = 0
                Else
                    ach = dr("target") / dr("actual") * 100
                End If
            Else 'MAX
                If dr("target") = 0 Then
                    ach = 0
                Else
                    ach = dr("actual") / dr("target") * 100
                End If
            End If
            If dr("oldact") = -9999 Then
                mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "equal.png'/></td>"
            ElseIf dr("oldact") > dr("actual") Then
                mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "down.png'/></td>"
            ElseIf dr("oldact") < dr("actual") Then
                mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "up.png'/></td>"
            Else
                mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "equal.png'/></td>"
            End If
            mytableresult += "<td align='center'><input type='button' id='btnUpload' Style='width:10px;height:25px;background-image: url(../images/attachment_icon.gif); background-repeat: no-repeat;cursor:hand;background-color:transparent;border:0' onclick='javascript:openupload(""" & dr("id") & """,""" & dr("level") & """,""" & dr("year") & """,""" & dr("month") & """,""" & dr("kpi") & """)'/></td>"
            mytableresult += "<td align='center'><input type='button' id='btnAdd' value='VIEW' onclick='javascript:openmodal(""" & dr("id") & """,""" & dr("level") & """,""" & dr("year") & """,""" & dr("month") & """,""" & dr("kpi") & """)'/></td></tr>"
            row += 1
        Next
        mytableresult = mytableresult.Replace("x&x", socount)
        result.InnerHtml = mytableresult + "</table>"
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, "Tes", "alert('a')", True)
        '===================
        Response.Write(header1.InnerHtml + result.InnerHtml)
        Response.End()
    End Sub

    Function getMapLevel(ByVal level As String) As String
        If level = "3" Then
            Return "0"
        ElseIf level = "2" Then
            Return "1"
        Else
            Return level
        End If
        Return level
    End Function

    Function GetMonth(ByVal s As String) As String
        Select Case s
            Case "1"
                Return "jan"
            Case "2"
                Return "feb"
            Case "3"
                Return "mar"
            Case "4"
                Return "apr"
            Case "5"
                Return "mei"
            Case "6"
                Return "jun"
            Case "7"
                Return "jul"
            Case "8"
                Return "agu"
            Case "9"
                Return "sep"
            Case "10"
                Return "okt"
            Case "11"
                Return "nov"
            Case "12"
                Return "des"
        End Select
    End Function

    Function getApproval(ByVal approved As Integer) As String
        If approved = 0 Then 'Draft
            Return "<img src='../images/draft.png' align='right'>"
        Else
            Return "<img src='../images/approved.png'  align='right'>"
        End If
    End Function

    Function getImage(ByVal value As String, ByVal redCond As String, ByVal redVal As Decimal, ByVal greenCond As String, ByVal greenVal As Decimal) As String
        Try
            Select Case redCond
                Case "0"
                    If value > redVal Then
                        Return "red"
                    End If
                Case "1"
                    If value >= redVal Then
                        Return "red"
                    End If
                Case "2"
                    If value < redVal Then
                        Return "red"
                    End If
                Case "3"
                    If value <= redVal Then
                        Return "red"
                    End If
            End Select
            Select Case greenCond
                Case "0"
                    If value > greenVal Then
                        Return "green"
                    End If
                Case "1"
                    If value >= greenVal Then
                        Return "green"
                    End If
                Case "2"
                    If value < greenVal Then
                        Return "green"
                    End If
                Case "3"
                    If value <= greenVal Then
                        Return "green"
                    End If
            End Select
            Return "yellow"
        Catch ex As Exception
            'lblMessage.Text = ex.Message
        End Try
    End Function
End Class