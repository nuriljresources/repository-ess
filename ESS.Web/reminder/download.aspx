﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="download.aspx.vb" Inherits="EXCELLENT.download" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="font-family: Arial, Helvetica, sans-serif; color: #0066FF; line-height: normal; border: thin solid #663300; font-size: smaller;">
        <asp:Label ID="lblinfo" runat="server" Text=""></asp:Label> <br />
        Department  : <asp:DropDownList ID="ddldept" runat="server">
        <asp:ListItem Value = "IT"> IT</asp:ListItem>
        <asp:ListItem Value = "HRCA"> HRCA</asp:ListItem>
        <asp:ListItem Value = "Legal"> Legal</asp:ListItem>
        <asp:ListItem Value = "Plant"> Plant</asp:ListItem>
        <asp:ListItem Value = "Finance Accounting"> Finance Accounting</asp:ListItem>
        <asp:ListItem Value = "IAS"> IAS</asp:ListItem>
        <asp:ListItem Value = "Finance"> Finance</asp:ListItem>
        <asp:ListItem Value = "HR"> HR</asp:ListItem>
        <asp:ListItem Value = "Accounting"> Accounting</asp:ListItem>
        <asp:ListItem Value = "Exploration"> Exploration</asp:ListItem>
        <asp:ListItem Value = "Technical Development"> Technical Development</asp:ListItem>
        <asp:ListItem Value = "External"> External</asp:ListItem>
        </asp:DropDownList> &nbsp;
        Folder Name : <asp:DropDownList ID="ddlupload" runat="server" 
            DataSourceID="DS_folder" DataTextField="folder" DataValueField="guid">
        </asp:DropDownList>
        <asp:SqlDataSource ID="DS_folder" runat="server" 
            ConnectionString="<%$ ConnectionStrings:jrn_permit %>" 
            SelectCommand="SELECT guid, folder FROM ms_folder"></asp:SqlDataSource> <br />
        <asp:Button ID="btnupload" runat = "server" Text = "Upload" />
    </div>
    </form>
</body>
</html>
