﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SearchEmployeeBSC.aspx.vb"
   Inherits="EXCELLENT.SearchEmployeeBSC" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Search Karyawan</title>

   <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>

   <script type="text/javascript">
      function executeClose(nik, nama) {
         window.returnValue = nik + ";" + nama;
         window.close();
      }
      function keyPressed(e) {
         switch (e.keyCode) {
            case 13:
               {
                  document.getElementById("btnSearch").focus();
                  document.getElementById("btnSearch").click();
                  break;
               }
            default:
               {
                  break;
               }
         }
      }
   </script>

   <style type="text/css">
      input.btn
      {
         color: #050;
         font: bold 90% 'trebuchet ms' ,helvetica,sans-serif;
         background-color: #fed;
         border: 1px solid;
         border-color: #696 #363 #363 #696;
         filter: progid:DXImageTransform.Microsoft.Gradient (GradientType=0,StartColorStr= '#ffffffff' ,EndColorStr= '#ffeeddaa' );
         padding: 0 2px 0 2px;
         cursor: hand;
      }
      body
      {
         font-family: arial;
         font-size: 14px;
      }
      .plain
      {
         border: 1px solid #000000;
      }
      .grid
      {
         cursor: hand;
         border: 1px solid #C0C0C0;
      }
      .grid2 td
      {
         border: 1px solid #C0C0C0;
      }
      .gridHeader th
      {
         border: 1px solid #C0C0C0;
         font-weight: bold;
         background: #00CC00;
      }
   </style>
</head>
<body>
   <form id="form1" runat="server">
   <div>
      <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
      </asp:ToolkitScriptManager>
      <asp:UpdatePanel ID="UpdatePanel2" runat="server">
         <ContentTemplate>
            <table>
               <tr>
                  <td colspan="3">
                     <asp:Label ID="lblKet" runat="server" Text=""></asp:Label>
                  </td>
               </tr>
               <tr>
                  <td>
                     Nama Karyawan
                  </td>
                  <td>
                     <asp:TextBox ID="srchTextBox" runat="server" CssClass="plain" onkeydown="keyPressed(event);"></asp:TextBox>
                  </td>
                  <td>
                     <asp:Button ID="btnSearch" CssClass="btn" runat="server" Text="Search" />
                  </td>
               </tr>
               <tr>
                  <td colspan="3">
                     <asp:GridView ID="srchResult" runat="server" AllowPaging="True" Width="100%" CssClass="grid">
                        <PagerSettings Position="Top" />
                        <RowStyle CssClass="grid2" />
                        <PagerStyle BorderStyle="None" />
                        <HeaderStyle CssClass="gridHeader" BackColor="#00CC00" />
                        <AlternatingRowStyle BackColor="silver" />
                     </asp:GridView>
                  </td>
               </tr>
            </table>
         </ContentTemplate>
      </asp:UpdatePanel>
      <asp:UpdateProgress ID="progress1" runat="server" DisplayAfter="50">
         <ProgressTemplate>
            <asp:Panel ID="pnlProgressBar" runat="server">
               <div>
                  <asp:Image ID="Image1" runat="Server" ImageUrl="../images/ajax-loader.gif" />Harap
                  Sabar Menunggu...</div>
            </asp:Panel>
         </ProgressTemplate>
      </asp:UpdateProgress>
   </div>
   </form>

   <script type="text/javascript">
      //document.getElementById("srchTextBox").focus();
      $(document).ready(function() {
         document.getElementById("srchTextBox").focus();
      });     
   </script>

</body>
</html>
