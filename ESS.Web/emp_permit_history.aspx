﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="emp_permit_history.aspx.vb" Inherits="EXCELLENT.emp_permit_history" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:GridView ID="GridView1" runat="server" BorderColor="#333333" 
            BorderStyle="Solid" BorderWidth="1px" CellPadding="2" Font-Size="Small" 
            ForeColor="#333333" GridLines="None">
            <RowStyle BackColor="#E3EAEB" BorderStyle="None" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#C5BBAF" BorderStyle="Solid" Font-Bold="True" 
                ForeColor="#333333" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#7C6F57" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    
    </div>
    </form>
</body>
</html>
