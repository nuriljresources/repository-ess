﻿Imports System.Data
Imports System.Data.SqlClient

Partial Public Class crossdept
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim strcon As String = "select emp_code, modul, lvl from ms_user where emp_code = '" + Session("otorisasi_permit") + "' and stedit <> '2'"
        Dim li_count As Integer

        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            For li_count = 0 To dtb.Rows.Count - 1
                If dtb.Rows(li_count)!modul.ToString = "IT" Then
                    ddldept.Items.Add(New ListItem("IT", "IT"))
                ElseIf dtb.Rows(li_count)!modul.ToString = "HRCA" Then
                    ddldept.Items.Add(New ListItem("HRCA", "HRCA"))
                ElseIf dtb.Rows(li_count)!modul.ToString = "Legal" Then
                    ddldept.Items.Add(New ListItem("Legal", "Legal"))
                ElseIf dtb.Rows(li_count)!modul.ToString = "Plant" Then
                    ddldept.Items.Add(New ListItem("Plant", "Plant"))
                ElseIf dtb.Rows(li_count)!modul.ToString = "FINACC" Then
                    ddldept.Items.Add(New ListItem("Finance Accounting", "FINACC"))
                ElseIf dtb.Rows(li_count)!modul.ToString = "IAS" Then
                    ddldept.Items.Add(New ListItem("IAS", "IAS"))
                ElseIf dtb.Rows(li_count)!modul.ToString = "HR" Then
                    ddldept.Items.Add(New ListItem("HR", "HR"))
                ElseIf dtb.Rows(li_count)!modul.ToString = "Accounting" Then
                    ddldept.Items.Add(New ListItem("Accounting", "Accounting"))
                ElseIf dtb.Rows(li_count)!modul.ToString = "Exploration" Then
                    ddldept.Items.Add(New ListItem("Exploration", "Exploration"))
                ElseIf dtb.Rows(li_count)!modul.ToString = "TD" Then
                    ddldept.Items.Add(New ListItem("Technical Development", "TD"))
                ElseIf dtb.Rows(li_count)!modul.ToString = "SCM-CCMS" Then
                    ddldept.Items.Add(New ListItem("SCM Contract/CMS", "SCM-CCMS"))
                ElseIf dtb.Rows(li_count)!modul.ToString = "SCM-PROC" Then
                    ddldept.Items.Add(New ListItem("SCM Procurement", "SCM-PROC"))
                End If
            Next

        Catch ex As Exception

        End Try



    End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
    '    Session("dept") = ""
    '    Session("otorisasi_permit") = ""
    '    Session("permission_permit") = ""
    '    Session("NmUser_permit") = ""
    '    Session("divisi") = ""
    '    Session("modul_permit") = ""
    '    Response.Redirect("signin_permit.aspx")
    'End Sub

    Private Sub seldept()

    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLogin.Click
        Session("modul_permit") = ddldept.SelectedValue.ToString

        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim strcon As String = "select lvl from ms_user where emp_code = '" + Session("otorisasi_permit") + "' and stedit <> '2' and modul = '" + Session("modul_permit") + "'"


        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)
            Session("permission_permit") = dtb.Rows(0)!lvl.ToString
            Response.Redirect("index.aspx")
        Catch ex As Exception

        End Try

    End Sub
End Class