﻿Imports System.Data.SqlClient

Partial Public Class pica
    Inherits System.Web.UI.Page

    Dim numOfRows As Integer = 1
    Dim click As New List(Of String)

    Protected Property Rows() As Integer
        Get
            If Not ViewState("Rows") Is Nothing Then
                Return CInt(Fix(ViewState("Rows")))
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("Rows") = value
        End Set
    End Property

    ' Columns property to hold the Columns in the ViewState
    Protected Property Columns() As Integer
        Get
            If Not ViewState("Columns") Is Nothing Then
                Return CInt(Fix(ViewState("Columns")))
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("Columns") = value
        End Set
    End Property

    'Protected Overrides Sub LoadViewState(ByVal earlierState As Object)
    '    MyBase.LoadViewState(earlierState)
    '    If ViewState("dynamictable") Is Nothing Then
    '        AddCA(Rows)
    '    End If
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            'Set the Rows and Columns property with the value
            'entered by the user in the respective textboxes
            Me.Rows = 1
            Dim year As String = Request.QueryString("y")
            Dim month As String = Request.QueryString("m")
            Dim id As String = Request.QueryString("id")
            txtYear.Text = year
            txtMonth.Text = month
            txtKPI.Text = id
            idkpi.Value = id

            Dim sqlQuery As String = "select count(*) from pica where year='" & year & "' and month='" & month & "' and id='" & id & "'"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim cmSql As New SqlCommand(sqlQuery, conn)
            Dim result As String
            Dim kpi As String
            'conn.Open()
            'result = cmSql.ExecuteScalar()
            'sqlQuery = "select kpi from bscmaster where id='" & id & "'"
            'cmSql = New SqlCommand(sqlQuery, conn)
            'kpi = cmSql.ExecuteScalar()
            'conn.Close()
            'txtKPI.Text = kpi
            result = "0"
            If result = "0" Then 'Tidak ada data
                'Do Nothing
            Else 'Ada data
                sqlQuery = "select * from pica where year='" & year & "' and month='" & month & "' and id='" & id & "'"
                Dim reader As SqlDataReader
                cmSql = New SqlCommand(sqlQuery, conn)
                conn.Open()
                reader = cmSql.ExecuteReader()
                Dim i As Integer = 1
                If reader.HasRows Then

                    While reader.Read
                        If i = 1 Then
                            txtTglPICA1.Text = reader("tglpica")
                            txtPI1.Text = reader("pi")
                            txtCA1.Text = reader("ca")
                            txtTarget1.Text = reader("target")
                            txtPIC1.Text = reader("pic")
                            txtDueDate1.Text = reader("duedate")
                            ddlStatus1.SelectedValue = reader("status")
                        Else
                            If reader("baris") = reader("noref") Then 'berarti ini PI
                                btnAddPI_Click(Me, Nothing)
                                'AddPI(reader("tglpica"), reader("pi"), reader("ca"), reader("target"), reader("pic"), reader("duedate"), reader("status"))
                                CType(PICAtabel.Rows(i).Cells(0).Controls(1), TextBox).Text = reader("tglpica")
                                CType(PICAtabel.Rows(i).Cells(1).Controls(0), TextBox).Text = reader("pi")
                                CType(PICAtabel.Rows(i).Cells(2).Controls(0), TextBox).Text = reader("ca")
                                CType(PICAtabel.Rows(i).Cells(3).Controls(0), TextBox).Text = reader("target")
                                CType(PICAtabel.Rows(i).Cells(4).Controls(0), TextBox).Text = reader("pic")
                                CType(PICAtabel.Rows(i).Cells(5).Controls(0), TextBox).Text = reader("duedate")
                                CType(PICAtabel.Rows(i).Cells(6).Controls(0), DropDownList).SelectedValue = reader("status")
                            Else 'berarti ca
                                btnAddCA_Click(Me, Nothing)
                                'AddCA(reader("ca"), reader("target"), reader("pic"), reader("duedate"), reader("status"))
                                CType(PICAtabel.Rows(i).Cells(2).Controls(0), TextBox).Text = reader("ca")
                                CType(PICAtabel.Rows(i).Cells(3).Controls(0), TextBox).Text = reader("target")
                                CType(PICAtabel.Rows(i).Cells(4).Controls(0), TextBox).Text = reader("pic")
                                CType(PICAtabel.Rows(i).Cells(5).Controls(0), TextBox).Text = reader("duedate")
                                CType(PICAtabel.Rows(i).Cells(6).Controls(0), DropDownList).SelectedValue = reader("status")

                            End If
                        End If
                        i += 1

                    End While
                End If
                conn.Close()

            End If
        Else
            If hdnval.Value <> "" Then
                Dim i As Integer
                click = ViewState("click")
                For i = 1 To CInt(hdnval.Value)
                    If click(i - 1).ToString() = "C" Then
                        AddCA()
                    Else
                        AddPI()
                    End If
                Next
            End If
        End If
    End Sub

    Private Sub btnAddCA1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCA1.Click
        If hdnval.Value = "" Then
            hdnval.Value = 1
        Else
            hdnval.Value = hdnval.Value + 1
        End If
        AddCA()
        If ViewState("click") Is Nothing Then
            click.Add("C")
            ViewState("click") = click
        Else
            click = ViewState("click")
            click.Add("C")
            ViewState("click") = click
        End If

    End Sub

    Private Sub btnAddPI1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPI1.Click
        If hdnval.Value = "" Then
            hdnval.Value = 1
        Else
            hdnval.Value = hdnval.Value + 1
        End If
        AddPI()
        If ViewState("click") Is Nothing Then
            click.Add("P")
            ViewState("click") = click
        Else
            click = ViewState("click")
            click.Add("P")
            ViewState("click") = click
        End If
    End Sub

    Private Sub btnAddCA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim myButton As Button
        'myButton = CType(sender, Button)
        'Dim myTemp As String = myButton.ID
        If hdnval.Value = "" Then
            hdnval.Value = 1
        Else
            hdnval.Value = hdnval.Value + 1
        End If
        AddCA()
        If ViewState("click") Is Nothing Then
            click.Add("C")
            ViewState("click") = click
        Else
            click = ViewState("click")
            click.Add("C")
            ViewState("click") = click
        End If
    End Sub

    Private Sub btnAddPI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim myButton As Button
        'myButton = CType(sender, Button)
        'Dim myTemp As String = myButton.ID
        If hdnval.Value = "" Then
            hdnval.Value = 1
        Else
            hdnval.Value = hdnval.Value + 1
        End If
        AddPI()
        If ViewState("click") Is Nothing Then
            click.Add("P")
            ViewState("click") = click
        Else
            click = ViewState("click")
            click.Add("P")
            ViewState("click") = click
        End If
    End Sub

    Sub AddCA()
        Try
            Dim rowTable As New TableRow()
            Dim rowCell As New TableCell()

            Dim numOfRows As Integer = PICAtabel.Rows.Count

            Dim hiddenField As New HiddenField()
            hiddenField.ID = "idpica" & numOfRows
            rowCell = New TableCell()
            rowCell.Controls.Add(hiddenField)

            rowTable.Cells.Add(rowCell)
            rowTable.Cells.Add(New TableCell())

            Dim txtCA As New TextBox()
            txtCA.TextMode = TextBoxMode.MultiLine
            txtCA.ID = "txtCA" & numOfRows
            txtCA.Rows = 5
            txtCA.Columns = 30
            txtCA.CssClass = "newStyle1"
            rowCell = New TableCell()
            rowCell.Controls.Add(txtCA)
            Dim btnAddCA As New Button()
            btnAddCA.ID = "btnAddCA" & numOfRows.ToString()
            btnAddCA.CssClass = "newStyle1"
            btnAddCA.Text = "Add"
            AddHandler btnAddCA.Click, AddressOf btnAddCA_Click
            rowCell.Controls.Add(btnAddCA)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            rowCell = New TableCell()
            Dim txtTarget As New TextBox()
            txtTarget.ID = "txtTarget" & numOfRows
            txtTarget.CssClass = "newStyle1"
            rowCell.Controls.Add(txtTarget)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)


            rowCell = New TableCell()
            Dim txtPIC As New TextBox()
            txtPIC.ID = "txtPIC" & numOfRows
            txtPIC.CssClass = "newStyle1"
            txtPIC.Columns = 20
            rowCell.Controls.Add(txtPIC)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            rowCell = New TableCell()
            Dim txtDueDate As New TextBox()
            txtDueDate.ID = "txtDueDate" & numOfRows
            txtDueDate.CssClass = "newStyle1"
            txtDueDate.Columns = 14
            Dim ce As New AjaxControlToolkit.CalendarExtender()
            ce.ID = "ajaxCal" & numOfRows.ToString()
            ce.TargetControlID = txtDueDate.ClientID
            ce.Format = "MM/dd/yyyy"
            rowCell.Controls.Add(txtDueDate)
            rowCell.Controls.Add(ce)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            rowCell = New TableCell()
            Dim ddlStatus As New DropDownList()
            ddlStatus.ID = "ddlStatus" & numOfRows
            ddlStatus.CssClass = "newStyle1"
            ddlStatus.Items.Add(New ListItem("Open", "0"))
            ddlStatus.Items.Add(New ListItem("Close", "1"))
            rowCell.Controls.Add(ddlStatus)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            If numOfRows Mod 2 = 0 Then
                rowTable.CssClass = "styleAlternate"
            Else
                rowTable.CssClass = "styleNonAlternate"
            End If
            PICAtabel.Rows.Add(rowTable)

        Catch ex As Exception
            lbWarning.Text = ex.Message
        End Try
    End Sub

    Sub AddCA(ByVal ca As String, ByVal target As String, ByVal pic As String, ByVal duedate As String, ByVal status As String)
        Try
            Dim rowTable As New TableRow()
            Dim rowCell As New TableCell()

            Dim numOfRows As Integer = PICAtabel.Rows.Count

            Dim hiddenField As New HiddenField()
            hiddenField.ID = "idpica" & numOfRows
            rowCell = New TableCell()
            rowCell.Controls.Add(hiddenField)

            rowTable.Cells.Add(rowCell)
            rowTable.Cells.Add(New TableCell())

            Dim txtCA As New TextBox()
            txtCA.TextMode = TextBoxMode.MultiLine
            txtCA.ID = "txtCA" & numOfRows
            txtCA.Rows = 5
            txtCA.Columns = 30
            txtCA.CssClass = "newStyle1"
            txtCA.Text = ca
            rowCell = New TableCell()
            rowCell.Controls.Add(txtCA)
            Dim btnAddCA As New Button()
            btnAddCA.ID = "btnAddCA" & numOfRows.ToString()
            btnAddCA.CssClass = "newStyle1"
            btnAddCA.Text = "Add"
            AddHandler btnAddCA.Click, AddressOf btnAddCA_Click
            rowCell.Controls.Add(btnAddCA)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            rowCell = New TableCell()
            Dim txtTarget As New TextBox()
            txtTarget.ID = "txtTarget" & numOfRows
            txtTarget.CssClass = "newStyle1"
            txtTarget.Text = target
            rowCell.Controls.Add(txtTarget)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)


            rowCell = New TableCell()
            Dim txtPIC As New TextBox()
            txtPIC.ID = "txtPIC" & numOfRows
            txtPIC.CssClass = "newStyle1"
            txtPIC.Columns = 20
            txtPIC.Text = pic
            rowCell.Controls.Add(txtPIC)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            rowCell = New TableCell()
            Dim txtDueDate As New TextBox()
            txtDueDate.ID = "txtDueDate" & numOfRows
            txtDueDate.CssClass = "newStyle1"
            txtDueDate.Columns = 14
            txtDueDate.Text = duedate
            Dim ce As New AjaxControlToolkit.CalendarExtender()
            ce.ID = "ajaxCal" & numOfRows.ToString()
            ce.TargetControlID = txtDueDate.ClientID
            ce.Format = "MM/dd/yyyy"
            rowCell.Controls.Add(txtDueDate)
            rowCell.Controls.Add(ce)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            rowCell = New TableCell()
            Dim ddlStatus As New DropDownList()
            ddlStatus.ID = "ddlStatus" & numOfRows
            ddlStatus.CssClass = "newStyle1"
            ddlStatus.Items.Add(New ListItem("Open", "0"))
            ddlStatus.Items.Add(New ListItem("Close", "1"))
            ddlStatus.SelectedValue = status
            rowCell.Controls.Add(ddlStatus)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            If numOfRows Mod 2 = 0 Then
                rowTable.CssClass = "styleAlternate"
            Else
                rowTable.CssClass = "styleNonAlternate"
            End If
            PICAtabel.Rows.Add(rowTable)

        Catch ex As Exception
            lbWarning.Text = ex.Message
        End Try
    End Sub

    Sub AddPI()
        Try
            Dim rowTable As New TableRow()
            Dim rowCell As New TableCell()

            Dim numOfRows As Integer = PICAtabel.Rows.Count

            Dim txtTglPica As New TextBox()
            Dim ce1 As New AjaxControlToolkit.CalendarExtender()
            Dim hiddenField As New HiddenField()
            hiddenField.ID = "idpica" & numOfRows
            txtTglPica.ID = "txtTglPica" & numOfRows.ToString()
            txtTglPica.CssClass = "newStyle1"
            txtTglPica.Columns = 14
            ce1.ID = "ajaxCal1" & numOfRows.ToString()
            ce1.Format = "MM/dd/yyyy"
            ce1.TargetControlID = txtTglPica.ClientID
            rowCell.Controls.Add(hiddenField)
            rowCell.Controls.Add(txtTglPica)
            rowCell.Controls.Add(ce1)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            rowCell = New TableCell()
            Dim txtPI As New TextBox()
            txtPI.TextMode = TextBoxMode.MultiLine
            txtPI.ID = "txtPI" & numOfRows
            txtPI.Rows = 5
            txtPI.Columns = 30
            txtPI.CssClass = "newStyle1"
            rowCell = New TableCell()
            rowCell.Controls.Add(txtPI)
            Dim btnAddPI As New Button()
            btnAddPI.ID = "btnAddPI" & numOfRows.ToString()
            btnAddPI.CssClass = "newStyle1"
            btnAddPI.Text = "Add"
            AddHandler btnAddPI.Click, AddressOf btnAddPI_Click
            rowCell.Controls.Add(btnAddPI)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            rowCell = New TableCell()
            Dim txtCA As New TextBox()
            txtCA.TextMode = TextBoxMode.MultiLine
            txtCA.ID = "txtCA" & numOfRows
            txtCA.Rows = 5
            txtCA.Columns = 30
            txtCA.CssClass = "newStyle1"
            rowCell = New TableCell()
            rowCell.Controls.Add(txtCA)
            Dim btnAddCA As New Button()
            btnAddCA.ID = "btnAddCA" & numOfRows.ToString()
            btnAddCA.CssClass = "newStyle1"
            btnAddCA.Text = "Add"
            AddHandler btnAddCA.Click, AddressOf btnAddCA_Click
            rowCell.Controls.Add(btnAddCA)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            rowCell = New TableCell()
            Dim txtTarget As New TextBox()
            txtTarget.ID = "txtTarget" & numOfRows
            txtTarget.CssClass = "newStyle1"
            rowCell.Controls.Add(txtTarget)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)


            rowCell = New TableCell()
            Dim txtPIC As New TextBox()
            txtPIC.ID = "txtPIC" & numOfRows
            txtPIC.CssClass = "newStyle1"
            txtPIC.Columns = 20
            rowCell.Controls.Add(txtPIC)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            rowCell = New TableCell()
            Dim txtDueDate As New TextBox()
            txtDueDate.ID = "txtDueDate" & numOfRows
            txtDueDate.CssClass = "newStyle1"
            txtDueDate.Columns = 14
            Dim ce As New AjaxControlToolkit.CalendarExtender()
            ce.ID = "ajaxCal" & numOfRows.ToString()
            ce.Format = "MM/dd/yyyy"
            ce.TargetControlID = txtDueDate.ClientID
            rowCell.Controls.Add(txtDueDate)
            rowCell.Controls.Add(ce)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            rowCell = New TableCell()
            Dim ddlStatus As New DropDownList()
            ddlStatus.ID = "ddlStatus" & numOfRows
            ddlStatus.CssClass = "newStyle1"
            ddlStatus.Items.Add(New ListItem("Open", "0"))
            ddlStatus.Items.Add(New ListItem("Close", "1"))
            rowCell.Controls.Add(ddlStatus)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)


            If numOfRows Mod 2 = 0 Then
                rowTable.CssClass = "styleAlternate"
            Else
                rowTable.CssClass = "styleNonAlternate"
            End If

            PICAtabel.Rows.Add(rowTable)
        Catch ex As Exception
            lbWarning.Text = ex.Message
        End Try
    End Sub

    Sub AddPI(ByVal tglpica As String, ByVal pi As String, ByVal ca As String, ByVal target As String, ByVal pic As String, ByVal duedate As String, ByVal status As String)
        Try
            Dim rowTable As New TableRow()
            Dim rowCell As New TableCell()

            Dim numOfRows As Integer = PICAtabel.Rows.Count

            Dim txtTglPica As New TextBox()
            Dim ce1 As New AjaxControlToolkit.CalendarExtender()
            Dim hiddenField As New HiddenField()
            hiddenField.ID = "idpica" & numOfRows
            txtTglPica.ID = "txtTglPica" & numOfRows.ToString()
            txtTglPica.CssClass = "newStyle1"
            txtTglPica.Columns = 14
            txtTglPica.Text = tglpica
            ce1.ID = "ajaxCal1" & numOfRows.ToString()
            ce1.Format = "MM/dd/yyyy"
            ce1.TargetControlID = txtTglPica.ClientID
            rowCell.Controls.Add(hiddenField)
            rowCell.Controls.Add(txtTglPica)
            rowCell.Controls.Add(ce1)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            rowCell = New TableCell()
            Dim txtPI As New TextBox()
            txtPI.TextMode = TextBoxMode.MultiLine
            txtPI.ID = "txtPI" & numOfRows
            txtPI.Rows = 5
            txtPI.Columns = 30
            txtPI.CssClass = "newStyle1"
            txtPI.Text = pi
            rowCell = New TableCell()
            rowCell.Controls.Add(txtPI)
            Dim btnAddPI As New Button()
            btnAddPI.ID = "btnAddPI" & numOfRows.ToString()
            btnAddPI.CssClass = "newStyle1"
            btnAddPI.Text = "Add"
            AddHandler btnAddPI.Click, AddressOf btnAddPI_Click
            rowCell.Controls.Add(btnAddPI)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            rowCell = New TableCell()
            Dim txtCA As New TextBox()
            txtCA.TextMode = TextBoxMode.MultiLine
            txtCA.ID = "txtCA" & numOfRows
            txtCA.Rows = 5
            txtCA.Columns = 30
            txtCA.CssClass = "newStyle1"
            txtCA.Text = ca
            rowCell = New TableCell()
            rowCell.Controls.Add(txtCA)
            Dim btnAddCA As New Button()
            btnAddCA.ID = "btnAddCA" & numOfRows.ToString()
            btnAddCA.CssClass = "newStyle1"
            btnAddCA.Text = "Add"
            AddHandler btnAddCA.Click, AddressOf btnAddCA_Click
            rowCell.Controls.Add(btnAddCA)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            rowCell = New TableCell()
            Dim txtTarget As New TextBox()
            txtTarget.ID = "txtTarget" & numOfRows
            txtTarget.CssClass = "newStyle1"
            txtTarget.Text = target
            rowCell.Controls.Add(txtTarget)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)


            rowCell = New TableCell()
            Dim txtPIC As New TextBox()
            txtPIC.ID = "txtPIC" & numOfRows
            txtPIC.CssClass = "newStyle1"
            txtPIC.Columns = 20
            txtPIC.Text = pic
            rowCell.Controls.Add(txtPIC)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            rowCell = New TableCell()
            Dim txtDueDate As New TextBox()
            txtDueDate.ID = "txtDueDate" & numOfRows
            txtDueDate.CssClass = "newStyle1"
            txtDueDate.Columns = 14
            txtDueDate.Text = duedate
            Dim ce As New AjaxControlToolkit.CalendarExtender()
            ce.ID = "ajaxCal" & numOfRows.ToString()
            ce.Format = "MM/dd/yyyy"
            ce.TargetControlID = txtDueDate.ClientID
            rowCell.Controls.Add(txtDueDate)
            rowCell.Controls.Add(ce)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)

            rowCell = New TableCell()
            Dim ddlStatus As New DropDownList()
            ddlStatus.ID = "ddlStatus" & numOfRows
            ddlStatus.CssClass = "newStyle1"
            ddlStatus.Items.Add(New ListItem("Open", "0"))
            ddlStatus.Items.Add(New ListItem("Close", "1"))
            ddlStatus.SelectedValue = status
            rowCell.Controls.Add(ddlStatus)
            rowCell.VerticalAlign = VerticalAlign.Top
            rowTable.Cells.Add(rowCell)


            If numOfRows Mod 2 = 0 Then
                rowTable.CssClass = "styleAlternate"
            Else
                rowTable.CssClass = "styleNonAlternate"
            End If

            PICAtabel.Rows.Add(rowTable)
        Catch ex As Exception
            lbWarning.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            Dim dt As New DataTable()
            dt.Columns.Add("state", GetType(String))
            dt.Columns.Add("baris", GetType(Integer))
            dt.Columns.Add("TglPica", GetType(Date))
            dt.Columns.Add("PI", GetType(String))
            dt.Columns.Add("CA", GetType(String))
            dt.Columns.Add("Target", GetType(String))
            dt.Columns.Add("PIC", GetType(String))
            dt.Columns.Add("duedate", GetType(Date))
            dt.Columns.Add("status", GetType(String))
            dt.Columns.Add("noref", GetType(String))
            Dim dr As DataRow
            Dim noref As Integer = 0
            For i As Integer = 1 To PICAtabel.Rows.Count - 1
                dr = dt.NewRow()
                dr("state") = IIf(CType(PICAtabel.Rows(i).Cells(0).Controls(0), HiddenField).Value = "", "NEW", CType(PICAtabel.Rows(i).Cells(0).Controls(0), HiddenField).Value)
                dr("baris") = i
                If PICAtabel.Rows(i).Cells(0).Controls.Count = 3 Then
                    dr("tglpica") = Date.ParseExact(CType(PICAtabel.Rows(i).Cells(0).Controls(1), TextBox).Text, "MM/dd/yyyy", Nothing)
                End If
                If PICAtabel.Rows(i).Cells(1).Controls.Count = 2 Then
                    dr("PI") = CType(PICAtabel.Rows(i).Cells(1).Controls(0), TextBox).Text
                End If
                dr("CA") = CType(PICAtabel.Rows(i).Cells(2).Controls(0), TextBox).Text
                dr("Target") = CType(PICAtabel.Rows(i).Cells(3).Controls(0), TextBox).Text
                dr("PIC") = CType(PICAtabel.Rows(i).Cells(4).Controls(0), TextBox).Text
                dr("duedate") = Date.ParseExact(CType(PICAtabel.Rows(i).Cells(5).Controls(0), TextBox).Text, "MM/dd/yyyy", Nothing)
                dr("status") = CType(PICAtabel.Rows(i).Cells(6).Controls(0), DropDownList).SelectedValue
                If noref = 0 Then
                    noref = 1
                ElseIf IsDBNull(dr("PI")) Then
                    lbWarning.Text = ""
                Else
                    noref = dr("baris")
                End If
                dr("noref") = noref
                dt.Rows.Add(dr)
            Next
            If dt.Rows.Count > 0 Then
                Dim sqlQuery As String = ""
                Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
                Dim cmSql As New SqlCommand()
                Dim result As String
                For Each dr1 As DataRow In dt.Rows
                    sqlQuery += "insert into pica values ('" & txtYear.Text & "','" & txtMonth.Text & "','" & idkpi.Value & "'," & _
                                dr1("baris") & "," & IIf(dr1("tglpica") Is Nothing, "NULL", "'" & dr1("tglpica") & "'") & ",'" & _
                                dr1("PI") & "','" & dr1("CA") & "','" & dr1("Target") & "','" & dr1("PIC") & "','" & dr1("duedate") & "','" & _
                                dr1("status") & "'," & dr1("noref") & ");"
                Next
                sqlQuery = "BEGIN TRANSACTION; BEGIN TRY " & sqlQuery
                sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end"
                lbWarning.Text = sqlQuery
                cmSql = New SqlCommand(sqlQuery, conn)
                conn.Open()
                result = cmSql.ExecuteScalar()
                conn.Close()
                If result = 0 Then
                    lbWarning.Text = "Simpan Berhasil"
                Else
                    lbWarning.Text = result
                End If
            Else
                lbWarning.Text = "Tidak ada data yang disimpan"
            End If

            '---FOR TESTING PURPOSE ONLY--'
            'GridView1.Visible = True
            'GridView1.DataSource = dt
            'GridView1.DataBind()
        Catch ex As Exception
            lbWarning.Text = ex.Message
        End Try
    End Sub
End Class


'Dim txtTglPica As New TextBox()
'Dim ce As New AjaxControlToolkit.CalendarExtender()
'txtTglPica.ID = "txtTglPica" & rowcount.ToString()
'txtTglPica.CssClass = "newStyle1"
'ce.ID = "ajaxCal" & rowcount.ToString()
'ce.Format = "dd/MM/yyyy"
'ce.TargetControlID = txtTglPica.ClientID
'rowCell.Controls.Add(txtTglPica)
'rowCell.Controls.Add(ce)
'rowTable.Cells.Add(rowCell)