﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="activitydetail.aspx.vb" Inherits="EXCELLENT.activitydetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Activity Plan Detail</title>
    <meta http-equiv="Expires" content="0"/>
    <meta http-equiv="Cache-Control" content="no-cache"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/json2.js" type="text/javascript"></script>

   <script type="text/javascript">

      var TABLE_NAME = 'activity';
      function addRow(mynomor, myname, mystate, mypicnik, mypicname, mytstate) {
         var tbl = document.getElementById(TABLE_NAME);
         var nextRow = tbl.tBodies[0].rows.length;
         var num = nextRow;
         var row = tbl.tBodies[0].insertRow(num);
         row.setAttribute('style', 'vertical-align: top');
         var nox = 1;
         if (nox != tbl.tBodies[0].rows.length) {
            for (var i = 1; i < tbl.tBodies[0].rows.length; i++) {
               //nox = i;
               if (tbl.rows[i].cells[1].getElementsByTagName('input')[0].value == 'PROGRAM' && tbl.rows[i].style.display != 'none') {
                  nox += 1;
               }
            }
         }
         row.setAttribute('id', 'tr' + (num + 1));
         var cell0 = row.insertCell(0);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'hstate' + (num + 1));
         txtInp.setAttribute('id', 'hstate' + (num + 1));
         txtInp.setAttribute('value', 'NEW');
         if (mystate != undefined) {
            txtInp.setAttribute('value', mystate);
         }
         cell0.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'hnox' + (num + 1));
         txtInp.setAttribute('id', 'hnox' + (num + 1));
         txtInp.setAttribute('size', '1');
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('value', num + 1);
         cell0.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'nox' + (num + 1));
         txtInp.setAttribute('id', 'nox' + (num + 1));
         txtInp.setAttribute('size', '1');
         txtInp.setAttribute('READONLY', 'READONLY');
         txtInp.setAttribute('style', 'border:0');
         txtInp.setAttribute('value', nox);
         if (mynomor != undefined) {
            txtInp.setAttribute('value', mynomor);
         }
         cell0.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'tstate' + (num + 1));
         txtInp.setAttribute('id', 'tstate' + (num + 1));
         txtInp.setAttribute('value', 'NEW');
         if (mytstate != undefined) {
            txtInp.setAttribute('value', mytstate);
         }
         cell0.appendChild(txtInp);

         var cell1 = row.insertCell(1);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'jenis' + (num + 1));
         txtInp.setAttribute('id', 'jenis' + (num + 1));
         txtInp.setAttribute('size', '10');
         txtInp.setAttribute('value', 'PROGRAM');
         txtInp.setAttribute('style', 'border:0');
         txtInp.setAttribute('READONLY', 'READONLY')
         cell1.appendChild(txtInp);

         var cell2 = row.insertCell(2);
         var txtInp = document.createElement('textarea');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'activity' + (num + 1));
         txtInp.setAttribute('id', 'activity' + (num + 1));
         txtInp.setAttribute('rows', '3');
         txtInp.setAttribute('cols', '20');
         txtInp.setAttribute('style', 'border:0');
         txtInp.setAttribute('READONLY', 'READONLY')
         if (myname != undefined) {
            var txt = document.createTextNode(myname);
            txtInp.appendChild(txt);
         }
         cell2.appendChild(txtInp);

         var cell3 = row.insertCell(3);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'nikpic' + (num + 1));
         txtInp.setAttribute('id', 'nikpic' + (num + 1));
         if (mypicnik != undefined) {
            txtInp.setAttribute('value', mypicnik);
         }
         cell3.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'pic' + (num + 1));
         txtInp.setAttribute('id', 'pic' + (num + 1));
         txtInp.setAttribute('READONLY', 'READONLY')
         txtInp.setAttribute('size', '15')
         txtInp.setAttribute('style', 'border:0');
         txtInp.setAttribute('READONLY', 'READONLY')
         if (mypicname != undefined) {
            txtInp.setAttribute('value', mypicname);
         }
         cell3.appendChild(txtInp);

         var cell4 = row.insertCell(4);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'd' + (num + 1));
         txtInp.setAttribute('id', 'd' + (num + 1));
         txtInp.setAttribute('style', 'visibility:hidden');
         cell4.appendChild(txtInp);

         var cell5 = row.insertCell(5);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'start' + (num + 1));
         txtInp.setAttribute('id', 'start' + (num + 1));
         txtInp.setAttribute('size', '4');
         txtInp.setAttribute('READONLY', 'READONLY');
         txtInp.setAttribute('style', 'visibility:hidden');
         cell5.appendChild(txtInp);


         var cell6 = row.insertCell(6);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'end' + (num + 1));
         txtInp.setAttribute('id', 'end' + (num + 1));
         txtInp.setAttribute('size', '5');
         txtInp.setAttribute('READONLY', 'READONLY');
         txtInp.setAttribute('style', 'visibility:hidden');
         cell6.appendChild(txtInp);


         var cell7 = row.insertCell(7);

         var cell8 = row.insertCell(8);

         var cell9 = row.insertCell(9);

         var cell10 = row.insertCell(10);

         reorderRows(tbl);
      }

      function addactivity(id, startposition, mynomor, mypicnik, mypicname, myname, mystate, mydeliverable, myprog, myacc, mypi, myca, mytstate) {
             var rowposition = document.getElementById('h' + id).value;
             var tbl = document.getElementById(TABLE_NAME);
             var rowToInsertAt = tbl.tBodies[0].rows.length;
             var nomor = document.getElementById(id).value;
             var visiblerow = 0;
             startposition = rowposition;
             var noactivity = 0;
             for (var i = startposition; i <= tbl.tBodies[0].rows.length; i++) {
                rowToInsertAt = i;
                if (tbl.rows[i].cells[1].getElementsByTagName('input')[0].value == 'PROGRAM' && (i != startposition)) {
                   if (rowposition == tbl.tBodies[0].rows.length) {
                      rowToInsertAt = rowToInsertAt;
                   } else {
                      rowToInsertAt = rowToInsertAt - 1;
                   }
                   break;
                }
                if (tbl.rows[i].cells[1].getElementsByTagName('input')[0].value == 'ACTIVITY') {
                   noactivity = noactivity + 1;
                }
             }

             var tbl = document.getElementById(TABLE_NAME);
             var nextRow = tbl.tBodies[0].rows.length;
             //var num = nextRow;
             var num = rowToInsertAt;
             var row = tbl.tBodies[0].insertRow(num);
             row.setAttribute('style', 'vertical-align: top');
             var cell0 = row.insertCell(0);
             var txtInp = document.createElement('input');
             txtInp.setAttribute('type', 'hidden');
             txtInp.setAttribute('name', 'hstate' + nomor + "#" + noactivity);
             txtInp.setAttribute('id', 'hstate' + nomor + "#" + noactivity);
             txtInp.setAttribute('value', 'NEW');
             if (mystate != undefined) {
                txtInp.setAttribute('value', mystate);
             }
             cell0.appendChild(txtInp);
             var txtInp = document.createElement('input');
             txtInp.setAttribute('type', 'hidden');
             txtInp.setAttribute('name', 'hnox' + nomor + "#" + noactivity);
             txtInp.setAttribute('id', 'hnox' + nomor + "#" + noactivity);
             txtInp.setAttribute('disabled', 'disabled');
             txtInp.setAttribute('value', num + 1);
             cell0.appendChild(txtInp);
             var txtInp = document.createElement('input');
             txtInp.setAttribute('type', 'text');
             txtInp.setAttribute('name', 'nox' + nomor + "#" + noactivity);
             txtInp.setAttribute('id', 'nox' + nomor + "#" + noactivity);
             txtInp.setAttribute('size', '1');
             txtInp.setAttribute('style', 'border:0');
             txtInp.setAttribute('READONLY', 'READONLY')
             txtInp.setAttribute('value', nomor + "." + noactivity);
             if (mynomor != undefined) {
                txtInp.setAttribute('value', mynomor);
             }
             cell0.appendChild(txtInp);
             var txtInp = document.createElement('input');
             txtInp.setAttribute('type', 'hidden');
             txtInp.setAttribute('name', 'tstate' + (num + 1));
             txtInp.setAttribute('id', 'tstate' + (num + 1));
             txtInp.setAttribute('value', 'NEW');
             if (mytstate != undefined) {
                txtInp.setAttribute('value', mytstate);
             }
             cell0.appendChild(txtInp);
             
             var cell1 = row.insertCell(1);
             var txtInp = document.createElement('input');
             txtInp.setAttribute('type', 'text');
             txtInp.setAttribute('name', 'jenis' + nomor + "#" + noactivity);
             txtInp.setAttribute('id', 'jenis' + nomor + "#" + noactivity);
             txtInp.setAttribute('size', '10');
             txtInp.setAttribute('value', 'ACTIVITY');
             txtInp.setAttribute('style', 'border:0');
             txtInp.setAttribute('READONLY', 'READONLY')
             cell1.appendChild(txtInp);

             var cell2 = row.insertCell(2);
             var txtInp = document.createElement('textarea');
             txtInp.setAttribute('type', 'text');
             txtInp.setAttribute('name', 'activity' + nomor + "#" + noactivity);
             txtInp.setAttribute('id', 'activity' + nomor + "#" + noactivity);
             txtInp.setAttribute('rows', '3');
             txtInp.setAttribute('cols', '20');
             txtInp.setAttribute('style', 'border:0');
             txtInp.setAttribute('READONLY', 'READONLY')
             if (myname != undefined) {
                var txt = document.createTextNode(myname);
                txtInp.appendChild(txt);
             }
             cell2.appendChild(txtInp);

             var cell3 = row.insertCell(3);
             var txtInp = document.createElement('input');
             txtInp.setAttribute('type', 'hidden');
             txtInp.setAttribute('name', 'nikpic' + nomor + "#" + noactivity);
             txtInp.setAttribute('id', 'nikpic' + nomor + "#" + noactivity);
             if (mypicnik != undefined) {
                txtInp.setAttribute('value', mypicnik);
             }
             cell3.appendChild(txtInp);
             var txtInp = document.createElement('input');
             txtInp.setAttribute('type', 'text');
             txtInp.setAttribute('name', 'pic' + nomor + "#" + noactivity);
             txtInp.setAttribute('id', 'pic' + nomor + "#" + noactivity);
             txtInp.setAttribute('READONLY', 'READONLY')
             txtInp.setAttribute('size', '15')
             txtInp.setAttribute('style', 'border:0');
             if (mypicname != undefined) {
                txtInp.setAttribute('value', mypicname);
             }
             cell3.appendChild(txtInp);

             var cell4 = row.insertCell(4);
             var txtInp = document.createElement('input');
             txtInp.setAttribute('type', 'text');
             txtInp.setAttribute('name', 'd' + nomor + "#" + noactivity);
             txtInp.setAttribute('id', 'd' + nomor + "#" + noactivity);
             txtInp.setAttribute('style', 'visibility:hidden');
             cell4.appendChild(txtInp);

             var cell5 = row.insertCell(5);
             var txtInp = document.createElement('input');
             txtInp.setAttribute('type', 'text');
             txtInp.setAttribute('name', 'start' + nomor + "#" + noactivity);
             txtInp.setAttribute('id', 'start' + nomor + "#" + noactivity);
             txtInp.setAttribute('size', '4');
             txtInp.setAttribute('READONLY', 'READONLY');
             txtInp.setAttribute('style', 'visibility:hidden');
             cell5.appendChild(txtInp);
             //          cell5.innerHTML = cell5.innerHTML + '<img title="Open Calendar" class="tcalIcon" onclick="A_TCALS[\'myCalstartID' +nomor + "#" + noactivity + '\'].f_toggle()" id="tcalico_myCalstartID' + nomor + "#" + noactivity + '" src="images/cal.gif"/>';
             //          new tcal({
             //             'formname': 'form1',
             //             'controlname': 'start' + nomor + "#" + noactivity,
             //             'id': 'myCalstartID' + nomor + "#" + noactivity
             //          });

             var cell6 = row.insertCell(6);
             var txtInp = document.createElement('input');
             txtInp.setAttribute('type', 'text');
             txtInp.setAttribute('name', 'end' + nomor + "#" + noactivity);
             txtInp.setAttribute('id', 'end' + nomor + "#" + noactivity);
             txtInp.setAttribute('size', '5');
             txtInp.setAttribute('READONLY', 'READONLY');
             txtInp.setAttribute('style', 'visibility:hidden');
             cell6.appendChild(txtInp);
             //          cell6.innerHTML = cell6.innerHTML + '<img title="Open Calendar" class="tcalIcon" onclick="A_TCALS[\'myCalendID' + nomor + "#" + noactivity + '\'].f_toggle()" id="tcalico_myCalendID' + nomor + "#" + noactivity + '" src="images/cal.gif"/>';
             //          new tcal({
             //             'formname': 'form1',
             //             'controlname': 'end' + nomor + "#" + noactivity,
             //             'id': 'myCalendID' + nomor + "#" + noactivity
             //          });

             var cell7 = row.insertCell(7);
             var txtInp = document.createElement('input');
             txtInp.setAttribute('type', 'text');
             txtInp.setAttribute('name', 'prog' + nomor + "#" + noactivity);
             txtInp.setAttribute('id', 'prog' + nomor + "#" + noactivity);
             txtInp.setAttribute('size', '5');
             txtInp.setAttribute('READONLY', 'READONLY');
             txtInp.setAttribute('style', 'border:0');
             if (myprog != undefined) {
                txtInp.setAttribute('value', myprog);
             }
             cell7.appendChild(txtInp);

             var cell8 = row.insertCell(8);
             var selector = document.createElement('select');
             var theOption = document.createElement("OPTION");
             theOption.value = "0";
             theOption.innerHTML = "On Schedule";
             selector.appendChild(theOption);
             var theOption = document.createElement("OPTION");
             theOption.value = "1";
             theOption.innerHTML = "Delayed";
             selector.appendChild(theOption);
             var theOption = document.createElement("OPTION");
             theOption.value = "2";
             theOption.innerHTML = "Advance";
             selector.appendChild(theOption);
             selector.disabled = "true";
             if (myacc != undefined) {
                if (myacc != '') {
                   selector.value = myacc;
                }
             }
             cell8.appendChild(selector);

             var cell9 = row.insertCell(9);
             var txtInp = document.createElement('textarea');
             txtInp.setAttribute('type', 'text');
             txtInp.setAttribute('name', 'pi' + nomor + "#" + noactivity);
             txtInp.setAttribute('id', 'pi' + nomor + "#" + noactivity);
             txtInp.setAttribute('rows', '3');
             txtInp.setAttribute('cols', '20');
             txtInp.setAttribute('READONLY', 'READONLY');
             txtInp.setAttribute('style', 'border:0');
             if (mypi != undefined) {
                var txt = document.createTextNode(mypi);
                txtInp.appendChild(txt);
             }
             cell9.appendChild(txtInp);

             var cell10 = row.insertCell(10);
             var txtInp = document.createElement('textarea');
             txtInp.setAttribute('type', 'text');
             txtInp.setAttribute('name', 'ca' + nomor + "#" + noactivity);
             txtInp.setAttribute('id', 'ca' + nomor + "#" + noactivity);
             txtInp.setAttribute('rows', '3');
             txtInp.setAttribute('cols', '20');
             txtInp.setAttribute('READONLY', 'READONLY');
             txtInp.setAttribute('style', 'border:0');
             if (myca != undefined) {
                var txt = document.createTextNode(myca);
                txtInp.appendChild(txt);
             }
             cell10.appendChild(txtInp);
             
             reorderRows(tbl);
          }

      function addtask(id, startposition, mynomor, myname, mystate, mypicnik, mypicname, mystart, myend, mydeliverable, myprog, myacc, mypi, myca, mytstate) {
         var rowposition = document.getElementById('h' + id).value;
         var tbl = document.getElementById(TABLE_NAME);
         var rowToInsertAt = tbl.tBodies[0].rows.length;
         var nomor = document.getElementById(id).value;
         startposition = rowposition;
         var noactivity = 0;
         for (var i = startposition; i <= tbl.tBodies[0].rows.length; i++) {
            rowToInsertAt = i;
            if ((tbl.rows[i].cells[1].getElementsByTagName('input')[0].value == 'ACTIVITY' || tbl.rows[i].cells[1].getElementsByTagName('input')[0].value == 'PROGRAM') && (i != startposition) && tbl.rows[i].style.display != 'none') {
               if (rowposition == tbl.tBodies[0].rows.length) {
                  rowToInsertAt = rowToInsertAt;
               } else {
                  rowToInsertAt = rowToInsertAt - 1;
               }
               break;
            }
            if (tbl.rows[i].cells[1].getElementsByTagName('input')[0].value == 'TASK' && tbl.rows[i].style.display != 'none') {
               noactivity = noactivity + 1;
            }
         }

         var tbl = document.getElementById(TABLE_NAME);
         var nextRow = tbl.tBodies[0].rows.length;
         //var num = nextRow;
         var num = rowToInsertAt;
         var row = tbl.tBodies[0].insertRow(num);
         row.setAttribute('style', 'vertical-align: top');
         var cell0 = row.insertCell(0);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'hstate' + nomor + "#" + noactivity);
         txtInp.setAttribute('id', 'hstate' + nomor + "#" + noactivity);
         txtInp.setAttribute('value', 'NEW');
         if (mystate != undefined) {
            txtInp.setAttribute('value', mystate);
         }
         cell0.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'hnox' + nomor + "#" + noactivity);
         txtInp.setAttribute('id', 'hnox' + nomor + "#" + noactivity);
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('value', num + 1);
         cell0.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'nox' + nomor + "#" + noactivity);
         txtInp.setAttribute('id', 'nox' + nomor + "#" + noactivity);
         txtInp.setAttribute('size', '1');
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('value', nomor + "." + noactivity);
         txtInp.setAttribute('style', 'border:0');
         txtInp.setAttribute('READONLY', 'READONLY')
         if (mynomor != undefined) {
            txtInp.setAttribute('value', mynomor);
         }
         cell0.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'tstate' + (num + 1));
         txtInp.setAttribute('id', 'tstate' + (num + 1));
         txtInp.setAttribute('value', 'NEW');
         if (mytstate != undefined) {
            txtInp.setAttribute('value', mytstate);
         }
         cell0.appendChild(txtInp);

         var cell1 = row.insertCell(1);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'jenis' + nomor + "#" + noactivity);
         txtInp.setAttribute('id', 'jenis' + nomor + "#" + noactivity);
         txtInp.setAttribute('size', '10');
         txtInp.setAttribute('value', 'TASK');
         txtInp.setAttribute('style', 'border:0');
         txtInp.setAttribute('READONLY', 'READONLY')
         cell1.appendChild(txtInp);

         var cell2 = row.insertCell(2);
         var txtInp = document.createElement('textarea');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'activity' + nomor + "#" + noactivity);
         txtInp.setAttribute('id', 'activity' + nomor + "#" + noactivity);
         txtInp.setAttribute('size', '30');
         txtInp.setAttribute('rows', '3');
         txtInp.setAttribute('cols', '20');
         txtInp.setAttribute('style', 'border:0');
         txtInp.setAttribute('READONLY', 'READONLY')
         if (myname != undefined) {
            var txt = document.createTextNode(myname);
            txtInp.appendChild(txt);
         }
         cell2.appendChild(txtInp);

         var cell3 = row.insertCell(3);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'nikpic' + nomor + "#" + noactivity);
         txtInp.setAttribute('id', 'nikpic' + nomor + "#" + noactivity);
         if (mypicnik != undefined) {
            txtInp.setAttribute('value', mypicnik);
         }
         cell3.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'pic' + nomor + "#" + noactivity);
         txtInp.setAttribute('id', 'pic' + nomor + "#" + noactivity);
         txtInp.setAttribute('size', '15')
         txtInp.setAttribute('READONLY', 'READONLY')
         txtInp.setAttribute('style', 'border:0');
         if (mypicname != undefined) {
            txtInp.setAttribute('value', mypicname);
         }
         cell3.appendChild(txtInp);

         var cell4 = row.insertCell(4);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'd' + nomor + "#" + noactivity);
         txtInp.setAttribute('id', 'd' + nomor + "#" + noactivity);
         txtInp.setAttribute('style', 'border:0');
         txtInp.setAttribute('READONLY', 'READONLY')
         if (mydeliverable != undefined) {
            txtInp.setAttribute('value', mydeliverable);
         }
         cell4.appendChild(txtInp);

         var cell5 = row.insertCell(5);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'start' + nomor + "#" + noactivity);
         txtInp.setAttribute('id', 'start' + nomor + "#" + noactivity);
         txtInp.setAttribute('size', '6');
         txtInp.setAttribute('READONLY', 'READONLY');
         txtInp.setAttribute('style', 'border:0');
         if (mystart != undefined) {
            txtInp.setAttribute('value', mystart);
         }
         cell5.appendChild(txtInp);

         var cell6 = row.insertCell(6);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'end' + nomor + "#" + noactivity);
         txtInp.setAttribute('id', 'end' + nomor + "#" + noactivity);
         txtInp.setAttribute('size', '6');
         txtInp.setAttribute('READONLY', 'READONLY');
         txtInp.setAttribute('style', 'border:0');
         if (myend != undefined) {
            txtInp.setAttribute('value', myend);
         }
         cell6.appendChild(txtInp);

         var cell7 = row.insertCell(7);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'prog' + nomor + "#" + noactivity);
         txtInp.setAttribute('id', 'prog' + nomor + "#" + noactivity);
         txtInp.setAttribute('size', '5');
         txtInp.setAttribute('READONLY', 'READONLY');
         txtInp.setAttribute('style', 'border:0');
         if (myprog != undefined) {
            txtInp.setAttribute('value', myprog);
         }
         cell7.appendChild(txtInp);

         var cell8 = row.insertCell(8);
         var selector = document.createElement('select');
         var theOption = document.createElement("OPTION");
         theOption.value = "0";
         theOption.innerHTML = "On Schedule";
         selector.appendChild(theOption);
         var theOption = document.createElement("OPTION");
         theOption.value = "1";
         theOption.innerHTML = "Delayed";
         selector.appendChild(theOption);
         var theOption = document.createElement("OPTION");
         theOption.value = "2";
         theOption.innerHTML = "Advance";
         selector.appendChild(theOption);
         selector.setAttribute('disabled', 'disabled');
         selector.disabled = "true";
         if (myacc != undefined) {
            if (myacc != '') {
               selector.value = myacc;
            }
         }
         cell8.appendChild(selector);

         var cell9 = row.insertCell(9);
         var txtInp = document.createElement('textarea');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'pi' + nomor + "#" + noactivity);
         txtInp.setAttribute('id', 'pi' + nomor + "#" + noactivity);
         txtInp.setAttribute('rows', '3');
         txtInp.setAttribute('cols', '20');
         txtInp.setAttribute('READONLY', 'READONLY');
         txtInp.setAttribute('style', 'border:0');
         if (mypi != undefined) {
            var txt = document.createTextNode(mypi);
            txtInp.appendChild(txt);
         }
         cell9.appendChild(txtInp);

         var cell10 = row.insertCell(10);
         var txtInp = document.createElement('textarea');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'ca' + nomor + "#" + noactivity);
         txtInp.setAttribute('id', 'ca' + nomor + "#" + noactivity);
         txtInp.setAttribute('rows', '3');
         txtInp.setAttribute('cols', '20');
         txtInp.setAttribute('READONLY', 'READONLY');
         txtInp.setAttribute('style', 'border:0');
         if (myca != undefined) {
            var txt = document.createTextNode(myca);
            txtInp.appendChild(txt);
         }
         cell10.appendChild(txtInp);

         reorderRows(tbl);
      }

      function reorderRows(tbl) {
         if (tbl.tBodies[0].rows[0]) {
            var count = 1;
            for (var i = 0; i < tbl.tBodies[0].rows.length; i++) {
               if (tbl.tBodies[0].rows[i].style.display != 'none') {
                  tbl.tBodies[0].rows[i].cells[0].getElementsByTagName('input')[1].value = count;
                  count++;
               }
            }
         }
      }


      function emptyRow() {
         var tbl = document.getElementById(TABLE_NAME);
         for (var i = tbl.rows.length; i > 1; i--) {
            tbl.deleteRow(i - 1);
         }
      }

      function getDetail() {
         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/RetTActivityPerProgram",
            data: "{'kpiid':" + getQuerystring('id') + ",'year':" + getQuerystring('year') + ",'week':" + getQuerystring('week') + ",'nomor':" + getQuerystring('nomor') + "}",
            dataType: "json",
            success: function(res) {
               MyAct = new Array();
               MyAct = res.d;
               emptyRow();
               var noprogram = 0;
               for (var i = 0; i < MyAct.length; i++) {
                  switch (MyAct[i].tipe) {
                     case 'PROGRAM':
                        addRow(MyAct[i].nomor, MyAct[i].activity, MyAct[i].state, MyAct[i].pic, MyAct[i].namapic);
                        noprogram = i + 1;
                        break;
                     case 'ACTIVITY':
                        var nomorku = MyAct[i].nomor;
                        addactivity('nox' + (noprogram), noprogram, MyAct[i].nomor, MyAct[i].pic, MyAct[i].namapic, MyAct[i].activity, MyAct[i].state, MyAct[i].deliverable, MyAct[i].progress, MyAct[i].accuracy, MyAct[i].pi, MyAct[i].ca, MyAct[i].tstate);
                        break;
                     case 'TASK':
                        addtask('nox' + nomorku.replace('.', '#'), nomorku.replace('.', '#'), MyAct[i].nomor, MyAct[i].activity, MyAct[i].state, MyAct[i].pic, MyAct[i].namapic, MyAct[i].start, MyAct[i].enddate, MyAct[i].deliverable, MyAct[i].progress, MyAct[i].accuracy, MyAct[i].pi, MyAct[i].ca, MyAct[i].tstate);
                        break;
                  }
               }
            },
            error: function(err) {
               alert("error: " + err.responseText);
            }
         });
      }

       function getQuerystring(key, default_)
         {
           if (default_==null) default_=""; 
           key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
           var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
           var qs = regex.exec(window.location.href);
           if(qs == null)
             return default_;
           else
             return qs[1];
       }

       getDetail();
   </script>    

    <style type="text/css">
        table.act {
	         border-width: 1px;
	         border-spacing: 0px;
	         border-color: #C0C0C0;
	         border-collapse: collapse;
	         background-color: white;
            }
         table.act thead {
	         border-width: 1px;
	         padding: 1px;
	         border-color: #C0C0C0;
         }
         table.act td {
	         border-width: 1px;
	         padding: 1px;
	         border-color: #C0C0C0;
         }
       </style>

</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table class="act" border="1" cellpadding="0" cellspacing="0" id="activity" width="800px">
      <thead>
         <tr align="center" style="font-size:14px;background-color:#449A50;color:white;font-weight:bold;">
            <td width="1%">No.</td>
            <td width="1%">Jenis</td>
            <td width="18%">Activity Plan</td>
            <td width="10%">PIC</td>
            <td width="10%">Deliverables</td>
            <td width="10%">Start</td>
            <td width="10%">End</td>
            <td width="10%">% Progress</td>
            <td width="10%">Ketepatan Waktu</td>
            <td width="10%">PI</td>
            <td width="10%">CA</td>
         </tr>
      </thead>
      <tbody></tbody>
    </table>
    </div>
    </form>
</body>
</html>
