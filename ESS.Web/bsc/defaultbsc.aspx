﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="defaultbsc.aspx.vb" Inherits="EXCELLENT._defaultbsc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>BSC</title>
    <link href="../css/site.css" rel="stylesheet" type="text/css" media="interactive, braille, emboss, handheld, projection, screen, tty, tv" />    
    <link href="../css/dropdown/dropdown.css" media="all" rel="stylesheet" type="text/css" />
    <link href="../css/dropdown/themes/default/default.ultimate.css" media="all" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <table width="100%">
         <tr>
         <td>
         <img src="../images/logo-new.png" alt="JResource" 
                 style="padding-left: 12px; padding-top: 12px;"/>
         </td>      
         <td>
           <div id="idnotif">        
           </div>
         </td>     
         <td>
         <img align="right" src="../images/Logo-memo-(20).jpg" alt="MEMO" width="187" 
                 height="65" style="padding-right: 12px; padding-top: 12px;"/>
         </td>
         </tr>
         </table> 
        <div>
          <div class="poster-inner"> 
          <div id="Div1" style="padding-left:30px;">
             <%                                                       
                If Session("Permission") Is Nothing Then
                ElseIf ("1").IndexOf(Session("Permission").ToString()) > -1 Then
                   Response.Write("<ul id='nav' class='dropdown dropdown-horizontal'>")
                   Response.Write("<li><a href='#' class='dir'>Input Data</a>")
                   Response.Write("<ul>")
                   Response.Write("<li><a href=""tperspective.aspx"" title=""Setup Perspective"" Target=""content"">Setup Perspective</a></li>")
                   Response.Write("<li><a href=""masterkpibuma.aspx"" title=""Master KPI BUMA"" Target=""content"">Master KPI BUMA</a></li>")
                   Response.Write("<li><a href=""transaction.aspx"" title=""BSC Transaction"" Target=""content"">BSC Transaction</a></li>")
                   Response.Write("<li><a href=""activityplan.aspx"" title=""Master Activity Plan"" Target=""content"">Master Activity Plan</a></li>")
                   Response.Write("<li><a href=""tactivityplan.aspx"" title=""Update Activity Plan"" Target=""content"">Update Activity Plan</a></li>")
                   Response.Write("</ul>")
                   Response.Write("</li>")
                   Response.Write("<li><a href=""../account.aspx"" title=""Account Management"" Target=""content"">Acc Mgmt</a></li>")
                   Response.Write("<li><a href='#' class='dir'>Report</a>")
                   Response.Write("<ul>")
                   Response.Write("<li><a href=""rtargets.aspx"" title=""View Target Summary"" Target=""content"">View Target Summary</a></li>")
                   Response.Write("<li><a href='performance.aspx' Target=""content"">Performance Report</a></li>")
                   Response.Write("<li><a href=""reportmaster.aspx"" title=""BSC View"" Target=""content"">BSC View</a></li>")
                   Response.Write("<li><a href=""stratmap.aspx"" title=""Strategy MAP JRN"" Target=""content"">Strategy MAP JRN</a></li>")
                   Response.Write("</ul>")
                   Response.Write("</li>")
                   Response.Write("</ul>")
                ElseIf ("2").IndexOf(Session("Permission").ToString()) > -1 Then
                   Response.Write("<ul id='nav' class='dropdown dropdown-horizontal'>")
                   Response.Write("<li><a href='#' class='dir'>Input Data</a>")
                   Response.Write("<ul>")
                   Response.Write("<li><a href=""masterkpibuma.aspx"" title=""MASTER KPI BUMA"" Target=""content"" class=""m2""><span style=""font-weight: bold"">Master KPI BUMA</span></a></li>")
                   Response.Write("<li><a href=""transaction.aspx"" title=""BSC Transaction"" Target=""content"" class=""m2""><span style=""font-weight: bold"">BSC Transaction</span></a></li>")
                   Response.Write("<li><a href=""activityplan.aspx"" title=""Master Activity Plan"" Target=""content"" class=""m2"">Master Activity Plan</a></li>")
                   Response.Write("<li><a href=""tactivityplan.aspx"" title=""Update Activity Plan"" Target=""content"" class=""m2"">Update Activity Plan</a></li>")
                   Response.Write("</ul>")
                   Response.Write("</li>")
                   Response.Write("<li><a href=""../account.aspx"" title=""Account Management"" Target=""content"">Acc Mgmt</a></li>")
                   Response.Write("<li><a href='#' class='dir'>Report</a>")
                   Response.Write("<ul>")
                   Response.Write("<li><a href='performance.aspx' Target=""content"">Performance Report</a></li>")
                   Response.Write("<li><a href=""reportmaster.aspx"" title=""BSC View"" Target=""content"">BSC View</a></li>")
                   Response.Write("<li><a href=""stratmap.aspx"" title=""Strategy MAP JRN"" Target=""content"">Strategy MAP JRN</a></li>")
                   Response.Write("</ul>")
                   Response.Write("</li>")
                   Response.Write("</ul>")
                ElseIf ("7").IndexOf(Session("Permission").ToString()) > -1 Then
                   Response.Write("<ul id='nav' class='dropdown dropdown-horizontal'>")
                   Response.Write("<li><a href='#' class='dir'>Input Data</a>")
                   Response.Write("<ul>")
                   Response.Write("<li><a href=""masterkpibuma.aspx"" title=""Master KPI BUMA"" Target=""content"" class=""m2"">Master KPI BUMA</a></li>")
                   Response.Write("<li><a href=""transaction.aspx"" title=""BSC Transaction"" Target=""content"" class=""m2"">BSC Transaction</a></li>")
                   Response.Write("<li><a href=""activityplan.aspx"" title=""Master Activity Plan"" Target=""content"" class=""m2"">Master Activity Plan</a></li>")
                   Response.Write("<li><a href=""tactivityplan.aspx"" title=""Update Activity Plan"" Target=""content"" class=""m2"">Update Activity Plan</a></li>")
                   Response.Write("</ul>")
                   Response.Write("</li>")
                   Response.Write("<li><a href=""../account.aspx"" title=""Account Management"" Target=""content"">Acc Mgmt</a></li>")
                   Response.Write("<li><a href='#' class='dir'>Report</a>")
                   Response.Write("<ul>")
                   Response.Write("<li><a href=""reportmaster.aspx"" title=""BSC View"" Target=""content"">BSC View</a></li>")
                   Response.Write("<li><a href=""stratmap.aspx"" title=""Strategy MAP JRN"" Target=""content"">Strategy MAP JRN</a></li>")
                   Response.Write("</ul>")
                   Response.Write("</li>")
                   Response.Write("</ul>")
                ElseIf ("8").IndexOf(Session("Permission").ToString()) > -1 Then
                   Response.Write("<ul id='nav' class='dropdown dropdown-horizontal'>")
                   Response.Write("<li><a href='#' class='dir'>Input Data</a>")
                   Response.Write("<ul>")
                   Response.Write("<li><a href=""tperspective.aspx"" title=""Setup Perspective"" Target=""content"" class=""m2"">Setup Perspective</a></li>")
                   Response.Write("<li><a href=""masterkpibuma.aspx"" title=""Master KPI BUMA"" Target=""content"" class=""m2"">Master KPI BUMA</a></li>")
                   Response.Write("<li><a href=""transaction.aspx"" title=""BSC Transaction"" Target=""content"" class=""m2"">BSC Transaction</a></li>")
                   Response.Write("<li><a href=""activityplan.aspx"" title=""Master Activity Plan"" Target=""content"" class=""m2"">Master Activity Plan</a></li>")
                   Response.Write("<li><a href=""tactivityplan.aspx"" title=""Update Activity Plan"" Target=""content"" class=""m2"">Update Activity Plan</a></li>")
                   Response.Write("</ul>")
                   Response.Write("</li>")
                   Response.Write("<li><a href=""../account.aspx"" title=""Account Management"" Target=""content"">Acc Mgmt</a></li>")
                   Response.Write("<li><a href='#' class='dir'>Report</a>")
                   Response.Write("<ul>")
                   Response.Write("<li><a href=""rtargets.aspx"" title=""View Target Summary"" Target=""content"">View Target Summary</a></li>")
                   Response.Write("<li><a href='performance.aspx' Target=""content"">Performance Report</a></li>")
                   Response.Write("<li><a href=""reportmaster.aspx"" title=""BSC View"" Target=""content"">BSC View</a></li>")
                   Response.Write("<li><a href=""stratmap.aspx"" title=""Strategy MAP JRN"" Target=""content"">Strategy MAP JRN</a></li>")
                   Response.Write("</ul>")
                   Response.Write("</li>")
                   Response.Write("</ul>")
                ElseIf Session("Permission") = "3" Or Session("Permission") = "5" Or Session("Permission") = "6" Then
                   Response.Write("<ul id='nav' class='dropdown dropdown-horizontal'>")
                   Response.Write("<li><a href=""../account.aspx"" title=""Account Management"" Target=""content"">Acc Mgmt</a></li>")
                   Response.Write("<li><a href='#' class='dir'>Report</a>")
                   Response.Write("<ul>")
                   Response.Write("<li><a href=""transaction.aspx"" title=""BSC Report"" Target=""content"">BSC Report</a></li>")
                   Response.Write("<li><a href='performance.aspx' Target=""content"">Performance Report</a></li>")
                   Response.Write("<li><a href=""reportmaster.aspx"" title=""BSC View"" Target=""content"">BSC View</a></li>")
                   Response.Write("<li><a href=""stratmap.aspx"" title=""Strategy MAP JRN"" Target=""content"">Strategy MAP JRN</a></li>")
                   Response.Write("</ul>")
                   Response.Write("</li>")
                   Response.Write("</ul>")
                ElseIf Session("Permission") = "4" Then
                   Response.Write("<ul id='nav' class='dropdown dropdown-horizontal'>")
                   Response.Write("<li><a href=""../account.aspx"" title=""Account Management"" Target=""content"">Acc Mgmt</a></li>")
                   Response.Write("<li><a href='#' class='dir'>Report</a>")
                   Response.Write("<ul>")
                   Response.Write("<li><a href=""transaction.aspx"" title=""BSC Report"" Target=""content"" class=""m2"">BSC Report</a></li>")
                   Response.Write("<li><a href='performance.aspx' Target=""content"">Performance Report</a></li>")
                   Response.Write("<li><a href=""reportmaster.aspx"" title=""BSC View"" Target=""content"">BSC View</a></li>")
                   Response.Write("<li><a href=""stratmap.aspx"" title=""Strategy MAP JRN"" Target=""content"">Strategy MAP JRN</a></li>")
                   Response.Write("</ul>")
                   Response.Write("</li>")
                   Response.Write("</ul>")
                ElseIf Session("Permission") = "S" Then
                End If
                %>            
             
          </div>
          <%
              If Session("otorisasi") IsNot Nothing Then
                Response.Write("<div id=""Logout"" Style=""text-align:right;padding-right:10px""> Selamat Datang " & Session("NmUser") & "-" & MAP(Session("Permission")) & ", " & "<a href=""../logout.aspx"">Logout</a></div>")
             End If
          %>
          </div>
      </div>
    </div>
    <div>
        <iframe name="content" style="width:100%;height:550px;" src="performance.aspx">
        </iframe>
    </div>
    </form>
</body>
</html>
