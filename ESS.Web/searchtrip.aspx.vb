﻿Imports System.Data.SqlClient

Partial Public Class searchtrip
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoadData(True)
    End Sub

    Public Property PageNumber() As Integer
        Get
            If ViewState("PageNumber") IsNot Nothing Then
                Return Convert.ToInt32(ViewState("PageNumber"))
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PageNumber") = value
        End Set
    End Property

    Function LoadData(ByVal bool As Boolean)
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Try
            Dim dt As New DataTable
            If bool Then
                Dim myCMD As SqlCommand = New SqlCommand("select TripNo, A.purpose, A.nik, A.kddepar, A.kdjabat , A.kdsite, A.kdlokasi, (SELECT  ISNULL(GLCostLS, '') FROM h_a150 WHERE  kdjabat = A.kdjabat) AS costcode, A.sup_nik, (case A.fstatus when '1' then 'Proses' else 'Unproses' end) as fstatus, B.nama, (select nama from H_A101 where nik = A.sup_nik ) as superior, (select nmjabat from H_A150 where kdjabat = A.kdjabat ) as nmjabat, (select NmDepar from H_A130 where kddepar = A.kddepar) as nmdepar, (SELECT costName FROM H_A221 WHERE costcode = A.costcode) AS costname, (case A.detailtrip when '01' then 'Business Trip' when '02' then 'Annual Leave' when '03' then 'Periodic Leave' when '04' then 'Recruitment' when '05' then 'Training' when '06' then 'Visitor' end) As Detailtrip from V_H001 A inner join H_A101 B on A.nik = B.nik where A.stedit <> '2' and A.fstatus = 1 and A.Tripno not in (select tripno from V_H002 where stedit <> '2') and A.nik like '%' + @Nama + '%' union select TripNo, A.purpose, A.nik, A.kddepar, A.kdjabat , A.kdsite, A.kdlokasi, (SELECT  ISNULL(GLCostLS, '') FROM h_a150 WHERE  kdjabat = A.kdjabat) AS costcode, A.sup_nik, (case A.fstatus when '1' then 'Proses' else 'Unproses' end) as fstatus, B.nama, (select nama from H_A101 where nik = A .sup_nik ) as superior, (select nmjabat from H_A150 where kdjabat = A.kdjabat ) as nmjabat, (select NmDepar from H_A130 where kddepar = A.kddepar) as nmdepar, (SELECT costName FROM H_A221 WHERE costcode = A.costcode) AS costname, (case A.detailtrip when '01' then 'Business Trip' when '02' then 'Annual Leave' when '03' then 'Periodic Leave' when '04' then 'Recruitment' when '05' then 'Training' when '06' then 'Visitor' end) As Detailtrip from V_H001 A inner join H_A101 B on A.nik = B.nik , V_A004 C where A.stedit <> '2' and A.fstatus = 1 and A.Tripno not in (select tripno from V_H002 where stedit <> '2') and C.nika like '%' + @Nama + '%' and c.nikb in(a.nik)", sqlConn)

                Dim param As New SqlParameter()
                param.ParameterName = "@Nama"
                param.Value = Session("niksite")
                myCMD.Parameters.Add(param)
                sqlConn.Open()
                Dim reader As SqlDataReader = myCMD.ExecuteReader()
                dt.Load(reader, LoadOption.OverwriteChanges)
                sqlConn.Close()
                ViewState("dt") = dt
                PageNumber = 0
            End If
            Dim pgitems As New PagedDataSource()
            Dim dv As New DataView(ViewState("dt"))
            pgitems.DataSource = dv
            pgitems.AllowPaging = True
            pgitems.PageSize = 15
            pgitems.CurrentPageIndex = PageNumber

            If pgitems.PageCount > 1 Then
                rptPages.Visible = True
                Dim pages As New ArrayList()
                For i As Integer = 0 To pgitems.PageCount - 1
                    pages.Add((i + 1).ToString())
                Next
                rptPages.DataSource = pages
                rptPages.DataBind()
            Else
                rptPages.Visible = False
            End If
            rptResult.DataSource = pgitems
            rptResult.DataBind()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            If sqlConn.State = ConnectionState.Open Then
                sqlConn.Close()
            End If
        End Try
    End Function

    Private Sub rptPages_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPages.ItemCommand
        PageNumber = Convert.ToInt32(e.CommandArgument) - 1
        LoadData(False)
    End Sub
End Class