﻿Imports System.Xml
Imports System.Text
Imports System.Web.UI.Page
Imports System.Net
Imports System.IO

Module Module1
    Public I As Integer = 0
    <System.Runtime.CompilerServices.Extension()> _
    Public Function IsLegalXmlChar(ByVal character As String) As Boolean
        ' == '\t' == 9   
        ' == '\n' == 10  
        ' == '\r' == 13  
        Return (character = &H9 OrElse character = &HA OrElse character = &HD OrElse (character >= &H20 AndAlso character <= &HD7FF) OrElse (character >= &HE000 AndAlso character <= &HFFFD) OrElse (character >= &H10000 AndAlso character <= &H10FFFF))
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function GetXElement(ByRef node As XmlNode) As XElement
        'Dim strdoc As String

        Dim xDoc As XDocument = New XDocument()
        'Dim buffer As New StringBuilder(xDoc.Root.ToString.Length)

        Using xmlWriter As XmlWriter = xDoc.CreateWriter()
            node.WriteTo(xmlWriter)
        End Using

        'If xDoc.Root.ToString.Contains("x0") Then
        '    strdoc = Replace(xDoc.Root.ToString, "x0", "")
        'End If

        Return xDoc.Root
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function GetXmlNode(ByRef element As XElement) As XmlNode
        I = (I + 1)
        If I = 30 Then
            I = I
        Else
            Using xmlReader As XmlReader = element.CreateReader()
                Dim xmlDoc As XmlDocument = New XmlDocument
                xmlDoc.Load(xmlReader)
                Return xmlDoc
            End Using
        End If
        
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function ToStringAlignAttributes(ByVal element As XElement) As String
        Dim settings As XmlWriterSettings = New XmlWriterSettings()
        settings.Indent = True
        settings.OmitXmlDeclaration = True
        settings.NewLineOnAttributes = True
        Dim stringBuilder As StringBuilder = New StringBuilder()
        Using xmlWriter As XmlWriter = xmlWriter.Create(stringBuilder, settings)
            element.WriteTo(xmlWriter)
        End Using
        Return stringBuilder.ToString()
    End Function

    Sub Main(ByVal args As String())
        Dim s As XNamespace = "http://schemas.microsoft.com/sharepoint/soap/"
        Dim rs As XNamespace = "urn:schemas-microsoft-com:rowset"
        Dim z As XNamespace = "#RowsetSchema"

        ' Make sure that you use the correct namespace, as well as the correct reference name.
        ' The namespace (by default) is the same as the name of the application when you created it.
        ' You specify the reference name in the Add Web Reference dialog box.
        '
        '                  Namespace         Reference Name              Namespace         Reference Name
        '                      |                  |                          |                     |
        '                      V                  V                          V                     V
        'Dim lists As SPWebServicesExample.ListsWebService.Lists = New SPWebServicesExample.ListsWebService.Lists()
        Dim lists As mossservices.Lists = New mossservices.Lists
        ' Make sure that you update the following URL to point to the Lists web service
        ' for your SharePoint site.
        If args(0) = "IT" Then
            lists.Url = "http://moss.jresources.com/IT/_vti_bin/Lists.asmx"
        ElseIf args(0) = "HR" Then
            lists.Url = "http://moss.jresources.com/HR/_vti_bin/Lists.asmx"
            'ElseIf args(0) = "" Then
            'lists.Url = "http://moss.jresources.com/HR/Documents/_vti_bin/Lists.asmx"
            'lists.Url = "http://moss.jresources.com/HRCA/_vti_bin/Lists.asmx"
        ElseIf args(0) = "HRCA" Then
            lists.Url = "http://moss.jresources.com/HRCA/_vti_bin/Lists.asmx"
        ElseIf args(0) = "Legal" Then
            lists.Url = "http://moss.jresources.com/Legal/_vti_bin/Lists.asmx"
        ElseIf args(0) = "Plant" Then
            lists.Url = "http://moss.jresources.com/Plant/_vti_bin/Lists.asmx"
        ElseIf args(0) = "FINACC" Then
            lists.Url = "http://moss.jresources.com/FINACC/_vti_bin/Lists.asmx"
        ElseIf args(0) = "IAS" Then
            lists.Url = "http://moss.jresources.com/IAS/_vti_bin/Lists.asmx"
        ElseIf args(0) = "Finance" Then
            lists.Url = "http://moss.jresources.com/Finance/_vti_bin/Lists.asmx"
        ElseIf args(0) = "Accounting" Then
            lists.Url = "http://moss.jresources.com/Accounting/_vti_bin/Lists.asmx"
        ElseIf args(0) = "Exploration" Then
            lists.Url = "http://moss.jresources.com/Exploration/_vti_bin/Lists.asmx"
        ElseIf args(0) = "techdev" Then
            lists.Url = "http://moss.jresources.com/techdev/_vti_bin/Lists.asmx"
        Else
            args(2) = "Department belum terdaftar di document center"
            Return
        End If


        'lists.Url = "http://xyzteamsite/_vti_bin/Lists.asmx";

        'lists.Credentials = System.Net.CredentialCache.DefaultCredentials

        'lists.GetListItems(l.Attribute("ID"), "", Nothing, _
        '           viewFields.GetXmlNode(), "", queryOptions.GetXmlNode(), "") _
        lists.Credentials = New System.Net.NetworkCredential("syam.kharisman", "sysyam356", "jresources")

        Dim queryOptions = _
            <QueryOptions>
                <IncludeMandatoryColumns>FALSE</IncludeMandatoryColumns>
                <ViewAttributes Scope='RecursiveAll' IncludeRootFolder='True'/>
            </QueryOptions>
        's<IncludeMandatoryColumns>FALSE</IncludeMandatoryColumns>
        '        <DateInUtc>TRUE</DateInUtc>
        '        <ViewAttributes Scope='Recursive'/>
        '<Folder>/hr/documents/others</Folder>
        '<ViewAttributes Scope='Recursive' ModerationType="Moderator"/>
        Dim viewFields = <ViewFields/>

        Dim query = _
        <Query><Where><Eq><FieldRef Name='FSObjType'/><Value Type='Lookup'>0</Value></Eq></Where></Query>
        'Dim xviewfields As XElement = XElement.Parse(viewFields)

        Dim listCollection As XElement = lists.GetListCollection().GetXElement()
        Try
            Dim report As XElement = _
        <Report>
            <%= listCollection _
                .Elements(s + "List") _
                .Select(Function(l) _
                New XElement("List", _
                l.Attribute("ID"), _
                l.Attribute("Title"), _
                l.Attribute("DefaultViewUrl"), _
                l.Attribute("Description"), _
                l.Attribute("DocTemplateUrl"), _
                l.Attribute("BaseType"), _
                l.Attribute("ItemCount"), _
                lists.GetListItems(l.Attribute("ID"), "", query.GetXmlNode(), _
                viewFields.GetXmlNode(), "1985", queryOptions.GetXmlNode(), "") _
                .GetXElement() _
                .Descendants(z + "row") _
                .Select(Function(r) _
                New XElement("Row", _
                r.Attribute("ows_EncodedAbsUrl"), _
                r.Attribute("ows_LinkFilename") _
                ) _
                ) _
                ) _
                ) %>
        </Report>

            Dim w As New System.Net.WebClient
            'w.Credentials = New System.Net.NetworkCredential("syam.kharisman", "356356", "jresources")

            'Server.ScriptTimeout = 3600
            'Console.WriteLine(report.ToStringAlignAttributes())
            args(1) = report.ToStringAlignAttributes()
            'args(1) = report
            'Dim Desturi As Uri = New Uri("\\files.jresources.com\Permit Documents\list.xml")
            'Dim LogonCred As NetworkCredential = New NetworkCredential("syam.kharisman", "356356", "jresources")

            Dim myCache As New CredentialCache()
            'myCache.Add(New Uri("\\files.jresources.com\Permit Documents\"), "Basic", New NetworkCredential("syam.kharisman", "356356", "jresources"))

            'Dim xmlDoc As New XmlDocument
            'xmlDoc.LoadXml(report.ToStringAlignAttributes())

            'myCache.Add(New Uri("\\files.jresources.com\Permit Documents\"), "Basic", New NetworkCredential("syam.kharisman", "356356", "jresources"))

            If args(0) = "IT" Then
                report.Save("C:\inetpub\wwwroot\HRMS\doc\list.xml")
            ElseIf args(0) = "HR" Then
                report.Save("C:\inetpub\wwwroot\HRMS\doc\hrlist.xml")
                'ElseIf args(0) = "" Then
                'report.Save("C:\inetpub\wwwroot\HRMS\doc\hrlist.xml")
            ElseIf args(0) = "HRCA" Then
                report.Save("C:\inetpub\wwwroot\HRMS\doc\hrcalist.xml")
            ElseIf args(0) = "Legal" Then
                report.Save("C:\inetpub\wwwroot\HRMS\doc\legallist.xml")
            ElseIf args(0) = "Plant" Then
                report.Save("C:\inetpub\wwwroot\HRMS\doc\plantlist.xml")
            ElseIf args(0) = "FINACC" Then
                report.Save("C:\inetpub\wwwroot\HRMS\doc\finacclist.xml")
            ElseIf args(0) = "IAS" Then
                report.Save("C:\inetpub\wwwroot\HRMS\doc\iaslist.xml")
            ElseIf args(0) = "Finance" Then
                report.Save("C:\inetpub\wwwroot\HRMS\doc\financelist.xml")
            ElseIf args(0) = "Accounting" Then
                report.Save("C:\inetpub\wwwroot\HRMS\doc\accountinglist.xml")
            ElseIf args(0) = "Exploration" Then
                report.Save("C:\inetpub\wwwroot\HRMS\doc\explorationlist.xml")
            ElseIf args(0) = "techdev" Then
                report.Save("C:\inetpub\wwwroot\HRMS\doc\techdevlist.xml")
            End If
        Catch ex As Exception
            Dim err As String
            err = ex.ToString
        End Try

        'Dim lists2 As mossservices.Lists = New mossservices.Lists
        '' Make sure that you update the following URL to point to the Lists web service
        '' for your SharePoint site.
        'If args(0) = "IT" Then
        '    lists2.Url = "http://moss.jresources.com/IT/_vti_bin/Lists.asmx"
        'ElseIf args(0) = "HR" Then
        '    'lists2.Url = "http://moss.jresources.com/HR/_vti_bin/Lists.asmx"
        '    'ElseIf args(0) = "" Then
        '    lists2.Url = "http://moss.jresources.com/HR/Documents/_vti_bin/Lists.asmx"
        '    'lists.Url = "http://moss.jresources.com/HRCA/_vti_bin/Lists.asmx"
        'ElseIf args(0) = "HRCA" Then
        '    lists2.Url = "http://moss.jresources.com/HRCA/_vti_bin/Lists.asmx"
        'ElseIf args(0) = "Legal" Then
        '    lists2.Url = "http://moss.jresources.com/Legal/_vti_bin/Lists.asmx"
        'ElseIf args(0) = "Plant" Then
        '    lists2.Url = "http://moss.jresources.com/Plant/_vti_bin/Lists.asmx"
        'ElseIf args(0) = "FINACC" Then
        '    lists2.Url = "http://moss.jresources.com/FINACC/_vti_bin/Lists.asmx"
        'ElseIf args(0) = "IAS" Then
        '    lists2.Url = "http://moss.jresources.com/IAS/_vti_bin/Lists.asmx"
        'ElseIf args(0) = "Finance" Then
        '    lists2.Url = "http://moss.jresources.com/Finance/_vti_bin/Lists.asmx"
        'ElseIf args(0) = "Accounting" Then
        '    lists2.Url = "http://moss.jresources.com/Accounting/_vti_bin/Lists.asmx"
        'ElseIf args(0) = "Exploration" Then
        '    lists2.Url = "http://moss.jresources.com/Exploration/_vti_bin/Lists.asmx"
        'ElseIf args(0) = "techdev" Then
        '    lists2.Url = "http://moss.jresources.com/techdev/_vti_bin/Lists.asmx"
        'Else
        '    args(2) = "Department belum terdaftar di document center"
        '    Return
        'End If

        'lists2.Credentials = New System.Net.NetworkCredential("syam.kharisman", "sysyam356", "jresources")
        'Dim listCollection2 As XElement = lists2.GetListCollection().GetXElement()
        'Try
        '    Dim report As XElement = _
        '<Report>
        '    <%= listCollection2 _
        '        .Elements(s + "List") _
        '        .Select(Function(l) _
        '        New XElement("List", _
        '        l.Attribute("ID"), _
        '        l.Attribute("Title"), _
        '        l.Attribute("DefaultViewUrl"), _
        '        l.Attribute("Description"), _
        '        l.Attribute("DocTemplateUrl"), _
        '        l.Attribute("BaseType"), _
        '        l.Attribute("ItemCount"), _
        '        lists.GetListItems(l.Attribute("ID"), "", query.GetXmlNode(), _
        '        viewFields.GetXmlNode(), "1984", queryOptions.GetXmlNode(), "") _
        '        .GetXElement() _
        '        .Descendants(z + "row") _
        '        .Select(Function(r) _
        '        New XElement("Row", _
        '        r.Attribute("ows_EncodedAbsUrl"), _
        '        r.Attribute("ows_LinkFilename") _
        '        ) _
        '        ) _
        '        ) _
        '        ) %>
        '</Report>

        '    Dim w As New System.Net.WebClient
        '    'w.Credentials = New System.Net.NetworkCredential("syam.kharisman", "356356", "jresources")

        '    'Server.ScriptTimeout = 3600
        '    'Console.WriteLine(report.ToStringAlignAttributes())
        '    args(1) = report.ToStringAlignAttributes()
        '    'args(1) = report
        '    'Dim Desturi As Uri = New Uri("\\files.jresources.com\Permit Documents\list.xml")
        '    'Dim LogonCred As NetworkCredential = New NetworkCredential("syam.kharisman", "356356", "jresources")

        '    Dim myCache As New CredentialCache()
        '    'myCache.Add(New Uri("\\files.jresources.com\Permit Documents\"), "Basic", New NetworkCredential("syam.kharisman", "356356", "jresources"))

        '    'Dim xmlDoc As New XmlDocument
        '    'xmlDoc.LoadXml(report.ToStringAlignAttributes())

        '    'myCache.Add(New Uri("\\files.jresources.com\Permit Documents\"), "Basic", New NetworkCredential("syam.kharisman", "356356", "jresources"))

        '    If args(0) = "IT" Then
        '        report.Save("C:\inetpub\wwwroot\HRMS\doc\list2.xml")
        '    ElseIf args(0) = "HR" Then
        '        report.Save("C:\inetpub\wwwroot\HRMS\doc\hrlist2.xml")
        '        'ElseIf args(0) = "" Then
        '        'report.Save("C:\inetpub\wwwroot\HRMS\doc\hrlist.xml")
        '    ElseIf args(0) = "HRCA" Then
        '        report.Save("C:\inetpub\wwwroot\HRMS\doc\hrcalist.xml")
        '    ElseIf args(0) = "Legal" Then
        '        report.Save("C:\inetpub\wwwroot\HRMS\doc\legallist.xml")
        '    ElseIf args(0) = "Plant" Then
        '        report.Save("C:\inetpub\wwwroot\HRMS\doc\plantlist.xml")
        '    ElseIf args(0) = "FINACC" Then
        '        report.Save("C:\inetpub\wwwroot\HRMS\doc\finacclist.xml")
        '    ElseIf args(0) = "IAS" Then
        '        report.Save("C:\inetpub\wwwroot\HRMS\doc\iaslist.xml")
        '    ElseIf args(0) = "Finance" Then
        '        report.Save("C:\inetpub\wwwroot\HRMS\doc\financelist.xml")
        '    ElseIf args(0) = "Accounting" Then
        '        report.Save("C:\inetpub\wwwroot\HRMS\doc\accountinglist.xml")
        '    ElseIf args(0) = "Exploration" Then
        '        report.Save("C:\inetpub\wwwroot\HRMS\doc\explorationlist.xml")
        '    ElseIf args(0) = "techdev" Then
        '        report.Save("C:\inetpub\wwwroot\HRMS\doc\techdevlist.xml")
        '    End If
        'Catch ex As Exception
        '    Dim err As String
        '    err = ex.ToString
        'End Try

        'xmlDoc.Save("\\files.jresources.com\Permit Documents\list.xml")
        'w.UploadString("\\files.jresources.com\Permit Documents\list.xml", report.ToStringAlignAttributes())
        'My.Computer.Network.UploadFile(report.ToStringAlignAttributes(), Desturi, LogonCred, False, 3500)


        'report.Save("../wwwroot/HRMS/permit/list.xml")

        'report.Save("C:\inetpub\wwwroot\HRMS\permit\list.xml")
        'args(1) = listCollection.ToStringAlignAttributes()
      
    End Sub
End Module


'l.Attribute("Title"), _
'                    l.Attribute("DefaultViewUrl"), _
'                    l.Attribute("Description"), _
'                    l.Attribute("DocTemplateUrl"), _
'                    l.Attribute("BaseType"), _
'                    l.Attribute("ItemCount"), _

'r.Attribute("ows_ContentType"), _
'r.Attribute("ows_FSObjType"), _
'r.Attribute("ows_Attachments"), _
'r.Attribute("ows_FirstName"), _
'r.Attribute("ows_LinkFilename"), _


'r.Attribute("ows_Title"), _
'r.Attribute("ows_BaseName"), _
'                    r.Attribute("ows_FileLeafRef"), _
'                    r.Attribute("ows_FileRef"), _
'                    r.Attribute("ows_ID"), _
'                    r.Attribute("ows_UniqueId"), _
'                    r.Attribute("ows_GUID") _