﻿
Public Class ResponseModel
    Private _isSuccess As Boolean
    Private _message As String

    Public Property IsSuccess() As Boolean
        Get
            Return _isSuccess
        End Get
        Set(ByVal value As Boolean)
            _isSuccess = value
        End Set
    End Property

    Public Property Message() As String
        Get
            Return _message
        End Get
        Set(ByVal value As String)
            _message = value
        End Set
    End Property

    Public Sub SetError(ByVal message As String)
        Me.IsSuccess = False
        Me.Message = message
    End Sub

    Public Sub SetSuccess(ByVal message As String)
        Me.IsSuccess = True
        Me.Message = message
    End Sub

End Class
