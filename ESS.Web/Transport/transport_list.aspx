﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="transport_list.aspx.vb" Inherits="EXCELLENT.transport_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Transport
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<%--<script src="Scripts/tcal.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/tcal.css"/> --%>
<script type="text/javascript" >
    function frmcetak() {
        var d = document;
        window.location = "business_trip_list.aspx?mth=" + '<%=imonth %>' + "&yr=" + '<%=th %>'
    }
</script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
<caption style="text-align: center; font-size: 1.5em; color:White;">Tranports List</caption>
<tr>
<td style="width:5%;">Periode</td>
<td style="width:8%;">
<asp:DropDownList ID="ddlbulan" runat="server">
    <asp:ListItem Value = "1">January</asp:ListItem>
    <asp:ListItem Value = "2">February</asp:ListItem>
    <asp:ListItem Value = "3">March</asp:ListItem>
    <asp:ListItem Value = "4">April</asp:ListItem>
    <asp:ListItem Value = "5">May</asp:ListItem>
    <asp:ListItem Value = "6">June</asp:ListItem>
    <asp:ListItem Value = "7">July</asp:ListItem>
    <asp:ListItem Value = "8">August</asp:ListItem>
    <asp:ListItem Value = "9">September</asp:ListItem>
    <asp:ListItem Value = "10">October</asp:ListItem>
    <asp:ListItem Value = "11">November</asp:ListItem>
    <asp:ListItem Value = "12">December</asp:ListItem>
</asp:DropDownList>
<asp:TextBox ID="tripfrom" style="width:70px;" runat="server" width="90px" ReadOnly="true" Visible ="false"></asp:TextBox></td>
<td style="width:5%;"><asp:DropDownList ID="ddltahun" runat="server"></asp:DropDownList></td>
<td style="width:5%;"><asp:Button ID="btnSearch" runat="server" Text="Search"/></td>
<%--<td style="width:5%;"><input type="button" visible="false" id="cetak" value="Print" onclick="frmcetak()" style="width: 80px; font-family: Calibri; " /></td>--%>
<td>
 <asp:TextBox ID="tripto" style="width:70px;" runat="server" ReadOnly="true" visible="false"></asp:TextBox>
</td>
<td></td>
</tr></table>


<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Transportno" AllowPaging="true" PageSize="100" Width="1200px">
    <RowStyle Font-Size="Smaller" />
<Columns>
<asp:CommandField ShowSelectButton="True" SelectText="Select" ButtonType="Button"  
        HeaderStyle-Width="40px" ItemStyle-VerticalAlign="top" >
<HeaderStyle Width="40px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:CommandField>
            <asp:BoundField DataField="transportno" HeaderText="Transport Number" 
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="niksite" HeaderText="NIK" 
        HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="90px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
     <asp:BoundField DataField="nama" HeaderText="Nama" 
        HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="200px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="nmdepar" HeaderText="Department" 
        HeaderStyle-Width="250px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="250px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>

<%--<asp:BoundField DataField="Vehicle_type" HeaderText="Type" 
        HeaderStyle-Width="120px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="120 px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>--%>
    
<asp:BoundField DataField="reqdate" HeaderText="Request Date" 
        HeaderStyle-Width="100px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>  
</Columns> 
    <SelectedRowStyle BackColor="#99CCFF" />
    <HeaderStyle BackColor="#0099FF" />
</asp:GridView>
</asp:Content>
