﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="searchemployeesplapp.aspx.vb" Inherits="EXCELLENT.searchemployeesplapp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <link href="css/site.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function UseSelected(nopeg, nama, kddepar, nmjabat, kdsite, kdjabat, kddepart, nik, pycostcode, app1, app2, app1nik, app2nik,app3,app3nik ) {
          var pw = window.opener;
          var inputFrm = pw.document.forms['aspnetForm'];
          if (inputFrm.elements['ctrlToFind'].value == 'Originator') {
              inputFrm.elements['ctl00_ContentPlaceHolder1_Originator'].value = nopeg;
              inputFrm.elements['ctl00_ContentPlaceHolder1_OriginatorName'].value = nama;
          } else if (inputFrm.elements['ctrlToFind'].value == 'PenanggungJawab') {
              inputFrm.elements['ctl00_ContentPlaceHolder1_PenanggungJawab'].value = nopeg;
              inputFrm.elements['ctl00_ContentPlaceHolder1_PenanggungJawabName'].value = nama;
          } else if (inputFrm.elements['ctrlToFind'].value == 'PIC') {
              inputFrm.elements['ctl00$ContentPlaceHolder1$AccordionPane1_content$PIC'].value = nopeg;
              inputFrm.elements['ctl00$ContentPlaceHolder1$AccordionPane1_content$PICName'].value = nama;
          } else if (Left(inputFrm.elements['ctrlToFind'].value, 2) == 'qq') {
              inputFrm.elements['ps' + Right(inputFrm.elements['ctrlToFind'].value, inputFrm.elements['ctrlToFind'].value.length - 3) + 'nik'].value = nopeg;
              inputFrm.elements['ps' + Right(inputFrm.elements['ctrlToFind'].value, inputFrm.elements['ctrlToFind'].value.length - 3) + 'name'].value = nama;
          } else if (inputFrm.elements['ctrlToFind'].value == 'PimRap') {
              inputFrm.elements['nikPimRap'].value = nopeg;
              inputFrm.elements['namaPimRap'].value = nama;
              pw.document.getElementById("jnsmeeting").focus();
          } else if (inputFrm.elements['ctrlToFind'].value == 'NotRap') {
              inputFrm.elements['nikNotRap'].value = nopeg;
              inputFrm.elements['namaNotRap'].value = nama;
          } else if (Left(inputFrm.elements['ctrlToFind'].value, 3) == 'pic') {
              inputFrm.elements[inputFrm.elements['ctrlToFind'].value].value = nopeg;
              inputFrm.elements[inputFrm.elements['ctrlToFind'].value + 'name'].value = nama;
          } else if (inputFrm.elements['ctrlToFind'].value == 'user') {
              inputFrm.elements['nik'].value = nopeg;
              inputFrm.elements['nama'].value = nama;
              inputFrm.elements['kddepar'].value = kddepar;
              inputFrm.elements['nmjabat'].value = nmjabat;
              inputFrm.elements['site'].value = kdsite;
              inputFrm.elements['kdjabat'].value = kdjabat;
              inputFrm.elements['kddepart'].value = kddepart;
              inputFrm.elements['kddepart'].value = kddepart;
          } else if (inputFrm.elements['ctrlToFind'].value == 'suser') {
            inputFrm.elements['snik'].value = nopeg;
            inputFrm.elements['ssnik'].value = nik;  
          } else if (inputFrm.elements['ctrlToFind'].value == 'reqby') {
              inputFrm.elements['reqby'].value = nopeg;
          } else if (inputFrm.elements['ctrlToFind'].value == 'supappr') {
              inputFrm.elements['apprby'].value = nopeg;
          } else if (inputFrm.elements['ctrlToFind'].value == 'revby') {
              inputFrm.elements['reviewby'].value = nopeg;
          } else if (inputFrm.elements['ctrlToFind'].value == 'finappr') {
              inputFrm.elements['proceedby'].value = nopeg;
          } else if (inputFrm.elements['ctrlToFind'].value == 'leave2') {
              inputFrm.elements['ctl00_ContentPlaceHolder1_txtnikdut'].value = nopeg;
              inputFrm.elements['ctl00_ContentPlaceHolder1_txtnmdut'].value = nama;
              inputFrm.elements['ctl00_ContentPlaceHolder1_txtposdut'].value = nmjabat;
              inputFrm.elements['ctl00_ContentPlaceHolder1_posdut'].value = kdjabat;
              inputFrm.elements['ctl00_ContentPlaceHolder1_txtniksitedut'].value = nik;
          } else if (inputFrm.elements['ctrlToFind'].value == 'leave3') {
              inputFrm.elements['ctl00_ContentPlaceHolder1_txtverby'].value = nik;
              inputFrm.elements['ctl00_ContentPlaceHolder1_txtposverby'].value = nmjabat;
              inputFrm.elements['ctl00_ContentPlaceHolder1_posverby'].value = kdjabat;
          } else if (inputFrm.elements['ctrlToFind'].value == 'leave4') {
              inputFrm.elements['ctl00_ContentPlaceHolder1_txtnikappby'].value = nik;
              inputFrm.elements['ctl00_ContentPlaceHolder1_txtposappby'].value = nmjabat;
              inputFrm.elements['ctl00_ContentPlaceHolder1_posappby'].value = kdjabat;
          } else if (inputFrm.elements['ctrlToFind'].value == 'ppa_spv') {
              inputFrm.elements['ctl00_ContentPlaceHolder1_hnikspv'].value = nik;
              inputFrm.elements['ctl00_ContentPlaceHolder1_txtspv'].value = nama;
          } else if (inputFrm.elements['ctrlToFind'].value == 'spl') {
                inputFrm.elements['ctl00_ContentPlaceHolder1_hnik'].value = nik;
                inputFrm.elements['ctl00_ContentPlaceHolder1_txtnik'].value = nopeg;
                inputFrm.elements['ctl00_ContentPlaceHolder1_txtnama'].value = nama;
                inputFrm.elements['ctl00_ContentPlaceHolder1_txtdept'].value = kddepar;
                inputFrm.elements['ctl00_ContentPlaceHolder1_hkdsite'].value = kdsite;
                inputFrm.elements['ctl00_ContentPlaceHolder1_hkddepar'].value = kddepart;
                inputFrm.elements['ctl00_ContentPlaceHolder1_hkdjabat'].value = kdjabat;
                inputFrm.elements['ctl00_ContentPlaceHolder1_txtsite'].value = kdsite;
                inputFrm.elements['ctl00_ContentPlaceHolder1_txtcostcode'].value = pycostcode;
                inputFrm.elements['ctl00_ContentPlaceHolder1_txtspv1'].value = app1;
                inputFrm.elements['ctl00_ContentPlaceHolder1_hnikspv1'].value = app1nik;
                if (app2 == '') {
                    inputFrm.elements['ctl00_ContentPlaceHolder1_txtspv2'].value = app3;
                    inputFrm.elements['ctl00_ContentPlaceHolder1_hnikspv2'].value = app3nik;
                } else {
                    inputFrm.elements['ctl00_ContentPlaceHolder1_txtspv2'].value = app2;
                    inputFrm.elements['ctl00_ContentPlaceHolder1_hnikspv2'].value = app2nik;
                }
          } else if (inputFrm.elements['ctrlToFind'].value == 'splreq') {
                inputFrm.elements['ctl00_ContentPlaceHolder1_hnikreq'].value = nik;
                inputFrm.elements['ctl00_ContentPlaceHolder1_txtnikreq'].value = nopeg;
                inputFrm.elements['ctl00_ContentPlaceHolder1_txtnamareq'].value = nama;
                inputFrm.elements['ctl00_ContentPlaceHolder1_txtdeptreq'].value = kddepar;
                inputFrm.elements['ctl00_ContentPlaceHolder1_hkdsitereq'].value = kdsite;
                inputFrm.elements['ctl00_ContentPlaceHolder1_hkddeparreq'].value = kddepart;
                inputFrm.elements['ctl00_ContentPlaceHolder1_hkdjabatreq'].value = kdjabat;
                inputFrm.elements['ctl00_ContentPlaceHolder1_txtsitereq'].value = kdsite;
                inputFrm.elements['ctl00_ContentPlaceHolder1_txtcostcodereq'].value = pycostcode;
                
          } else if (inputFrm.elements['ctrlToFind'].value == 'splspv1') {
                inputFrm.elements['ctl00_ContentPlaceHolder1_hnikspv1'].value = nik;
                inputFrm.elements['ctl00_ContentPlaceHolder1_txtspv1'].value = nama;
          } else if (inputFrm.elements['ctrlToFind'].value == 'splspv2') {
                inputFrm.elements['ctl00_ContentPlaceHolder1_hnikspv2'].value = nik;
                inputFrm.elements['ctl00_ContentPlaceHolder1_txtspv2'].value = nama;
          } else {
              inputFrm.elements[inputFrm.elements['ctrlToFind'].value].value = nopeg;
              inputFrm.elements[inputFrm.elements['ctrlToFind'].value + 'name'].value = nama;
          }
          window.close();
       }
       function Left(str, n) {
          if (n <= 0)
             return "";
          else if (n > String(str).length)
             return str;
          else
             return String(str).substring(0, n);
       }

       function Right(str, n) {
          if (n <= 0)
             return "";
          else if (n > String(str).length)
             return str;
          else {
             var iLen = String(str).length;
             return String(str).substring(iLen, iLen - n);
          }
      }

      function keyPressed(e) {
         switch (e.keyCode) {
            case 13:
               {
                  document.getElementById("btnSearch").click();
                  break;
               }
             default:
               {
                  break;
               }
         }
     }

     function cekArgumen() {
        var d = document;
        if ((d.getElementById("SearchKey").value.length < 3) || (d.getElementById("SearchKey").value.length == 0)){
            alert("Please Entry Specific NIK / Name (minimal 3 Character)");
            return false;
        }
    }

    
    </script>
</head>
<body>
    <form id="form1" runat="server">
  <ajaxToolkit:ToolkitScriptManager  ID="ToolkitScriptManager1" runat="server"/>
   <asp:UpdatePanel id="MyUpdatePanel" runat="server">
    <ContentTemplate>
      <div>           
      <asp:HiddenField id="hnik" runat = "server" />
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
         <caption style="text-align:center">Select Superintendent</caption>
         <tr>
            <td style="padding: 5px; width:20%"> </td>
            <td style="width:15%"><asp:TextBox ID="SearchKey" runat="server" class="tb10" onkeydown="keyPressed(event);"/></td>
            <td><input type="text" id="hide" style="display:none;"/><asp:Button ID="btnSearch" 
                    runat="server" Text="Cari Nama" Font-Bold="False" ForeColor="#0033CC" 
                    OnClientClick="cekArgumen()" Height="26px" Width="90px"/></td>
         </tr>
         <tr>
         <td>
         </td>
         </tr>
         <tr>
         <td colspan="3">
         <asp:Repeater ID="rptPages" Runat="server">
      <HeaderTemplate>
      <table cellpadding="0" cellspacing="0" border="0">
      <tr>
         <td style="vertical-align:top"><b>Page:</b>&nbsp;</td>
         <td>
      </HeaderTemplate>
      <ItemTemplate>
                        <asp:LinkButton ID="btnPage" CommandName="Page" 
                         CommandArgument="<%# Container.DataItem %>" 
                         CssClass="text" 
                         Runat="server" 
                         Visible='<%# (CInt(Container.DataItem) <> (PageNumber+1)).ToString() %>'>
                         <%#Container.DataItem%> 
                         </asp:LinkButton>
                         <asp:Label ID="lblPage" runat="server" Visible='<%# (CInt(Container.DataItem) = (PageNumber+1)).ToString()%>' 
                         Text="<%#Container.DataItem%>" 
                         ForeColor="black" Font-Bold="true">
                         </asp:Label>
      </ItemTemplate>
      <FooterTemplate>
         </td>
      </tr>
      </td>
      </table>
      </FooterTemplate>
      </asp:Repeater>
         </tr>
         <tr>
            <td colspan="3">
               <asp:Repeater ID="rptResult" runat="server">
               <HeaderTemplate>
                           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table-inside">
                           <tr>               
                                <th>No.</th>
                                <th>Nik</th>
                                <th>Name</th>
                                <%--<th>Kd. Departemen</th>--%>
                                <th>Department</th>
                                <%--<th>Jobsite</th>--%>
                                <th>Position</th>
                           </tr>
                        </HeaderTemplate>  
                        <ItemTemplate>
                       <tr>
                        <td><%# Container.ItemIndex + 1 %></td>
                        <td><a href="#" onclick="UseSelected('<%#DataBinder.Eval(Container.DataItem, "niksite")%>','<%#DataBinder.Eval(Container.DataItem, "Nama")%>','<%#DataBinder.Eval(Container.DataItem, "KdDepar")%>','<%#DataBinder.Eval(Container.DataItem, "NmJabat")%>','<%#DataBinder.Eval(Container.DataItem, "KdSite")%>','<%#DataBinder.Eval(Container.DataItem, "kdjabat")%>','<%#DataBinder.Eval(Container.DataItem, "kddepart")%>','<%#DataBinder.Eval(Container.DataItem, "nik")%>','<%#DataBinder.Eval(Container.DataItem, "pycostcode")%>','<%#DataBinder.Eval(Container.DataItem, "app1")%>','<%#DataBinder.Eval(Container.DataItem, "app2")%>','<%#DataBinder.Eval(Container.DataItem, "app1nik")%>','<%#DataBinder.Eval(Container.DataItem, "app2nik")%>','<%#DataBinder.Eval(Container.DataItem, "app3")%>','<%#DataBinder.Eval(Container.DataItem, "app3nik")%>');"><%#DataBinder.Eval(Container.DataItem, "niksite")%></a></td>
                        <td><%#DataBinder.Eval(Container.DataItem, "Nama")%></td>
                        <%--<td align="center"><%#DataBinder.Eval(Container.DataItem, "KdDepar")%></td>--%>
                        <td><%#DataBinder.Eval(Container.DataItem, "NmDepar")%></td>
                        <%--<td align="center"><%#DataBinder.Eval(Container.DataItem, "KdSite")%></td>--%>
                        <td align="center"><%#DataBinder.Eval(Container.DataItem, "NmJabat")%></td>
                        </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                           </table>
                        </FooterTemplate>
               </asp:Repeater>             
            </td>
         </tr>
      
      </table>  
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
     <asp:UpdateProgress ID="progress1" runat="server" DisplayAfter="50">    
                  <ProgressTemplate>
                  <asp:Panel ID="pnlProgressBar" runat="server">    
                     <div><asp:Image ID="Image1" runat="Server" ImageUrl="images/ajax-loader.gif"/>Please 
                         Wait...</div>    
                  </asp:Panel>   
                  </ProgressTemplate> 
                  </asp:UpdateProgress>
    </form>
    <script type="text/javascript">
        

        var ps = window.opener;
        var inputFrms = ps.document.forms['aspnetForm'];

    </script>
</body>
</html>