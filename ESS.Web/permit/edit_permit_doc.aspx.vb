﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Public Class edit_permit_doc
    Inherits System.Web.UI.Page
    Public opttype As String
    Public fsubject As String
    Public doctype As String
    Public rems As String
    Public mnentry As String
    Public mnview As String
    Public mnmgmt As String
    Public msgbox As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim pid As String
        pid = Session("Pid")

        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim strsql As String = "select id,ftype,fsubject,kdcom, company_name,kdsite,site_name,fyear,fcategory,doctype,department,subdepartment,number,must_report,cms_start,cms_end,f_path,f_name, rem_code, pic1_mail, pic2_mail, pic3_mail, pic4_mail, pic5_mail, createdtime from docpermit_trans where id='" + pid + "'"

        Dim dtb As DataTable = New DataTable
        Dim sda As SqlDataAdapter = New SqlDataAdapter(strsql, sqlConn)
        sda.Fill(dtb)

        Dim ftype As String
        Dim dtype As String

        If Session("otorisasi_permit") = Nothing Or Session("otorisasi_permit") = "" Then
            Response.Redirect("signin_permit.aspx")
        End If

        If dtb.Rows.Count > 0 Then
            ddlcomp.SelectedValue = dtb.Rows(0)!kdcom.ToString
            ddlsite.SelectedValue = dtb.Rows(0)!kdsite.ToString
            ftype = dtb.Rows(0)!ftype.ToString
            'If ftype = "Surat Masuk" Then
            'ltype.SelectedIndex = 0
            'opttype = "<select id='type'><option value='Surat Masuk' selected='selected'>Surat Masuk</option><option value='Surat Keluar'>Surat Keluar</option></select>"
            'ElseIf ftype = "Surat Keluar" Then
            'ltype.SelectedIndex = 1
            opttype = "<select id='type' style='visibility:hidden;'><option value=' '> </option></select>"
            ' End If

            fsubject = dtb.Rows(0)!fsubject.ToString
            subject.Value = fsubject

            'comp.Value = dtb.Rows(0)!company_name.ToString
            'site.Value = dtb.Rows(0)!site_name.ToString
            txtyear.Value = CDate(dtb.Rows(0)!createdtime).ToString("MM/dd/yyyy")
            category.Value = dtb.Rows(0)!fcategory.ToString
            dtype = dtb.Rows(0)!doctype.ToString
            If dtype = "SK" Then
                'ldoctype.SelectedIndex = 0
                doctype = "<select id='doctype' style='width:200px;'><option value='SK'>SK</option><option value='SE'>SE</option><option value='SOP'>SOP</option><option value='WI'>WI</option><option value='SPP'>SPP</option><option value='IM'>Interoffice Memorandum</option><option value='ITM'>Internal Memorandum</option><option value='SKR'>Surat Keluar</option></select>"
            ElseIf dtype = "SE" Then
                'ldoctype.SelectedIndex = 1
                doctype = "<select id='doctype' style='width:200px;'><option value='SK' selected='selected'>SK</option><option value='SE' selected='selected'>SE</option><option value='SOP'>SOP</option><option value='WI'>WI</option><option value='SPP'>SPP</option><option value='IM'>Interoffice Memorandum</option><option value='ITM'>Internal Memorandum</option><option value='SKR'>Surat Keluar</option></select>"
            ElseIf dtype = "SOP" Then
                'ldoctype.SelectedIndex = 2
                doctype = "<select id='doctype' style='width:200px;'><option value='SK' selected='selected'>SK</option><option value='SE'>SE</option><option value='SOP' selected='selected'>SOP</option><option value='WI'>WI</option><option value='SPP'>SPP</option><option value='IM'>Interoffice Memorandum</option><option value='ITM'>Internal Memorandum</option><option value='SKR'>Surat Keluar</option></select>"
            ElseIf dtype = "WI" Then
                'ldoctype.SelectedIndex = 3
                doctype = "<select id='doctype' style='width:200px;'><option value='SK' selected='selected'>SK</option><option value='SE'>SE</option><option value='SOP'>SOP</option><option value='WI' selected='selected'>WI</option><option value='SPP'>SPP</option><option value='IM'>Interoffice Memorandum</option><option value='ITM'>Internal Memorandum</option><option value='SKR'>Surat Keluar</option></select>"
            ElseIf dtype = "SPP" Then
                'ldoctype.SelectedIndex = 3
                doctype = "<select id='doctype' style='width:200px;'><option value='SK' selected='selected'>SK</option><option value='SE'>SE</option><option value='SOP'>SOP</option><option value='WI'>WI</option><option value='SPP' selected='selected'>SPP</option><option value='IM'>Interoffice Memorandum</option><option value='ITM'>Internal Memorandum</option><option value='SKR'>Surat Keluar</option></select>"
            ElseIf dtype = "IM" Then
                'ldoctype.SelectedIndex = 3
                doctype = "<select id='doctype' style='width:200px;'><option value='SK' selected='selected'>SK</option><option value='SE'>SE</option><option value='SOP'>SOP</option><option value='WI'>WI</option><option value='SPP'>SPP</option><option value='IM' selected='selected'>Interoffice Memorandum</option><option value='ITM'>Internal Memorandum</option><option value='SKR'>Surat Keluar</option></select>"
            ElseIf dtype = "ITM" Then
                doctype = "<select id='doctype' style='width:200px;'><option value='SK' selected='selected'>SK</option><option value='SE'>SE</option><option value='SOP'>SOP</option><option value='WI'>WI</option><option value='SPP'>SPP</option><option value='IM'>Interoffice Memorandum</option><option value='ITM' selected='selected'>Internal Memorandum</option><option value='SKR'>Surat Keluar</option></select>"
            ElseIf dtype = "SKR" Then
                doctype = "<select id='doctype' style='width:200px;'><option value='SK' selected='selected'>SK</option><option value='SE'>SE</option><option value='SOP'>SOP</option><option value='WI'>WI</option><option value='SPP'>SPP</option><option value='IM'>Interoffice Memorandum</option><option value='ITM'>Internal Memorandum</option><option value='SKR' selected='selected'>Surat Keluar</option></select>"
            End If

            Department.Value = dtb.Rows(0)!department.ToString
            subdept.Value = dtb.Rows(0)!subdepartment.ToString
            Number.Value = dtb.Rows(0)!number.ToString
            report.Value = dtb.Rows(0)!must_report.ToString
            dtissue.Value = dtb.Rows(0)!cms_start.ToString
            dtexp.Value = dtb.Rows(0)!cms_end.ToString

            dtissue.Value = CDate(dtissue.Value).ToString("MM/dd/yyyy")
            dtexp.Value = CDate(dtexp.Value).ToString("MM/dd/yyyy")

            If report.Value = 1 Then
                rpt.Checked = True
            Else
                rpt.Checked = False
            End If


            filename.Value = dtb.Rows(0)!f_name

            rem_days.SelectedValue = dtb.Rows(0)!rem_code
            'If Trim(dtb.Rows(0)!pic1_mail) <> "" Then
            '    txtpic1.SelectedValue = dtb.Rows(0)!pic1_mail
            '    chkpic1.Checked = True
            'Else
            '    txtpic1.Style.Add("visibility", "hidden")
            'End If
            'If Trim(dtb.Rows(0)!pic2_mail) <> "" Then
            '    txtpic2.SelectedValue = dtb.Rows(0)!pic2_mail
            '    chkpic2.Checked = True
            'Else
            '    txtpic2.Style.Add("visibility", "hidden")
            'End If
            'If Trim(dtb.Rows(0)!pic3_mail) <> "" Then
            '    txtpic3.SelectedValue = dtb.Rows(0)!pic3_mail
            '    chkpic3.Checked = True
            'Else
            '    txtpic3.Style.Add("visibility", "hidden")
            'End If
            'If Trim(dtb.Rows(0)!pic4_mail) <> "" Then
            '    txtpic4.SelectedValue = dtb.Rows(0)!pic4_mail
            '    chkpic4.Checked = True
            'Else
            '    txtpic4.Style.Add("visibility", "hidden")
            'End If
            'If Trim(dtb.Rows(0)!pic5_mail) <> "" Then
            '    txtpic5.SelectedValue = dtb.Rows(0)!pic5_mail
            '    chkpic5.Checked = True
            'Else
            '    txtpic5.Style.Add("visibility", "hidden")
            'End If

        End If

        If Trim(Session("permission_permit")) = "1" Or Trim(Session("permission_permit")) = "2" Then
            mnentry = "<div id='mnentry'><img alt='entry document' src='../images/open_doc_small.png' /> <a href='doc_center_entry.aspx'>&nbsp;Set Document</a></div>"
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
        End If

        If Trim(Session("permission_permit")) = "1" Then
            mnmgmt = "<div id='mnmgmt'> <img alt='Management User' src='../images/open_doc_small.png' /> <a href='mgmnt_list.aspx'>&nbsp;Management User</a></div>"
        End If

        If Trim(Session("permission_permit")) = "3" Or Trim(Session("permission_permit")) = "4" Then
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
        End If
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        tny.Text = "Are you sure want to remove this document reminder?"
        yes.Visible = True
        no.Visible = True
    End Sub

    Protected Sub yes_Click(ByVal sender As Object, ByVal e As EventArgs) Handles yes.Click
        Dim dtbfindemp As DataTable = New DataTable
        Dim cmd As SqlCommand
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Try
            sqlConn.Open()
            Dim sqlQuery As String = "UPDATE docpermit_trans SET stedit = '2', deleteby = '" + Session("otorisasi_permit") + "', deletetime = '" + DateTime.Now + "' where id = '" + Session("Pid").ToString + "'"
            cmd = New SqlCommand(sqlQuery, sqlConn)
            cmd.ExecuteScalar()
            msgbox = "<script language='javascript' type='text/javascript'> alert('data has been deleted') </script>"
            Session("docedituser") = ""

            Response.Redirect("permit_list.aspx")
        Catch ex As Exception
            msgbox = "<script language='javascript' type='text/javascript'> alert('Failed to delete data') </script>"
        Finally
            sqlConn.Close()
        End Try
    End Sub

    Protected Sub no_Click(ByVal sender As Object, ByVal e As EventArgs) Handles no.Click
        tny.Text = ""
        yes.Visible = False
        no.Visible = False
    End Sub

    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
    '    Dim pid As String
    '    pid = Session("Pid")
    '    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

    '    If rpt.Checked = True Then
    '        report.Value = 1
    '    Else
    '        report.Value = 0
    '    End If
    '    conn.Open()
    '    Try
    '        Dim sqlquery As String = "update docpermit_trans set ftype= '" + ltype.SelectedValue + "', fsubject='" + subject.Value + "', company_name='" + comp.Value + "',site_name='" + site.Value + "', fyear='" + txtyear.Value + "', fcategory = '" + category.Value + "',doctype = '" + ldoctype.SelectedValue + "', department = '" + Department.Value + "', subdepartment = '" + subdept.Value + "', number = '" + Number.Value + "', must_report = '" + report.Value + "', cms_start = '" + dtissue.Value + "', cms_end='" + dtexp.Value + "' where id = '" + pid + "'"
    '        Dim cmd As SqlCommand
    '        cmd = New SqlCommand(sqlquery, conn)
    '        cmd.ExecuteScalar()
    '    Catch ex As Exception

    '    Finally
    '        conn.Close()
    '    End Try

    'End Sub

    'Protected Sub uploaddoc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles uploaddoc.Click
    '    Response.Redirect("entry_permit_doc.aspx")
    'End Sub

    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
    'Dim pid As String
    'pid = Session("Pid")
    'Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
    'Dim strsql As String = "select f_name from docpermit_trans where id='" + pid + "'"

    'Dim dtb As DataTable = New DataTable
    'Dim sda As SqlDataAdapter = New SqlDataAdapter(strsql, sqlConn)
    'sda.Fill(dtb)

    'Dim filepath As String = "C:\" + f_name

    'If File.Exists(filepath) Then
    '    ' Give a new name
    '    My.Computer.FileSystem.RenameFile(filepath, "")
    'Else
    '    ' Use existing name
    'End If
    'End Sub
End Class