﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="bussiness_trip_report_.aspx.vb" Inherits="EXCELLENT.bussiness_trip_report_" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Register assembly="System.Web.Entity, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" namespace="System.Web.UI.WebControls" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <style type="text/css">
        #form1
        {
            width: 800px;
            height: 608px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:Button ID="btnsavepdf" runat="server" BorderStyle="None" Text="Save to PDF" 
                Font-Underline="True" ForeColor="#3333FF" BackColor="White" /><br />
    <input  type="hidden" id="param" value= "201211/T/000001" />
    <div id="savepdf" 
            style="position:absolute; right:10px; top:20px; height: 22px;">
            </div>
        <asp:TextBox ID="TextBox1" runat="server" Visible="false"></asp:TextBox>
    </div>
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
        Font-Size="8pt" Height="538px" Width="98%" style="margin-right: 0px" ShowExportControls="False" 
        ShowPrintButton="False">
        <LocalReport ReportPath="Laporan\Bussinestrip.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="SqlDataSource6" Name="DataRow" />
                <rsweb:ReportDataSource DataSourceId="SqlDataSource6" 
                    Name="DataWindowObjectEntry" />
                <rsweb:ReportDataSource DataSourceId="SqlDataSource6" 
                    Name="DataSet5_V_H001" />
                <rsweb:ReportDataSource DataSourceId="SqlDataSource6" Name="DataSet5_H_A101" />
                <rsweb:ReportDataSource DataSourceId="SqlDataSource6" Name="DataSet5_H_A150" />
                <rsweb:ReportDataSource DataSourceId="SqlDataSource6" Name="DataSet5_H_A130" />
                <rsweb:ReportDataSource DataSourceId="SqlDataSource6" Name="DataSet5_H_A1011" />
                <rsweb:ReportDataSource DataSourceId="SqlDataSource6" Name="DataSet5_V_H0011" />
                <rsweb:ReportDataSource DataSourceId="SqlDataSource1" 
                    Name="DataSet5_V_H00102" />
                <rsweb:ReportDataSource DataSourceId="SqlDataSource2" 
                    Name="DataSet5_V_H00106" />
                <rsweb:ReportDataSource DataSourceId="SqlDataSource3" Name="DataSet5_V_H0012" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
        ConnectionString="<%$ ConnectionStrings:jrn_citrixcon %>" 
        
        
        
        SelectCommand="SELECT TripNo, nik, detailTrip, detailTrip AS detailtrip2, (SELECT TOP (1) NIKSITE FROM H_A101 WHERE (Nik = V_H001.nik)) AS niksite, (SELECT TOP (1) Nama FROM H_A101 AS H_A101_6 WHERE (Nik = V_H001.nik)) AS nama, (SELECT TOP (1) NmDepar FROM H_A130 WHERE (KdDepar = V_H001.kddepar)) AS nmdepar, (SELECT TOP (1) NmJabat FROM H_A150 WHERE (KdJabat = V_H001.kdjabat)) AS nmjabat, costcode, (SELECT TOP (1) Nama FROM H_A101 AS H_A101_5 WHERE (Nik = V_H001.sup_nik)) AS superior, (SELECT TOP (1) DateFrom FROM V_H00104 WHERE (Tripno = V_H001.TripNo)) AS datefrom, (SELECT TOP (1) DateTo FROM V_H00104 AS V_H00104_7 WHERE (Tripno = V_H001.TripNo)) AS dateto, fcomAko, comAkoStart, comAkoEnd, purpose, tripfrom, tripto, tripday, fland, landname, (SELECT FlightName + '-' + FlightNo AS Expr1 FROM V_H00103 WHERE (GenID IN ((SELECT TOP (1) GenID FROM V_H00103 AS V_H00103_1 WHERE (Tripno = V_H001.TripNo)))) AND (Tripno = V_H001.TripNo)) AS 'Flight_Name', (SELECT HotelName FROM V_H00104 AS V_H00104_6 WHERE (GenID IN ((SELECT TOP (1) GenID FROM V_H00104 AS V_H00104_5 WHERE (Tripno = V_H001.TripNo)))) AND (Tripno = V_H001.TripNo)) AS 'hotelname', (SELECT DateFrom FROM V_H00104 AS V_H00104_4 WHERE (GenID IN ((SELECT TOP (1) GenID FROM V_H00104 AS V_H00104_3 WHERE (Tripno = V_H001.TripNo)))) AND (Tripno = V_H001.TripNo)) AS 'hdatefrom', (SELECT DateTo FROM V_H00104 AS V_H00104_2 WHERE (GenID IN ((SELECT TOP (1) GenID FROM V_H00104 AS V_H00104_1 WHERE (Tripno = V_H001.TripNo)))) AND (Tripno = V_H001.TripNo)) AS 'hdateto', reqby, reqdate, apprby, apprdate, reviewby, reviewdate, proceedby, proceeddate, (SELECT Nama FROM H_A101 AS H_A101_4 WHERE (Nik = V_H001.reqby)) AS req, (SELECT Nama FROM H_A101 AS H_A101_3 WHERE (Nik = V_H001.apprby)) AS appr, (SELECT Nama FROM H_A101 AS H_A101_2 WHERE (Nik = V_H001.reviewby)) AS review, (SELECT Nama FROM H_A101 AS H_A101_1 WHERE (Nik = V_H001.proceedby)) AS proceed, cashadvan, currcode, (SELECT H_A110.NmCompany FROM H_A110 INNER JOIN H_A120 ON H_A110.KdCompany = H_A120.KdCompany WHERE (H_A120.KdSite = V_H001.kdsite)) AS nmcompany, kdsite, (SELECT costName FROM H_A221 WHERE (costcode = V_H001.costcode)) AS costname, (SELECT NmDivisi FROM H_A140 WHERE (KdDivisi = (SELECT KdDivisi FROM H_A130 AS H_A130_1 WHERE (KdDepar = V_H001.kddepar)))) AS nmdivisi, dectotal FROM V_H001 WHERE (TripNo = @argtripno)">
        <SelectParameters>
            <asp:ControlParameter ControlID="TextBox1" Name="argtripno" 
                PropertyName="Text" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:jrn_citrixcon %>" 
        SelectCommand="SELECT GenID, Tripno, location, datein, dateout, remark FROM V_H00106 WHERE (Tripno = @argtrip)">
        <SelectParameters>
            <asp:ControlParameter ControlID="TextBox1" Name="argtrip" PropertyName="Text" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:jrn_citrixcon %>" 
        SelectCommand="SELECT * FROM [V_H00102] WHERE ([Tripno] = @Tripno)">
        <SelectParameters>
            <asp:ControlParameter ControlID="TextBox1" Name="Tripno" PropertyName="Text" 
                Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource6" runat="server" 
        ConnectionString="<%$ ConnectionStrings:jrn_citrixcon %>" 
        SelectCommand="NewSelectCommand" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="TextBox1" DefaultValue="" Name="argReg" 
                PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    </form>
</body>
</html>
