﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="SFPP.aspx.vb" Inherits="EXCELLENT.SFPP" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <ajaxToolkit:ToolkitScriptManager  ID="ToolkitScriptManager1" runat="server"/>
<asp:UpdatePanel id="MyUpdatePanel" runat="server">
 <ContentTemplate>
 <Table class="menu-table">
   <Tr>
      <td><a href="FPP.aspx">Create FPP</a></td>
      <td><a href="SFPP.aspx">Search FPP</a></td>
   </Tr>
 </Table>
 <br />
 <div>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
       <caption>
       CARI FORM PERBAIKAN DAN PENCEGAHAN
       </caption>
       <tr>
         <td>Tanggal</td>
         <td>
         <asp:TextBox ID="TglAwal" runat="server" class="input-box-slim"/>
         <img src="images/Calendar_scheduleHS.png" alt="search" id="Img2"/>
           <ajaxToolkit:CalendarExtender ID="TglAwal_CalendarExtender" runat="server" Enabled="True" PopupButtonID ="Img2" Format="dd/MM/yyyy"
              TargetControlID="TglAwal">
           </ajaxToolkit:CalendarExtender>
           &nbsp;-&nbsp;
            <asp:TextBox ID="TglAkhir" runat="server" class="input-box-slim"/>
         <img src="images/Calendar_scheduleHS.png" alt="search" id="Img1"/>
           <ajaxToolkit:CalendarExtender ID="TglAkhir_CalendarExtender" runat="server" Enabled="True" PopupButtonID ="Img1" Format="dd/MM/yyyy"
              TargetControlID="TglAkhir">
           </ajaxToolkit:CalendarExtender>
            <asp:Button ID="btnSearch" runat="server" Text="Submit" />
         </td>
       </tr>
       <tr>
       <td colspan="2">
          <asp:Repeater ID="rptSearchResult" runat="server">
            <HeaderTemplate>
                           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table-inside">
                           <tr>               
                                <th>No.</th>
                                <th>No FPP</th>
                                <th>Judul FPP</th>
                                <th>Tanggal</th>
                                <th>Delete</th> 
                                <th>Edit</th>                
                           </tr>
                        </HeaderTemplate>  
                        <ItemTemplate>
                       <tr>
                        <td><%# Container.ItemIndex + 1 %></td>
                        <td><%#DataBinder.Eval(Container.DataItem, "NoFPP")%></td>
                        <td><%#DataBinder.Eval(Container.DataItem, "NmFPP")%></td>
                        <td><%#FormatDate(DataBinder.Eval(Container.DataItem, "Tgl"))%></td>
                        <td align="center"><ASP:Button ID="btnDelete" Text="X" CommandName="Delete" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "NoFPP")%>'/></td>
                        <td align="center"><ASP:Button ID="btnEdit" Text="E" CommandName="Edit" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "NoFPP")%>'/></td>
                        </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                           </table>
                        </FooterTemplate>
          </asp:Repeater>
       </td>
       </tr>
  </table>
 </div>
 
 </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
