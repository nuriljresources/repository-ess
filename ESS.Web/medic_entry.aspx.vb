﻿Imports System.Data.SqlClient
Partial Public Class medic_entry
    Inherits System.Web.UI.Page
    Public js As String
    Public myDt As DataTable
    Public msg As String
    Public benefit As String
    Public kodeSite As String
    Dim lmedic As New medic
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnPaymentUsage.Visible = ESS.Common.SITE_SETTINGS.PRINT_PAYMENT_APP

        Dim qryheader As String
        Dim dt As DataTable
        Dim dtb As DataTable = New DataTable()
        Dim sqleditheader, sqleditdetail, sqlfindnama, terbilang As String
        Dim dtbeditmedic As DataTable = New DataTable()
        Dim dtbeditmedicd As DataTable = New DataTable()
        Dim dtbfindnama As DataTable = New DataTable()
        Dim lscount, ltotal As Integer
        Dim tglout As DateTime
        Dim tglin As DateTime
        Dim jmlhari As Double

        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        If Session("niksite") = Nothing Or Session("niksite") = "" Then Response.Redirect("login.aspx")

        msg = ""
        'Session("niksite") = "0001576"
        'terbilang = lmedic.f_amount_in_word("100000").ToString()

        If Not IsPostBack Then
            sqlConn.Open()
            If Session("medicnoregst").ToString = "Editinpatient" Then
                sqleditheader = "select *, (select nama from H_A101 where nik = H_H215.nik) as nama,(select nmsite from H_A120 where KdSite = H_H215.kdsite) as nmsite, (select niksite from H_A101 where nik=H_H215.nik) as niksite, (select nmjabat from H_A150 where kdjabat = (select kdjabatan from H_A101 where nik = H_H215.nik)) as nmjabat, (select kdjabatan from H_A101 where nik = H_H215.nik) as kdjabat, " & _
                "(SELECT  isnull(H_A21501.amount1  ,0) FROM     H_A101 a,  H_A21501, H_A21502 WHERE   ( H_A21501.nobuk = H_A21502.nobuk ) and ( a.kdlevel = H_A21502.kdlevel ) and ( H_A21501.stedit<>'2' ) AND ( H_A21501.TipeOutPatient='1') AND ( a.nik =  '" + Session("niksite") + "') and ( H_A21501.kdsite= a.kdsite)) as InapMax, fstatus from H_H215 where noreg = '" + Session("medicnoreg") + "'"
                Dim sdaeditheader As SqlDataAdapter = New SqlDataAdapter(sqleditheader, sqlConn)
                sdaeditheader.Fill(dtbeditmedic)
                If dtbeditmedic.Rows.Count > 0 Then
                    txtnoreg.Text = dtbeditmedic.Rows(0)!noreg.ToString
                    txtnmsite.Text = dtbeditmedic.Rows(0)!nmsite.ToString
                    hnmsite.Value = dtbeditmedic.Rows(0)!kdsite.ToString
                    txtnik.Text = dtbeditmedic.Rows(0)!niksite.ToString
                    hnik.Value = dtbeditmedic.Rows(0)!nik.ToString
                    txtlevel.Text = dtbeditmedic.Rows(0)!nmjabat.ToString
                    hkdlevel.Value = dtbeditmedic.Rows(0)!kdjabat.ToString
                    txtdate.Text = dtbeditmedic.Rows(0)!TglTrans.ToString
                    txtcostcode.Text = dtbeditmedic.Rows(0)!pycostcode.ToString
                    txtnama.Text = dtbeditmedic.Rows(0)!nama.ToString
                    txtremarks.Text = dtbeditmedic.Rows(0)!keterangan.ToString
                    txtoutpatientmax.Text = Format(dtbeditmedic.Rows(0)!InapMax, "#,##.00").ToString
                    benefit = Format(dtbeditmedic.Rows(0)!InapMax, "#,##.00").ToString
                    txtnoreg.Text = dtbeditmedic.Rows(0)!NoReg.ToString
                    kodeSite = hnmsite.Value

                    If dtbeditmedic.Rows(0)!fstatus = 1 Then
                        btnsave.Enabled = False
                        btndelete.Enabled = False
                    End If

                    If Session("kdsite") = "JKT" Or kodeSite = "JKT" Then
                        btnPaymentUsage.Enabled = True
                    Else
                        btnPaymentUsage.Enabled = False
                    End If

                    sqleditdetail = "select '' As No, NoReg, (convert(char(11),TglDet,101)) as TglDet, '' as TglDetOut, Person, '' as nama, CONVERT(DECIMAL(14,2),HrgKmr) As HrgKmr, '100' as PersenClaim,CONVERT(DECIMAL(14,2),ObatDll) as ObatDll,Hari,CONVERT(DECIMAL(14,2),BiayaObat) as BiayaObat,CONVERT(DECIMAL(14,2),BiayaKons) as BiayaKons,CONVERT(DECIMAL(14,2),BiayaLab) as BiayaLab,CONVERT(DECIMAL(14,2),BiayaLain) as BiayaLain,CONVERT(DECIMAL(14,2),BiayaTot) as BiayaTot,tDokter,JDokter,kdSpesialis,kdDiagnosa as diagnosa from H_H21500 where NoReg = '" + Session("medicnoreg") + "'"
                    Dim sdaeditdetail As SqlDataAdapter = New SqlDataAdapter(sqleditdetail, sqlConn)
                    sdaeditdetail.Fill(dtbeditmedicd)
                    If dtbeditmedicd.Rows.Count > 0 Then
                        For lscount = 0 To dtbeditmedicd.Rows.Count - 1
                            If dtbeditmedicd.Rows(lscount)!person.ToString = hnik.Value Then
                                sqlfindnama = "select nama from H_A101 where nik = '" + hnik.Value + "'"
                                Dim sdafindnama As SqlDataAdapter = New SqlDataAdapter(sqlfindnama, sqlConn)
                                dtbfindnama.Clear()
                                sdafindnama.Fill(dtbfindnama)
                                dtbeditmedicd.Rows(lscount)!nama = dtbfindnama.Rows(0)!nama.ToString
                            Else
                                sqlfindnama = "select familyname from H_A104 where nik = '" + hnik.Value + "' and familyid = '" + dtbeditmedicd.Rows(lscount)!person.ToString + "'"
                                Dim sdafindnama As SqlDataAdapter = New SqlDataAdapter(sqlfindnama, sqlConn)
                                dtbfindnama.Clear()
                                sdafindnama.Fill(dtbfindnama)
                                dtbeditmedicd.Rows(lscount)!nama = dtbfindnama.Rows(0)!familyname
                            End If
                            ltotal = ltotal + dtbeditmedicd(lscount)!biayatot
                            jmlhari = CDbl(dtbeditmedicd(lscount)!Hari.ToString)
                            tglin = dtbeditmedicd(lscount)!TglDet
                            tglout = tglin.AddDays(jmlhari - 1)
                            dtbeditmedicd(lscount)!TglDetOut = tglout.ToString("MM/dd/yyyy")
                        Next
                        Session("myDatatable") = dtbeditmedicd
                        fillgrid()
                        txttotal.Text = Format(ltotal, "#,##.00")
                    Else
                        dt = New DataTable
                        dt = lmedic.Fetch()

                        Session("myDatatable") = dt
                        fillgrid()
                    End If
                End If
            End If

            If Session("medicnoregst").ToString = "" Then
                qryheader = "select nik, KdSite, (select nmsite from H_A120 where KdSite = H_A101.kdsite) as nmsite,pycostcode,NIKSITE, Nama,KdJabatan, (select nmjabat from H_A150 where kdjabat = H_A101.KdJabatan) as nmjabat, " & _
                            "( SELECT  isnull(H_A21501.amount1  ,0) FROM     H_A101 a,  H_A21501, H_A21502 WHERE   ( H_A21501.nobuk = H_A21502.nobuk ) and ( H_A101.kdlevel = H_A21502.kdlevel ) and " & _
                            "( H_A21501.stedit<>'2' ) AND ( H_A21501.TipeOutPatient='1') AND ( H_A101.nik =  a.Nik) and ( H_A21501.kdsite= a.kdsite) ) as 'InapMax' " & _
                            "from H_A101 where nik = '" + Session("niksite") + "' and( H_A101.Active = '1' ) AND ( H_A101.StEdit <> '2' )"
                Dim sda As SqlDataAdapter = New SqlDataAdapter(qryheader, sqlConn)
                sda.Fill(dtb)

                If dtb.Rows.Count > 0 Then
                    If dtb.Rows(0)!InapMax.ToString = "" Then
                        dtb.Rows(0)!InapMax = "0"
                    End If
                    txtnmsite.Text = dtb.Rows(0)!nmsite.ToString
                    hnmsite.Value = dtb.Rows(0)!KdSite.ToString
                    txtcostcode.Text = dtb.Rows(0)!pycostcode.ToString
                    txtnik.Text = dtb.Rows(0)!NIKSITE.ToString
                    hnik.Value = dtb.Rows(0)!nik.ToString
                    txtnama.Text = dtb.Rows(0)!Nama.ToString
                    txtlevel.Text = dtb.Rows(0)!nmjabat.ToString
                    hkdlevel.Value = dtb.Rows(0)!KdJabatan.ToString
                    txtoutpatientmax.Text = Format(dtb.Rows(0)!InapMax, "#,##.00")
                    benefit = Format(dtb.Rows(0)!InapMax, "#,##.00")
                End If

                dt = New DataTable
                dt = lmedic.Fetch()

                Session("myDatatable") = dt
                fillgrid()
                btndelete.Visible = False
                btnprint.Enabled = False
                btnPaymentUsage.Enabled = False
            End If
            txtdate.Attributes.Add("readonly", "readonly")
            txtnmsite.Attributes.Add("readonly", "readonly")
            txtcostcode.Attributes.Add("readonly", "readonly")
            txtnik.Attributes.Add("readonly", "readonly")
            txtnama.Attributes.Add("readonly", "readonly")
            txtlevel.Attributes.Add("readonly", "readonly")
            txtoutpatientmax.Attributes.Add("readonly", "readonly")
            txttotal.Attributes.Add("readonly", "readonly")
            Session("medicnoreg") = ""
            Session("medicnoregst") = ""
        End If
        If Session("medicnoregst") = "" Then
            Try
                js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewdtout").ClientID + "', '%m/%d/%Y');</script>"
                cnt.Value = GridView1.FooterRow.FindControl("txtNewNama").ClientID
                cnt2.Value = GridView1.FooterRow.FindControl("hnewperson").ClientID
            Catch ex As Exception

            End Try
        End If
        txtdate.Text = Today.Date.ToString("MM/dd/yyyy")
    End Sub

    Private Sub fillgrid()
        Dim dt As DataTable = Session("myDatatable")

        If dt.Rows.Count > 0 Then
            If dt.Rows(0)!TglDet.ToString = "" Then
                GridView1.DataSource = dt
                GridView1.DataBind()

                Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                GridView1.Rows(0).Cells.Clear()
                GridView1.Rows(0).Cells.Add(New TableCell())
                GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                GridView1.Rows(0).Cells(0).Text = "Insert medical claim"
                GridView1.Rows(0).ForeColor = Drawing.Color.White
                Session("myDatatable") = dt
            Else
                GridView1.DataSource = dt
                GridView1.DataBind()
            End If
        Else
            dt.Rows.Add(dt.NewRow())
            GridView1.DataSource = dt
            GridView1.DataBind()

            Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
            GridView1.Rows(0).Cells.Clear()
            GridView1.Rows(0).Cells.Add(New TableCell())
            GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
            GridView1.Rows(0).Cells(0).Text = "Insert medical claim"
            GridView1.Rows(0).ForeColor = Drawing.Color.White
            Session("myDatatable") = dt
        End If
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
        If e.CommandName.Equals("AddNew") Then
            'Dim No As HiddenField = DirectCast(GridView1.FooterRow.FindControl("No"), HiddenField)
            Dim txtNewdt As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewdt"), TextBox)
            Dim txtnewdtout As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewdtout"), TextBox)
            Dim hnewperson As HiddenField = DirectCast(GridView1.FooterRow.FindControl("hnewperson"), HiddenField)
            Dim txtNewNama As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewNama"), TextBox)
            Dim txtNewHargaKamar As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewHargaKamar"), TextBox)
            Dim txtNewHari As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewHari"), TextBox)
            'Dim txtNewperclaim As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewperclaim"), TextBox)
            Dim txtNewbiayakons As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewbiayakons"), TextBox)
            Dim txtNewbiayaobat As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewbiayaobat"), TextBox)
            Dim txtNewbiayalain As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewbiayalain"), TextBox)
            Dim txtNewbiayalab As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewbiayalab"), TextBox)
            Dim txtNewbiayatot As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewbiayatot"), TextBox)
            'Dim txtNewdiagnosa As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewdiagnosa"), TextBox)
            Dim jmlhari As Double

            If txtNewHargaKamar.Text = "" Then txtNewHargaKamar.Text = "0"
            If txtNewHari.Text = "" Then txtNewHari.Text = "0"
            If txtNewbiayakons.Text = "" Then txtNewbiayakons.Text = "0"
            If txtNewbiayaobat.Text = "" Then txtNewbiayaobat.Text = "0"
            If txtNewbiayalain.Text = "" Then txtNewbiayalain.Text = "0"
            If txtNewbiayalab.Text = "" Then txtNewbiayalab.Text = "0"

            myDt = Session("myDatatable")

            Dim li As Integer
            For li = 0 To myDt.Rows.Count - 1
                If myDt.Rows(li)!TglDet.ToString = "" Then myDt.Rows(li)!TglDet = "1/1/1900"
                If CDate(myDt.Rows(li)!TglDet) = CDate(txtNewdt.Text) And myDt.Rows(li)!person.ToString = hnewperson.Value Then
                    msg = "<script type='text/javascript'> alert('Data detail already exist'); </script>"
                    refresh()
                    Return
                End If
            Next

            jmlhari = DateDiff("d", CDate(txtNewdt.Text), CDate(txtnewdtout.Text)) + 1
            If jmlhari < 1 Then
                msg = "<script type='text/javascript'> alert('Day can not 0'); </script>"
                refresh()
                Return
            End If

            If CDbl(txtNewHargaKamar.Text) < 0 Or CDbl(txtNewbiayakons.Text) < 0 Or CDbl(txtNewbiayaobat.Text) < 0 Or CDbl(txtNewbiayalain.Text) < 0 Or CDbl(txtNewbiayalab.Text) < 0 Then
                msg = "<script type='text/javascript'> alert('Medicine and other Expenses must not minus'); </script>"
                refresh()
                Return
            End If

            'If CDbl(txtNewHargaKamar.Text) > txtoutpatientmax.Text Then
            '    msg = "<script type='text/javascript'> alert('Room rate over limit maximum room rate is " + txtoutpatientmax.Text + "'); </script>"
            '    refresh()
            '    Return
            'End If

            Dim sqlcheckmedicmonth As String
            Dim dtb As DataTable = New DataTable
            Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
            Try
                sqlcheckmedicmonth = "select kdsite, medicmonth from  H_a120 where KdSite = '" + hnmsite.Value + "'"
                Dim sqlda As SqlDataAdapter = New SqlDataAdapter(sqlcheckmedicmonth, sqlConn)
                sqlda.Fill(dtb)

                If dtb.Rows.Count > 0 Then
                    Dim datenumberd As Double
                    Dim liBlnSlisih As Double
                    Dim medicmonth As String
                    medicmonth = dtb.Rows(0)!medicmonth.ToString
                    If medicmonth = "" Then medicmonth = 0

                    datenumberd = DateDiff("d", CDate(txtNewdt.Text), CDate(txtdate.Text))
                    datenumberd = datenumberd / 30
                    liBlnSlisih = Math.Round(datenumberd, 2)
                    If liBlnSlisih > CDbl(medicmonth) Then
                        If GridView1.Rows(0).Cells(0).Text = "Insert medical claim" Then
                            Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                            GridView1.Rows(0).Cells.Clear()
                            GridView1.Rows(0).Cells.Add(New TableCell())
                            GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                            GridView1.Rows(0).Cells(0).Text = "Insert medical claim"
                            GridView1.Rows(0).ForeColor = Drawing.Color.White
                        End If
                        msg = "<script type='text/javascript'> alert('Detail Receipt Date Claim Expired; over " + medicmonth + " month from header Claim Date'); </script>"
                        Return
                    End If
                End If
            Catch ex As Exception
                msg = "<script type='text/javascript'> alert('failed to check medic month'); </script>"
            End Try
            
            lmedic.Insert(txtNewdt.Text, txtnewdtout.Text, txtNewNama.Text, hnewperson.Value, txtNewHargaKamar.Text, txtNewHari.Text, "100", txtNewbiayakons.Text, txtNewbiayaobat.Text, txtNewbiayalain.Text, txtNewbiayalab.Text, txtNewbiayatot.Text, "", CType(Session("myDatatable"), DataTable))
            myDt = Session("myDatatable")
            fillgrid()

            Dim licount As Integer
            Dim ltotal As Decimal
            'txttotal.text = mydt.rows.count.tostring

            For licount = 0 To myDt.Rows.Count - 1
                ltotal = ltotal + myDt.Rows(licount)!biayatot
            Next

            txttotal.Text = Format(ltotal, "#,##.00")

            js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewdtout").ClientID + "', '%m/%d/%Y');</script>"
            cnt.Value = GridView1.FooterRow.FindControl("txtNewNama").ClientID
            cnt2.Value = GridView1.FooterRow.FindControl("hnewperson").ClientID
        End If
    End Sub

    Protected Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As GridViewEditEventArgs)
        GridView1.EditIndex = e.NewEditIndex
        fillgrid()
        cnt.Value = GridView1.Rows(e.NewEditIndex).FindControl("txtNama").ClientID
        cnt2.Value = GridView1.Rows(e.NewEditIndex).FindControl("hperson").ClientID
        'js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn2', '" + GridView1.Rows(e.NewEditIndex).ClientID + "_txtDate', '%m/%d/%Y');</script>"
        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn2', '" + GridView1.Rows(e.NewEditIndex).ClientID + "_txtDateinv', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewdtout").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn4', '" + GridView1.Rows(e.NewEditIndex).ClientID + "_txtDateout', '%m/%d/%Y');</script>"
    End Sub

    Protected Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As GridViewCancelEditEventArgs)
        GridView1.EditIndex = -1
        fillgrid()
        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y');</script>"
    End Sub

    Protected Sub GridView1_RowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs)
        Dim txtDateinv As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtDateinv"), TextBox)
        Dim txtDateout As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtDateout"), TextBox)
        Dim hperson As HiddenField = DirectCast(GridView1.Rows(e.RowIndex).FindControl("hperson"), HiddenField)
        Dim txtNama As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtNama"), TextBox)
        Dim txtHargaKamar As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtHargaKamar"), TextBox)
        Dim txtHari As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtHari"), TextBox)
        'Dim txtperclaim As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtperclaim"), TextBox)
        Dim txtbiayakons As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtbiayakons"), TextBox)
        Dim txtbiayaobat As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtbiayaobat"), TextBox)
        Dim txtbiayalain As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtbiayalain"), TextBox)
        Dim txtbiayalab As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtbiayalab"), TextBox)
        Dim txtbiayatot As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtbiayatot"), TextBox)
        'Dim txtdiagnosa As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtdiagnosa"), TextBox)
        Dim jmlhari As Double

        If txtHargaKamar.Text = "" Then txtHargaKamar.Text = "0"
        If txtHari.Text = "" Then txtHari.Text = "0"
        If txtbiayakons.Text = "" Then txtbiayakons.Text = "0"
        If txtbiayaobat.Text = "" Then txtbiayaobat.Text = "0"
        If txtbiayalain.Text = "" Then txtbiayalain.Text = "0"
        If txtbiayalab.Text = "" Then txtbiayalab.Text = "0"

        myDt = Session("myDatatable")

        Dim li As Integer
        For li = 0 To myDt.Rows.Count - 1
            If e.RowIndex <> li Then
                If myDt.Rows(li)!TglDet.ToString = "" Then myDt.Rows(li)!TglDet = "1/1/1900"
                If CDate(myDt.Rows(li)!TglDet.ToString) = CDate(txtDateinv.Text) And myDt.Rows(li)!person.ToString = hperson.Value Then
                    msg = "<script type='text/javascript'> alert('Data detail already exist'); </script>"
                    js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn2', '" + GridView1.Rows(e.RowIndex).ClientID + "_txtDateinv', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewdtout").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn4', '" + GridView1.Rows(e.RowIndex).ClientID + "_txtDateout', '%m/%d/%Y');</script>"
                    Return
                End If
            End If
        Next

        jmlhari = DateDiff("d", CDate(txtDateinv.Text), CDate(txtDateout.Text)) + 1
        If jmlhari < 1 Then
            msg = "<script type='text/javascript'> alert('Day can not 0'); </script>"
            js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn2', '" + GridView1.Rows(e.RowIndex).ClientID + "_txtDateinv', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewdtout").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn4', '" + GridView1.Rows(e.RowIndex).ClientID + "_txtDateout', '%m/%d/%Y');</script>"
            Return
        End If

        If CDbl(txtHargaKamar.Text) < 0 Or CDbl(txtbiayakons.Text) < 0 Or CDbl(txtbiayaobat.Text) < 0 Or CDbl(txtbiayalain.Text) < 0 Or CDbl(txtbiayalab.Text) < 0 Then
            msg = "<script type='text/javascript'> alert('Medicine and other Expenses must not minus'); </script>"
            js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn2', '" + GridView1.Rows(e.RowIndex).ClientID + "_txtDateinv', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewdtout").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn4', '" + GridView1.Rows(e.RowIndex).ClientID + "_txtDateout', '%m/%d/%Y');</script>"
            Return
        End If

        Dim sqlcheckmedicmonth As String
        Dim dtb As DataTable = New DataTable
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Try
            sqlcheckmedicmonth = "select kdsite, medicmonth from  H_a120 where KdSite = '" + hnmsite.Value + "'"
            Dim sqlda As SqlDataAdapter = New SqlDataAdapter(sqlcheckmedicmonth, sqlConn)
            sqlda.Fill(dtb)

            If dtb.Rows.Count > 0 Then
                Dim datenumberd As Double
                Dim liBlnSlisih As Double
                Dim medicmonth As String
                medicmonth = dtb.Rows(0)!medicmonth.ToString
                If medicmonth = "" Then medicmonth = 0

                datenumberd = DateDiff("d", txtDateinv.Text, txtdate.Text)
                datenumberd = datenumberd / 30
                liBlnSlisih = Math.Round(datenumberd, 2)
                If liBlnSlisih > CDbl(medicmonth) Then
                    js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn2', '" + GridView1.Rows(e.RowIndex).ClientID + "_txtDateinv', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewdtout").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn4', '" + GridView1.Rows(e.RowIndex).ClientID + "_txtDateout', '%m/%d/%Y');</script>"
                    msg = "<script type='text/javascript'> alert('Detail Receipt Date Claim Expired; over " + medicmonth + " month from header Claim Date'); </script>"
                    Return
                End If
            End If
        Catch ex As Exception
            msg = "<script type='text/javascript'> alert('failed to check medic month'); </script>"
        End Try

        lmedic.Update(txtDateinv.Text, txtDateout.Text, hperson.Value, txtNama.Text, txtHargaKamar.Text, txtHari.Text, "100", txtbiayakons.Text, txtbiayaobat.Text, txtbiayalain.Text, txtbiayalab.Text, txtbiayatot.Text, "", CType(Session("myDatatable"), DataTable), e.RowIndex)
        GridView1.EditIndex = -1
        fillgrid()

        Dim licount As Integer
        Dim ltotal As Decimal
        'txttotal.text = mydt.rows.count.tostring
        myDt = Session("myDatatable")
        For licount = 0 To myDt.Rows.Count - 1
            ltotal = ltotal + myDt.Rows(licount)!biayatot
        Next

        txttotal.Text = Format(ltotal, "#,##.00")
    End Sub

    Protected Sub GridView1_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)
        Dim ltotal As Double
        lmedic.Delete(CType(Session("myDatatable"), DataTable), e.RowIndex)
        fillgrid()
        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn3', '" + GridView1.FooterRow.FindControl("txtNewdtout").ClientID + "', '%m/%d/%Y');</script>"
        cnt.Value = GridView1.FooterRow.FindControl("txtNewNama").ClientID
        cnt2.Value = GridView1.FooterRow.FindControl("hnewperson").ClientID
        myDt = Session("myDatatable")
        For licount = 0 To myDt.Rows.Count - 1
            If myDt.Rows(licount)!biayatot.ToString = "" Then myDt.Rows(licount)!biayatot = 0
            ltotal = ltotal + myDt.Rows(licount)!biayatot
        Next

        txttotal.Text = Format(ltotal, "#,##.00")
    End Sub

    Private Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim mytable As DataTable = New DataTable
        Dim dtb As DataTable = New DataTable()
        Dim licount As Integer
        Dim sqlinsertheader, lkdsite, dtmnow, dtynow, stransno, transno, createdin, sqlinsertdetail, lshrgkamar As String
        Dim sqlupdateheader, sqlupdatedetail As String
        Dim cmdupdateheader As SqlCommand
        Dim cmdupdatedetail As SqlCommand
        Dim cmdinsertheader As SqlCommand
        Dim cmdinsertdetail As SqlCommand
        Dim sqleditheader As String
        Dim dtbeditmedic As DataTable = New DataTable()
        licount = 0

        'cek ulang inpatient user
        sqleditheader = "select nik, KdSite, (select nmsite from H_A120 where KdSite = H_A101.kdsite) as nmsite,pycostcode,NIKSITE, Nama,KdJabatan, (select nmjabat from H_A150 where kdjabat = H_A101.KdJabatan) as nmjabat, " & _
                            "( SELECT  isnull(H_A21501.amount1  ,0) FROM     H_A101 a,  H_A21501, H_A21502 WHERE   ( H_A21501.nobuk = H_A21502.nobuk ) and ( H_A101.kdlevel = H_A21502.kdlevel ) and " & _
                            "( H_A21501.stedit<>'2' ) AND ( H_A21501.TipeOutPatient='1') AND ( H_A101.nik =  a.Nik) and ( H_A21501.kdsite= a.kdsite) ) as 'InapMax' " & _
                            "from H_A101 where nik = '" + hnik.Value + "' and( H_A101.Active = '1' ) AND ( H_A101.StEdit <> '2' )"
        Dim sdaeditheader As SqlDataAdapter = New SqlDataAdapter(sqleditheader, conn)
        sdaeditheader.Fill(dtbeditmedic)

        If dtbeditmedic.Rows.Count > 0 Then
            txtoutpatientmax.Text = dtbeditmedic.Rows(0)!inapMax.ToString()
        End If

        mytable = Session("myDatatable")
        createdin = System.Net.Dns.GetHostName().ToString

        If txttotal.Text = "" Then txttotal.Text = 0
        If CDbl(txttotal.Text) <= 0 Then
            lblinfo.Text = "Total Outpatient claim value must not 0."
            msg = "<script type='text/javascript'> alert('Total Outpatient claim value must not 0'); </script>"
            refresh()
            Return
        End If

        If Session("kdsite").ToString = "" Then
            msg = "<script type='text/javascript'> alert('Cannot save data medic site code empty'); </script>"
            fillgrid()
            Return
        End If

        If mytable.Rows.Count > 0 Then
            If mytable.Rows(0)!TglDet.ToString = "" And mytable.Rows(0)!person.ToString = "" Then
                msg = "<script type='text/javascript'> alert('Cannot save data inpatient list empty'); </script>"
                fillgrid()
                Return
            Else
                If CDbl(txttotal.Text) <= 0 Then
                    lblinfo.Text = "Total Outpatient claim value must not 0."
                    msg = "<script type='text/javascript'> alert('Total Outpatient claim value must not 0'); </script>"
                    Return
                End If

                Try
                    conn.Open()
                    If txtnoreg.Text <> "Auto" Then
                        sqlupdateheader = "Update H_H215 set Keterangan = '" + txtremarks.Text + "', modifiedby = '" + Session("otorisasi").ToString + "', modifiedin = '" + createdin + "', modifiedtime = '" + DateTime.Today.ToString + "' where noreg = '" + txtnoreg.Text + "'"
                        cmdupdateheader = New SqlCommand(sqlupdateheader, conn)
                        cmdupdateheader.ExecuteScalar()

                        Dim ls_delquery As String = "Delete H_H21500 where noreg = '" + txtnoreg.Text + "'"
                        Dim ls_delcmd As SqlCommand
                        ls_delcmd = New SqlCommand(ls_delquery, conn)
                        ls_delcmd.ExecuteScalar()

                        For licount = 0 To mytable.Rows.Count - 1
                            lshrgkamar = mytable.Rows(licount)!hrgkmr.ToString
                            If CDbl(lshrgkamar) > CDbl(txtoutpatientmax.Text) Then
                                hprint.Value = "1"
                            Else
                                hprint.Value = "0"
                            End If
                            sqlinsertdetail = "Insert into H_H21500 (ID, NoReg, TglDet, Person, HrgKmr, PersenClaim, Hari, BiayaObat, BiayaKons, BiayaLab, BiayaLain, BiayaTot, kdDiagnosa) Values (NEWID(), '" + txtnoreg.Text + "', '" + mytable.Rows(licount)!TglDet.ToString + "', '" + mytable.Rows(licount)!Person.ToString + "', '" + mytable.Rows(licount)!hrgkmr.ToString + "', 1, '" + mytable.Rows(licount)!hari.ToString + "', '" + mytable.Rows(licount)!biayaobat.ToString + "', '" + mytable.Rows(licount)!biayakons.ToString + "', '" + mytable.Rows(licount)!biayalab.ToString + "', '" + mytable.Rows(licount)!biayalain.ToString + "', '" + mytable.Rows(licount)!biayatot.ToString + "', '" + mytable.Rows(licount)!diagnosa.ToString + "')"
                            cmdinsertdetail = New SqlCommand(sqlinsertdetail, conn)
                            cmdinsertdetail.ExecuteScalar()
                        Next

                        'For licount = 0 To mytable.Rows.Count - 1
                        '    sqlupdatedetail = "update H_H21500 set TglDet = '" + mytable.Rows(licount)!TglDet.ToString + "', Person = '" + mytable.Rows(licount)!Person.ToString + "', HrgKmr = '" + mytable.Rows(licount)!hrgkmr.ToString + "', Hari = '" + mytable.Rows(licount)!hari.ToString + "', BiayaObat = '" + mytable.Rows(licount)!biayaobat.ToString + "', BiayaKons = '" + mytable.Rows(licount)!biayakons.ToString + "', BiayaLab = '" + mytable.Rows(licount)!biayalab.ToString + "', BiayaLain = '" + mytable.Rows(licount)!biayalain.ToString + "',BiayaTot = '" + mytable.Rows(licount)!biayatot.ToString + "', kdDiagnosa = '" + mytable.Rows(licount)!diagnosa + "' where NoReg = '" + txtnoreg.Text + "'"
                        '    cmdupdatedetail = New SqlCommand(sqlupdatedetail, conn)
                        '    cmdupdatedetail.ExecuteScalar()
                        'Next
                        sqlupdateheader = "Update H_H215 set fprint = '" + hprint.Value + "' where noreg = '" + txtnoreg.Text + "'"
                        cmdupdateheader = New SqlCommand(sqlupdateheader, conn)
                        cmdupdateheader.ExecuteScalar()

                        If hprint.Value = "1" Then
                            btnprint.Enabled = False
                            btnPaymentUsage.Enabled = False
                            msg = "<script type='text/javascript'> alert('Information : Room rate over limit maximum room rate is " + txtoutpatientmax.Text + ". Please contact HR for further assistance'); </script>"
                            lblinfo.Text = "Cannot process room rate over limit maximum room rate is " + txtoutpatientmax.Text + ". Please contact HR for further assistance"
                        Else
                            btnprint.Enabled = True
                            If Session("kdsite") = "JKT" Or kodeSite = "JKT" Then
                                btnPaymentUsage.Enabled = True
                            End If
                            msg = "<script type='text/javascript'> alert('Data has been update'); </script>"
                        End If

                    Else
                        If txtnoreg.Text = "Auto" Then
                            dtmnow = DateTime.Now.ToString("MM")
                            dtynow = Date.Now.Year.ToString

                            stransno = Session("kdsite") + "/" + dtynow + "/" + dtmnow + "/" + "1" + "/"

                            Dim strcon As String = "select Noreg from H_H215 where Noreg like '%" + stransno + "%'"
                            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, conn)
                            sda.Fill(dtb)

                            If dtb.Rows.Count = 0 Then
                                transno = stransno + "000001"
                            ElseIf dtb.Rows.Count > 0 Then
                                Dim I As Integer = 0
                                I = dtb.Rows.Count + 1
                                If dtb.Rows.Count > 0 And dtb.Rows.Count < 9 Then
                                    transno = stransno + "00000" + I.ToString
                                ElseIf dtb.Rows.Count >= 9 And dtb.Rows.Count < 99 Then
                                    transno = stransno + "0000" + I.ToString
                                ElseIf dtb.Rows.Count >= 99 And dtb.Rows.Count < 999 Then
                                    transno = stransno + "000" + I.ToString
                                ElseIf dtb.Rows.Count >= 999 And dtb.Rows.Count < 9999 Then
                                    transno = stransno + "00" + I.ToString
                                ElseIf dtb.Rows.Count >= 9999 And dtb.Rows.Count < 99999 Then
                                    transno = stransno + "0" + I.ToString
                                ElseIf dtb.Rows.Count >= 99999 And dtb.Rows.Count < 999999 Then
                                    transno = stransno + I.ToString
                                ElseIf dtb.Rows.Count >= 999999 Then
                                    transno = "Error on generate Noreg"
                                End If
                            End If

                            sqlinsertheader = "Insert into H_H215 (NoReg, KdSite, TglTrans, Tipe, Nik, fstatus, Keterangan, CreatedBy, CreatedIn, CreatedTime, StEdit, pycostcode) Values ('" + transno + "', '" + hnmsite.Value + "', '" + DateTime.Today.ToString + "', 1, '" + hnik.Value + "', 0, '" + txtremarks.Text + "', '" + Session("otorisasi").ToString + "', '" + createdin + "', '" + DateTime.Today.ToString + "', 0,'" + txtcostcode.Text + "')"
                            cmdinsertheader = New SqlCommand(sqlinsertheader, conn)
                            cmdinsertheader.ExecuteScalar()

                            For licount = 0 To mytable.Rows.Count - 1
                                lshrgkamar = mytable.Rows(licount)!hrgkmr.ToString
                                If CDbl(lshrgkamar) > CDbl(txtoutpatientmax.Text) Then
                                    hprint.Value = "1"
                                Else
                                    hprint.Value = "0"
                                End If
                                sqlinsertdetail = "Insert into H_H21500 (ID, NoReg, TglDet, Person, HrgKmr, PersenClaim, Hari, BiayaObat, BiayaKons, BiayaLab, BiayaLain, BiayaTot, kdDiagnosa) Values (NEWID(), '" + transno + "', '" + mytable.Rows(licount)!TglDet.ToString + "', '" + mytable.Rows(licount)!Person.ToString + "', '" + mytable.Rows(licount)!hrgkmr.ToString + "', 1, '" + mytable.Rows(licount)!hari.ToString + "', '" + mytable.Rows(licount)!biayaobat.ToString + "', '" + mytable.Rows(licount)!biayakons.ToString + "', '" + mytable.Rows(licount)!biayalab.ToString + "', '" + mytable.Rows(licount)!biayalain.ToString + "', '" + mytable.Rows(licount)!biayatot.ToString + "', '" + mytable.Rows(licount)!diagnosa.ToString + "')"
                                cmdinsertdetail = New SqlCommand(sqlinsertdetail, conn)
                                cmdinsertdetail.ExecuteScalar()
                            Next
                            btnsave.Enabled = False
                            txtnoreg.Text = transno

                            sqlupdateheader = "Update H_H215 set fprint = '" + hprint.Value + "' where noreg = '" + txtnoreg.Text + "'"
                            cmdupdateheader = New SqlCommand(sqlupdateheader, conn)
                            cmdupdateheader.ExecuteScalar()

                            If hprint.Value = "1" Then
                                btnprint.Enabled = False
                                btnPaymentUsage.Enabled = False
                                msg = "<script type='text/javascript'> alert('Room rate over limit maximum room rate is " + txtoutpatientmax.Text + ". Please contact HR for further assistance'); </script>"
                                lblinfo.Text = "Information : Cannot process room rate over limit maximum room rate is " + txtoutpatientmax.Text + ". Please contact HR for further assistance"
                            Else
                                btnprint.Enabled = True
                                If Session("kdsite") = "JKT" Or kodeSite = "JKT" Then
                                    btnPaymentUsage.Enabled = True
                                End If
                                msg = "<script type='text/javascript'> alert('Data has been save'); </script>"
                            End If
                        Else
                            msg = "<script type='text/javascript'> alert('Failed to save data'); </script>"
                        End If
                    End If
                Catch ex As Exception
                    msg = "<script type='text/javascript'> alert('Failed to save data'); </script>"
                End Try
            End If
        Else
            msg = "<script type='text/javascript'> alert('Cannot save data inpatient list empty'); </script>"
        End If
    End Sub

    Private Sub btnprint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnprint.Click
        Session("printinpatient") = txtnoreg.Text
        Response.Redirect("cetak_inpatient.aspx?tipe=1")
    End Sub

    Private Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Dim sqlupdate As String
        Dim cmdupdate As SqlCommand
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

        Try
            conn.Open()
            sqlupdate = "update H_H215 set stedit = 2, deleteby = '" + Session("otorisasi").ToString + "', Deletetime = '" + DateTime.Today.ToString + "' where noreg = '" + txtnoreg.Text + "'"
            cmdupdate = New SqlCommand(sqlupdate, conn)
            cmdupdate.ExecuteScalar()

            Response.Redirect("medic_patientlist.aspx")
        Catch ex As Exception

        Finally
            conn.Close()
        End Try
        
    End Sub

    Public Sub refresh()
        If GridView1.Rows(0).Cells(0).Text = "Insert medical claim" Then
            Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
            GridView1.Rows(0).Cells.Clear()
            GridView1.Rows(0).Cells.Add(New TableCell())
            GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
            GridView1.Rows(0).Cells(0).Text = "Insert medical claim"
            GridView1.Rows(0).ForeColor = Drawing.Color.White
        End If
    End Sub

    Protected Sub btnPaymentUsage_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPaymentUsage.Click
        Dim user As ESS.DataAccess.EmployeeModel = ESS.DataAccess.UserLogic.GetUserByNIKSite(txtnik.Text)
        Session("payment_usage") = String.Format("{0}|{1}|{2}|{3}|{4}", txtnoreg.Text, txttotal.Text, "Inpatient Medical Claim", DateTime.Now.ToString("dd MMM yyyy"), user.Nik)

        msg = ""
        Response.Redirect("payment_application_internal_form.aspx")
    End Sub
End Class