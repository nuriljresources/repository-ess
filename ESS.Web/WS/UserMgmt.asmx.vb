﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports EXCELLENT.Excellent
Imports System.Data.SqlClient

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
<ScriptService()> _
Public Class UserMgmt1
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function

    <WebMethod()> _
    Public Function ChangePass(ByVal _nik As String, ByVal _oldpass As String, ByVal _newpass As String) As String
        Try
            Dim _adapter As New ExcellentTableAdapters.MUSERTableAdapter()
            Dim dt As New MUSERDataTable()
            Dim dr As MUSERRow
            _adapter.FillByNik(dt, _nik)
            If dt.Rows.Count = 0 Then
                Return "User Tidak Ditemukan"
            Else
                dr = dt.Rows(0)
                If SecureIt.Secure.Decrypt(dr.Password) <> _oldpass Then
                    Return "Password Lama Salah"
                Else
                    dr.Password = _newpass
                    _adapter.UpdatePassword(SecureIt.Secure.Encrypt(_newpass), HttpContext.Current.Request.UserHostName, HttpContext.Current.Request.UserHostName, Now, _nik)
                    Return "Update Berhasil"
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function
    <WebMethod()> _
    Public Function Addbagian(ByVal _nama As String, ByVal _kdbagian As String) As String
        Try
            'Dim _adapter As New ExcellentTableAdapters.MBIDANGTableAdapter()
            'Dim dt As New MBIDANGDataTable()
            'Dim dr As MBIDANGRow
            'dr = dt.NewMBIDANGRow()
            'dr.bidang_name = _nama
            'dr.bidang_code = _kdbagian

            'dt.AddMBIDANGRow(dr)

            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim sqlQuery As String
            Dim funcid As String = ""
            Dim secid As String = ""
            Dim cmd As SqlCommand
            conn.Open()

            sqlQuery = "insert into mbidang (bidang_name,bidang_code) values ('" & _nama & "','" & _kdbagian & "')"

            cmd = New SqlCommand(sqlQuery, conn)
            cmd.ExecuteScalar()
            conn.Close()
            Return "**Edit User Berhasil"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
    <WebMethod(True)> _
    Public Function AddTrip(ByVal _tripno As String, ByVal _site As String, ByVal _nik As String, ByVal _costcode As String, ByVal _snik As String, ByVal _detailtrip As String, ByVal _purpose As String, ByVal _tripday As String, ByVal _tripfrom As String, ByVal _tripto As String, ByVal _reqdate As String, ByVal _qry As String, ByVal _currcode As String, ByVal _cashadvan As String, ByVal _kdjabat As String, ByVal _kddepar As String, ByVal _akoqry As String, ByVal _cash As String, ByVal _createdby As String) As String
        Dim dtmnow As String
        Dim dtynow As String
        Dim stripno As String
        Dim i As Integer
        Dim tripno As String
        Dim conn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim Createdin As String
        Dim createdtime As String
        Session("tripno") = ""
        Createdin = System.Net.Dns.GetHostName()
        createdtime = Date.Now.ToString
        Try

            dtmnow = DateTime.Now.ToString("MM")
            dtynow = Date.Now.Year.ToString
            stripno = dtynow + dtmnow + "/" + "T" + "/"

            Dim dtb As DataTable = New DataTable()
            Dim strcon As String = "select TripNo from V_H001 where tripno like '%" + stripno + "%'"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, conn2)
            sda.Fill(dtb)

            If dtb.Rows.Count = 0 Then
                tripno = stripno + "000001"
            ElseIf dtb.Rows.Count > 0 Then
                i = 0
                i = dtb.Rows.Count + 1
                If dtb.Rows.Count > 0 And dtb.Rows.Count < 9 Then
                    tripno = stripno + "00000" + i.ToString
                ElseIf dtb.Rows.Count >= 9 And dtb.Rows.Count < 99 Then
                    tripno = stripno + "0000" + i.ToString
                ElseIf dtb.Rows.Count >= 99 And dtb.Rows.Count < 999 Then
                    tripno = stripno + "000" + i.ToString
                ElseIf dtb.Rows.Count >= 999 And dtb.Rows.Count < 9999 Then
                    tripno = stripno + "00" + i.ToString
                ElseIf dtb.Rows.Count >= 9999 And dtb.Rows.Count < 99999 Then
                    tripno = stripno + "0" + i.ToString
                ElseIf dtb.Rows.Count >= 99999 And dtb.Rows.Count < 999999 Then
                    tripno = stripno + i.ToString
                ElseIf dtb.Rows.Count >= 999999 Then
                    tripno = "Error on generate Tripnum"
                End If
            End If
            conn2.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


        Try
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
            Dim sqlQuery As String
            Dim sqlquery2 As String
            Dim akoquery As String
            Dim cmd As SqlCommand
            Dim cmd2 As SqlCommand
            Dim cmd3 As SqlCommand
            Dim ins As String
            Dim ins2 As String
            Dim ins3 As String
            Dim ins4 As String
            Dim chksess As String
            conn.Open()

            sqlQuery = "insert into V_H001 (tripNo, kdsite, nik, costcode, sup_nik, detailtrip, purpose, tripday, tripfrom, tripto, reqdate, stedit, currcode, cashadvan, fstatus, editGA, kddepar, kdjabat, fflight, fland, fhotel, fcomako, dectotal, decbalance, fconsult, consultype, reqby, createdby, createdin, createdtime) values ('" & tripno & "','" & _site & "', '" & _nik & "', '" & _costcode & "', '" & _snik & "', '" & _detailtrip & "', '" & _purpose & "', '" & _tripday & "', '" & _tripfrom & "', '" & _tripto & "', '" & _reqdate & "', '0', '" & _currcode & "', '" & _cashadvan & "', 0, 0, '" & _kddepar & "', '" & _kdjabat & "', 0, 0, 0, 0, '" & _cash & "' , 0, 0, '00', '" & _nik & "', '" & _createdby & "', '" & Createdin & "', '" & createdtime & "')"
            cmd = New SqlCommand(sqlQuery, conn)
            cmd.ExecuteScalar()

            If _qry = "null" Then
            Else
                ins = Replace(_qry, "+", " insert into V_H00102 (GenID,Tripno,dep_from, dep_date, arrive_to, arrive_date, remark, timeprefer, typetrans, TypeClass) values ")
                ins = Replace(ins, "||", tripno)
                ins2 = "insert into V_H00102 (GenID, Tripno,dep_from, dep_date, arrive_to, arrive_date, remark, timeprefer, typetrans, TypeClass) values " + ins
                sqlquery2 = ins2


                cmd2 = New SqlCommand(sqlquery2, conn)
                cmd2.ExecuteScalar()
            End If

            If _akoqry = "null" Then
            Else
                ins3 = Replace(_akoqry, "+", " insert into V_H00106 (genid, tripno, location, datein, dateout, remark) values ")
                ins3 = Replace(ins3, "||", tripno)
                ins4 = "insert into V_H00106 (genid, tripno, location, datein, dateout, remark) values " + ins3
                akoquery = ins4

                cmd3 = New SqlCommand(akoquery, conn)
                cmd3.ExecuteScalar()
            End If
            Session("tripno") = tripno
            chksess = Session("tripno")
            conn.Close()
            Return "**Entry Data Trip Berhasil"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
    <WebMethod(True)> _
        Public Function EditTrip(ByVal _tripno As String, ByVal _site As String, ByVal _nik As String, ByVal _costcode As String, ByVal _snik As String, ByVal _detailtrip As String, ByVal _purpose As String, ByVal _tripday As String, ByVal _tripfrom As String, ByVal _tripto As String, ByVal _reqdate As String, ByVal _qry As String, ByVal _akoqry As String, ByVal _cashadvan As String, ByVal _dtsts As String, ByVal _curcode As String, ByVal _cash As String, ByVal _detsts As String, ByVal _detsts2 As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim cmd2 As SqlCommand = conn.CreateCommand()
        Dim trans As SqlTransaction = Nothing
        Dim modifiedby As String
        Dim modifiedin As String
        Dim modifiedtime As String

        modifiedby = Session("otorisasi").ToString
        modifiedin = System.Net.Dns.GetHostName()
        modifiedtime = Date.Now.ToString("MM/dd/yyyy h:mm:ss")

        Try
            Dim sqlQuery As String
            Dim sqlquery2 As String
            Dim sqlquery3 As String
            Dim akoquery As String
            Dim cmd As SqlCommand
            Dim cmd3 As SqlCommand
            Dim ins As String
            Dim ins2 As String
            Dim ins3 As String
            Dim ins4 As String
            Dim rep As String
            Dim rep2 As String
            Dim i As Integer
            Dim chksess As String
            conn.Open()

            sqlQuery = "update V_H001 set detailtrip = '" & _detailtrip & "', purpose = '" & _purpose & "', cashadvan ='" & _cashadvan & "', tripfrom ='" & _tripfrom & "', tripto ='" & _tripto & "', tripday ='" & _tripday & "', sup_nik ='" & _snik & "', currcode ='" & _curcode & "', dectotal ='" & _cash & "', modifiedby = '" + modifiedby + "', modifiedin = '" + modifiedin + "', modifiedtime = '" + modifiedtime + "' where TripNo = '" & _tripno & "'"
            ', ByVal _tripfrom As String, ByVal _tripto As String
            'reqdate, , tripfrom, tripto
            ', '" & _tripfrom & "', '" & _tripto & "'
            cmd = New SqlCommand(sqlQuery, conn)
            cmd.ExecuteScalar()

            If _dtsts = 0 Then
                trans = conn.BeginTransaction()
                cmd2.Transaction = trans
                If _qry = "null" Then
                Else
                    If _detsts = 1 Then
                        If _qry = "null" Then
                        Else
                            cmd2.CommandText = "delete V_H00102 where tripno = '" + _tripno + "'"
                            cmd2.ExecuteNonQuery()

                            ins = Replace(_qry, "+", " insert into V_H00102 (GenID,Tripno,dep_from, dep_date, arrive_to, arrive_date, remark, timeprefer, typetrans, TypeClass) values ")
                            ins = Replace(ins, "||", _tripno)
                            ins2 = "insert into V_H00102 (GenID, Tripno,dep_from, dep_date, arrive_to, arrive_date, remark, timeprefer, typetrans, TypeClass) values " + ins
                            sqlquery2 = ins2

                            cmd2.CommandText = sqlquery2
                            cmd2.ExecuteNonQuery()
                        End If
                    Else
                        ins = Replace(_qry, "+", " update V_H00102 ")
                        ins2 = "update V_H00102 " + ins
                        rep = Replace(ins2, "@", "and tripno = '" & _tripno & "'")
                        sqlquery2 = rep

                        cmd2.CommandText = sqlquery2
                        cmd2.ExecuteNonQuery()
                        'cmd2 = New SqlCommand(sqlquery2, conn)
                        'cmd2.ExecuteScalar()
                    End If

                End If

                If _akoqry = "null" Then
                Else
                    If _detsts2 = 1 Then
                        cmd2.CommandText = "delete V_H00106 where tripno = '" + _tripno + "'"
                        cmd2.ExecuteNonQuery()

                        ins3 = Replace(_akoqry, "+", " insert into V_H00106 (genid, tripno, location, datein, dateout, remark) values ")
                        ins3 = Replace(ins3, "||", _tripno)
                        ins4 = "insert into V_H00106 (genid, tripno, location, datein, dateout, remark) values " + ins3
                        akoquery = ins4

                        cmd2.CommandText = akoquery
                        cmd2.ExecuteNonQuery()
                    Else
                        ins3 = Replace(_akoqry, "+", " update V_H00106 ")
                        ins4 = "update V_H00106 " + ins3
                        rep2 = Replace(ins4, "@", "and tripno = '" & _tripno & "'")
                        sqlquery3 = rep2

                        cmd2.CommandText = sqlquery3
                        cmd2.ExecuteNonQuery()
                    End If

                End If
                'cmd3 = New SqlCommand(sqlquery3, conn)
                'cmd3.ExecuteScalar()
            Else
                trans = conn.BeginTransaction()

                cmd2.Transaction = trans
                If _qry = "null" Then
                Else
                    cmd2.CommandText = "delete V_H00102 where tripno = '" + _tripno + "'"
                    cmd2.ExecuteNonQuery()

                    ins = Replace(_qry, "+", " insert into V_H00102 (GenID,Tripno,dep_from, dep_date, arrive_to, arrive_date, remark, timeprefer, typetrans, TypeClass) values ")
                    ins = Replace(ins, "||", _tripno)
                    ins2 = "insert into V_H00102 (GenID, Tripno,dep_from, dep_date, arrive_to, arrive_date, remark, timeprefer, typetrans, TypeClass) values " + ins
                    sqlquery2 = ins2

                    cmd2.CommandText = sqlquery2
                    cmd2.ExecuteNonQuery()
                End If

                If _akoqry = "null" Then
                Else
                    cmd2.CommandText = "delete V_H00106 where tripno = '" + _tripno + "'"
                    cmd2.ExecuteNonQuery()

                    ins3 = Replace(_akoqry, "+", " insert into V_H00106 (genid, tripno, location, datein, dateout, remark) values ")
                    ins3 = Replace(ins3, "||", _tripno)
                    ins4 = "insert into V_H00106 (genid, tripno, location, datein, dateout, remark) values " + ins3
                    akoquery = ins4

                    cmd2.CommandText = akoquery
                    cmd2.ExecuteNonQuery()
                End If
                'Session("tripno") = _tripno
                'chksess = Session("tripno")
            End If

            Session("tripno") = _tripno
            trans.Commit()
            Return "**Entry Data Trip Berhasil"
        Catch ex As Exception
            trans.Rollback()
            Return ex.Message
        Finally
            conn.Close()
        End Try
    End Function
    <WebMethod(True)> _
    Public Function Adddec(ByVal _decno As String, ByVal _site As String, ByVal _costcode As String, ByVal _tripno As String, ByVal _kddepar As String, ByVal _snik As String, ByVal _nik As String, ByVal _nmjabat As String, ByVal _fstatus As String, ByVal _nama As String, ByVal _decdate As String, ByVal _remark As String, ByVal _reqby As String, ByVal _reqbydt As String, ByVal _apprby As String, ByVal _apprdate As String, ByVal _reviewby As String, ByVal _reviewdate As String, ByVal _proceedby As String, ByVal _proceeddate As String, ByVal _qry As String) As String
        Dim decno As String
        Dim dtmnow As String
        Dim dtynow As String
        Dim stripno As String
        Dim i As Integer

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Try
            conn.Open()

            dtmnow = DateTime.Now.ToString("MM")
            dtynow = Date.Now.Year.ToString
            stripno = dtynow + dtmnow + "/" + "D" + "/"

            Dim dtb As DataTable = New DataTable()
            Dim strcon As String = "select Decno from V_H002 where Decno like '%" + stripno + "%'"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, conn)
            sda.Fill(dtb)

            If dtb.Rows.Count = 0 Then
                decno = stripno + "000001"
            ElseIf dtb.Rows.Count > 0 Then
                i = 0
                i = dtb.Rows.Count + 1
                If dtb.Rows.Count > 0 And dtb.Rows.Count < 9 Then
                    decno = stripno + "00000" + i.ToString
                ElseIf dtb.Rows.Count >= 9 And dtb.Rows.Count < 99 Then
                    decno = stripno + "0000" + i.ToString
                ElseIf dtb.Rows.Count >= 99 And dtb.Rows.Count < 999 Then
                    decno = stripno + "000" + i.ToString
                ElseIf dtb.Rows.Count >= 999 And dtb.Rows.Count < 9999 Then
                    decno = stripno + "00" + i.ToString
                ElseIf dtb.Rows.Count >= 9999 And dtb.Rows.Count < 99999 Then
                    decno = stripno + "0" + i.ToString
                ElseIf dtb.Rows.Count >= 99999 And dtb.Rows.Count < 999999 Then
                    decno = stripno + i.ToString
                ElseIf dtb.Rows.Count >= 999999 Then
                    decno = "Error on generate Tripnum"
                End If
            End If
            conn.Close()
        Catch ex As Exception

        End Try

        Try
            Dim conn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
            Dim sqlQuery As String
            Dim sqlquery2 As String
            Dim cmd As SqlCommand
            Dim cmd2 As SqlCommand
            Dim ins As String
            Dim ins2 As String
            conn2.Open()

            'sqlQuery = "insert into V_H002 (decno, tripno, transdate, remarks, reqby, reqdate, apprby, apprdate, reviewby, reviewdate, proceedby, proceeddate, createdby, modifiedby, modifiedtime, stedit, fstatus, nik, currcode, check_sup)"
            sqlQuery = "insert into V_H002 (decno, tripno, transdate, remarks, createdby, modifiedby, modifiedtime, stedit, fstatus, nik, currcode, check_sup, reqby, reqdate)"
            sqlQuery = sqlQuery + " Values ('" + decno + "', '" + _tripno + "', '" + _decdate + "', '" + _remark + "', '" + _nik + "', '" + _nik + "', '" + DateTime.Now + "', 0, 0, '" + _nik + "', 'IDR', '0', '" + _nik + "', '" + _decdate + "')"
            cmd = New SqlCommand(sqlQuery, conn2)
            cmd.ExecuteScalar()
            If _qry = "null" Then
            Else
                ins = Replace(_qry, "+", " insert into V_H00105 (genid,decno,decdate, expensetype, currency, subtotal, remarks) values ")
                ins = Replace(ins, "||", decno)
                ins2 = "insert into V_H00105 (genid,decno,decdate, expensetype, currency, subtotal, remarks) values " + ins
                sqlquery2 = ins2

                cmd2 = New SqlCommand(sqlquery2, conn2)
                cmd2.ExecuteScalar()
            End If
            
            Session("decno") = decno

            Dim strcon2 As String = String.Format("SELECT SUM(subtotal) as total FROM V_H00105 WHERE Decno = '{0}'", decno)

            Dim dtb2 As DataTable = New DataTable()
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, conn2)
            sda2.Fill(dtb2)
            Dim total As Integer

            If dtb2.Rows.Count > 0 Then
                total = Convert.ToInt32(dtb2.Rows(0)!total)
            End If

            Session("payment_usage") = String.Format("{0}|{1}|{2}|{3}|{4}", decno, String.Format("{0:0,0.00}", total), "Business Declaration", DateTime.Now, _nik)

            conn2.Close()
        Catch ex As Exception

        End Try
    End Function

    <WebMethod(True)> _
   Public Function Editdec(ByVal _decno As String, ByVal _site As String, ByVal _costcode As String, ByVal _tripno As String, ByVal _kddepar As String, ByVal _snik As String, ByVal _nik As String, ByVal _nmjabat As String, ByVal _fstatus As String, ByVal _nama As String, ByVal _decdate As String, ByVal _remark As String, ByVal _reqby As String, ByVal _reqbydt As String, ByVal _apprby As String, ByVal _apprdate As String, ByVal _reviewby As String, ByVal _reviewdate As String, ByVal _proceedby As String, ByVal _proceeddate As String, ByVal _qry As String, ByVal _dtsts As String, ByVal _detsts As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim cmd2 As SqlCommand = conn.CreateCommand()
        Dim trans As SqlTransaction = Nothing

        Try

            Dim sqlQuery As String
            Dim sqlQuery2 As String
            Dim cmd As SqlCommand

            Dim sqlQuerychk As String
            'Dim cmd2 As SqlCommand
            Dim ins As String
            Dim ins2 As String
            Dim rep As String
            Dim sqlQuerychk2 As String
            Dim dtb As DataTable = New DataTable
            Dim dtb2 As DataTable = New DataTable
            Dim dtbcheckV_H00105 = New DataTable
            Dim nominal As String

            conn.Open()
            sqlQuerychk = "SELECT b.Nominal FROM H_A101 a, V_H00107 b WHERE a.KdLevel = b.kdlevel AND a.Nik = '" + _nik + "'"
            Dim sdachk As SqlDataAdapter = New SqlDataAdapter(sqlQuerychk, conn)
            sdachk.Fill(dtb)

            If dtb.Rows.Count > 0 Then
                nominal = dtb.Rows(0)!Nominal.ToString

                'Return "nominal : " + nominal.ToString
            End If

            'sqlQuery = "update V_H002 set remarks = '" + _remark + "', reqdate = '" + _reqbydt + "', apprdate = '" + _apprdate + "', reviewdate = '" + _reviewdate + "', proceeddate = '" + _proceeddate + "' where decno = '" & _decno & "'"
            sqlQuery = "update V_H002 set remarks = '" + _remark + "' where decno = '" & _decno & "'"
            cmd = New SqlCommand(sqlQuery, conn)
            cmd.ExecuteScalar()

            If _dtsts = 0 Then
                trans = conn.BeginTransaction()
                cmd2.Transaction = trans
                If _qry = "null" Then
                Else
                    If _detsts = 1 Then
                        'trans = conn.BeginTransaction()
                        'cmd2.Transaction = trans
                        If _qry = "null" Then
                        Else
                            cmd2.CommandText = "delete V_H00105 where decno = '" + _decno + "'"
                            cmd2.ExecuteNonQuery()

                            ins = Replace(_qry, "+", " insert into V_H00105 (genid,decno,decdate, expensetype, currency, subtotal, remarks) values ")
                            ins = Replace(ins, "||", _decno)
                            ins2 = "insert into V_H00105 (genid,decno,decdate, expensetype, currency, subtotal, remarks) values " + ins
                            sqlQuery2 = ins2

                            cmd2.CommandText = sqlQuery2
                            cmd2.ExecuteNonQuery()
                        End If
                        Session("decno") = _decno
                    Else
                        ins = Replace(_qry, "+", " update V_H00105 ")
                        ins2 = "update V_H00105 " + ins
                        rep = Replace(ins2, "@%", "and decno = '" & _decno & "'")
                        sqlQuery2 = rep

                        cmd2.CommandText = sqlQuery2
                        cmd2.ExecuteNonQuery()
                    End If
                End If
            'conn.Close()
            Else
            trans = conn.BeginTransaction()
            cmd2.Transaction = trans
            If _qry = "null" Then
            Else
                cmd2.CommandText = "delete V_H00105 where decno = '" + _decno + "'"
                cmd2.ExecuteNonQuery()

                ins = Replace(_qry, "+", " insert into V_H00105 (genid,decno,decdate, expensetype, currency, subtotal, remarks) values ")
                ins = Replace(ins, "||", _decno)
                ins2 = "insert into V_H00105 (genid,decno,decdate, expensetype, currency, subtotal, remarks) values " + ins
                sqlQuery2 = ins2

                cmd2.CommandText = sqlQuery2
                cmd2.ExecuteNonQuery()
            End If
            Session("decno") = _decno

            'trans.Commit()
            End If
            'Dim tot As String
            'sqlQuerychk2 = "select sum(subtotal) as total from V_H00105 where Decno = '" + _decno + "'"
            'Dim cmd3 As New SqlCommand(sqlQuerychk2, conn)
            'Dim rdr As SqlDataReader = cmd.ExecuteReader()
            'While rdr.Read()
            '    tot = (Convert.ToString(rdr("total")))
            'End While
            'Dim sdachk2 As SqlDataAdapter = New SqlDataAdapter(sqlQuerychk2, conn)
            'sdachk2.Fill(dtb2)

            'If dtb2.Rows.Count > 0 Then
            'If tot > nominal Then
            '    trans.Rollback()
            '    Return "over quota"
            'Else
            '    trans.Commit()
            '    Return "ok"
            'End If
            'End If
            trans.Commit()
            Return "Data Has Been Successfully Saved"
        Catch ex As Exception
            trans.Rollback()
            Return "Failed To Save Data"
        Finally
            conn.Close()
        End Try
    End Function
    <WebMethod()> _
   Public Function AddUser(ByVal _nik As String, ByVal _nama As String, ByVal _kddepar As String, ByVal _kdcabang As String, ByVal _password As String, ByVal _grup As String, ByVal _akses As String, ByVal _flag As String, ByVal _divisi As String, ByVal _divisiid As String, ByVal _sectionid As String, ByVal _grupshe As String) As String
        Try
            Dim _adapter As New ExcellentTableAdapters.MUSERTableAdapter()
            Dim dt As New MUSERDataTable()
            Dim dr As MUSERRow
            dr = dt.NewMUSERRow()
            dr.NikUser = _nik
            dr.IdUser = _nik
            dr.NmUser = _nama
            dr.Departemen = _kddepar
            'dr.Departemen = ""
            'dr.Jobsite = _kdcabang
            dr.Jobsite = ""
            dr.Password = SecureIt.Secure.Encrypt(_password)
            dr.Grup = _grup
            dr.UpGrup = _akses
            dr.SHEGrup = _grupshe
            dr.StEdit = "0"
            'dr.Divisi = _divisi
            dr.Divisi = ""
            dt.AddMUSERRow(dr)

            If cekUser(_nik) = True Then
                'Jika NIK sudah ada maka ubah kembali status edit menjadi 0
                _flag = "edit"
            End If

            If _flag = "new" Then
                _adapter.Update(dt)
                If _grup = "4" Or _grup = "5" Or _grup = "7" Or _grup = "8" Or _grup = "1" Or _grup = "A" Then
                    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
                    Dim sqlQuery As String
                    Dim funcid As String = ""
                    Dim secid As String = ""
                    Dim cmd As SqlCommand
                    conn.Open()
                    If _grup = "1" Then
                        sqlQuery = "select a.ID as funcid,isnull(b.ID,'0') as secid from dbo.MFUNCTION a left join MSECTION b on a.ID=b.FuncID where a.FunctionName='Management System Development'"
                        Dim dreader As SqlDataReader
                        cmd = New SqlCommand(sqlQuery, conn)
                        dreader = cmd.ExecuteReader()
                        If dreader.HasRows Then
                            While dreader.Read
                                funcid = dreader.Item("funcid")
                                secid = dreader.Item("secid")
                            End While
                        End If
                        dreader.Close()
                        dreader = Nothing
                    Else
                        '    sqlQuery = "select a.ID as funcid,isnull(b.ID,'0') as secid from dbo.MFUNCTION a left join MSECTION b on a.ID=b.FuncID where a.FunctionName='" & _divisi & "'"
                        funcid = _divisiid
                        secid = _sectionid
                    End If
                    sqlQuery = "insert into BSCFUNCMAP values ('" & _nik & "','" & funcid & "','" & secid & "')"
                    cmd = New SqlCommand(sqlQuery, conn)
                    cmd.ExecuteScalar()
                    conn.Close()
                End If
                Return "**Penambahan User Berhasil"
            Else
                dt.AcceptChanges()
                dt.Rows(0).SetModified()
                _adapter.Update(dt)
                If _grup = "4" Or _grup = "5" Or _grup = "7" Or _grup = "8" Or _grup = "1" Or _grup = "A" Then
                    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
                    Dim sqlQuery As String
                    Dim funcid As String = ""
                    Dim secid As String = ""
                    Dim cmd As SqlCommand
                    conn.Open()
                    If _grup = "1" Then
                        sqlQuery = "select a.ID as funcid,isnull(b.ID,'0') as secid from dbo.MFUNCTION a left join MSECTION b on a.ID=b.FuncID where a.FunctionName='Management System Development'"
                        Dim dreader As SqlDataReader
                        cmd = New SqlCommand(sqlQuery, conn)
                        dreader = cmd.ExecuteReader()
                        If dreader.HasRows Then
                            While dreader.Read
                                funcid = dreader.Item("funcid")
                                secid = dreader.Item("secid")
                            End While
                        End If
                        dreader.Close()
                        dreader = Nothing
                    Else
                        '    sqlQuery = "select a.ID as funcid,isnull(b.ID,'0') as secid from dbo.MFUNCTION a left join MSECTION b on a.ID=b.FuncID where a.FunctionName='" & _divisi & "'"
                        funcid = _divisiid
                        secid = _sectionid
                    End If

                    Dim result As Integer = 0
                    ''cek dl data sebelumnya udah ada atau belum
                    sqlQuery = "select count(*) from BSCFUNCMAP where nik='" & _nik & "'"
                    cmd = New SqlCommand(sqlQuery, conn)
                    result = cmd.ExecuteScalar()
                    ''------------------------------------------
                    If result = 0 Then
                        sqlQuery = "insert into BSCFUNCMAP values ('" & _nik & "','" & funcid & "','" & secid & "')"
                    Else
                        sqlQuery = "UPDATE BSCFUNCMAP set bscdivisi=" & funcid & ",bscdepar=" & IIf(secid = "", "''", secid) & " where nik='" & _nik & "'"
                    End If
                    cmd = New SqlCommand(sqlQuery, conn)
                    cmd.ExecuteScalar()
                    conn.Close()
                End If
                Return "**Edit User Berhasil"
            End If
        Catch ex As SqlClient.SqlException
            Select Case ex.ErrorCode
                Case -2146232060
                    Return "**User sudah pernah didaftarkan"
                Case Else
                    Return ex.Message
            End Select
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Function cekUser(ByVal _nik As String) As Boolean
        Dim lada As Boolean = False
        Try
            Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
            Dim conn As New SqlConnection(arg)
            conn.Open()

            Dim squery As String = "SELECT NikUser FROM MUSER WHERE NikUser = '" + _nik + "'"
            Dim scom2 As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom2.ExecuteReader()

            If sdr.HasRows Then
                lada = True
            End If

            conn.Close()
            conn.Dispose()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return lada
    End Function

    <WebMethod()> _
   Public Function GetUser(ByVal _nik As String) As MyAccount
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString)
        Try
            Dim _adapter As New ExcellentTableAdapters.MUSERTableAdapter()
            Dim dt As New MUSERDataTable()
            Dim dr As MUSERRow
            _adapter.FillByNik(dt, _nik)
            dr = dt.Rows(0)
            Dim o As New MyAccount()
            o.Nik = dr.NikUser
            o.Nama = dr.NmUser
            o.Departemen = dr.Departemen
            o.Jobsite = dr.Jobsite
            o.Password = SecureIt.Secure.Decrypt(dr.Password)
            o.Grup = dr.Grup
            o.UpGrup = dr.UpGrup
            o.SheGrup = dr.SHEGrup
            'o.Divisi = dr.Divisi
            Dim query As String = "select * from bscfuncmap where nik='" & o.Nik & "'"
            Dim cmd As New SqlCommand(query, conn)
            Dim dreader As SqlDataReader
            conn.Open()
            dreader = cmd.ExecuteReader()
            If dreader.HasRows Then
                While dreader.Read
                    o.Divisi = dreader("bscdivisi")
                    o.Section = dreader("bscdepar")
                End While
            End If
            conn.Close()
            Return o
        Catch ex As SqlClient.SqlException
            Select Case ex.ErrorCode
                Case -2146232060
                    'Return "**User sudah pernah didaftarkan"
                Case Else
                    'Return ex.Message
            End Select
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            'Return ex.Message
        End Try
    End Function

    <WebMethod()> _
    Public Function getFunction() As List(Of MyFunction)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim query As String = "select ID,FunctionName from MFUNCTION where Active='1'  order by ID"
            Dim dr As SqlDataReader
            Dim cmd As New SqlCommand(query, conn)
            conn.Open()
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                Dim x As New List(Of MyFunction)
                Dim y As MyFunction
                While dr.Read
                    y = New MyFunction()
                    y.Id = dr("ID")
                    y.Nama = dr("FunctionName")
                    x.Add(y)
                End While
                conn.Close()
                Return x
            End If
        Catch ex As Exception
            conn.Close()
        End Try
    End Function

    <WebMethod()> _
   Public Function getSection(ByVal _funcid As String) As List(Of MyFunction)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim query As String = "select ID,SectionName from MSECTION where Active='1' order by ID"
            Dim dr As SqlDataReader
            Dim cmd As New SqlCommand(query, conn)
            conn.Open()
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                Dim x As New List(Of MyFunction)
                Dim y As MyFunction
                While dr.Read
                    y = New MyFunction()
                    y.Id = dr("ID")
                    y.Nama = dr("SectionName")
                    x.Add(y)
                End While
                conn.Close()
                Return x
            End If
        Catch ex As Exception
            conn.Close()
        End Try
    End Function

    <WebMethod()> _
   Public Function GetUserSignatureApproved(ByVal _nik As String) As List(Of MyAccount)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString)
        Try
            Dim _adapter As New ExcellentTableAdapters.MUSERTableAdapter()
            Dim dt As New MUSERDataTable()
            Dim dr As MUSERRow
            _adapter.FillByNik(dt, _nik)
            dr = dt.Rows(0)

            Dim oL As New List(Of MyAccount)

            Dim o As New MyAccount()
            o.Nik = dr.NikUser
            o.Nama = dr.NmUser
            o.Departemen = dr.Departemen
            o.Jobsite = dr.Jobsite
            o.Password = SecureIt.Secure.Decrypt(dr.Password)
            o.Grup = dr.Grup
            o.UpGrup = dr.UpGrup
            o.SheGrup = dr.SHEGrup
            'o.Divisi = dr.Divisi
            oL.Add(o)

            Dim query As String = "select * from bscfuncmap where nik='" & o.Nik & "'"
            Dim cmd As New SqlCommand(query, conn)
            Dim dreader As SqlDataReader
            conn.Open()
            dreader = cmd.ExecuteReader()
            If dreader.HasRows Then
                While dreader.Read
                    o.Divisi = dreader("bscdivisi")
                    o.Section = dreader("bscdepar")
                End While
            End If
            conn.Close()


            Return oL

        Catch ex As SqlClient.SqlException
            Select Case ex.ErrorCode
                Case -2146232060
                    'Return "**User sudah pernah didaftarkan"
                Case Else
                    'Return ex.Message
            End Select
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            'Return ex.Message
        End Try
    End Function


End Class

Public Class MyAccount
    Private _nik As String
    Private _nama As String
    Private _departemen As String
    Private _jobsite As String
    Private _password As String
    Private _grup As String
    Private _upGrup As String
    Private _sheGrup As String
    Private _divisi As String
    Private _section As String


    Public Property Departemen() As String
        Get
            Return _departemen
        End Get
        Set(ByVal value As String)
            _departemen = value
        End Set
    End Property
    Public Property Grup() As String
        Get
            Return _grup
        End Get
        Set(ByVal value As String)
            _grup = value
        End Set
    End Property
    Public Property Jobsite() As String
        Get
            Return _jobsite
        End Get
        Set(ByVal value As String)
            _jobsite = value
        End Set
    End Property
    Public Property Nama() As String
        Get
            Return _nama
        End Get
        Set(ByVal value As String)
            _nama = value
        End Set
    End Property
    Public Property Nik() As String
        Get
            Return _nik
        End Get
        Set(ByVal value As String)
            _nik = value
        End Set
    End Property
    Public Property Password() As String
        Get
            Return _password
        End Get
        Set(ByVal value As String)
            _password = value
        End Set
    End Property
    Public Property UpGrup() As String
        Get
            Return _upGrup
        End Get
        Set(ByVal value As String)
            _upGrup = value
        End Set
    End Property
    Public Property Divisi() As String
        Get
            Return _divisi
        End Get
        Set(ByVal value As String)
            _divisi = value
        End Set
    End Property
    Public Property Section() As String
        Get
            Return _section
        End Get
        Set(ByVal value As String)
            _section = value
        End Set
    End Property
    Public Property SheGrup() As String
        Get
            Return _sheGrup
        End Get
        Set(ByVal value As String)
            _sheGrup = value
        End Set
    End Property

End Class

Public Class MyFunction
    Private _id As String
    Private _nama As String
    Public Property Id() As String
        Get
            Return _id
        End Get
        Set(ByVal value As String)
            _id = value
        End Set
    End Property

    Public Property Nama() As String
        Get
            Return _nama
        End Get
        Set(ByVal value As String)
            _nama = value
        End Set
    End Property
    

    Sub New()

    End Sub

    Sub New(ByVal id As String, ByVal nama As String)
        Me._id = id
        Me._nama = nama
    End Sub

End Class