﻿Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports System.IO
Imports System.Net.Mail
Partial Public Class spl_approval
    Inherits System.Web.UI.Page
    Public tglaw As String
    Public th As String
    Public v1 As String
    Public v2 As String
    Public imonth As Integer
    Public js As String
    Public msg As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("niksite") = Nothing Or Session("niksite") = "" Then Response.Redirect("login.aspx")
        Dim baris As Integer

        If Not IsPostBack Then
            Dim a As String

            tglaw = Request(ddlbulan.UniqueID)
            th = Request(ddltahun.UniqueID)
            Dim i As Integer

            Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

            sqlConn.Open()
            Dim strcon As String = "select min(Tanggal) as min_date ,max(Tanggal) as max_date from H_H110"
            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            v1 = dtb.Rows(0)!min_date.ToString
            If v1.ToString = "" Then
                v1 = Today.Year.ToString
            Else
                v1 = CDate(v1).ToString("yyyy")
            End If

            v2 = dtb.Rows(0)!max_date.ToString
            If v2.ToString = "" Then
                v2 = Today.Year.ToString
            Else
                v2 = CDate(v2).ToString("yyyy")
            End If

            ddltahun.Items.Clear()
            For i = v1 To v2
                ddltahun.Items.Add(i)
            Next

            ddltahun.SelectedValue = (i - 1).ToString

            Select Case tglaw
                Case "January"
                    imonth = 1
                Case "February"
                    imonth = 2
                Case "March"
                    imonth = 3
                Case "April"
                    imonth = 4
                Case "May"
                    imonth = 5
                Case "June"
                    imonth = 6
                Case "July"
                    imonth = 7
                Case "August"
                    imonth = 8
                Case "September"
                    imonth = 9
                Case "October"
                    imonth = 10
                Case "November"
                    imonth = 11
                Case "December"
                    imonth = 12
                Case Else
                    imonth = 1
            End Select

            Try

                Dim dtb2 As DataTable = New DataTable()

                Dim strcon2 As String

                If Session("kdsite").ToString = "LAN" Or Session("kdsite").ToString = "BAK" Or Session("kdsite").ToString = "JKT" Or Session("kdsite").ToString = "PEN" Then
                    If Today.DayOfWeek = 1 Then
                        'strcon2 = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where checkapp1 = '" + Session("niksite") + "' and stedit = '1' and freject1 = '0' and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + " and jam_start >= '" + Today.Date.AddDays(-2) + "'"
                        strcon2 = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where (checkapp1 = '" + Session("niksite") + "' or checkapp2 = (select top 1 nikdel from H_H11002 where nikdel = '" + Session("niksite") + "' and  date_to >= '" + Today.Date + "' )) and stedit = '1' and freject1 = '0' and freject2 = '0' and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + " and jam_start >= '" + Today.Date.AddDays(-2) + "'"
                        strcon2 = strcon2 + " union "
                        strcon2 = strcon2 + "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where (app2 = (select top 1 nikreq from H_H11002 where nikdel = '" + Session("niksite") + "' and date_to >= '" + Today.Date + "') or app1 = (select top 1 nikreq from H_H11002 where nikdel = '" + Session("niksite") + "' and date_to >= '" + Today.Date + "')) and stedit = '1' and freject1 = '0' and freject2 = '0' and StEdit <> 2 and Jam_start >= '" + Today.Date.AddDays(-2) + "'"
                    Else
                        'strcon2 = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where checkapp1 = '" + Session("niksite") + "' and stedit = '1' and freject1 = '0' and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + " and jam_start >= '" + Today.Date.AddDays(-1) + "'"
                        strcon2 = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where (checkapp1 = '" + Session("niksite") + "' or checkapp2 = (select top 1 nikdel from H_H11002 where nikdel = '" + Session("niksite") + "' and  date_to >= '" + Today.Date + "' )) and stedit = '1' and freject1 = '0' and freject2 = '0' and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + " and jam_start >= '" + Today.Date.AddDays(-2) + "'"
                        strcon2 = strcon2 + " union "
                        strcon2 = strcon2 + "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where (app2 = (select top 1 nikreq from H_H11002 where nikdel = '" + Session("niksite") + "' and date_to >= '" + Today.Date + "') or app1 = (select top 1 nikreq from H_H11002 where nikdel = '" + Session("niksite") + "' and date_to >= '" + Today.Date + "')) and stedit = '1' and freject1 = '0' and freject2 = '0' and StEdit <> 2 and Jam_start >= '" + Today.Date.AddDays(-2) + "'"
                    End If
                Else
                    If Today.DayOfWeek = 1 Then
                        'strcon2 = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where checkapp1 = '" + Session("niksite") + "' and stedit = '1' and freject1 = '0' and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + " and jam_start >= '" + Today.Date.AddDays(-2) + "'"
                        strcon2 = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where (checkapp1 = '" + Session("niksite") + "' or checkapp2 = (select top 1 nikdel from H_H11002 where nikdel = '" + Session("niksite") + "' and  date_to >= '" + Today.Date + "' )) and stedit = '1' and freject1 = '0' and freject2 = '0' and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + " and jam_start >= '" + Today.Date.AddDays(-2) + "'"
                        strcon2 = strcon2 + " union "
                        strcon2 = strcon2 + "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where (app2 = (select top 1 nikreq from H_H11002 where nikdel = '" + Session("niksite") + "' and date_to >= '" + Today.Date + "') or app1 = (select top 1 nikreq from H_H11002 where nikdel = '" + Session("niksite") + "' and date_to >= '" + Today.Date + "')) and stedit = '1' and freject1 = '0' and freject2 = '0' and StEdit <> 2 and Jam_start >= '" + Today.Date.AddDays(-2) + "'"
                    Else
                        'strcon2 = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where checkapp1 = '" + Session("niksite") + "' and stedit = '1' and freject1 = '0' and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + " and jam_start >= '" + Today.Date.AddDays(-1) + "'"
                        strcon2 = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where (checkapp1 = '" + Session("niksite") + "' or checkapp2 = (select top 1 nikdel from H_H11002 where nikdel = '" + Session("niksite") + "' and  date_to >= '" + Today.Date + "' )) and stedit = '1' and freject1 = '0' and freject2 = '0' and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + " and jam_start >= '" + Today.Date.AddDays(-1) + "'"
                        strcon2 = strcon2 + " union "
                        strcon2 = strcon2 + "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where (app2 = (select top 1 nikreq from H_H11002 where nikdel = '" + Session("niksite") + "' and date_to >= '" + Today.Date + "') or app1 = (select top 1 nikreq from H_H11002 where nikdel = '" + Session("niksite") + "' and date_to >= '" + Today.Date + "')) and stedit = '1' and freject1 = '0' and freject2 = '0' and StEdit <> 2 and Jam_start >= '" + Today.Date.AddDays(-1) + "'"
                    End If
                End If

                'strcon2 = strcon2 + "where nik = '" + Session("niksite") + "' and StEdit <> 2 and month(TglTrans) = " + Today.Month.ToString + " and YEAR(TglTrans) = " + Today.Year.ToString + ""
                Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn)
                sda2.Fill(dtb2)

                For baris = 0 To dtb2.Rows.Count - 1
                    If dtb2.Rows(baris)!checkapp1.ToString = "" And dtb2.Rows(baris)!checkapp2.ToString = "" Then
                        dtb2.Rows(baris)!status = "Complete"
                    ElseIf dtb2.Rows(baris)!checkapp1.ToString <> "" And dtb2.Rows(baris)!checkapp2.ToString <> "" And dtb2.Rows(baris)!stedit.ToString = "1" Then
                        dtb2.Rows(baris)!status = "Approval 1"
                    ElseIf dtb2.Rows(baris)!checkapp1.ToString = "" And dtb2.Rows(baris)!checkapp2.ToString <> "" And dtb2.Rows(baris)!stedit.ToString = "1" Then
                        dtb2.Rows(baris)!status = "Approval 2"
                    ElseIf dtb2.Rows(baris)!checkapp1.ToString <> "" And dtb2.Rows(baris)!checkapp2.ToString <> "" And dtb2.Rows(baris)!stedit.ToString = "0" Then
                        dtb2.Rows(baris)!status = "Save"
                    End If
                Next
                GridView1.DataSource = dtb2
                GridView1.DataBind()
                ddlbulan.SelectedValue = Today.Month

            Catch ex As Exception
            Finally
                sqlConn.Close()
            End Try
        End If
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim baris As Integer
        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()

            Dim strcon As String

            If Today.DayOfWeek = 1 Then
                strcon = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 "
                strcon = strcon + "where (checkapp1 = '" + Session("niksite") + "' or checkapp2 = (select top 1 nikdel from H_H11002 where nikdel = '" + Session("niksite") + "' and  date_to >= '" + Today.Date + "' )) and freject1 = '0' and freject2 = '0' and stedit = '1' and StEdit <> 2 and month(Tanggal) = " + ddlbulan.SelectedValue.ToString + " and YEAR(Tanggal) = " + ddltahun.SelectedValue.ToString + " and jam_start >= '" + Today.Date.AddDays(-2) + "'"
                strcon = strcon + " union "
                strcon = strcon + "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where (app2 = (select top 1 nikreq from H_H11002 where nikdel = '" + Session("niksite") + "' and date_to >= '" + Today.Date + "') or app1 = (select top 1 nikreq from H_H11002 where nikdel = '" + Session("niksite") + "' and date_to >= '" + Today.Date + "')) and stedit = '1' and freject1 = '0' and freject2 = '0' and StEdit <> 2 and Jam_start >= '" + Today.Date.AddDays(-2) + "'"
            Else
                strcon = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 "
                strcon = strcon + "where (checkapp1 = '" + Session("niksite") + "' or checkapp2 = (select top 1 nikdel from H_H11002 where nikdel = '" + Session("niksite") + "' and  date_to >= '" + Today.Date + "' )) and freject1 = '0' and freject2 = '0' and stedit = '1' and StEdit <> 2 and month(Tanggal) = " + ddlbulan.SelectedValue.ToString + " and YEAR(Tanggal) = " + ddltahun.SelectedValue.ToString + " and jam_start >= '" + Today.Date.AddDays(-1) + "'"
                strcon = strcon + " union "
                strcon = strcon + "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where (app2 = (select top 1 nikreq from H_H11002 where nikdel = '" + Session("niksite") + "' and date_to >= '" + Today.Date + "') or app1 = (select top 1 nikreq from H_H11002 where nikdel = '" + Session("niksite") + "' and date_to >= '" + Today.Date + "')) and stedit = '1' and freject1 = '0' and freject2 = '0' and StEdit <> 2 and Jam_start >= '" + Today.Date.AddDays(-1) + "'"
            End If

            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)
            For baris = 0 To dtb.Rows.Count - 1
                If dtb.Rows(baris)!checkapp1.ToString = "" And dtb.Rows(baris)!checkapp2.ToString = "" Then
                    dtb.Rows(baris)!status = "Complete"
                ElseIf dtb.Rows(baris)!checkapp1.ToString <> "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "1" Then
                    dtb.Rows(baris)!status = "Approval 1"
                ElseIf dtb.Rows(baris)!checkapp1.ToString = "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "1" Then
                    dtb.Rows(baris)!status = "Approval 2"
                ElseIf dtb.Rows(baris)!checkapp1.ToString <> "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "0" Then
                    dtb.Rows(baris)!status = "Save"
                End If
            Next
            GridView1.DataSource = dtb
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging

        GridView1.PageIndex = e.NewPageIndex
        GridView1.DataBind()

    End Sub

    Private Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim drv As DataRowView = TryCast(e.Row.DataItem, DataRowView)
            If drv("status").ToString().Equals("Complete") Then
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#11FF66")
            ElseIf drv("status").ToString().Equals("Approval 1") Then
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF85")
            ElseIf drv("status").ToString().Equals("Approval 2") Then
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFE985")
            ElseIf drv("status").ToString().Equals("Save") Then
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#EDE8E8")
            Else
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF3B3B")
            End If
        End If
    End Sub

    Private Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridView1.RowEditing
        'Dim row As GridViewRow = e.NewEditIndex
        Dim dtbemail As DataTable = New DataTable
        Dim message As New MailMessage()
        Dim mail, niksite, nama, jam_start, jam_end, remarks, reqemail, app1nama, creemail As String
        'Session("splrecordid") = GridView1.DataKeys(row.RowIndex).Value.ToString
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim sqlinsert As String
        Dim cmdinsert As SqlCommand
        Dim ssubject As String
        Dim srejectby As String
        Try
            sqlConn.Open()


            Dim strcon As String = "select (select niksite from H_A101 where nik = H_H110.nik) as niksite, (select nama from H_A101 where nik = H_H110.nik) as nama, jam_start, jam_end, remarks, (select email from H_A101 where nik = H_H110.requestby) as reqemail, (select email from H_A101 where niksite = H_H110.createdby) as creemail, (select nama from H_A101 where nik = H_H110.app1) as app1nama, app1, app2, (select nama from H_A101 where nik = H_H110.app2) as app2nama from H_H110 where noreg = '" + GridView1.DataKeys(e.NewEditIndex).Value.ToString + "'"
            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtbemail)

            If dtbemail.Rows.Count > 0 Then
                If Trim(dtbemail.Rows(0)!app2.ToString) = Trim(Session("niksite").ToString) Then
                    sqlinsert = "update H_H110 set freject2 = 1, fdate2 = '" + DateTime.Now + "' where noreg = '" + GridView1.DataKeys(e.NewEditIndex).Value.ToString + "'"
                    ssubject = "Overtime Request Refusal App2 " + GridView1.DataKeys(e.NewEditIndex).Value.ToString
                    srejectby = dtbemail.Rows(0)!app2nama.ToString
                Else
                    sqlinsert = "update H_H110 set freject1 = 1, fdate1 = '" + DateTime.Now + "' where noreg = '" + GridView1.DataKeys(e.NewEditIndex).Value.ToString + "'"
                    ssubject = "Overtime Request Refusal App1 " + GridView1.DataKeys(e.NewEditIndex).Value.ToString
                    srejectby = dtbemail.Rows(0)!app1nama.ToString
                End If
            End If

            cmdinsert = New SqlCommand(sqlinsert, sqlConn)
            cmdinsert.ExecuteScalar()


            If dtbemail.Rows.Count > 0 Then
                reqemail = dtbemail.Rows(0)!reqemail.ToString
                niksite = dtbemail.Rows(0)!niksite.ToString
                nama = dtbemail.Rows(0)!nama.ToString
                jam_start = CDate(dtbemail.Rows(0)!jam_start).ToString("MM/dd/yyyy HH:mm")
                jam_end = CDate(dtbemail.Rows(0)!jam_end).ToString("MM/dd/yyyy HH:mm")
                remarks = dtbemail.Rows(0)!remarks.ToString
                app1nama = dtbemail.Rows(0)!app1nama.ToString
                creemail = dtbemail.Rows(0)!creemail.ToString
            End If

            message = New MailMessage()
            message.From = New MailAddress("noreply.overtime@jresources.com", "Overtime Request Refusal")
            message.Subject = ssubject
            message.IsBodyHtml = True
            message.Body = "Dear, <br><br>"
            message.Body = message.Body + "Overtime Requests: <br><br>"
            message.Body = message.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:10%'>NIK</th><th style='width:30%'>Name</th><th style='width:20%'>Time Start</th><th style='width:20%'>Time End</th><th style='width:20%'>Task</th>"
            message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" + niksite + "</td><td style='text-align:left;border:solid 1px'>" + nama + "</td><td style='text-align:center;border:solid 1px'>" + jam_start + "</td><td style='text-align:center;border:solid 1px'>" + jam_end + "</td><td style='text-align:left;border:solid 1px'>" + remarks + "</td></tr></table>"
            message.Body = message.Body + "<br>Is Rejected by : <strong>" + srejectby + "</strong>"

            message.Body = message.Body + "<br><br> Regards, <br><br> <strong> </strong>"

            If reqemail = "" And creemail = "" Then
                message.To.Add("syam.kharisman@jresources.com")
            ElseIf reqemail = "" Then
                message.To.Add(creemail)
            ElseIf creemail = "" Then
                message.To.Add(reqemail)
            Else
                message.To.Add(reqemail)
                message.To.Add(creemail)
            End If

            Dim smtp As New SmtpClient()
            'smtp.Host = "smtp.gmail.com"
            smtp.Host = "webmail.jresources.com"
            'smtp.Port = 587
            smtp.Port = 25
            smtp.EnableSsl = True
            'smtp.Credentials = New System.Net.NetworkCredential("noreply.overtime@jresources.com", "jrec54321")
            smtp.Credentials = New System.Net.NetworkCredential("noreply.overtime@jresources.com", "jrec12345")

            smtp.Send(message)

            refresh()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        If GridView1.SelectedRow Is Nothing Then Exit Sub

        Dim row As GridViewRow = GridView1.SelectedRow
        Dim message As New MailMessage()
        Dim mail, niksite, nama, jam_start, jam_end, remarks As String
        Dim dtbemail As DataTable = New DataTable
        Dim dtbemail2 As DataTable = New DataTable
        Dim dtbchkot As DataTable = New DataTable
        Dim dtbchkotact As DataTable = New DataTable
        Dim dtbchkotact2 As DataTable = New DataTable
        'Session("splrecordid") = GridView1.DataKeys(row.RowIndex).Value.ToString
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim sqlinsert As String
        Dim cmdinsert As SqlCommand
        Dim sqlchkfot As String
        Dim sqlchkfotact As String
        Dim sqlchkfotact2 As String
        Dim i As Integer
        Dim tot, fotact, gt As Double

        tot = 0
        fotact = 0
        gt = 0

        Dim shari As String
        Dim otactuala As String
        Dim otactualb As String
        Dim otnik As String

        shari = Today.Day.ToString

        Try
            sqlConn.Open()

            sqlchkfotact = "select splactual, kdsite, nik from H_H110 where noreg = '" + GridView1.DataKeys(row.RowIndex).Value.ToString + "'"
            Dim sdachkotact As SqlDataAdapter = New SqlDataAdapter(sqlchkfotact, sqlConn)
            sdachkotact.Fill(dtbchkotact)
            
            If dtbchkotact.Rows.Count > 0 Then
                otnik = dtbchkotact.Rows(0)!nik.ToString
            End If

            If dtbchkotact.Rows(0)!kdsite = "PEN" Then
                If Today.Month <> 12 And Today.Month <> 1 Then
                    If CDbl(shari) > 15 Then
                        sqlchkfot = "select (select nfixedot from h_h104 where KdHari=H_H105.kdhari and shift=H_H105.shift and KdSite=H_H105.kdsite) as nfixot,* from H_H105 where TglSchedule between '" + Today.Year.ToString + "-" + Today.Month.ToString + "-" + "16" + "' and '" + Today.Year.ToString + "-" + Today.AddMonths(1).Month.ToString + "-" + "15" + "' and Nik=(select nik from H_H110 where noreg = '" + GridView1.DataKeys(row.RowIndex).Value.ToString + "') and StEdit <> '2' and KdAbsen <> 'DO'"
                        otactuala = Today.Year.ToString + "-" + Today.Month.ToString + "-" + "16"
                        otactualb = Today.Year.ToString + "-" + Today.AddMonths(1).Month.ToString + "-" + "15"
                    Else
                        sqlchkfot = "select (select nfixedot from h_h104 where KdHari=H_H105.kdhari and shift=H_H105.shift and KdSite=H_H105.kdsite) as nfixot,* from H_H105 where TglSchedule between '" + Today.Year.ToString + "-" + Today.AddMonths(-1).Month.ToString + "-" + "16" + "' and '" + Today.Year.ToString + "-" + Today.Month.ToString + "-" + "15" + "' and Nik=(select nik from H_H110 where noreg = '" + GridView1.DataKeys(row.RowIndex).Value.ToString + "') and StEdit <> '2' and KdAbsen <> 'DO'"
                        otactuala = Today.Year.ToString + "-" + Today.AddMonths(-1).Month.ToString + "-" + "16"
                        otactualb = Today.Year.ToString + "-" + Today.Month.ToString + "-" + "15"
                    End If
                Else
                    If Today.Month = 1 Then
                        If CDbl(shari) > 15 Then
                            sqlchkfot = "select (select nfixedot from h_h104 where KdHari=H_H105.kdhari and shift=H_H105.shift and KdSite=H_H105.kdsite) as nfixot,* from H_H105 where TglSchedule between '" + Today.Year.ToString + "-" + Today.Month.ToString + "-" + "16" + "' and '" + Today.Year.ToString + "-" + Today.AddMonths(1).Month.ToString + "-" + "15" + "' and Nik=(select nik from H_H110 where noreg = '" + GridView1.DataKeys(row.RowIndex).Value.ToString + "') and StEdit <> '2' and KdAbsen <> 'DO'"
                            otactuala = Today.Year.ToString + "-" + Today.Month.ToString + "-" + "16"
                            otactualb = Today.Year.ToString + "-" + Today.AddMonths(1).Month.ToString + "-" + "15"
                        Else
                            sqlchkfot = "select (select nfixedot from h_h104 where KdHari=H_H105.kdhari and shift=H_H105.shift and KdSite=H_H105.kdsite) as nfixot,* from H_H105 where TglSchedule between '" + Today.AddYears(-1).Year.ToString + "-" + Today.AddMonths(-1).Month.ToString + "-" + "16" + "' and '" + Today.Year.ToString + "-" + Today.Month.ToString + "-" + "15" + "' and Nik=(select nik from H_H110 where noreg = '" + GridView1.DataKeys(row.RowIndex).Value.ToString + "') and StEdit <> '2' and KdAbsen <> 'DO'"
                            otactuala = Today.AddYears(-1).Year.ToString + "-" + Today.AddMonths(-1).Month.ToString + "-" + "16"
                            otactualb = Today.Year.ToString + "-" + Today.Month.ToString + "-" + "15"
                        End If
                    Else
                        If Today.Month = 12 Then
                            If CDbl(shari) > 15 Then
                                sqlchkfot = "select (select nfixedot from h_h104 where KdHari=H_H105.kdhari and shift=H_H105.shift and KdSite=H_H105.kdsite) as nfixot,* from H_H105 where TglSchedule between '" + Today.Year.ToString + "-" + Today.Month.ToString + "-" + "16" + "' and '" + Today.AddYears(1).Year.ToString + "-" + Today.AddMonths(1).Month.ToString + "-" + "15" + "' and Nik=(select nik from H_H110 where noreg = '" + GridView1.DataKeys(row.RowIndex).Value.ToString + "') and StEdit <> '2' and KdAbsen <> 'DO'"
                                otactuala = Today.Year.ToString + "-" + Today.Month.ToString + "-" + "16"
                                otactualb = Today.AddYears(1).Year.ToString + "-" + Today.AddMonths(1).Month.ToString + "-" + "15"
                            Else
                                sqlchkfot = "select (select nfixedot from h_h104 where KdHari=H_H105.kdhari and shift=H_H105.shift and KdSite=H_H105.kdsite) as nfixot,* from H_H105 where TglSchedule between '" + Today.Year.ToString + "-" + Today.AddMonths(-1).Month.ToString + "-" + "16" + "' and '" + Today.Year.ToString + "-" + Today.Month.ToString + "-" + "15" + "' and Nik=(select nik from H_H110 where noreg = '" + GridView1.DataKeys(row.RowIndex).Value.ToString + "') and StEdit <> '2' and KdAbsen <> 'DO'"
                                otactuala = Today.Year.ToString + "-" + Today.AddMonths(-1).Month.ToString + "-" + "16"
                                otactualb = Today.Year.ToString + "-" + Today.Month.ToString + "-" + "15"
                            End If
                        End If
                    End If
                End If

                Dim sdachkot As SqlDataAdapter = New SqlDataAdapter(sqlchkfot, sqlConn)
                sdachkot.Fill(dtbchkot)

                If dtbchkot.Rows.Count > 0 Then
                    For i = 0 To dtbchkot.Rows.Count - 1
                        tot = tot + CDbl(dtbchkot.Rows(i)!nfixot)
                    Next
                End If

                sqlchkfotact2 = "select top 1 (select sum(SPLActual) from H_H110 a where a.nik = H_H110.nik and a.Jam_start between '" + otactuala + "' and '" + otactualb + "') as ottotal from H_H110 where nik = '" + otnik + "' and Jam_start between '" + otactuala + "' and '" + otactualb + "' and stedit = '3'"
                Dim sdachkotact2 As SqlDataAdapter = New SqlDataAdapter(sqlchkfotact2, sqlConn)
                sdachkotact2.Fill(dtbchkotact2)

                If dtbchkotact2.Rows.Count > 0 Then
                    fotact = CDbl(dtbchkotact2.Rows(0)!ottotal)
                End If

                gt = fotact + tot

                If gt <= 6240 Then
                    Dim strcon As String = "select app2, (select niksite from H_A101 where nik = H_H110.nik) as niksite, (select nama from H_A101 where nik = H_H110.nik) as nama, jam_start, jam_end, remarks, (select email from H_A101 where nik = H_H110.app2) as email from H_H110 where noreg = '" + GridView1.DataKeys(row.RowIndex).Value.ToString + "'"
                    Dim dtb As DataTable = New DataTable
                    Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
                    sda.Fill(dtbemail)

                    If dtbemail.Rows.Count > 0 Then
                        If Trim(dtbemail.Rows(0)!app2.ToString) = Trim(Session("niksite").ToString) Then
                            sqlinsert = "update H_H110 set checkapp2 = '', stedit = '3', fdate2 = '" + DateTime.Now + "' where noreg = '" + GridView1.DataKeys(row.RowIndex).Value.ToString + "'"
                        Else
                            sqlinsert = "update H_H110 set checkapp1 = '', fdate1 = '" + DateTime.Now + "' where noreg = '" + GridView1.DataKeys(row.RowIndex).Value.ToString + "'"
                        End If
                    End If

                    cmdinsert = New SqlCommand(sqlinsert, sqlConn)
                    cmdinsert.ExecuteScalar()
                    'msg = "<script type='text/javascript'> alert('Data has been submit email has been sent'); </script>"
                    If Trim(dtbemail.Rows(0)!app2.ToString) = Trim(Session("niksite").ToString) Then
                        refresh()
                    Else
                        If dtbemail.Rows.Count > 0 Then
                            mail = dtbemail.Rows(0)!email.ToString
                            niksite = dtbemail.Rows(0)!niksite.ToString
                            nama = dtbemail.Rows(0)!nama.ToString
                            jam_start = CDate(dtbemail.Rows(0)!jam_start).ToString("MM/dd/yyyy HH:mm")
                            jam_end = CDate(dtbemail.Rows(0)!jam_end).ToString("MM/dd/yyyy HH:mm")
                            remarks = dtbemail.Rows(0)!remarks.ToString
                        End If

                        message = New MailMessage()
                        message.From = New MailAddress("noreply.overtime@jresources.com", "Overtime Request Approval")
                        message.Subject = "Overtime Request Reminder " + GridView1.DataKeys(row.RowIndex).Value.ToString
                        message.IsBodyHtml = True
                        message.Body = "Dear, <br><br>"
                        message.Body = message.Body + "Please do Overtime Requests Approval for the following : <br><br>"
                        message.Body = message.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:10%'>NIK</th><th style='width:30%'>Name</th><th style='width:20%'>Time Start</th><th style='width:20%'>Time End</th><th style='width:20%'>Task</th>"
                        message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" + niksite + "</td><td style='text-align:left;border:solid 1px'>" + nama + "</td><td style='text-align:center;border:solid 1px'>" + jam_start + "</td><td style='text-align:center;border:solid 1px'>" + jam_end + "</td><td style='text-align:left;border:solid 1px'>" + remarks + "</td></tr></table>"
                        message.Body = message.Body + "<br>To Approval please go to http://portal.jresources.com"

                        message.Body = message.Body + "<br><br> Regards, <br><br> <strong>Admin</strong>"

                        message.To.Add(mail)
                        Dim smtp As New SmtpClient()
                        smtp.Host = "webmail.jresources.com"
                        smtp.Port = 587
                        smtp.EnableSsl = True
                        smtp.Credentials = New System.Net.NetworkCredential("noreply.overtime@jresources.com", "jrec12345")

                        smtp.Send(message)

                        msg = "<script type='text/javascript'> alert('Data has been save'); </script>"
                        refresh()
                    End If
                Else
                    msg = "<script type='text/javascript'> alert('Cannot process : Overtime is over 104 Hours'); </script>"
                End If
            Else

                Dim strcon As String = "select app2, (select niksite from H_A101 where nik = H_H110.nik) as niksite, (select nama from H_A101 where nik = H_H110.nik) as nama, jam_start, jam_end, remarks, (select email from H_A101 where nik = H_H110.app2) as email from H_H110 where noreg = '" + GridView1.DataKeys(row.RowIndex).Value.ToString + "'"
                Dim dtb As DataTable = New DataTable
                Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
                sda.Fill(dtbemail)

                Dim app2 As String = Trim(dtbemail.Rows(0)!app2.ToString)
                Dim app2Del As String = String.Empty


                Dim strcon2 As String = "SELECT nikdel FROM H_H11002 WHERE nikreq = '" + app2 + "' AND GETDATE() BETWEEN date_from AND date_to"
                Dim dtb2 As DataTable = New DataTable
                Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn)
                sda2.Fill(dtbemail2)

                
                If dtbemail2.Rows.Count > 0 Then
                    app2Del = Trim(dtbemail2.Rows(0)!nikdel.ToString)
                End If

                If dtbemail.Rows.Count > 0 Then
                    If app2 = Trim(Session("niksite").ToString) Or app2Del = Trim(Session("niksite").ToString) Then
                        sqlinsert = "update H_H110 set checkapp2 = '', stedit = '3', fdate2 = '" + DateTime.Now + "' where noreg = '" + GridView1.DataKeys(row.RowIndex).Value.ToString + "'"
                    Else
                        sqlinsert = "update H_H110 set checkapp1 = '', fdate1 = '" + DateTime.Now + "' where noreg = '" + GridView1.DataKeys(row.RowIndex).Value.ToString + "'"
                    End If
                End If

                cmdinsert = New SqlCommand(sqlinsert, sqlConn)
                cmdinsert.ExecuteScalar()
                'msg = "<script type='text/javascript'> alert('Data has been submit email has been sent'); </script>"
                If Trim(dtbemail.Rows(0)!app2.ToString) = Trim(Session("niksite").ToString) Then
                    refresh()
                Else
                    If dtbemail.Rows.Count > 0 Then
                        mail = dtbemail.Rows(0)!email.ToString
                        niksite = dtbemail.Rows(0)!niksite.ToString
                        nama = dtbemail.Rows(0)!nama.ToString
                        jam_start = CDate(dtbemail.Rows(0)!jam_start).ToString("MM/dd/yyyy HH:mm")
                        jam_end = CDate(dtbemail.Rows(0)!jam_end).ToString("MM/dd/yyyy HH:mm")
                        remarks = dtbemail.Rows(0)!remarks.ToString
                    End If

                    message = New MailMessage()
                    message.From = New MailAddress("noreply.overtime@jresources.com", "Overtime Request Approval")
                    message.Subject = "Overtime Request Reminder " + GridView1.DataKeys(row.RowIndex).Value.ToString
                    message.IsBodyHtml = True
                    message.Body = "Dear, <br><br>"
                    message.Body = message.Body + "Please do Overtime Requests Approval for the following : <br><br>"
                    message.Body = message.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:10%'>NIK</th><th style='width:30%'>Name</th><th style='width:20%'>Time Start</th><th style='width:20%'>Time End</th><th style='width:20%'>Task</th>"
                    message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" + niksite + "</td><td style='text-align:left;border:solid 1px'>" + nama + "</td><td style='text-align:center;border:solid 1px'>" + jam_start + "</td><td style='text-align:center;border:solid 1px'>" + jam_end + "</td><td style='text-align:left;border:solid 1px'>" + remarks + "</td></tr></table>"
                    message.Body = message.Body + "<br>To Approval please go to http://portal.jresources.com"

                    message.Body = message.Body + "<br><br> Regards, <br><br> <strong>Admin</strong>"

                    message.To.Add(mail)
                    Dim smtp As New SmtpClient()
                    smtp.Host = "webmail.jresources.com"
                    smtp.Port = 587
                    smtp.EnableSsl = True
                    smtp.Credentials = New System.Net.NetworkCredential("noreply.overtime@jresources.com", "jrec12345")

                    smtp.Send(message)

                    msg = "<script type='text/javascript'> alert('Data has been save'); </script>"
                    refresh()
                End If
            End If
        Catch ex As Exception

        Finally
            sqlConn.Close()
        End Try
    End Sub

    Sub refresh()
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim baris As Integer
        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()

            'Dim strcon As String = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, jam_start, jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 "
            'strcon = strcon + "where checkapp1 = '" + Session("niksite") + "' and freject1 = '0' and stedit = '1' and StEdit <> 2 and month(Tanggal) = " + ddlbulan.SelectedValue.ToString + " and YEAR(Tanggal) = " + ddltahun.SelectedValue.ToString
            'Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            Dim strcon As String

            If Today.DayOfWeek = 1 Then
                strcon = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 "
                strcon = strcon + "where (checkapp1 = '" + Session("niksite") + "' or checkapp2 = (select top 1 nikdel from H_H11002 where nikdel = '" + Session("niksite") + "' and  date_to >= '" + Today.Date + "' )) and freject1 = '0' and freject2 = '0' and stedit = '1' and StEdit <> 2 and month(Tanggal) = " + ddlbulan.SelectedValue.ToString + " and YEAR(Tanggal) = " + ddltahun.SelectedValue.ToString + " and jam_start >= '" + Today.Date.AddDays(-2) + "'"
                strcon = strcon + " union "
                strcon = strcon + "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where (app2 = (select top 1 nikreq from H_H11002 where nikdel = '" + Session("niksite") + "' and date_to >= '" + Today.Date + "') or app1 = (select top 1 nikreq from H_H11002 where nikdel = '" + Session("niksite") + "' and date_to >= '" + Today.Date + "')) and stedit = '1' and freject1 = '0' and freject2 = '0' and StEdit <> 2 and Jam_start >= '" + Today.Date.AddDays(-2) + "'"
            Else
                strcon = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 "
                strcon = strcon + "where (checkapp1 = '" + Session("niksite") + "' or checkapp2 = (select top 1 nikdel from H_H11002 where nikdel = '" + Session("niksite") + "' and  date_to >= '" + Today.Date + "' )) and freject1 = '0' and freject2 = '0' and stedit = '1' and StEdit <> 2 and month(Tanggal) = " + ddlbulan.SelectedValue.ToString + " and YEAR(Tanggal) = " + ddltahun.SelectedValue.ToString + " and jam_start >= '" + Today.Date.AddDays(-1) + "'"
                strcon = strcon + " union "
                strcon = strcon + "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where (app2 = (select top 1 nikreq from H_H11002 where nikdel = '" + Session("niksite") + "' and date_to >= '" + Today.Date + "') or app1 = (select top 1 nikreq from H_H11002 where nikdel = '" + Session("niksite") + "' and date_to >= '" + Today.Date + "')) and stedit = '1' and freject1 = '0' and freject2 = '0' and StEdit <> 2 and Jam_start >= '" + Today.Date.AddDays(-1) + "'"
            End If

            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)
            For baris = 0 To dtb.Rows.Count - 1
                If dtb.Rows(baris)!checkapp1.ToString = "" And dtb.Rows(baris)!checkapp2.ToString = "" Then
                    dtb.Rows(baris)!status = "Complete"
                ElseIf dtb.Rows(baris)!checkapp1.ToString <> "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "1" Then
                    dtb.Rows(baris)!status = "Approval 1"
                ElseIf dtb.Rows(baris)!checkapp1.ToString = "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "1" Then
                    dtb.Rows(baris)!status = "Approval 2"
                ElseIf dtb.Rows(baris)!checkapp1.ToString <> "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "0" Then
                    dtb.Rows(baris)!status = "Save"
                End If
            Next
            GridView1.DataSource = dtb
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btncomplist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncomplist.Click
        Response.Redirect("spl_approval_list.aspx")
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
        Try
            Dim row As GridViewRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)
            If e.CommandName.Equals("List") Then
                Dim shari As String

                shari = Today.Day.ToString

                If Today.Month <> 12 And Today.Month <> 1 Then
                    If CDbl(shari) > 15 Then
                        Session("otdate1") = Today.Year.ToString + "-" + Today.Month.ToString + "-" + "16"
                        Session("otdate2") = Today.Year.ToString + "-" + Today.AddMonths(1).Month.ToString + "-" + "15"
                        Session("otnik") = row.Cells(1).Text
                        Page.ClientScript.RegisterStartupScript(Me.[GetType](), "OpenWindow", "window.open('rptot_plan.aspx','_newtab');", True)
                    Else
                        Session("otdate1") = Today.Year.ToString + "-" + Today.AddMonths(-1).Month.ToString + "-" + "16"
                        Session("otdate2") = Today.Year.ToString + "-" + Today.Month.ToString + "-" + "15"
                        Session("otnik") = row.Cells(1).Text
                        Page.ClientScript.RegisterStartupScript(Me.[GetType](), "OpenWindow", "window.open('rptot_plan.aspx','_newtab');", True)
                    End If
                Else
                    If Today.Month = 1 Then
                        If CDbl(shari) > 15 Then
                            Session("otdate1") = Today.Year.ToString + "-" + Today.Month.ToString + "-" + "16"
                            Session("otdate2") = Today.Year.ToString + "-" + Today.AddMonths(1).Month.ToString + "-" + "15"
                            Session("otnik") = row.Cells(1).Text
                            Page.ClientScript.RegisterStartupScript(Me.[GetType](), "OpenWindow", "window.open('rptot_plan.aspx','_newtab');", True)
                        Else
                            Session("otdate1") = Today.AddYears(-1).Year.ToString + "-" + Today.AddMonths(-1).Month.ToString + "-" + "16"
                            Session("otdate2") = Today.Year.ToString + "-" + Today.Month.ToString + "-" + "15"
                            Session("otnik") = row.Cells(1).Text
                            Page.ClientScript.RegisterStartupScript(Me.[GetType](), "OpenWindow", "window.open('rptot_plan.aspx','_newtab');", True)
                        End If
                    Else
                        If Today.Month = 12 Then
                            If CDbl(shari) > 15 Then
                                Session("otdate1") = Today.Year.ToString + "-" + Today.Month.ToString + "-" + "16"
                                Session("otdate2") = Today.AddYears(1).Year.ToString + "-" + Today.AddMonths(1).Month.ToString + "-" + "15"
                                Session("otnik") = row.Cells(1).Text
                                Page.ClientScript.RegisterStartupScript(Me.[GetType](), "OpenWindow", "window.open('rptot_plan.aspx','_newtab');", True)
                            Else
                                Session("otdate1") = Today.Year.ToString + "-" + Today.AddMonths(-1).Month.ToString + "-" + "16"
                                Session("otdate2") = Today.Year.ToString + "-" + Today.Month.ToString + "-" + "15"
                                Session("otnik") = row.Cells(1).Text
                                Page.ClientScript.RegisterStartupScript(Me.[GetType](), "OpenWindow", "window.open('rptot_plan.aspx','_newtab');", True)
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub
End Class