﻿Imports System.Data.SqlClient
Partial Public Class srcfamily
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            LoadData(True)
            SearchKey.Visible = False
            btnSearch.Visible = False
        End If
    End Sub

    Public Property PageNumber() As Integer
        Get
            If ViewState("PageNumber") IsNot Nothing Then
                Return Convert.ToInt32(ViewState("PageNumber"))
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PageNumber") = value
        End Set
    End Property

    'Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
    '    If (SearchKey.Text.Length < 3) Then
    '    Else
    '        LoadData(True)
    '    End If
    'End Sub

    Function LoadData(ByVal bool As Boolean)
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        'Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("test_con").ConnectionString)
        Dim lsnik As String = Request.QueryString("id").ToString
        Try
            Dim dt As New DataTable
            If bool Then

                Dim strSQL As String = "select FamilyID as familyid,FamilyName,familyStatus,FamilyTglLahir as familytgllahir from H_A104 where Nik='" + lsnik + "' and (familyStatus='02') union select Nik,Nama,'Karyawan',TglLahir from H_A101 where Nik='" + lsnik + "' union select * from ( select top 3 FamilyID,FamilyName,familyStatus,FamilyTglLahir as familyTglLahir from H_A104 b,H_A101 a where a.NIK=b.Nik and a.KdSite<>'PEN' and a.Nik='" + lsnik + "' and (familyStatus='01') order by FamilyTglLahir asc)g union select FamilyID ,FamilyName ,FamilyStatus ,FamilyTglLahir from ( select (CASE WHEN DATEPART(day, FamilyTglLahir ) > DATEPART(day, getdate()) THEN DATEDIFF(month, FamilyTglLahir, getdate()) - 1 ELSE DATEDIFF(month, FamilyTglLahir, getdate()) END / 12) as umur, FamilyID,FamilyName,familyStatus,FamilyTglLahir as familyTglLahir from H_A104 b,H_A101 a where a.NIK=b.Nik and a.KdSite='PEN' and a.Nik='" + lsnik + "' and (familyStatus='01') )g where umur<=18 union select b.FamilyID,b.FamilyName,b.familyStatus,b.FamilyTglLahir from H_A104 b, H_A101 a where b.Nik='" + lsnik + "' and (b.familyStatus='03') and a.NIK=b.nik and a.KdSite in ('LAN','BAK','KTB','KTG','MDO') and a.Gender ='P'"
                Dim myCMD As SqlCommand = New SqlCommand(strSQL, sqlConn)

                'Dim myCMD As SqlCommand = New SqlCommand("select '2' as urutan, FamilyID, replace(FamilyName,'''','') as FamilyName,'Istri' as familyStatus,FamilyTglLahir from H_A104 where Nik='" + lsnik + "' and (familyStatus='02') union select '1' as urutan, nik, replace(Nama,'''','') as Nama,'Karyawan',TglLahir from H_A101 where Nik='" + lsnik + "' union select '4' as urutan, FamilyID,replace(FamilyName,'''','') as FamilyName,'Suami' as familyStatus,FamilyTglLahir from H_A104 where Nik='" + lsnik + "' and (familyStatus='03') union select * from (select top 3 '3' as urutan,FamilyID,replace(FamilyName,'''','') as FamilyName,'Anak' as familyStatus,FamilyTglLahir from H_A104 where Nik='" + lsnik + "' and (familyStatus='01') order by FamilyTglLahir asc)g", sqlConn)

                'Dim param As New SqlParameter()
                'param.ParameterName = "@Nama"
                'param.Value = SearchKey.Text
                'myCMD.Parameters.Add(param)
                sqlConn.Open()
                Dim reader As SqlDataReader = myCMD.ExecuteReader()
                dt.Load(reader, LoadOption.OverwriteChanges)
                sqlConn.Close()
                ViewState("dt") = dt
                PageNumber = 0
            End If
            Dim pgitems As New PagedDataSource()
            Dim dv As New DataView(ViewState("dt"))
            pgitems.DataSource = dv
            pgitems.AllowPaging = True
            pgitems.PageSize = 15
            pgitems.CurrentPageIndex = PageNumber

            If pgitems.PageCount > 1 Then
                rptPages.Visible = True
                Dim pages As New ArrayList()
                For i As Integer = 0 To pgitems.PageCount - 1
                    pages.Add((i + 1).ToString())
                Next
                rptPages.DataSource = pages
                rptPages.DataBind()
            Else
                rptPages.Visible = False
            End If
            rptResult.DataSource = pgitems
            rptResult.DataBind()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            If sqlConn.State = ConnectionState.Open Then
                sqlConn.Close()
            End If
        End Try
    End Function

    Private Sub rptPages_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPages.ItemCommand
        PageNumber = Convert.ToInt32(e.CommandArgument) - 1
        LoadData(False)
    End Sub

End Class