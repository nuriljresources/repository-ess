﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ppa_task_entry.aspx.vb" Inherits="EXCELLENT.ppa_task_entry" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Performance Appraisal</title>
<link rel="stylesheet" href="menu.css"/> 
<link rel="stylesheet" href="../css/jquery.tooltip/jquery.tooltip.css" type="text/css" />
    <script type="text/javascript" src="../Scripts/tooltip/jquery.min.js"></script>
    <script type="text/javascript" src="../Scripts/tooltip/jquery.tooltip.js"></script>
    
<style type="text/css">

body{
  padding: 1em;
  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAAQElEQVQIW2P89OvDfwYo+PHjJ4zJwMHBzsAIk0SXAKkCS2KTAEu++vQSbizIKGQAl0SXAJkGlsQmAbcT2Shk+wH0sCzAEOZW1AAAAABJRU5ErkJggg==);
}
a{
  color: #739931;
}
.page{
  max-width: 70em;
  margin: 0 auto;
}
table th,
table td{
  text-align: left;
}
table.layout{
  width: 100%;
  border-collapse: collapse;
}
table.display{
  margin: 1em 0;
}
table.display th,
table.display td{
  border: 1px solid #B3BFAA;
  padding: .5em 1em;
}

table.display th{ background: #D5E0CC; }
table.display td{ background: #fff; }

table.responsive-table{
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
}

.center{
  text-align:center;
  }
.none{
	border-style:none;
    text-align:center;
  }
  
  /* Accordion */
.accordionHeader
{
    border: 1px solid #2F4F4F;
    color: white;
    background-color: #2E4d7B;
	font-family: Arial, Sans-Serif;
	font-size: 12px;
	font-weight: bold;
    padding: 5px;
    margin-top: 5px;
    cursor: pointer;
}

#master_content .accordionHeader a
{
	color: #FFFFFF;
	background: none;
	text-decoration: none;
}

#master_content .accordionHeader a:hover
{
	background: none;
	text-decoration: underline;
}

.accordionHeaderSelected
{
    border: 1px solid #2F4F4F;
    color: white;
    background-color: #5078B3;
	font-family: Arial, Sans-Serif;
	font-size: 12px;
	font-weight: bold;
    padding: 5px;
    margin-top: 5px;
    cursor: pointer;
}

#master_content .accordionHeaderSelected a
{
	color: #FFFFFF;
	background: none;
	text-decoration: none;
}

#master_content .accordionHeaderSelected a:hover
{
	background: none;
	text-decoration: underline;
}

.accordionContent
{
    background-color: #D3DEEF;
    border: 1px dashed #2F4F4F;
    border-top: none;
    padding: 5px;
    padding-top: 10px;
}
/*end accordion*/
  
  #footer {
	background:#373737;
	clear:both;
	bottom: 0;
	position: relative;
	margin-top: -50px; /* negative value of footer height */
	height: 20px;
	clear:both;
	}
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 
	 FOOTER
	 	 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

.nav-footer
{
	position: relative;
	text-align: center;
	clear: both;
}

.nav-footer ul{
	padding:0;
	margin:0;
	text-align:center;
	}

.nav-footer li{
	background:none;
	display:inline;
	border-right:1px dotted #686868;
	padding:0 10px;
	}

.nav-footer li.first {
	border-left:1px dotted #686868;
	}
	
.copyright {
	color:#999;
	font-size:.8em;
	clear:both;
	}

.nav-footer a:link,
.nav-footer a:visited {
	color:#CCC;
	}

@media (max-width: 30em){
    table.responsive-table{
      box-shadow: none;  
    }
    table.responsive-table thead{
      display: none; 
    }
  table.display th,
  table.display td{
    padding: .5em;
  }
    
  table.responsive-table td:nth-child(1):before{
    content: 'Number';
  }
  table.responsive-table td:nth-child(2):before{
    content: 'Name';
  }
  table.responsive-table td:nth-child(1),
  table.responsive-table td:nth-child(2){
    padding-left: 25%;
  }
  table.responsive-table td:nth-child(1):before,
  table.responsive-table td:nth-child(2):before{
    position: absolute;
    left: .5em;
    font-weight: bold;
  }
  
    table.responsive-table tr,
    table.responsive-table td{
        display: block;
    }
    table.responsive-table tr{
        position: relative;
        margin-bottom: 1em;
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
    }
    table.responsive-table td{
        border-top: none;
    }
    table.responsive-table td.organisationnumber{
        background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    table.responsive-table td.actions{
        position: absolute;
        top: 0;
        right: 0;
        border: none;
        background: none;
    }
    table.responsive-table td.top{
    	background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    
    .container{font-size:14px; margin:0 auto; width:960px}
    .test_content{margin:10px 0;}
    .scroller_anchor{height:0px; margin:0; padding:0;}
    .scroller{max-width: 70em; margin: 0 auto;}
}

    * { font-family: Times New Roman; }
      div.item { width:20px; height:20px; background-color: Transparent; text-align:center; padding-top:25px; color: black;}
      div#item_1 { position: absolute; top: 250px; left: 305px; color: black; font-family:arial;}
      div#item_2 { position: absolute; top: 250px; left: 629px; color: black; font-family:arial;}      
      div#item_3 { position: absolute; top: 250px; left: 1010px; color: black; font-family:arial;}            
      div#item_4 { position: absolute; top: 250px; left: 1195px; color: black; font-family:arial;}
      div#item_5 { position: absolute; top: 250px; left: 875px; color: black; font-family:arial;}
</style>

<script type="text/javascript">
    $j = jQuery.noConflict();
    $j(document).ready(function() {
        $j("div.item").tooltip();
    });

    function chkchar(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode == 39)
            return false;

        return true;
    }
    </script>
    
</head>
<body>

    <form id="form1" runat="server">
    <div class="page" style="margin-bottom:0px;">
    
    <div id="div2" style="float:right;">
    <%=mreport%>
    <label><a href="ppa_logout.aspx">LogOut</a></label>
    </div>
    
    <img src="../images/logo.png" />
    <center style="font-size:1.5em; font-weight:bold; margin-bottom:30px;">PERSONAL PERFORMANCE APPRAISAL</center>
    <asp:HiddenField ID="txtnobuk" runat="server" />
    <asp:HiddenField ID="txtstats" runat="server" />
        <table class="" style="margin-bottom:0px; padding-bottom:0px;">
            <tr>
                <td style="width:300px;">
                    Nama Karyawan / <i>Employee's Name</i>
                </td>
                <td style="vertical-align:bottom;" >
                    <asp:TextBox ID="txtnama" runat="server" Width="400px" BackColor="#cccccc" Font-Bold="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    NIK / <i>Employee ID</i>
                </td>
                <td style="vertical-align:bottom;">
                    <asp:TextBox ID="txtnik" runat="server" Width="400px" BackColor="#cccccc" Font-Bold="true"></asp:TextBox>
                    <asp:HiddenField ID="hnik" runat="server" />
                    <asp:TextBox ID="txtjabat" runat="server" Visible="false" Width="400px" BackColor="#cccccc" Font-Bold="true"></asp:TextBox>
                    <asp:HiddenField ID="hjabat" runat="server" />
                </td>
            </tr>
            <%--<tr>
                <td>
                    Jabatan / <i>Position</i>
                </td>
                <td style="vertical-align:bottom;">
                    
                </td>
            </tr>--%>
            <tr>
                <td>
                    Departemen / <i>Department</i>
                </td>
                <td style="vertical-align:bottom;">
                    <asp:TextBox ID="txtdepar" runat="server" Width="400px" BackColor="#cccccc" Font-Bold="true"></asp:TextBox>
                    <asp:HiddenField ID="hdepar" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Atasan / <i>Superior</i>
                </td>
                <td style="vertical-align:bottom;">
                    <asp:TextBox ID="txtspv" runat="server" Width="400px" BackColor="#cccccc" Font-Bold="true"></asp:TextBox>
                    <asp:HiddenField ID="hnikspv" runat="server" />
                </td>
            </tr>
        </table>
        <br />
        <div style="position:absolute; Top:120px; right:180px; text-align:center;">
            <%--<table class="responsive-table" style="font-size:0.7em; border:1px; border-style:solid;">
            <tr>
                <th>
                    Skor
                </th>
                <th>
                    Definisi
                </th>
            </tr>
            <tr>
                <td>
                    5
                </td>
                <td>
                    Exceed All Of Objective
                </td>
                <td>
                    Melebihi Seluruh Target
                </td>
            </tr>
            <tr>
                <td>
                    4
                </td>
                <td>
                    Exceed Most Of Objective
                </td>
                <td>
                    Melebihi Sebagian Besar Target
                </td>
            </tr>
            <tr>
                <td>
                    3
                </td>
                 <td>
                    Meet Objective
                </td>
                 <td>
                    Mencapai Seluruh Target
                </td>
            </tr>
            <tr>
                <td>
                    2
                </td>
                <td>
                    Meet Some Objective
                </td>
                <td>
                    Mencapai Sebagian Target
                </td>
            </tr>
            <tr>
                <td>
                    1
                </td>
                <td>
                    Does Not Meet Any Objective
                </td>
                <td>
                    Tidak Mencapai Target Apapun
                </td>
            </tr>
            </table>--%>
            <a href="Image/Picture2.png" target="_blank"><img style="width:290px; height:140px;" src="Image/Picture2.png" /></a>
        </div>
        
        <div style="width:100px; float:right; position:absolute; top:200px; left:1130px"><p style="text-align:right;"><asp:ImageButton ID="imbtnprev" runat="server" ImageUrl="../images/Print_preview.png" AlternateText="Print Preview" /></p></div>
        
        <div id='cssmenu' style="width:10%; border: 1px solid #707070; background-color:#707070;text-align:center;">
            <ul>
               <li style="background-color:#707070;"><a style="text-decoration:none;" href='ppa_entry.aspx'><span style="color:white; text-decoration:none;">Behaviour</span></a></li>
               <li><span style="background-color:#707070;color:#A3A3A3;">/ Task</span></li>
            </ul>
        </div>
        
        <div class="scroller_anchor"></div>
        <div class="scroller">
         <table id="myTable" class="layout display responsive-table" border="1" style="font-size:0.9em;">
            <thead>
            <tr>
                <th style="width:1%; border-style:none;"><center>No</center></th>
                <th style="width:19%; border-style:none;">
                    <center>Title</center>
                <div id="item_1" class="item">
                    <img src="../images/alert.png" alt="Title" height="20px" width="20px" />
                <div class="tooltip_description" style="display:none" title="Title Example">
                    Cost Efficiency
                </div>
                </div> 
                </th>
                <th style="width:30%; border-style:none;">
                    <center>Key Performance Indicator</center>
                <div id="item_2" class="item">
                    <img src="../images/alert.png" alt="Title" height="20px" width="20px" />
                <div class="tooltip_description" style="display:none" title="Task Example">
                    Penghematan pemakaian fuel dari seluruh unit utama (Loader & Hauler) operasional waste removal di site
                </div> 
                </div> 
                </th>
                <th style="width:5%; border-style:none;">
                    <center>UOM</center>
                <div id="item_5" class="item">
                    <img src="../images/alert.png" alt="Title" height="20px" width="20px" />
                <div class="tooltip_description" style="display:none" title="Unit Of Measurement">
                    Litre / Ton
                </div> 
                </div> 
                </th>
                <th style="width:15%; border-style:none;">
                    <center>Target</center> 
                    <div id="item_3" class="item">
                    <img src="../images/alert.png" alt="Title" height="20px" width="20px" />
                <div class="tooltip_description" style="display:none" title="Target Example">
                    10 litre/ton
                </div> 
                </div> 
                </th>
                <th style="width:15%; border-style:none;">
                    <center>Actual</center> 
                    <div id="item_4" class="item">
                    <img src="../images/alert.png" alt="Title" height="20px" width="20px" />
                <div class="tooltip_description" style="display:none" title="Actual Example">
                    15 litre/ton
                </div> 
                </div> 
                </th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td rowspan="2">
                       <center><strong> 1</strong></center> 
                    </td>
                    <td>
                        <asp:TextBox ID="txttitle1" runat="server" onkeypress="return chkchar(event)" 
                            Rows="10" Width="220px" TextMode="MultiLine" MaxLength="5000"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txttask1" runat="server" onkeypress="return chkchar(event)" 
                            Rows="10" Columns="45" Width="360px" TextMode="MultiLine" MaxLength="5000"></asp:TextBox>
                    </td>
                    <td style="vertical-align:top;">
                        <asp:TextBox ID="txtuom1" runat="server" onkeypress="return chkchar(event)" 
                            Rows="1" Columns="10" Width="50px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txttarget1" runat="server" onkeypress="return chkchar(event)" 
                            Rows="10" Width="150px" TextMode="MultiLine" MaxLength="5000"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtactual1" runat="server" onkeypress="return chkchar(event)" 
                            Rows="10" Width="150px" TextMode="MultiLine" MaxLength="5000"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Score :</strong>
                    </td>
                    <td>
                         1 <asp:RadioButton ID="t11" GroupName="t1" runat="server" />
                    &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp;
                         2 <asp:RadioButton ID="t12" GroupName="t1" runat="server" />
                    &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp;
                         3 <asp:RadioButton ID="t13" GroupName="t1" runat="server" />
                    &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp;     
                         4 <asp:RadioButton ID="t14" GroupName="t1" runat="server" />
                    &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp;
                         5 <asp:RadioButton ID="t15" GroupName="t1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td rowspan="2">
                        <center><strong>2</strong></center> 
                    </td>
                    <td>
                        <asp:TextBox ID="txttitle2" runat="server" onkeypress="return chkchar(event)" 
                            Rows="10" Width="220px" TextMode="MultiLine" MaxLength="5000"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txttask2" runat="server" onkeypress="return chkchar(event)" 
                            Rows="10" Columns="45" Width="360px" TextMode="MultiLine" MaxLength="5000"></asp:TextBox>
                    </td>
                    <td style="vertical-align:top;">
                        <asp:TextBox ID="txtuom2" runat="server" onkeypress="return chkchar(event)" 
                            Rows="1" Columns="10" Width="50px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txttarget2" runat="server" onkeypress="return chkchar(event)" 
                            Rows="10" Width="150px" TextMode="MultiLine" MaxLength="5000"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtactual2" runat="server" onkeypress="return chkchar(event)" 
                            Rows="10" Width="150px" TextMode="MultiLine" MaxLength="5000"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Score :</strong>
                    </td>
                    <td>
                         1 <asp:RadioButton ID="t21" GroupName="t2" runat="server" />
                         &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp;
                         2 <asp:RadioButton ID="t22" GroupName="t2" runat="server" />
                         &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp;
                         3 <asp:RadioButton ID="t23" GroupName="t2" runat="server" />
                         &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp;
                         4 <asp:RadioButton ID="t24" GroupName="t2" runat="server" />
                         &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp;
                         5 <asp:RadioButton ID="t25" GroupName="t2" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td rowspan="2">
                         <center><strong>3</strong></center> 
                    </td>
                    <td>
                        <asp:TextBox ID="txttitle3" runat="server" onkeypress="return chkchar(event)" 
                            Rows="10" Width="220px" TextMode="MultiLine" MaxLength="5000"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txttask3" runat="server" onkeypress="return chkchar(event)" 
                            Rows="10" Columns="45" Width="360px" TextMode="MultiLine" MaxLength="5000"></asp:TextBox>
                    </td>
                    <td style="vertical-align:top;">
                        <asp:TextBox ID="txtuom3" runat="server" onkeypress="return chkchar(event)" 
                            Rows="1" Columns="10" Width="50px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txttarget3" runat="server" onkeypress="return chkchar(event)" 
                            Rows="10" Width="150px" TextMode="MultiLine" MaxLength="5000"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtactual3" runat="server" onkeypress="return chkchar(event)" 
                            Rows="10" Width="150px" TextMode="MultiLine" MaxLength="5000"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Score :</strong>
                    </td>
                    <td>
                         1 <asp:RadioButton ID="t31" GroupName="t3" runat="server" />
                         &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; 
                         2 <asp:RadioButton ID="t32" GroupName="t3" runat="server" />
                         &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; 
                         3 <asp:RadioButton ID="t33" GroupName="t3" runat="server" />
                         &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; 
                         4 <asp:RadioButton ID="t34" GroupName="t3" runat="server" />
                         &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; 
                         5 <asp:RadioButton ID="t35" GroupName="t3" runat="server" />
                    </td>
                </tr>
            </tbody>
            
            </table> 
            </div> 
    </div>
    <div class="page">
        <asp:Button ID="cmdSubmit" runat="server" Text="SAVE" />
        <asp:Button ID="cmdprint" runat="server" Text="PRINT" />
        <asp:TextBox ID="no" runat="server" style="visibility:hidden;"></asp:TextBox>    
    </div>
    <div style="padding-left: 15px; padding-right: 15px;">
    <div id="footer" style="margin-top:50px;">
        <div class="nav-footer">
          <p class="copyright">&copy; 2013 - 2015 PT JRESOURCES NUSANTARA</p>
        </div>
    </div>
    </div>
    </form>
    
    <%=msg %>
</body>
</html>
    