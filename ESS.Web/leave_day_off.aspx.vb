﻿Imports System.Data.SqlClient
Partial Public Class leave_day_off
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoadData(True)
    End Sub

    Public Property PageNumber() As Integer
        Get
            If ViewState("PageNumber") IsNot Nothing Then
                Return Convert.ToInt32(ViewState("PageNumber"))
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PageNumber") = value
        End Set
    End Property

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        'If (SearchKey.Items. < 3) Then
        'Else
        LoadData(True)
        'End If
    End Sub

    Function LoadData(ByVal bool As Boolean)
        'dnik = Request(SearchKey.UniqueID)
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        'Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("test_con").ConnectionString)
        Try
            Dim dt As New DataTable
            If bool Then
                Dim myCMD As SqlCommand = New SqlCommand("SELECT transid, ISNULL(reference, '') AS reference, CONVERT(varchar,expiredDate,101) as expiredDate,  totleave, totUsage, (totleave - totUsage) as totBalance FROM V_HR_FieldBreak_RAW WHERE nik = (SELECT TOP 1 nik FROM H_A101 WHERE NIKSITE = @niksite) AND expiredDate >= @expiredDate", sqlConn)


                Dim param As New SqlParameter()
                param.ParameterName = "@niksite"
                Dim nikSite As String = Request.QueryString("nik")
                If (nikSite = Nothing) Then
                    nikSite = Session("niksite").ToString
                End If

                param.Value = nikSite
                myCMD.Parameters.Add(param)
                param = New SqlParameter()
                param.ParameterName = "@expiredDate"

                Dim dtFrom As String = Request.QueryString("Id")
                If (dtFrom = Nothing) Then
                    dtFrom = DateTime.Now.AddYears(1).ToString("yyyy-MM-dd")
                End If

                param.Value = dtFrom
                myCMD.Parameters.Add(param)

                sqlConn.Open()
                Dim reader As SqlDataReader = myCMD.ExecuteReader()
                dt.Load(reader, LoadOption.OverwriteChanges)
                sqlConn.Close()
                ViewState("dt") = dt
                PageNumber = 0
            End If
            Dim pgitems As New PagedDataSource()
            Dim dv As New DataView(ViewState("dt"))
            pgitems.DataSource = dv
            pgitems.AllowPaging = True
            pgitems.PageSize = 15
            pgitems.CurrentPageIndex = PageNumber

            If pgitems.PageCount > 1 Then
                rptPages.Visible = True
                Dim pages As New ArrayList()
                For i As Integer = 0 To pgitems.PageCount - 1
                    pages.Add((i + 1).ToString())
                Next
                rptPages.DataSource = pages
                rptPages.DataBind()
            Else
                rptPages.Visible = False
            End If
            rptResult.DataSource = pgitems
            rptResult.DataBind()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            If sqlConn.State = ConnectionState.Open Then
                sqlConn.Close()
            End If
        End Try
    End Function

    Private Sub rptPages_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPages.ItemCommand
        PageNumber = Convert.ToInt32(e.CommandArgument) - 1
        LoadData(False)
    End Sub

End Class