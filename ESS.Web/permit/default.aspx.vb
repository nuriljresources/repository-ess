﻿Imports System.Data
Imports System.Data.SqlClient
Partial Public Class _default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim strcon As String = "select id,f_name,ftype,fsubject,company_name, site_name,fyear,fcategory,f_path from docpermit_trans where stedit <> 2"
        'strcon = strcon + " where month(reqdate) = '" + imonth.ToString + "' and YEAR(reqdate) = '" + th + "' and nik = '1231313' and stedit <> 2"
        Dim dtb As DataTable = New DataTable
        Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)

        sda.Fill(dtb)
        GridView1.DataSource = dtb
        GridView1.DataBind()
    End Sub

    Protected Sub uploaddoc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles uploaddoc.Click
        Response.Redirect("entry_permit_doc.aspx")
    End Sub

    Private Sub GridView1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridView1.RowDeleting
        'If GridView1.SelectedRow Is Nothing Then Exit Sub


        Dim row As GridViewRow = GridView1.SelectedRow
        Dim pid As String
        Dim i As Integer

        i = e.RowIndex
        pid = GridView1.DataKeys(i).Value.ToString
        
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

        conn.Open()
        Try
            Dim sqlquery As String = "update docpermit_trans set stedit = '2' where id='" + pid + "'"
            Dim cmd As SqlCommand
            cmd = New SqlCommand(sqlquery, conn)
            cmd.ExecuteScalar()
        Catch ex As Exception

        Finally
            conn.Close()
            Response.Redirect("default.aspx")
        End Try
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        If GridView1.SelectedRow Is Nothing Then Exit Sub

        Dim row As GridViewRow = GridView1.SelectedRow
        Session("pid") = GridView1.DataKeys(row.RowIndex).Value.ToString
        'Response.Redirect("view_trip.aspx?ID=" & GridView1.DataKeys(row.RowIndex).Value.ToString)
        Response.Redirect("edit_permit_doc.aspx")
    End Sub
End Class