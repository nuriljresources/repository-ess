﻿Imports System.Data
Imports System.Data.SqlClient
Imports ESS.DataAccess

Partial Public Class saldo_cuti
    Inherits System.Web.UI.Page
    Public persno As String
    Public outpatcovmax As String
    Public outpatcovrem As String
    Public framecov As String
    Public lensnormcov As String
    Public matcovnormal As String
    Public matcovcaesar As String
    Public anualleaveeli As String
    Public anualleaveeli_L As String
    Public anualleaveadd As String
    Public anualleaveusg As String
    Public anualleaveusg_L As String
    Public anualleavefor As String
    Public anualleaverem As String
    Public anualleaverem_L As String
    Public caryfwdrem As String
    Public caryfwdrem_L As String
    Public anualleavedtap As String
    Public anualleavedtap_L As String
    Public anualleavedtfor As String
    Public anualleavedtfor_L As String
    Public caryfwdap As String
    Public caryfwdap_L As String
    Public caryfwdfor As String
    Public caryfwdfor_L As String
    Public fieldbrkeli As String
    Public fieldbrkusd As String
    Public fieldbrkfor As String
    Public fieldbrkrem As String
    Public year As String
    Public site As String
    Public name As String
    Public nmcompany As String
    Public nmdepar As String
    Public nmdivisi As String
    Public nmjabat As String
    Public nmlevel As String
    Public tglefektif As String
    Public tglmasuk As String
    Public emptype As String

    Public SaldoCutiLong As SaldoCutiModel

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("niksite") = Nothing Or Session("niksite") = "" Then Response.Redirect("login.aspx")

        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        'Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("test_con").ConnectionString)
        year = Date.Today.Year.ToString
        Dim yearfb As String = Date.Today.Year.ToString
        'informasi cuti
        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()
            Dim dtbyear As DataTable = New DataTable()

            Dim sqlyear As String = "select top 1 datepop from h_h217 where nik = '" + Session("niksite") + "' order by datePop desc"
            Dim dtayear As SqlDataAdapter = New SqlDataAdapter(sqlyear, sqlConn)
            dtayear.Fill(dtbyear)

            If dtbyear.Rows.Count > 0 Then
                If CDate(dtbyear.Rows(0)!datepop.ToString) >= CDate(Date.Today.ToString) Then
                    'year = CDate(dtbyear.Rows(0)!datepop.ToString).Year - 1
                    year = CDate(dtbyear.Rows(0)!datepop.ToString).Year
                End If
            End If
            'Session("niksite") = "0001336"
            'Dim strcon As String = "SELECT H_A101.ClaimPercentage, H_A101.Nik, h_a101.outpatientmax,(Select outpatientmax from h_h216 Where kdsite = H_A101.kdsite AND " & _
            '"Tahun  = '" + year + "' AND nik    = H_A101.nik   	) as 'MaxJln', ( isnull(h_a101.outpatientmax,0) + isnull((SELECT sum( H_H21501.biayatot) " & _
            '"FROM H_H21501, H_H215 WHERE ( H_H21501.NoReg = H_H215.NoReg ) and H_H215.Tipe='0' and H_H215.fstatus='1' and " & _
            '"(( H_H215.StEdit <> '2' ) AND ( H_H215.Nik = H_A101.nik ) AND year(H_H21501.Tgldet)='" + year + "' )),0)) as 'Usage', " & _
            '"(select 	H_A21501.Amount1 from      H_A21501 inner join H_A21502 On H_A21501.nobuk = H_A21502.nobuk " & _
            '"inner join H_A120   On H_A120.KdSite=H_A21501.KdSite inner join H_A160   On H_A21502.kdlevel = H_A160.kdlevel " & _
            '"WHERE H_A21501.stedit<>'2' AND H_A21501.TipeOutPatient='3' AND H_A21501.kdsite  = H_A101.kdsite AND H_A21502.kdlevel = H_A101.kdlevel) as 'LhrCaesar', " & _
            '"(select 	H_A21501.Amount2 from     H_A21501 inner join H_A21502 On H_A21501.nobuk = H_A21502.nobuk " & _
            '"inner join H_A120   On H_A120.KdSite=H_A21501.KdSite inner join H_A160   On H_A21502.kdlevel = H_A160.kdlevel " & _
            '"WHERE H_A21501.stedit<>'2' AND H_A21501.TipeOutPatient='3' AND H_A21501.kdsite  = H_A101.kdsite AND H_A21502.kdlevel = H_A101.kdlevel) as 'LhrNormal', " & _
            '"(select H_A21501.Amount1  from H_A21501 inner join H_A21502 On H_A21501.nobuk = H_A21502.nobuk " & _
            '"inner join H_A120   On H_A120.KdSite=H_A21501.KdSite inner join H_A160   On H_A21502.kdlevel = H_A160.kdlevel " & _
            '"WHERE H_A21501.stedit<>'2' AND H_A21501.TipeOutPatient='2' AND H_A21501.kdsite  = H_A101.kdsite " & _
            '"AND H_A21502.kdlevel = H_A101.kdlevel) as 'MonoNormal', (select H_A21501.Amount2   from H_A21501 " & _
            '"inner join H_A21502 On H_A21501.nobuk = H_A21502.nobuk inner join H_A120   On H_A120.KdSite=H_A21501.KdSite " & _
            '"inner join H_A160   On H_A21502.kdlevel = H_A160.kdlevel WHERE H_A21501.stedit<>'2' AND H_A21501.TipeOutPatient='2' " & _
            '"AND H_A21501.kdsite  = H_A101.kdsite AND H_A21502.kdlevel = H_A101.kdlevel) as 'MonoCyn',(select  H_A21501.Amount3    from H_A21501 " & _
            '"inner join H_A21502 On H_A21501.nobuk = H_A21502.nobuk inner join H_A120   On H_A120.KdSite=H_A21501.KdSite " & _
            '"inner join H_A160   On H_A21502.kdlevel = H_A160.kdlevel WHERE H_A21501.stedit<>'2' AND H_A21501.TipeOutPatient='2' " & _
            '"AND H_A21501.kdsite  = H_A101.kdsite AND H_A21502.kdlevel = H_A101.kdlevel) as 'BifoNormal', (select H_A21501.Amount4  from H_A21501 " & _
            '"inner join H_A21502 On H_A21501.nobuk = H_A21502.nobuk inner join H_A120   On H_A120.KdSite=H_A21501.KdSite " & _
            '"inner join H_A160   On H_A21502.kdlevel = H_A160.kdlevel WHERE H_A21501.stedit<>'2' AND H_A21501.TipeOutPatient='2' " & _
            '"AND H_A21501.kdsite  = H_A101.kdsite AND H_A21502.kdlevel = H_A101.kdlevel) as 'BifoCyn', (select  H_A21501.Amount5   from H_A21501 " & _
            '"inner join H_A21502 On H_A21501.nobuk = H_A21502.nobuk inner join H_A120   On H_A120.KdSite=H_A21501.KdSite " & _
            '"inner join H_A160   On H_A21502.kdlevel = H_A160.kdlevel WHERE H_A21501.stedit<>'2' AND H_A21501.TipeOutPatient='2' " & _
            '"AND H_A21501.kdsite  = H_A101.kdsite AND H_A21502.kdlevel = H_A101.kdlevel) as 'frame',(SELECT DATEPOP FROM " & _
            '"h_h217 WHERE NIK = '" + Session("niksite") + "' AND YEAR = '" + year + "') AS 'DateLeavePop', isnull((SELECT TOTlEAVE FROM h_h217 WHERE NIK = '" + Session("niksite") + "' AND YEAR = '" + year + "'),0) AS 'Totleave', " & _
            '"(SELECT REMAINLEAVE FROM h_h217 WHERE NIK = '" + Session("niksite") + "' AND YEAR = '" + year + "') AS 'RemainLeave', (SELECT DATEEXPIRED FROM h_h217 WHERE NIK = '" + Session("niksite") + "' AND YEAR = '" + year + "') AS 'ExpiredLeave', " & _
            '"isnull((select SUM(atot) from L_H001 where stedit <> '2' and fstatus=1 and typeleave='00' and nik = '" + Session("niksite") + "' and yeara =   '" + year + "' ),0) + " & _
            '"isnull((select SUM(btot) from L_H001 where stedit <> '2' and fstatus=1 and typeleave='00' and nik =  '" + Session("niksite") + "' and yearb =  '" + year + "' ),0) as 'usedLeave', " & _
            '"isnull((select SUM(cutabsent) from L_H001 where stedit <> '2' and fstatus=1 and typeleave='00' and nik = '" + Session("niksite") + "' and YEAR(datefrom) = '" + year + "'),0) as 'AbsentLeave', " & _
            '"isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='01' and nik = '" + Session("niksite") + "' and YEAR(trans_date) = '" + year + "'),0) as 'AddLeave', " & _
            '"isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='00' and nik = '" + Session("niksite") + "' and YEAR(trans_date) = '" + year + "'),0) as 'burnLeave', " & _
            '"isnull((select remainLeave from H_H217 where nik = '" + Session("niksite") + "' and year < '" + year + "'  and dateExpired <=GETDATE()),0) as 'RemainLast', " & _
            '"isnull((select SUM(atot) from L_H001 where stedit <> '2' and fstatus=1 and typeleave='00' and nik = '" + Session("niksite") + "' and yeara =   '" + year + "' - 1 ),0) + " & _
            '"isnull((select SUM(btot) from L_H001 where stedit <> '2' and fstatus=1 and typeleave='00' and nik =  '" + Session("niksite") + "' and yearb =  '" + year + "' - 1 ),0) as 'usedLeaveLast', " & _
            '"isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='00' and nik = '" + Session("niksite") + "' and YEAR(trans_date) = '" + year + "' - 1),0) as 'burnLeaveLast', " & _
            '"isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='01' and nik = '" + Session("niksite") + "' and YEAR(trans_date) = '" + year + "' - 1),0) as 'AddLeaveLast', " & _
            '"isnull((SELECT TOTlEAVE FROM h_h217 WHERE NIK = '" + Session("niksite") + "' AND YEAR = '" + year + "' - 1),0) AS 'TotleaveLast', " & _
            '"(SELECT DATEEXPIRED FROM h_h217 WHERE NIK = '" + Session("niksite") + "' AND YEAR = '" + year + "' -1) AS 'ExpiredLeaveLast',(SELECT DATEPOP FROM h_h217 WHERE NIK = '" + Session("niksite") + "' AND YEAR = '" + year + "' -1 ) AS 'DateLeavePopLast', " & _
            '"isnull((select SUM(totleave) from L_H005 where stedit <> '2' and fstatus=1 and ltype='00' and nik = '" + Session("niksite") + "'  and YEAR(trans_date) = '" + year + "' ),0) as 'burnLeaveField', " & _
            '"isnull((select SUM(totleave) from L_H005 where stedit <> '2' and fstatus=1 and ltype='01' and nik = '" + Session("niksite") + "'  and YEAR(trans_date) = '" + year + "' ),0) as 'AddLeaveField', " & _
            '"isnull(( select SUM(totleave) from L_H001 where stedit <> '2' and fstatus=1 and typeleave='02' and nik = '" + Session("niksite") + "' and YEAR(trans_date) = '" + year + "' ),0)    as 'usedLeaveField' " & _
            '"FROM H_A101 WHERE H_A101.Nik = '" + Session("niksite") + "'"

            'Dim strcon As String = "SELECT H_A101.ClaimPercentage, " & _
            '"H_A101.Nik, " & _
            '"h_a101.outpatientmax, " & _
            '"(Select outpatientmax from h_h216 " & _
            '"Where kdsite = H_A101.kdsite AND " & _
            '"Tahun  = '" + year + "' AND " & _
            '"nik    = H_A101.nik       ) as 'MaxJln', " & _
            '"(isnull(h_a101.outpatientmax,0) + isnull((SELECT sum( H_H21501.biayatot) " & _
            '"FROM H_H21501, H_H215 " & _
            '"WHERE ( H_H21501.NoReg = H_H215.NoReg ) and " & _
            '"H_H215.Tipe='0' and H_H215.fstatus='1' and ( ( H_H215.StEdit <> '2' ) AND ( H_H215.Nik = H_A101.nik ) AND " & _
            '"year(H_H21501.Tgldet)='" + year + "')),0)) as 'Usage', " & _
            '"(select H_A21501.Amount1 from H_A21501 inner join H_A21502 On H_A21501.nobuk = H_A21502.nobuk " & _
            '"inner join H_A120   On H_A120.KdSite=H_A21501.KdSite " & _
            '"inner join H_A160   On H_A21502.kdlevel = H_A160.kdlevel " & _
            '"WHERE H_A21501.stedit<>'2' " & _
            '"AND H_A21501.TipeOutPatient='3' " & _
            '"AND H_A21501.kdsite  = H_A101.kdsite " & _
            '"AND H_A21502.kdlevel = H_A101.kdlevel) as 'LhrCaesar', " & _
            '"(select     H_A21501.Amount2 " & _
            '"from H_A21501 " & _
            '"inner join H_A21502 On H_A21501.nobuk = H_A21502.nobuk " & _
            '"inner join H_A120   On H_A120.KdSite=H_A21501.KdSite " & _
            '"inner join H_A160   On H_A21502.kdlevel = H_A160.kdlevel " & _
            '"WHERE H_A21501.stedit<>'2' " & _
            '"AND H_A21501.TipeOutPatient='3' " & _
            '"AND H_A21501.kdsite  = H_A101.kdsite " & _
            '"AND H_A21502.kdlevel = H_A101.kdlevel) as 'LhrNormal', " & _
            '"(select H_A21501.Amount1  from H_A21501 " & _
            '"inner join H_A21502 On H_A21501.nobuk = H_A21502.nobuk " & _
            '"inner join H_A120   On H_A120.KdSite=H_A21501.KdSite " & _
            '"inner join H_A160   On H_A21502.kdlevel = H_A160.kdlevel " & _
            '"WHERE H_A21501.stedit<>'2' " & _
            '"AND H_A21501.TipeOutPatient='2' " & _
            '"AND H_A21501.kdsite  = H_A101.kdsite " & _
            '"AND H_A21502.kdlevel = H_A101.kdlevel) as 'MonoNormal', " & _
            '"(select H_A21501.Amount2   from H_A21501 " & _
            '"inner join H_A21502 On H_A21501.nobuk = H_A21502.nobuk " & _
            '"inner join H_A120   On H_A120.KdSite=H_A21501.KdSite " & _
            '"inner join H_A160   On H_A21502.kdlevel = H_A160.kdlevel " & _
            '"WHERE H_A21501.stedit<>'2' " & _
            '"AND H_A21501.TipeOutPatient='2' " & _
            '"AND H_A21501.kdsite  = H_A101.kdsite " & _
            '"AND H_A21502.kdlevel = H_A101.kdlevel) as 'MonoCyn', " & _
            '"(select  H_A21501.Amount3    from H_A21501 " & _
            '"inner join H_A21502 On H_A21501.nobuk = H_A21502.nobuk " & _
            '"inner join H_A120   On H_A120.KdSite=H_A21501.KdSite " & _
            '"inner join H_A160   On H_A21502.kdlevel = H_A160.kdlevel " & _
            '"WHERE H_A21501.stedit<>'2' " & _
            '"AND H_A21501.TipeOutPatient='2' " & _
            '"AND H_A21501.kdsite  = H_A101.kdsite " & _
            '"AND H_A21502.kdlevel = H_A101.kdlevel) as 'BifoNormal', " & _
            '"(select H_A21501.Amount4  from H_A21501 " & _
            '"inner join H_A21502 On H_A21501.nobuk = H_A21502.nobuk " & _
            '"inner join H_A120   On H_A120.KdSite=H_A21501.KdSite " & _
            '"inner join H_A160   On H_A21502.kdlevel = H_A160.kdlevel " & _
            '"WHERE H_A21501.stedit<>'2' " & _
            '"AND H_A21501.TipeOutPatient='2' " & _
            '"AND H_A21501.kdsite  = H_A101.kdsite " & _
            '"AND H_A21502.kdlevel = H_A101.kdlevel) as 'BifoCyn', " & _
            '"(select  H_A21501.Amount5   from H_A21501 " & _
            '"inner join H_A21502 On H_A21501.nobuk = H_A21502.nobuk " & _
            '"inner join H_A120   On H_A120.KdSite=H_A21501.KdSite " & _
            '"inner join H_A160   On H_A21502.kdlevel = H_A160.kdlevel " & _
            '"WHERE H_A21501.stedit<>'2' " & _
            '"AND H_A21501.TipeOutPatient='2' " & _
            '"AND H_A21501.kdsite  = H_A101.kdsite " & _
            '"AND H_A21502.kdlevel = H_A101.kdlevel) as 'frame', " & _
            '"(SELECT DATEPOP FROM " & _
            '"h_h217 WHERE NIK = '" + Session("niksite") + "' AND YEAR = '" + year + "') AS 'DateLeavePop', " & _
            '"isnull((SELECT TOTlEAVE FROM " & _
            '"h_h217 WHERE NIK = '" + Session("niksite") + "' AND YEAR = '" + year + "'),0) AS 'Totleave', " & _
            '"(SELECT REMAINLEAVE FROM " & _
            '"h_h217 WHERE NIK = '" + Session("niksite") + "' AND YEAR = '" + year + "') AS 'RemainLeave', " & _
            '"(SELECT DATEEXPIRED FROM " & _
            '"h_h217 WHERE NIK = '" + Session("niksite") + "' AND YEAR = '" + year + "') AS 'ExpiredLeave', " & _
            '"isnull((select SUM(atot) from L_H001 " & _
            '"where stedit <> '2' and fstatus=1 " & _
            '"and typeleave='00' and nik = '" + Session("niksite") + "' and yeara =   '" + year + "' ),0) + " & _
            '"isnull((select SUM(btot) from L_H001 " & _
            '"where stedit <> '2' and fstatus=1 " & _
            '"and typeleave='00' and nik =  '" + Session("niksite") + "' and yearb =  '" + year + "' ),0) as 'usedLeave', " & _
            '"isnull((select SUM(cutabsent) from L_H001 " & _
            '"where stedit <> '2' and fstatus=1 " & _
            '"and typeleave='00' and nik = '" + Session("niksite") + "' and YEAR(datefrom) = '" + year + "'),0) as 'AbsentLeave', " & _
            '"isnull((select SUM(totleave) from L_H002 " & _
            '"where stedit <> '2' and fstatus=1 and ltype='01' " & _
            '"and nik = '" + Session("niksite") + "' and YEAR(trans_date) = '" + year + "'),0) as 'AddLeave', " & _
            '"isnull((select SUM(totleave) from L_H002 " & _
            '"where stedit <> '2' and fstatus=1 and ltype='00' " & _
            '"and nik = '" + Session("niksite") + "' and YEAR(trans_date) = '" + year + "'),0) as 'burnLeave', " & _
            '"isnull((select Top 1 remainLeave from H_H217 where nik = '" + Session("niksite") + "' " & _
            '"and year < '" + year + "'  and dateExpired <=GETDATE()),0) as 'RemainLast', " & _
            '"isnull((select SUM(atot) from L_H001 " & _
            '"where stedit <> '2' and fstatus=1 " & _
            '"and typeleave='00' and nik = '" + Session("niksite") + "' and yeara in (select year from H_H217 where nik = H_A101.Nik  AND dateExpired >= GETDATE() and year < '" + year + "') ),0) + " & _
            '"isnull((select SUM(btot) from L_H001 " & _
            '"where stedit <> '2' and fstatus=1 " & _
            '"and typeleave='00' and nik =  '" + Session("niksite") + "' and yearb in (select year from H_H217 where nik = H_A101.Nik  AND dateExpired >= GETDATE() and year < '" + year + "')  ),0) as 'usedLeaveLast', " & _
            '"isnull((select SUM(totleave) from L_H002 " & _
            '"where stedit <> '2' and fstatus=1 and ltype='00' " & _
            '"and nik = '" + Session("niksite") + "' and YEAR(trans_date) < '" + year + "' ),0) as 'burnLeaveLast', " & _
            '"isnull((select SUM(totleave) from L_H002 " & _
            '"where stedit <> '2' and fstatus=1 and ltype='01' " & _
            '"and nik = '" + Session("niksite") + "' and YEAR(trans_date) < '" + year + "' ),0) as 'AddLeaveLast', " & _
            '"isnull((SELECT sum(TOTlEAVE) FROM " & _
            '"h_h217 WHERE NIK = '" + Session("niksite") + "' AND dateExpired >= GETDATE() AND YEAR < '" + year + "' ),0) AS 'TotleaveLast', " & _
            '"(SELECT DATEEXPIRED FROM " & _
            '"h_h217 WHERE NIK = '" + Session("niksite") + "' AND YEAR = '" + year + "' -1) AS 'ExpiredLeaveLast', " & _
            '"(SELECT DATEPOP FROM " & _
            '"h_h217 WHERE NIK = '" + Session("niksite") + "' AND YEAR = '" + year + "' -1 ) AS 'DateLeavePopLast', " & _
            '"isnull((select SUM(totleave) from L_H005 " & _
            '"where stedit <> '2' and fstatus=1 and ltype='00' " & _
            '"and nik = '" + Session("niksite") + "'  and YEAR(trans_date) = '" + yearfb + "' ),0) as 'burnLeaveField', " & _
            '"isnull((select SUM(totleave) from L_H005 " & _
            '"where stedit <> '2' and fstatus=1 and ltype='01' " & _
            '"and nik = '" + Session("niksite") + "'  and YEAR(trans_date) = '" + yearfb + "' ),0) as 'AddLeaveField', " & _
            '"isnull(( select SUM(totleave) from L_H001 " & _
            '"where stedit <> '2' and fstatus=1 " & _
            '"and typeleave='02' and nik = '" + Session("niksite") + "' and YEAR(trans_date) = '" + yearfb + "' ),0)    as 'usedLeaveField' " & _
            '"FROM H_A101 WHERE H_A101.Nik = '" + Session("niksite") + "'"

            Dim strcon As String = String.Empty

            '          strcon = <![CDATA[SELECT h_a101.claimpercentage, 
            '                         h_a101.nik, 
            '                         h_a101.outpatientmax, 
            '                         (SELECT outpatientmax 
            '                          FROM   h_h216 
            '                          WHERE  kdsite = h_a101.kdsite 
            '                                 AND tahun = '@year' 
            '                                 AND nik = h_a101.nik) AS 
            '                         'MaxJln', 
            '                         ( Isnull(h_a101.outpatientmax, 0) 
            '                           + Isnull((SELECT Sum( h_h21501.biayatot) FROM h_h21501, h_h215 WHERE ( 
            '                           h_h21501.noreg = h_h215.noreg ) AND h_h215.tipe='0' AND 
            '                           h_h215.fstatus='1' AND 
            '                           ( ( h_h215.stedit <> '2' ) AND ( h_h215.nik = h_a101.nik ) AND 
            '                           Year(h_h21501.tgldet)='@year')), 0) ) AS 
            '                         'Usage', 
            '                         (SELECT h_a21501.amount1 
            '                          FROM   h_a21501 
            '                                 INNER JOIN h_a21502 
            '                    ON h_a21501.nobuk = h_a21502.nobuk 
            '                                 INNER JOIN h_a120 
            '                    ON h_a120.kdsite = h_a21501.kdsite 
            '                                 INNER JOIN h_a160 
            '                    ON h_a21502.kdlevel = h_a160.kdlevel 
            '                          WHERE  h_a21501.stedit <> '2' 
            '                                 AND h_a21501.tipeoutpatient = '3' 
            '                                 AND h_a21501.kdsite = h_a101.kdsite 
            '                                 AND h_a21502.kdlevel = h_a101.kdlevel) AS 
            '                         'LhrCaesar', 
            '                         (SELECT h_a21501.amount2 
            '                          FROM   h_a21501 
            '                                 INNER JOIN h_a21502 
            '                    ON h_a21501.nobuk = h_a21502.nobuk 
            '                                 INNER JOIN h_a120 
            '                    ON h_a120.kdsite = h_a21501.kdsite 
            '                                 INNER JOIN h_a160 
            '                    ON h_a21502.kdlevel = h_a160.kdlevel 
            '                          WHERE  h_a21501.stedit <> '2' 
            '                                 AND h_a21501.tipeoutpatient = '3' 
            '                                 AND h_a21501.kdsite = h_a101.kdsite 
            '                                 AND h_a21502.kdlevel = h_a101.kdlevel) AS 
            '                         'LhrNormal', 
            '                         (SELECT h_a21501.amount1 
            '                          FROM   h_a21501 
            '                                 INNER JOIN h_a21502 
            '                    ON h_a21501.nobuk = h_a21502.nobuk 
            '                                 INNER JOIN h_a120 
            '                    ON h_a120.kdsite = h_a21501.kdsite 
            '                                 INNER JOIN h_a160 
            '                    ON h_a21502.kdlevel = h_a160.kdlevel 
            '                          WHERE  h_a21501.stedit <> '2' 
            '                                 AND h_a21501.tipeoutpatient = '2' 
            '                                 AND h_a21501.kdsite = h_a101.kdsite 
            '                                 AND h_a21502.kdlevel = h_a101.kdlevel) AS 
            '                         'MonoNormal', 
            '                         (SELECT h_a21501.amount2 
            '                          FROM   h_a21501 
            '                                 INNER JOIN h_a21502 
            '                    ON h_a21501.nobuk = h_a21502.nobuk 
            '                                 INNER JOIN h_a120 
            '                    ON h_a120.kdsite = h_a21501.kdsite 
            '                                 INNER JOIN h_a160 
            '                    ON h_a21502.kdlevel = h_a160.kdlevel 
            '                          WHERE  h_a21501.stedit <> '2' 
            '                                 AND h_a21501.tipeoutpatient = '2' 
            '                                 AND h_a21501.kdsite = h_a101.kdsite 
            '                                 AND h_a21502.kdlevel = h_a101.kdlevel) AS 
            '                         'MonoCyn', 
            '                         (SELECT h_a21501.amount3 
            '                          FROM   h_a21501 
            '                                 INNER JOIN h_a21502 
            '                    ON h_a21501.nobuk = h_a21502.nobuk 
            '                                 INNER JOIN h_a120 
            '                    ON h_a120.kdsite = h_a21501.kdsite 
            '                                 INNER JOIN h_a160 
            '                    ON h_a21502.kdlevel = h_a160.kdlevel 
            '                          WHERE  h_a21501.stedit <> '2' 
            '                                 AND h_a21501.tipeoutpatient = '2' 
            '                                 AND h_a21501.kdsite = h_a101.kdsite 
            '                                 AND h_a21502.kdlevel = h_a101.kdlevel) AS 
            '                         'BifoNormal', 
            '                         (SELECT h_a21501.amount4 
            '                          FROM   h_a21501 
            '                                 INNER JOIN h_a21502 
            '                    ON h_a21501.nobuk = h_a21502.nobuk 
            '                                 INNER JOIN h_a120 
            '                    ON h_a120.kdsite = h_a21501.kdsite 
            '                                 INNER JOIN h_a160 
            '                    ON h_a21502.kdlevel = h_a160.kdlevel 
            '                          WHERE  h_a21501.stedit <> '2' 
            '                                 AND h_a21501.tipeoutpatient = '2' 
            '                                 AND h_a21501.kdsite = h_a101.kdsite 
            '                                 AND h_a21502.kdlevel = h_a101.kdlevel) AS 
            '                         'BifoCyn', 
            '                         (SELECT h_a21501.amount5 
            '                          FROM   h_a21501 
            '                                 INNER JOIN h_a21502 
            '                    ON h_a21501.nobuk = h_a21502.nobuk 
            '                                 INNER JOIN h_a120 
            '                    ON h_a120.kdsite = h_a21501.kdsite 
            '                                 INNER JOIN h_a160 
            '                    ON h_a21502.kdlevel = h_a160.kdlevel 
            '                          WHERE  h_a21501.stedit <> '2' 
            '                                 AND h_a21501.tipeoutpatient = '2' 
            '                                 AND h_a21501.kdsite = h_a101.kdsite 
            '                                 AND h_a21502.kdlevel = h_a101.kdlevel) AS 
            '                         'frame', 
            '                         (SELECT datepop 
            '                          FROM   h_h217 
            '                          WHERE  nik = '@NIK' 
            '                                 AND year = '@year')    AS 
            '                         'DateLeavePop', 
            '                         Isnull((SELECT totleave 
            '                                 FROM   h_h217 
            '                                 WHERE  nik = '@NIK' 
            '                   AND year = '@year'), 0)  AS 
            '                         'Totleave', 


            'isnull((select sum(TOTlEAVE) from H_H217 where nik =  H_A101.Nik and dateExpired < getdate()
            '   and year < @year   ),0) as 'LastTotEx',



            '   isnull((select SUM(atot) from L_H001
            '   where stedit <> '2' and fstatus=1
            '   and typeleave='00' and nik = H_A101.Nik and yeara in (select year from H_H217 where nik = H_A101.Nik  AND dateExpired < getdate() and year < @year 
            '																	   union select YEAR from l_A099 where year < (select min(year) from H_H217 where nik = H_A101.Nik  AND dateExpired < getdate() and year < @year) 
            '                                                                                       union select year(h_a101.tglmasuk) )       ),0)  + 
            '   isnull((select SUM(btot) from L_H001
            '   where stedit <> '2' and fstatus=1
            '   and typeleave='00' and nik =  H_A101.Nik and yearb  in (select year from H_H217 where nik = H_A101.Nik  AND dateExpired < getdate() and year < @year
            '                                                                                        union select YEAR from l_A099 where year < (select min(year) from H_H217 where nik = H_A101.Nik  AND dateExpired < getdate() and year < @year)
            '                                                                                        union select year(h_a101.tglmasuk)  )    ),0) as 'LastusedLeaveEx',


            '                         (SELECT remainleave 
            '                          FROM   h_h217 
            '                          WHERE  nik = '@NIK' 
            '                                 AND year = '@year')    AS 
            '                         'RemainLeave', 
            '                         (SELECT dateexpired 
            '                          FROM   h_h217 
            '                          WHERE  nik = '@NIK' 
            '                                 AND year = '@year')    AS 
            '                         'ExpiredLeave', 
            '                         Isnull((SELECT Sum(atot) FROM l_h001 WHERE stedit <> '2' AND fstatus=1 
            '                         AND 
            '                         typeleave='00' AND nik = '@NIK' AND yeara = '@year' ), 0) 
            '                         + Isnull((SELECT Sum(btot) FROM l_h001 WHERE stedit <> '2' AND fstatus=1 
            '                         AND 
            '                         typeleave='00' AND nik = '@NIK' AND yearb = '@year' ), 0)            AS 
            '                         'usedLeave', 
            '                         Isnull((SELECT Sum(cutabsent) 
            '                                 FROM   l_h001 
            '                                 WHERE  stedit <> '2' 
            '                   AND fstatus = 1 
            '                   AND typeleave = '00' 
            '                   AND nik = '@NIK' 
            '                   AND Year(datefrom) = '@year'), 0)   AS 
            '                         'AbsentLeave', 
            '                         Isnull((SELECT Sum(totleave) 
            '                                 FROM   l_h002 
            '                                 WHERE  stedit <> '2' 
            '                   AND fstatus = 1 
            '                   AND ltype = '01' 
            '                   AND nik = '@NIK' 
            '                   AND Year(trans_date) = '@year'), 0) AS 
            '                         'AddLeave', 
            '                         Isnull((SELECT Sum(totleave) 
            '                                 FROM   l_h002 
            '                                 WHERE  stedit <> '2' 
            '                   AND fstatus = 1 
            '                   AND ltype = '00' 
            '                   AND nik = '@NIK' 
            '                   AND Year(trans_date) = '@year'), 0) AS 
            '                         'burnLeave', 
            '                         Isnull((SELECT TOP 1 remainleave 
            '                                 FROM   h_h217 
            '                                 WHERE  nik = '@NIK' 
            '                   AND year < '@year' 
            '                   AND dateexpired <= Getdate()), 0)  AS 
            '                         'RemainLast', 
            '                         Isnull((SELECT Sum(atot) FROM l_h001 WHERE stedit <> '2' AND fstatus=1 
            '                         AND 
            '                         typeleave='00' AND nik = '@NIK' AND yeara IN (SELECT year FROM h_h217 
            '                         WHERE 
            '                         nik = h_a101.nik AND dateexpired >= Getdate() AND year < '@year') ), 0) 
            '                         + Isnull((SELECT Sum(btot) FROM l_h001 WHERE stedit <> '2' AND fstatus=1 
            '                         AND 
            '                         typeleave='00' AND nik = '@NIK' AND yearb IN (SELECT year FROM h_h217 
            '                         WHERE 
            '                         nik = h_a101.nik AND dateexpired >= Getdate() AND year < '@year') ), 0) AS 
            '                         'usedLeaveLast', 
            '                         Isnull((SELECT Sum(totleave) 
            '                                 FROM   l_h002 
            '                                 WHERE  stedit <> '2' 
            '                   AND fstatus = 1 
            '                   AND ltype = '00' 
            '                   AND nik = '@NIK' 
            '                   AND Year(trans_date) < '@year'), 0) AS 
            '                         'burnLeaveLast', 
            '                         Isnull((SELECT Sum(totleave) 
            '                                 FROM   l_h002 
            '                                 WHERE  stedit <> '2' 
            '                   AND fstatus = 1 
            '                   AND ltype = '01' 
            '                   AND nik = '@NIK' 
            '                   AND Year(trans_date) < '@year'), 0) AS 
            '                         'AddLeaveLast', 
            '                         Isnull((SELECT Sum(totleave) 
            '                                 FROM   h_h217 
            '                                 WHERE  nik = '@NIK' 
            '                   AND dateexpired >= Getdate() 
            '                   AND year < '@year'), 0)  AS 
            '                         'TotleaveLast', 
            '                         (SELECT dateexpired 
            '                          FROM   h_h217 
            '                          WHERE  nik = '@NIK' 
            '                                 AND year = '@year' - 1)         AS 
            '                         'ExpiredLeaveLast', 
            '                         (SELECT datepop 
            '                          FROM   h_h217 
            '                          WHERE  nik = '@NIK' 
            '                                 AND year = '@year' - 1)         AS 
            '                         'DateLeavePopLast',      


            '                         (SELECT DATEPOP FROM 
            '                          h_h217l WHERE NIK = '@NIK'  AND YEAR = '@year') AS 'lDateLeavePop',
            '                          isnull((SELECT TOTlEAVE FROM 
            '                          h_h217l WHERE NIK = '@NIK'  AND YEAR = '@year'),0) AS 'lTotleave',
            '                          (SELECT DATEEXPIRED FROM 
            '                          h_h217l WHERE NIK = '@NIK'  AND YEAR = '@year') AS 'lExpiredLeave',

            '                          isnull((select SUM(atot) from L_H001
            '                          where stedit <> '2' and fstatus=1
            '                          and typeleave='01' and nik = '@NIK'  and yeara =   '@year' ),0) + 
            '                          isnull((select SUM(btot) from L_H001
            '                          where stedit <> '2' and fstatus=1
            '                          and typeleave='01' and nik =  '@NIK'  and yearb =  '@year' ),0) as 'lusedLeave',

            '                          isnull((select SUM(totleave) from L_H002L
            '                          where stedit <> '2' and fstatus=1 and ltype='01'
            '                          and nik = '@NIK'  and YEAR(trans_date) = '@year'),0) as 'lAddLeave',

            '                          isnull((select SUM(totleave) from L_H002L
            '                          where stedit <> '2' and fstatus=1 and ltype='00'
            '                          and nik = '@NIK'  and YEAR(trans_date) = '@year'),0) as 'lburnLeave',

            '                          isnull((select SUM(atot) from L_H001
            '                          where stedit <> '2' and fstatus=1
            '                          and typeleave='01' and nik = '@NIK'  and yeara in (select year from H_H217L where nik = H_A101.Nik  AND dateExpired >= getdate() and year < '@year')  ),0) + 
            '                          isnull((select SUM(btot) from L_H001
            '                          where stedit <> '2' and fstatus=1
            '                          and typeleave='01' and nik =  '@NIK'  and yearb  in (select year from H_H217L where nik = H_A101.Nik  AND dateExpired >= getdate() and year < '@year')  ),0) as 'lusedLeaveLast',
            '                          isnull((select SUM(totleave) from L_H002L
            '                          where stedit <> '2' and fstatus=1 and ltype='00'
            '                          and nik = '@NIK'  and YEAR(trans_date) < '@year' ),0) as 'lburnLeaveLast',
            '                          isnull((select SUM(totleave) from L_H002L
            '                          where stedit <> '2' and fstatus=1 and ltype='01'
            '                          and nik = '@NIK'  and YEAR(trans_date) < '@year' ),0) as 'lAddLeaveLast',
            '                          isnull((SELECT sum(TOTlEAVE) FROM 
            '                          h_h217L WHERE NIK = '@NIK'  and dateExpired >=  getdate()  AND YEAR < '@year' ),0) AS 'lTotleaveLast',

            '                          isnull((select sum(TOTlEAVE) from H_H217L where nik =  H_A101.Nik and dateExpired < getdate()
            '                          and year < '@year'   ),0) as 'lLastTotEx',

            '                          isnull((select SUM(atot) from L_H001
            '                          where stedit <> '2' and fstatus=1
            '                          and typeleave='01' and nik = H_A101.Nik and yeara in (select year from H_H217l where nik = H_A101.Nik  AND dateExpired < getdate() and year < '@year' 
            '                        union select YEAR from l_A099 where year < (select min(year) from H_H217l where nik = H_A101.Nik  AND dateExpired < getdate() and year < '@year') 
            '                       union select year(h_a101.tglmasuk) )       ),0)  + 
            '                          isnull((select SUM(btot) from L_H001
            '                          where stedit <> '2' and fstatus=1
            '                          and typeleave='01' and nik =  H_A101.Nik and yearb  in (select year from H_H217l where nik = H_A101.Nik  AND dateExpired < getdate() and year < '@year'
            '                          union select YEAR from l_A099 where year < (select min(year) from H_H217l where nik = H_A101.Nik  AND dateExpired < getdate() and year < '@year')
            '                          union select year(h_a101.tglmasuk)  )    ),0) as 'lLastusedLeaveEx',

            '                          (SELECT DATEEXPIRED FROM 
            '                          h_h217l WHERE NIK = '@NIK'  AND YEAR = '@year' -1) AS 'lExpiredLeaveLast',
            '                                 (SELECT DATEPOP FROM 
            '                          h_h217l WHERE NIK = '@NIK'  AND YEAR = '@year' -1 ) AS 'lDateLeavePopLast',

            '                         Isnull((SELECT Sum(totleave) 
            '                                 FROM   l_h005 
            '                                 WHERE  stedit <> '2' 
            '                   AND fstatus = 1 
            '                   AND ltype = '00' 
            '                   AND nik = '@NIK' 
            '                   AND Year(trans_date) = '@year'), 0) AS 
            '                         'burnLeaveField', 
            '                         Isnull((SELECT Sum(totleave) 
            '                                 FROM   l_h005 
            '                                 WHERE  stedit <> '2' 
            '                   AND fstatus = 1 
            '                   AND ltype = '01' 
            '                   AND nik = '@NIK' 
            '                   AND Year(trans_date) = '@year'), 0) AS 
            '                         'AddLeaveField', 
            '                         Isnull((SELECT Sum(totleave) 
            '                                 FROM   l_h001 
            '                                 WHERE  stedit <> '2' 
            '                   AND fstatus = 1 
            '                   AND typeleave = '02' 
            '                   AND nik = '@NIK' 
            '                   AND Year(trans_date) = '@year'), 0) AS 
            '                         'usedLeaveField' 
            '                  FROM   h_a101 
            '                  WHERE  h_a101.nik = '@NIK' 
            '                  -- JIJIK KALI LIAT INI QUERY
            '          ']]>.Value

            strcon = <![CDATA['sp_ESS_LEAVE_CALC']]>.Value


            Dim CMD As New SqlCommand("sp_ESS_LEAVE_CALC")
            CMD.Parameters.AddWithValue("@nik", Session("niksite"))
            CMD.Parameters.AddWithValue("@argThn", yearfb)

            Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
            CMD.Connection = connection
            CMD.CommandType = CommandType.StoredProcedure


            Dim sda As SqlDataAdapter = New SqlDataAdapter(CMD)
            sda.Fill(dtb)

            If dtb.Rows().Count > 0 Then
                'persno = dtb.Rows(0)!nik.ToString
                'outpatcovmax = dtb.Rows(0)!maxjln.ToString
                'outpatcovrem = (dtb.Rows(0)!maxjln - dtb.Rows(0)!Usage).ToString
                'framecov = dtb.Rows(0)!frame.ToString
                'lensnormcov = dtb.Rows(0)!mononormal.ToString
                'matcovnormal = dtb.Rows(0)!lhrnormal.ToString
                'matcovcaesar = dtb.Rows(0)!lhrcaesar.ToString
                year = Date.Today.Year.ToString
                anualleaveeli = CDec(dtb.Rows(0)!totleave).ToString("#.##")
                anualleaveeli_L = CDec(dtb.Rows(0)!lTotleave).ToString("#.##")

                anualleaveadd = CDec(dtb.Rows(0)!addleave).ToString("#.##")
                anualleaveusg = CDec(dtb.Rows(0)!usedleave).ToString("#.##")
                anualleaveusg_L = CDec(dtb.Rows(0)!lusedleave).ToString("#.##")

                anualleavefor = dtb.Rows(0)!burnleave.ToString
                anualleaverem = CDec((dtb.Rows(0)!totleave + dtb.Rows(0)!addleave) - (dtb.Rows(0)!usedleave + dtb.Rows(0)!burnleave)).ToString("#.##")
                anualleaverem_L = CDec((dtb.Rows(0)!ltotleave + dtb.Rows(0)!laddleave) - (dtb.Rows(0)!lusedleave + dtb.Rows(0)!lburnleave)).ToString("#.##")
                'fieldbrkeli = CDec(dtb.Rows(0)!AddLeaveField).ToString("#.##")
                'fieldbrkusd = CDec(dtb.Rows(0)!UsedLeaveField).ToString("#.##")
                'fieldbrkfor = CDec(dtb.Rows(0)!BurnLeaveField).ToString("#.##")
                'fieldbrkrem = CDec((dtb.Rows(0)!AddLeaveField) - (dtb.Rows(0)!UsedLeaveField + dtb.Rows(0)!BurnLeaveField)).ToString("#.##")

                'Dim usedleavelast As String = CDec(dtb.Rows(0)!Usedleavelast).ToString("#.##")
                'Dim remainlast As String = CDec(dtb.Rows(0)!RemainLast).ToString("#.##")

                'If usedleavelast = "" Then usedleavelast = "0"
                'If remainlast = "" Then remainlast = "0"

                'klo cutinya blm muncul sudah ambil cuti jd minus
                'If dtb.Rows(0)!ExpiredLeaveLast.ToString <> Nothing And dtb.Rows(0)!DateLeavePopLast.ToString <> Nothing Then
                '    caryfwdrem = usedleavelast - remainlast
                'Else
                '    caryfwdrem = 0 - usedleavelast
                'End If


                'If dtb.Rows(0)!ExpiredLeaveLast.ToString = "" Then
                'caryfwdrem = 0
                'Else
                'If dtb.Rows(0)!ExpiredLeaveLast.ToString = "" Then dtb.Rows(0)!ExpiredLeaveLast = "1/1/1900"
                'If dtb.Rows(0)!ExpiredLeaveLast < Today.Date Then
                '    If (dtb.Rows(0)!lasttotex - dtb.Rows(0)!lastusedleaveex) + (dtb.Rows(0)!TotleaveLast + dtb.Rows(0)!addleavelast) - (dtb.Rows(0)!usedLeaveLast + dtb.Rows(0)!burnleavelast) < 0 Then
                '        caryfwdrem = CDec((dtb.Rows(0)!lasttotex - dtb.Rows(0)!lastusedleaveex) + (dtb.Rows(0)!TotleaveLast + dtb.Rows(0)!addleavelast) - (dtb.Rows(0)!usedLeaveLast + dtb.Rows(0)!burnleavelast)).ToString("#.##")
                '    Else
                '        caryfwdrem = "0"
                '    End If
                'Else
                '    caryfwdrem = CDec((dtb.Rows(0)!lasttotex - dtb.Rows(0)!lastusedleaveex) + (dtb.Rows(0)!TotleaveLast + dtb.Rows(0)!addleavelast) - (dtb.Rows(0)!usedLeaveLast + dtb.Rows(0)!burnleavelast)).ToString("#.##")
                'End If

                caryfwdrem = CInt(dtb.Rows(0)!CarryFRemain)

                'LONG LEAVE
                'If dtb.Rows(0)!lExpiredLeaveLast.ToString = "" Then dtb.Rows(0)!lExpiredLeaveLast = "1/1/1900"
                'If dtb.Rows(0)!lExpiredLeaveLast < Today.Date Then
                '    If (dtb.Rows(0)!lTotleaveLast + dtb.Rows(0)!laddleavelast) - (dtb.Rows(0)!lusedLeaveLast + dtb.Rows(0)!lburnleavelast) < 0 Then
                '        caryfwdrem_L = CDec((dtb.Rows(0)!lTotleaveLast + dtb.Rows(0)!laddleavelast) - (dtb.Rows(0)!lusedLeaveLast + dtb.Rows(0)!lburnleavelast)).ToString("#.##")
                '    Else
                '        caryfwdrem_L = "0"
                '    End If
                'Else
                '    caryfwdrem_L = CDec((dtb.Rows(0)!lTotleaveLast + dtb.Rows(0)!laddleavelast) - (dtb.Rows(0)!lusedLeaveLast + dtb.Rows(0)!lburnleavelast)).ToString("#.##")
                'End If
                caryfwdrem_L = CInt(dtb.Rows(0)!lCarryFRemain)

                'End If

                If anualleaveeli = "" Then anualleaveeli = "0"
                If anualleaveeli_L = "" Then anualleaveeli_L = "0"
                If anualleaveusg = "" Then anualleaveusg = "0"
                If anualleaveusg_L = "" Then anualleaveusg_L = "0"
                If anualleavefor = "" Then anualleavefor = "0"
                If anualleaverem = "" Then anualleaverem = "0"
                If anualleaverem_L = "" Then anualleaverem_L = "0"
                If fieldbrkeli = "" Then fieldbrkeli = "0"
                If fieldbrkusd = "" Then fieldbrkusd = "0"
                If fieldbrkfor = "" Then fieldbrkfor = "0"
                If fieldbrkrem = "" Then fieldbrkrem = "0"
                If caryfwdrem = "" Then caryfwdrem = "0"
                If caryfwdrem_L = "" Then caryfwdrem_L = "0"

                If dtb.Rows(0)!DateLeavePop.ToString <> Nothing Then
                    anualleavedtap = CDate(dtb.Rows(0)!DateLeavePop).ToString("MM-dd-yyyy")
                End If

                'LONG LEAVE
                If dtb.Rows(0)!lDateLeavePop.ToString <> Nothing Then
                    anualleavedtap_L = CDate(dtb.Rows(0)!lDateLeavePop).ToString("MM-dd-yyyy")
                End If

                If dtb.Rows(0)!ExpiredLeave.ToString <> Nothing Then
                    anualleavedtfor = CDate(dtb.Rows(0)!ExpiredLeave).ToString("MM-dd-yyyy")
                End If
                If dtb.Rows(0)!lExpiredLeave.ToString <> Nothing Then
                    anualleavedtfor_L = CDate(dtb.Rows(0)!lExpiredLeave).ToString("MM-dd-yyyy")
                End If

                If dtb.Rows(0)!DateLeavePopLast.ToString <> Nothing Then
                    caryfwdap = CDate(dtb.Rows(0)!DateLeavePopLast).ToString("MM-dd-yyyy")
                End If

                If dtb.Rows(0)!lDateLeavePopLast.ToString <> Nothing Then
                    caryfwdap_L = CDate(dtb.Rows(0)!lDateLeavePopLast).ToString("MM-dd-yyyy")
                End If

                If dtb.Rows(0)!ExpiredLeaveLast.ToString <> Nothing Then
                    If dtb.Rows(0)!ExpiredLeaveLast = "1/1/1900" Then
                        caryfwdfor = ""
                    Else
                        caryfwdfor = CDate(dtb.Rows(0)!ExpiredLeaveLast).ToString("MM-dd-yyyy")
                    End If

                End If

                If dtb.Rows(0)!lExpiredLeaveLast.ToString <> Nothing Then
                    If dtb.Rows(0)!lExpiredLeaveLast = "1/1/1900" Then
                        caryfwdfor_L = ""
                    Else
                        caryfwdfor_L = CDate(dtb.Rows(0)!lExpiredLeaveLast).ToString("MM-dd-yyyy")
                    End If

                End If

            End If
        Catch ex As Exception
        Finally
            sqlConn.Close()
        End Try

        Try
            sqlConn.Open()
            Dim dtb2 As DataTable = New DataTable()

            'informasi karyawan
            Dim strcon2 As String = "SELECT H_A101.Nik,H_A101.Nama,H_A101.Gender,H_A101.TmpLahir,H_A101.TglLahir,H_A101.Alamat,H_A101.KdPos," & _
            "H_A101.kdtelp,H_A101.Telp,H_A101.NoHP,H_A101.Email,H_A101.Agama,H_A101.StSipil,H_A101.StMarital,H_A101.GolDarah,H_A101.NoKTP," & _
            "H_A101.TglExpiredKTP,H_A101.AlamatID,H_A101.KdPosID,H_A101.TypeSIM,H_A101.NoSIM,H_A101.TglExpiredSIM," & _
            "H_A101.UkuranBaju, H_A101.UkuranSepatu,H_A101.UkuranCelana,H_A101.KdSite,H_A101.KdDepar,H_A101.KdLevel,H_A101.KdSubLevel," & _
            "H_A101.TglMasuk,H_A101.tglEfektif,H_A101.EmpType,H_A101.TglAwalKontrak,H_A101.TglAkhirKontrak,H_A101.KontrakKe,H_A101.TglAwalTetap," & _
            "H_A101.FExpatriate, H_A101.FSchedulingShift,H_A101.Shift,H_A101.FSPL,H_A101.PointHire,H_A101.ContactPerson,H_A101.AlmPerson," & _
            "H_A101.TelpPerson,H_A101.HubPerson,H_A101.Active,H_A101.TglResign,H_A101.NoMutasi,H_A101.NoPromosi,H_A101.NoDemosi,H_A101.FHousing," & _
            "H_A101.FFlexible,H_A101.FHitungOT,H_A101.FNotRP,H_A101.ClaimPercentage,H_A101.PeriodeTimbulClaim,H_A101.PeriodeLastClaimFrame,H_A101.PeriodeLastClaimLens," & _
            "H_A101.OutpatientMax,H_A101.OutpatientClaim,H_A101.OutpatientSisa,H_A101.InpatientMax,H_A101.InpatientClaim,H_A101.InpatientSisa," & _
            "H_A101.GlassesMonoNormalMax,H_A101.GlassesMonoNormalClaim,H_A101.GlassesMonoNormalSisa,H_A101.GlassesMonoSilinderMax,H_A101.GlassesMonoSilinderClaim," & _
            "H_A101.GlassesMonoSilinderSisa,H_A101.GlassesBiNormalMax,H_A101.GlassesBiNormalClaim,H_A101.GlassesBiNormalSisa,H_A101.GlassesBiSilinderMax," & _
            "H_A101.GlassesBiSilinderClaim,H_A101.GlassesBiSilinderSisa,H_A101.GlassesFrameMax,H_A101.GlassesFrameClaim,H_A101.GlassesFrameSisa,H_A101.MaternityMax," & _
            "H_A101.MaternityClaim,H_A101.MaternitySisa,H_A101.nMaternity,H_A101.CreatedBy,H_A101.CreatedIn,H_A101.CreatedTime," & _
            "H_A101.ModifiedBy,H_A101.ModifiedIn,H_A101.ModifiedTime,H_A101.StEdit,H_A101.DeleteBy,H_A101.DeleteTime,H_A101.Kota," & _
            "H_A101.Propinsi,H_A101.KotaID,H_A101.PropinsiID,H_A101.KdJabatan,H_A130.KdDivisi,H_A140.NmDivisi,H_A101.Fcover,H_A101.FKirim," & _
            "H_A101.kdsublevelrit,H_A101.kdlokasi,H_A101.pos_code,H_A101.pycostcode,H_A101.old_nik,H_A101.niksite,H_A101.heirPerson," & _
            "H_A101.heiralm,H_A101.heirtelp,H_A101.heirhub,H_A101.email2, (select NmSite from H_A120 where KdSite = H_A101.KdSite) as Nmsite,(select NmCompany from H_A110 where KdCompany = (select KdCompany from H_A120 where KdSite = H_A101.kdsite)) as nmcompany, " & _
            "(select NmDepar from H_A130 where KdDepar = H_A101.KdDepar) as nmdepar, (select nmjabat from H_A150 where kdjabat = H_A101.KdJabatan) as nmjabat, " & _
            "(select NmLevel from H_A160 where KdLevel = H_A101.KdLevel) as nmlevel " & _
            "FROM H_A101,H_A130,H_A140 " & _
            "WHERE ( H_A101.Nik = '" + persno + "') and (H_A101.KdDepar=H_A130.KdDepar) and (H_A130.KdDivisi= H_A140.KdDivisi)"

            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn)
            sda2.Fill(dtb2)

            If dtb2.Rows().Count > 0 Then
                'site = dtb2.Rows(0)!nmsite.ToString
                'name = dtb2.Rows(0)!nama.ToString
                'nmcompany = dtb2.Rows(0)!nmcompany.ToString
                'nmdepar = dtb2.Rows(0)!nmdepar.ToString
                'nmdivisi = dtb2.Rows(0)!nmdivisi.ToString
                'nmjabat = dtb2.Rows(0)!nmjabat.ToString
                'nmlevel = dtb2.Rows(0)!nmlevel.ToString
                'tglefektif = dtb2.Rows(0)!tglefektif
                'tglmasuk = dtb2.Rows(0)!tglmasuk
                'emptype = dtb2.Rows(0)!emptype
            End If
        Catch ex As Exception

        End Try

    End Sub

End Class