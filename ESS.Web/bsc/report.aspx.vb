﻿Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms

Partial Public Class report
    Inherits System.Web.UI.Page

    Dim globalID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim jnsReport As String = Request.QueryString("jns")
            Dim id As String = Request.QueryString("id")
            globalID = id
            If jnsReport = "sectionsummary" Then
                AddHandler rpt.LocalReport.SubreportProcessing, AddressOf Me.SubreportProcessingEventHandler
                MakeFunctionSummary(id)
            End If
        Catch ex As Exception
            lbwarning.Text = ex.Message
        End Try
    End Sub

    Public Sub SubreportProcessingEventHandler(ByVal sender As Object, ByVal e As SubreportProcessingEventArgs)
        Try
            Dim sqlquery As String = "Select idref from bscmasterref where id='" & globalID & "'"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim cmsql As New SqlCommand(sqlquery, conn)
            Dim result As String = ""
            conn.Open()
            result = cmsql.ExecuteScalar()
            sqlquery = "select a.monthid,a.jobsiteid,isnull(b.target,0) as target,isnull(b.actual,0) as actual from " & _
            "(select * from month,jobsite) a " & _
            "left join (select * from bsctransaction where id ='" & result & "')b " & _
            "on a.monthid=b.month and a.jobsiteid=b.site"
            conn.Close()
            Dim da As New SqlDataAdapter(sqlquery, conn)
            Dim dt As New bscds.sectionsummaryDataTable()
            da.Fill(dt)
            e.DataSources.Add(New ReportDataSource("bscds_sectionsummary", dt))
        Catch ex As Exception
            lbwarning.Text = ex.Message
        End Try
    End Sub

    Sub MakeFunctionSummary(ByVal id As String)
        Try
            Dim sqlquery As String
            Dim plist As New Generic.List(Of ReportParameter)
            sqlquery = "Select kpi,workunit,level from bscmaster where id='" & id & "'"
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim reader As SqlDataReader
            Dim result As String = ""
            Dim cmsql As New SqlCommand(sqlquery, conn)
            conn.Open()
            reader = cmsql.ExecuteReader()
            If reader.HasRows Then
                While reader.Read
                    result = "KPI : "
                    result += reader(0) + " " + reader(1)
                    Select Case reader(2)
                        Case 0
                            result += "(JRN)"
                        Case 1
                            result += "(FUNCTION)"
                        Case 2
                            result += "(SECTION)"
                        Case 3
                            result += "(JOBSITE)"
                    End Select
                End While
            End If
            Dim param1 As New ReportParameter("namakpi", result)
            conn.Close()
            plist.Add(param1)
            sqlquery = "select a.monthid,isnull(b.target,0) as target,isnull(b.actual,0) as actual from " & _
                                 "(select * from month) a " & _
                                 "left join (select * from bsctransaction where id ='" & globalID & "')b " & _
                                 "on a.monthid=b.month"
            Dim da As New SqlDataAdapter(sqlquery, conn)
            Dim dt As New bscds.functionsummaryDataTable()
            da.Fill(dt)
            rpt.LocalReport.ReportPath = "Laporan\rptFunctionHead.rdlc"
            rpt.LocalReport.DataSources.Clear()
            rpt.LocalReport.SetParameters(plist)
            rpt.LocalReport.DataSources.Add(New ReportDataSource("bscds_functionsummary", dt))
            rpt.LocalReport.Refresh()
        Catch ex As Exception
            lbwarning.Text = ex.Message
        End Try
    End Sub

End Class