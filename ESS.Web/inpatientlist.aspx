﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="inpatientlist.aspx.vb" Inherits="EXCELLENT.inpatientlist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Inpatient List
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
<caption style="text-align: center; font-size: 1.5em; color:White;">Inpatient List</caption>
<tr>
<td style="width:5%;">Periode</td>
<td style="width:8%;">
<asp:DropDownList ID="ddlbulan" runat="server"></asp:DropDownList>
<asp:TextBox ID="tripfrom" style="width:70px;" runat="server" width="90px" ReadOnly="true" Visible ="false"></asp:TextBox></td>
<td style="width:5%;"><asp:DropDownList ID="ddltahun" runat="server"></asp:DropDownList></td>
<td style="width:5%;"><asp:Button ID="btnSearch" runat="server" Text="Search"/></td>
<td style="width:5%;"><%--<input type="button" id="cetak" value="Print" onclick="frmcetak()" style="width: 80px; font-family: Calibri; " />--%></td>
<td>
 <asp:TextBox ID="tripto" style="width:70px;" runat="server" ReadOnly="true" visible="false"></asp:TextBox>
</td>
<td></td>
</tr></table>

<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="NoReg" AllowPaging="true" PageSize="100" Width="1200px">
    <RowStyle Font-Size="Smaller" />
<Columns>
<asp:CommandField ShowSelectButton="True" SelectText="Select" ButtonType="Button"  
        HeaderStyle-Width="40px" ItemStyle-VerticalAlign="top" >
<HeaderStyle Width="40px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:CommandField>
 <asp:BoundField DataField="NoReg" HeaderText="No Reg" 
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
<HeaderStyle Width="100px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="Tgltrans" HeaderText="Tanggal" 
        HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="90px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
     <asp:BoundField DataField="nama" HeaderText="Nama" 
        HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="200px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="Nmdepar" HeaderText="Departement" 
        HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="200px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="fstatus" HeaderText="Status" 
        HeaderStyle-Width="120px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="120 px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
            <asp:BoundField DataField="pycostcode" HeaderText="Company Costcode" 
        HeaderStyle-Width="300px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="300px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
</Columns> 
    <SelectedRowStyle BackColor="#99CCFF" />
    <HeaderStyle BackColor="#0099FF" />
</asp:GridView>
</asp:Content>