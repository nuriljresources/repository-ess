﻿Imports System.Data.SqlClient

Partial Public Class mgmnt_user_entry
    Inherits System.Web.UI.Page
    Public msgbox As String
    Public mnentry As String
    Public mnview As String
    Public mnmgmt As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'mnentry = "<div id='mnentry'><img alt='open document' src='../images/open_doc_small.png' /> <a href='entry_permit_doc.aspx'>&nbsp;Upload Document</a></div>"
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)

        'If Session("docedituser") <> "" Then
        '    Dim strdocuser As String = "select * from ms_user where emp_code = '" + Session("docedituser") + "'"

        '    Dim dtb As DataTable = New DataTable
        '    Dim sda As SqlDataAdapter = New SqlDataAdapter(strdocuser, sqlConn)

        '    sda.Fill(dtb)

        '    If dtb.Rows.Count >= 0 Then
        '        DropDownList1.SelectedValue = Trim(dtb.Rows(0)!emp_code.ToString)
        '        ddllvl.SelectedValue = Trim(dtb.Rows(0)!lvl.ToString)
        '        ddlmodul.SelectedValue = Trim(dtb.Rows(0)!modul.ToString)
        '    End If
        'End If

        If Session("otorisasi_permit") = Nothing Or Session("otorisasi_permit") = "" Then
            Response.Redirect("signin_permit.aspx")
        End If

        If Not IsPostBack Then
            If Trim(Session("permission_permit")) = "1" Or Trim(Session("permission_permit")) = "2" Or Trim(Session("permission_permit")) = "4" Then
                mnentry = "<div id='mnentry'><img alt='entry document' src='../images/open_doc_small.png' /> <a href='doc_center_entry.aspx'>&nbsp;Set Document</a></div>"
                mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
            End If

            If Trim(Session("permission_permit")) = "1" Then
                mnmgmt = "<div id='mnmgmt'> <img alt='Management User' src='../images/open_doc_small.png' /> <a href='mgmnt_list.aspx'>&nbsp;Management User</a></div>"
            End If

            If Trim(Session("permission_permit")) = "3" Or Trim(Session("permission_permit")) = "4" Then
                mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
            End If
        End If

        If Trim(Session("modul_permit")) <> "IT" Then
            ddlmodul.SelectedValue = Session("modul_permit")
            ddlmodul.Enabled = False

        Else
            'ddlmodul.SelectedValue = Session("modul_permit")
            ddllvl.Items.Add(New ListItem("Reminder Only", "R"))
        End If

    End Sub

    Protected Sub Save_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Save.Click
        Dim dtbfindemp As DataTable = New DataTable
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim cmd As SqlCommand
        'If Session("docedituser") <> "" Or Session("docedituser") <> Nothing Then

        '    Try
        '        conn.Open()
        '        Dim sqlQuery As String = "UPDATE ms_user SET emp_code = '" + DropDownList1.SelectedValue + "', employee_nik = '" + DropDownList1.SelectedValue + "', name = '" + DropDownList1.SelectedItem.ToString + "', lvl = '" + ddllvl.SelectedValue + "', modul = '" + ddlmodul.SelectedValue + "' where emp_code = '" + DropDownList1.SelectedValue + "'"
        '        cmd = New SqlCommand(sqlQuery, conn)
        '        cmd.ExecuteScalar()
        '        msgbox = "<script language='javascript' type='text/javascript'> alert('data has been updated') </script>"
        '        Session("docedituser") = ""
        '    Catch ex As Exception
        '        msgbox = "<script language='javascript' type='text/javascript'> alert('failed to update data') </script>"
        '    Finally
        '        conn.Close()
        '    End Try

        'Else
        name.Text = nm.Text
        If name.Text = "" Then
            msgbox = "<script language='javascript' type='text/javascript'> alert('Name can not empty') </script>"
            menu()
            Exit Sub
        End If

        Try
            conn.Open()
            Dim sqlfindemp As String = "select emp_code, stedit, modul from ms_user where emp_code = '" + nik.Text + "' and modul = '" + ddlmodul.SelectedValue.ToString + "'"
            Dim sdafindemp As SqlDataAdapter = New SqlDataAdapter(sqlfindemp, conn)
            sdafindemp.Fill(dtbfindemp)

            If dtbfindemp.Rows.Count > 0 Then
                If dtbfindemp.Rows(0)!stedit = "2" Then
                    Dim sqlQuery As String = "update ms_user set stedit = '0', lvl = '" + ddllvl.SelectedValue + "', modul = '" + ddlmodul.SelectedValue + "' where emp_code = '" + nik.Text + "' and modul = '" + ddlmodul.SelectedValue + "'"
                    cmd = New SqlCommand(sqlQuery, conn)
                    cmd.ExecuteScalar()
                    msgbox = "<script language='javascript' type='text/javascript'> alert('data has been stored') </script>"
                Else
                    msgbox = "<script language='javascript' type='text/javascript'> alert('user already exist in the database') </script>"
                End If
            Else
                Dim sqlQuery As String = "INSERT INTO ms_user (emp_code, employee_nik, name, lvl, stamp, usr, modul, stedit) VALUES ('" + nik.Text + "', '" + nik.Text + "', '" + name.Text + "', '" + ddllvl.SelectedValue + "', '" + DateTime.Now.ToString + "', '" + Session("otorisasi_permit").ToString + "', '" + ddlmodul.SelectedValue + "', '0')"
                cmd = New SqlCommand(sqlQuery, conn)
                cmd.ExecuteScalar()
                msgbox = "<script language='javascript' type='text/javascript'> alert('data has been stored') </script>"
            End If

        Catch ex As Exception
            msgbox = "<script language='javascript' type='text/javascript'> alert('failed to stored data') </script>"
        Finally
            conn.Close()
        End Try
        'End If

        menu()
    End Sub

    Private Sub menu()
        If Trim(Session("permission_permit")) = "1" Or Trim(Session("permission_permit")) = "2" Then
            mnentry = "<div id='mnentry'><img alt='entry document' src='../images/open_doc_small.png' /> <a href='doc_center_entry.aspx'>&nbsp;Set Document</a></div>"
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
        End If

        If Trim(Session("permission_permit")) = "1" Then
            mnmgmt = "<div id='mnmgmt'> <img alt='Management User' src='../images/open_doc_small.png' /> <a href='usermgmnt.aspx'>&nbsp;Management User</a></div>"
        End If

        If Trim(Session("permission_permit")) = "3" Or Trim(Session("permission_permit")) = "4" Then
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
        End If
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        'response.Redirect("Http://trip.jresources.com:8082/permit/mgmnt_list.aspx")
        Response.Redirect("mgmnt_list.aspx")
    End Sub
End Class