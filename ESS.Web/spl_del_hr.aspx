﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="spl_del_hr.aspx.vb" Inherits="EXCELLENT.spl_del_hr" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Overtime HR Delegation</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script src="Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="Scripts/jscal2.js" type ="text/javascript"></script>
    <script src="Scripts/en.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="css/win2k/win2k.css" />
    <link rel="stylesheet" href="css/jquery.tooltip/jquery.tooltip2.css" type="text/css" />
    <script type="text/javascript" src="Scripts/tooltip/jquery.min.js"></script>
    <script type="text/javascript" src="Scripts/tooltip/jquery.tooltip.js"></script>
    <script src="Scripts/jquery.timeentry.js" type="text/javascript"></script>
    
    <script type="text/javascript" >
        function OpenPopup(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("searchemployeespl.aspx?k=delhr", "List", "scrollbars=yes,resizable=no,width=600,height=500");
            return false;
        }
    </script>
    
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="ctrlToFind" />
    <table class="accordionContent" style="margin-left:10px;">
        <caption class="accordionHeader">Delegate Approval From:</caption>
         <tr>
            <td>
                Nik    
            </td>
            <td>
                <asp:HiddenField ID="hid" runat="server" />  
                <asp:TextBox ID="txtnikreq" runat="server"></asp:TextBox>    
                <img alt="add" id="Img1" src="images/Search.gif" align="absmiddle" style="cursor: pointer;" onclick="OpenPopup('spldel')" />
            </td>
        </tr>
        <tr>
            <td style="padding:1px 60px 1px 1px;">
                 Name       
            </td>
            <td>
                 <asp:HiddenField ID="hnikreq" runat="server" /> <asp:HiddenField ID="hkdsitereq" runat="server" /> <asp:HiddenField ID="hkddeparreq" runat="server" /> <asp:HiddenField ID="hkdjabatreq" runat="server" />
                 <asp:TextBox ID="txtnamareq" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
<table class="accordionContent" style="margin-left:10px;">
    <caption class="accordionHeader">Delegate Approval To:</caption>
       
        <tr>
            <td>
                Nik    
            </td>
            <td>
                <asp:HiddenField ID="hid2" runat="server" />  
                <asp:TextBox ID="txtnikreq2" runat="server"></asp:TextBox>    
                <img alt="add" id="Img2" src="images/Search.gif" align="absmiddle" style="cursor: pointer;" onclick="OpenPopup('spldel2')" />
            </td>
        </tr>
        <tr>
            <td style="padding:1px 60px 1px 1px;">
                 Name       
            </td>
            <td>
                 <asp:HiddenField ID="hnikreq2" runat="server" /> <asp:HiddenField ID="HiddenField3" runat="server" /> <asp:HiddenField ID="HiddenField4" runat="server" /> <asp:HiddenField ID="HiddenField5" runat="server" />
                 <asp:TextBox ID="txtnamareq2" runat="server"></asp:TextBox>
            </td>
        </tr>
       
    </table>
    
    <table class="accordionContent" style="margin-left:10px;">
    <caption class="accordionHeader">Date:</caption>
        <tr>
            <td>
                From 
            </td>
            <td>
                <asp:TextBox ID="txtdtfrom" runat = "server"></asp:TextBox><button id="btn1">...</button>
            </td>
        </tr>
       <tr>
            <td>
                To 
            </td>
            <td>
                <asp:TextBox ID="Txtdtto" runat = "server"></asp:TextBox><button id="btn2">...</button>
            </td>
       </tr>
    </table>
    <br />
    <asp:Button ID="btnsave" runat="server" Text="Save"/> <asp:Button ID="btnnew" runat="server" Text="New Entry" /> <asp:Button ID="btndelete" runat="server" Text="Delete"/>
    <br />
    <table>
        <tr>
        <td style="width:5%;"> </td>
          <td>
            <asp:DropDownList ID="ddlbulan" runat="server">
                <asp:ListItem Value = "1">January</asp:ListItem>
                <asp:ListItem Value = "2">February</asp:ListItem>
                <asp:ListItem Value = "3">March</asp:ListItem>
                <asp:ListItem Value = "4">April</asp:ListItem>
                <asp:ListItem Value = "5">May</asp:ListItem>
                <asp:ListItem Value = "6">June</asp:ListItem>
                <asp:ListItem Value = "7">July</asp:ListItem>
                <asp:ListItem Value = "8">August</asp:ListItem>
                <asp:ListItem Value = "9">September</asp:ListItem>
                <asp:ListItem Value = "10">October</asp:ListItem>
                <asp:ListItem Value = "11">November</asp:ListItem>
                <asp:ListItem Value = "12">December</asp:ListItem>
            </asp:DropDownList>
            
            <asp:DropDownList ID="ddltahun" runat="server"></asp:DropDownList>
            <asp:Button ID="btnSearch" runat="server" Text="Search"/>
          </td>
            <td style="width:5%;"></td>
          <td style="width:5%;"></td>
        </tr>
    </table>
    <br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="delegation_id" AllowPaging="true" PageSize="100" Width="1200px">
    <RowStyle Font-Size="Smaller" />
<Columns>

<asp:BoundField DataField="delegation_id" HeaderText="Trans Number" Visible="false" 
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
 </asp:BoundField>
 
 <asp:BoundField DataField="niksite" HeaderText="Nik" HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="200px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
 
<asp:BoundField DataField="nama" HeaderText="Nama" 
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
 </asp:BoundField>
    <asp:BoundField DataField="date_from" HeaderText="Date From" HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="90px"></HeaderStyle>
<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    
     <asp:BoundField DataField="date_to" HeaderText="Date To" HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top">
<HeaderStyle Width="200px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    
    <%--<asp:TemplateField>
        <ItemTemplate>
            <asp:CheckBox ID="chkbox1" runat="server" />
        </ItemTemplate>
    </asp:TemplateField>--%>

<asp:CommandField ShowSelectButton="True" SelectText="Select" ButtonType="Button"  HeaderStyle-Width="40px" ItemStyle-VerticalAlign="top" >
<HeaderStyle Width="40px"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:CommandField>
</Columns> 
    <SelectedRowStyle BackColor="#99CCFF" />
    <HeaderStyle BackColor="#0099FF" />
</asp:GridView>


<div style="position:absolute; top:10px; left:400px">
<p><%=msg2 %></p>
<asp:Button ID="btnsel" runat="server" Text="Select All"/> <asp:Button ID="btndsel" runat="server" Text="Diselect All"/> 
<br />
    <asp:GridView ID="gridview2" AutoGenerateColumns="False" runat="server" DataKeyNames="recordid" PageSize="200">
    <Columns>
        <asp:BoundField DataField="recordid" HeaderText="recordid" Visible="false">
            <HeaderStyle Width="200px"></HeaderStyle>

            <ItemStyle VerticalAlign="Top"></ItemStyle>
        </asp:BoundField>
    
        <asp:BoundField DataField="Nik" HeaderText="Nik" HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top">
            <HeaderStyle Width="200px"></HeaderStyle>

            <ItemStyle VerticalAlign="Top"></ItemStyle>
        </asp:BoundField>
        
        <asp:BoundField DataField="Nama" HeaderText="Name" HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top">
            <HeaderStyle Width="200px"></HeaderStyle>

            <ItemStyle VerticalAlign="Top"></ItemStyle>
        </asp:BoundField>
        
        <asp:TemplateField>
            <ItemTemplate>
                <asp:CheckBox ID="chkboxss" runat="server" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>

<asp:Button id="btnchk" runat = "server" Text = "Procees"/>   
</div>
 
    <script type="text/javascript" >
        var today = new Date();
        var cal = Calendar.setup({
            onSelect: function(cal) { cal.hide() },
            showTime: false,
            min: Calendar.intToDate(today)
        });

        cal.manageFields("btn1", "ctl00_ContentPlaceHolder1_txtdtfrom", "%m/%d/%Y");
        cal.manageFields("btn2", "ctl00_ContentPlaceHolder1_Txtdtto", "%m/%d/%Y");
    </script>

<%=msg%>
</asp:Content>