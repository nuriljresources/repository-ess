﻿Public Partial Class gauge
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'permission.Value = Session("permission")
            If Session("permission") = "" Or Session("permission") = Nothing Then
                Response.Redirect("../Login.aspx", False)
                Exit Sub
            ElseIf ("9").IndexOf(Session("permission")) <> -1 Then 'Others ga bisa akses
                Response.Redirect("../default.aspx", False)
                Exit Sub
            End If
            jobsite.Value = Session("jobsite")
            permission.Value = Session("permission")
            Dim year As String = Now.Year
            Dim month As String = Now.Month

            If Now.Month = 1 Then
                year = Now.Year - 1
                month = 12
            Else
                month = Now.Month - 1
            End If

            For Each s As ListItem In ddlYear.Items
                If s.Value = year Then
                    s.Selected = True
                End If
            Next
            For Each s As ListItem In ddlMonth.Items
                If s.Value = month Then
                    s.Selected = True
                End If
            Next
        Catch ex As Exception

        End Try
    End Sub

End Class