﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="cetak_leave.aspx.vb" Inherits="EXCELLENT.cetak_leave" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="txtleaveid" runat="server" Visible="false"></asp:TextBox><br />
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" Height="605px" Width="915px">
            <LocalReport ReportPath="Laporan\cetak_leave.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                        Name="DataSet7_L_H001" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            DeleteMethod="Delete" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            TypeName="EXCELLENT.DataSet7TableAdapters.L_H001TableAdapter">
            <DeleteParameters>
                <asp:Parameter Name="Original_transid" Type="String" />
            </DeleteParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="txtleaveid" Name="argReg" PropertyName="Text" 
                    Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        
    </div>
    </form>
</body>
</html>
