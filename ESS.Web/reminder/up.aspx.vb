﻿Imports System.Net
Imports System.IO
Imports Microsoft.SharePoint.Client
Imports System.Collections.Generic
Imports System.Linq
Imports System.Reflection
Imports System.Security.Principal
Imports System.Text
Imports Microsoft.SharePoint.Client.Utilities
Imports File = Microsoft.SharePoint.Client.File
Imports compDocCenter
Imports compDocCenter.cDoc

Partial Public Class up
    Inherits System.Web.UI.Page
    Private clientContext As ClientContext
    Private rootWeb As Web

    Public Sub s(ByVal url As String, ByVal username As String, ByVal password As String)
        clientContext = New ClientContext(url)
        Dim credentials = New NetworkCredential(username, password, "jresources")
        clientContext.Credentials = credentials
        rootWeb = clientContext.Web
        clientContext.Load(rootWeb)
    End Sub

    Public Function GetWebByTitle(ByVal siteTitle As String) As Web
        Dim query = clientContext.LoadQuery(rootWeb.Webs.Where(Function(p) p.Title = siteTitle))
        'Dim query As CamlQuery 
        clientContext.ExecuteQuery()
        Return query.FirstOrDefault()
    End Function

    Public Function GetDocumentLibrary(ByVal siteTitle As String, ByVal libraryName As String) As List
        Dim web = GetWebByTitle(siteTitle)
        If web IsNot Nothing Then
            Dim query = clientContext.LoadQuery(web.Lists.Where(Function(p) p.Title = libraryName))
            clientContext.ExecuteQuery()
            Return query.FirstOrDefault()
        End If
        Return Nothing
    End Function

    Public Function ListFiles(ByVal siteTitle As String, ByVal libraryName As String) As List(Of File)
        Dim list = GetDocumentLibrary(siteTitle, libraryName)
        Dim files = list.RootFolder.Files
        clientContext.Load(files)
        clientContext.ExecuteQuery()
        Return files.ToList()
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim olist As Microsoft.SharePoint.Client.ListItemCollection
        Dim cclass As cDoc = New cDoc()
        Dim myDt As DataTable
        Dim i As Integer

        myDt = New DataTable()
        myDt = CreateDataTable()
        Session("myDatatable") = myDt

        cclass.vGetListDocument("Syam.kharisman", "sysyam356", "jresources.com", "http://moss.jresources.com/hr", "Documents", olist, "/hr")

        For Each list As ListItem In olist
            i = i + 1
            'lbltes.Text = lbltes.Text + list("Title") + "<BR>"
            'FileRef
            'FileLeafRef
            AddDataToTable(i, list("FileRef"), list("FileLeafRef"), CType(Session("myDatatable"), DataTable))

            Me.GridView1.DataSource = CType(Session("myDatatable"), DataTable).DefaultView
            Me.GridView1.DataBind()
        Next

        'vGetListDocument("Syam.kharisman", "sysyam356", "jresources", "http://moss.jresources.com", "HR", olist, "Documents")

        'Dim strurl As String = "http://moss.jresources.com/HR/"

        'Dim clientcontext As ClientContext = New ClientContext(strurl)
        'clientcontext.Credentials = New NetworkCredential("syam.kharisman", "sysyam356", "jresources")
        'Dim website As Web = clientcontext.Web
        'Dim targetlist As List = website.Lists.GetByTitle("Documents")



        'Dim query As CamlQuery = New CamlQuery()

        'query.ViewXml = "<QueryOptions><ViewAttributes Scope='RecursiveAll' /></QueryOptions><Where><Eq><FieldRef Name='FSObjType' /><Value Type='Integer'>0</Value></Eq></Where>"
        'Dim itemcreateinfo As ListCreationInformation = New ListCreationInformation()
        'Dim collListItem As ListItemCollection = targetlist.GetItems(query)

        'returnValue = collListItem(i)
        'clientcontext.Load(website)


        'clientcontext.Load(targetlist)
        'clientcontext.Load(collListItem)
        'clientcontext.ExecuteQuery()


        'lbltes.Text = targetlist.GetItems
        'Dim i As Integer = 1

        'Dim instance As ClientObjectCollection
        'Dim instance As ListItemCollection
        'Dim id As Integer
        'Dim returnValue As ListItem

        'For Each targetListItem As ListItem In collListItem
        '    'lbltes.Text = targetListItem("ID")
        '    lbltes.Text = targetListItem("Name")
        'Next

    End Sub

    Private Sub uploadFile(ByVal FTPAddress As String, ByVal filePath As String, ByVal username As String, ByVal password As String, ByVal fullpath As String)
        Try
            Dim request As FtpWebRequest = DirectCast(FtpWebRequest.Create(FTPAddress & "/" & Path.GetFileName(filePath)), FtpWebRequest)

            request.Method = WebRequestMethods.Ftp.UploadFile
            request.Credentials = New NetworkCredential(username, password)
            request.UsePassive = True
            request.UseBinary = True
            request.KeepAlive = False

            'Load the file
            'Dim stream As FileStream = File.OpenRead(fullpath)
            'Dim stream As FileStream = FileUpload1.PostedFile.InputStream
            Dim fStream As Stream = FileUpload1.PostedFile.InputStream
            'stream.Seek(0, SeekOrigin.Begin)
            Dim buffer As Byte() = New Byte(CInt(fStream.Length - 1)) {}

            fStream.Read(buffer, 0, buffer.Length)
            fStream.Close()

            'Upload file
            Dim reqStream As Stream = request.GetRequestStream()
            reqStream.Write(buffer, 0, buffer.Length)
            reqStream.Close()
            err.Text = "success"
            'MsgBox("Uploaded Successfully", MsgBoxStyle.Information)
        Catch e As Exception
            err.Text = e.Message.ToString()
            'MsgBox("Failed to upload.Please check the ftp settings" + "Reason: " + e.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Button1.Enabled = False

        uploadFile("ftp://20.110.1.6/syam", FileUpload1.FileName, "syam", "r3d0.678", FileUpload1.PostedFile.FileName)
        Button1.Enabled = True
    End Sub

    'Public Sub LoadList()


    '    Dim ctx As New ClientContext("http://moss.jresources.com/HR/")


    '    Dim web = ctx.Web



    '     Let's only work with the specific List object and save resources

    '     by not fetching any other objects

    '    Dim list = ctx.Web.Lists.GetByTitle("Documents")


    '     Load only the list variable and execute the query

    '    ctx.Load(list)

    '    ctx.ExecuteQuery()


    '    Dim camlQuery As New Microsoft.SharePoint.Client.CamlQuery()


    '    camlQuery.ViewXml = "<View>" & vbCr & vbLf & vbCr & vbLf & "                        <Query>" & vbCr & vbLf & vbCr & vbLf & "                                  <Where>" & vbCr & vbLf & vbCr & vbLf & "                                <Eq>" & vbCr & vbLf & vbCr & vbLf & "                                  <FieldRef Name='Name'/>" & vbCr & vbLf & vbCr & vbLf & "                                  <Value Type='Text'>Shailesh</Value>" & vbCr & vbLf & vbCr & vbLf & "                                </Eq>" & vbCr & vbLf & vbCr & vbLf & "                                  </Where>" & vbCr & vbLf & vbCr & vbLf & "                        </Query>" & vbCr & vbLf & vbCr & vbLf & "                              </View>"

    '    Dim listItems As ListItemCollection = list.GetItems(camlQuery)

    '    ctx.Load(listItems)

    '    ctx.ExecuteQuery()


    '    Now you can iterate listitems collection and can get listitems using foreach loop


    '    For Each listItem As Microsoft.SharePoint.Client.ListItem In listItems

    '        you can get value of list column from listitem as follow:

    '        lbltes.Text = listItem("Name")

    '        listitem["Name"]



    '    Next
    'End Sub

    Private Function CreateDataTable() As DataTable
        Dim myDataTable As DataTable = New DataTable()

        Dim myDataColumn As DataColumn

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "fname"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "path"
        myDataTable.Columns.Add(myDataColumn)

        Return myDataTable
    End Function

    Private Sub AddDataToTable(ByVal num As Integer, ByVal fname As String, ByVal path As String, ByVal myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("No") = num
        row("fname") = fname
        row("path") = path

        myTable.Rows.Add(row)

    End Sub

End Class