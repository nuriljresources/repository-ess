﻿Imports System.Linq

Partial Public Class TestInputForm
    Inherits System.Web.UI.Page

    Class TestInputData

        Public TransNo As String
        Public Company As String
        Public Division As String
        Public Location As String

        Public Bank As String
        Public Address As Integer
        Public Site As String
        Public Remark As String

        Public TestInputDataDetails As New List(Of TestInputDataDetail)

    End Class

    Class TestInputDataDetail
        Public DateInStr As String
        Public DateIn As Date
        Public DateOutStr As String
        Public DateOut As Date
        Public Days As Integer
        Public Name As String
        Public RoomDay As Double
        Public Consultation As Double
        Public Medicine As Double
        Public Lab As Double
        Public Other As Double
        Public Total As Double
    End Class

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub


End Class