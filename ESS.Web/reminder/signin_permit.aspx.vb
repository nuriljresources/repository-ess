﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Security.Cryptography
Imports System.IO
Imports System.Text
Imports System.Configuration
Imports System.Collections.Generic
Imports System.Linq
Imports System.DirectoryServices  'Note dicek perlu ditick pada add reference
Imports System.Net.Mail
Partial Public Class signin_permit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Redirect("http://portal.jresources.com/")
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLogin.Click
        Dim msg As String
        Dim dept As String
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim empConn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
        Dim status As String = ""
        If Trim(Me.TextBox2.Text) = "" Then
            ' Me.lblConfirm.Text = "Harap Isi Password Anda"
            Me.lblConfirm.Text = "Password Should Not Empty Please Fill Your Password"
            Exit Sub
        End If

        Dim ls_user, ls_pass, lsErr As String
        Dim lsnik As String
        Dim lbl_login As Boolean = False
        Dim Gsuser As String

        Dim index As Integer = TextBox1.Text.IndexOf("\")
        Dim dm As String
        Dim nm As String
        Dim count As Integer
        If index > 0 Then
            count = TextBox1.Text.Length
            dm = TextBox1.Text.Substring(0, index)
            nm = TextBox1.Text.Substring(index, count - index)
            nm = Replace(nm, "\", "")
        End If

        'ls_user = Mid(TextBox1.Text, 12)
        ls_pass = TextBox2.Text
        If status = "" Then
            Dim domain As String = Left(TextBox1.Text, 10)
            lbl_login = ValidateActiveDirectoryLogin(domain, nm, ls_pass, lsnik, lsErr)

            If lbl_login = True Then
                'Check login id.
                If lsnik = "" Then
                    'MsgBox("Nik user name : " + ls_user + " not found,Please inform SysAdmin Check user domain setting.", MsgBoxStyle.Exclamation, "Confirmation")
                    Me.lblConfirm.Text = "Nik user name : " + ls_user + " not found,Please inform SysAdmin Check user domain setting."
                    Return
                Else
                    Gsuser = lsnik  ' GsUser yg dipakai untuk tampung NIK user dlm transaksi
                    'MsgBox("Sukses login masuk", MsgBoxStyle.Exclamation, Gsuser)
                    'Masuk Form Login
                    Me.lblConfirm.Text = "Login Success"
                End If
            Else
                'MsgBox(lsErr, MsgBoxStyle.Exclamation, "Confirmation")
                'Me.lblConfirm.Text = lsErr + " " + "user : " + ls_user + " " + "password : " + ls_pass + " " + "nik : " + lsnik + " domain: " + Domain
                Me.lblConfirm.Text = lsErr
                Return
            End If
        End If

        'cek nik user didatabase
        Try
            'lsnik = "JRN0114"
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()
            'HashString
            'Dim strcon As String = "select kduser,nmuser,kdsite,kddivisi,kddepar, kdlevel from B_C012 where kduser = '" + Replace(Me.TextBox1.Text, "'", "") + "' and password = '" + SecureIt.Secure.Encrypt(Replace(Me.TextBox2.Text, "'", "")) + "' and stedit in ('0','1')"
            'Dim strcon As String = "select kduser,nmuser,kdsite,kddivisi,kddepar, kdlevel from B_C012 where kduser = '" + Replace(Me.TextBox1.Text, "'", "") + "' and password = '" + HashString(Replace(Me.TextBox2.Text, "'", "")) + "' and stedit in ('0','1')"
            Dim strcon As String = "select emp_code,employee_nik,name,lvl,modul from ms_user where emp_code = '" + Replace(lsnik, "'", "") + "' and stedit <> '2'"
            'untuk tes
            'strcon = "select emp_code,employee_nik,name,lvl,modul from ms_user where emp_code = '" + Replace(TextBox1.Text, "'", "") + "' and stedit <> '2'"

            'Dim strcon As String = "select emp_code,employee_nik,name,lvl from ms_user where emp_code = '" + Replace(lsnik, "'", "") + "'"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            Dim dtb2 As DataTable = New DataTable()
            'Dim str As String = "select (select nmdepar from H_A130 where KdDepar = H_A101.KdDepar) as nmdepar from JRN_TEST.dbo.H_A101 where NIKSITE ='" + Replace(lsnik, "'", "") + "'"
            Dim str As String = "select (select nmdepar from H_A130 where KdDepar = H_A101.KdDepar) as nmdepar, (SELECT KdDivisi from H_A130 where KdDepar = H_A101.KdDepar) as divisi from JRN_TEST.dbo.H_A101 where NIKSITE ='" + Replace(lsnik, "'", "") + "'"
            'untuk tes
            'str = "select (select nmdepar from H_A130 where KdDepar = H_A101.KdDepar) as nmdepar, (SELECT KdDivisi from H_A130 where KdDepar = H_A101.KdDepar) as divisi from JRN_TEST.dbo.H_A101 where NIKSITE ='" + Replace(TextBox1.Text, "'", "") + "'"

            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(str, empConn)
            sda2.Fill(dtb2)

            If dtb.Rows.Count = 0 Then
                Me.lblConfirm.Text = "Please Check Again your login ..."
                sqlConn.Close()
                Exit Sub
            End If

            If dtb2.Rows.Count = 0 Then
                dept = ""
                sqlConn.Close()
                Exit Sub
            Else
                dept = dtb2.Rows(0)("nmdepar").ToString
            End If

            If Session("otorisasi_permit") = "" Then
                Session("dept") = dept
                Session("otorisasi_permit") = dtb.Rows(0)("emp_code").ToString
                Session("permission_permit") = dtb.Rows(0)("lvl").ToString
                Session("NmUser_permit") = dtb.Rows(0)("name").ToString
                Session("divisi") = dtb2.Rows(0)("divisi").ToString
                Session("modul_permit") = dtb.Rows(0)("modul").ToString
            End If

            If dtb.Rows.Count = 1 Then
                Response.Redirect("index.aspx")
            Else
                Response.Redirect("crossdept.aspx")
            End If
        Catch ex As Exception
            Me.lblConfirm.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Session("otorisasi_permit") = ""
        'Session("departemen") = ""
        'Session("jobsite") = ""
        Session.Abandon()
        Response.Redirect("signin_permit.aspx")
    End Sub

    Private Function ValidateActiveDirectoryLogin(ByVal Domain As String, ByVal Username As String, ByVal Password As String, ByRef NIK As String, ByRef msgErr As String) As Boolean
        Dim Success As Boolean = False
        Dim Entry As New System.DirectoryServices.DirectoryEntry("LDAP://" & Domain, Username, Password)
        Dim Searcher As New System.DirectoryServices.DirectorySearcher(Entry)
        Dim RetArray As New Hashtable()
        Dim oResult As SearchResult
        Dim oResults As SearchResultCollection
        Dim lsnik As String
        Dim msg As String

        Searcher.PropertiesToLoad.Add("SAMAccountName")
        Searcher.PropertiesToLoad.Add("givenname")
        Searcher.PropertiesToLoad.Add("employeeid")
        Searcher.PropertiesToLoad.Add("mail")

        Searcher.SearchScope = DirectoryServices.SearchScope.Subtree


        Try
            Dim Results As System.DirectoryServices.SearchResult = Searcher.FindOne
            Success = Not (Results Is Nothing)

            If Success = True Then
                Searcher.Filter = "(samAccountName=" & Username & ")"

                Dim Resultsxx As System.DirectoryServices.SearchResultCollection = Searcher.FindAll

                For Each oResult In Resultsxx
                    Dim de As System.DirectoryServices.DirectoryEntry = oResult.GetDirectoryEntry()

                    If de.Properties("GivenName").Value() Is Nothing Then
                    Else
                        lsnik = de.Properties("GivenName").Value().ToString()
                    End If


                    If de.Properties("employeeid").Value() Is Nothing Then
                        lsnik = ""
                    Else
                        lsnik = de.Properties("employeeid").Value().ToString()
                    End If

                Next

                NIK = lsnik

            End If


        Catch ex As Exception
            msgErr = ex.Message
            Success = False

        End Try
        Return Success
    End Function
End Class