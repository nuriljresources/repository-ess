﻿Imports System.Data.SqlClient
Imports System.IO

Partial Public Class UploadFile
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("otorisasi") = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If
    End Sub

    Private Sub BtnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnUpload.Click
        LblError.Visible = False

        Try
            Dim conn As SqlConnection
            Dim cmd As SqlCommand

            If FileChooser.PostedFile.ContentLength > 600000 Then
                LblError.Visible = True
                LblError.Text = "Ukuran File Tidak Boleh > 600 KB"
                Exit Sub
            End If

            ' Get the HttpFileCollection
            Dim hfc As HttpFileCollection = Request.Files
            Dim imgByte As Byte() = Nothing
            Dim imgType As String = ""
            Dim nmFile As String = ""
            Dim imgLength As Integer

            Dim noprog As String = Me.NoProg.Value
            Dim sidrap As String = Me.Id1.Value
            Dim sidiss As String = Me.Id2.Value
            Dim sidact As String = Me.Id3.Value
            Dim tgupld As String = Format(System.DateTime.Now, "MM/dd/yyyy HH:mm:ss")

            Dim sekuel As String = ""

            For i As Integer = 0 To hfc.Count - 1
                Dim hpf As HttpPostedFile = hfc(i)
                'imgByte = New Byte(hpf.ContentLength - 1) {}
                imgByte = GetStreamAsByteArray(hpf.InputStream)

                imgType = hpf.ContentType
                nmFile = GetShortName(hpf.FileName)
                imgLength = hpf.ContentLength

                sekuel = "INSERT INTO TRLAMPIRAN(Nomor,IdRap,IdIssue,NoAction,MimeData,MimeType,MimeLength,NmFile,TglUpload,NoProgres) VALUES(@nom,@rap,@iss,@act,@img,@mtp,@mln,@file,@tgl,@nop)"

                conn = New SqlConnection(ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString)
                cmd = New SqlCommand(sekuel, conn)
                cmd.Parameters.AddWithValue("@nom", noRecord())
                cmd.Parameters.AddWithValue("@rap", sidrap)
                cmd.Parameters.AddWithValue("@iss", sidiss)
                cmd.Parameters.AddWithValue("@act", sidact)
                cmd.Parameters.AddWithValue("@img", imgByte)
                cmd.Parameters.AddWithValue("@mtp", imgType)
                cmd.Parameters.AddWithValue("@mln", imgLength)
                cmd.Parameters.AddWithValue("@file", nmFile)
                cmd.Parameters.AddWithValue("@tgl", tgupld)
                cmd.Parameters.AddWithValue("@nop", noprog)
                conn.Open()
                cmd.ExecuteNonQuery()

                conn.Close()
            Next i
        Catch ex As HttpException
            LblError.Visible = True
            LblError.Text = "Ukuran File Tidak Boleh > 600 KB"
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Function noRecord() As Int16
        Dim no As Int16 = 0
        Try
            Dim conn As SqlConnection
            Dim cmd As SqlCommand

            Dim sekuel As String = "SELECT MAX(NOMOR) AS URUT FROM TRLAMPIRAN"
            conn = New SqlConnection(ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString)
            cmd = New SqlCommand(sekuel, conn)
            conn.Open()
            Dim sdr As SqlDataReader = cmd.ExecuteReader()

            While sdr.Read
                no = IIf(IsDBNull(sdr("URUT")), 0, sdr("URUT")) + 1
            End While

            conn.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Return no
    End Function

    Function GetShortName(ByVal nmfile As String) As String
        Dim pnjg As Int16 = nmfile.Length

        Dim njumlah As Int16 = 0
        For i As Int16 = pnjg - 1 To 0 Step -1
            If nmfile.Substring(i, 1) = "\" Then
                Exit For
            Else
                njumlah = njumlah + 1
            End If
        Next

        Return nmfile.Substring(pnjg - njumlah, njumlah)
    End Function

    Private Function GetStreamAsByteArray(ByVal stream As System.IO.Stream) As Byte()
        Dim streamLength As Integer = Convert.ToInt32(stream.Length)
        Dim fileData As Byte() = New Byte(streamLength) {}

        ' Read the file into a byte array
        stream.Read(fileData, 0, streamLength)
        stream.Close()

        Return fileData
    End Function

End Class