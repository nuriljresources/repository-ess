﻿Public Partial Class SFPP
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        Try
            Dim r As New Repository()
            rptSearchResult.DataSource = r.SearchHFPP(CDate(TglAwal.Text), CDate(TglAkhir.Text))
            rptSearchResult.DataBind()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Function FormatDate(ByVal dt As DateTime) As String
        Return dt.ToString("dd/MM/yyyy")
    End Function

    Private Sub rptSearchResult_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSearchResult.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim str As String = e.CommandArgument.ToString()
                Dim r As New Repository()
                r.DeleteHFPP(str)
                rptSearchResult.DataSource = r.SearchHFPP(CDate(TglAwal.Text), CDate(TglAkhir.Text))
                rptSearchResult.DataBind()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class