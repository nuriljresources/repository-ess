﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="AddKomentar.aspx.vb" Inherits="EXCELLENT.AddKomentar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
List Issue - Application MEMO
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    
    <script src="Scripts/jscom.js" type="text/javascript"></script>    
    <script src="scripts/calendar_us.js" type="text/javascript"></script> 
    <script language="javascript" type="text/javascript">
        function cekTanggal() {
            var d = document;
            var tg1 = d.getElementById("TxtTgl1").value;
            var tg2 = d.getElementById("TxtTgl2").value;

            var d11 = tg1.substring(0, 2);
            var d12 = tg1.substring(3, 5);
            var d13 = tg1.substring(6, 10);

            var d21 = tg2.substring(0, 2);
            var d22 = tg2.substring(3, 5);
            var d23 = tg2.substring(6, 10);

            if (parseFloat(d23) < parseFloat(d13)) {
                alert("Date end must not smaller than date start ...!");
                return false;
            }
            if (parseFloat(d23) == parseFloat(d13)) {
                if (parseFloat(d22) < parseFloat(d12)) {
                    alert("Date end must not smaller than date start ...!");
                    return false;
                }
            }
            if (parseFloat(d23) == parseFloat(d13)) {
                if (parseFloat(d22) == parseFloat(d12)) {
                    if (parseFloat(d21) < parseFloat(d11)) {
                        alert("Date end must not smaller than date start ...!");
                        return false;
                    }
                }
            }
            return true;
        }
    </script>
    <link href="css/calendar-c.css"rel="stylesheet" />   
    <link href="css/general.css" rel="stylesheet" type="text/css"/>
    <style type="text/css">
        ul#navigation li a.m4
        {        	
	        background-color:#00CC00;color: #000000;
        }
        .style4
        {
            height: 26px;
        }
        ul.dropdown *.dir3 {
          padding-right: 20px;
          background-image: url("/memo/css/dropdown/themes/default/images/nav-arrow-down.png");
          background-position: 100% 50%;
          background-repeat: no-repeat;
          background-color: #FFA500;
         }
   </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

       <input type="hidden" id="Id1"/>
       <input type="hidden" id="Id2"/>
       <input type="hidden" id="Id3"/>
       <input type="hidden" id="Nomor"/>
       <input type="hidden" id="currpage"/>
       <input type="hidden" id="akses" value="<%=Session("Permission")%>"/>
       
    <table id="menutop" cellspacing="2">
           <tr>
           <td style="border-style: solid; border-width: 1px; background-color: #FFFF66">
                <a href="#" onclick="Back()">Back to Issue</a>
           </td>
           <td>
                <input id="TxtNIK" type="text" value="<%=Session("otorisasi")%>" style="visibility: hidden;" disabled="disabled" />
            <input id="TxtPage" type="text" style="visibility: hidden; width: 58px;" disabled="disabled"/>
           </td>
           </tr>
    </table> 
   
    <div id="divbutton" align="left">
        <table><tr>
        <td align="left" width="10px">
            <input id="btnhide" type="button" value="Hide Filter" onclick="hideOption()" 
                style="width: 150px; font-family: Calibri;"  />
            <input id="btnshow" type="button" value="Show Filter" onclick="showOption();" style="width: 150px; font-family: Calibri;"/>
         </td>
        <td align="left" width="10px">
            <input id="btncetak1" type="button" value="Print All" onclick="cetakIsu(1)" 
                style="width: 150px; font-family: Calibri;"  />
            <input id="btncetak2" type="button" value="Print Choosen" 
                onclick="cetakIsu(2);" style="width: 150px; font-family: Calibri;"/>
         </td>
        </tr>
        </table>   
    </div>
    <div id="criteria" style="width:400px">
    <div id="divopsi1" align="right" style="width:400px">        
        <table width="100%">
        <tr>
        <td align="right" style="width: 60px">Date</td>
        <td align="right" style="width: 90px"> 
            <select id="seltanggal" style="width: 90px">
                <option value="rap" selected="selected">Meeting</option>
                <option value="due" >Due To</option>
            </select>        
        </td>
        <td style="width: 85px">
        <input id="TxtTgl1" type="text" disabled="disabled" style="width: 65px" />
        <script language="JavaScript" type="text/javascript">
            new tcal({
                'formname': 'aspnetForm',
                'controlname': 'TxtTgl1',
                'id': 'Tgl1'
            });
        </script>     
        </td>
        <td align="center" style="width: 10px">        
        <label id="lblsd">S.D.</label>
        </td>
        <td align="right" style="width: 85px">
        <input id="TxtTgl2" type="text" disabled="disabled" style="width: 65px" />
        <script language="JavaScript" type="text/javascript">
            new tcal({
                'formname': 'aspnetForm',
                'controlname': 'TxtTgl2',
                'id': 'Tgl2'
            });
        </script> 
        </td>
        <td align="right" style="width: 20px">
            <input id="chktgl" type="checkbox" />
        </td>
        </tr>
        </table>
       </div>   
       
       <div id="divopsi2" align="right" style="width:400px">   
        <table width="100%">
        <tr>
        <td width="20%" class="style4">   
            <span>Jobsite</span></td>
        <td align="right" width="20%" class="style4">        
            <select id="cmbjob" style="width: 90px">
                <%
                   Select Case Session("permission")
                        Case "2", "6", "7","A"
                %>
                <option value="<%= Session("jobsite")%>" selected="selected"><%= Session("jobsite")%></option>
                <%Case Else%>
                <option value="ALL" selected="selected">ALL</option>
                <option value="JKT">JKT</option>
                <option value="ADR">ADR</option>
                <option value="BBE">BBE</option>
                <option value="BIN">BIN</option>
                <option value="DWA">DWA</option> 
                <option value="GBP">GBP</option>
                <option value="KDC">KDC</option>
                <option value="LAN">LAN</option>
                <option value="LAT">LAT</option>  
                <option value="MGM">MGM</option>
                <option value="PIK">PIK</option>
                <option value="SNK">SNK</option>
                <%End Select%>   
            </select> 
        </td>
        <td align="left" class="style4"> 
        </td>
        <td width="20%" class="style4">Status</td>
        <td align="right" width="20%" class="style4">
            <select id="cmbsts" style="width: 90px" name="D1">
                <option value="ALL" selected="selected">ALL</option>
                <option value="Open">Open</option>
                <option value="Close">Close</option>                    
            </select></td>
        <td align="left" class="style4">
        </td>
        </tr>
        
        <tr>        
        <td width="20%"><span id="spnf">Function</span></td>
        <td id="tdfungsi" align="right" width="30%">        
        <select id="cmbdepar" style="width: 90px">
            <option value="ALL" selected="selected">ALL</option>
            <option value="ENG">ENG</option> 
            <option value="FAB">FAB</option>  
            <option value="HRGA">HRGA</option>             
            <option value="IAS">IAS</option>
            <option value="MDV">MDV</option>
            <option value="MM">MM</option>
            <option value="OPR">OPR</option>
            <option value="PLT">PLT</option>
            <option value="SHE">SHE</option>
            <option value="BUD">BUD</option>
            <option value="CID">CID</option>
            <option value="IT">IT</option>
            <option value="ERP">ERP</option>
            <option value="NON">NON</option>
        </select>
        </td>
        <td align="left">
        </td>
        <td width="20%">Filter</td>
        <td align="right" width="20%">            
            <select id="cmbover" style="width: 90px" name="D2">
                <option value="ALL" selected="selected">ALL</option>
                <option value="due">Due To</option>
                <option value="h3">H-3</option>                    
            </select></td>
        <td align="left">
        </td>
        </tr> 
          
        <tr> 
        <td></td>
        <td><span id="spnm">Meeting</span></td>
        <td></td>
        <td id="tdjenis" colspan="2" align="right"> 
            <select id="cmbjenis" style="width: 190px" name="D3">
                <option value="0" selected="selected" style="font-size: small">ALL</option>
                <option value="1">Weekly Review Jobsite</option>
                <option value="2">Monthly Review Jobsite</option>
                <option value="3">Weekly Internal Functional</option>                   
                <option value="4">Weekly Management Meeting</option>                  
                <option value="5">Monthly Management Meeting</option>                     
                <option value="6">Others</option>                    
            </select></td>
        <td align="left">        
            <%--<input id="chkjenis" type="checkbox"/>--%>
        </td>
        </tr>             
        </table>  
        <br />           
           <input id="btncari" type="button" value="Find" onclick="cari();cekTanggal();" style="width:80px; font-family: Calibri;" />  
        </div>
    </div>
    
    <div id="divprogress" style="display:block; margin-top:100px"> 
    <center><img id="imgloading" alt="" src="images/loading.gif" width="30px" height="30px" /></center>
    </div>
    
    <br />
    
    <div id="divmain"></div>
        
    <div id="divaction"></div>
    
    <div id="divlampiran"></div>           
    
    <script language="JavaScript" type="text/javascript">
        retrieveIssueHead(1); initPage();
    </script>
    
    <script type="text/javascript">
        hideOption();

        if ((document.getElementById("akses").value != 'S') && (document.getElementById("akses").value != '1') && (document.getElementById("akses").value != '3')) {
            document.getElementById("cmbdepar").style.display = "none";
            document.getElementById("spnf").style.display = "none";
            //document.getElementById("chkfunction").style.display = "none";
            document.getElementById("cmbjenis").style.display = "none";
            document.getElementById("spnm").style.display = "none";
            //document.getElementById("chkjenis").style.display = "none";
        }
        else {            
            document.getElementById("cmbdepar").style.display = "block";
            document.getElementById("spnf").style.display = "block";
            //document.getElementById("chkfunction").style.display = "block";
            document.getElementById("cmbjenis").style.display = "block";
            document.getElementById("spnm").style.display = "block";
            //document.getElementById("chkjenis").style.display = "block";
        }
    </script>
</asp:Content>