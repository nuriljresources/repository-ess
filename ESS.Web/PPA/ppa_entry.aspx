﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ppa_entry.aspx.vb" Inherits="EXCELLENT.ppa_entry" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Performance Appraisal</title>
<%--<style type="text/css">
    .styletbl table 
    {
    	font-family:Arial Black;
    	font-size:0.5em;
    	
    }
    .styletbl td
    {
    	background-color:Gray;
    	border-color:Black;
    	border-width:medium;
    	border-style:solid; 
    	border-color:Black;
    }
    
</style>--%>
    <link rel="stylesheet" href="menu.css"/> 
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    
    <script type="text/javascript" >
        $(window).scroll(function(e) {
            // Get the position of the location where the scroller starts.
            var scroller_anchor = $(".scroller_anchor").offset().top;

            // Check if the user has scrolled and the current position is after the scroller start location and if its not already fixed at the top
            if ($(this).scrollTop() >= scroller_anchor && $('.scroller').css('position') != 'fixed') {    // Change the CSS of the scroller to hilight it and fix it at the top of the screen.
                $('.scroller').css({
                    //'background': '#CCC',
                    'border': '0px none #000',
                    'position': 'fixed',
                    'top': '-20px',
                    'max-width': '70em',
                    'margin': '0 auto',
                    'margin-top': '0px',
                    'margin-bottom': '0px'
                });

                $('table th, table td').css({
                    'text-align': 'left'
                });

                $('table.layout').css({
                    'width': '100%',
                    'border-collapse': 'collapse'
                });

                $('.center').css({
                    'text-align': 'center'
                });
                
                // Changing the height of the scroller anchor to that of scroller so that there is no change in the overall height of the page.
                $('.scroller_anchor').css('height', '0px');
            }
            else if ($(this).scrollTop() < scroller_anchor && $('.scroller').css('position') != 'relative') {    // If the user has scrolled back to the location above the scroller anchor place it back into the content.

                // Change the height of the scroller anchor to 0 and now we will be adding the scroller back to the content.
                $('.scroller_anchor').css('height', '-10px');

                // Change the CSS and put it back to its original position.
                $('.scroller').css({
                    //'background': '#FFF',
                    'border': '0px none #CCC',
                    'position': 'relative',
                    'max-width': '70em',
                    'margin': '0 auto',
                    'margin-top': '40px',
                    'margin-bottom': '-35px',
                    'width': '100%'
                });
            }
        });

        function OpenPopup(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("ppa_src_spv.aspx", "List", "scrollbars=yes,resizable=no,width=500,height=400");
            return false;
        }
    </script>
    
<style type="text/css">

body{
  padding: 1em;
  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAAQElEQVQIW2P89OvDfwYo+PHjJ4zJwMHBzsAIk0SXAKkCS2KTAEu++vQSbizIKGQAl0SXAJkGlsQmAbcT2Shk+wH0sCzAEOZW1AAAAABJRU5ErkJggg==);
}
a{
  color: #739931;
}
.page{
  max-width: 70em;
  margin: 0 auto;
}
table th,
table td{
  text-align: left;
}
table.layout{
  width: 100%;
  border-collapse: collapse;
}
table.display{
  margin: 1em 0;
}
table.display th,
table.display td{
  border: 1px solid #B3BFAA;
  padding: .5em 1em;
}

table.display th{ background: #D5E0CC; }
table.display td{ background: #fff; }

table.responsive-table{
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
}

.center{
  text-align:center;
  }
.none{
	border-style:none;
    text-align:center;
  }
  
  /* Accordion */
.accordionHeader
{
    border: 1px solid #2F4F4F;
    color: white;
    background-color: #2E4d7B;
	font-family: Arial, Sans-Serif;
	font-size: 12px;
	font-weight: bold;
    padding: 5px;
    margin-top: 5px;
    cursor: pointer;
}

#master_content .accordionHeader a
{
	color: #FFFFFF;
	background: none;
	text-decoration: none;
}

#master_content .accordionHeader a:hover
{
	background: none;
	text-decoration: underline;
}

.accordionHeaderSelected
{
    border: 1px solid #2F4F4F;
    color: white;
    background-color: #5078B3;
	font-family: Arial, Sans-Serif;
	font-size: 12px;
	font-weight: bold;
    padding: 5px;
    margin-top: 5px;
    cursor: pointer;
}

#master_content .accordionHeaderSelected a
{
	color: #FFFFFF;
	background: none;
	text-decoration: none;
}

#master_content .accordionHeaderSelected a:hover
{
	background: none;
	text-decoration: underline;
}

.accordionContent
{
    background-color: #D3DEEF;
    border: 1px dashed #2F4F4F;
    border-top: none;
    padding: 5px;
    padding-top: 10px;
}
/*end accordion*/
  
  #footer {
	background:#373737;
	clear:both;
	bottom: 0;
	position: relative;
	margin-top: -50px; /* negative value of footer height */
	height: 20px;
	clear:both;
	}
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 
	 FOOTER
	 	 
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

.nav-footer
{
	position: relative;
	text-align: center;
	clear: both;
}

.nav-footer ul{
	padding:0;
	margin:0;
	text-align:center;
	}

.nav-footer li{
	background:none;
	display:inline;
	border-right:1px dotted #686868;
	padding:0 10px;
	}

.nav-footer li.first {
	border-left:1px dotted #686868;
	}
	
.copyright {
	color:#999;
	font-size:.8em;
	clear:both;
	}

.nav-footer a:link,
.nav-footer a:visited {
	color:#CCC;
	}

@media (max-width: 30em){
    table.responsive-table{
      box-shadow: none;  
    }
    table.responsive-table thead{
      display: none; 
    }
  table.display th,
  table.display td{
    padding: .5em;
  }
    
  table.responsive-table td:nth-child(1):before{
    content: 'Number';
  }
  table.responsive-table td:nth-child(2):before{
    content: 'Name';
  }
  table.responsive-table td:nth-child(1),
  table.responsive-table td:nth-child(2){
    padding-left: 25%;
  }
  table.responsive-table td:nth-child(1):before,
  table.responsive-table td:nth-child(2):before{
    position: absolute;
    left: .5em;
    font-weight: bold;
  }
  
    table.responsive-table tr,
    table.responsive-table td{
        display: block;
    }
    table.responsive-table tr{
        position: relative;
        margin-bottom: 1em;
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
    }
    table.responsive-table td{
        border-top: none;
    }
    table.responsive-table td.organisationnumber{
        background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    table.responsive-table td.actions{
        position: absolute;
        top: 0;
        right: 0;
        border: none;
        background: none;
    }
    table.responsive-table td.top{
    	background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    
    .container{font-size:14px; margin:0 auto; width:960px}
    .test_content{margin:10px 0;}
    .scroller_anchor{height:0px; margin:0; padding:0;}
    .scroller{max-width: 70em; margin: 0 auto;}
}
* { font-family: Times New Roman; color: black; }
</style>

</head>
<body>
    <form id="form1" runat="server">
    <input type="hidden" id="ctrlToFind" />
     
    <!-- This div is used to indicate the original position of the scrollable fixed div. -->
    <%--<div class="scroller_anchor"></div>--%>
     
    <!-- This div will be displayed as fixed bar at the top of the page, when user scrolls -->
    <%--<div class="scroller">This is the scrollable bar</div>--%>
    
    <div class="page" style="margin-bottom:0px;">
    
    <div id="div2" style="float:right;">
    <%=mreport%>
    <label><a href="ppa_logout.aspx">LogOut</a></label>
    </div>
    
    <img src="../images/logo.png" />
    <center style="font-size:1.5em; font-weight:bold; margin-bottom:30px;">PERSONAL PERFORMANCE APPRAISAL</center>
    <asp:HiddenField ID="txtnobuk" runat="server" />
    <asp:HiddenField ID="txtstats" runat="server" />
    <div>
        <table class="" style="margin-bottom:0px; padding-bottom:0px;">
            <tr>
                <td style="width:300px;">
                    Nama Karyawan / <i>Employee's Name</i>
                </td>
                <td style="vertical-align:bottom;" >
                    <asp:TextBox ID="txtnama" runat="server" Width="400px" BackColor="#cccccc" Font-Bold="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    NIK / <i>Employee ID</i>
                </td>
                <td style="vertical-align:bottom;">
                    <asp:TextBox ID="txtnik" runat="server" Width="400px" BackColor="#cccccc" Font-Bold="true"></asp:TextBox>
                    <asp:HiddenField ID="hnik" runat="server" />
                    <asp:TextBox ID="txtjabat" runat="server" Visible="false" Width="400px" BackColor="#cccccc" Font-Bold="true"></asp:TextBox>
                    <asp:HiddenField ID="hjabat" runat="server" />
                </td>
            </tr>
            <%--<tr>
                <td>
                    Jabatan / <i>Position</i>
                </td>
                <td style="vertical-align:bottom;">
                    
                </td>
            </tr>--%>
            <tr>
                <td>
                    Departemen / <i>Department</i>
                </td>
                <td style="vertical-align:bottom;">
                    <asp:TextBox ID="txtdepar" runat="server" Width="400px" BackColor="#cccccc" Font-Bold="true"></asp:TextBox>
                    <asp:HiddenField ID="hdepar" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Atasan / <i>Superior</i>
                </td>
                <td style="vertical-align:bottom;">
                    <asp:TextBox ID="txtspv" runat="server" Width="400px" BackColor="#cccccc" Font-Bold="true"></asp:TextBox>
                    <img alt="add" id="Img12" src="../images/Search.gif" align="absmiddle" style="cursor: pointer" onclick="OpenPopup('ppa_spv')" />
                    <asp:HiddenField ID="hnikspv" runat="server" />
                </td>
            </tr>
        </table>
        </div>
        <br />
        
        <div style="width:100px; float:right; position:absolute; top:200px; left:1130px"><p style="text-align:right;"><asp:ImageButton ID="imbtnprev" runat="server" ImageUrl="../images/Print_preview.png" AlternateText="Print Preview" /></p></div>
        
        <div id='cssmenu' style="width:10%; border: 1px solid #707070; background-color:#707070; text-align:center;">
            <ul>
               <li><span style="background-color:#707070;color:#A3A3A3;">Behaviour /</span></li>
               <li><asp:LinkButton ID="link_task" runat="server" style="text-decoration:none; color:White;" > Task </asp:LinkButton></li>
            </ul>
        </div>
        <div class="scroller_anchor"></div>
        <div class="scroller">
         <table id="myTable" class="layout display responsive-table" border="1" style="font-size:0.9em;">
            <thead>
            <tr>
                <th style="width:1%; border-style:none;">No</th>
                <th style="width:20%; border-style:none;">
                    Jenis perilaku Pribadi<br />
                    <i>Type of Personal Behavior</i>
                </th>
                <th style="width:16%; border-style:none;">
                    Tidak Mencapai Target Apapun<br />
                    <i>Does Not Meet Any Objective</i>
                </th>
                <th style="width:16%; border-style:none;">
                    Mencapai Sebagian Target<br />
                    <i>Meet Some Objective</i>
                </th>
                <th style="width:16%; border-style:none;">
                    Mencapai Seluruh Target<br />
                    <i>Meet Objective</i>
                </th>
                <th style="width:16%; border-style:none;">
                    Melebihi Sebagian Besar Target<br />
                    <i>Exceed Most Of Objective</i>
                </th>
                <th style="width:16%; border-style:none;">
                    Melebihi Seluruh Target<br />
                    <i>Exceed All Of Objective</i>
                </th>
            </tr>
            </thead>
            </table> 
            </div> 
    </div>
    
    <div class="page" style="margin-top:-15px; padding-top:0px;">
    
        <%--<table class="layout display responsive-table">
            <thead>
            <tr>
                <th style="width:20%;">
                    Jenis perilaku Pribadi<br />
                    <i>Type of Personal Behavior</i>
                </th>
                <th style="width:16%;">
                    Kurang<br />
                    <i>Poor</i>
                </th>
                <th style="width:16%;">
                    Perlu Peningkatan<br />
                    <i>Need Improvement</i>
                </th>
                <th style="width:16%;">
                    Rata-rata<br />
                    <i>Meet Standard</i>
                </th>
                <th style="width:16%;">
                    Diatas Rata-rata<br />
                    <i>Above Average</i>
                </th>
                <th style="width:16%;">
                    Istimewa<br />
                    <i>Exceptional</i>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr >
                <td rowspan="2" class="organisationnumber" style="vertical-align:top;">
                    <b>Integritas</b><br />
                    Tidak menutupi kesalahan dan jujur
                    
                    <br /><br />
                    <b>Integrity</b><br />
                    No whitewash and honest
                </td>
                
                <td class="organisationnumber" style="vertical-align:top;">
                Jika diberitahukan kesalahan, masih banyak berargumentasi
                <br /> <br />
                Giving a lot or argument when errors are disclosed
                </td>
                
                <td class="organisationnumber" style="vertical-align:top;">
                Jika diberitahukan kesalahan, seolah-olah menerima tetapi tetap melakukan tindakan yang kurang jujur
                <br /> <br />
                Pretending to accept rowing, but still conducting dishonest actions
                </td>
                
                <td class="organisationnumber" style="vertical-align:top;">
                Jika diberitahukan kesalahan, menerima dan tidak melakukan lagi
                <br /> <br />
                Fully accept of warning given, and not conducting faulty actions anymore
                </td>
                
                <td class="organisationnumber" style="vertical-align:top;">
                Memberitahu kesalahan tanpa perlu ditegur, dan melakukan perbaikan
                <br /> <br />
                Admit to errors without any reminder and execute corrective action immediately
                </td>
                
                <td class="organisationnumber" style="vertical-align:top;">
                Memberikan contoh kepada orang lain
                <br /> <br />
                Give examples to others
                </td>
            </tr>
            <tr>
                <td class="center">
                    <asp:RadioButton ID="a11" GroupName="q1" runat="server" />
                </td>
                
                <td class="center">
                    <asp:RadioButton ID="a12" GroupName="q1" runat="server" />
                </td>
                
                <td class="center">
                    <asp:RadioButton ID="a13" GroupName="q1" runat="server" />
                </td>
                
                <td class="center">
                    <asp:RadioButton ID="a14" GroupName="q1" runat="server" />
                </td>
                
                <td class="center">
                    <asp:RadioButton ID="a15" GroupName="q1" runat="server" />
                </td>
                
            </tr>
            
            
            </tbody>
        </table>--%>
        
        <asp:Repeater ID="Repeater1" runat="server">
            <HeaderTemplate>
                <table id="myTable" class="layout display responsive-table" style="font-size:0.9em;">
            <thead>
            <%--<tr>
                <th>No</th>
                <th style="width:20%;">
                    Jenis perilaku Pribadi<br />
                    <i>Type of Personal Behavior</i>
                </th>
                <th style="width:16%;">
                    Kurang<br />
                    <i>Poor</i>
                </th>
                <th style="width:16%;">
                    Perlu Peningkatan<br />
                    <i>Need Improvement</i>
                </th>
                <th style="width:16%;">
                    Rata-rata<br />
                    <i>Meet Standard</i>
                </th>
                <th style="width:16%;">
                    Diatas Rata-rata<br />
                    <i>Above Average</i>
                </th>
                <th style="width:16%;">
                    Istimewa<br />
                    <i>Exceptional</i>
                </th>
            </tr>--%>
            </thead>
            </HeaderTemplate>
            
            <ItemTemplate>
            <tbody>
            <tr>
                <td rowspan="3" class="organisationnumber" style="vertical-align:top; width:2%;">
                <br />
                <asp:label ID="txtid" runat="server" Text='<%#Container.DataItem("ms_id")%>'></asp:label>.
                </td>
                <td class="organisationnumber" style="vertical-align:top; border-bottom-style:none; width:15%;">
                   <%#Container.DataItem("header2")%>
                </td>
                
                <td class="organisationnumber" style="vertical-align:top; border-bottom-style:none; width:16%;">
                    <%#Container.DataItem("det1")%>
                </td>
                
                <td class="organisationnumber" style="vertical-align:top; border-bottom-style:none;  width:16%;">
                    <%#Container.DataItem("det2")%>
                </td>
                
                <td class="organisationnumber" style="vertical-align:top; border-bottom-style:none;  width:16%;">
                    <%#Container.DataItem("det3")%>
                </td>
                
                <td class="organisationnumber" style="vertical-align:top; border-bottom-style:none;  width:16%;">
                    <%#Container.DataItem("det4")%>
                </td>
                
                <td class="organisationnumber" style="vertical-align:top; border-bottom-style:none;  width:18%;">
                    <%#Container.DataItem("det5")%>
                </td>
            </tr>
            <tr>
                <td class="organisationnumber" style="vertical-align:top; border-bottom-style:none; border-top-style:none; width:15%;">
                    <%#Container.DataItem("eng_header2")%>
                </td>
                <td class="organisationnumber" style="vertical-align:top; border-top-style:none; width:16%;">
                    <%#Container.DataItem("eng_det1")%>
                </td>
                <td class="organisationnumber" style="vertical-align:top; border-top-style:none; width:16%;">
                    <%#Container.DataItem("eng_det2")%>
                </td>
                <td class="organisationnumber" style="vertical-align:top; border-top-style:none; width:16%;">
                    <%#Container.DataItem("eng_det3")%>
                </td>
                <td class="organisationnumber" style="vertical-align:top; border-top-style:none; width:16%;">
                    <%#Container.DataItem("eng_det4")%>
                </td>
                <td class="organisationnumber" style="vertical-align:top; border-top-style:none; width:18%;">
                    <%#Container.DataItem("eng_det5")%>
                </td>
            </tr>
            <tr>
                <td class="center" style="border-bottom-style:solid; border-top-style:none;"> </td>
                <td class="center">
                    <asp:RadioButton ID="a11" GroupName="q1" runat="server" /> <asp:HiddenField ID="hid" Value="<%#Container.DataItem("ms_id")%>" runat="server" />
                </td> 
                
                <td class="center">
                    <asp:RadioButton ID="a12" GroupName="q1" runat="server" />
                </td>
                
                <td class="center">
                    <asp:RadioButton ID="a13" GroupName="q1" runat="server" />
                </td>
                
                <td class="center">
                    <asp:RadioButton ID="a14" GroupName="q1" runat="server" />
                </td>
                
                <td class="center">
                    <asp:RadioButton ID="a15" GroupName="q1" runat="server" />
                </td>
            </tr>
            
            
            </tbody>
            </ItemTemplate>
            
            <FooterTemplate>
            </table>
           
            </FooterTemplate>
        </asp:Repeater>
        
        <asp:PlaceHolder ID="placeholder" runat="server" ></asp:PlaceHolder>
    </div>
    <div class="page">
        <asp:Button ID="cmdSubmit" runat="server" Text="SAVE" OnClick="cmdSubmit_Click" />
        <asp:Button ID="cmdprint" runat="server" Text="PRINT" />
        <asp:TextBox ID="no" runat="server" style="visibility:hidden;"></asp:TextBox>    
    </div>    
    
<%--<div class="page">
 <h1>A responsive table</h1>
<table class="layout display responsive-table">
    <thead>
        <tr>
            <th>Number</th>
            <th colspan="2">Name</th>
        </tr>
    </thead>
    <tbody>

        <tr>
            <td class="organisationnumber">140406</td>
            <td class="organisationname">Stet clita kasd gubergren, no sea takimata sanctus est</td>
            <td class="actions">
                <a href="?" class="edit-item" title="Edit">Edit</a>
                <a href="?" class="remove-item" title="Remove">Remove</a>
            </td>
        </tr>

        <tr>
            <td class="organisationnumber">140412</td>
            <td class="organisationname">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat</td>
            <td class="actions">
                <a href="?" class="edit-item" title="Edit">Edit</a>
                <a href="?" class="remove-item" title="Remove">Remove</a>
            </td>
        </tr>

        <tr>
            <td class="organisationnumber">140404</td>
            <td class="organisationname">Vel illum dolore eu feugiat nulla facilisis at vero eros</td>
            <td class="actions">
                <a href="?" class="edit-item" title="Edit">Edit</a>
                <a href="?" class="remove-item" title="Remove">Remove</a>
            </td>
        </tr>

        <tr>
            <td class="organisationnumber">140408</td>
            <td class="organisationname">Iusto odio dignissim qui blandit praesent luptatum zzril delenit</td>
            <td class="actions">
                <a href="?" class="edit-item" title="Edit">Edit</a>
                <a href="?" class="remove-item" title="Remove">Remove</a>
            </td>
        </tr>

        <tr>
            <td class="organisationnumber">140410</td>
            <td class="organisationname">
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam
            </td>
            <td class="actions">
                <a href="?" class="edit-item" title="Edit">Edit</a>
                <a href="?" class="remove-item" title="Remove">Remove</a>
            </td>
        </tr>

    </tbody>
</table>
</div>--%>

    <div style="padding-left: 15px; padding-right: 15px;">
    <div id="footer" style="margin-top:50px;">
        <div class="nav-footer">
          <p class="copyright">&copy; 2013-2014 PT JRESOURCES NUSANTARA</p>
        </div>
    </div>
    </div>
    </form>
<%=msg%>
</body>
</html>
