﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="medic_entry.aspx.vb" Inherits="EXCELLENT.medic_entry" %>
<%--<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Medical Entry
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    
    <script src="Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="Scripts/jscal2.js" type ="text/javascript"></script>
    <script src="Scripts/en.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="css/win2k/win2k.css" />
    <link rel="stylesheet" href="css/jquery.tooltip/jquery.tooltip2.css" type="text/css" />
    <script type="text/javascript" src="Scripts/tooltip/jquery.min.js"></script>
    <script type="text/javascript" src="Scripts/tooltip/jquery.tooltip.js"></script>
    
    <script type="text/javascript" >
        function OpenPopup(key) {
            var d = document.getElementById("ctl00_ContentPlaceHolder1_hnik").value
            document.getElementById("ctrlToFind").value = key;
            window.open("srcfamily.aspx?id=" + d, "List", "scrollbars=yes,resizable=no,width=600,height=500");
            return false;
        }

        function OpenPopupd(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("searchmedicdel.aspx", "List", "scrollbars=yes,resizable=no,width=600,height=500");
            return false;
        }
        
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode < 45 || charCode > 57 || charCode == 47 || charCode == 45)
                return false;

            return true;
        }

        function readonly(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 00)
                return false;

            return true;
        }

        $j = jQuery.noConflict();
        $j(document).ready(function() {
            $j("div.item").tooltip();
        });

    </script>
    <style type="text/css" >
    /** { font-family: Times New Roman; }*/
      div.item { width:150px; height:25px; background-color: Transparent; text-align:center; padding-top:0px; color: white;}
      div#item_1 { position: absolute; top: 110px; left: 650px; color: white; font-family:arial; background-color:#E38695;}
      div#item_2 { position: absolute; top: 250px; left: 649px; color: black; font-family:arial;}      
      div#item_3 { position: absolute; top: 250px; left: 955px; color: black; font-family:arial;}            
      div#item_4 { position: absolute; top: 250px; left: 1155px; color: black; font-family:arial;}
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<ajaxToolkit:ToolkitScriptManager runat="server" ID="ScriptManager1" EnableScriptGlobalization="true" EnableScriptLocalization="true" />  
--%>        <input type="hidden" id="ctrlToFind" /> <asp:HiddenField ID="hprint" runat="server" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
        <caption style="text-align:center; font-size: 1.5em; color:White;">Inpatient Medical Claim</caption>
        <tr>
            <td style="width:10%;">
                Trans No
            </td>
            <td style="width:15%;">
                <asp:TextBox ID="txtnoreg" Width="150px" Text="Auto" Enabled="false" runat="server" Font-Size="1.0em" >Auto</asp:TextBox>
            </td>
            <td style="width:10%;">Date</td>
            <td colspan="2">
                <asp:TextBox ID="txtdate" Width="200px" runat="server" Font-Size="1.0em"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Site
            </td>
            <td>
                <asp:TextBox ID="txtnmsite" Width="150px" runat="server" Font-Size="1.0em" ></asp:TextBox>
                <asp:HiddenField ID="hnmsite" runat="server" />
            </td>
            <td>
                costcode
            </td>
            <td>
                <asp:TextBox ID="txtcostcode" Width="200px" runat="server" Font-Size="1.0em"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Nik <strong style="color:Red;">*</strong>
            </td>
            <td>
                <asp:TextBox ID="txtnik" Width="150px" runat="server" Font-Size="1.0em"></asp:TextBox>
                <img alt="add" id="Img1" src="images/Search.gif" align="absmiddle" style="cursor: pointer" onclick="OpenPopupd('srcd')" />
                <asp:HiddenField ID="hnik" runat="server" />
                
            </td>
            <td> Name </td>
            <td>
            <asp:TextBox ID="txtnama" runat="server" Width="200px" Font-Size="1.0em" ></asp:TextBox>
            </td>
            
        </tr>
        <tr>
            <td>
                Position
            </td>
            <td>
                <asp:TextBox ID="txtlevel" Width="150px" runat="server" Font-Size="1.0em" ></asp:TextBox>
                <asp:HiddenField ID="hkdlevel" runat="server" />
            </td>
            <td> </td>
            <td colspan="2">
                <div id="item_1" class="item" style="font-weight:bold;">
                Medical Benefit Policy <img alt="info" src="images/alert.png" width="20px" height="20px" />
                <div class="tooltip_description" style="display:none;" title="Medical Benefit Policy">
                    Room Rate IDR. <%=benefit%> / Day
                </div> 
                </div> 
                <asp:TextBox ID="txtoutpatientmax" Visible="false" Width="200px" runat="server" Font-Size="1.0em" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Remark
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtremarks" runat="server" TextMode="MultiLine" Width="483px" 
                    Font-Size="1.0em"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <%--<div style="width:86%;">
        <table class="data-table" style="border-style:none; padding: 0 0 0 0;" width="100%"><tr><th rowspan="2" style="width:98px;">Date In</th><th rowspan="2" style="width:100px;">Date Out</th><th rowspan="2" style="width:30px;">Days</th><th rowspan="2" style="width:309px;">Name</th><th colspan="6">Expense</th> </tr>
    <tr> <th style="width:81px;">Room</th> <th style="width:83px;">Consultation</th> <th style="width:83px;">Medicine</th> <th style="width:83px;">Lab</th> <th style="width:82px;">Other</th> <th style="width:86px;">Total</th></tr></table>
    </div>--%>
    <div id="grid" style="width:80%; font-size:1.0em;">
    <div style="background-color:#d1ffc1; margin-left:553px; text-align:center; font-weight:bold; font-size:0.9em; width:523px; height:18px"> Expenses </div>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="No" OnRowCancelingEdit="GridView1_RowCancelingEdit"
       OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" OnRowCommand="GridView1_RowCommand"
       ShowFooter="True" OnRowDeleting="GridView1_RowDeleting" CssClass="data-table" ShowHeader="true">
       
       <Columns>
           <asp:TemplateField HeaderText="Date In">
               <EditItemTemplate>
               <div style="width:100px">
                   <asp:HiddenField ID="No" runat="server" />
                   <asp:TextBox ID="txtDateinv" style="width:70px" runat="server" Text='<%# Eval("TglDet") %>' Font-Size="1.0em" onkeypress="return readonly(event)"> </asp:TextBox>
                   <button id="btn2" style="height:20px; width:15px;">..</button>
                </div> 
               </EditItemTemplate>
               <FooterTemplate>
                   <div style="width:100px">
                   <asp:TextBox ID="txtNewdt" runat="server" style="width:70px" Font-Size="1.0em" onkeypress="return readonly(event)"> </asp:TextBox> 
                   <button id="btn1" style="height:20px; width:15px;">..</button>
                   </div>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label6" runat="server" Text='<%# Bind("TglDet") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Date Out">
           <EditItemTemplate>
           <div style="width:100px">
                <asp:TextBox ID="txtDateout" style="width:70px" runat="server" Text='<%# Eval("TglDetOut") %>' Font-Size="1.0em" onkeypress="return readonly(event)"> </asp:TextBox>
                <button id="btn4" style="height:20px; width:15px;">..</button>
           </div> 
           </EditItemTemplate>
           <FooterTemplate>
               <div style="width:100px">
               <asp:TextBox ID="txtNewdtout" runat="server" style="width:70px" Font-Size="1.0em" onkeypress="return readonly(event)"> </asp:TextBox> 
               <button id="btn3" style="height:20px; width:15px;">..</button>
               </div>
           </FooterTemplate>
           <ItemTemplate>
                   <asp:Label ID="Labeldtout" runat="server" Text='<%# Bind("TglDetOut") %>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>         
           
           <asp:TemplateField HeaderText="Days" HeaderStyle-Width="100px" SortExpression="Hari">
               <EditItemTemplate>
               <asp:Label ID="Labelhari2" Width="30px" runat="server" Text='<%# Bind("hari") %>'></asp:Label>
                   <asp:TextBox ID="txtHari" Width="30px" Visible="false" runat="server" Text='<%# Eval("hari") %>' Font-Size="1.0em" onkeypress="return isNumberKey(event)"></asp:TextBox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:Label ID="Labelhari1" Width="30px" runat="server" Text='<%# Bind("hari") %>'></asp:Label>
                   <asp:TextBox ID="txtNewHari" Width="30px" runat="server" Visible="false" style="width:30px" Font-Size="1.0em" onkeypress="return isNumberKey(event)"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label4" Width="30px" style="text-align:center;" runat="server" Text='<%# Bind("hari") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Name" HeaderStyle-Width="100px">
               <EditItemTemplate>
               <div style="width:310px">
                   <asp:HiddenField ID="hperson" runat="server" Value='<%# Bind("person") %>' />
                   <asp:TextBox ID="txtNama" runat="server" ReadOnly="true" Width="280px" Text='<%# Bind("nama") %>' Font-Size="1.0em" onkeypress="return readonly(event)"></asp:TextBox>
                   <img alt="add" id="Img1" src="images/Search.gif" align="absmiddle" style="cursor: pointer" onclick="OpenPopup('srcfamily')" />
               </div> 
               </EditItemTemplate>
               <FooterTemplate>
               <div style="width:310px">
                   <asp:HiddenField ID="hnewperson" runat="server" />
                   <asp:TextBox ID="txtNewNama" ReadOnly="true" Width="280px" runat="server" Font-Size="1.0em" onkeypress="return readonly(event)"></asp:TextBox>
                   <img alt="add" id="Img1" src="images/Search.gif" align="absmiddle" style="cursor: pointer" onclick="OpenPopup('srcfamily')" />
               <div style="width:110px">
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label3" runat="server" Text='<%# Bind("nama") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Room/day<br>(IDR)">
               <EditItemTemplate>
                   <asp:TextBox ID="txtHargaKamar" Width="80px" runat="server" Text='<%# Eval("HrgKmr", "{0:#,##.00}") %>' onkeypress="return isNumberKey(event)" Font-Size="1.0em"></asp:TextBox>
                   <%--<ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender2" runat="server"
            TargetControlID="txtHargaKamar"
            Mask="99,999,999.99"
            MessageValidatorTip="true"
            OnFocusCssClass="MaskedEditFocus"
            OnInvalidCssClass="MaskedEditError"
            MaskType="Number"
            InputDirection="RightToLeft"
            AcceptNegative="None" 
            DisplayMoney="None"  
            ErrorTooltipEnabled="True" />
        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator2" runat="server"
            ControlExtender="MaskedEditExtender2"
            ControlToValidate="txtHargaKamar"
            IsValidEmpty="false"
            Display="Dynamic"
            ValidationGroup="MKE" />--%>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewHargaKamar" onkeypress="return isNumberKey(event)" runat="server" style="width:80px" Font-Size="1.0em"></asp:TextBox>
                   <%--<ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender3" runat="server"
            TargetControlID="txtNewHargaKamar"
            Mask="99,999,999.99"
            MessageValidatorTip="true"
            OnFocusCssClass="MaskedEditFocus"
            OnInvalidCssClass="MaskedEditError"
            MaskType="Number"
            InputDirection="RightToLeft" 
            AcceptNegative="None" 
            DisplayMoney="None"  
            ErrorTooltipEnabled="True" />
        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator3" runat="server"
            ControlExtender="MaskedEditExtender3"
            ControlToValidate="txtNewHargaKamar"
            IsValidEmpty="false"
            Display="Dynamic" 
            ValidationGroup="MKE" />--%>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label12" runat="server" Text='<%# Bind("HrgKmr", "{0:#,##.00}") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           
           <%--<asp:TemplateField HeaderText="%Claim">
               <EditItemTemplate>
                   <asp:TextBox ID="txtperclaim" runat="server" Width="50px" Text='<%# Eval("persenclaim") %>' Font-Size="1.0em" onkeypress="return readonly(event)"></asp:TextBox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewperclaim" runat="server" Text="100%" Width="50px" Font-Size="1.0em" onkeypress="return readonly(event)"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label8" runat="server" Width="50px" Text='<%# Bind("persenclaim") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>--%>
           <asp:TemplateField HeaderText="Consultation<br>(IDR)" HeaderStyle-Width="300px">
                <EditItemTemplate>
                   <asp:textbox ID="txtbiayakons" Width="80px" runat="server" Text='<%# Eval("biayakons") %>' Font-Size="1.0em" onkeypress="return isNumberKey(event)"></asp:textbox>
                   <%--<ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender4" runat="server"
            TargetControlID="txtbiayakons"
            Mask="99,999,999.99"
            MessageValidatorTip="true"
            OnFocusCssClass="MaskedEditFocus"
            OnInvalidCssClass="MaskedEditError"
            MaskType="Number"
            InputDirection="RightToLeft"
            AcceptNegative="None" 
            DisplayMoney="None"  
            ErrorTooltipEnabled="True" />
        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator4" runat="server"
            ControlExtender="MaskedEditExtender4"
            ControlToValidate="txtbiayakons"
            IsValidEmpty="false"
            Display="Dynamic"
            ValidationGroup="MKE" />--%>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewbiayakons" runat="server" style="width:80px" onkeypress="return isNumberKey(event)" Font-Size="1.0em"></asp:TextBox>
                   <%--<ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender5" runat="server"
            TargetControlID="txtNewbiayakons"
            Mask="99,999,999.99"
            MessageValidatorTip="true"
            OnFocusCssClass="MaskedEditFocus"
            OnInvalidCssClass="MaskedEditError"
            MaskType="Number"
            InputDirection="RightToLeft"
            AcceptNegative="None" 
            DisplayMoney="None"  
            ErrorTooltipEnabled="True" />
        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator5" runat="server"
            ControlExtender="MaskedEditExtender5"
            ControlToValidate="txtNewbiayakons"
            IsValidEmpty="false"
            Display="Dynamic"
            ValidationGroup="MKE" />--%>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label10" runat="server" Text='<%# Bind("biayakons", "{0:#,##.00}") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Medicine<br>(IDR)" HeaderStyle-Width="80px">
                <EditItemTemplate>
                   <asp:textbox ID="txtbiayaobat" Width="80px" runat="server" Text='<%# Eval("biayaobat") %>' Font-Size="1.0em" onkeypress="return isNumberKey(event)"></asp:textbox>
                   <%--<ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender6" runat="server"
            TargetControlID="txtbiayaobat"
            Mask="99,999,999.99"
            MessageValidatorTip="true"
            OnFocusCssClass="MaskedEditFocus"
            OnInvalidCssClass="MaskedEditError"
            MaskType="Number"
            InputDirection="RightToLeft"
            AcceptNegative="None" 
            DisplayMoney="None"  
            ErrorTooltipEnabled="True" />
        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator6" runat="server"
            ControlExtender="MaskedEditExtender6"
            ControlToValidate="txtbiayaobat"
            IsValidEmpty="false"
            Display="Dynamic"
            ValidationGroup="MKE" />--%>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewbiayaobat" runat="server" style="width:80px" Font-Size="1.0em" onkeypress="return isNumberKey(event)"></asp:TextBox>
                   <%--<ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender7" runat="server"
            TargetControlID="txtNewbiayaobat"
            Mask="99,999,999.99"
            MessageValidatorTip="true"
            OnFocusCssClass="MaskedEditFocus"
            OnInvalidCssClass="MaskedEditError"
            MaskType="Number"
            InputDirection="RightToLeft"
            AcceptNegative="None" 
            DisplayMoney="None"  
            ErrorTooltipEnabled="True" />
        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator7" runat="server"
            ControlExtender="MaskedEditExtender7"
            ControlToValidate="txtNewbiayaobat"
            IsValidEmpty="false"
            Display="Dynamic"
            ValidationGroup="MKE" />--%>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label20" runat="server" Text='<%# Bind("biayaobat", "{0:#,##.00}") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Lab<br>(IDR)" HeaderStyle-Width="80px">
                <EditItemTemplate>
                   <asp:textbox ID="txtbiayalab" Width="80px" runat="server" Text='<%# Eval("biayalab") %>' Font-Size="1.0em" onkeypress="return isNumberKey(event)"></asp:textbox>
                   <%--<ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender10" runat="server"
            TargetControlID="txtbiayalab"
            Mask="99,999,999.99"
            MessageValidatorTip="true"
            OnFocusCssClass="MaskedEditFocus"
            OnInvalidCssClass="MaskedEditError"
            MaskType="Number"
            InputDirection="RightToLeft"
            AcceptNegative="None" 
            DisplayMoney="None"  
            ErrorTooltipEnabled="True" />
        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator10" runat="server"
            ControlExtender="MaskedEditExtender10"
            ControlToValidate="txtbiayalab"
            IsValidEmpty="false"
            Display="Dynamic"
            ValidationGroup="MKE" />--%>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewbiayalab" runat="server" style="width:80px" onkeypress="return isNumberKey(event)" Font-Size="1.0em"></asp:TextBox>
                   <%--<ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender11" runat="server"
            TargetControlID="txtNewbiayalab"
            Mask="99,999,999.99"
            MessageValidatorTip="true"
            OnFocusCssClass="MaskedEditFocus"
            OnInvalidCssClass="MaskedEditError"
            MaskType="Number"
            InputDirection="RightToLeft"
            AcceptNegative="None" 
            DisplayMoney="None"  
            ErrorTooltipEnabled="True" />
        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator11" runat="server"
            ControlExtender="MaskedEditExtender11"
            ControlToValidate="txtNewbiayalab"
            IsValidEmpty="false"
            Display="Dynamic"
            ValidationGroup="MKE" />--%>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label24" runat="server" Text='<%# Bind("biayalab", "{0:#,##.00}") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Other<br>(IDR)" HeaderStyle-Width="80px">
                <EditItemTemplate>
                   <asp:textbox ID="txtbiayalain" Width="80px" runat="server" Text='<%# Eval("biayalain") %>' Font-Size="1.0em" onkeypress="return isNumberKey(event)"></asp:textbox>
                   <%--<ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender8" runat="server"
            TargetControlID="txtbiayalain"
            Mask="99,999,999.99"
            MessageValidatorTip="true"
            OnFocusCssClass="MaskedEditFocus"
            OnInvalidCssClass="MaskedEditError"
            MaskType="Number"
            InputDirection="RightToLeft"
            AcceptNegative="None" 
            DisplayMoney="None"  
            ErrorTooltipEnabled="True" />
        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator8" runat="server"
            ControlExtender="MaskedEditExtender8"
            ControlToValidate="txtbiayalain"
            IsValidEmpty="false"
            Display="Dynamic"
            ValidationGroup="MKE" />--%>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewbiayalain" runat="server" style="width:80px" onkeypress="return isNumberKey(event)" Font-Size="1.0em"></asp:TextBox>
                   <%--<ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender9" runat="server"
            TargetControlID="txtNewbiayalain"
            Mask="99,999,999.99"
            MessageValidatorTip="true"
            OnFocusCssClass="MaskedEditFocus"
            OnInvalidCssClass="MaskedEditError"
            MaskType="Number"
            InputDirection="RightToLeft"
            AcceptNegative="None" 
            DisplayMoney="None"  
            ErrorTooltipEnabled="True" />
        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator9" runat="server"
            ControlExtender="MaskedEditExtender9"
            ControlToValidate="txtNewbiayalain"
            IsValidEmpty="false"
            Display="Dynamic"
            ValidationGroup="MKE" />--%>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label22" runat="server" Text='<%# Bind("biayalain", "{0:#,##.00}") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Total<br>(IDR)" HeaderStyle-Width="80px">
                <EditItemTemplate>
                   <asp:textbox ID="txtbiayatot" Width="80px" runat="server" Text='<%# Eval("biayatot") %>' Font-Size="1.0em" onkeypress="return readonly(event)"></asp:textbox>
                   <%--<ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender12" runat="server"
            TargetControlID="txtbiayatot"
            Mask="99,999,999.99"
            MessageValidatorTip="true"
            OnFocusCssClass="MaskedEditFocus"
            OnInvalidCssClass="MaskedEditError"
            MaskType="Number"
            InputDirection="RightToLeft"
            AcceptNegative="None" 
            DisplayMoney="None"  
            ErrorTooltipEnabled="True" />
        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator12" runat="server"
            ControlExtender="MaskedEditExtender12"
            ControlToValidate="txtbiayatot"
            IsValidEmpty="false"
            Display="Dynamic"
            ValidationGroup="MKE" />--%>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:Label ID="lblnewbiaya" runat="server" Width="80px" BackColor="#FCFFA3">  </asp:Label>
                   <asp:TextBox ID="txtNewbiayatot" Visible="false" runat="server" style="width:80px" onkeypress="return readonly(event)" Font-Size="1.0em"></asp:TextBox>
                   <%--<ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender13" runat="server"
            TargetControlID="txtNewbiayatot"
            Mask="99,999,999.99"
            MessageValidatorTip="true"
            OnFocusCssClass="MaskedEditFocus"
            OnInvalidCssClass="MaskedEditError"
            MaskType="Number"
            InputDirection="RightToLeft"
            AcceptNegative="None" 
            DisplayMoney="None"  
            ErrorTooltipEnabled="True" />
        <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator13" runat="server"
            ControlExtender="MaskedEditExtender13"
            ControlToValidate="txtNewbiayatot"
            IsValidEmpty="false"
            Display="Dynamic"
            ValidationGroup="MKE" />--%>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label26" runat="server" Text='<%# Bind("biayatot", "{0:#,##.00}") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
          <%-- <asp:TemplateField HeaderText="Diagnosa">
                <EditItemTemplate>
                    <asp:textbox ID="txtdiagnosa" Width="80px" runat="server" Text='<%# Eval("diagnosa") %>' Font-Size="1.0em"></asp:textbox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtNewdiagnosa" runat="server" style="width:80px" Font-Size="1.0em"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label28" runat="server" Text='<%# Bind("diagnosa") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>--%>
          <%-- <asp:TemplateField HeaderText="Treatment By" HeaderStyle-Width="300px">
                <EditItemTemplate>
                   <asp:textbox ID="txttdokter" Width="300px" runat="server" Text='<%# Eval("tdokter") %>'></asp:textbox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewtdokter" runat="server" style="width:300px"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label20" runat="server" Text='<%# Bind("tdokter") %>'></asp:Label>
               </ItemTemplate>

<HeaderStyle Width="300px"></HeaderStyle>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Docter Type" HeaderStyle-Width="300px">
                <EditItemTemplate>
                   <asp:textbox ID="txtjdokter" Width="300px" runat="server" Text='<%# Eval("jdokter") %>'></asp:textbox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewjdokter" runat="server" style="width:300px"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label20" runat="server" Text='<%# Bind("jdokter") %>'></asp:Label>
               </ItemTemplate>

<HeaderStyle Width="300px"></HeaderStyle>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Specialist" HeaderStyle-Width="300px">
                <EditItemTemplate>
                   <asp:textbox ID="txtkdspesialis" Width="300px" runat="server" Text='<%# Eval("kdspesialis") %>'></asp:textbox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewkdspesialis" runat="server" style="width:300px"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label20" runat="server" Text='<%# Bind("trs") %>'></asp:Label>
               </ItemTemplate>

<HeaderStyle Width="300px"></HeaderStyle>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Treatment Place" HeaderStyle-Width="300px">
                <EditItemTemplate>
                   <asp:textbox ID="txttrs" Width="300px" runat="server" Text='<%# Eval("trs") %>'></asp:textbox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewtrs" runat="server" style="width:300px"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label20" runat="server" Text='<%# Bind("trs") %>'></asp:Label>
               </ItemTemplate>

<HeaderStyle Width="300px"></HeaderStyle>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Hospital Type" HeaderStyle-Width="300px">
                <EditItemTemplate>
                   <asp:textbox ID="txtjrs" Width="300px" runat="server" Text='<%# Eval("jrs") %>'></asp:textbox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewjrs" runat="server" style="width:300px"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label20" runat="server" Text='<%# Bind("jrs") %>'></asp:Label>
               </ItemTemplate>

<HeaderStyle Width="300px"></HeaderStyle>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Collaborate Hospital" HeaderStyle-Width="300px">
                <EditItemTemplate>
                   <asp:textbox ID="txtkdrs" Width="300px" runat="server" Text='<%# Eval("kdrs") %>'></asp:textbox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewkdrs" runat="server" style="width:300px"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label20" runat="server" Text='<%# Bind("kdrs") %>'></asp:Label>
               </ItemTemplate>

<HeaderStyle Width="300px"></HeaderStyle>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Diagnose" HeaderStyle-Width="300px">
                <EditItemTemplate>
                   <asp:textbox ID="txtkddiagnosa" Width="300px" runat="server" Text='<%# Eval("kddiagnosa") %>'></asp:textbox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewkddiagnosa" runat="server" style="width:300px"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label20" runat="server" Text='<%# Bind("kddiagnosa") %>'></asp:Label>
               </ItemTemplate>

<HeaderStyle Width="300px"></HeaderStyle>
           </asp:TemplateField>--%>
           
           <asp:TemplateField HeaderText="" ShowHeader="False" FooterStyle-BorderStyle="None" HeaderStyle-Width="200px" HeaderStyle-BorderStyle="None" ItemStyle-BorderStyle="None">
               <EditItemTemplate>
                <div style="width:85px;">
                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update">
                   </asp:LinkButton>
                   <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel">
                   </asp:LinkButton>
               </div>
               </EditItemTemplate>
               <FooterTemplate>
               
                   <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="AddNew" Text="Save"></asp:LinkButton>
               
               </FooterTemplate>
               <ItemTemplate>
               
                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
               
               </ItemTemplate>
           </asp:TemplateField>
           
                <asp:CommandField HeaderText="" HeaderStyle-BorderStyle="None" ShowDeleteButton="True" ShowHeader="False" ItemStyle-BorderStyle="None" FooterStyle-BorderStyle="None"/>
           
       </Columns>
        <FooterStyle Width="300px" />
   </asp:GridView>
   
   <div style="width:106%; border-top-style:solid; border-top-color:Black; margin-top: 20px;">
        <table class="data-table" style="width:100%;">
            <tr>
                <td style="text-align:right;">
                    <asp:Label ID="lblinfo" runat="server" style="color:Red;"></asp:Label>
                </td>
                <td style="text-align:right;">
                   <strong>Total</strong> <asp:TextBox Width="120px" BackColor="#FCFFA3" ID="txttotal" runat="server" style="text-align:right;"></asp:TextBox>       
                </td>
            </tr>
        </table>
   </div>
   
   <asp:HiddenField ID="cnt" runat = "server" /> <asp:HiddenField ID="cnt2" runat = "server" />
   </div>
   
   <table>
    <tr>
        <td style="width:10%;">
            File Upload
        </td>
        <td>
            <asp:FileUpload ID="FileUpload1" runat="server" visible="false" />          
            <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="UploadFile" visible="false" />                        
        </td>
    </tr>
    
    <tr>
        <td></td>
        <td>
            <asp:Label ID="lblInfoUpload" runat="server" Text="save the document first"></asp:Label>
            <br />
            <asp:GridView ID="GridView2" Visible="false" runat="server" AutoGenerateColumns="false" EmptyDataText = "No files uploaded">
                <Columns>
                    <asp:BoundField DataField="Text" HeaderText="File Name" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDownload" Text = "Download" CommandArgument = '<%# Eval("Value") %>' runat="server" OnClick = "DownloadFile"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID = "lnkDelete" Text = "Delete" CommandArgument = '<%# Eval("Value") %>' runat = "server" OnClick = "DeleteFile" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>


        </td>
    </tr>
    </table>
   
   <asp:Button ID="btnsave" runat="server" Text="SAVE"/>
   <asp:Button ID="btnprint" runat="server" Text="PRINT"/> 
   <asp:Button ID="btnPaymentUsage" runat="server" Text="PRINT PA" /> 
   <asp:Button ID="btndelete" runat="server" Text="DELETE"/> 
   
   
   <% =js%>
   <%=msg%>
</asp:Content>