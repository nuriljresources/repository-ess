﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="report.aspx.vb" Inherits="EXCELLENT.report" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
       <rsweb:ReportViewer ID="rpt" runat="server" Height="600px" Width="100%">
       </rsweb:ReportViewer>
    
       <br />
       <asp:Label ID="lbwarning" runat="server"></asp:Label>
    
    </div>
    </form>
</body>
</html>
