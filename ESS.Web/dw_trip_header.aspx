<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="dw_trip_header.aspx.vb" Inherits="EXCELLENT.dw_trip_header" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    trip_header
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<%--<script src="Scripts/calendar_us.js" type="text/javascript"></script>--%>
<%--<script src="Scripts/jsm02.js" type="text/javascript"></script>--%>
<script src="Scripts/jsm01.js" type="text/javascript"></script>
<script src="Scripts/tcal.js" type="text/javascript"></script>
<script src="Scripts/jquery.min.js" type="text/javascript" ></script>
<link rel="stylesheet" href="css/tcal.css" type="text/css" />
<script src="Scripts/jscal2.js" type ="text/javascript"></script>
<script src="Scripts/en.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="css/gold/gold.css" />

    <style type="text/css">
        .style4
        {
            width: 62%;
        }
        .style6
        {
            width: 353px;
        }
        .style7
        {
            width: 309px;
        }
        .style8
        {
            width: 186px;
        }
        .style9
        {
            width: 603px;
        }
        .style10
        {
            width: 133px;
        }
        .entri
        {
            font-size :1.0em;
            height:16px; 
        }
       
    </style>
        <script type="text/javascript">


            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }

            function chkchar(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode == 39 || charCode == 43)
                    return false;

                return true;
            }

            function chktype(obj) {
                var numRow = '';
                var valtypetrans = '';
                var valClass = '';
                
                if (obj != undefined && obj != null && obj.id != undefined && obj.id != null) {
                    var idName = obj.id;
                    numRow = idName.replace('typetrans', '');

                    var typetransObj = $('#typetrans' + numRow);
                    if (typetransObj != undefined && typetransObj != null) {
                        valtypetrans = $(typetransObj).val()
                    }
                    
                }

                var IsEnableClass = true;
                if (numRow != '' && valtypetrans != '') {
                
                    var hlvl = $('#hlvl').val();
                    var classObj = $(".class" + numRow);
                    if (classObj != undefined && classObj != null) {
                        valClass = $(classObj).val();
                    }

                    
                    if (valtypetrans == '01') {
                        if (valClass == "00") {
                            $(".class" + numRow).val("01");
                        }

                        if (hlvl < 4) {
                            $(".class" + numRow).val("01");
                            IsEnableClass = false;
                        }                        
                    } else {

                    }
                    
                    
                    if (IsEnableClass) {
                        $(className).removeAttr('disabled');
                    } else {
                        $(className).attr('disabled', 'disabled');
                    }
                }

                
                
                var j = 1;
                var jml = document.getElementById("jml").value
                for (j = 1; j <= jml; j++) {
                    if (document.getElementById("typetrans" + j).value == '01') {
                        if (document.getElementById("class" + j).value == '00') {
                            document.getElementById("class" + j).value = '01';
                        }
                        document.getElementById("class" + j).disabled = "";

                        if (document.getElementById("hlvl").value < 4) {
                            document.getElementById("class" + j).value = '01';
                            document.getElementById("class" + j).disabled = true;
                        }
                    }

                    if (document.getElementById("typetrans" + j).value != '01') {
                        document.getElementById("class" + j).value = '00';
                        document.getElementById("class" + j).disabled = "disabled";
                    }
                }
            }

            function cek() {
                var d = document;
                if (d.getElementById("tripfrom").value.length == 0) {
                    alert("Please Entry Trip From Date");
                    return false;
                }
                if (d.getElementById("tripto").value.length == 0) {
                    alert("Please Entry Trip To Date");
                    return false;
                }
                //d.getElementById("tripday").value = count

                var v1 = d.getElementById("tripto").value;
                var v2 = d.getElementById("tripfrom").value;

                var parts1 = v1.split('/');
                var parts2 = v2.split('/');

                var date1 = new Date(parts1[2], parts1[0], parts1[1]);
                var date2 = new Date(parts2[2], parts2[0], parts2[1]);

                var difference = date1 - date2;
                difference = parseInt(difference / 86400000);

                //                var today = new Date();
                //                var dd = today.getDate();
                //                var mm = today.getMonth() + 1;

                //                var yyyy = today.getFullYear();
                //                if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } today = mm + '/' + dd + '/' + yyyy;
                //                if (v1 < today) {
                //                    alert("Please make sure the entered date equal to or greater than today " + today + " " + v1)
                //                    return false
                //                }

                var diff = Math.floor((Date.parse(v1) - Date.parse(v2)) / 86400000);
                if (diff < 0) {
                    alert("please make sure the date entered is correct")
                    return false;
                }

                d.getElementById("tripday").value = diff + 1
            }


            function OpenPopup(key) {
                document.getElementById("ctrlToFind").value = key;
                window.open("searchspv.aspx", "List", "scrollbars=yes,resizable=no,width=500,height=400");
                return false;
            }

            function OpenPopupdel(key) {
                document.getElementById("ctrlToFind").value = key;
                window.open("Searchempdel.aspx", "List", "scrollbars=yes,resizable=no,width=500,height=400");
                return false;
            }

            function OpenPopupfieldcostcd(key) {
                document.getElementById("ctrlToFind").value = key;
                window.open("searchcostcode.aspx", "List", "scrollbars=yes,resizable=no,width=500,height=400");
                return false;
            }

            function frmcetak() {
                var d = document;
                if (d.getElementById("nik").value.length == 0) {
                    alert("Please Entry Your Data");
                    return false;
                }
                window.location = "bussiness_trip_report_.aspx?id=" + '<%=session("tripno")%>'
            }

            function frmcetakcashadv() {
                var d = document;
                if (d.getElementById("nik").value.length == 0) {
                    alert("Please Entry Your Data");
                    return false;
                }
                window.location = "bussiness_trip_report_.aspx?id=" + '<%=session("tripno")%>'
            }

            function ddldtltrip() {
                var d = document;
                if (d.getElementById('detailtrip').value == '04') {
                    if (d.getElementById('costcode').value != 'E0101&#8211;6113309') {
                        d.getElementById('hcostc').value = d.getElementById('costcode').value
                    }
                    d.getElementById('costcode').value = 'E0101&#8211;6113311';
                } else if (d.getElementById('detailtrip').value == '05') {
                    if (d.getElementById('costcode').value != 'E0101&#8211;6113311') {
                        d.getElementById('hcostc').value = d.getElementById('costcode').value;
                    }
                    d.getElementById('costcode').value = 'E0101&#8211;6113309';
                } else {
                    if (d.getElementById('hcostc').value == '') {

                    } else {
                        d.getElementById('costcode').value = d.getElementById('hcostc').value;
                    }
                }
            }

            function save() {

                var d = document;
                var cash = "0";
                if (d.getElementById("cash").checked == true) {
                    cash = "1";
                }

                if (d.getElementById("nik").value.length == 0) {
                    alert("Please Entry Specific NIK");
                    return false;
                }
                if (d.getElementById("site").value.length == 0) {
                    alert("Please Entry Site Location");
                    return false;
                }

                if (d.getElementById("costcode").value.length == 0) {
                    alert("Please Entry Costcode");
                    return false;
                }

                if (d.getElementById("snik").value.length == 0) {
                    alert("Please Entry Supervisor");
                    return false;
                }
                if (d.getElementById("purpose").value.length == 0) {
                    alert("Please Entry Purpose");
                    return false;
                }

                if (d.getElementById("cashadvan").value == "") {
                    d.getElementById("cashadvan").value = 0;
                }

                if (d.getElementById("tripfrom").value.length == 0) {
                    alert("Please Entry Trip From Date");
                    return false;
                }
                if (d.getElementById("tripday").value.length == 0) {
                    alert("Please Entry Trip Day");
                    return false;
                }

                if (d.getElementById("tripday").value == 0) {
                    alert("Please Entry Trip Day");
                    return false;
                }

                if (d.getElementById("detailtrip").value == '00') {
                    alert("Please Entry Trip Type");
                    return false;
                }

                var s = document.getElementById('detailtrip');
                var detailtrip = s.options[s.selectedIndex].value;

                var curr = document.getElementById('curcode');
                var currcode = curr.options[curr.selectedIndex].value;

                var genid = '<%= genid %>'
                var genid2 = '<%= genid2 %>'

                var i = 1;
                var jml = d.getElementById("jml").value;
                var qry = "";
                for (i = 1; i <= jml; i++) {
                    if (i <= jml) {
                        genid = ((genid * 1) + 1)
                    }


                    qry = qry + " (" + genid + ","
                    qry = qry + "'||',"
                    qry = qry + "'" + d.getElementById("depfrom" + i).value + "',"
                    qry = qry + "'" + d.getElementById("depdate" + i).value + "',"
                    qry = qry + "'" + d.getElementById("arriveto" + i).value + "',"
                    qry = qry + "'" + d.getElementById("arrivedate" + i).value + "',"
                    qry = qry + "'" + d.getElementById("remark" + i).value + "',"
                    qry = qry + "'" + d.getElementById("timeprefer" + i).value + "',"
                    qry = qry + "'" + d.getElementById("typetrans" + i).value + "', "
                    qry = qry + "'" + d.getElementById("class" + i).value + "') ";
                    if (i != jml) {
                        qry = qry + "+";
                    }
                }

                var j = 1;
                var jml2 = d.getElementById("jml2").value;
                var akoqry = "";
                for (j = 1; j <= jml2; j++) {
                    if (j <= jml2) {
                        genid2 = ((genid2 * 1) + 1)
                    }

                    akoqry = akoqry + " (" + genid2 + ","
                    akoqry = akoqry + "'||',"
                    akoqry = akoqry + "'" + d.getElementById("location" + j).value + "',"
                    akoqry = akoqry + "'" + d.getElementById("datein" + j).value + "',"
                    akoqry = akoqry + "'" + d.getElementById("dateout" + j).value + "',"
                    akoqry = akoqry + "'" + d.getElementById("rem" + j).value + "')"
                    if (j != jml2) {
                        akoqry = akoqry + "+";
                    }
                }
                if (jml == 1 && d.getElementById("depdate1").value == "" && d.getElementById("arrivedate1").value == "") {
                    qry = "null"
                }

                if (jml2 == 1 && d.getElementById("datein1").value == "" && d.getElementById("dateout1").value == "") {
                    akoqry = "null"
                }

                $.ajax({
                    //            window:location = "account.aspx",

                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "WS/UserMgmt.asmx/AddTrip",
                    //data: "{'_tripno':" + JSON.stringify(document.getElementById("tripno").value) + ",'_site':" + JSON.stringify(document.getElementById("site").value) + ",'_nik':" + JSON.stringify(document.getElementById("nik").value) + ",'_costcode':" + JSON.stringify(document.getElementById("costcode").value) + ",'_snik':" + JSON.stringify(document.getElementById("snik").value) + ",'_kddepar':" + JSON.stringify(document.getElementById("kddepar").value) + ",'_nmjabat':" + JSON.stringify(document.getElementById("nmjabat").value) + ",'_consultant':" + JSON.stringify(document.getElementById("consultant").value) + ",'_consultantype':" + JSON.stringify(document.getElementById("consultantype").value) + ",'_tripfrom':" + JSON.stringify(document.getElementById("tripfrom").value) + ",'_tripto':" + JSON.stringify(document.getElementById("tripto").value) + ",'_tripday':" + JSON.stringify(document.getElementById("tripday").value) + "}",
                    data: "{'_tripno':" + JSON.stringify(document.getElementById("tripno").value) + ",'_site':" + JSON.stringify(document.getElementById("site").value) + ",'_nik':" + JSON.stringify(document.getElementById("nsite").value) + ",'_costcode':" + JSON.stringify(document.getElementById("costcode").value) + ",'_snik':" + JSON.stringify(document.getElementById("ssnik").value) + ",'_detailtrip':" + JSON.stringify(detailtrip) + ",'_purpose':" + JSON.stringify(document.getElementById("purpose").value) + ",'_tripday':" + JSON.stringify(document.getElementById("tripday").value) + ",'_tripfrom':" + JSON.stringify(document.getElementById("tripfrom").value) + ",'_tripto':" + JSON.stringify(document.getElementById("tripto").value) + ",'_reqdate':" + JSON.stringify(document.getElementById('<%= reqdate.ClientID %>').value) + ",'_qry':" + JSON.stringify(qry) + ",'_currcode':" + JSON.stringify(currcode) + ",'_cashadvan':" + JSON.stringify(document.getElementById("cashadvan").value) + ",'_kdjabat':" + JSON.stringify(document.getElementById("kdjabat").value) + ",'_kddepar':" + JSON.stringify(document.getElementById("kddepart").value) + ",'_akoqry':" + JSON.stringify(akoqry) + ",'_cash':" + JSON.stringify(cash) + ",'_createdby':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_hcreatedby").value) + "}",
                    dataType: "json",
                    success: function(res) {
                        d.getElementById('<%=cetak.clientid%>').disabled = false;
                        d.getElementById('<%=btnSubmit.clientid%>').disabled = false;
                        d.getElementById("Save").disabled = true;

                        alert("Data Saved Successfully");

                        //               document.getElementById("log").innerHTML = res.d;
                        //tripno, site, nik, costcode, nama, input, kddepar, nmjabat, consultant, consultantype, tripfrom, tripto, tripday
                        //window.location = "bussiness_trip_report_.aspx?id=" + d.getElementById("tripno").value;
                    },
                    error: function(err) {
                        //               document.getElementById("log").innerHTML = "error: " + err.responseText;
                        window.location = "home.aspx";
                    }
                });

            }
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <input type="hidden" id="ctrlToFind" /> <input type="hidden" id="hlvl" />
    <asp:HiddenField ID="hcreatedby" runat = "server" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
    <caption style="text-align: center; font-size: 1.5em; color:White;">Trip</caption>
        <tr style="width:100%;">
             <td style="width:15%">Trip ID</td>
             <td style="width:15%"><input style="width:90px" class="entri" disabled="disabled" type="text" id="Text1" value='Auto' /><input style="width:90px; visibility:hidden;" class="entri" disabled="disabled" type="text" id="tripno" value='<%=tripno %>' /></td>
             <td style="width:15%">Site</td>
             <td style="width:15%"><input style="width:90px;" class="entri" disabled="disabled" type="text" id="site" /></td>
             <td style="width:5%">POH</td>
             <td style="width:35%"><input style="width:90px;" class="entri" disabled="disabled" type="text" id="txpoh" /></td>
             
        </tr>
        <tr style="width:100%;">
             <td class="">NIK <strong style="color:Red;">*</strong></td>
             <td colspan="0" class=""><input style="width:90px;" class="entri" disabled="disabled" id="nik"/><input type="hidden" id="nsite" />
             <img alt="add" id="Img4" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopupdel('user')" /> 
             </td>
             <td class="">Cost Code <strong style="color:Red;">*</strong></td>
             <td class=""><input style="width:90px;" class="entri" disabled="disabled" type="text" id="costcode" />
             <img alt="add" id="Img1" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer; visibility:hidden;" onclick="OpenPopupfieldcostcd('cc')" /> 
               <input type="hidden" id="hcostc" />
             </td>
             <td >Phone</td>
             <td ><input style="width:90px;" class="entri" disabled="disabled" type="text" id="txphone" /></td>
        </tr>
        <tr style="width:100%;">
             <td class="">Name</td>
             <td class=""><input style="width:90px;" class="entri" disabled="disabled" type="text" id="nama" /> 
             </td>
             <td class=""> Superior Name <strong style="color:Red;">*</strong></td>
             <td class=""><input style="width:90px;" class="entri" type="text" disabled="disabled" id="snik" /><input type="hidden" id="ssnik" />
                          <img alt="add" id="Img5" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopup('suser')" /> </td>
             <td class="style7"></td><td class="style8"></td>
        </tr>
        <tr style="width:100%;">
             <td class="">Department</td>
             <td class=""><input style="width:90px;" class="entri" disabled="disabled" type="text" id="kddepar" />
             </td>
             <td>Type</td>
             <td>
             <%--<select id="detailtrip" style="width:95px;" onchange="ddldtltrip()">--%>
             <select id="detailtrip" style="width:95px;">
             <option value="00"> </option>
             <option value="01">Business Trip</option>
             <option value="02">Annual Leave</option>
             <option value="03">Periodic Leave</option>
             <option value="04">Recruitment</option>
             <option value="05">Training</option>
             <option value="06">Visitor</option>                    
             </select></td>
        </tr>
        <tr style="width:100%;">
             <td class="">Position</td>
             <td class="">
                <input style="width:90px;" disabled="disabled" type="text" id="nmjabat" />
             </td>
             <td >Cash Advance Req</td>
             <td >
             <input id="cash" type="checkbox" />
             
            </td>
            <td>
            
            </td>
            <select id="curcode" style = "visibility:hidden;" >
                 <option value=" "> </option>
                 <option value="IDR">IDR</option>
                 <option value="USD">USD</option>
            </select> <input type="text" id="cashadvan" style="visibility:hidden; " onkeypress="return isNumberKey(event)" />     
        </tr>
        <tr style="width:100%;">
             <td class="">Request Date</td>
             <td class="">
                  <asp:TextBox ID="reqdate" style="width:70px;" class="tcal" runat="Server" width="90px" Enabled="false"></asp:TextBox>
<%--                  <script type="text/javascript">
                      new tcal({
                          // form name
                          'formname': 'aspnetForm',
                          // input name
                          'controlname': 'ctl00$ContentPlaceHolder1$reqdate'
                      });
                  </script>--%>
             </td><td></td>
             <td colspan="2">
              <%--<input style="width:90px;" class="entri" type="text" disabled="disabled" id="consultant"/>--%></td>
        </tr> 
        <tr style="width:100%;">
             <td class=""></td>
             <td><input type="hidden" id="kdjabat" /> <input type="hidden" id="kddepart" /> </td><td></td>
             
             <%--<input style="width:90px;" class="entri" type="text" disabled="disabled" id="consultantype"/>--%>
        </tr>
        <tr>
            <td colspan = "4"><%--<asp:TextBox id="detailtrip" runat="server" Height="60px" Width="40%" 
             TextMode="MultiLine" class="tb10"></asp:TextBox>--%>
             <%--<input style="width:40%;Height:60px" class="entri" type="text" id="detailtrip"/>--%>
             <%--<textarea id="detailtrip" style="width:107%;Height:100px" class="entri" cols="5" rows="5"></textarea>--%>
             </td>
        </tr>
        <tr style="width:100%;">
             <td colspan="2">Travelling Purpose <strong style="color:Red;" >*</strong></td>
             <td></td>
        </tr>
        <tr>
            <td colspan = "4"><%--<asp:TextBox ID="purpose" runat="server" Height="60px" Width="40%" 
             TextMode="MultiLine" class="tb10"></asp:TextBox>--%>
             <%--<input style="width:40%;Height:60px" class="entri" type="text" id="purpose"/>--%>
             <textarea id="purpose" style="width:107%;Height:100px" onkeypress="return chkchar(event)" class="entri" cols="5" rows="5"></textarea>
             </td>
        </tr>
        
        </table>
        
        <br />
        
                                      <table width="100%" class="data-table-1" cellpadding="0" cellspacing="0" id="tb">
                                  <caption style="color:White;">Departure &amp; Arrival</caption>
                                  <thead>
                                      <tr>
                                          <th width="2%">
                                              No.</th>
                                          <th width="11.25%">
                                              Dep From</th>
                                          <th width="9.25%">
                                              Dep Date</th>
                                          <th width="10%">
                                              Arrive To</th>
                                          <th width="10%">
                                              Arrive Date</th>
                                          <th width="23%">
                                              Remark</th>
                                          <th width="7.25%">
                                              Time Prefer</th>
                                          <th width="4.25%">
                                              &nbsp;&nbsp;Type Trans</th>
                                          <th width="7.25%">Class</th>
                                      </tr>
                                      <tr><td colspan="9">
                                      <div id="input1" style="margin-bottom:0px; width:100%; position:relative;" class="clonedInput">
		                                    <input type="text" name="no1" id="no1" value="1" style="width:2%;" disabled="disabled"/>
		                                    <input type="text" name="depfrom1" id="depfrom1" onkeypress="return chkchar(event)" style="width:12.25%;"/>
		                                    <input type="text" name="depdate1" id="depdate1" style="width:11%;" disabled="disabled" /><button id="bdep1">...</button>
		                                    <input type="text" name="arriveto1" id="arriveto1" onkeypress="return chkchar(event)" style="width:10.25%;"/>
		                                    <input type="text" name="arrivedate1" id="arrivedate1" style="width:10%;" disabled="disabled" /><button id="barrive1">...</button>
		                                    <input type="text" name="remark1" id="remark1" onkeypress="return chkchar(event)" style="width:28%;" />
		                                    <select name="timeprefer1" id="timeprefer1" style="width:5.25%;">
		                                    <option value="01">Morning</option>
		                                    <option value="02">Noon</option>
		                                    <option value="03">Afternoon</option>
		                                    <option value="04">Night</option>
		                                    </select>
		                                    <select name="typetrans1" id="typetrans1" onchange="chktype(this)" style="width:5.25%;">
		                                    <option value="01">Air</option>
		                                    <option value="02">Land</option>
		                                    <option value="03">Sea</option>
		                                    </select>
		                                    <select name="class1" id="class1" style="width:7.25%;"> 
		                                    <option value="01">Economy</option>
		                                    <option value="02">Business</option>
		                                    <option value="00"> </option>
		                                    </select>
	                                    </div>
	                                    <div style="margin-bottom:4px;">
		                                    <input type="button" id="btnAdd" value="add" />
		                                    <input type="button" id="btnDel" value="remove" />
	                                    </div>
                                      </td></tr>
                                  </thead>
                                  <tbody>
                                  </tbody>
                             </table>
                             
                             <table width="100%" class="data-table-1" cellpadding="0" cellspacing="0" id="tb2">
                             <caption style="color:White;">Acomodation</caption>
                                  <thead>
                                  <tr>
                                          <th width="2%">
                                              No.</th>
                                          <th width="11%">
                                              Location</th>
                                          <th width="11%">
                                              Date In</th>
                                          <th width="11%">
                                              Date Out</th>
                                          <th width="32%">
                                              Remark</th>
                                          <th></th>
                                          <th></th>
                                          <th></th>
                                          <th></th>
                                          <th></th>
                                      </tr>
                                      </thead>
                                      <tbody >
                                <tr>
                                <td colspan="10">
                                    <div id="akoinput1" style="margin-bottom:0px; width:100%" class="akoinput">
	                                        <input type="text" name="n1" id="n1" value="1" style="width:2%;" disabled="disabled" />
	                                        <input type="text" name="location1" id="location1" onkeypress="return chkchar(event)" style="width:10%;" />
	                                        <input type="text" name="datein1" id="datein1" style="width:10%;" disabled="disabled"/><button id="bin1">...</button>
	                                        <input type="text" name="dateout1" id="dateout1" style="width:10%;" disabled="disabled"/><button id="bout1">...</button>
	                                        <input type="text" name="rem1" id="rem1" onkeypress="return chkchar(event)" style="width:27%;" />
	                                 </div>
	                                 <div>
		                                    <input type="button" id="akoadd" value="add" />
		                                    <input type="button" id="akodel" value="remove" />
	                                 </div>
                                </td>
                                </tr>
                                </tbody>
                             </table>
                             
                             <script type ="text/javascript" >

                                 //<![CDATA[

                                 var cal = Calendar.setup({
                                     onSelect: function(cal) { cal.hide() },
                                     showTime: false
                                 });

                                 cal.manageFields("bin1", "datein1", "%m/%d/%Y");
                                 cal.manageFields("bout1", "dateout1", "%m/%d/%Y");
                                 cal.manageFields("bdep1", "depdate1", "%m/%d/%Y");
                                 cal.manageFields("barrive1", "arrivedate1", "%m/%d/%Y");

                                 //]]



                                 $(document).ready(function() {
                                     $('#btnAdd').click(function() {

                                         var num = $('.clonedInput').length;
                                         var newNum = new Number(num + 1);

                                         var newElem = $('#input' + num).clone().attr('id', 'input' + newNum);

                                         newElem.children(':eq(0)').attr('id', 'no' + newNum).attr('name', 'no' + newNum).attr('value', newNum);
                                         newElem.children(':eq(1)').attr('id', 'depfrom' + newNum).attr('name', 'depfrom' + newNum);
                                         newElem.children(':eq(2)').attr('id', 'depdate' + newNum).attr('name', 'depdate' + newNum);
                                         newElem.children(':eq(3)').attr('id', 'bdep' + newNum).attr('name', 'bdep' + newNum);
                                         newElem.children(':eq(4)').attr('id', 'arriveto' + newNum).attr('name', 'arriveto' + newNum);
                                         newElem.children(':eq(5)').attr('id', 'arrivedate' + newNum).attr('name', 'arrivedate' + newNum);
                                         newElem.children(':eq(6)').attr('id', 'barrive' + newNum).attr('name', 'barrive' + newNum);
                                         newElem.children(':eq(7)').attr('id', 'remark' + newNum).attr('name', 'remark' + newNum);
                                         newElem.children(':eq(8)').attr('id', 'timeprefer' + newNum).attr('name', 'timeprefer' + newNum);
                                         newElem.children(':eq(9)').attr('id', 'typetrans' + newNum).attr('name', 'typetrans' + newNum);
                                         newElem.children(':eq(10)').attr('id', 'class' + newNum).attr('name', 'class' + newNum);

                                         $('#input' + num).after(newElem);
                                         $('#btnDel').attr('disabled', '');

                                         var d = document;
                                         (d.getElementById("jml").value = newNum)

                                         if (newNum == 100)
                                             $('#btnAdd').attr('disabled', 'disabled');

                                         cal.manageFields("bdep1", "depdate1", "%m/%d/%Y");
                                         if (newNum == 2)
                                             cal.manageFields("bdep2", "depdate2", "%m/%d/%Y");
                                         if (newNum == 3)
                                             cal.manageFields("bdep3", "depdate3", "%m/%d/%Y");
                                         if (newNum == 4)
                                             cal.manageFields("bdep4", "depdate4", "%m/%d/%Y");
                                         if (newNum == 5)
                                             cal.manageFields("bdep5", "depdate5", "%m/%d/%Y");
                                         if (newNum == 6)
                                             cal.manageFields("bdep6", "depdate6", "%m/%d/%Y");
                                         if (newNum == 7)
                                             cal.manageFields("bdep7", "depdate7", "%m/%d/%Y");
                                         if (newNum == 8)
                                             cal.manageFields("bdep8", "depdate8", "%m/%d/%Y");
                                         if (newNum == 9)
                                             cal.manageFields("bdep9", "depdate9", "%m/%d/%Y");
                                         if (newNum == 10)
                                             cal.manageFields("bdep10", "depdate10", "%m/%d/%Y");
                                         if (newNum == 11)
                                             cal.manageFields("bdep11", "depdate11", "%m/%d/%Y");
                                         if (newNum == 12)
                                             cal.manageFields("bdep12", "depdate12", "%m/%d/%Y");
                                         if (newNum == 13)
                                             cal.manageFields("bdep13", "depdate13", "%m/%d/%Y");
                                         if (newNum == 14)
                                             cal.manageFields("bdep14", "depdate14", "%m/%d/%Y");
                                         if (newNum == 15)
                                             cal.manageFields("bdep15", "depdate15", "%m/%d/%Y");
                                         if (newNum == 16)
                                             cal.manageFields("bdep16", "depdate16", "%m/%d/%Y");
                                         if (newNum == 17)
                                             cal.manageFields("bdep17", "depdate17", "%m/%d/%Y");
                                         if (newNum == 18)
                                             cal.manageFields("bdep18", "depdate18", "%m/%d/%Y");
                                         if (newNum == 19)
                                             cal.manageFields("bdep19", "depdate19", "%m/%d/%Y");
                                         if (newNum == 20)
                                             cal.manageFields("bdep20", "depdate20", "%m/%d/%Y");

                                         cal.manageFields("barrive1", "arrivedate1", "%m/%d/%Y");
                                         if (newNum == 2)
                                             cal.manageFields("barrive2", "arrivedate2", "%m/%d/%Y");
                                         if (newNum == 3)
                                             cal.manageFields("barrive3", "arrivedate3", "%m/%d/%Y");
                                         if (newNum == 4)
                                             cal.manageFields("barrive4", "arrivedate4", "%m/%d/%Y");
                                         if (newNum == 5)
                                             cal.manageFields("barrive5", "arrivedate5", "%m/%d/%Y");
                                         if (newNum == 6)
                                             cal.manageFields("barrive6", "arrivedate6", "%m/%d/%Y");
                                         if (newNum == 7)
                                             cal.manageFields("barrive7", "arrivedate7", "%m/%d/%Y");
                                         if (newNum == 8)
                                             cal.manageFields("barrive8", "arrivedate8", "%m/%d/%Y");
                                         if (newNum == 9)
                                             cal.manageFields("barrive9", "arrivedate9", "%m/%d/%Y");
                                         if (newNum == 10)
                                             cal.manageFields("barrive10", "arrivedate10", "%m/%d/%Y");
                                         if (newNum == 11)
                                             cal.manageFields("barrive11", "arrivedate11", "%m/%d/%Y");
                                         if (newNum == 12)
                                             cal.manageFields("barrive12", "arrivedate12", "%m/%d/%Y");
                                         if (newNum == 13)
                                             cal.manageFields("barrive13", "arrivedate13", "%m/%d/%Y");
                                         if (newNum == 14)
                                             cal.manageFields("barrive14", "arrivedate14", "%m/%d/%Y");
                                         if (newNum == 15)
                                             cal.manageFields("barrive15", "arrivedate15", "%m/%d/%Y");
                                         if (newNum == 16)
                                             cal.manageFields("barrive16", "arrivedate16", "%m/%d/%Y");
                                         if (newNum == 17)
                                             cal.manageFields("barrive17", "arrivedate17", "%m/%d/%Y");
                                         if (newNum == 18)
                                             cal.manageFields("barrive18", "arrivedate18", "%m/%d/%Y");
                                         if (newNum == 19)
                                             cal.manageFields("barrive19", "arrivedate19", "%m/%d/%Y");
                                         if (newNum == 20)
                                             cal.manageFields("barrive20", "arrivedate20", "%m/%d/%Y");
                                     });

                                     $('#btnDel').click(function() {
                                         var num = $('.clonedInput').length;

                                         $('#input' + num).remove();
                                         $('#btnAdd').attr('disabled', '');

                                         var d = document;
                                         (d.getElementById("jml").value = d.getElementById("jml").value - 1)

                                         if (num - 1 == 1)
                                             $('#btnDel').attr('disabled', 'disabled');
                                     });

                                     $('#btnDel').attr('disabled', 'disabled');

                                     $('#akoadd').click(function() {

                                         var num = $('.akoinput').length;
                                         var newNum = new Number(num + 1);

                                         var newElem2 = $('#akoinput' + num).clone().attr('id', 'akoinput' + newNum);

                                         newElem2.children(':eq(0)').attr('id', 'no' + newNum).attr('name', 'no' + newNum).attr('value', newNum);
                                         newElem2.children(':eq(1)').attr('id', 'location' + newNum).attr('name', 'location' + newNum);
                                         newElem2.children(':eq(2)').attr('id', 'datein' + newNum).attr('name', 'datein' + newNum);
                                         newElem2.children(':eq(3)').attr('id', 'bin' + newNum).attr('name', 'bin' + newNum);
                                         newElem2.children(':eq(4)').attr('id', 'dateout' + newNum).attr('name', 'dateout' + newNum);
                                         newElem2.children(':eq(5)').attr('id', 'bout' + newNum).attr('name', 'bout' + newNum);
                                         newElem2.children(':eq(6)').attr('id', 'rem' + newNum).attr('name', 'rem' + newNum);

                                         $('#akoinput' + num).after(newElem2);
                                         $('#akodel').attr('disabled', '');

                                         var d = document;
                                         (d.getElementById("jml2").value = newNum)

                                         cal.manageFields("bin1", "datein1", "%m/%d/%Y");
                                         if (newNum == 2)
                                             cal.manageFields("bin2", "datein2", "%m/%d/%Y");
                                         if (newNum == 3)
                                             cal.manageFields("bin3", "datein3", "%m/%d/%Y");
                                         if (newNum == 4)
                                             cal.manageFields("bin4", "datein4", "%m/%d/%Y");
                                         if (newNum == 5)
                                             cal.manageFields("bin5", "datein5", "%m/%d/%Y");
                                         if (newNum == 6)
                                             cal.manageFields("bin6", "datein6", "%m/%d/%Y");
                                         if (newNum == 7)
                                             cal.manageFields("bin7", "datein7", "%m/%d/%Y");
                                         if (newNum == 8)
                                             cal.manageFields("bin8", "datein8", "%m/%d/%Y");
                                         if (newNum == 9)
                                             cal.manageFields("bin9", "datein9", "%m/%d/%Y");
                                         if (newNum == 10)
                                             cal.manageFields("bin10", "datein10", "%m/%d/%Y");
                                         if (newNum == 11)
                                             cal.manageFields("bin11", "datein11", "%m/%d/%Y");
                                         if (newNum == 12)
                                             cal.manageFields("bin12", "datein12", "%m/%d/%Y");
                                         if (newNum == 13)
                                             cal.manageFields("bin13", "datein13", "%m/%d/%Y");
                                         if (newNum == 14)
                                             cal.manageFields("bin14", "datein14", "%m/%d/%Y");
                                         if (newNum == 15)
                                             cal.manageFields("bin15", "datein15", "%m/%d/%Y");
                                         if (newNum == 16)
                                             cal.manageFields("bin16", "datein16", "%m/%d/%Y");
                                         if (newNum == 17)
                                             cal.manageFields("bin17", "datein17", "%m/%d/%Y");
                                         if (newNum == 18)
                                             cal.manageFields("bin18", "datein18", "%m/%d/%Y");
                                         if (newNum == 19)
                                             cal.manageFields("bin19", "datein19", "%m/%d/%Y");
                                         if (newNum == 20)
                                             cal.manageFields("bin20", "datein20", "%m/%d/%Y");

                                         cal.manageFields("bout1", "dateout1", "%m/%d/%Y");
                                         if (newNum == 2)
                                             cal.manageFields("bout2", "dateout2", "%m/%d/%Y");
                                         if (newNum == 3)
                                             cal.manageFields("bout3", "dateout3", "%m/%d/%Y");
                                         if (newNum == 4)
                                             cal.manageFields("bout4", "dateout4", "%m/%d/%Y");
                                         if (newNum == 5)
                                             cal.manageFields("bout5", "dateout5", "%m/%d/%Y");
                                         if (newNum == 6)
                                             cal.manageFields("bout6", "dateout6", "%m/%d/%Y");
                                         if (newNum == 7)
                                             cal.manageFields("bout7", "dateout7", "%m/%d/%Y");
                                         if (newNum == 8)
                                             cal.manageFields("bout8", "dateout8", "%m/%d/%Y");
                                         if (newNum == 9)
                                             cal.manageFields("bout9", "dateout9", "%m/%d/%Y");
                                         if (newNum == 10)
                                             cal.manageFields("bout10", "dateout10", "%m/%d/%Y");
                                         if (newNum == 11)
                                             cal.manageFields("bout11", "dateout11", "%m/%d/%Y");
                                         if (newNum == 12)
                                             cal.manageFields("bout12", "dateout12", "%m/%d/%Y");
                                         if (newNum == 13)
                                             cal.manageFields("bout13", "dateout13", "%m/%d/%Y");
                                         if (newNum == 14)
                                             cal.manageFields("bout14", "dateout14", "%m/%d/%Y");
                                         if (newNum == 15)
                                             cal.manageFields("bout15", "dateout15", "%m/%d/%Y");
                                         if (newNum == 16)
                                             cal.manageFields("bout16", "dateout16", "%m/%d/%Y");
                                         if (newNum == 17)
                                             cal.manageFields("bout17", "dateout17", "%m/%d/%Y");
                                         if (newNum == 18)
                                             cal.manageFields("bout18", "dateout18", "%m/%d/%Y");
                                         if (newNum == 19)
                                             cal.manageFields("bout19", "dateout19", "%m/%d/%Y");
                                         if (newNum == 20)
                                             cal.manageFields("bout20", "dateout20", "%m/%d/%Y");
                                     });
                                     $('#akodel').click(function() {
                                         var num = $('.akoinput').length;

                                         $('#akoinput' + num).remove();
                                         $('#akoadd').attr('disabled', '');

                                         var d = document;

                                         var d = document;
                                         (d.getElementById("jml2").value = d.getElementById("jml2").value - 1)

                                         if (num - 1 == 1)
                                             $('#akodel').attr('disabled', 'disabled');
                                     });
                                     $('#akodel').attr('disabled', 'disabled');
                                 });
</script>
	    
	    <br />
	    
	    <table class="data-table" >
	           </table>
	    
	    <br />

       <table>
       <tr><td><input type="hidden" id = "jml" value = "1" /> <input type="hidden" id = "jml2" value = "1" /></td></tr>
       <tr style="width:100%;">
             <td colspan="3">I shall absent from office ( Tanggal tidak masuk kantor )</td>
             <td></td>
        </tr>
        <tr style="width:100%;">
             <td>From(dari) <strong style="color:Red;">*</strong></td>
             <td> <input type="text" id="tripfrom" style="width:70px;" disabled="disabled"  /><button id="btfrom">...</button>
             </td><td>No. of day(Jmlhari) <strong style="color:Red;">*</strong></td>
             <td colspan="2"><input style="width:90px;" class="entri" type="text" id="tripday" disabled="disabled" /> 
                 day(s) (hari)</td>
        </tr>
        <tr style="width:100%;">
             <td>To(sampai) <strong style="color:Red;">*</strong></td>
             <td>
                <input type="text" id="tripto" style="width:70px;" disabled = "disabled" /><button id="btto">...</button>
             </td><td><input type="button" value="Count" onclick="cek()" style="width: 80px; height:20px; font-family: Calibri;" /></td>
             <td colspan="2"><br /></td>
             
        </tr>
              <!-- Tombol Save-->
      <tr>
          
          <td style="height:40px;" colspan="3">
            <input type="button" id="Save" value="Save" onclick="save()" style="width: 80px; font-family: Calibri;" />
            <asp:Button Text="Submit" runat="server" ID="btnSubmit" style="width: 80px; font-family: Calibri;" Enabled="False" />
            <asp:Button ID="cetak" Text="Print" runat = "server" OnClick="cetak_Click" OnClientClick="aspnetForm.target ='_blank';" style="width: 80px; font-family: Calibri;" />
            <%--<asp:Button ID="cetak2" Text="Print Cash Adv" runat="server" OnClick="cetak2_Click" OnClientClick="aspnetForm.target ='_blank';" style="width: 80px; font-family: Calibri;" />--%>
         </td>
         <td><%--<input type="button" id="cetak" value="Print" onclick="frmcetak()" style="width: 80px; font-family: Calibri; " disabled="disabled" />--%>
            
         </td>
         <td><%--<input type="button" id="cetak" value="Print" onclick="frmcetak()" style="width: 80px; font-family: Calibri; " disabled="disabled" />--%>
            
         </td>
      </tr>
      <tr>
         <td colspan="7">
            <div id="log" style="color: red; font: Trebechuet;">
            </div>
         </td>
      </tr>
    </table> 
	    <script type ="text/javascript" >
	        document.getElementById('<%=cetak.clientid%>').disabled = true;

	        //<![CDATA[
	        var cal = Calendar.setup({
	            onSelect: function(cal) { cal.hide() },
	            showTime: false
	        });
	        cal.manageFields("btfrom", "tripfrom", "%m/%d/%Y");
	        cal.manageFields("btto", "tripto", "%m/%d/%Y");

	        //]]
	        
        </script>
        
        <%=alert%>
</asp:Content>