﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="rpt_leave.aspx.vb" Inherits="EXCELLENT.rpt_leave" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Trip Report</title>
    <script src="Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="Scripts/jscal2.js" type ="text/javascript"></script>
    <script src="Scripts/en.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="css/win2k/win2k.css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hkdsite" runat="server" />
    <table>
    <tr>
    <td>
        From
    </td>
    <td><asp:TextBox ID="txtdate1" runat="server"></asp:TextBox> <button id="btndt1">...</button></td>
    <td> To </td>
    <td><asp:TextBox ID="txtdate2" runat="server"></asp:TextBox> <button id="btndt2">...</button></td>
    <td><asp:Button ID="btnok" runat="server" text="OK"/></td>
    <td>&nbsp;</td>
    </tr>
    </table>
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
        Font-Size="8pt"  Height="579px" Width="1160px">
        <LocalReport ReportPath="Laporan\rptleave.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="hr_dtleave" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>

    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="EXCELLENT.hrTableAdapters.dtleaveTableAdapter" UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_transid" Type="String" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="datefrom" Type="DateTime" />
            <asp:Parameter Name="dateto" Type="DateTime" />
            <asp:Parameter Name="totleave" Type="Decimal" />
            <asp:Parameter Name="remarks" Type="String" />
            <asp:Parameter Name="trans_date" Type="DateTime" />
            <asp:Parameter Name="Original_transid" Type="String" />
        </UpdateParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="hkdsite" Name="kdsite" PropertyName="Value" 
                Type="String" />
            <asp:ControlParameter ControlID="txtdate1" Name="date1" PropertyName="Text" 
                Type="DateTime" />
            <asp:ControlParameter ControlID="txtdate2" Name="date2" PropertyName="Text" 
                Type="DateTime" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="transid" Type="String" />
            <asp:Parameter Name="datefrom" Type="DateTime" />
            <asp:Parameter Name="dateto" Type="DateTime" />
            <asp:Parameter Name="totleave" Type="Decimal" />
            <asp:Parameter Name="remarks" Type="String" />
            <asp:Parameter Name="trans_date" Type="DateTime" />
        </InsertParameters>
    </asp:ObjectDataSource>

    <div>
    
    </div>
    </form>
</body>
 <script type = "text/javascript" >
        var today = new Date();
        var cal = Calendar.setup({
            onSelect: function(cal) { cal.hide() },
            showTime: false
        });


        cal.manageFields("btndt1", "txtdate1", "%m/%d/%Y");
        cal.manageFields("btndt2", "txtdate2", "%m/%d/%Y");
 </script>
</html>
