﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="transport_entry.aspx.vb" Inherits="EXCELLENT.transport_entry" %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="System.Threading" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Transport
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jscal2.js" type ="text/javascript"></script>
    <script src="../Scripts/en.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="../css/win2k/win2k.css" />
<%--    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.5.js" type="text/javascript"></script>
    <script src="http://cloud.github.com/downloads/digitalBush/jquery.maskedinput/jquery.maskedinput-1.3.min.js"type="text/javascript"></script>
--%>
<script type ="text/javascript" >
    
    function OpenPopup(key) {
        document.getElementById("ctrlToFind").value = key;
        document.getElementById("ctnm").value = document.getElementById('<%=cnt.clientid%>').value;
        window.open("findpes.aspx", "List", "scrollbars=yes,resizable=no,width=600,height=500");
        return false;
    }

    function OpenPopuptrans(key) {
        document.getElementById("ctrlToFind").value = key;
        document.getElementById("ctnm").value = document.getElementById('<%=cnt.clientid%>').value;
        window.open("search_trans_emp.aspx", "List", "scrollbars=yes,resizable=yes,width=600,height=500");
        return false;
    }

    function OpenPopupdel(key) {
        document.getElementById("ctrlToFind").value = key;
        document.getElementById("ctnm").value = document.getElementById('<%=cnt.clientid%>').value;
        window.open("empdel.aspx", "List", "scrollbars=yes,resizable=no,width=600,height=500");
        return false;
    }
</script>

<%=jsmask%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ajaxToolkit:ToolkitScriptManager runat="server" ID="ScriptManager1" EnableScriptGlobalization="true" EnableScriptLocalization="true" />  
<table width="100%" align="center" border="0" style="text-align:center;"><tr><td align="center">
<table width="90%" border="0" cellpadding="0" cellspacing="0" class="data-table">
<caption style="text-align: center; font-size: 1.5em; color:White;">Transportation Request</caption>
                
                <tr>
                    <td>
                        No
                    </td>
                    <td>
                        <input style="width:100px" disabled="disabled" type="text" value="Auto" id="txtyear"/>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Requestor
                    </td>
                    <td>
                        <asp:TextBox ID="txtreq" runat="server" style="width:220px; font-size: 1.0em"></asp:TextBox> 
                        <img alt="add" id="Img2" src="../images/Search.gif" align="absmiddle" style="cursor: pointer;" onclick="OpenPopupdel('del')" /> 
                        <asp:HiddenField ID="hnik" runat="server" />
                        <asp:HiddenField ID="hname" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Department
                    </td>
                    <td>
                        <asp:TextBox ID="txtdepar" runat="server" style="width:260px; font-size: 1.0em"></asp:TextBox>
                        <asp:HiddenField ID="hkdepar" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Division
                    </td>
                    <td>
                        <asp:TextBox ID="txtdivision" runat="server" style="width:400px; font-size: 1.0em" ></asp:TextBox>
                        <asp:HiddenField ID="hkddivisi" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Superior
                    </td>
                    <td>
                        <asp:TextBox ID="txtspv" runat="server" style="width:220px; font-size: 1.0em"></asp:TextBox>
                        <img alt="add" id="Img3" src="../images/Search.gif" align="absmiddle" style="cursor: pointer;" onclick="OpenPopuptrans('trans')" /> 
                        <asp:HiddenField ID="hkdspv" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Costcode
                    </td>
                    <td>
                        <asp:TextBox ID="txtcostcode" runat="server" style="width:50px; font-size: 1.0em"></asp:TextBox>
                        <asp:HiddenField ID="hcostcode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Special Request
                    </td>
                    <td>
                        <%--<asp:DropDownList ID="ddlvtype" runat="server" >
                            <asp:ListItem Value="00">Pool Car</asp:ListItem>
                            <asp:ListItem Value="01">Taxi</asp:ListItem>
                        </asp:DropDownList>--%>
                        <asp:TextBox ID="txtspreq" runat="server" MaxLength="500" Width="400px" Height="100px" TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <%--<tr>
                    <td>
                        
                    </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Employee
                    </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Visitors
                    </td>
                </tr>
                <tr>
                    <td>
                        Passenger   
                    </td>
                    <td>
                        <input style="width:90px" disabled="disabled" type="text" id="txtannualleaveeli1" value="" />
                    </td>
                    <td>
                        <input style="width:90px" disabled="disabled" type="text" id="txtannualleaveeli2" value ="" />
                    </td>
                </tr>--%>
            </table>
            <br />
            
<table width="90%" border="0" cellpadding="0" cellspacing="0" class="data-table" >
        
    <tr>
    <td style="background-color:Gray;border-width:thin; border-style:solid;">
    <div style="float:left; width:358px; text-align:center; background-color:Gray; color:White;"> 
        Departure </div>
    <div style="float:left; width:316px; text-align:center; background-color:Gray; color:White;"> 
        Arrival </div>
    <div style="float:left; width:470px; background-color:Gray; border-right-style:none; color:White;"> 
        <center>Purpose</center> </div>
    </td>
    
        <%--<td style="text-align:center">
            Departure
        </td>
        <td style="text-align:center">
            Arrival
        </td>
        <td style="text-align:center">
            Purpose
        </td>--%>
    </tr>
    <tr>
        <td colspan="10">
       
       <%--<asp:GridView ID="GridView2" runat="server" DataKeyNames="No" AutoGenerateColumns="False" Width="795px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" EmptyDataText="No User Associated to this Document." ForeColor="Red">
            <Columns>
                <asp:BoundField DataField="Time" HeaderText="Time">
                <HeaderStyle Width="30px" />
                </asp:BoundField>
                <asp:BoundField DataField="Date" HeaderText="Date" >
                <HeaderStyle Width="30px" />
                </asp:BoundField>
                <asp:BoundField DataField="From" HeaderText="From" >
                <HeaderStyle Width="10px" />
                </asp:BoundField>
                <asp:BoundField DataField="Timeto" HeaderText="Timeto">
                <HeaderStyle Width="10px" />
                </asp:BoundField>
                <asp:BoundField DataField="Dateto" HeaderText="Dateto" >
                    <HeaderStyle Width="10px" />
                </asp:BoundField>
                
                <asp:BoundField DataField="To" HeaderText="To" >
                    <HeaderStyle Width="150px" />
                </asp:BoundField>
                <asp:BoundField DataField="Purpose" HeaderText="Purpose">
                    <HeaderStyle Width="150px" />
                </asp:BoundField>
                <asp:CommandField ShowDeleteButton="true" ButtonType="Link" >
                    <HeaderStyle Width="10px" />
                </asp:CommandField>
                <asp:CommandField ShowEditButton="true" ButtonType="Link">
                <HeaderStyle Width="10px" />
                </asp:CommandField>
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <RowStyle ForeColor="#000066" BorderStyle="Groove" BorderColor="#CCCCCC"/>
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <HeaderStyle BackColor="#0066ff" Font-Bold="false" ForeColor="#282828" />
        </asp:GridView>--%>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="No"
       OnRowCancelingEdit="GridView1_RowCancelingEdit"
       OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" OnRowCommand="GridView1_RowCommand"
       ShowFooter="True" OnRowDeleting="GridView1_RowDeleting">
       <Columns>
          <%-- <asp:TemplateField HeaderText="No" SortExpression="No" HeaderStyle-Width="30px">
                   <EditItemTemplate>
                     <asp:TextBox Width="30px" Enabled = "false"  ID="txtNoT" runat="server" Text='<%# Eval("No") %>'></asp:TextBox>
                   </EditItemTemplate>
                   <FooterTemplate>
                     <asp:TextBox Width="30px" Enabled = "false" BorderStyle="None" ID="txtNewNoT" runat="server"></asp:TextBox>
                   </FooterTemplate>
                   <ItemTemplate>
                     <asp:Label ID="LblNoT" runat="server" Text='<%# Bind("No") %>'></asp:Label>
                   </ItemTemplate>
           </asp:TemplateField>--%>
           <asp:TemplateField HeaderText="Date">
               <EditItemTemplate>
                   <asp:TextBox ID="txtDate" style="width:80px" runat="server" Text='<%# Eval("Date") %>'></asp:TextBox>
                   <button id="btn2" style="height:20px; width:15px;">..</button>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewdt" runat="server" style="width:80px"></asp:TextBox> 
                   <button id="btn1" style="height:20px; width:15px;">..</button>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label6" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Time" SortExpression="Time">
               <EditItemTemplate>
                   <asp:TextBox ID="txtTime" Width="50px" runat="server" Text='<%# Eval("Time") %>'></asp:TextBox>
                   <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender3" runat="server"
                    TargetControlID="txtTime" 
                    Mask="99:99"
                    MessageValidatorTip="true"
                    OnFocusCssClass="MaskedEditFocus"
                    OnInvalidCssClass="MaskedEditError"
                    MaskType="Time"
                    AcceptAMPM="False"
                    ErrorTooltipEnabled="True" />
                    <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator3" runat="server"
                    ControlExtender="MaskedEditExtender3"
                    ControlToValidate="txtTime"
                    IsValidEmpty="False"
                    EmptyValueMessage="Time is required"
                    InvalidValueMessage="Time is invalid"
                    Display="Dynamic"
                    TooltipMessage="Input a time"
                    EmptyValueBlurredText="*"
                    InvalidValueBlurredMessage="*"
                    ValidationGroup="MKE"/>
                   <%--<asp:TextBox ID="TextBox1" Width="25px" runat="server" Text='<%# Eval("Time") %>'></asp:TextBox>--%>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewtm" runat="server" style="width:50px"></asp:TextBox> 
                   <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender4" runat="server"
                    TargetControlID="txtNewtm" 
                    Mask="99:99"
                    MessageValidatorTip="true"
                    OnFocusCssClass="MaskedEditFocus"
                    OnInvalidCssClass="MaskedEditError"
                    MaskType="Time"
                    AcceptAMPM="False"
                    ErrorTooltipEnabled="True" />
                    <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator4" runat="server"
                    ControlExtender="MaskedEditExtender4"
                    ControlToValidate="txtNewtm"
                    IsValidEmpty="False"
                    EmptyValueMessage="Time is required"
                    InvalidValueMessage="Time is invalid"
                    Display="Dynamic"
                    TooltipMessage="Input a time"
                    EmptyValueBlurredText="*"
                    InvalidValueBlurredMessage="*"
                    ValidationGroup="MKE"/>
                   <%--<asp:TextBox ID="TextBox2" runat="server" style="width:25px"></asp:TextBox>--%>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label2" runat="server" Text='<%# Bind("Time") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>         
           <asp:TemplateField HeaderText="From" HeaderStyle-Width="100px" >
               <EditItemTemplate>
                   <asp:TextBox ID="txtfrom" runat="server" Text='<%# Bind("From") %>'></asp:TextBox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewfrom" runat="server"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label3" runat="server" Text='<%# Bind("From") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Date">
               <EditItemTemplate>
                   <asp:TextBox ID="txtdateto" Width="80px" runat="server" Text='<%# Eval("DateTo") %>'></asp:TextBox>
                   <button id="btn4" style="height:20px; width:15px;">..</button>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewDateto" runat="server" style="width:80px"></asp:TextBox>
                   <button id="btn3" style="height:20px; width:15px;">..</button>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label12" runat="server" Text='<%# Bind("DateTo") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Time" SortExpression="State">
               <EditItemTemplate>
                   <asp:TextBox ID="txttimeto" Width="50px" runat="server" Text='<%# Eval("Timeto") %>'></asp:TextBox>
                   <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender5" runat="server"
                    TargetControlID="txttimeto" 
                    Mask="99:99"
                    MessageValidatorTip="true"
                    OnFocusCssClass="MaskedEditFocus"
                    OnInvalidCssClass="MaskedEditError"
                    MaskType="Time"
                    AcceptAMPM="False"
                    ErrorTooltipEnabled="True" />
                    <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator5" runat="server"
                    ControlExtender="MaskedEditExtender5"
                    ControlToValidate="txttimeto"
                    IsValidEmpty="False"
                    EmptyValueMessage="Time is required"
                    InvalidValueMessage="Time is invalid"
                    Display="Dynamic"
                    TooltipMessage="Input a time"
                    EmptyValueBlurredText="*"
                    InvalidValueBlurredMessage="*"
                    ValidationGroup="MKE"/>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewTimeto" runat="server" style="width:50px"></asp:TextBox>
                   <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender6" runat="server"
                    TargetControlID="txtNewTimeto" 
                    Mask="99:99"
                    MessageValidatorTip="true"
                    OnFocusCssClass="MaskedEditFocus"
                    OnInvalidCssClass="MaskedEditError"
                    MaskType="Time"
                    AcceptAMPM="False"
                    ErrorTooltipEnabled="True" />
                    <ajaxToolkit:MaskedEditValidator ID="MaskedEditValidator6" runat="server"
                    ControlExtender="MaskedEditExtender6"
                    ControlToValidate="txtNewTimeto"
                    IsValidEmpty="False"
                    EmptyValueMessage="Time is required"
                    InvalidValueMessage="Time is invalid"
                    Display="Dynamic"
                    TooltipMessage="Input a time"
                    EmptyValueBlurredText="*"
                    InvalidValueBlurredMessage="*"
                    ValidationGroup="MKE"/>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label4" runat="server" Text='<%# Bind("Timeto") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="To" HeaderStyle-Width="100px">
               <EditItemTemplate>
                   <asp:TextBox ID="txtto" runat="server" Text='<%# Eval("To") %>'></asp:TextBox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewTo" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label8" runat="server" Text='<%# Bind("To") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Purpose" HeaderStyle-Width="300px">
                <EditItemTemplate>
                   <asp:textbox ID="txtpurpose" Width="300px" runat="server" Text='<%# Eval("Purpose") %>'></asp:textbox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewPurpose" runat="server" style="width:300px"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label10" runat="server" Text='<%# Bind("Purpose") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="" ShowHeader="False">
               <EditItemTemplate>
                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update"
                       Text="Update"></asp:LinkButton>
                   <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                       Text="Cancel"></asp:LinkButton>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="AddNew"
                       Text="Save"></asp:LinkButton>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit"
                       Text="Edit"></asp:LinkButton>
               </ItemTemplate>
           </asp:TemplateField>
           
           <asp:CommandField HeaderText="" ShowDeleteButton="True" ShowHeader="False" />
       </Columns>
   </asp:GridView>
            </td>
        </tr>
        
        <%--<tr>
            <td>
                <asp:Button ID="btnadd" runat="server" Text="Add" />
            </td>
        </tr>--%>
</table> 
<br />
<table width="90%" border="0" cellpadding="0" cellspacing="0" class="data-table">
<tr>
    <td colspan="2" style="border-width:thin; border-style:solid; text-align:center; background-color:Gray; color:White;">
        Passenger
    </td>
</tr>
<%--<tr>
    <td>
        Name
    </td>
    <td>
        Type
    </td>
</tr>--%>
<%--<tr>
    <td>
        <asp:TextBox ID="TextBox1" runat="server" style="width:260px; font-size: 1.0em" ReadOnly="true"></asp:TextBox>
    </td>
    <td style="width:400px;">
        <asp:DropDownList ID="DropDownList1" runat="server" >
                <asp:ListItem Value="00">Employee</asp:ListItem>
                <asp:ListItem Value="01">Visitor</asp:ListItem>
        </asp:DropDownList>
    </td>
</tr>--%>
<tr>
    <td colspan = "3">
        <asp:GridView ID="Gridview2" runat="server" AutoGenerateColumns="False" DataKeyNames="PNo"
        OnRowCancelingEdit="GridView2_RowCancelingEdit" 
        OnRowEditing="GridView2_RowEditing" OnRowUpdating="GridView2_RowUpdating" OnRowCommand="GridView2_RowCommand"
        ShowFooter="True" OnRowDeleting="GridView2_RowDeleting">
                <Columns>
                <%--<asp:TemplateField HeaderText="No" SortExpression="No">
                   <EditItemTemplate>
                     <asp:TextBox Width="30px" Enabled = "false" ID="txtNo" runat="server" Text='<%# Eval("PNo") %>'></asp:TextBox>
                   </EditItemTemplate>
                   <FooterTemplate>
                       <asp:TextBox ID="txtNewNo" Enabled = "false" BorderStyle="None" runat="server" style="width:30px"></asp:TextBox>
                   </FooterTemplate>
                   <ItemTemplate>
                       <asp:Label ID="LblNo" runat="server" Text='<%# Bind("PNo") %>'></asp:Label>
                   </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="Nik" SortExpression="Nik">
                   <EditItemTemplate>
                     <asp:TextBox Width="60px" ID="txtNik" runat="server" Text='<%# Eval("PNik") %>'></asp:TextBox>
                   </EditItemTemplate>
                   <FooterTemplate>
                       <asp:TextBox ID="txtNewNik" runat="server" style="width:60px"></asp:TextBox>
                   </FooterTemplate>
                   <ItemTemplate>
                       <asp:Label ID="LblNik" runat="server" Text='<%# Bind("PNik") %>'></asp:Label>
                   </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="240px" HeaderText = "Name" SortExpression="Passenger">
                    <EditItemTemplate>
                     <asp:TextBox ID="txtPName" runat="server" Width="200px" Text='<%# Eval("PName") %>'></asp:TextBox>
                     <img alt="add" id="Img1" src="../images/Search.gif" align="absmiddle" style="cursor: pointer" onclick="OpenPopup('suer')" />
                   </EditItemTemplate>
                   <FooterTemplate>
                       <asp:TextBox Width="200px" ID="txtNewPName" runat="server"></asp:TextBox>
                       <img alt="add" id="Img1" src="../images/Search.gif" align="absmiddle" style="cursor: pointer" onclick="OpenPopup('suer')" /> 
                   </FooterTemplate>
                    <ItemTemplate>
                       <asp:Label Width="200px" ID="LblPName" runat="server" Text='<%# Bind("PName") %>'></asp:Label>
                   </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Employee / Visitor"> 
                    <EditItemTemplate> 
                      <asp:DropDownList ID="cmbType" runat="server" SelectedValue='<%# Eval("PType") %>'> 
                        <asp:ListItem Value="Employee" Text="Employee"></asp:ListItem>
                        <asp:ListItem Value="Visitor" Text="Visitor"></asp:ListItem>
                      </asp:DropDownList> 
                    </EditItemTemplate> 
                    <ItemTemplate> 
                      <asp:Label ID="lbGender" runat="server" Text='<%# Eval("PType") %>'></asp:Label> 
                    </ItemTemplate> 
                    <FooterTemplate> 
                      <asp:DropDownList ID="cmbNewType" runat="server" >
                        <asp:ListItem Selected="True" Text="Employee" Value="Employee"></asp:ListItem> 
                        <asp:ListItem Text="Visitor" Value="Visitor"></asp:ListItem> </asp:DropDownList> 
                    </FooterTemplate> 
                </asp:TemplateField> 
               
               <asp:TemplateField HeaderText="" ShowHeader="False">
               <EditItemTemplate>
                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update"
                       Text="Update"></asp:LinkButton>
                   <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                       Text="Cancel"></asp:LinkButton>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="AddNew"
                       Text="Save"></asp:LinkButton>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit"
                       Text="Edit"></asp:LinkButton>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:CommandField HeaderText="" ShowDeleteButton="True" ShowHeader="False" />
                </Columns>
        </asp:GridView>
    </td>
</tr>
<tr>
<td>
<br />
<asp:Button ID="btnsave" Width="100px" Height = "20px" Text="Save Request" runat="server" /> <asp:Button ID="btnprint" Enabled = "false" Width="50px" Height = "20px" Text="Print" runat="server" /></td>
<td><input type="hidden" id="ctrlToFind" /> <input type="hidden" id="ctnm" /><asp:HiddenField ID="cnt" runat = "server" /> <asp:HiddenField ID="cnt2" runat = "server" /></td> 
</tr>
</table> 
</td></tr></table>
 <%--<script type = "text/javascript" >
            var cal = Calendar.setup({
                onSelect: function(cal) { cal.hide() },
                showTime: false
            });

            cal.manageFields("btn1", "ctl00_ContentPlaceHolder1_GridView1_ctl03_txtNewdt", "%m/%d/%Y");
            cal.manageFields("btn2", "ctl00_ContentPlaceHolder1_GridView1_ctl02_txtDate", "%m/%d/%Y");
</script>--%>
<%=js %>
<%=msg%>
</asp:Content>