﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports System.Xml.Serialization
Imports System.Diagnostics
Imports EXCELLENT.Excellent
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
<ScriptService()> _
Public Class Service
    Inherits System.Web.Services.WebService
    <WebMethod(True)> _
    <ScriptMethod()> _
    Public Function Addpermit(ByVal _type As String, ByVal _subject As String, ByVal _comp As String, ByVal _site As String, ByVal _year As String, ByVal _category As String, ByVal _department As String, ByVal _subdept As String, ByVal _remdays As String, ByVal _pic1 As String, ByVal _pic2 As String, ByVal _pic3 As String, ByVal _pic4 As String, ByVal _pic5 As String, ByVal _dtissue As String, ByVal _dtexp As String, ByVal _url As String, ByVal _filename As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim sqlQuery As String
        Dim cmd As SqlCommand
        Dim dttm As String
        Dim dtmnow As String
        Dim dtynow As String
        Dim stripno As String
        Dim permitid As String
        Dim nmcomp As String
        Dim nmsite As String
        Dim mustreport As String
        Dim conn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim i As Integer
        Dim strcon As String = "select id from docpermit_trans"
        Dim dtb As DataTable = New DataTable
        Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, conn)
        Dim nik1 As String
        Dim nik2 As String
        Dim nik3 As String
        Dim nik4 As String
        Dim nik5 As String

        If _remdays <> 0 Then mustreport = "1"

        Try
            dtmnow = DateTime.Now.ToString("MM")
            dtynow = Date.Now.Year.ToString
            stripno = dtynow + dtmnow + "/"

            sda.Fill(dtb)

            If dtb.Rows.Count = 0 Then
                permitid = stripno + "000001"
            ElseIf dtb.Rows.Count > 0 Then
                i = 0
                i = dtb.Rows.Count + 1
                If dtb.Rows.Count > 0 And dtb.Rows.Count < 9 Then
                    permitid = stripno + "00000" + i.ToString
                ElseIf dtb.Rows.Count >= 9 And dtb.Rows.Count < 99 Then
                    permitid = stripno + "0000" + i.ToString
                ElseIf dtb.Rows.Count >= 99 And dtb.Rows.Count < 999 Then
                    permitid = stripno + "000" + i.ToString
                ElseIf dtb.Rows.Count >= 999 And dtb.Rows.Count < 9999 Then
                    permitid = stripno + "00" + i.ToString
                ElseIf dtb.Rows.Count >= 9999 And dtb.Rows.Count < 99999 Then
                    permitid = stripno + "0" + i.ToString
                ElseIf dtb.Rows.Count >= 99999 And dtb.Rows.Count < 999999 Then
                    permitid = stripno + i.ToString
                ElseIf dtb.Rows.Count >= 999999 Then
                    permitid = "Error on generate Tripnum"
                End If
            End If
            conn2.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        conn.Open()

        Try
            Dim strcon2 As String = "select Nmcompany from jrn_test.dbo.H_A110 where kdcompany = '" + _comp + "'"
            Dim dtb2 As DataTable = New DataTable
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, conn)
            sda2.Fill(dtb2)

            nmcomp = dtb2.Rows(0)!Nmcompany.ToString
        Catch ex As Exception

        End Try

        Try
            Dim strcon3 As String = "select Nmsite from jrn_test.dbo.H_A120 where kdsite = '" + _site + "'"
            Dim dtb3 As DataTable = New DataTable
            Dim sda3 As SqlDataAdapter = New SqlDataAdapter(strcon3, conn)
            sda3.Fill(dtb3)

            nmsite = dtb3.Rows(0)!Nmsite.ToString
        Catch ex As Exception

        End Try

        dttm = Date.Now.ToString

        'If _pic1 <> Nothing Then
        '    Try
        '        Dim sqlqueryemail1 As String = "SELECT pic_code FROM docpermit_pic WHERE pic_email = '" + _pic1 + "'"
        '        Dim dtbemail1 As DataTable = New DataTable
        '        Dim sdaemail1 As SqlDataAdapter = New SqlDataAdapter(sqlqueryemail1, conn)
        '        sdaemail1.Fill(dtbemail1)

        '        If dtbemail1.Rows.Count > 0 Then
        '            nik1 = dtbemail1.Rows(0)!pic_code
        '        End If
        '    Catch ex As Exception

        '    End Try
        'Else
        '    nik1 = ""
        'End If

        'If _pic2 <> Nothing Then
        '    Try
        '        Dim sqlqueryemail2 As String = "SELECT pic_code FROM docpermit_pic WHERE pic_email = '" + _pic2 + "'"
        '        Dim dtbemail2 As DataTable = New DataTable
        '        Dim sdaemail2 As SqlDataAdapter = New SqlDataAdapter(sqlqueryemail2, conn)
        '        sdaemail2.Fill(dtbemail2)

        '        If dtbemail2.Rows.Count > 0 Then
        '            nik2 = dtbemail2.Rows(0)!pic_code
        '        End If
        '    Catch ex As Exception

        '    End Try
        'Else
        '    nik2 = ""
        'End If

        'If _pic3 <> Nothing Then
        '    Try
        '        Dim sqlqueryemail3 As String = "SELECT pic_code FROM docpermit_pic WHERE pic_email = '" + _pic3 + "'"
        '        Dim dtbemail3 As DataTable = New DataTable
        '        Dim sdaemail3 As SqlDataAdapter = New SqlDataAdapter(sqlqueryemail3, conn)
        '        sdaemail3.Fill(dtbemail3)

        '        If dtbemail3.Rows.Count > 0 Then
        '            nik3 = dtbemail3.Rows(0)!pic_code
        '        End If
        '    Catch ex As Exception

        '    End Try
        'Else
        '    nik3 = ""
        'End If

        'If _pic4 <> Nothing Then
        '    Try
        '        Dim sqlqueryemail4 As String = "SELECT pic_code FROM docpermit_pic WHERE pic_email = '" + _pic4 + "'"
        '        Dim dtbemail4 As DataTable = New DataTable
        '        Dim sdaemail4 As SqlDataAdapter = New SqlDataAdapter(sqlqueryemail4, conn)
        '        sdaemail4.Fill(dtbemail4)

        '        If dtbemail4.Rows.Count > 0 Then
        '            nik4 = dtbemail4.Rows(0)!pic_code
        '        End If
        '    Catch ex As Exception

        '    End Try
        'Else
        '    nik4 = ""
        'End If

        'If _pic5 <> Nothing Then
        '    Try
        '        Dim sqlqueryemail5 As String = "SELECT pic_code FROM docpermit_pic WHERE pic_email = '" + _pic5 + "'"
        '        Dim dtbemail5 As DataTable = New DataTable
        '        Dim sdaemail5 As SqlDataAdapter = New SqlDataAdapter(sqlqueryemail5, conn)
        '        sdaemail5.Fill(dtbemail5)

        '        If dtbemail5.Rows.Count > 0 Then
        '            nik5 = dtbemail5.Rows(0)!pic_code
        '        End If
        '    Catch ex As Exception

        '    End Try
        'Else
        '    nik5 = ""
        'End If

        Dim div As String = Session("divisi")

        Try
            sqlQuery = "Insert into docpermit_trans (id,doctype,fsubject,kdcom,company_name,kdsite,site_name,fyear,fcategory,department,subdepartment,cms_start,cms_end, f_path, f_name, stedit,createdtime, must_report, rem_code, kddiv, modul, createdby) values ('" + permitid + "' , '" + _type + "', '" + _subject + "', '" + _comp + "', '" + nmcomp + "', '" + _site + "', '" + nmsite + "', '" + _year + "', '" + _category + "', '" + _department + "', '" + _subdept + "', '" + _dtissue + "', '" + _dtexp + "', '" + _url + "', '" + _filename + "', '1','" + dttm + "','" + mustreport + "','" + _remdays + "','" + div + "' ,'" + Session("modul_permit").ToString + "', '" + Session("otorisasi_permit") + "')"
            'sqlQuery = "Insert into docpermit_trans (id,ftype,fsubject,kdcom,company_name,kdsite,site_name,fyear,fcategory,doctype,department,subdepartment,number,must_report,cms_start,cms_end, f_path, f_name, stedit) values ('" + permitid + "' , '" + _type + "', '" + _subject + "', '" + _comp + "', '" + nmcomp + "', '" + _site + "', '" + nmsite + "', '" + _year + "', '" + _category + "', '" + _doctype + "', '" + _department + "', '" + _subdept + "', '" + _number + "', '" + _report + "', '" + _dtissue + "', '" + _dtexp + "', 'C:\uploads\" + _fileupload1 + "', '" + _filename + "', '1' )"
            cmd = New SqlCommand(sqlQuery, conn)
            cmd.ExecuteScalar()

            Return "success"
        Catch ex As Exception

            Return "failed"
        Finally
            conn.Close()
        End Try
    End Function

    <WebMethod(True)> _
 <ScriptMethod()> _
 Public Function Editpermit(ByVal _type As String, ByVal _subject As String, ByVal _comp As String, ByVal _site As String, ByVal _year As String, ByVal _category As String, ByVal _doctype As String, ByVal _department As String, ByVal _subdept As String, ByVal _number As String, ByVal _report As String, ByVal _dtissue As String, ByVal _dtexp As String, ByVal _fname As String, ByVal _rem_days As String, ByVal _txtpic1 As String, ByVal _txtpic2 As String, ByVal _txtpic3 As String, ByVal _txtpic4 As String, ByVal _txtpic5 As String) As String
        Dim pid As String
        Dim nmcomp As String
        Dim nmsite As String
        Dim dttm As String
        Dim nik1 As String
        Dim nik2 As String
        Dim nik3 As String
        Dim nik4 As String
        Dim nik5 As String

        pid = Session("Pid")
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)

        conn.Open()

        Try
            Dim strcon2 As String = "select Nmcompany from jrn_test.dbo.H_A110 where kdcompany = '" + _comp + "'"
            Dim dtb2 As DataTable = New DataTable
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, conn)
            sda2.Fill(dtb2)

            nmcomp = dtb2.Rows(0)!Nmcompany.ToString
        Catch ex As Exception

        End Try

        Try
            Dim strcon3 As String = "select Nmsite from jrn_test.dbo.H_A120 where kdsite = '" + _site + "'"
            Dim dtb3 As DataTable = New DataTable
            Dim sda3 As SqlDataAdapter = New SqlDataAdapter(strcon3, conn)
            sda3.Fill(dtb3)

            nmsite = dtb3.Rows(0)!Nmsite.ToString
        Catch ex As Exception

        End Try

        'Try
        '    Dim old_path As String
        '    Dim strcon As String = "select f_path from docpermit_trans where [id] ='" + pid + "'"
        '    Dim dtb As DataTable = New DataTable
        '    Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, conn)
        '    sda.Fill(dtb)

        '    If dtb.Rows.Count > 0 Then
        '        old_path = dtb.Rows(0)!f_path
        '        'old_path = Replace(old_path, "\\files.jresources.com", "\\jrnsharepoint")

        '    End If
        '    Dim s1 As String
        '    Dim s2 As String
        '    Dim s3 As String

        '    'Dim LogonCred As NetworkCredential = New NetworkCredential("syam.kharisman", "sysyam", "jresources.com")
        '    'Dim myCache As New CredentialCache()

        '    'myCache.Add(New Uri("\\20.110.1.6\syam\"), "Basic", New NetworkCredential("syam", "r3d0.678"))

        '    Dim filepath_new As String = _doctype + "_" + _subject + "_" + _comp + "_" + _site + "_" + _year + "_" + _fname
        '    Dim filepath_old As String = old_path
        '    Dim fname_old As String
        '    'If File.Exists(filepath_old) Then
        '    '    ' Give a new name
        '    '    My.Computer.FileSystem.RenameFile(filepath_old, filepath_new)
        '    'Else
        '    '    ' Use existing name
        '    'End If
        '    fname_old = Path.GetFileName(filepath_old)
        '    RenameFileOnServer("ftp://20.110.1.6/syam/", fname_old, filepath_new, "syam", "r3d0.678")

        'Catch ex As Exception

        'End Try

        dttm = Date.Now.ToString

        If _txtpic1 <> Nothing Then
            Try
                Dim sqlqueryemail1 As String = "SELECT pic_code FROM docpermit_pic WHERE pic_email = '" + _txtpic1 + "'"
                Dim dtbemail1 As DataTable = New DataTable
                Dim sdaemail1 As SqlDataAdapter = New SqlDataAdapter(sqlqueryemail1, conn)
                sdaemail1.Fill(dtbemail1)

                If dtbemail1.Rows.Count > 0 Then
                    nik1 = dtbemail1.Rows(0)!pic_code
                End If
            Catch ex As Exception

            End Try
        Else
            nik1 = ""
        End If

        If _txtpic2 <> Nothing Then
            Try
                Dim sqlqueryemail2 As String = "SELECT pic_code FROM docpermit_pic WHERE pic_email = '" + _txtpic2 + "'"
                Dim dtbemail2 As DataTable = New DataTable
                Dim sdaemail2 As SqlDataAdapter = New SqlDataAdapter(sqlqueryemail2, conn)
                sdaemail2.Fill(dtbemail2)

                If dtbemail2.Rows.Count > 0 Then
                    nik2 = dtbemail2.Rows(0)!pic_code
                End If
            Catch ex As Exception

            End Try
        Else
            nik2 = ""
        End If

        If _txtpic3 <> Nothing Then
            Try
                Dim sqlqueryemail3 As String = "SELECT pic_code FROM docpermit_pic WHERE pic_email = '" + _txtpic3 + "'"
                Dim dtbemail3 As DataTable = New DataTable
                Dim sdaemail3 As SqlDataAdapter = New SqlDataAdapter(sqlqueryemail3, conn)
                sdaemail3.Fill(dtbemail3)

                If dtbemail3.Rows.Count > 0 Then
                    nik3 = dtbemail3.Rows(0)!pic_code
                End If
            Catch ex As Exception

            End Try
        Else
            nik3 = ""
        End If

        If _txtpic4 <> Nothing Then
            Try
                Dim sqlqueryemail4 As String = "SELECT pic_code FROM docpermit_pic WHERE pic_email = '" + _txtpic4 + "'"
                Dim dtbemail4 As DataTable = New DataTable
                Dim sdaemail4 As SqlDataAdapter = New SqlDataAdapter(sqlqueryemail4, conn)
                sdaemail4.Fill(dtbemail4)

                If dtbemail4.Rows.Count > 0 Then
                    nik4 = dtbemail4.Rows(0)!pic_code
                End If
            Catch ex As Exception

            End Try
        Else
            nik4 = ""
        End If

        If _txtpic5 <> Nothing Then
            Try
                Dim sqlqueryemail5 As String = "SELECT pic_code FROM docpermit_pic WHERE pic_email = '" + _txtpic5 + "'"
                Dim dtbemail5 As DataTable = New DataTable
                Dim sdaemail5 As SqlDataAdapter = New SqlDataAdapter(sqlqueryemail5, conn)
                sdaemail5.Fill(dtbemail5)

                If dtbemail5.Rows.Count > 0 Then
                    nik5 = dtbemail5.Rows(0)!pic_code
                End If
            Catch ex As Exception

            End Try
        Else
            nik5 = ""
        End If

        Try
            'Dim sqlquery As String = "update docpermit_trans set ftype= '" + _type + "', fsubject='" + _subject + "', kdcom ='" + _comp + "', company_name='" + nmcomp + "', kdsite ='" + _site + "', site_name='" + nmsite + "', fyear='" + _year + "', fcategory = '" + _category + "',doctype = '" + _doctype + "', department = '" + _department + "', subdepartment = '" + _subdept + "', number = '" + _number + "', must_report = '" + _report + "', cms_start = '" + _dtissue + "', cms_end='" + _dtexp + "', f_name = '" + _fname + "', modifiedtime = '" + dttm + "', rem_code = '" + _rem_days + "' where [id] = '" + pid + "'"

            Dim sqlquery As String = "update docpermit_trans set ftype= '" + _type + "', fsubject='" + _subject + "', kdcom ='" + _comp + "', company_name='" + nmcomp + "', kdsite ='" + _site + "', site_name='" + nmsite + "',doctype = '" + _doctype + "', must_report = '" + _report + "', cms_start = '" + _dtissue + "', cms_end='" + _dtexp + "', f_name = '" + _fname + "', modifiedtime = '" + dttm + "', rem_code = '" + _rem_days + "', modifiedby = '" + Session("otorisasi_permit") + "' where [id] = '" + pid + "'"
            'Dim sqlquery As String = "update docpermit_trans set ftype= '" + _type + "', fsubject='" + _subject + "', kdcom ='" + _comp + "', company_name='" + nmcomp + "', kdsite ='" + _site + "', site_name='" + nmsite + "', fyear='" + _year + "', fcategory = '" + _category + "',doctype = '" + _doctype + "', department = '" + _department + "', subdepartment = '" + _subdept + "', number = '" + _number + "', must_report = '" + _report + "', cms_start = '" + _dtissue + "', cms_end='" + _dtexp + "', f_name = '" + _fname + "', f_path = 'C:\uploads\" + _type + "_" + _subject + "_" + _comp + "_" + _site + "_" + _year + "_" + _fname + "' where [id] = '" + pid + "'"

            Dim cmd As SqlCommand
            cmd = New SqlCommand(sqlquery, conn)
            cmd.ExecuteScalar()
        Catch ex As Exception

        End Try

        conn.Close()
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
   Public Function AddIssue(ByVal _issue As List(Of MyIssue)) As String
        Dim _adapter As New ExcellentTableAdapters.DRAPATTableAdapter()
        Try
            Dim dt As New DRAPATDataTable()
            For Each i As MyIssue In _issue
                If i.NoMeet = "NEW" Then
                    i.NoMeet = i.IDMeet
                    Dim dr As DRAPATRow
                    dr = dt.NewDRAPATRow()
                    dr.IdRap = i.NoMeet
                    dr.IdIssue = i.NoIssue
                    dr.NmIssue = i.NmIssue
                    dr.Action = i.Action
                    dr.NoAction = i.NoRef
                    dr.NikPIC = i.PIC
                    dr.JnsIssue = i.jenis
                    dr.StIssue = i.status
                    dr.DueDate = DateTime.ParseExact(i.due, "MM/dd/yyyy", Nothing)
                    dr.StEdit = "0"

                    dr.CreatedIn = HttpContext.Current.Request.UserHostName
                    dr.CreatedTime = DateTime.Now
                    dt.AddDRAPATRow(dr)
                ElseIf i.Deleted = "1" Then
                    Dim dr As DRAPATRow
                    dr = dt.NewDRAPATRow()
                    dr.IdRap = i.NoMeet
                    dr.IdIssue = i.NoIssue
                    dr.NoAction = i.NoRef
                    dt.AddDRAPATRow(dr)
                    dr.AcceptChanges()
                    dr.Delete()
                Else
                    Dim dr As DRAPATRow
                    dr = dt.NewDRAPATRow()
                    dr.IdRap = i.NoMeet
                    dr.IdIssue = i.NoIssue
                    dr.NmIssue = i.NmIssue
                    dr.Action = i.Action
                    dr.NoAction = i.NoRef
                    dr.NikPIC = i.PIC
                    dr.JnsIssue = i.jenis
                    dr.StIssue = i.status
                    dr.DueDate = DateTime.ParseExact(i.due, "MM/dd/yyyy", Nothing)
                    dr.ModifiedIn = HttpContext.Current.Request.UserHostName
                    dr.ModifiedTime = DateTime.Now
                    dt.AddDRAPATRow(dr)
                    dr.AcceptChanges()
                    dr.SetModified()
                End If
            Next
            _adapter.Update(dt)
            Return "success"
        Catch ex As Exception
            MsgBox(ex.Message)
            Return "failed"
        Finally
            If _adapter.Connection.State = ConnectionState.Open Then
                MsgBox("closing connection")
                _adapter.Connection.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
  <ScriptMethod()> _
  Public Function DeleteIssue(ByVal _issue As MyIssue) As String
        Dim _adapter As New ExcellentTableAdapters.DRAPATTableAdapter()
        Try
            Dim dt As New DRAPATDataTable()
            Dim dr As DRAPATRow
            dr = dt.NewDRAPATRow()
            dr.IdRap = _issue.NoMeet
            dr.IdIssue = _issue.NoIssue
            dr.NoAction = _issue.NoRef
            dt.AddDRAPATRow(dr)
            dr.AcceptChanges()
            dr.Delete()
            _adapter.Update(dt)
            Return "success"
        Catch ex As Exception
            MsgBox(ex.Message)
            Return "failed"
        Finally
            If _adapter.Connection.State = ConnectionState.Open Then
                MsgBox("closing connection")
                _adapter.Connection.Close()
            End If
        End Try
    End Function

    Function GetNewNoBuk(ByVal dt As DataTable) As String
        Try
            Dim jumlahnobuk As String
            If dt.Rows.Count = 0 Then
                jumlahnobuk = "0"
            Else
                jumlahnobuk = CInt(Microsoft.VisualBasic.Right(dt.Rows(0)("jumlahNoBuk").ToString(), 4))
            End If
            Return (jumlahnobuk + 1).ToString("0000")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Function GenerateNoRapat(ByVal id As String, ByVal conn As SqlConnection) As DataTable
        Dim q As String = "select isnull(max(IDRap),0) as jumlahnobuk from HRapat where left(IDRap," + CStr(id.Length) + ")='" + CStr(id) + "'"
        Dim dt As New DataTable()
        Dim intRow As Integer
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            Dim cmSQL As New SqlCommand(q, conn)
            Dim ObjAdapter As New SqlDataAdapter(cmSQL)
            intRow = ObjAdapter.Fill(dt)
            cmSQL.Dispose()
            Return dt
        Catch ex As SqlException
            MsgBox(ex.Message)
        Finally
            If Not (conn Is Nothing) Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Public Function SaveRapat(ByVal _rapat As MyRapat, ByVal _peserta As List(Of MyPeserta), ByVal _issue As List(Of MyIssue), ByVal _depar As String, ByVal _site As String)
        Dim _adapter As New ExcellentTableAdapters.DRAPATTableAdapter()
        Try
            If DateTime.ParseExact(_rapat.TglRap, "dd/MM/yyyy", Nothing) > DateTime.ParseExact(Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", Nothing) Then
                Return "failed;Tanggal Meeting lebih besar daripada tanggal hari ini"
            End If
            If _rapat.IdRap = "NEW" Then
                Dim dt As New HRAPATDataTable()
                Dim dr As HRAPATRow
                dr = dt.NewHRAPATRow

                Dim _ksite As String = ""
                If _site = "JKT" Then
                    _ksite = "HO"
                Else
                    _ksite = _site
                End If

                If (_rapat.JenisRap = "1") Or (_rapat.JenisRap = "2") Then
                    'dr.IdRap = "NOT/" & IIf(GetJobSite(_rapat.Pimpinan) = "JKT", "HO", GetJobSite(_rapat.Pimpinan)) & "/PM/" & Right(Now.Year.ToString(), 2) & "/" & Now.Month.ToString("00") & "/" & GetNewNoBuk(GenerateNoRapat("NOT/" & _ksite & "/PM/" & Right(Now.Year.ToString(), 2) & "/" & Now.Month.ToString("00"), _adapter.Connection))
                    'Dim JS As String = IIf(GetJobSite(_rapat.Pimpinan) = "JKT", "HO", GetJobSite(_rapat.Pimpinan))
                    Dim JS As String = Getdept(_rapat.Pimpinan)
                    'dr.IdRap = "NOT/" & JS & "/PM/" & Right(Now.Year.ToString(), 2) & "/" & Now.Month.ToString("00") & "/" & GetNewNoBuk(GenerateNoRapat("NOT/" & JS & "/PM/" & Right(Now.Year.ToString(), 2) & "/" & Now.Month.ToString("00"), _adapter.Connection))
                    dr.IdRap = "NOT/" & JS & "/" & Now.Year.ToString() & "/" & Now.Month.ToString("00") & "/" & GetNewNoBuk(GenerateNoRapat("NOT/" & JS & "/" & Now.Year.ToString() & "/" & Now.Month.ToString("00"), _adapter.Connection))
                End If

                If (_rapat.JenisRap = "4") Or (_rapat.JenisRap = "5") Then
                    'dr.IdRap = "NOT/" & IIf(GetJobSite(_rapat.Pimpinan) = "JKT", "HO", GetJobSite(_rapat.Pimpinan)) & "/BOD/" & Right(Now.Year.ToString(), 2) & "/" & Now.Month.ToString("00") & "/" & GetNewNoBuk(GenerateNoRapat("NOT/" & _ksite & "/BOD/" & Right(Now.Year.ToString(), 2) & "/" & Now.Month.ToString("00"), _adapter.Connection))
                    'Dim JS As String = IIf(GetJobSite(_rapat.Pimpinan) = "JKT", "HO", GetJobSite(_rapat.Pimpinan))
                    Dim JS As String = Getdept(_rapat.Pimpinan)
                    'dr.IdRap = "NOT/" & JS & "/BOD/" & Right(Now.Year.ToString(), 2) & "/" & Now.Month.ToString("00") & "/" & GetNewNoBuk(GenerateNoRapat("NOT/" & JS & "/BOD/" & Right(Now.Year.ToString(), 2) & "/" & Now.Month.ToString("00"), _adapter.Connection))
                    dr.IdRap = "NOT/" & JS & "/" & Now.Year.ToString() & "/" & Now.Month.ToString("00") & "/" & GetNewNoBuk(GenerateNoRapat("NOT/" & JS & "/" & Now.Year.ToString() & "/" & Now.Month.ToString("00"), _adapter.Connection))
                End If

                If (_rapat.JenisRap >= "6") Or (_rapat.JenisRap = "3") Then
                    'dr.IdRap = "NOT/" & IIf(GetJobSite(_rapat.Pimpinan) = "JKT", "HO", GetJobSite(_rapat.Pimpinan)) & "/" & GetFunction(GetCodeFunction(_rapat.Pimpinan)) & "/" & Right(Now.Year.ToString(), 2) & "/" & Now.Month.ToString("00") & "/" & GetNewNoBuk(GenerateNoRapat("NOT/" & _ksite & "/" & GetFunction(GetCodeFunction(_rapat.Pimpinan)) & "/" & Right(Now.Year.ToString(), 2) & "/" & Now.Month.ToString("00"), _adapter.Connection))
                    'Dim JS As String = IIf(GetJobSite(_rapat.Pimpinan) = "JKT", "HO", GetJobSite(_rapat.Pimpinan))
                    Dim JS As String = Getdept(_rapat.Pimpinan)
                    If JS = "" Or JS = "null" Then
                        JS = "NFO"
                    End If
                    'dr.IdRap = "NOT/" & JS & "/" & GetFunction(GetCodeFunction(_rapat.Pimpinan)) & "/" & Right(Now.Year.ToString(), 2) & "/" & Now.Month.ToString("00") & "/" & GetNewNoBuk(GenerateNoRapat("NOT/" & JS & "/" & GetFunction(GetCodeFunction(_rapat.Pimpinan)) & "/" & Right(Now.Year.ToString(), 2) & "/" & Now.Month.ToString("00"), _adapter.Connection))
                    dr.IdRap = "NOT/" & JS & "/" & Now.Year.ToString() & "/" & Now.Month.ToString("00") & "/" & GetNewNoBuk(GenerateNoRapat("NOT/" & JS & "/" & Now.Year.ToString() & "/" & Now.Month.ToString("00"), _adapter.Connection))
                End If

                dr.NmRap = _rapat.NmRap
                dr.TmpRap = _rapat.TmpRap
                dr.TglRap = DateTime.ParseExact(_rapat.TglRap, "dd/MM/yyyy", Nothing)
                If _rapat.Agenda = "null" Then
                    dr.SetAgendaNull()
                Else
                    dr.Agenda = _rapat.Agenda.Replace("'", "`")
                End If
                dr.Pimpinan = _rapat.Pimpinan
                dr.NmPimpinan = _rapat.Nmpimpinan
                dr.Notulis = _rapat.Notulis
                dr.NmNotulis = _rapat.Nmnotulis
                dr.JenisRap = _rapat.JenisRap
                dr.WkRap = CDate("1/1/1900 " & _rapat.WkRap)
                dr.SdWkRap = CDate("1/1/1900 " & _rapat.SdwkRap)
                'dr.KdCabang = _site
                'dr.KdDepar = _depar
                dr.KdCabang = GetJobSite(_rapat.Pimpinan)
                dr.KdDepar = GetFunction(GetCodeFunction(_rapat.Pimpinan))
                If _rapat.Informasi = "null" Then
                    dr.SetInformasiNull()
                Else
                    dr.Informasi = _rapat.Informasi.Replace("'", "`")
                End If
                If _rapat.MemberEx = "null" Then
                    dr.SetMemberExNull()
                Else
                    dr.MemberEx = _rapat.MemberEx.Replace("'", "`")
                End If
                If _rapat.Kebijakan = "null" Then
                    dr.SetKebijakanNull()
                Else
                    dr.Kebijakan = _rapat.Kebijakan.Replace("'", "`")
                End If

                dr.StEdit = "0"
                dr.CreatedBy = _rapat.Notulis
                dr.CreatedIn = HttpContext.Current.Request.UserHostName
                dr.CreatedTime = Now
                dt.AddHRAPATRow(dr)

                Dim dtPeserta As New DPESERTADataTable()
                Dim drPeserta As DPESERTARow
                For Each o As MyPeserta In _peserta
                    drPeserta = dtPeserta.NewDPESERTARow()
                    drPeserta.IdRap = dr.IdRap
                    drPeserta.NikMember = o.NikMember
                    drPeserta.NmMember = o.NmMember
                    drPeserta.StHadir = o.StHadir
                    drPeserta.StEdit = "0"
                    drPeserta.CreatedBy = _rapat.Notulis
                    drPeserta.CreatedIn = HttpContext.Current.Request.UserHostName
                    drPeserta.CreatedTime = Now
                    dtPeserta.AddDPESERTARow(drPeserta)
                Next

                Dim drDRapat As DRAPATRow
                Dim dtDRapat As New DRAPATDataTable()
                Dim urutan As Integer = 0
                For Each i As MyIssue In _issue
                    drDRapat = dtDRapat.NewDRAPATRow()
                    drDRapat.IdRap = dr.IdRap
                    drDRapat.IdIssue = i.NoIssue
                    drDRapat.NmIssue = i.NmIssue.Replace("'", "`")
                    drDRapat.Action = i.Action.Replace("'", "`")
                    drDRapat.NoAction = i.NoRef
                    drDRapat.NikPIC = i.PIC
                    drDRapat.NmPIC = i.Nmpic
                    drDRapat.JnsIssue = i.jenis
                    drDRapat.StIssue = i.status
                    If i.due = "" Then
                        drDRapat.SetDueDateNull()
                    Else
                        drDRapat.DueDate = DateTime.ParseExact(i.due, "dd/MM/yyyy", Nothing)
                    End If
                    drDRapat.StEdit = "0"
                    drDRapat.CreatedIn = HttpContext.Current.Request.UserHostName
                    drDRapat.CreatedTime = DateTime.Now
                    If i.Info = "null" Then
                        drDRapat.SetNikInfoNull()
                    Else
                        drDRapat.NikInfo = i.Info
                    End If

                    If i.NmInfo = "null" Then
                        drDRapat.SetNmInfoNull()
                    Else
                        drDRapat.NmInfo = i.NmInfo
                    End If

                    drDRapat.Deliverable = i.Deliverables.Replace("'", "`")
                    drDRapat.Urutan = urutan
                    drDRapat.CreatedBy = _rapat.Notulis
                    drDRapat.CreatedIn = HttpContext.Current.Request.UserHostName
                    drDRapat.CreatedTime = Now
                    dtDRapat.AddDRAPATRow(drDRapat)
                    urutan += 1
                Next
                Dim transaction As SqlTransaction = Nothing
                Try
                    Using _HRapatAdapter As New ExcellentTableAdapters.HRAPATTableAdapter
                        transaction = TableAdapterHelper.BeginTransaction(_HRapatAdapter)
                        _HRapatAdapter.Update(dt)
                    End Using

                    Using _DPesertaAdapter As New ExcellentTableAdapters.DPESERTATableAdapter
                        TableAdapterHelper.SetTransaction(_DPesertaAdapter, transaction)
                        _DPesertaAdapter.Update(dtPeserta)
                    End Using

                    Using _DRapatAdapter As New ExcellentTableAdapters.DRAPATTableAdapter
                        TableAdapterHelper.SetTransaction(_DRapatAdapter, transaction)
                        _DRapatAdapter.Update(dtDRapat)
                    End Using
                    transaction.Commit()
                Catch
                    transaction.Rollback()
                    Throw
                Finally
                    transaction.Dispose()
                End Try
                Return "success;" & dr.IdRap
            Else
                Dim dt As New HRAPATDataTable()
                Dim dr As HRAPATRow
                dr = dt.NewHRAPATRow
                dr.IdRap = _rapat.IdRap
                dr.NmRap = _rapat.NmRap
                dr.TmpRap = _rapat.TmpRap
                dr.TglRap = DateTime.ParseExact(_rapat.TglRap, "dd/MM/yyyy", Nothing)
                If _rapat.Agenda = "null" Then
                    dr.SetAgendaNull()
                Else
                    dr.Agenda = _rapat.Agenda.Replace("'", "`")
                End If
                dr.Pimpinan = _rapat.Pimpinan
                dr.NmPimpinan = _rapat.Nmpimpinan
                dr.Notulis = _rapat.Notulis
                dr.NmNotulis = _rapat.Nmnotulis
                dr.JenisRap = _rapat.JenisRap
                dr.WkRap = CDate("1/1/1900 " & _rapat.WkRap)
                dr.SdWkRap = CDate("1/1/1900 " & _rapat.SdwkRap)
                If _rapat.Informasi = "null" Then
                    dr.SetInformasiNull()
                Else
                    dr.Informasi = _rapat.Informasi.Replace("'", "`")
                End If
                If _rapat.MemberEx = "null" Then
                    dr.SetMemberExNull()
                Else
                    dr.MemberEx = _rapat.MemberEx.Replace("'", "`")
                End If
                If _rapat.Kebijakan = "null" Then
                    dr.SetKebijakanNull()
                Else
                    dr.Kebijakan = _rapat.Kebijakan.Replace("'", "`")
                End If

                dr.StEdit = "0"
                'dr.KdCabang = _site
                'dr.KdDepar = _depar
                dr.KdCabang = GetJobSite(_rapat.Pimpinan)
                dr.KdDepar = GetFunction(GetCodeFunction(_rapat.Pimpinan))
                dr.ModifiedBy = _rapat.Notulis
                dr.ModifiedIn = HttpContext.Current.Request.UserHostName
                dr.ModifiedTime = Now
                dt.AddHRAPATRow(dr)
                dr.AcceptChanges()
                dr.SetModified()
                Dim dtPeserta As New DPESERTADataTable()
                Dim drPeserta As DPESERTARow
                For Each o As MyPeserta In _peserta
                    If o.IdRap = "NEW" Then
                        drPeserta = dtPeserta.NewDPESERTARow()
                        drPeserta.IdRap = dr.IdRap
                        drPeserta.NikMember = o.NikMember
                        drPeserta.NmMember = o.NmMember
                        drPeserta.StHadir = o.StHadir
                        drPeserta.StEdit = "0"
                        drPeserta.CreatedBy = _rapat.Notulis
                        drPeserta.CreatedIn = HttpContext.Current.Request.UserHostName
                        drPeserta.CreatedTime = Now
                        dtPeserta.AddDPESERTARow(drPeserta)
                    ElseIf o.Deleted = "1" Then
                        drPeserta = dtPeserta.NewDPESERTARow()
                        drPeserta.IdRap = dr.IdRap
                        drPeserta.NikMember = o.NikOri
                        drPeserta.NmMember = o.NmMember
                        dtPeserta.AddDPESERTARow(drPeserta)
                        drPeserta.AcceptChanges()
                        drPeserta.Delete()
                    Else
                        drPeserta = dtPeserta.NewDPESERTARow()
                        drPeserta.IdRap = dr.IdRap
                        drPeserta.NikMember = o.NikOri
                        drPeserta.StEdit = "0"
                        drPeserta.ModifiedBy = _rapat.Notulis
                        drPeserta.ModifiedIn = HttpContext.Current.Request.UserHostName
                        drPeserta.ModifiedTime = Now
                        dtPeserta.AddDPESERTARow(drPeserta)
                        drPeserta.AcceptChanges()
                        drPeserta.NikMember = o.NikMember
                        drPeserta.NmMember = o.NmMember
                        drPeserta.StHadir = o.StHadir
                    End If
                Next
                Dim drDRapat As DRAPATRow
                Dim dtDRapat As New DRAPATDataTable()
                Dim urutan As Integer = 0
                For Each i As MyIssue In _issue
                    If i.NoMeet = "NEW" Then
                        drDRapat = dtDRapat.NewDRAPATRow()
                        drDRapat.IdRap = dr.IdRap
                        drDRapat.IdIssue = i.NoIssue
                        drDRapat.NmIssue = i.NmIssue.Replace("'", "`")
                        drDRapat.Action = i.Action.Replace("'", "`")
                        drDRapat.NoAction = i.NoRef
                        drDRapat.NikPIC = i.PIC
                        drDRapat.NmPIC = i.Nmpic
                        drDRapat.JnsIssue = i.jenis
                        drDRapat.StIssue = i.status
                        'drDRapat.DueDate = DateTime.ParseExact(i.due, "MM/dd/yyyy", Nothing)
                        If i.due = "" Then
                            drDRapat.SetDueDateNull()
                        Else
                            drDRapat.DueDate = DateTime.ParseExact(i.due, "dd/MM/yyyy", Nothing)
                        End If
                        drDRapat.StEdit = "0"
                        If i.Info = "null" Then
                            drDRapat.SetNikInfoNull()
                        Else
                            drDRapat.NikInfo = i.Info
                        End If

                        If i.NmInfo = "null" Then
                            drDRapat.SetNmInfoNull()
                        Else
                            drDRapat.NmInfo = i.NmInfo
                        End If
                        drDRapat.CreatedBy = _rapat.Notulis
                        drDRapat.CreatedIn = HttpContext.Current.Request.UserHostName
                        drDRapat.CreatedTime = DateTime.Now
                        drDRapat.Deliverable = i.Deliverables.Replace("'", "`")
                        drDRapat.Urutan = urutan
                        dtDRapat.AddDRAPATRow(drDRapat)
                        urutan += 1
                    ElseIf i.Deleted = "1" Then
                        drDRapat = dtDRapat.NewDRAPATRow()
                        drDRapat.IdRap = i.NoMeet
                        drDRapat.IdIssue = i.NoIssue
                        drDRapat.NoAction = i.NoRef
                        dtDRapat.AddDRAPATRow(drDRapat)
                        drDRapat.AcceptChanges()
                        drDRapat.Delete()

                        ' Jika Delete Detail Rapat maka hapus juga Detail TRISSUE
                        DeleteDetailIssue(i.NoMeet, i.NoIssue, i.NoRef)
                    Else
                        drDRapat = dtDRapat.NewDRAPATRow()
                        drDRapat.IdRap = i.NoMeet
                        drDRapat.IdIssue = i.NoIssue
                        drDRapat.NmIssue = i.NmIssue.Replace("'", "`")
                        drDRapat.Action = i.Action.Replace("'", "`")
                        drDRapat.NoAction = i.NoRef
                        drDRapat.NikPIC = i.PIC
                        drDRapat.NmPIC = i.Nmpic
                        drDRapat.JnsIssue = i.jenis
                        drDRapat.StIssue = i.status
                        If i.due = "" Then
                            drDRapat.SetDueDateNull()
                        Else
                            drDRapat.DueDate = DateTime.ParseExact(i.due, "dd/MM/yyyy", Nothing)
                        End If
                        drDRapat.StEdit = "0"
                        If i.Info = "null" Then
                            drDRapat.SetNikInfoNull()
                        Else
                            drDRapat.NikInfo = i.Info
                        End If

                        If i.NmInfo = "null" Then
                            drDRapat.SetNmInfoNull()
                        Else
                            drDRapat.NmInfo = i.NmInfo
                        End If
                        drDRapat.ModifiedBy = _rapat.Notulis
                        drDRapat.ModifiedIn = HttpContext.Current.Request.UserHostName
                        drDRapat.ModifiedTime = DateTime.Now
                        drDRapat.Deliverable = i.Deliverables.Replace("'", "`")
                        drDRapat.Urutan = urutan
                        dtDRapat.AddDRAPATRow(drDRapat)
                        drDRapat.AcceptChanges()
                        drDRapat.SetModified()
                        urutan += 1
                    End If
                Next
                Dim transaction As SqlTransaction = Nothing
                Try
                    Using _HRapatAdapter As New ExcellentTableAdapters.HRAPATTableAdapter
                        transaction = TableAdapterHelper.BeginTransaction(_HRapatAdapter)
                        _HRapatAdapter.Update(dt)
                    End Using
                    Using _DPesertaAdapter As New ExcellentTableAdapters.DPESERTATableAdapter
                        TableAdapterHelper.SetTransaction(_DPesertaAdapter, transaction)
                        _DPesertaAdapter.Update(dtPeserta)
                    End Using
                    Using _DRapatAdapter As New ExcellentTableAdapters.DRAPATTableAdapter
                        TableAdapterHelper.SetTransaction(_DRapatAdapter, transaction)
                        _DRapatAdapter.Update(dtDRapat)
                    End Using

                    transaction.Commit()
                Catch ex As Exception
                    MsgBox(ex.Message.ToString + " " + ex.StackTrace)
                    transaction.Rollback()
                    Throw
                Finally
                    transaction.Dispose()
                End Try
            End If
            Return "success;edit"
        Catch ex As Exception
            Return "failed;" & Replace(ex.Message, "'", "")
        Finally
            If _adapter.Connection.State = ConnectionState.Open Then
                _adapter.Connection.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Public Function GetRapat(ByVal id As String) As MyRapatWrap
        Try
            Dim _adapterH As New ExcellentTableAdapters.HRAPATTableAdapter()
            Dim _adapterD As New ExcellentTableAdapters.DRAPATTableAdapter()
            Dim _adapterP As New ExcellentTableAdapters.DPESERTATableAdapter()
            Dim _dtH As New HRAPATDataTable()
            Dim _dtD As New DRAPATDataTable()
            Dim _dtP As New DPESERTADataTable()

            _adapterH.FillByIdRap(_dtH, id)
            _adapterD.FillByIdRap(_dtD, id)
            _adapterP.FillByIdRap(_dtP, id)

            Dim _myRapatWrap As New MyRapatWrap
            Dim _myRapat As New MyRapat
            Dim _myIssue As MyIssue
            Dim _myPeserta As MyPeserta

            Dim _lmyIssue As New List(Of MyIssue)
            Dim _lmyPeserta As New List(Of MyPeserta)

            _myRapat.IdRap = _dtH(0).IdRap
            _myRapat.NmRap = _dtH(0).NmRap
            _myRapat.TglRap = _dtH(0).TglRap.ToString("dd/MM/yyyy")
            _myRapat.TmpRap = _dtH(0).TmpRap
            _myRapat.Pimpinan = _dtH(0).Pimpinan
            _myRapat.Nmpimpinan = _dtH(0).NmPimpinan
            _myRapat.Notulis = _dtH(0).Notulis
            _myRapat.Nmnotulis = _dtH(0).NmNotulis
            _myRapat.JenisRap = _dtH(0).JenisRap
            If _dtH(0).IsAgendaNull Then
                _myRapat.Agenda = ""
            Else
                _myRapat.Agenda = _dtH(0).Agenda
            End If
            If _dtH(0).IsMailNull Then
                _myRapat.Mail = "0"
            Else
                _myRapat.Mail = _dtH(0).Mail.ToString()
            End If
            If _dtH(0).IsInformasiNull Then
                _myRapat.Informasi = ""
            Else
                _myRapat.Informasi = _dtH(0).Informasi
            End If
            If _dtH(0).IsWkRapNull Then
                _myRapat.WkRap = ""
            Else
                _myRapat.WkRap = _dtH(0).WkRap.ToString("HH:mm")
            End If
            If _dtH(0).IsSdWkRapNull Then
                _myRapat.SdwkRap = ""
            Else
                _myRapat.SdwkRap = _dtH(0).SdWkRap.ToString("HH:mm")
            End If
            If _dtH(0).IsMemberExNull Then
                _myRapat.MemberEx = ""
            Else
                _myRapat.MemberEx = _dtH(0).MemberEx.ToString()
            End If
            If _dtH(0).IsKebijakanNull Then
                _myRapat.Kebijakan = ""
            Else
                _myRapat.Kebijakan = _dtH(0).Kebijakan.ToString()
            End If

            For Each dr As DRAPATRow In _dtD.Rows
                _myIssue = New MyIssue
                _myIssue.IDMeet = dr.IdRap
                _myIssue.NoMeet = dr.IdRap
                _myIssue.NoIssue = dr.IdIssue
                _myIssue.NmIssue = dr.NmIssue
                _myIssue.Action = dr.Action
                _myIssue.NoRef = dr.NoAction
                _myIssue.PIC = dr.NikPIC
                _myIssue.Nmpic = dr.NmPIC
                _myIssue.jenis = dr.JnsIssue
                _myIssue.status = dr.StIssue
                If dr.IsDueDateNull Then
                    _myIssue.due = ""
                Else
                    _myIssue.due = dr.DueDate.ToString("dd/MM/yyyy")
                End If
                _myIssue.Deleted = "0"
                If dr.IsNikInfoNull Then
                    _myIssue.Info = ""
                Else
                    _myIssue.Info = dr.NikInfo
                End If

                If dr.IsNmInfoNull Then
                    _myIssue.NmInfo = ""
                Else
                    _myIssue.NmInfo = dr.NmInfo
                End If

                _myIssue.Deliverables = dr.Deliverable
                _lmyIssue.Add(_myIssue)
            Next

            For Each dr As DPESERTARow In _dtP.Rows
                _myPeserta = New MyPeserta()
                _myPeserta.IdRap = dr.IdRap
                _myPeserta.NikOri = dr.NikMember
                _myPeserta.NikMember = dr.NikMember
                _myPeserta.NmMember = dr.NmMember
                _myPeserta.StHadir = dr.StHadir
                _myPeserta.Deleted = "0"
                _lmyPeserta.Add(_myPeserta)
            Next
            _myRapatWrap.MyRapat1 = _myRapat
            _myRapatWrap.MyPeserta = _lmyPeserta
            _myRapatWrap.MyIssue = _lmyIssue
            Return _myRapatWrap
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Function GetJobSite(ByVal _iduser As String) As String
        Dim slokasi As String = ""

        ''KODE JOBSITE TIDAK AMBIL LAGI DARI HRIS KARENA LEVEL SCTN HEAD UP
        ''KODE JOBSITE DIANGGAP HO - MAKA AMBIL DARI TABEL MUSER

        'KODE JOBSITE AMBIL LAGI DARI HRIS - BASED RAPAT DI RUANG 3B TGL 1/6/2010

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            'JIKA LEVEL E-Up MAKA AMBIL KODE LOKASI DARI H_A101 HRIS
            'SEDANGKAN D-Down AMBIL KODE JOBSITE DARI H_A101 HRIS

            Dim squery As String = ""
            'squery = "SELECT Nik, KdLokasi = case KdLevel when 5 Then KdLokasi when 6 Then KdLokasi When 7 Then KdLokasi else KdSite end "
            'squery = squery + "FROM H_A101 WHERE Nik = '" + _iduser + "'"

            'squery = "SELECT SITE as KdLokasi FROM MAPSITESAP a inner join OPENROWSET('SQLNCLI','SERVER=172.16.0.7\hris;UID=IICS;PWD=$IICS-123-ONLINE$', 'SELECT WERKS,BTRTL from BERP_CENTRAL.dbo.H_A101_SAP where pernr=''" & _iduser & "''') x on a.WERKS=x.WERKS and a.BTRTL=x.BTRTL"
            squery = "SELECT SITE as KdLokasi FROM MAPSITESAP a inner join OPENROWSET('SQLNCLI','SERVER=20.110.1.5;UID=dev;PWD=hrishris', 'SELECT kdsite  from JRN_TEST.dbo.H_A101 where nik=''" & _iduser & "''') x on a.SITE=x.KDSITE  "

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            If sdr.HasRows Then
                While sdr.Read()
                    slokasi = IIf(IsDBNull(sdr("KdLokasi")), "NFO", sdr("KdLokasi"))
                End While
            Else
                slokasi = "NFO"
            End If

            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return slokasi
    End Function

    Sub DeleteDetailIssue(ByVal _idrap As String, ByVal _idissue As String, ByVal _idacton As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString)
        Try
            conn.Open()
            Dim squery As String = ""
            squery = "DELETE FROM TRISSUE WHERE IdRap = '" + _idrap + "' AND IdIssue = '" + _idissue + "' AND IdAction = '" + _idacton + "'"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            scom.ExecuteNonQuery()

            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try
    End Sub

    Sub RenameFileOnServer(ByVal TheServerURL As String, ByVal OldFile As String, ByVal NewName As String, ByVal username As String, ByVal password As String)
        Dim MyFtpWebRequest As System.Net.FtpWebRequest = CType(System.Net.FtpWebRequest.Create(TheServerURL & OldFile), FtpWebRequest)
        MyFtpWebRequest.Credentials = New System.Net.NetworkCredential(username, password)
        MyFtpWebRequest.Method = System.Net.WebRequestMethods.Ftp.Rename
        MyFtpWebRequest.RenameTo() = NewName
        Dim MyResponse As System.Net.FtpWebResponse

        Try
            MyResponse = CType(MyFtpWebRequest.GetResponse, FtpWebResponse)

            Dim MyStatusStr As String = MyResponse.StatusDescription
            Dim MyStatusCode As System.Net.FtpStatusCode = MyResponse.StatusCode

            If MyStatusCode <> Net.FtpStatusCode.FileActionOK Then
                'MessageBox("*** Rename " & OldFile & " to " & NewName & " failed.  Returned status = " & MyStatusStr)
                'label2.Text = "*** Rename " & OldFile & " to " & NewName & " failed.  Returned status = " & MyStatusStr
            Else
                'MessageBox("Rename " & OldFile & " to " & NewName & " ok at " & Now.ToString)
                'label2.Text = "Rename " & OldFile & " to " & NewName & " ok at " & Now.ToString
            End If
        Catch ex As Exception
            'MessageBox("*** Rename " & OldFile & " to " & NewName & " failed due to the following error: " & ex.Message)

        End Try
    End Sub

    Function GetFunction(ByVal _kode As String) As String
        Dim sfungsi As String = _kode

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT abbr FROM mappdep WHERE NmDepar = '" + _kode + "'"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            If sdr.HasRows Then
                While sdr.Read()
                    sfungsi = IIf(IsDBNull(sdr("abbr")), "", sdr("abbr"))
                End While
            Else
                sfungsi = "UNK"
            End If
            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return sfungsi
    End Function

    Function Getdept(ByVal _kode2 As String) As String
        Dim sfungsi As String = _kode2

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "select departemen from muser where iduser = '" + _kode2 + "'"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            If sdr.HasRows Then
                While sdr.Read()
                    sfungsi = IIf(IsDBNull(sdr("departemen")), "", sdr("departemen"))
                End While
            Else
                sfungsi = "NFO"
            End If
            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return sfungsi
    End Function

    Function GetCodeFunction(ByVal _iduser As String) As String
        Dim sfungsi As String = ""

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
        Try
            If _iduser.IndexOf("10000306") > -1 Or _iduser.IndexOf("10002040") > -1 Or _iduser.IndexOf("10006574") > -1 Or _iduser.IndexOf("10010546") > -1 Or _iduser.IndexOf("10011006") > -1 Or _iduser.IndexOf("10011038") > -1 Then
                sfungsi = "Human Resource & General Affair"
            ElseIf _iduser.IndexOf("10002373") > -1 Or _iduser.IndexOf("10002609") > -1 Or _iduser.IndexOf("10002779") > -1 Or _iduser.IndexOf("10009167") > -1 Or _iduser.IndexOf("10010394") > -1 Or _iduser.IndexOf("10008053") > -1 Or _iduser.IndexOf("10010983") > -1 Or _iduser.IndexOf("10000973") > -1 Then
                sfungsi = "Operation"
            ElseIf _iduser.IndexOf("10007237") > -1 Or _iduser.IndexOf("10011034") > -1 Then
                sfungsi = "Plant"
            ElseIf _iduser.IndexOf("10007792") > -1 Then
                sfungsi = "Finance & Accounting"
            ElseIf _iduser.IndexOf("10010983") > -1 Then
                sfungsi = "None Function"
            ElseIf _iduser.IndexOf("10010982") > -1 Then
                sfungsi = "Engineering"
            Else
                conn.Open()

                Dim squery As String = ""
                squery = "SELECT KdDepar as ZFUN FROM H_A101 WHERE Nik = '" + _iduser + "'"
                'squery = "SELECT ZFUN FROM H_A101_SAP WHERE PERNR = '" + _iduser + "' OR PNALT='" + _iduser + "'"

                Dim scom As SqlCommand = New SqlCommand(squery, conn)
                Dim sdr As SqlDataReader = scom.ExecuteReader()

                While sdr.Read()
                    sfungsi = IIf(IsDBNull(sdr("ZFUN")), "", sdr("ZFUN"))
                End While

                conn.Close()
            End If
        Catch eks As Exception
            conn.Close()
        End Try

        Return sfungsi
    End Function
End Class

Public Class MyIssue
    Private _idMeet As String
    Private _noMeet As String
    Private _noIssue As String
    Private _nmIssue As String
    Private _noRef As String
    Private _action As String
    Private _pic As String
    Private _nmpic As String
    Private _info As String
    Private _nmInfo As String
    Private _due As String
    Private _jenis As String
    Private _status As String
    Private _deleted As String
    Private _deliverables As String

    Public Property IDMeet() As String
        Get
            Return _idMeet
        End Get
        Set(ByVal value As String)
            _idMeet = value
        End Set
    End Property
    Public Property NoMeet() As String
        Get
            Return _noMeet
        End Get
        Set(ByVal value As String)
            _noMeet = value
        End Set
    End Property
    Public Property NoIssue() As String
        Get
            Return _noIssue
        End Get
        Set(ByVal value As String)
            _noIssue = value
        End Set
    End Property
    Public Property NmIssue() As String
        Get
            Return _nmIssue
        End Get
        Set(ByVal value As String)
            _nmIssue = value
        End Set
    End Property
    Public Property NoRef() As String
        Get
            Return _noRef
        End Get
        Set(ByVal value As String)
            _noRef = value
        End Set
    End Property
    Public Property Action() As String
        Get
            Return _action
        End Get
        Set(ByVal value As String)
            _action = value
        End Set
    End Property
    Public Property PIC() As String
        Get
            Return _pic
        End Get
        Set(ByVal value As String)
            _pic = value
        End Set
    End Property
    Public Property Nmpic() As String
        Get
            Return _nmpic
        End Get
        Set(ByVal value As String)
            _nmpic = value
        End Set
    End Property
    Public Property due() As String
        Get
            Return _due
        End Get
        Set(ByVal value As String)
            _due = value
        End Set
    End Property
    Public Property jenis() As String
        Get
            Return _jenis
        End Get
        Set(ByVal value As String)
            _jenis = value
        End Set
    End Property
    Public Property status() As String
        Get
            Return _status
        End Get
        Set(ByVal value As String)
            _status = value
        End Set
    End Property
    Public Property Deleted() As String
        Get
            Return _deleted
        End Get
        Set(ByVal value As String)
            _deleted = value
        End Set
    End Property
    Public Property Info() As String
        Get
            Return _info
        End Get
        Set(ByVal value As String)
            _info = value
        End Set
    End Property
    Public Property NmInfo() As String
        Get
            Return _nmInfo
        End Get
        Set(ByVal value As String)
            _nmInfo = value
        End Set
    End Property
    Public Property Deliverables() As String
        Get
            Return _deliverables
        End Get
        Set(ByVal value As String)
            _deliverables = value
        End Set
    End Property
End Class

Public Class MyRapat
    Private _idRap As String
    Private _nmRap As String
    Private _tmpRap As String
    Private _wkRap As String
    Private _sdwkRap As String
    Private _tglRap As String
    Private _agenda As String
    Private _pimpinan As String
    Private _nmpimpinan As String
    Private _notulis As String
    Private _nmnotulis As String
    Private _jenisRap As String
    Private _informasi As String
    Private _mail As String
    Private _member As String
    Private _kebijakan As String

    Public Property IdRap() As String
        Get
            Return _idRap
        End Get
        Set(ByVal value As String)
            _idRap = value
        End Set
    End Property
    Public Property NmRap() As String
        Get
            Return _nmRap
        End Get
        Set(ByVal value As String)
            _nmRap = value
        End Set
    End Property
    Public Property TmpRap() As String
        Get
            Return _tmpRap
        End Get
        Set(ByVal value As String)
            _tmpRap = value
        End Set
    End Property
    Public Property WkRap() As String
        Get
            Return _wkRap
        End Get
        Set(ByVal value As String)
            _wkRap = value
        End Set
    End Property
    Public Property SdwkRap() As String
        Get
            Return _sdwkRap
        End Get
        Set(ByVal value As String)
            _sdwkRap = value
        End Set
    End Property
    Public Property TglRap() As String
        Get
            Return _tglRap
        End Get
        Set(ByVal value As String)
            _tglRap = value
        End Set
    End Property
    Public Property Agenda() As String
        Get
            Return _agenda
        End Get
        Set(ByVal value As String)
            _agenda = value
        End Set
    End Property
    Public Property Pimpinan() As String
        Get
            Return _pimpinan
        End Get
        Set(ByVal value As String)
            _pimpinan = value
        End Set
    End Property
    Public Property Nmpimpinan() As String
        Get
            Return _nmpimpinan
        End Get
        Set(ByVal value As String)
            _nmpimpinan = value
        End Set
    End Property
    Public Property Notulis() As String
        Get
            Return _notulis
        End Get
        Set(ByVal value As String)
            _notulis = value
        End Set
    End Property
    Public Property Nmnotulis() As String
        Get
            Return _nmnotulis
        End Get
        Set(ByVal value As String)
            _nmnotulis = value
        End Set
    End Property
    Public Property JenisRap() As String
        Get
            Return _jenisRap
        End Get
        Set(ByVal value As String)
            _jenisRap = value
        End Set
    End Property
    Public Property Informasi() As String
        Get
            Return _informasi
        End Get
        Set(ByVal value As String)
            _informasi = value
        End Set
    End Property
    Public Property Mail() As String
        Get
            Return _mail
        End Get
        Set(ByVal value As String)
            _mail = value
        End Set
    End Property
    Public Property MemberEx() As String
        Get
            Return _member
        End Get
        Set(ByVal value As String)
            _member = value
        End Set
    End Property
    Public Property Kebijakan() As String
        Get
            Return _kebijakan
        End Get
        Set(ByVal value As String)
            _kebijakan = value
        End Set
    End Property
End Class

Public Class MyPeserta
    Private _idRap As String
    Private _nikOri As String
    Private _nikMember As String
    Private _nmMember As String
    Private _stHadir As String
    Private _deleted As String

    Public Property IdRap() As String
        Get
            Return _idRap
        End Get
        Set(ByVal value As String)
            _idRap = value
        End Set
    End Property
    Public Property NikOri() As String
        Get
            Return _nikOri
        End Get
        Set(ByVal value As String)
            _nikOri = value
        End Set
    End Property
    Public Property NikMember() As String
        Get
            Return _nikMember
        End Get
        Set(ByVal value As String)
            _nikMember = value
        End Set
    End Property
    Public Property NmMember() As String
        Get
            Return _nmMember
        End Get
        Set(ByVal value As String)
            _nmMember = value
        End Set
    End Property
    Public Property StHadir() As String
        Get
            Return _stHadir
        End Get
        Set(ByVal value As String)
            _stHadir = value
        End Set
    End Property
    Public Property Deleted() As String
        Get
            Return _deleted
        End Get
        Set(ByVal value As String)
            _deleted = value
        End Set
    End Property
End Class

Public Class MyRapatWrap
    Private _MyRapat As MyRapat
    Private _MyPeserta As List(Of MyPeserta)
    Private _MyIssue As List(Of MyIssue)

    Public Property MyRapat1() As MyRapat
        Get
            Return _MyRapat
        End Get
        Set(ByVal value As MyRapat)
            Me._MyRapat = value
        End Set
    End Property
    Public Property MyPeserta() As List(Of MyPeserta)
        Get
            Return _MyPeserta
        End Get
        Set(ByVal value As List(Of MyPeserta))
            _MyPeserta = value
        End Set
    End Property
    Public Property MyIssue() As List(Of MyIssue)
        Get
            Return _MyIssue
        End Get
        Set(ByVal value As List(Of MyIssue))
            _MyIssue = value
        End Set
    End Property

    'Function uploadFile(ByVal FTPAddress As String, ByVal filePath As String, ByVal username As String, ByVal password As String, ByVal fullpath As String)
    '    Try
    '        Dim request As FtpWebRequest = DirectCast(FtpWebRequest.Create(FTPAddress & "/" & Path.GetFileName(filePath)), FtpWebRequest)

    '        request.Method = WebRequestMethods.Ftp.UploadFile
    '        request.Credentials = New NetworkCredential(username, password)
    '        request.UsePassive = True
    '        request.UseBinary = True
    '        request.KeepAlive = False

    '        'Load the file
    '        'Dim stream As FileStream = File.OpenRead(filePath)
    '        Dim fStream As Stream = FileUpload1.PostedFile.InputStream
    '        'MsgBox("test " + File.OpenRead(fullpath).ToString())
    '        Dim buffer As Byte() = New Byte(CInt(fStream.Length - 1)) {}

    '        fStream.Read(buffer, 0, buffer.Length)
    '        fStream.Close()

    '        'Upload file
    '        Dim reqStream As Stream = request.GetRequestStream()
    '        reqStream.Write(buffer, 0, buffer.Length)
    '        reqStream.Close()

    '        'MsgBox("Uploaded Successfully", MsgBoxStyle.Information)
    '    Catch e As Exception
    '        label1.Text = "ERROR: " & e.Message.ToString()
    '        'MsgBox("Failed to upload.Please check the ftp settings" + "Reason: " + e.Message, MsgBoxStyle.Critical)
    '        MessageBox(e.Message.ToString())
    '    End Try
    'End Function
End Class




