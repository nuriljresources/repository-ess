﻿Public Class PermitModel
    Public TransID As String
    Public Site As String
    Public EmployeeNIK As String
    Public EmployeeNIKSITE As String
    Public EmployeeName As String
    Public TypePermit As String
    Public DatePermit As Date
    Public TimePermit As TimeSpan
    Public Remarks As String
    Public StatusEdit As Integer
End Class
