﻿Imports System.Data.SqlClient

Partial Public Class ppa_entry
    Inherits System.Web.UI.Page
    Public msg As String
    Public status As String
    Public mreport As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("localdev").ConnectionString)
        Dim sqlconn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim syear As String
        Dim icount As Integer

        If Session("ppa_otorisasi") = "" Then
            Response.Redirect("login_ppa.aspx")
        End If

        If Not IsPostBack Then
            'Session("ppa_niksite") = "0001576"
            link_task.Enabled = False
            syear = Today.Year.ToString
            Try
                'generate pertanyaan
                Dim dtb As DataTable = New DataTable()
                Dim strsql As String = ""

                If Session("ppa_level") = "1L" Or Session("ppa_level") = "1M" Or Session("ppa_level") = "1H" Then
                    strsql = "select top 6 ms_id, header1, header2, det1, det2, det3, det4, det5,eng_header1, eng_header2, eng_det1, eng_det2, eng_det3, eng_det4, eng_det5 from ms_questions order by ms_id"
                ElseIf Session("ppa_level").ToString.Contains("4") Then
                    strsql = "select top 10 ms_id, header1, header2, det1, det2, det3, det4, det5,eng_header1, eng_header2, eng_det1, eng_det2, eng_det3, eng_det4, eng_det5 from ms_questions order by ms_id"
                ElseIf Session("ppa_level").ToString.Contains("5") Then
                    Response.Redirect("ppa_logout.aspx")
                Else
                    strsql = "select top 9 ms_id, header1, header2, det1, det2, det3, det4, det5,eng_header1, eng_header2, eng_det1, eng_det2, eng_det3, eng_det4, eng_det5 from ms_questions order by ms_id"
                End If

                Dim sda As SqlDataAdapter = New SqlDataAdapter(strsql, sqlConn)
                sda.Fill(dtb)

                Repeater1.DataSource = dtb
                Repeater1.DataBind()

                Dim dtb_employee As DataTable = New DataTable
                Dim strsql2 As String = "SELECT  Nik, niksite, Nama,KdSite, kddepar, (select nmdepar from h_a130 where kddepar= H_a101.kddepar) as kddepar,(select nmdepar from h_a130 where kddepar= H_a101.kddepar) + ' [' + (select nmsec from H_A209 ,H_A150 where H_A209.kdsec = H_A150.kdsec and H_A150.kdjabat = H_A101.kdjabatan )  + ']' as NmDepar, (select nmjabat from H_A209 ,H_A150 where H_A209.kdsec = H_A150.kdsec and H_A150.kdjabat = H_A101.kdjabatan ) as nmjabat, (SELECT kdjabat from H_A209 ,H_A150 where H_A209.kdsec = H_A150.kdsec and H_A150.kdjabat = H_A101.kdjabatan) as kdjabat, (select kddepar from h_a130 where kddepar= H_a101.kddepar) as kddepart, (select top 1 nik from H_A101 a where KdJabatan = (select kdjabatDirect from H_A150 where kdjabat = H_A101.KdJabatan) and StEdit <> '2' and Active = '1') as nikspv, (select top 1 nama from H_A101 a where KdJabatan = (select kdjabatDirect from H_A150 where kdjabat = H_A101.KdJabatan) and StEdit <> '2' and Active = '1') as namaspv FROM H_A101 WHERE active = '1' AND stedit <> '2' AND nik = '" + Session("ppa_niksite") + "'"
                Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strsql2, sqlconn2)
                sda2.Fill(dtb_employee)

                If dtb_employee.Rows.Count > 0 Then
                    txtnama.Text = dtb_employee.Rows(0)!nama.ToString
                    txtnik.Text = dtb_employee.Rows(0)!niksite.ToString
                    txtjabat.Text = dtb_employee.Rows(0)!nmjabat.ToString
                    txtdepar.Text = dtb_employee.Rows(0)!nmdepar.ToString
                    txtspv.Text = dtb_employee.Rows(0)!namaspv.ToString
                    hnik.Value = dtb_employee.Rows(0)!nik.ToString
                    hjabat.Value = dtb_employee.Rows(0)!kdjabat.ToString
                    hdepar.Value = dtb_employee.Rows(0)!kddepar.ToString
                    hnikspv.Value = dtb_employee.Rows(0)!nikspv.ToString
                End If

                txtnama.Attributes.Add("readonly", "readonly")
                txtnik.Attributes.Add("readonly", "readonly")
                txtjabat.Attributes.Add("readonly", "readonly")
                txtdepar.Attributes.Add("readonly", "readonly")
                txtspv.Attributes.Add("readonly", "readonly")

                sqlConn.Open()
                Dim dtbperiod As DataTable = New DataTable
                Dim qrycheckperiod As String = "select nobuk,periode,(select nama from jrn_test..H_A101 where nik = tr_ppa_h.nik_spv) as spv, nik_spv from tr_ppa_h where periode = '" + syear + "' and nik = '" + Session("ppa_niksite") + "' and stedit <> 2 and [status] = 1"
                Dim sdaperiod As SqlDataAdapter = New SqlDataAdapter(qrycheckperiod, sqlConn)
                sdaperiod.Fill(dtbperiod)

                If dtbperiod.Rows.Count > 0 Then
                    txtnobuk.Value = dtbperiod.Rows(0)!nobuk.ToString
                    txtspv.Text = dtbperiod.Rows(0)!spv.ToString
                    hnikspv.Value = dtbperiod.Rows(0)!nik_spv.ToString

                    Dim dtbopenppa As DataTable = New DataTable
                    Dim qryopenppa As String = "select nobuk, ms_id, score_isi, komentar_isi from tr_ppa_d where nobuk = '" + dtbperiod.Rows(0)!nobuk + "'"
                    Dim sdaopenppa As SqlDataAdapter = New SqlDataAdapter(qryopenppa, sqlConn)
                    sdaopenppa.Fill(dtbopenppa)

                    If dtbopenppa.Rows.Count > 0 Then
                        'For icount = 0 To dtbopenppa.Rows.Count - 1
                        For Each i As RepeaterItem In Repeater1.Items
                            Dim radio1 As RadioButton = DirectCast(i.FindControl("a11"), RadioButton)
                            Dim radio2 As RadioButton = DirectCast(i.FindControl("a12"), RadioButton)
                            Dim radio3 As RadioButton = DirectCast(i.FindControl("a13"), RadioButton)
                            Dim radio4 As RadioButton = DirectCast(i.FindControl("a14"), RadioButton)
                            Dim radio5 As RadioButton = DirectCast(i.FindControl("a15"), RadioButton)
                            Dim hid As HiddenField = DirectCast(i.FindControl("hid"), HiddenField)
                            Dim txtid As Label = DirectCast(i.FindControl("txtid"), Label)

                            If dtbopenppa.Rows(i.ItemIndex)!score_isi = 1 Then
                                radio1.Checked = True
                            End If
                            If dtbopenppa.Rows(i.ItemIndex)!score_isi = 2 Then
                                radio2.Checked = True
                            End If
                            If dtbopenppa.Rows(i.ItemIndex)!score_isi = 3 Then
                                radio3.Checked = True
                            End If
                            If dtbopenppa.Rows(i.ItemIndex)!score_isi = 4 Then
                                radio4.Checked = True
                            End If
                            If dtbopenppa.Rows(i.ItemIndex)!score_isi = 5 Then
                                radio5.Checked = True
                            End If
                        Next
                        'Next
                        txtstats.Value = "Edit"
                        Session("ppa_nobuk") = dtbperiod.Rows(0)!nobuk
                        link_task.Enabled = True
                    End If
                End If

            Catch ex As Exception

            Finally
                sqlConn.Close()
            End Try
            'End If
            'If dtb.Rows.Count > 0 Then
            '    Dim i, x As Integer
            '    x = 3
            '    For i = 1 To dtb.Rows.Count - 1

            '    Next
        End If

        Dim qrychkadmin As String = "Select nik, nama, admintype from admin_ppa where nik = '" + Session("ppa_niksite") + "'"
        Dim dtbchkadmin As DataTable = New DataTable

        Dim sdachkadmin As SqlDataAdapter = New SqlDataAdapter(qrychkadmin, sqlConn)
        sdachkadmin.Fill(dtbchkadmin)

        If dtbchkadmin.Rows.Count > 0 Then
            mreport = "<label><a href='ppa_entry_list.aspx'>Report</a></label>"
        End If
    End Sub

    Public Sub innerFunction(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Dim i As Integer

        Dim myradio1 As RadioButton = DirectCast(e.Item.FindControl("a11"), RadioButton)
        Dim myradio2 As RadioButton = DirectCast(e.Item.FindControl("a12"), RadioButton)
        Dim myradio3 As RadioButton = DirectCast(e.Item.FindControl("a13"), RadioButton)
        Dim myradio4 As RadioButton = DirectCast(e.Item.FindControl("a14"), RadioButton)
        Dim myradio5 As RadioButton = DirectCast(e.Item.FindControl("a15"), RadioButton)

        Try
            If myradio1.Checked = True Then no.Text = "1"
            If myradio2.Checked = True Then no.Text = "2"
            If myradio3.Checked = True Then no.Text = "3"
            If myradio4.Checked = True Then no.Text = "4"
            If myradio5.Checked = True Then no.Text = "5"
        Catch ex As Exception

        End Try

    End Sub

    Public Sub LinkButton1_Command(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim i As Integer

        Dim myradio1 As RadioButton = DirectCast(Repeater1.FindControl("a11"), RadioButton)
        Dim myradio2 As RadioButton = DirectCast(Repeater1.FindControl("a12"), RadioButton)
        Dim myradio3 As RadioButton = DirectCast(Repeater1.FindControl("a13"), RadioButton)
        Dim myradio4 As RadioButton = DirectCast(Repeater1.FindControl("a14"), RadioButton)
        Dim myradio5 As RadioButton = DirectCast(Repeater1.FindControl("a15"), RadioButton)

        Try
            If myradio1.Checked = True Then no.Text = "1"
            If myradio2.Checked = True Then no.Text = "2"
            If myradio3.Checked = True Then no.Text = "3"
            If myradio4.Checked = True Then no.Text = "4"
            If myradio5.Checked = True Then no.Text = "5"
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub cmdSubmit_Click(ByVal sender As Object, ByVal e As EventArgs)
        'baca output dari daftar pertanyaan
        no.Text = ""
        Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("localdev").ConnectionString)
        Dim sqlsave As String
        Dim cmd As SqlCommand
        Dim sqlsave_d As String
        Dim sqlsave_h As String
        Dim cmd_h As SqlCommand
        Dim cmd_d As SqlCommand
        Dim newnobuk As String
        Dim dtmnow As String
        Dim dtynow As String
        Dim stripno As String
        Dim checkscore As Boolean = True
        Dim score As String = "0"

        If Session("ppa_otorisasi") = "" Then
            Response.Redirect("login_ppa.aspx")
        End If

        For Each i As RepeaterItem In Repeater1.Items
            Dim radio1 As RadioButton = DirectCast(i.FindControl("a11"), RadioButton)
            Dim radio2 As RadioButton = DirectCast(i.FindControl("a12"), RadioButton)
            Dim radio3 As RadioButton = DirectCast(i.FindControl("a13"), RadioButton)
            Dim radio4 As RadioButton = DirectCast(i.FindControl("a14"), RadioButton)
            Dim radio5 As RadioButton = DirectCast(i.FindControl("a15"), RadioButton)
            Dim hid As HiddenField = DirectCast(i.FindControl("hid"), HiddenField)
            Dim txtid As Label = DirectCast(i.FindControl("txtid"), Label)

            If txtid IsNot Nothing Then
                If radio1 IsNot Nothing And radio2 IsNot Nothing And radio3 IsNot Nothing And radio4 IsNot Nothing And radio5 IsNot Nothing Then
                    If radio1.Checked <> True And radio2.Checked <> True And radio3.Checked <> True And radio4.Checked <> True And radio5.Checked <> True Then
                        checkscore = False
                    End If

                    If checkscore = False Then
                        msg = "<script type ='text/javascript' > alert('Please Entry Question number " + txtid.Text.ToString + "') </script>"
                        Return
                    End If
                End If

            End If
        Next

        sqlcon.Open()
        If txtstats.Value = "Edit" Then
            Try
                For Each i As RepeaterItem In Repeater1.Items

                    Dim radio1 As RadioButton = DirectCast(i.FindControl("a11"), RadioButton)
                    Dim radio2 As RadioButton = DirectCast(i.FindControl("a12"), RadioButton)
                    Dim radio3 As RadioButton = DirectCast(i.FindControl("a13"), RadioButton)
                    Dim radio4 As RadioButton = DirectCast(i.FindControl("a14"), RadioButton)
                    Dim radio5 As RadioButton = DirectCast(i.FindControl("a15"), RadioButton)
                    Dim hid As HiddenField = DirectCast(i.FindControl("hid"), HiddenField)
                    Dim txtid As Label = DirectCast(i.FindControl("txtid"), Label)

                    If txtid IsNot Nothing Then
                        If radio1 IsNot Nothing Then
                            If radio1.Checked = True Then
                                'no.Text += "1" + ", "
                                score = "1"
                            End If
                        End If

                        If radio2 IsNot Nothing Then
                            If radio2.Checked = True Then
                                'no.Text += "2" + ", "
                                score = "2"
                            End If
                        End If

                        If radio3 IsNot Nothing Then
                            If radio3.Checked = True Then
                                'no.Text += "3" + ", "
                                score = "3"
                            End If
                        End If

                        If radio4 IsNot Nothing Then
                            If radio4.Checked = True Then
                                'no.Text += "4" + ", "
                                score = "4"
                            End If
                        End If

                        If radio5 IsNot Nothing Then
                            If radio5.Checked = True Then
                                'no.Text += "5" + ", "
                                score = "5"
                            End If
                        End If

                        sqlsave_h = "update tr_ppa_h set nik_spv = '" + hnikspv.Value + "' where nobuk = '" + txtnobuk.Value + "'"
                        cmd_h = New SqlCommand(sqlsave_h, sqlcon)
                        cmd_h.ExecuteScalar()

                        sqlsave_d = "update tr_ppa_d set score_isi = " + score + " where nobuk = '" + txtnobuk.Value + "' and ms_id = '" + txtid.Text + "'"
                        cmd_d = New SqlCommand(sqlsave_d, sqlcon)
                        cmd_d.ExecuteScalar()
                    End If
                Next
            Catch ex As Exception

            Finally
                sqlcon.Close()
            End Try
        Else
            Try
                dtmnow = DateTime.Now.ToString("MM")
                dtynow = Date.Now.Year.ToString
                stripno = dtynow + dtmnow + "/" + "PPA" + "/"

                Dim strppa_h As String = "select nobuk from tr_ppa_h where nobuk like '%" + stripno + "%'"

                Dim dtbppa_h As DataTable = New DataTable
                Dim sdappa_h As SqlDataAdapter = New SqlDataAdapter(strppa_h, sqlcon)

                sdappa_h.Fill(dtbppa_h)

                If dtbppa_h.Rows.Count = 0 Then
                    newnobuk = stripno + "000001"
                ElseIf dtbppa_h.Rows.Count > 0 Then
                    I = 0
                    I = dtbppa_h.Rows.Count + 1
                    If dtbppa_h.Rows.Count > 0 And dtbppa_h.Rows.Count < 9 Then
                        newnobuk = stripno + "00000" + I.ToString
                    ElseIf dtbppa_h.Rows.Count >= 9 And dtbppa_h.Rows.Count < 99 Then
                        newnobuk = stripno + "0000" + I.ToString
                    ElseIf dtbppa_h.Rows.Count >= 99 And dtbppa_h.Rows.Count < 999 Then
                        newnobuk = stripno + "000" + I.ToString
                    ElseIf dtbppa_h.Rows.Count >= 999 And dtbppa_h.Rows.Count < 9999 Then
                        newnobuk = stripno + "00" + I.ToString
                    ElseIf dtbppa_h.Rows.Count >= 9999 And dtbppa_h.Rows.Count < 99999 Then
                        newnobuk = stripno + "0" + I.ToString
                    ElseIf dtbppa_h.Rows.Count >= 99999 And dtbppa_h.Rows.Count < 999999 Then
                        newnobuk = stripno + I.ToString
                    ElseIf dtbppa_h.Rows.Count >= 999999 Then
                        newnobuk = "Error on generate nobuk"
                    End If
                End If

                sqlsave = "insert into tr_ppa_h (nobuk, nik, nama, kdjabatan, kddept, kdsite, periode, stedit, [status], nik_spv, createddate) values ('" + newnobuk + "','" + hnik.Value + "', '" + txtnama.Text + "', '" + hjabat.Value + "', '" + hdepar.Value + "', '', '" + Today.Year.ToString + "', '0', '1', '" + hnikspv.Value + "', '" + Date.Now.ToString + "' )"
                cmd = New SqlCommand(sqlsave, sqlcon)
                cmd.ExecuteScalar()

                For Each i As RepeaterItem In Repeater1.Items

                    Dim radio1 As RadioButton = DirectCast(i.FindControl("a11"), RadioButton)
                    Dim radio2 As RadioButton = DirectCast(i.FindControl("a12"), RadioButton)
                    Dim radio3 As RadioButton = DirectCast(i.FindControl("a13"), RadioButton)
                    Dim radio4 As RadioButton = DirectCast(i.FindControl("a14"), RadioButton)
                    Dim radio5 As RadioButton = DirectCast(i.FindControl("a15"), RadioButton)
                    Dim hid As HiddenField = DirectCast(i.FindControl("hid"), HiddenField)
                    Dim txtid As Label = DirectCast(i.FindControl("txtid"), Label)

                    If txtid IsNot Nothing Then
                        If radio1 IsNot Nothing Then
                            If radio1.Checked = True Then
                                'no.Text += "1" + ", "
                                score = "1"
                            End If
                        End If

                        If radio2 IsNot Nothing Then
                            If radio2.Checked = True Then
                                'no.Text += "2" + ", "
                                score = "2"
                            End If
                        End If

                        If radio3 IsNot Nothing Then
                            If radio3.Checked = True Then
                                'no.Text += "3" + ", "
                                score = "3"
                            End If
                        End If

                        If radio4 IsNot Nothing Then
                            If radio4.Checked = True Then
                                'no.Text += "4" + ", "
                                score = "4"
                            End If
                        End If

                        If radio5 IsNot Nothing Then
                            If radio5.Checked = True Then
                                'no.Text += "5" + ", "
                                score = "5"
                            End If
                        End If

                        sqlsave_d = "insert into tr_ppa_d (nobuk, ms_id, score_isi, komentar_isi) values ('" + newnobuk + "', " + txtid.Text.ToString + ", " + score + ", '')"
                        cmd_d = New SqlCommand(sqlsave_d, sqlcon)
                        cmd_d.ExecuteScalar()
                    End If
                Next

                Session("ppa_nobuk") = newnobuk
                Response.Redirect("ppa_task_entry.aspx?nbo=" + Session("ppa_nobuk").ToString() + "&nsie=" + Session("ppa_niksite").ToString() + "")
            Catch ex As Exception

            Finally
                sqlcon.Close()
            End Try
        End If
    End Sub

    Private Sub link_task_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles link_task.Click
        Response.Redirect("ppa_task_entry.aspx?nbo=" + Session("ppa_nobuk").ToString() + "&nsie=" + Session("ppa_niksite").ToString() + "")
    End Sub

    Private Sub cmdprint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdprint.Click
        Response.Redirect("print.aspx")
    End Sub

    Protected Sub imbtnprev_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbtnprev.Click
        Response.Redirect("print.aspx")
    End Sub
End Class