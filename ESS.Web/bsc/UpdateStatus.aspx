﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UpdateStatus.aspx.vb"
   Inherits="EXCELLENT.UpdateStatus" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title></title>

   <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>

   <script src="../Scripts/json2.js" type="text/javascript"></script>

   <script src="../Scripts/json2.js" type="text/javascript"></script>

   <script src="../Scripts/calendar_us.js" type="text/javascript"></script>

   <link rel="stylesheet" href="../css/calendar-c.css" />

   <script type="text/javascript">
      var obj = window.dialogArguments;


      function update() {
         alert(obj.id);
      }

      function ReadPassedData() {

         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/RetrieveStatus",
            data: "{'id':" + JSON.stringify(obj.id) + "}",
            dataType: "json",
            success: function(res) {
               if (res.d == "1" || res.d == "2") {
                  for (var i = 0; i < 3; i++) {
                     if (i != res.d) {
                        var opt = document.createElement("option");
                        document.getElementById("status").options.add(opt);
                        switch (i) {
                           case 0:
                              opt.text = 'IDLE';
                              break;
                           case 1:
                              opt.text = 'ON PROGRESS';
                              break;
                           case 2:
                              opt.text = 'DONE';
                              break;
                        }
                        opt.value = i;
                     }
                  }
               } else {
                  var opt = document.createElement("option");
                  document.getElementById("status").options.add(opt);
                  opt.text = 'ON PROGRESS';
                  opt.value = 1;
               };
               changetext(document.getElementById("status"));
            },
            error: function(err) {
               document.getElementById("divprogress").style.visibility = 'hidden';
               alert("error: " + err.responseText);
            }
         });
      }

      function update() {
         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/UpdateStatus",
            data: "{'id':" + JSON.stringify(obj.id) + ",'stat':" + JSON.stringify(document.getElementById("status").options[document.getElementById("status").selectedIndex].value) + ",'tanggal':" + JSON.stringify(document.getElementById("tanggal").value) + ",'nik':" + JSON.stringify(obj.nik) + "}",
            dataType: "json",
            success: function(res) {
               if (res.d == "0") {
                  window.returnValue = document.getElementById("status").options[document.getElementById("status").selectedIndex].value;
                  window.close();
               } else {
                  alert(res.d);
               }
            },
            error: function(err) {
               alert("error: " + err.responseText);
            }
         });
      }

      ReadPassedData();
   </script>

   <style type="text/css">
      td.styleHeader
      {
         background-color: #00CC00;
         color: white;
         font-weight: bold;
         font-size: medium;
      }
   </style>
</head>
<body>
   <form id="form1" runat="server">
   <div>
      <table style="width: 100%">
         <tr>
            <td colspan="2" class="styleHeader">
               <div id="header">
               </div>
            </td>
         </tr>
         <tr>
            <td>
               Status Terakhir
            </td>
            <td>
               <asp:DropDownList ID="status" runat="Server" onChange="changetext(this);">
               </asp:DropDownList>
            </td>
         </tr>
         <tr>
            <td>
               Tanggal <span id="lblstatus"></span>
            </td>
            <td>
               <input type="text" readonly="readonly" id="tanggal" />

               <script type="text/javascript">
                  new tcal({
                     'formname': 'form1',
                     'controlname': 'tanggal',
                     'id': 'myCalstartID'
                  }); </script>

            </td>
         </tr>
         <tr>
            <td colspan="2">
               <input type="button" value="Update" onclick="update()" />
            </td>
         </tr>
      </table>
   </div>

   <script type="text/javascript">
      document.getElementById("header").innerText = 'Activity: ' + obj.activity;

      function changetext(dropdown) {
         var myindex  = dropdown.selectedIndex;
         var SelText = dropdown.options[myindex].text;
         document.getElementById("lblstatus").innerText = SelText;
      }

      
   </script>

   </form>
</body>
</html>
