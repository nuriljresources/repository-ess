﻿Imports System.Data
Imports System.Data.SqlClient
Partial Public Class medic_outpatient
    Inherits System.Web.UI.Page
    Public js As String
    Public myDt As DataTable
    Public msg As String
    Public benefit As String
    Public kodeSite As String
    Dim lmedic As New medic
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnPaymentUsage.Visible = ESS.Common.SITE_SETTINGS.PRINT_PAYMENT_APP
        If Session("kdsite") = "JKT" Or kodeSite = "JKT" Then
            btnPaymentUsage.Enabled = True
        Else
            btnPaymentUsage.Enabled = False
        End If

        Dim qryheader As String
        Dim dt As DataTable
        Dim dtb As DataTable = New DataTable()
        Dim sqleditheader, sqleditdetail, sqlfindnama, terbilang, lssaldo, lsused, lsmax As String
        Dim dtbeditmedic As DataTable = New DataTable()
        Dim dtbeditmedicd As DataTable = New DataTable()
        Dim dtbfindnama As DataTable = New DataTable()
        Dim dtbbal As DataTable = New DataTable()
        Dim lscount, ltotal As Integer

        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

        If Session("niksite") = Nothing Or Session("niksite") = "" Then Response.Redirect("login.aspx")

        msg = ""

        If Trim(Session("kdsite").ToString) = "PEN" Then
            IS_PENJOM.Value = "True"
        End If

        If Not IsPostBack Then
            sqlConn.Open()

            If Session("medicnoregstout").ToString = "Editoutpatient" Then
                sqleditheader = "select *, (select nama from H_A101 where nik = H_H215.nik) as nama,(select nmsite from H_A120 where KdSite = H_H215.kdsite) as nmsite, (select niksite from H_A101 where nik=H_H215.nik) as niksite, (select nmjabat from H_A150 where kdjabat = (select kdjabatan from H_A101 where nik = H_H215.nik)) as nmjabat, (select kdjabatan from H_A101 where nik = H_H215.nik) as kdjabat, " & _
                "isnull((Select outpatientmax FROM H_H216 WHERE nik =  H_H215.Nik AND kdsite =  H_H215.kdsite AND tahun =  YEAR(GETDATE()) ),0) as outpatientmax, fstatus, fprint from H_H215 where noreg = '" + Session("medicnoregout") + "'"
                Dim sdaeditheader As SqlDataAdapter = New SqlDataAdapter(sqleditheader, sqlConn)
                sdaeditheader.Fill(dtbeditmedic)
                If dtbeditmedic.Rows.Count > 0 Then
                    txtnoreg.Text = dtbeditmedic.Rows(0)!noreg.ToString
                    txtnmsite.Text = dtbeditmedic.Rows(0)!nmsite.ToString
                    hnmsite.Value = dtbeditmedic.Rows(0)!kdsite.ToString
                    txtnik.Text = dtbeditmedic.Rows(0)!niksite.ToString
                    hnik.Value = dtbeditmedic.Rows(0)!nik.ToString
                    txtlevel.Text = dtbeditmedic.Rows(0)!nmjabat.ToString
                    hkdlevel.Value = dtbeditmedic.Rows(0)!kdjabat.ToString
                    txtdate.Text = dtbeditmedic.Rows(0)!TglTrans.ToString
                    txtcostcode.Text = dtbeditmedic.Rows(0)!pycostcode.ToString
                    txtnama.Text = dtbeditmedic.Rows(0)!nama.ToString
                    txtremarks.Text = dtbeditmedic.Rows(0)!keterangan.ToString
                    'txtplafon.Text = Format(dtbeditmedic.Rows(0)!outpatientmax, "#,##.00").ToString
                    'txtoutpatientmax.Text = Format(dtbeditmedic.Rows(0)!InapMax, "#,##.00").ToString
                    'txtnoreg.Text = dtbeditmedic.Rows(0)!NoReg.ToString
                    kodeSite = hnmsite.Value

                    If dtbeditmedic.Rows(0)!fstatus = 1 Then
                        btnsave.Enabled = False
                        btndelete.Enabled = False
                    End If

                    If dtbeditmedic.Rows(0)!fprint = 1 Then
                        btnprint.Enabled = False
                        btnPaymentUsage.Enabled = False
                    End If

                    sqleditdetail = "select '' As No, NoReg, (convert(char(11),TglDet,101)) as TglDet,Person, '' as nama, '100' as PersenClaim,CONVERT(DECIMAL(14,2),BiayaObat) as BiayaObat,CONVERT(DECIMAL(14,2),BiayaKons) as BiayaKons,CONVERT(DECIMAL(14,2),BiayaLab) as BiayaLab,CONVERT(DECIMAL(14,2),BiayaLain) as BiayaLain,CONVERT(DECIMAL(14,2),BiayaTot) as BiayaTot,(case tdokter when 0 then 'Doctor' when 1 then 'Non Doctor' end) as tdokter ,JDokter,kdSpesialis,kdDiagnosa, dremarks, dokter_remark as dokter_remarks, tDokter as htdokter, (select jNamaDokter from H_A21504 where jDokter = H_H21501.JDokter) as jdokternama, (select nmSpesialis from H_A21505 where kdSpesialis = H_H21501.kdSpesialis) as nmspesialis from H_H21501 where NoReg = '" + Session("medicnoregout") + "'"
                    Dim sdaeditdetail As SqlDataAdapter = New SqlDataAdapter(sqleditdetail, sqlConn)
                    sdaeditdetail.Fill(dtbeditmedicd)
                    If dtbeditmedicd.Rows.Count > 0 Then
                        For lscount = 0 To dtbeditmedicd.Rows.Count - 1
                            If dtbeditmedicd.Rows(lscount)!person.ToString = hnik.Value Then
                                sqlfindnama = "select nama from H_A101 where nik = '" + hnik.Value + "'"
                                Dim sdafindnama As SqlDataAdapter = New SqlDataAdapter(sqlfindnama, sqlConn)
                                dtbfindnama.Clear()
                                sdafindnama.Fill(dtbfindnama)
                                dtbeditmedicd.Rows(lscount)!nama = dtbfindnama.Rows(0)!nama
                            Else
                                sqlfindnama = "select familyname from H_A104 where nik = '" + hnik.Value + "' and familyid = '" + dtbeditmedicd.Rows(lscount)!person.ToString + "'"
                                Dim sdafindnama As SqlDataAdapter = New SqlDataAdapter(sqlfindnama, sqlConn)
                                dtbfindnama.Clear()
                                sdafindnama.Fill(dtbfindnama)
                                dtbeditmedicd.Rows(lscount)!nama = dtbfindnama.Rows(0)!familyname
                            End If
                            ltotal = ltotal + dtbeditmedicd(lscount)!biayatot
                        Next
                        Session("myDatatable") = dtbeditmedicd
                        fillgrid()
                        txttotal.Text = Format(ltotal, "#,##.00")
                    Else
                        dt = New DataTable
                        dt = lmedic.FetchOutpatient()

                        Session("myDatatable") = dt
                        fillgrid()
                    End If
                End If
            End If

            If Session("medicnoregstout").ToString = "" Then
                qryheader = "select nik, KdSite, (select nmsite from H_A120 where KdSite = H_A101.kdsite) as nmsite,pycostcode,NIKSITE, Nama,KdJabatan, (select nmjabat from H_A150 where kdjabat = H_A101.KdJabatan) as nmjabat, " & _
                            "isnull((Select outpatientmax FROM H_H216 WHERE nik =  H_A101.Nik AND kdsite =  H_A101.kdsite AND tahun =  YEAR(GETDATE()) ),0) as outpatientmax " & _
                            "from H_A101 where nik = '" + Session("niksite") + "' and( H_A101.Active = '1' ) AND ( H_A101.StEdit <> '2' )"
                Dim sda As SqlDataAdapter = New SqlDataAdapter(qryheader, sqlConn)
                sda.Fill(dtb)

                If dtb.Rows.Count > 0 Then
                    txtnmsite.Text = dtb.Rows(0)!nmsite.ToString
                    hnmsite.Value = dtb.Rows(0)!KdSite.ToString
                    txtcostcode.Text = dtb.Rows(0)!pycostcode.ToString
                    txtnik.Text = dtb.Rows(0)!NIKSITE.ToString
                    hnik.Value = dtb.Rows(0)!nik.ToString
                    txtnama.Text = dtb.Rows(0)!Nama.ToString
                    txtlevel.Text = dtb.Rows(0)!nmjabat.ToString
                    hkdlevel.Value = dtb.Rows(0)!KdJabatan.ToString
                    'txtplafon.Text = dtb.Rows(0)!outpatientmax.ToString
                    'txtoutpatientmax.Text = Format(dtb.Rows(0)!outpatientmax, "#,##.00")
                End If

                dt = New DataTable
                dt = lmedic.FetchOutpatient()

                Session("myDatatable") = dt
                fillgrid()
                btndelete.Visible = False
                btnprint.Enabled = False
                btnPaymentUsage.Enabled = False
            End If
            txtdate.Attributes.Add("readonly", "readonly")
            txtnmsite.Attributes.Add("readonly", "readonly")
            txtcostcode.Attributes.Add("readonly", "readonly")
            txtnik.Attributes.Add("readonly", "readonly")
            txtnama.Attributes.Add("readonly", "readonly")
            txtlevel.Attributes.Add("readonly", "readonly")
            'txtoutpatientmax.Attributes.Add("readonly", "readonly")
            txttotal.Attributes.Add("readonly", "readonly")
            Session("medicnoregout") = ""
            Session("medicnoregstout") = ""
            'js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%d/%m/%Y');</script>"
            js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y');</script>"
            cnt.Value = GridView1.FooterRow.FindControl("txtNewNama").ClientID
            cnt2.Value = GridView1.FooterRow.FindControl("hnewperson").ClientID
            hprint.Value = 0
        End If
        txtdate.Text = Today.Date.ToString("MM/dd/yyyy")
        'Dim ddlnewtdokter As DropDownList = DirectCast(GridView1.FooterRow.FindControl("ddlnewtdokter"), DropDownList)
        'Dim ddlnewdoctortype As DropDownList = DirectCast(GridView1.FooterRow.FindControl("ddlnewdoctortype"), DropDownList)
        'Dim ddlnewspecialist As DropDownList = DirectCast(GridView1.FooterRow.FindControl("ddlnewspecialist"), DropDownList)

        'If Not IsPostBack Then
        '    ddlnewdoctortype.Items.Insert(0, New ListItem("Umum", "1"))
        '    ddlnewdoctortype.Items.Insert(1, New ListItem("Spesialis", "2"))
        '    ddlnewdoctortype.Items.Insert(2, New ListItem("Gigi", "3"))

        '    If ddlnewdoctortype.SelectedItem.ToString <> "Spesialis" Then
        '        ddlnewspecialist.Enabled = False
        '        'ddlnewspecialist.SelectedValue = ""
        '    End If
        'End If

        'Dim sqlbal As String = "SELECT top 1 CASE year(h_a000.tglmulai) WHEN year(H_H21501.TglDet) THEN isnull(h_a101.outpatientmax , 0) ELSE isnull((SELECT usedsaldo FROM h_h216 WHERE tahun = year(H_H21501.TglDet) AND nik = H_H215.Nik AND kdsite = H_H215.KdSite) , 0) END AS 'saldo', ISNULL((SELECT SUM(a.BiayaTot) AS Expr1 FROM H_H21501 AS a INNER JOIN H_H215 AS b ON a.NoReg = b.NoReg WHERE (b.Nik = H_H215.Nik) AND (YEAR(a.TglDet) = YEAR(H_H21501.TglDet)) AND (b.TglTrans <= H_H215.TglTrans) AND (b.StEdit <> '2') AND (b.fstatus = 1)), 0) AS used, ISNULL((SELECT TOP (1) outpatientmax FROM H_H216 WHERE (Nik = H_H215.Nik) AND (KdSite = H_H215.KdSite) AND (Tahun = YEAR(H_H21501.TglDet))), 0) AS Max FROM H_H215 INNER JOIN H_H21501 ON H_H215.NoReg = H_H21501.NoReg INNER JOIN H_A101 ON H_H215.Nik = H_A101.Nik INNER JOIN H_A130 ON H_A101.KdDepar = H_A130.KdDepar CROSS JOIN H_A000 WHERE (H_H215.Nik = '" + hnik.Value + "') order by H_H215.TglTrans desc"
        Dim sqlbal As String = "select '0' as saldo, (isnull(( SELECT sum( isnull(H_H21501.biayatot,0) ) FROM H_H21501, H_H215 WHERE H_H215.Tipe='0' and H_H215.fstatus='1' and H_H215.StEdit <> '2'  AND H_H215.Nik  = '" + hnik.Value + "'  and H_H215.noreg = H_H21501.noreg AND year(H_H21501.tgldet)=  YEAR(GETDATE()) group by h_h215.nik  ),0)  + "
        sqlbal = sqlbal + " case  year(h_a000.tglmulai) when   YEAR(GETDATE()) then  isnull(h_a101.outpatientmax,0) else   isnull((select usedsaldo from h_h216 where tahun= YEAR(GETDATE()) and nik='" + hnik.Value + "' and kdsite= h_a101.kdsite),0) end) as used,"
        sqlbal = sqlbal + " (select H_H216.outpatientmax from H_H216 where nik = '" + hnik.Value + "' and tahun= YEAR(GETDATE()) and kdsite= h_a101.kdsite) as max from h_a101,h_a000 where nik = '" + hnik.Value + "' "

        Dim sdabal As SqlDataAdapter = New SqlDataAdapter(sqlbal, sqlConn)
        sdabal.Fill(dtbbal)

        If dtbbal.Rows.Count > 0 Then
            lssaldo = dtbbal.Rows(0)!saldo.ToString
            lsused = dtbbal.Rows(0)!used.ToString
            lsmax = dtbbal.Rows(0)!max.ToString
            If lssaldo = "" Then lssaldo = "0"
            If lsused = "" Then lsused = "0"
            If lsmax = "" Then lsmax = "0"
            txtplafon.Text = Format(CDbl(lsmax) - (CDbl(lsused) + CDbl(lssaldo)), "#,##.00").ToString
            benefit = Format(CDbl(lsmax), "#,##.00").ToString
        End If

        If Session("medicnoregstout") = "" Then
            Try
                'js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%d/%m/%Y');</script>"
                js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y');</script>"
                cnt.Value = GridView1.FooterRow.FindControl("txtNewNama").ClientID
                cnt2.Value = GridView1.FooterRow.FindControl("hnewperson").ClientID
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub fillgrid()
        Dim dt As DataTable = Session("myDatatable")

        If dt.Rows.Count > 0 Then
            If dt.Rows(0)!TglDet.ToString = "" Then
                GridView1.DataSource = dt
                GridView1.DataBind()

                Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                GridView1.Rows(0).Cells.Clear()
                GridView1.Rows(0).Cells.Add(New TableCell())
                GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                GridView1.Rows(0).Cells(0).Text = "Insert medical claim"
                GridView1.Rows(0).ForeColor = Drawing.Color.White
                Session("myDatatable") = dt
            Else
                GridView1.DataSource = dt
                GridView1.DataBind()
            End If
        Else
            dt.Rows.Add(dt.NewRow())
            GridView1.DataSource = dt
            GridView1.DataBind()

            Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
            GridView1.Rows(0).Cells.Clear()
            GridView1.Rows(0).Cells.Add(New TableCell())
            GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
            GridView1.Rows(0).Cells(0).Text = "Insert medical claim"
            GridView1.Rows(0).ForeColor = Drawing.Color.White
            Session("myDatatable") = dt
        End If
    End Sub

    Private Sub fillgridedit()
        Dim dt As DataTable = Session("myDatatable")
        Dim lscount As Integer

        If dt.Rows.Count > 0 Then
            If dt.Rows(0)!TglDet.ToString = "" Then
                GridView1.DataSource = dt
                GridView1.DataBind()

                Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                GridView1.Rows(0).Cells.Clear()
                GridView1.Rows(0).Cells.Add(New TableCell())
                GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                GridView1.Rows(0).Cells(0).Text = "Insert medical claim"
                GridView1.Rows(0).ForeColor = Drawing.Color.White
                Session("myDatatable") = dt
            Else
                For lscount = 0 To dt.Rows.Count - 1
                    'If dt.Rows(lscount)!tdokter.ToString = "Doctor" Then
                    '    dt.Rows(lscount)!tdokter = "0"
                    'ElseIf dt.Rows(lscount)!tdokter.ToString = "Non Doctor" Then
                    '    dt.Rows(lscount)!tdokter = "1"
                    'ElseIf dt.Rows(lscount)!tdokter.ToString = "0" Then
                    '    dt.Rows(lscount)!tdokter = "Doctor"
                    'ElseIf dt.Rows(lscount)!tdokter.ToString = "1" Then
                    '    dt.Rows(lscount)!tdokter = "Non Doctor"
                    'End If

                    'If dt.Rows(lscount)!jdokter.ToString = "Umum" Then
                    '    dt.Rows(lscount)!jdokter = "1"
                    'ElseIf dt.Rows(lscount)!jdokter.ToString = "Spesialis" Then
                    '    dt.Rows(lscount)!jdokter = "2"
                    'ElseIf dt.Rows(lscount)!jdokter.ToString = "Gigi" Then
                    '    dt.Rows(lscount)!jdokter = "3"
                    'ElseIf dt.Rows(lscount)!jdokter.ToString = "Bidan" Then
                    '    dt.Rows(lscount)!jdokter = "4"
                    'ElseIf dt.Rows(lscount)!jdokter.ToString = "Paramedik" Then
                    '    dt.Rows(lscount)!jdokter = "5"
                    'ElseIf dt.Rows(lscount)!jdokter.ToString = "Lainnya" Then
                    '    dt.Rows(lscount)!jdokter = "6"
                    'ElseIf dt.Rows(lscount)!jdokter.ToString = "1" Then
                    '    dt.Rows(lscount)!jdokter = "Umum"
                    'ElseIf dt.Rows(lscount)!jdokter.ToString = "2" Then
                    '    dt.Rows(lscount)!jdokter = "Spesialis"
                    'ElseIf dt.Rows(lscount)!jdokter.ToString = "3" Then
                    '    dt.Rows(lscount)!jdokter = "Gigi"
                    'ElseIf dt.Rows(lscount)!jdokter.ToString = "4" Then
                    '    dt.Rows(lscount)!jdokter = "Bidan"
                    'ElseIf dt.Rows(lscount)!jdokter.ToString = "5" Then
                    '    dt.Rows(lscount)!jdokter = "Paramedik"
                    'ElseIf dt.Rows(lscount)!jdokter.ToString = "6" Then
                    '    dt.Rows(lscount)!jdokter = "Lainnya"
                    'Else
                    '    dt.Rows(lscount)!jdokter = ""
                    'End If

                    'If dt.Rows(lscount)!kdspesialis.ToString = "Penyakit Dalam" Then
                    '    dt.Rows(lscount)!kdspesialis = "01"
                    'ElseIf dt.Rows(lscount)!kdspesialis.ToString = "Bedah Ortopedi" Then
                    '    dt.Rows(lscount)!kdspesialis = "02"
                    'ElseIf dt.Rows(lscount)!kdspesialis.ToString = "Kebidanan dan kandungan" Then
                    '    dt.Rows(lscount)!kdspesialis = "03"
                    'ElseIf dt.Rows(lscount)!kdspesialis.ToString = "Anak" Then
                    '    dt.Rows(lscount)!kdspesialis = "04"
                    'ElseIf dt.Rows(lscount)!kdspesialis.ToString = "Mata" Then
                    '    dt.Rows(lscount)!kdspesialis = "05"
                    'ElseIf dt.Rows(lscount)!kdspesialis.ToString = "THT" Then
                    '    dt.Rows(lscount)!kdspesialis = "06"
                    'ElseIf dt.Rows(lscount)!kdspesialis.ToString = "Saraf" Then
                    '    dt.Rows(lscount)!kdspesialis = "07"
                    'ElseIf dt.Rows(lscount)!kdspesialis.ToString = "Kulit dan Kelamin" Then
                    '    dt.Rows(lscount)!kdspesialis = "08"
                    'ElseIf dt.Rows(lscount)!kdspesialis.ToString = "01" Then
                    '    dt.Rows(lscount)!kdspesialis = "Penyakit Dalam"
                    'ElseIf dt.Rows(lscount)!kdspesialis.ToString = "02" Then
                    '    dt.Rows(lscount)!kdspesialis = "Bedah Ortopedi"
                    'ElseIf dt.Rows(lscount)!kdspesialis.ToString = "03" Then
                    '    dt.Rows(lscount)!kdspesialis = "Kebidanan dan kandungan"
                    'ElseIf dt.Rows(lscount)!kdspesialis.ToString = "04" Then
                    '    dt.Rows(lscount)!kdspesialis = "Anak"
                    'ElseIf dt.Rows(lscount)!kdspesialis.ToString = "05" Then
                    '    dt.Rows(lscount)!kdspesialis = "Mata"
                    'ElseIf dt.Rows(lscount)!kdspesialis.ToString = "06" Then
                    '    dt.Rows(lscount)!kdspesialis = "THT"
                    'ElseIf dt.Rows(lscount)!kdspesialis.ToString = "07" Then
                    '    dt.Rows(lscount)!kdspesialis = "Saraf"
                    'ElseIf dt.Rows(lscount)!kdspesialis.ToString = "08" Then
                    '    dt.Rows(lscount)!kdspesialis = "Kulit dan Kelamin"
                    'Else
                    '    dt.Rows(lscount)!kdspesialis = ""
                    'End If
                Next

                GridView1.DataSource = dt
                GridView1.DataBind()
            End If
        Else
            dt.Rows.Add(dt.NewRow())
            GridView1.DataSource = dt
            GridView1.DataBind()

            Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
            GridView1.Rows(0).Cells.Clear()
            GridView1.Rows(0).Cells.Add(New TableCell())
            GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
            GridView1.Rows(0).Cells(0).Text = "Insert medical claim"
            GridView1.Rows(0).ForeColor = Drawing.Color.White
            Session("myDatatable") = dt
        End If
    End Sub


    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
        If e.CommandName.Equals("AddNew") Then
            'Dim No As HiddenField = DirectCast(GridView1.FooterRow.FindControl("No"), HiddenField)
            Dim txtNewdt As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewdt"), TextBox)
            Dim hnewperson As HiddenField = DirectCast(GridView1.FooterRow.FindControl("hnewperson"), HiddenField)
            Dim txtNewNama As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewNama"), TextBox)
            'Dim txtNewperclaim As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewperclaim"), TextBox)
            Dim txtNewbiayakons As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewbiayakons"), TextBox)
            Dim txtNewbiayaobat As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewbiayaobat"), TextBox)
            Dim txtNewbiayalain As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewbiayalain"), TextBox)
            Dim txtNewbiayalab As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewbiayalab"), TextBox)
            Dim txtNewbiayatot As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewbiayatot"), TextBox)
            Dim ddlnewtdokter As DropDownList = DirectCast(GridView1.FooterRow.FindControl("ddlnewtdokter"), DropDownList)
            Dim ddlnewdoctortype As DropDownList = DirectCast(GridView1.FooterRow.FindControl("ddlnewdoctortype"), DropDownList)
            Dim ddlnewspecialist As DropDownList = DirectCast(GridView1.FooterRow.FindControl("ddlnewspecialist"), DropDownList)
            Dim txtNewdiagnose As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewdiagnose"), TextBox)
            Dim txtNewdoctorname As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewdoctorname"), TextBox)
            Dim txtNewremarks As TextBox = DirectCast(GridView1.FooterRow.FindControl("txtNewremarks"), TextBox)

            myDt = Session("myDatatable")

            Dim li As Integer
            For li = 0 To myDt.Rows.Count - 1
                If GridView1.Rows(0).Cells(0).Text <> "Insert medical claim" Then
                    If myDt.Rows(li)!TglDet.ToString = "" Then myDt.Rows(li)!TglDet = "1/1/1900"
                    If CDate(myDt.Rows(li)!TglDet) = CDate(txtNewdt.Text) And myDt.Rows(li)!person.ToString = hnewperson.Value Then
                        msg = "<script type='text/javascript'> alert('Data detail already exist'); </script>"
                        Return
                    End If
                    If CDate(myDt.Rows(li)!TglDet).Year <> CDate(txtNewdt.Text).Year Then
                        msg = "<script type='text/javascript'> alert('Must in the same year'); </script>"
                        Return
                    End If
                End If
            Next

            Dim tipedokter As String

            If ddlnewdoctortype.SelectedValue.ToString = "" Then
                tipedokter = 0
            Else
                tipedokter = ddlnewdoctortype.SelectedValue.ToString
            End If

            If tipedokter <> 2 Then
                ddlnewspecialist.Items.Clear()
                ddlnewspecialist.Items.Insert(0, New ListItem(" ", ""))
                ddlnewspecialist.SelectedValue = ""
            End If

            If txtNewbiayakons.Text = "" Then txtNewbiayakons.Text = "0"
            If txtNewbiayaobat.Text = "" Then txtNewbiayaobat.Text = "0"
            If txtNewbiayalain.Text = "" Then txtNewbiayalain.Text = "0"
            If txtNewbiayalab.Text = "" Then txtNewbiayalab.Text = "0"

            If ddlnewtdokter.SelectedValue = "" Then
                msg = "<script type='text/javascript'> alert('Please select TreatmentBy'); </script>"
                Return
            End If

            If CDbl(txtNewbiayakons.Text) < 0 Or CDbl(txtNewbiayaobat.Text) < 0 Or CDbl(txtNewbiayalain.Text) < 0 Or CDbl(txtNewbiayalab.Text) < 0 Then
                msg = "<script type='text/javascript'> alert('Medicine and other Expenses must not minus'); </script>"
                Return
            End If

            Dim sqlcheckmedicmonth As String
            Dim dtb As DataTable = New DataTable
            Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
            Try
                sqlcheckmedicmonth = "select kdsite, medicmonth from  H_a120 where KdSite = '" + hnmsite.Value + "'"
                Dim sqlda As SqlDataAdapter = New SqlDataAdapter(sqlcheckmedicmonth, sqlConn)
                sqlda.Fill(dtb)

                If dtb.Rows.Count > 0 Then
                    Dim datenumberd As Double
                    Dim liBlnSlisih As Double
                    Dim medicmonth As String
                    Dim nowdate As String
                    Dim nextdate As String

                    'perubahan 14 april 2014
                    nowdate = dtb.Rows(0)!medicmonth.ToString
                    If Month(txtNewdt.Text) = 12 Then
                        If Trim(Session("kdsite").ToString) = "SER" Then
                            'bug aneh dulunya gak masalah sekarang jadi error 8 agustus 2018
                            'nextdate = Day(txtNewdt.Text).ToString + "/" + Month(CDate(txtNewdt.Text).AddMonths(6)).ToString + "/" + Year(CDate(txtNewdt.Text).AddYears(1)).ToString
                            nextdate = Month(CDate(txtNewdt.Text).AddMonths(6)).ToString + "/" + Day(txtNewdt.Text).ToString + "/" + Year(CDate(txtNewdt.Text).AddYears(1)).ToString
                        Else
                            'nextdate = Day(txtNewdt.Text).ToString + "/" + Month(CDate(txtNewdt.Text).AddMonths(1)).ToString + "/" + Year(CDate(txtNewdt.Text).AddYears(1)).ToString
                            nextdate = Month(CDate(txtNewdt.Text).AddMonths(1)).ToString + "/" + Day(txtNewdt.Text).ToString + "/" + Year(CDate(txtNewdt.Text).AddYears(1)).ToString
                        End If
                    Else
                        Dim ismonth As Date
                        Dim ilastday As Integer

                        If Trim(Session("kdsite").ToString) = "SER" Then
                            'nextdate = Day(txtNewdt.Text).ToString + "/" + Month(CDate(txtNewdt.Text).AddMonths(6)).ToString + "/" + Year(CDate(txtNewdt.Text).AddMonths(6)).ToString
                            nextdate = Month(CDate(txtNewdt.Text).AddMonths(6)).ToString + "/" + Day(txtNewdt.Text).ToString + "/" + Year(CDate(txtNewdt.Text).AddMonths(6)).ToString
                        Else
                            'nextdate = Day(txtNewdt.Text).ToString + "/" + Month(CDate(txtNewdt.Text).AddMonths(1)).ToString + "/" + Year(txtNewdt.Text).ToString
                            nextdate = Month(CDate(txtNewdt.Text).AddMonths(1)).ToString + "/" + Day(txtNewdt.Text).ToString + "/" + Year(txtNewdt.Text).ToString
                        End If
                        'ismonth = "1/" + Month(CDate(txtNewdt.Text).AddMonths(1)).ToString + "/" + Year(txtNewdt.Text).ToString
                        ismonth = Month(CDate(txtNewdt.Text).AddMonths(1)).ToString + "/" + "1/" + Year(txtNewdt.Text).ToString
                        ilastday = CInt(Date.DaysInMonth(ismonth.Year, ismonth.Month))

                        If ilastday < CInt(Day(nextdate).ToString) Then
                            'nextdate = ilastday.ToString + "/" + Month(CDate(nextdate).AddMonths(1)).ToString + "/" + Year(nextdate).ToString
                            nextdate = Month(CDate(nextdate).AddMonths(1)).ToString + "/" + ilastday.ToString + "/" + Year(nextdate).ToString
                            nextdate = CDate(nextdate).AddDays(1).ToString
                        End If
                    End If



                    If txtNewdt.Text = "" Then txtNewdt.Text = "1/1/1900"
                    Dim dtDetail As Date = CDate(txtNewdt.Text)

                    If Session("kdsite") = "BAK" Then

                        If Date.Now.Subtract(dtDetail).Days >= 365 Then
                            msg = "<script type='text/javascript'> alert('Date cannot over 1 year from date receipt'); </script>"
                            Return
                        End If

                    ElseIf Session("kdsite") = "SER" Then

                        If Date.Now.Subtract(dtDetail).Days >= 90 Then
                            msg = "<script type='text/javascript'> alert('Date cannot over 90 days from date receipt'); </script>"
                            Return
                        End If

                    ElseIf Session("kdsite") = "PEN" Then

                        If Date.Now.Subtract(dtDetail).Days >= 30 Then
                            msg = "<script type='text/javascript'> alert('Date cannot over 30 days from date receipt'); </script>"
                            Return
                        End If

                    Else


                        'medicmonth = dtb.Rows(0)!medicmonth.ToString
                        'If medicmonth = "" Then medicmonth = 0

                        'datenumberd = DateDiff("d", CDate(txtNewdt.Text), CDate(txtdate.Text))
                        'datenumberd = datenumberd / 30
                        'liBlnSlisih = Math.Round(datenumberd, 2)
                        'If liBlnSlisih > CDbl(medicmonth) Then
                        If Trim(Session("kdsite").ToString) <> "LAN" And Trim(Session("kdsite").ToString) <> "BAK" And Trim(Session("kdsite").ToString) <> "PAN" And Trim(Session("kdsite").ToString) <> "DOU" Then
                            If CDate(txtdate.Text) > CDate(nextdate) Then
                                'js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%d/%m/%Y');</script>"
                                js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y');</script>"
                                cnt.Value = GridView1.FooterRow.FindControl("txtNewNama").ClientID
                                cnt2.Value = GridView1.FooterRow.FindControl("hnewperson").ClientID
                                If Trim(Session("kdsite").ToString) = "SER" Then
                                    msg = "<script type='text/javascript'> alert('Detail Receipt Date Claim Expired; over 6 month from header Claim Date'); </script>"
                                Else
                                    msg = "<script type='text/javascript'> alert('Detail Receipt Date Claim Expired; over 1 month from header Claim Date'); </script>"
                                End If

                                If GridView1.Rows(0).Cells(0).Text = "Insert medical claim" Then
                                    Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
                                    GridView1.Rows(0).Cells.Clear()
                                    GridView1.Rows(0).Cells.Add(New TableCell())
                                    GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
                                    GridView1.Rows(0).Cells(0).Text = "Insert medical claim"
                                    GridView1.Rows(0).ForeColor = Drawing.Color.White
                                End If

                                Return
                            End If
                        End If


                    End If





                End If
            Catch ex As Exception
                msg = "<script type='text/javascript'> alert('failed to check medic month'); </script>"
            End Try

            lmedic.InsertOutpatient(txtNewdt.Text, txtNewNama.Text, hnewperson.Value, "100", txtNewbiayakons.Text, txtNewbiayaobat.Text, txtNewbiayalain.Text, txtNewbiayalab.Text, txtNewbiayatot.Text, ddlnewtdokter.SelectedItem.ToString, ddlnewtdokter.SelectedValue, ddlnewdoctortype.SelectedItem.ToString, ddlnewdoctortype.SelectedValue, ddlnewspecialist.SelectedItem.ToString, ddlnewspecialist.SelectedValue, txtNewdiagnose.Text, txtNewdoctorname.Text, txtNewremarks.Text, CType(Session("myDatatable"), DataTable))
            myDt = Session("myDatatable")
            fillgrid()

            Dim licount As Integer
            Dim ltotal As Decimal
            'txttotal.text = mydt.rows.count.tostring

            For licount = 0 To myDt.Rows.Count - 1
                ltotal = CDec(ltotal) + CDec(myDt.Rows(licount)!biayatot)
            Next

            txttotal.Text = ltotal.ToString("#,##.00")

            js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y');</script>"
            cnt.Value = GridView1.FooterRow.FindControl("txtNewNama").ClientID
            cnt2.Value = GridView1.FooterRow.FindControl("hnewperson").ClientID
            'Page_Load(0, e)
        End If
    End Sub

    Protected Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As GridViewEditEventArgs)
        Dim lsddldoctype, lsddlspecialist As String
        Dim dt As DataTable = Session("myDatatable")
        GridView1.EditIndex = e.NewEditIndex

        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

        fillgridedit()

        Dim ddltdokter As DropDownList = DirectCast(GridView1.Rows(e.NewEditIndex).FindControl("ddltdokter"), DropDownList)
        Dim ddldoctortype As DropDownList = DirectCast(GridView1.Rows(e.NewEditIndex).FindControl("ddldoctortype"), DropDownList)
        Dim ddlspecialist As DropDownList = DirectCast(GridView1.Rows(e.NewEditIndex).FindControl("ddlspecialist"), DropDownList)

        Dim qry As String
        Dim qry2 As String
        Dim dtb As DataTable = New DataTable()
        Dim dtb2 As DataTable = New DataTable()

        lsddldoctype = dt.Rows(e.NewEditIndex)!jdokter.ToString
        lsddlspecialist = dt.Rows(e.NewEditIndex)!kdspesialis.ToString

        qry = "select jDokter,jNamaDokter,tdokter from H_A21504 where tdokter = '" + ddltdokter.SelectedValue + "'"
        Dim sda As SqlDataAdapter = New SqlDataAdapter(qry, sqlConn)
        sda.Fill(dtb)
        If dtb.Rows.Count > 0 Then
            ddldoctortype.DataSource = dtb
            ddldoctortype.DataTextField = "jNamaDokter"
            ddldoctortype.DataValueField = "jDokter"
            ddldoctortype.DataBind()
        End If

        ddldoctortype.SelectedValue = lsddldoctype

        If ddldoctortype.SelectedValue <> 2 Then
            ddlspecialist.Items.Clear()
            ddlspecialist.Items.Insert(0, New ListItem(" ", ""))
            ddlspecialist.SelectedValue = ""
        Else
            qry2 = "select kdspesialis, nmspesialis from H_A21505"
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(qry2, sqlConn)
            sda2.Fill(dtb2)
            If dtb2.Rows.Count > 0 Then
                ddlspecialist.DataSource = dtb2
                ddlspecialist.DataTextField = "nmspesialis"
                ddlspecialist.DataValueField = "kdspesialis"
                ddlspecialist.DataBind()
            End If
        End If
        ddlspecialist.SelectedValue = lsddlspecialist

        cnt.Value = GridView1.Rows(e.NewEditIndex).FindControl("txtNama").ClientID
        cnt2.Value = GridView1.Rows(e.NewEditIndex).FindControl("hperson").ClientID
        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn2', '" + GridView1.Rows(e.NewEditIndex).ClientID + "_txtDateinv', '%m/%d/%Y');</script>"

    End Sub

    Protected Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As GridViewCancelEditEventArgs)
        GridView1.EditIndex = -1

        fillgridedit()
        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y');</script>"
    End Sub

    Protected Sub GridView1_RowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs)
        Dim txtDateinv As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtDateinv"), TextBox)
        Dim hperson As HiddenField = DirectCast(GridView1.Rows(e.RowIndex).FindControl("hperson"), HiddenField)
        Dim txtNama As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtNama"), TextBox)
        'Dim txtperclaim As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtperclaim"), TextBox)
        Dim txtbiayakons As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtbiayakons"), TextBox)
        Dim txtbiayaobat As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtbiayaobat"), TextBox)
        Dim txtbiayalain As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtbiayalain"), TextBox)
        Dim txtbiayalab As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtbiayalab"), TextBox)
        Dim txtbiayatot As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtbiayatot"), TextBox)
        Dim ddltdokter As DropDownList = DirectCast(GridView1.Rows(e.RowIndex).FindControl("ddltdokter"), DropDownList)
        Dim ddldoctortype As DropDownList = DirectCast(GridView1.Rows(e.RowIndex).FindControl("ddldoctortype"), DropDownList)
        Dim ddlspecialist As DropDownList = DirectCast(GridView1.Rows(e.RowIndex).FindControl("ddlspecialist"), DropDownList)
        Dim txtdiagnose As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtdiagnose"), TextBox)
        Dim txtdoctorname As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtdoctorname"), TextBox)
        Dim txtremarks As TextBox = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtremarks"), TextBox)

        Dim ltotal As Double

        If txtbiayakons.Text = "" Then txtbiayakons.Text = "0"
        If txtbiayaobat.Text = "" Then txtbiayaobat.Text = "0"
        If txtbiayalain.Text = "" Then txtbiayalain.Text = "0"
        If txtbiayalab.Text = "" Then txtbiayalab.Text = "0"

        myDt = Session("myDatatable")

        Dim li As Integer
        For li = 0 To myDt.Rows.Count - 1
            If myDt.Rows(li)!TglDet.ToString = "" Then myDt.Rows(li)!TglDet = "1/1/1900"
            If e.RowIndex <> li Then
                If CDate(myDt.Rows(li)!TglDet.ToString) = CDate(txtDateinv.Text) And myDt.Rows(li)!Person.ToString = hperson.Value Then
                    msg = "<script type='text/javascript'> alert('Data detail already exist'); </script>"
                    Return
                End If

                If CDate(myDt.Rows(li)!TglDet).Year <> CDate(txtDateinv.Text).Year Then
                    msg = "<script type='text/javascript'> alert('Must same in the same year'); </script>"
                    Return
                End If
            End If
        Next

        If CDbl(txtbiayakons.Text) < 0 Or CDbl(txtbiayaobat.Text) < 0 Or CDbl(txtbiayalain.Text) < 0 Or CDbl(txtbiayalab.Text) < 0 Then
            msg = "<script type='text/javascript'> alert('Medicine and other Expenses must not minus'); </script>"
            Return
        End If

        Dim sqlcheckmedicmonth As String
        Dim dtb As DataTable = New DataTable
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Try
            sqlcheckmedicmonth = "select kdsite, medicmonth from  H_a120 where KdSite = '" + hnmsite.Value + "'"
            Dim sqlda As SqlDataAdapter = New SqlDataAdapter(sqlcheckmedicmonth, sqlConn)
            sqlda.Fill(dtb)

            If dtb.Rows.Count > 0 Then
                Dim datenumberd As Double
                Dim liBlnSlisih As Double
                Dim medicmonth As String
                Dim nowdate As String
                Dim nextdate As String

                'perubahan 14 april 2014
                nowdate = dtb.Rows(0)!medicmonth.ToString
                If Month(txtDateinv.Text) = 12 Then
                    If Trim(Session("kdsite").ToString) = "SER" Then
                        nextdate = Month(CDate(txtDateinv.Text).AddMonths(1)).ToString + "/" + Day(txtDateinv.Text).ToString + "/" + Year(CDate(txtDateinv.Text).AddYears(1)).ToString
                    Else
                        nextdate = Month(CDate(txtDateinv.Text).AddMonths(6)).ToString + "/" + Day(txtDateinv.Text).ToString + "/" + Year(CDate(txtDateinv.Text).AddYears(1)).ToString
                    End If
                Else
                    Dim ismonth As Date
                    Dim ilastday As Integer

                    If Trim(Session("kdsite").ToString) = "SER" Then
                        nextdate = Month(CDate(txtDateinv.Text).AddMonths(6)).ToString + "/" + Day(txtDateinv.Text).ToString + "/" + Year(CDate(txtDateinv.Text).AddMonths(6)).ToString
                    Else
                        nextdate = Month(CDate(txtDateinv.Text).AddMonths(1)).ToString + "/" + Day(txtDateinv.Text).ToString + "/" + Year(txtDateinv.Text).ToString
                    End If

                    ismonth = Month(CDate(txtDateinv.Text).AddMonths(1)).ToString + "/1/" + Year(txtDateinv.Text).ToString
                    ilastday = CInt(Date.DaysInMonth(ismonth.Year, ismonth.Month))

                    If ilastday < CInt(Day(txtDateinv.Text).ToString) Then
                        nextdate = Month(CDate(txtDateinv.Text).AddMonths(1)).ToString + "/" + ilastday.ToString + "/" + Year(txtDateinv.Text).ToString
                        nextdate = CDate(nextdate).AddDays(1).ToString
                    End If

                End If

                'medicmonth = dtb.Rows(0)!medicmonth.ToString
                'If medicmonth = "" Then medicmonth = 0

                'datenumberd = DateDiff("d", txtDateinv.Text, txtdate.Text)
                'datenumberd = datenumberd / 30
                'liBlnSlisih = Math.Round(datenumberd, 2)
                'If liBlnSlisih > CDbl(medicmonth) Then
                If Trim(Session("kdsite").ToString) <> "LAN" And Trim(Session("kdsite").ToString) <> "BAK" And Trim(Session("kdsite").ToString) <> "PAN" And Trim(Session("kdsite").ToString) <> "DOU" Then
                    If CDate(txtdate.Text) > CDate(nextdate) Then
                        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y'); cal.manageFields('btn2', '" + GridView1.Rows(e.RowIndex).ClientID + "_txtDateinv', '%m/%d/%Y');</script>"
                        If Trim(Session("kdsite").ToString) = "SER" Then
                            msg = "<script type='text/javascript'> alert('Detail Receipt Date Claim Expired; over 6 month from header Claim Date'); </script>"
                        Else
                            msg = "<script type='text/javascript'> alert('Detail Receipt Date Claim Expired; over 1 month from header Claim Date'); </script>"
                        End If
                        Return
                    Else
                        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y');</script>"
                    End If

                End If
            End If
        Catch ex As Exception
            msg = "<script type='text/javascript'> alert('failed to check medic month'); </script>"
        End Try

        lmedic.UpdateOutpatient(txtDateinv.Text, hperson.Value, txtNama.Text, "100", txtbiayakons.Text, txtbiayaobat.Text, txtbiayalain.Text, txtbiayalab.Text, txtbiayatot.Text, ddltdokter.SelectedItem.ToString, ddltdokter.SelectedValue, ddldoctortype.SelectedItem.ToString, ddldoctortype.SelectedValue, ddlspecialist.SelectedItem.ToString, ddlspecialist.SelectedValue, txtdiagnose.Text, txtdoctorname.Text, txtremarks.Text, CType(Session("myDatatable"), DataTable), e.RowIndex)

        For licount = 0 To myDt.Rows.Count - 1
            ltotal = CDec(ltotal) + CDec(myDt.Rows(licount)!biayatot)
        Next

        txttotal.Text = ltotal.ToString("#,##.00")

        GridView1.EditIndex = -1
        fillgrid()
    End Sub

    Protected Sub GridView1_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)
        Dim ltotal As Double
        lmedic.Delete(CType(Session("myDatatable"), DataTable), e.RowIndex)
        fillgrid()
        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y');</script>"
        cnt.Value = GridView1.FooterRow.FindControl("txtNewNama").ClientID
        cnt2.Value = GridView1.FooterRow.FindControl("hnewperson").ClientID
        myDt = Session("myDatatable")
        For licount = 0 To myDt.Rows.Count - 1
            If myDt.Rows(licount)!biayatot.ToString = "" Then myDt.Rows(licount)!biayatot = 0
            ltotal = ltotal + myDt.Rows(licount)!biayatot
        Next

        txttotal.Text = Format(ltotal, "#,##.00")
    End Sub

    Protected Sub ddlnewtdokter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlnewtdokter As DropDownList = DirectCast(GridView1.FooterRow.FindControl("ddlnewtdokter"), DropDownList)
        Dim ddlnewdoctortype As DropDownList = DirectCast(GridView1.FooterRow.FindControl("ddlnewdoctortype"), DropDownList)
        Dim ddlnewspecialist As DropDownList = DirectCast(GridView1.FooterRow.FindControl("ddlnewspecialist"), DropDownList)

        'If Not IsPostBack Then
        '    Dim dtdoctortype As DataTable = New DataTable
        '    Dim sqldoctortype As String

        '    sqldoctortype = "SELECT jDokter, jNamaDokter, tdokter FROM H_A21504 WHERE tdokter = '" + ddlnewtdokter.SelectedValue + "'"
        '    Dim dtadoctortype As SqlDataAdapter = New SqlDataAdapter(sqldoctortype, sqlConn)
        '    dtadoctortype.Fill(dtdoctortype)
        '    ddlnewdoctortype.DataSource = dtdoctortype
        '    ddlnewdoctortype.DataTextField = "jnamadokter"
        '    ddlnewdoctortype.DataValueField = "tdokter"
        '    ddlnewdoctortype.DataBind()

        'End If

        If ddlnewtdokter.SelectedValue = "0" Then
            ddlnewdoctortype.Items.Clear()
            ddlnewdoctortype.Items.Insert(0, New ListItem("Umum", "1"))
            ddlnewdoctortype.Items.Insert(1, New ListItem("Spesialis", "2"))
            ddlnewdoctortype.Items.Insert(2, New ListItem("Gigi", "3"))
        ElseIf ddlnewtdokter.SelectedValue = "1" Then
            ddlnewdoctortype.Items.Clear()
            ddlnewdoctortype.Items.Insert(0, New ListItem("Bidan", "4"))
            ddlnewdoctortype.Items.Insert(1, New ListItem("Paramedic", "5"))
            ddlnewdoctortype.Items.Insert(2, New ListItem("Lainnya", "6"))
        End If

        If ddlnewdoctortype.SelectedItem.ToString <> "Spesialis" Then
            ddlnewspecialist.Enabled = False
            'ddlnewspecialist.SelectedValue = ""
        Else
            ddlnewspecialist.Enabled = True
        End If

        Dim dt As DataTable = Session("myDatatable")

        If dt.Rows(0)!TglDet.ToString = "" Then
            Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
            GridView1.Rows(0).Cells.Clear()
            GridView1.Rows(0).Cells.Add(New TableCell())
            GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
            GridView1.Rows(0).Cells(0).Text = "Insert medical claim"
            GridView1.Rows(0).ForeColor = Drawing.Color.White
        End If

        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y');</script>"
        cnt.Value = GridView1.FooterRow.FindControl("txtNewNama").ClientID
        cnt2.Value = GridView1.FooterRow.FindControl("hnewperson").ClientID

    End Sub

    Protected Sub ddltdokter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim ddltdokter As DropDownList = DirectCast(GridView1.FindControl("ddltdokter"), DropDownList)
        'Dim ddldoctortype As DropDownList = DirectCast(GridView1.FindControl("ddldoctortype"), DropDownList)
        'Dim ddlspecialist As DropDownList = DirectCast(GridView1.TemplateControl.FindControl("ddlspecialist"), DropDownList)
        Dim str As String
        Dim ddl As DropDownList = DirectCast(sender, DropDownList)

        str = ddl.SelectedItem.Value

        Dim row As GridViewRow = DirectCast(ddl.NamingContainer, GridViewRow)

        ' Find your control
        'Dim ddltdokter As Control = row.FindControl("ddltdokter")
        'Dim ddldoctortype As Control = row.FindControl("ddldoctortype")
        'Dim ddlspecialist As Control = row.FindControl("ddlspecialist")

        'Dim ddltdokter As DropDownList = row.FindControl("ddldoctortype")
        Dim ddldoctortype As DropDownList = row.FindControl("ddldoctortype")
        Dim ddlspecialist As DropDownList = row.FindControl("ddlspecialist")
        Dim dtdoctortype As DataTable = New DataTable
        Dim sqldoctortype As String

        If str = "0" Then
            ddldoctortype.Items.Clear()
            ddldoctortype.Items.Insert(0, New ListItem("Umum", "1"))
            ddldoctortype.Items.Insert(1, New ListItem("Spesialis", "2"))
            ddldoctortype.Items.Insert(2, New ListItem("Gigi", "3"))
        ElseIf str = "1" Then
            ddldoctortype.Items.Clear()
            ddldoctortype.Items.Insert(0, New ListItem("Bidan", "4"))
            ddldoctortype.Items.Insert(1, New ListItem("Paramedic", "5"))
            ddldoctortype.Items.Insert(2, New ListItem("Lainnya", "6"))
        Else
            ddldoctortype.Items.Clear()
        End If

        If ddldoctortype.SelectedItem.ToString <> "Spesialis" Then
            ddlspecialist.Enabled = False
            'ddlnewspecialist.SelectedValue = ""
        Else
            ddlspecialist.Enabled = True
        End If

        Dim dt As DataTable = Session("myDatatable")

        If dt.Rows(0)!TglDet.ToString = "" Then
            Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
            GridView1.Rows(0).Cells.Clear()
            GridView1.Rows(0).Cells.Add(New TableCell())
            GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
            GridView1.Rows(0).Cells(0).Text = "Insert medical claim"
            GridView1.Rows(0).ForeColor = Drawing.Color.White
        End If

        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y');</script>"
        cnt.Value = GridView1.FooterRow.FindControl("txtNewNama").ClientID
        cnt2.Value = GridView1.FooterRow.FindControl("hnewperson").ClientID
    End Sub

    Protected Sub ddlnewdoctortype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlnewtdokter As DropDownList = DirectCast(GridView1.FooterRow.FindControl("ddlnewtdokter"), DropDownList)
        Dim ddlnewdoctortype As DropDownList = DirectCast(GridView1.FooterRow.FindControl("ddlnewdoctortype"), DropDownList)
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim qry As String
        Dim dtb As DataTable = New DataTable()

        Dim ddlnewspecialist As DropDownList = DirectCast(GridView1.FooterRow.FindControl("ddlnewspecialist"), DropDownList)
        If ddlnewdoctortype.SelectedItem.ToString <> "Specialist" Then
            ddlnewspecialist.Items.Clear()
            'ddlnewspecialist.Enabled = False
            'ddlnewspecialist.SelectedValue = ""
        Else

            qry = "select kdspesialis, nmspesialis from H_A21505"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(qry, sqlConn)
            sda.Fill(dtb)
            If dtb.Rows.Count > 0 Then
                ddlnewspecialist.DataSource = dtb
                ddlnewspecialist.DataTextField = "nmspesialis"
                ddlnewspecialist.DataValueField = "kdspesialis"
                ddlnewspecialist.DataBind()
            End If
            ddlnewspecialist.Enabled = True
        End If

        Dim dt As DataTable = Session("myDatatable")

        If dt.Rows(0)!TglDet.ToString = "" Then
            Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
            GridView1.Rows(0).Cells.Clear()
            GridView1.Rows(0).Cells.Add(New TableCell())
            GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
            GridView1.Rows(0).Cells(0).Text = "Insert medical claim"
            GridView1.Rows(0).ForeColor = Drawing.Color.White
        End If
        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y');</script>"
        cnt.Value = GridView1.FooterRow.FindControl("txtNewNama").ClientID
        cnt2.Value = GridView1.FooterRow.FindControl("hnewperson").ClientID
    End Sub
    Protected Sub ddldoctortype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim str, qry As String
        Dim ddl As DropDownList = DirectCast(sender, DropDownList)
        Dim dtb As DataTable = New DataTable()
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

        str = ddl.SelectedItem.Value

        Dim row As GridViewRow = DirectCast(ddl.NamingContainer, GridViewRow)

        Dim ddldoctortype As DropDownList = row.FindControl("ddldoctortype")
        Dim ddlspecialist As DropDownList = row.FindControl("ddlspecialist")

        If str <> "2" Then
            ddlspecialist.Items.Clear()
            ddlspecialist.Items.Insert(0, New ListItem(" ", ""))
            ddlspecialist.SelectedValue = ""
        Else
            qry = "select kdspesialis, nmspesialis from H_A21505"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(qry, sqlConn)
            sda.Fill(dtb)
            If dtb.Rows.Count > 0 Then
                ddlspecialist.DataSource = dtb
                ddlspecialist.DataTextField = "nmspesialis"
                ddlspecialist.DataValueField = "kdspesialis"
                ddlspecialist.DataBind()
            End If
        End If
        js = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() },showTime: false}); cal.manageFields('btn1', '" + GridView1.FooterRow.FindControl("txtNewdt").ClientID + "', '%m/%d/%Y');</script>"
        cnt.Value = GridView1.FooterRow.FindControl("txtNewNama").ClientID
        cnt2.Value = GridView1.FooterRow.FindControl("hnewperson").ClientID
    End Sub

    Private Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim mytable As DataTable = New DataTable
        Dim dtb As DataTable = New DataTable()
        Dim licount As Integer
        Dim sqlinsertheader, lkdsite, dtmnow, dtynow, stransno, transno, createdin, sqlinsertdetail As String
        Dim sqlupdateheader, sqlupdatedetail As String
        Dim cmdupdateheader As SqlCommand
        Dim cmdupdatedetail As SqlCommand
        Dim cmdinsertheader As SqlCommand
        Dim cmdinsertdetail As SqlCommand
        Dim dtbbal As DataTable = New DataTable()
        Dim lssaldo, lsused, lsmax As String

        mytable = Session("myDatatable")
        createdin = System.Net.Dns.GetHostName().ToString

        'Dim sqlbal As String = "select '0' as saldo, (isnull(( SELECT sum( isnull(H_H21501.biayatot,0) ) FROM H_H21501, H_H215 WHERE H_H215.Tipe='0' and H_H215.fstatus='1' and H_H215.StEdit <> '2'  AND H_H215.Nik  = '" + hnik.Value + "'  and H_H215.noreg = H_H21501.noreg AND year(H_H21501.tgldet)=  YEAR(GETDATE()) group by h_h215.nik  ),0)  + "
        'sqlbal = sqlbal + " case  year(h_a000.tglmulai) when   YEAR(GETDATE()) then  isnull(h_a101.outpatientmax,0) else   isnull((select usedsaldo from h_h216 where tahun= YEAR(GETDATE()) and nik='" + hnik.Value + "' and kdsite= h_a101.kdsite),0) end) as used,"
        'sqlbal = sqlbal + " (select H_H216.outpatientmax from H_H216 where nik = '" + hnik.Value + "' and tahun= YEAR(GETDATE()) and kdsite= h_a101.kdsite) as max from h_a101,h_a000 where nik = '" + hnik.Value + "' "

        'Dim sdabal As SqlDataAdapter = New SqlDataAdapter(sqlbal, conn)
        'sdabal.Fill(dtbbal)

        'If dtbbal.Rows.Count > 0 Then
        '    lssaldo = dtbbal.Rows(0)!saldo.ToString
        '    lsused = dtbbal.Rows(0)!used.ToString
        '    lsmax = dtbbal.Rows(0)!max.ToString
        '    If lssaldo = "" Then lssaldo = "0"
        '    If lsused = "" Then lsused = "0"
        '    If lsmax = "" Then lsmax = "0"
        '    txtplafon.Text = Format(CDbl(lsmax) - (CDbl(lsused) + CDbl(lssaldo)), "#,##.00").ToString
        '    benefit = Format(CDbl(lsmax), "#,##.00").ToString
        'End If

        If txttotal.Text = "" Then txttotal.Text = 0
        If txttotal.Text <= 0 Then
            lblinfo.Text = "Total Outpatient claim value must not 0."
            msg = "<script type='text/javascript'> alert('Total Outpatient claim value must not 0'); </script>"
            refresh()
            Return
        End If

        If Session("kdsite").ToString = "" Then
            msg = "<script type='text/javascript'> alert('Cannot save data medic site code empty'); </script>"
            fillgrid()
            Return
        End If

        If mytable.Rows.Count > 0 Then
            If mytable.Rows(0)!TglDet.ToString = "" And mytable.Rows(0)!person.ToString = "" Then
                msg = "<script type='text/javascript'> alert('Cannot save data inpatient list empty'); </script>"
                fillgrid()
                Return
            Else
                If txttotal.Text <= 0 Then
                    lblinfo.Text = "Total Outpatient claim value must not 0."
                    msg = "<script type='text/javascript'> alert('Total Outpatient claim value must not 0'); </script>"
                    refresh()
                    Return
                End If

                Try
                    conn.Open()
                    If txtnoreg.Text <> "Auto" Then
                        sqlupdateheader = "Update H_H215 set Keterangan = '" + txtremarks.Text + "', modifiedby = '" + Session("otorisasi").ToString + "', modifiedin = '" + createdin + "', modifiedtime = '" + DateTime.Today.ToString + "' where noreg = '" + txtnoreg.Text + "'"
                        cmdupdateheader = New SqlCommand(sqlupdateheader, conn)
                        cmdupdateheader.ExecuteScalar()

                        Dim ls_delquery As String = "Delete H_H21501 where noreg = '" + txtnoreg.Text + "'"
                        Dim ls_delcmd As SqlCommand
                        ls_delcmd = New SqlCommand(ls_delquery, conn)
                        ls_delcmd.ExecuteScalar()

                        'For licount = 0 To mytable.Rows.Count - 1
                        '    sqlupdatedetail = "update H_H21501 set TglDet = '" + mytable.Rows(licount)!TglDet.ToString + "', Person = '" + mytable.Rows(licount)!Person.ToString + "', BiayaObat = '" + mytable.Rows(licount)!biayaobat.ToString + "', BiayaKons = '" + mytable.Rows(licount)!biayakons.ToString + "', BiayaLab = '" + mytable.Rows(licount)!biayalab.ToString + "', BiayaLain = '" + mytable.Rows(licount)!biayalain.ToString + "', BiayaTot = '" + mytable.Rows(licount)!biayatot.ToString + "', kdDiagnosa = '" + mytable.Rows(licount)!kddiagnosa.ToString + "', dokter_remark = '" + mytable.Rows(licount)!dokter_remarks.ToString + "', dremarks = '" + mytable.Rows(licount)!dremarks.ToString + "' where NoReg = '" + txtnoreg.Text + "'"
                        '    cmdupdatedetail = New SqlCommand(sqlupdatedetail, conn)
                        '    cmdupdatedetail.ExecuteScalar()
                        'Next

                        For licount = 0 To mytable.Rows.Count - 1
                            sqlinsertdetail = "Insert into H_H21501 (NoReg, TglDet, Person, PersenClaim, BiayaObat, BiayaKons, BiayaLab, BiayaLain, BiayaTot, tdokter, jdokter, kdspesialis, kdDiagnosa, dremarks, dokter_remark) Values ('" + txtnoreg.Text + "', '" + mytable.Rows(licount)!TglDet.ToString + "', '" + mytable.Rows(licount)!Person.ToString + "', 1  , '" + mytable.Rows(licount)!biayaobat.ToString + "', '" + mytable.Rows(licount)!biayakons.ToString + "', '" + mytable.Rows(licount)!biayalab.ToString + "', '" + mytable.Rows(licount)!biayalain.ToString + "', '" + mytable.Rows(licount)!biayatot.ToString + "', '" + mytable.Rows(licount)!htdokter.ToString + "', '" + mytable.Rows(licount)!jdokter.ToString + "', '" + mytable.Rows(licount)!kdspesialis.ToString + "', '" + mytable.Rows(licount)!kddiagnosa.ToString.Replace("'", "") + "', '" + mytable.Rows(licount)!dremarks + "', '" + mytable.Rows(licount)!dokter_remarks + "')"
                            cmdinsertdetail = New SqlCommand(sqlinsertdetail, conn)
                            cmdinsertdetail.ExecuteScalar()
                        Next
                        'btnsave.Enabled = False

                        If CDbl(txtplafon.Text) - CDbl(txttotal.Text) < 0 Then
                            hprint.Value = "1"
                        Else
                            hprint.Value = "0"
                        End If

                        sqlupdateheader = "Update H_H215 set fprint = '" + hprint.Value + "' where noreg = '" + txtnoreg.Text + "'"
                        cmdupdateheader = New SqlCommand(sqlupdateheader, conn)
                        cmdupdateheader.ExecuteScalar()

                        If hprint.Value = "1" Then
                            btnprint.Enabled = False
                            btnPaymentUsage.Enabled = False
                            msg = "<script type='text/javascript'> alert('Information : Your remaining balance is 0. Please contact HR for further assistance.'); </script>"
                            lblinfo.Text = "Cannot process. Your remaining balance is 0. Please contact HR for further assistance."
                        Else
                            btnprint.Enabled = True
                            If Session("kdsite") = "JKT" Or kodeSite = "JKT" Then
                                btnPaymentUsage.Enabled = True
                            End If

                            msg = "<script type='text/javascript'> alert('Data has been update'); </script>"
                        End If

                    Else
                        If txtnoreg.Text = "Auto" Then
                            dtmnow = DateTime.Now.ToString("MM")
                            dtynow = Date.Now.Year.ToString

                            stransno = Session("kdsite") + "/" + dtynow + "/" + dtmnow + "/" + "0" + "/"

                            Dim strcon As String = "select Noreg from H_H215 where Noreg like '%" + stransno + "%'"
                            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, conn)
                            sda.Fill(dtb)

                            If dtb.Rows.Count = 0 Then
                                transno = stransno + "000001"
                            ElseIf dtb.Rows.Count > 0 Then
                                I = 0
                                I = dtb.Rows.Count + 1
                                If dtb.Rows.Count > 0 And dtb.Rows.Count < 9 Then
                                    transno = stransno + "00000" + I.ToString
                                ElseIf dtb.Rows.Count >= 9 And dtb.Rows.Count < 99 Then
                                    transno = stransno + "0000" + I.ToString
                                ElseIf dtb.Rows.Count >= 99 And dtb.Rows.Count < 999 Then
                                    transno = stransno + "000" + I.ToString
                                ElseIf dtb.Rows.Count >= 999 And dtb.Rows.Count < 9999 Then
                                    transno = stransno + "00" + I.ToString
                                ElseIf dtb.Rows.Count >= 9999 And dtb.Rows.Count < 99999 Then
                                    transno = stransno + "0" + I.ToString
                                ElseIf dtb.Rows.Count >= 99999 And dtb.Rows.Count < 999999 Then
                                    transno = stransno + I.ToString
                                ElseIf dtb.Rows.Count >= 999999 Then
                                    transno = "Error on generate Noreg"
                                End If
                            End If

                            sqlinsertheader = "Insert into H_H215 (NoReg, KdSite, TglTrans, Tipe, Nik, fstatus, Keterangan, CreatedBy, CreatedIn, CreatedTime, StEdit, pycostcode) Values ('" + transno + "', '" + hnmsite.Value + "', '" + DateTime.Today.ToString + "', 0, '" + hnik.Value + "', 0, '" + txtremarks.Text + "', '" + Session("otorisasi").ToString + "', '" + createdin + "', '" + DateTime.Today.ToString + "', 0,'" + txtcostcode.Text + "')"
                            cmdinsertheader = New SqlCommand(sqlinsertheader, conn)
                            cmdinsertheader.ExecuteScalar()

                            For licount = 0 To mytable.Rows.Count - 1
                                sqlinsertdetail = "Insert into H_H21501 (NoReg, TglDet, Person, PersenClaim, BiayaObat, BiayaKons, BiayaLab, BiayaLain, BiayaTot, tdokter, jdokter, kdspesialis, kdDiagnosa, dremarks, dokter_remark) Values ('" + transno + "', '" + mytable.Rows(licount)!TglDet.ToString + "', '" + mytable.Rows(licount)!Person.ToString + "', 1  , '" + mytable.Rows(licount)!biayaobat.ToString + "', '" + mytable.Rows(licount)!biayakons.ToString + "', '" + mytable.Rows(licount)!biayalab.ToString + "', '" + mytable.Rows(licount)!biayalain.ToString + "', '" + mytable.Rows(licount)!biayatot.ToString + "', '" + mytable.Rows(licount)!htdokter.ToString + "', '" + mytable.Rows(licount)!jdokter.ToString + "', '" + mytable.Rows(licount)!kdspesialis.ToString + "', '" + mytable.Rows(licount)!kddiagnosa.ToString.Replace("'", "") + "', '" + mytable.Rows(licount)!dremarks + "', '" + mytable.Rows(licount)!dokter_remarks.ToString + "')"
                                cmdinsertdetail = New SqlCommand(sqlinsertdetail, conn)
                                cmdinsertdetail.ExecuteScalar()
                            Next
                            btnsave.Enabled = False
                            btnprint.Enabled = True
                            If Session("kdsite") = "JKT" Or kodeSite = "JKT" Then
                                btnPaymentUsage.Enabled = True
                            End If
                            txtnoreg.Text = transno

                            If CDbl(txtplafon.Text) - CDbl(txttotal.Text) < 0 Then
                                hprint.Value = "1"
                            Else
                                hprint.Value = "0"
                            End If

                            sqlupdateheader = "Update H_H215 set fprint = '" + hprint.Value + "' where noreg = '" + txtnoreg.Text + "'"
                            cmdupdateheader = New SqlCommand(sqlupdateheader, conn)
                            cmdupdateheader.ExecuteScalar()

                            If hprint.Value = "1" Then
                                btnprint.Enabled = False
                                btnPaymentUsage.Enabled = False
                                msg = "<script type='text/javascript'> alert('Information : Your remaining balance is 0. Please contact HR for further assistance.'); </script>"
                                lblinfo.Text = "Cannot process. Your remaining balance is 0. Please contact HR for further assistance."
                            Else
                                btnprint.Enabled = True
                                If Session("kdsite") = "JKT" Or kodeSite = "JKT" Then
                                    btnPaymentUsage.Enabled = True
                                End If
                                msg = "<script type='text/javascript'> alert('Data has been save'); </script>"
                            End If
                        Else
                            msg = "<script type='text/javascript'> alert('Failed to save data'); </script>"
                        End If

                    End If
                Catch ex As Exception
                    msg = "<script type='text/javascript'> alert('Failed to save data'); </script>"
                End Try
            End If
        Else
            msg = "<script type='text/javascript'> alert('Cannot save data inpatient list empty'); </script>"
        End If
    End Sub

    Private Sub btnprint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnprint.Click
        Session("printinoutpatient") = txtnoreg.Text
        msg = ""
        Response.Redirect("cetakoutpatient.aspx?tipe=0")
    End Sub

    Private Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Dim sqlupdate As String
        Dim cmdupdate As SqlCommand
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Try
            conn.Open()
            sqlupdate = "update H_H215 set stedit = 2, deleteby = '" + Session("otorisasi").ToString + "', Deletetime = '" + DateTime.Today.ToString + "' where noreg = '" + txtnoreg.Text + "'"
            cmdupdate = New SqlCommand(sqlupdate, conn)
            cmdupdate.ExecuteScalar()

            Response.Redirect("medic_patientlist.aspx")
        Catch ex As Exception

        Finally
            conn.Close()
        End Try

    End Sub

    Public Sub refresh()
        If GridView1.Rows(0).Cells(0).Text = "Insert medical claim" Then
            Dim TotalColumns As Integer = GridView1.Rows(0).Cells.Count
            GridView1.Rows(0).Cells.Clear()
            GridView1.Rows(0).Cells.Add(New TableCell())
            GridView1.Rows(0).Cells(0).ColumnSpan = TotalColumns
            GridView1.Rows(0).Cells(0).Text = "Insert medical claim"
            GridView1.Rows(0).ForeColor = Drawing.Color.White
        End If
    End Sub

    Protected Sub btnPaymentUsage_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPaymentUsage.Click

        Dim user As ESS.DataAccess.EmployeeModel = ESS.DataAccess.UserLogic.GetUserByNIKSite(txtnik.Text)
        Session("payment_usage") = String.Format("{0}|{1}|{2}|{3}|{4}", txtnoreg.Text, txttotal.Text, "Outpatient Medical Claim", DateTime.Now.ToString("dd MMM yyyy"), user.Nik)

        msg = ""
        Response.Redirect("payment_application_internal_form.aspx")
    End Sub
End Class