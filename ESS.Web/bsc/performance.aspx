﻿<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/site.master" CodeBehind="performance.aspx.vb" Inherits="EXCELLENT.performance" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
      table.act {
	      border-width: 1px;
	      border-spacing: 0px;
	      border-color: #C0C0C0;
	      border-collapse: collapse;
	      background-color: white;
         }
      table.act td {
	      border-width: 1px;
	      padding: 1px;
	      border-color: #C0C0C0;
      }
      ul.dropdown *.dir1 {
          padding-right: 20px;
          background-image: url("/memo/css/dropdown/themes/default/images/nav-arrow-down.png");
          background-position: 100% 50%;
          background-repeat: no-repeat;
          background-color: #FFA500;
         }
          input.btn{
        color:#050;
        font: bold 90% 'trebuchet ms',helvetica,sans-serif;
        background-color:#fed;
        border: 1px solid;
        border-color: #696 #363 #363 #696;
        filter:progid:DXImageTransform.Microsoft.Gradient
        (GradientType=0,StartColorStr='#ffffffff',EndColorStr='#ffeeddaa');
        padding:0 10px 0 10px;
        cursor:hand;
       }
        select.plains{
         font-family:"trebuchet MS", verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }  
      #result{
       font-family:"trebuchet MS", verdana, sans-serif;
      }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
   <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>

    <table width="100%">
    <tr>
      <td width="1%">Tahun</td>
      <td width="99%">
          <asp:DropDownList ID="ddlYear" runat="server" CssClass="plains">
              <asp:ListItem>2010</asp:ListItem>
              <asp:ListItem>2011</asp:ListItem>
              <asp:ListItem>2012</asp:ListItem>
          </asp:DropDownList>
      </td>
    </tr>
    <tr>
      <td>Jenis</td>
      <td>
          <asp:DropDownList ID="ddlJenis" runat="server" AutoPostBack="true" CssClass="plains">
              <asp:ListItem Value="0">Performance JRN & All Jobsite</asp:ListItem>
              <asp:ListItem Value="4">Performance Direktorat</asp:ListItem>
              <asp:ListItem Value="1" enabled="false">>Performance All Functions</asp:ListItem>
              <asp:ListItem Value="2" enabled="false">Performance Jobsite</asp:ListItem>
              <asp:ListItem Value="3">Performance Function</asp:ListItem>
          </asp:DropDownList>
          <asp:DropDownList ID="KPIJobsite" runat="server" Visible="False" CssClass="plains"></asp:DropDownList>
          <asp:DropDownList ID="KPIFunc" runat="server" Visible="False" CssClass="plains"></asp:DropDownList>
          <asp:DropDownList ID="KPIDirektorat" runat="server" Visible="False" CssClass="plains"></asp:DropDownList>
          </td>
          </tr><tr>
          <td colspan="2">
         <asp:Button ID="Button1" runat="server" Text="View" class="btn" />
      </td>
    </tr>
    <tr>
    <td colspan="2">
      <div id="result" runat="server"></div>
    </td>
    </tr>
    </table>
    </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="progress1" runat="server" DisplayAfter="50">    
         <ProgressTemplate>
         <asp:Panel ID="pnlProgressBar" runat="server">    
            <div><asp:Image ID="Image1" runat="Server" ImageUrl="../images/ajax-loader.gif"/>Harap Sabar Menunggu...</div>    
         </asp:Panel>   
         </ProgressTemplate> 
         </asp:UpdateProgress>
    </div>
    <script type="text/javascript">
       function fnprint() {
         document.getElementById('btnprint').style.visibility = 'hidden';
         var pp = window.open();
         pp.document.writeln('<HTML><HEAD><title>Print Preview</title><style type="text/css">');
         pp.document.writeln('table.act {');
	      pp.document.writeln('border-width: 1px;');
	      pp.document.writeln('border-spacing: 0px;');
	      pp.document.writeln('border-color: #C0C0C0;');
	      pp.document.writeln('border-collapse: collapse;');
	      pp.document.writeln('background-color: white;');
         pp.document.writeln('}');
         pp.document.writeln('table.act td {');
	      pp.document.writeln('border-width: 1px;');
	      pp.document.writeln('padding: 1px;');
	      pp.document.writeln('border-color: #C0C0C0;');
	      pp.document.writeln('}');
	      pp.document.writeln('</style></HEAD>');
//	      switch (document.getElementById('ctl00_ContentPlaceHolder1_ddlJenis').value) {
//	         case '0':
//	            pp.document.writeln('<div align=\'center\' style=\'background-color:#90D050\'>' + '<b>' + document.getElementById('ctl00_ContentPlaceHolder1_ddlJenis').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlJenis').selectedIndex].text + ' Tahun ' + document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').selectedIndex].text + '</b> ' + '</div>');
//	            break;
//	         case '1':
//	            pp.document.writeln('<div align=\'center\' style=\'background-color:#90D050\'>' + '<b>' + document.getElementById('ctl00_ContentPlaceHolder1_ddlJenis').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlJenis').selectedIndex].text + ' Tahun ' + document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').selectedIndex].text + '</b> ' + '</div>');
//	            break;
//	         case '2':
//	            pp.document.writeln('<div align=\'center\' style=\'background-color:#90D050\'>' + '<b>' + document.getElementById('ctl00_ContentPlaceHolder1_ddlJenis').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlJenis').selectedIndex].text + ' ' + document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').selectedIndex].text + ' Tahun ' + document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').selectedIndex].text + '</b> ' + '</div>');
//	            break;
//	         case '3':
//	            pp.document.writeln('<div align=\'center\' style=\'background-color:#90D050\'>' + '<b>' + document.getElementById('ctl00_ContentPlaceHolder1_ddlJenis').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlJenis').selectedIndex].text + ' ' + document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').options[document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').selectedIndex].text + ' Tahun ' + document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').selectedIndex].text + '</b> ' + '</div>');
//	            break;
//	      }
	      
         //pp.document.writeln('<b>' + document.getElementById('ctl00_ContentPlaceHolder1_ddlJenis').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlJenis').selectedIndex].text + ' Tahun ' + document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').options[document.getElementById('ctl00_ContentPlaceHolder1_ddlYear').selectedIndex].text + '</b><br/>');
         pp.document.writeln(document.getElementById('ctl00_ContentPlaceHolder1_result').innerHTML);
         pp.document.writeln('<\HTML>');
         document.getElementById('btnprint').style.visibility = 'visible';
       }
    </script>
</asp:Content>
