﻿Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.XPath
Imports System.Net
Imports compDocCenter
Imports compDocCenter.cDoc

Partial Public Class findpic
    Inherits System.Web.UI.Page
    Public nmurl As String
    Public nmfile As String
    Public list As String
    Public filelist As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Trim(SearchKey.Text) <> "" Then
                LoadData(False)
            End If
        End If
    End Sub

    Public Property PageNumber() As Integer
        Get
            If ViewState("PageNumber") IsNot Nothing Then
                Return Convert.ToInt32(ViewState("PageNumber"))
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PageNumber") = value
        End Set
    End Property

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        If (SearchKey.Text.Length < 3) Then
        Else
            filelist = ""
            LoadData(True)
        End If
    End Sub

    Function LoadData(ByVal bool As Boolean)
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        'Try
        '    Dim dt As New DataTable
        '    If bool Then
        '        Dim myCMD As SqlCommand = New SqlCommand("select pic_code, pic_name, pic_email from docpermit_pic where pic_name like '%' + @Nama + '%'", sqlConn)

        '        Dim param As New SqlParameter()
        '        param.ParameterName = "@Nama"
        '        param.Value = SearchKey.Text
        '        myCMD.Parameters.Add(param)
        '        sqlConn.Open()
        '        Dim reader As SqlDataReader = myCMD.ExecuteReader()
        '        dt.Load(reader, LoadOption.OverwriteChanges)
        '        sqlConn.Close()
        '        ViewState("dt") = dt
        '        PageNumber = 0
        '    End If
        '    Dim pgitems As New PagedDataSource()
        '    Dim dv As New DataView(ViewState("dt"))
        '    pgitems.DataSource = dv
        '    pgitems.AllowPaging = True
        '    pgitems.PageSize = 15
        '    pgitems.CurrentPageIndex = PageNumber

        '    If pgitems.PageCount > 1 Then
        '        rptPages.Visible = True
        '        Dim pages As New ArrayList()
        '        For i As Integer = 0 To pgitems.PageCount - 1
        '            pages.Add((i + 1).ToString())
        '        Next
        '        rptPages.DataSource = pages
        '        rptPages.DataBind()
        '    Else
        '        rptPages.Visible = False
        '    End If
        '    rptResult.DataSource = pgitems
        '    rptResult.DataBind()
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'Finally
        '    If sqlConn.State = ConnectionState.Open Then
        '        sqlConn.Close()
        '    End If
        'End Try
        'Dim urlstr As String
        'Dim node As XmlNode
        'Dim myXmlDocument As XmlDocument = New XmlDocument()
        'Dim myCache As New CredentialCache()

        ''myCache.Add(New Uri("\\files.jresources.com\Permit Documents\"), "Basic", New NetworkCredential("syam.kharisman", "356356", "jresources"))

        'If Trim(Session("dept")) = "Non-Department Information Technology" Then
        '    myXmlDocument.Load("C:\inetpub\wwwroot\HRMS\doc\list.xml")
        'ElseIf Trim(Session("dept")) = "Reward & Workforce Plan" Or Trim(Session("dept")) = "Organization Development" Then
        '    myXmlDocument.Load("C:\inetpub\wwwroot\HRMS\doc\hrlist.xml")
        'Else
        '    'departmen tidak ada
        '    Exit Function
        'End If

        ''myXmlDocument.Load("../wwwroot/HRMS/permit/list.xml")
        ''myXmlDocument.Load("C:\inetpub\wwwroot\HRMS\permit\list.xml")
        'node = myXmlDocument.DocumentElement

        'Dim node2 As XmlNode

        'For Each node In node.ChildNodes
        '    For Each node2 In node.ChildNodes
        '        If node2.Name = "Row" Then
        '            'list = node2.InnerText
        '            If node2.OuterXml.ToLower.Contains(".pdf") Or node2.OuterXml.ToLower.Contains(".docx") Or _
        '            node2.OuterXml.ToLower.Contains(".zip") Or node2.OuterXml.ToLower.Contains(".rar") Or node2.OuterXml.ToLower.Contains(".xls") _
        '            Or node2.OuterXml.ToLower.Contains(".xlsx") Or node2.OuterXml.ToLower.Contains(".doc") Or node2.OuterXml.ToLower.Contains(".jpg") _
        '                Or node2.OuterXml.ToLower.Contains(".png") Then
        '                urlstr = Replace(node2.OuterXml, "<Row ows_EncodedAbsUrl=", "")
        '                urlstr = Replace(urlstr, "ows_LinkFilename=", "")
        '                urlstr = Replace(urlstr, " />", "")
        '                urlstr = Replace(urlstr, """", "")
        '                'urlstr = Mid(urlstr, 1, urlstr.IndexOf(" "))
        '                'urlstr = Mid(urlstr, urlstr.IndexOf(" ") + 1, urlstr.Length)
        '                If urlstr.ToLower.Contains(SearchKey.Text) Then
        '                    nmurl = Mid(urlstr, 1, urlstr.IndexOf(" "))
        '                    nmfile = Mid(urlstr, urlstr.IndexOf(" ") + 1, urlstr.Length)
        '                    list = list + "<tr><td><a href='#' onclick=""UseSelected('" + nmfile + "','" + nmurl + "');"">" + nmfile + "</a></td></tr>"
        '                End If
        '            End If
        '        End If
        '    Next
        'Next
        Dim olist As Microsoft.SharePoint.Client.ListItemCollection
        Dim cclass As cDoc = New cDoc()
        Dim depar As String
        Dim depurl As String
        Dim depfolder As String
        Dim irowcount As Integer

        Dim qrydocmod As String = "select modul from ms_user where emp_code = '" + Session("otorisasi_permit") + "'"
        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()
            Dim sda As SqlDataAdapter = New SqlDataAdapter(qrydocmod, sqlConn)
            sda.Fill(dtb)

            depar = dtb.Rows(0)!modul.ToString
            Session("depar") = depar
        Catch ex As Exception
        Finally
            sqlConn.Close()
        End Try

        If depar = "IT" Then
            depurl = "http://moss.jresources.com/IT"
            depfolder = "/IT"
        ElseIf depar = "HR" Then
            depurl = "http://moss.jresources.com/hr"
            depfolder = "/hr"
        ElseIf depar = "HRCA" Then
            depurl = "http://moss.jresources.com/HRCA"
            depfolder = "/HRCA"
        ElseIf depar = "Legal" Then
            depurl = "http://moss.jresources.com/legal"
            depfolder = "/legal"
        ElseIf depar = "Plant" Then
            depurl = "http://moss.jresources.com/plant"
            depfolder = "/plant"
        ElseIf depar = "FINACC" Then
            depurl = "http://moss.jresources.com/finacc"
            depfolder = "/finacc"
        ElseIf depar = "IAS" Then
            depurl = "http://moss.jresources.com/ias"
            depfolder = "/ias"
        ElseIf depar = "Finance" Then
            depurl = "http://moss.jresources.com/finance"
            depfolder = "/finance"
        ElseIf depar = "Accounting" Then
            depurl = "http://moss.jresources.com/accounting"
            depfolder = "/accounting"
        ElseIf depar = "Exploration" Then
            depurl = "http://moss.jresources.com/exploration"
            depfolder = "/exploration"
        ElseIf depar = "techdev" Then
            depurl = "http://moss.jresources.com/techdev"
            depfolder = "/techdev"
        End If

        Dim qryflderlist As String = "select modul, doc_folder from ms_folder where modul = '" + depar + "'"
        Try
            sqlConn.Open()
            Dim dtbflderlist As DataTable = New DataTable()
            Dim sdaflderlist As SqlDataAdapter = New SqlDataAdapter(qryflderlist, sqlConn)
            sdaflderlist.Fill(dtbflderlist)

            If dtbflderlist.Rows.Count > 0 Then
                For irowcount = 0 To dtbflderlist.Rows.Count - 1
                    cclass.vGetListDocument("Syam.kharisman", "sysyam356", "jresources.com", depurl, dtbflderlist.Rows(irowcount)!doc_folder.ToString, olist, depfolder)

                    For Each list As Microsoft.SharePoint.Client.ListItem In olist
                        If list("FileLeafRef").ToString.ToLower.Contains(SearchKey.Text) Then
                            filelist = filelist + "<tr><td><a href='#' onclick=""UseSelected('" + list("FileLeafRef") + "','" + list("FileRef") + "');"">" + list("FileLeafRef") + "</a></td><td>" + list("FileRef").ToString + "</a></td></tr>"
                        End If
                        'AddDataToTable(I, list("FileLeafRef"), list("FileRef"), CType(Session("myDatatable"), DataTable))
                    Next
                Next

                If filelist = Nothing Or filelist = "" Then
                    filelist = "<tr><td colspan = '2'><br><strong style='color:red'>0 Result</strong> For " + SearchKey.Text + "</td></tr>"
                End If
            End If

        Catch ex As Exception
        Finally
            sqlConn.Close()
        End Try

    End Function

    Private Sub rptPages_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPages.ItemCommand
        PageNumber = Convert.ToInt32(e.CommandArgument) - 1
        LoadData(False)
    End Sub
End Class