﻿Imports System.Data.SqlClient

Partial Public Class PopUpAddSecurity
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Trim(SearchKey.Text) <> "" Then
                LoadData(False)
            End If
        End If
    End Sub

    Public Property PageNumber() As Integer
        Get
            If ViewState("PageNumber") IsNot Nothing Then
                Return Convert.ToInt32(ViewState("PageNumber"))
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PageNumber") = value
        End Set
    End Property

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        If (SearchKey.Text.Length < 3) Then
        Else
            LoadData(True)
        End If
    End Sub

    Function LoadData(ByVal bool As Boolean)
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
        Try
            Dim dt As New DataTable
            If bool Then
                'JIKA LEVEL E-Up MAKA AMBIL KODE LOKASI DARI H_A101 HRIS
                'SEDANGKAN D-Down AMBIL KODE JOBSITE DARI H_A101 HRIS

                'Dim myCMD As SqlCommand = New SqlCommand("SELECT Nik,Nama,H_A101.KdDepar,NmDepar,KdSite = case KdLevel when 5 Then KdLokasi when 6 Then KdLokasi When 7 Then KdLokasi else KdSite end FROM H_A101 LEFT JOIN H_A130 ON H_A101.KdDepar = H_A130.KdDepar WHERE Active = '1' AND (Nama like + '%' + @Nama + '%' OR Nik like + '%' + @Nama + '%') ", sqlConn)
                'Dim myCMD As SqlCommand = New SqlCommand("SELECT PERNR as NIK,Cname as Nama,ZDEP as NmDepar,WERKS_TEXT as KdSite FROM H_A101_SAP WHERE PERSG = '1' AND (CName like '%' + @Nama + '%' OR CName like '%' + @Nama + '%') ", sqlConn)
                Dim myCMD As SqlCommand = New SqlCommand("SELECT Nik,Nama,H_A101.KdDepar,NmDepar,KdSite  FROM H_A101 LEFT JOIN H_A130 ON H_A101.KdDepar = H_A130.KdDepar WHERE Active = '1' AND (Nama like + '%' + @Nama + '%' OR Nik like + '%' + @Nama + '%') ", sqlConn)

                Dim param As New SqlParameter()
                param.ParameterName = "@Nama"
                param.Value = SearchKey.Text
                myCMD.Parameters.Add(param)
                sqlConn.Open()
                Dim reader As SqlDataReader = myCMD.ExecuteReader()
                dt.Load(reader, LoadOption.OverwriteChanges)
                sqlConn.Close()
                ViewState("dt") = dt
                PageNumber = 0
            End If
            Dim pgitems As New PagedDataSource()
            Dim dv As New DataView(ViewState("dt"))
            pgitems.DataSource = dv
            pgitems.AllowPaging = True
            pgitems.PageSize = 10
            pgitems.CurrentPageIndex = PageNumber

            If pgitems.PageCount > 1 Then
                rptPages.Visible = True
                Dim pages As New ArrayList()
                For i As Integer = 0 To pgitems.PageCount - 1
                    pages.Add((i + 1).ToString())
                Next
                rptPages.DataSource = pages
                rptPages.DataBind()
            Else
                rptPages.Visible = False
            End If
            rptResult.DataSource = pgitems
            rptResult.DataBind()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            If sqlConn.State = ConnectionState.Open Then
                sqlConn.Close()
            End If
        End Try
    End Function

    Private Sub rptPages_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPages.ItemCommand
        PageNumber = Convert.ToInt32(e.CommandArgument) - 1
        LoadData(False)
    End Sub
End Class