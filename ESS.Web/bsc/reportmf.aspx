﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="reportmf.aspx.vb"
   Inherits="EXCELLENT.reportmf" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
   Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
   <title></title>
   <style type="text/css">
      ul.dropdown *.dir1
      {
         padding-right: 20px;
         background-image: url("/remo/css/dropdown/themes/default/images/nav-arrow-down.png");
         background-position: 100% 50%;
         background-repeat: no-repeat;
         background-color: #FFA500;
      }
      input.btn
      {
         color: #050;
         font: bold 90% 'trebuchet ms' ,helvetica,sans-serif;
         background-color: #fed;
         border: 1px solid;
         border-color: #696 #363 #363 #696;
         filter: progid:DXImageTransform.Microsoft.Gradient (GradientType=0,StartColorStr= '#ffffffff' ,EndColorStr= '#ffeeddaa' );
         padding: 0 10px 0 10px;
         cursor: hand;
      }
      select.plains
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }
      input.plain
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }
      .plain:hover
      {
         background-color: #FFFFA8;
      }
   </style>

   <script type="text/javascript">
      function print() {
         if (document.getElementById("kpiid").value != "") {
            window.open('printkpi.aspx?id=' + document.getElementById("kpiid").value, 'Print');
         } else {
            alert('Harap Cari KPI yang ingin di-View');
         }
      }

      function openSearchModal() {
         var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPILevel');
         var selIndex = selObj.selectedIndex;
         var yearObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIYear');
         var yearIndex = yearObj.selectedIndex;
         if (selObj.options[selIndex].value == '1') {
            var funcObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc');
            var funcselIndex = funcObj.selectedIndex;
            var returnVal = window.showModalDialog('PopUpSearch.aspx?y=' + yearObj.options[yearIndex].value + '&type=' + selObj.options[selIndex].value + '&func=' + funcObj.options[funcselIndex].value + '&wtr=mf', '', 'dialogWidth=300px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         } else if (selObj.options[selIndex].value == '2') {
            var secObj = document.getElementById('ctl00_ContentPlaceHolder1_KPISection');
            var secselIndex = secObj.selectedIndex;
            var siteObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite');
            var siteselIndex = siteObj.selectedIndex;
            var returnVal = window.showModalDialog('PopUpSearch.aspx?y=' + yearObj.options[yearIndex].value + '&type=' + selObj.options[selIndex].value + '&jobsite=' + siteObj.options[siteselIndex].value + '&sec=' + secObj.options[secselIndex].value + '&wtr=mf', '', 'dialogWidth=300px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         } else if (selObj.options[selIndex].value == '3') {
            var siteObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite');
            var siteselIndex = siteObj.selectedIndex;
            var returnVal = window.showModalDialog('PopUpSearch.aspx?y=' + yearObj.options[yearIndex].value + '&type=' + selObj.options[selIndex].value + '&jobsite=' + siteObj.options[siteselIndex].value + '&wtr=mf', '', 'dialogWidth=300px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         } else if (selObj.options[selIndex].value == '4') {
            var dirObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIDirektorat');
            var dirselIndex = dirObj.selectedIndex;
            var returnVal = window.showModalDialog('PopUpSearch.aspx?y=' + yearObj.options[yearIndex].value + '&type=' + selObj.options[selIndex].value + '&dir=' + dirObj.options[dirselIndex].value + '&wtr=mf', '', 'dialogWidth=300px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         }else {
            var returnVal = window.showModalDialog('PopUpSearch.aspx?y=' + yearObj.options[yearIndex].value + '&type=' + selObj.options[selIndex].value + '&wtr=mf', '', 'dialogWidth=300px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         }
         if (returnVal != undefined) {
            document.getElementById("kpiid").value = returnVal.split("|")[0];
            document.getElementById("kpiname").value = returnVal.split("|")[1];
         }
      }
   </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePartialRendering="false">
   </asp:ToolkitScriptManager>
   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
      <ContentTemplate>
         <div>
            <table>
               <tr>
                  <td>
                     Tahun KPI
                  </td>
                  <td colspan="3">
                     <asp:DropDownList ID="KPIYear" runat="server" CssClass="plains">
                        <asp:ListItem Value="2011" Text="2011"></asp:ListItem>
                        <asp:ListItem Value="2012" Text="2012"></asp:ListItem>
                        <asp:ListItem Value="2013" Text="2013"></asp:ListItem>
                        <asp:ListItem Value="2014" Text="2014"></asp:ListItem>
                        <asp:ListItem Value="2015" Text="2015"></asp:ListItem>
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td>
                     KPI Level
                  </td>
                  <td>
                     <asp:DropDownList ID="KPILevel" runat="server" AutoPostBack="true" CssClass="plains">
                        <asp:ListItem Value="0" Text="JRN"></asp:ListItem>                        
                        <asp:ListItem Value="4" Text="Direktorat"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Function"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Section"></asp:ListItem>
                        <asp:ListItem Value="3" Text="Jobsite"></asp:ListItem>
                     </asp:DropDownList>
                     <asp:DropDownList ID="KPIDirektorat" runat="server" CssClass="plains">
                     </asp:DropDownList>
                     <asp:DropDownList ID="KPIFunc" runat="server" Visible="False" CssClass="plains">
                     </asp:DropDownList>
                     <asp:DropDownList ID="KPISection" runat="server" Visible="False" CssClass="plains">
                     </asp:DropDownList>
                     <asp:DropDownList ID="KPIJobsite" runat="server" Visible="False" CssClass="plains">
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td>
                     KPI
                  </td>
                  <td>
                     <input type="hidden" id="kpiid" />
                     <input type="text" id="kpiname" size="35" class="plain" readonly />
                     <input type="button" id="btnSearch" onclick="openSearchModal();" style="background-image: url(../images/Search.gif);
                        background-repeat: no-repeat; cursor: hand; vertical-align: top; width: 20px;
                        height: 20px; background-color: transparent; border: 0" />
                  </td>
               </tr>
               <tr>
                  <td colspan="2">
                     <input type="button" value="View" onclick="print()" class="btn" />
                  </td>
               </tr>
            </table>
            <asp:Label ID="lblMessage" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="#FF3300"></asp:Label>
            <br />
         </div>
      </ContentTemplate>
   </asp:UpdatePanel>
</asp:Content>
