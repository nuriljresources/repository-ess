﻿Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms

Partial Public Class PrintIssue
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("NikUser") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        Try
            Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
            Dim conn As New SqlConnection(arg)
            conn.Open()

            Dim dTabel As New Excellent.LISTISUDataTable()

            Dim Periode As String = ""
            Dim Jobsite As String = ""
            Dim StsIsu As String = ""
            Dim Over As String = ""
            Dim Dep As String = ""
            Dim Jns As String = ""

            Dim query As String = ""
            If Request.QueryString("jnslap") = "1" Then
                query = getString()
                Periode = "ALL"
                Jobsite = "ALL"
                StsIsu = "ALL"
                Over = "ALL"
                Dep = "ALL"
                Jns = "ALL"
            Else
                query = getStringOpsi()

                Dim stg1 As String = Request.QueryString("tg1")
                Dim stg2 As String = Request.QueryString("tg2")
                Dim job As String = Request.QueryString("job")
                Dim sts As String = Request.QueryString("sts")
                Dim overdue As String = Request.QueryString("over")
                Dim dpr As String = Request.QueryString("dpr")
                Dim jnsmtg As String = Request.QueryString("jnsmtg")

                Periode = stg1 + " - " + stg2
                Jobsite = IIf(job = "", "ALL", job)
                StsIsu = IIf(sts = "", "ALL", sts)
                Over = IIf(overdue = "", "ALL", overdue)
                Dep = IIf(dpr = "", "ALL", dpr)
                Jns = IIf(jnsmtg = "", "ALL", IIf(jnsmtg = "0", "ALL", IIf(jnsmtg = "1", "Weekly Review Jobsite", IIf(jnsmtg = "2", "Monthly Review Jobsite", _
                        IIf(jnsmtg = "3", "Weekly Internal Functional", IIf(jnsmtg = "4", "Weekly Management Meeting", IIf(jnsmtg = "5", "Monthly Management Meeting", "Others")))))))
            End If

            Dim cmd As SqlCommand = New SqlCommand(query, conn)
            Dim sdr As SqlDataReader = cmd.ExecuteReader()

            Dim dr As DataRow
            Dim i As Int16 = 0
            While sdr.Read
                i = i + 1
                dr = dTabel.NewRow()
                dr(0) = sdr(0)
                dr(1) = sdr(1)
                dr(2) = sdr(2)
                dr(3) = sdr(3)
                dr(4) = sdr(4)
                dr(5) = sdr(5)
                dr(6) = sdr(6)
                dr(7) = sdr(7)
                dr(8) = sdr(8)
                dr(9) = sdr(9)
                dr(10) = GetPIC(sdr(0), sdr(1))
                dr(11) = GetInfoTo(sdr(0), sdr(1))
                dr(12) = sdr(10)
                dr(13) = sdr(11)
                dr(14) = GetLastProgress(sdr(0), sdr(1), sdr(3))

                dTabel.Rows.Add(dr)
            End While

            Dim rSource As ReportDataSource = New ReportDataSource("Excellent_LISTISU", dTabel)
            ReportViewer.ProcessingMode = ProcessingMode.Local
            ReportViewer.LocalReport.ReportPath = "Laporan\Issue.rdlc"
            ReportViewer.LocalReport.DataSources.Add(rSource)

            Dim alis1 As New Generic.List(Of ReportParameter)
            Dim alis2 As New Generic.List(Of ReportParameter)
            Dim alis3 As New Generic.List(Of ReportParameter)
            Dim alis4 As New Generic.List(Of ReportParameter)
            Dim alis5 As New Generic.List(Of ReportParameter)
            Dim alis6 As New Generic.List(Of ReportParameter)

            alis1.Add(New ReportParameter("Periode", Periode))
            alis2.Add(New ReportParameter("Jobsite", Jobsite))
            alis3.Add(New ReportParameter("StsIsu", StsIsu))
            alis4.Add(New ReportParameter("Over", Over))
            alis5.Add(New ReportParameter("Dep", Dep))
            alis6.Add(New ReportParameter("Jns", Jns))

            ReportViewer.LocalReport.SetParameters(alis1)
            ReportViewer.LocalReport.SetParameters(alis2)
            ReportViewer.LocalReport.SetParameters(alis3)
            ReportViewer.LocalReport.SetParameters(alis4)
            ReportViewer.LocalReport.SetParameters(alis5)
            ReportViewer.LocalReport.SetParameters(alis6)

            ReportViewer.ShowPrintButton = True

            conn.Close()
            conn.Dispose()
        Catch ex As Exception
            Response.Write(ex.Message + " " + ex.StackTrace)
        End Try
    End Sub

    Function getString() As String
        Dim nik As String = Request.QueryString("nik")

        Dim squery As String = ""
        Dim skon As String = ""
        Dim sgroup As String = GroupUser(nik)

        Dim s As String = ""
        Dim upgrup As String = GetUpGroup(nik)
        For i As Integer = 1 To upgrup.Length
            If i = Len(upgrup) Then
                If Mid(upgrup, i, 1) = 1 Then
                    s = s + "'" + i.ToString + "'"
                End If
            Else
                If Mid(upgrup, i, 1) = 1 Then
                    s = s + "'" + i.ToString + "',"
                End If
            End If
        Next

        If Right(s, 1) = "," Then
            s = s.Substring(0, s.Length - 1)
        End If

        If s = "" Then
            s = "'0'"
        End If

        skon = " Where jenisRap in (" + s + ")"

        If sgroup = "S" Or sgroup = "1" Or sgroup = "3" Then
            skon = skon
        Else
            ' Jika GM cuma bisa liat divisinya
            If sgroup = "4" Then
                skon = skon

                If skon.IndexOf("'3',") > -1 Then
                    skon = skon.Replace("'3',", "")
                    skon = skon + " OR (JenisRap = '3' AND KdDepar = '" + GetFunction(GetCodeFunction(nik)) + "')"
                ElseIf skon.IndexOf("and JenisRap = '3'") > -1 Then
                    skon = skon.Replace("and JenisRap = '3'", "AND (JenisRap = '3' and KdDepar = '" + GetFunction(GetCodeFunction(nik)) + "')")
                End If
            Else
                ' PM/DPM, SekPro, Sect. Head
                If sgroup = "2" Or sgroup = "6" Or sgroup = "7" Then
                    skon = skon + " and JenisRap in (" + s + ") and KdCabang = '" + GetJobSite(nik) + "'"
                Else
                    ' PIC PDCA bisa lihat meeting cross function
                    skon = skon + " and JenisRap in (" + s + ")"

                    If skon.IndexOf("'3',") > -1 Then
                        skon = skon.Replace("'3',", "")
                        skon = skon + " OR (JenisRap = '3' AND KdDepar = '" + GetFunction(GetCodeFunction(nik)) + "')"
                    ElseIf skon.IndexOf("and JenisRap = '3'") > -1 Then
                        skon = skon.Replace("and JenisRap = '3'", "AND (JenisRap = '3' and KdDepar = '" + GetFunction(GetCodeFunction(nik)) + "')")
                    End If
                End If
            End If
        End If

        squery = squery + "select *, ROW_NUMBER() OVER(ORDER BY TglRap DESC, IdRap) AS Indeks from ("
        squery = squery + "select b.IdRap,a.IdIssue,a.NmIssue,a.NoAction,b.kdcabang,b.kddepar,b.jenisrap, "
        squery = squery + "convert(varchar(12),b.tglrap,105) as tglrap, "
        squery = squery + "convert(varchar(12),(SELECT MAX(DueDate) FROM DRAPAT b WHERE b.IdRap+b.IdIssue = a.IdRap+a.IdIssue),105) as DueDate, "
        squery = squery + "isnull(d.stissue,'0') as stissue, a.NmPIC, a.nmInfo,"
        squery = squery + "Flag = CASE WHEN (DueDate > GetDate() + 3) Then 'B' WHEN DueDate < GetDate() THEN 'R' ELSE 'Y' END "
        squery = squery + "from hrapat b left join drapat a on b.idrap=a.idrap "
        squery = squery + "left join ( "
        squery = squery + "select idrap,idissue,min(stissue) as stissue from (select idrap,idissue,noaction,sum(cast(stissue as integer)) as stissue from TRISSUE "
        squery = squery + "group by idrap,idissue,noaction)g "
        squery = squery + "group by idrap,idissue)d "
        squery = squery + "on a.idrap=d.idrap and a.idissue=d.idissue "
        squery = squery + "where a.nikpic is not null and a.nikpic != '' and b.stedit='0' and a.idissue=a.noaction) "
        squery = squery + "FIN "
        If skon.Length > 0 Then
            squery = squery + skon
        End If
        Return squery
    End Function

    Function getStringOpsi() As String
        Dim nik As String = Request.QueryString("nik")

        Dim jnstgl As String = Request.QueryString("jnstgl")
        Dim stg1 As String = Request.QueryString("tg1")
        Dim stg2 As String = Request.QueryString("tg2")
        Dim job As String = Request.QueryString("job")
        Dim sts As String = Request.QueryString("sts")
        Dim over As String = Request.QueryString("over")
        Dim dpr As String = Request.QueryString("dpr")
        Dim jnsmtg As String = Request.QueryString("jnsmtg")

        Dim sgroup As String = GroupUser(nik)

        Dim sekuel As String = ""
        ' Tanggal - COND 1
        If (stg1 <> "" And stg2 <> "") And (stg1 <> "null" Or stg2 <> "null") Then
            If jnstgl = "rap" Then
                sekuel = " TglRap BETWEEN '" + DateTime.ParseExact(stg1, "dd/MM/yyyy", Nothing).ToString("MM/dd/yyyy") + "' AND '" + Format(DateTime.ParseExact(stg2, "dd/MM/yyyy", Nothing), "MM/dd/yyyy") + "'"
            Else
                sekuel = " DueDate BETWEEN '" + Format(DateTime.ParseExact(stg1, "dd/MM/yyyy", Nothing), "MM/dd/yyyy") + "' AND '" + Format(DateTime.ParseExact(stg2, "dd/MM/yyyy", Nothing), "MM/dd/yyyy") + "'"
            End If
        End If

        ' GROUP PERMISSION
        Select Case sgroup
            Case "2" 'SekPro
                sekuel = GenQuery(sekuel) & " KdCabang like '%" + GetJobSite(nik) + "%'"
            Case "3" 'BOD
                If job <> "" And job <> "null" Then
                    sekuel = GenQuery(sekuel) & " KdCabang like '%" + job & "%' "
                End If
            Case "4" 'GM
                If job <> "" And job <> "null" Then
                    sekuel = GenQuery(sekuel) & " KdCabang like '%" + job & "%' "
                End If
            Case "6" 'PM/DPM
                sekuel = GenQuery(sekuel) & " KdCabang like '%" + GetJobSite(nik) + "%' "
            Case "7" 'Sect Head
                sekuel = GenQuery(sekuel) & " KdCabang like '%" + GetJobSite(nik) + "%' "
            Case "8" 'PIC PDCA
                If job <> "" And job <> "null" Then
                    sekuel = GenQuery(sekuel) & " KdCabang like '%" + job & "%' "
                End If
            Case Else
                If job <> "" And job <> "null" Then
                    sekuel = GenQuery(sekuel) & " KdCabang like '%" + job & "%' "
                End If
        End Select

        Dim s As String = ""
        Dim upgrup As String = GetUpGroup(nik)
        For i As Integer = 1 To upgrup.Length
            If i = Len(upgrup) Then
                If Mid(upgrup, i, 1) = 1 Then
                    s = s + "'" + i.ToString + "'"
                End If
            Else
                If Mid(upgrup, i, 1) = 1 Then
                    s = s + "'" + i.ToString + "',"
                End If
            End If
        Next

        If Right(s, 1) = "," Then
            s = s.Substring(0, s.Length - 1)
        End If

        If s = "" Then
            s = "'0'"
        End If

        Dim strToReplace As String
        If sekuel <> "" Then
            sekuel = sekuel + " and (JenisRap in (" + s + ") "
            strToReplace = " and (JenisRap in (" + s + ") "
        Else
            sekuel = sekuel + " (JenisRap in (" + s + ") "
            strToReplace = " (JenisRap in (" + s + ") "
        End If

        'Jika punya akses S,1,3 (Admin,MDV,BOD) - Kode Departemen dapat disesuaikan dengan filter
        If (dpr <> "") And (dpr <> "null") Then
            If sekuel.IndexOf("'3',") > -1 Then
                If strToReplace <> "" Then
                    sekuel = sekuel.Replace(strToReplace, "")
                    If strToReplace.IndexOf(" AND ") > -1 Then
                        sekuel = sekuel + " AND (JenisRap = '3' AND KdDepar = '" + dpr + "') "
                    Else
                        sekuel = IIf(sekuel = "", sekuel + " (JenisRap = '3' AND KdDepar = '" + dpr + "') ", sekuel + " AND (JenisRap = '3' AND KdDepar = '" + dpr + "') ")
                    End If
                Else
                    sekuel = sekuel.Replace("'3',", "")
                    sekuel = sekuel + " OR (JenisRap = '3' AND KdDepar = '" + dpr + "')) "
                End If
            Else
                sekuel = sekuel + ") "
            End If
        Else
            Select Case sgroup
                Case "4" 'GM
                    If sekuel.IndexOf("'3',") > -1 Then
                        sekuel = sekuel.Replace("'3',", "")
                        sekuel = sekuel + " OR (JenisRap = '3' AND KdDepar = '" + GetFunction(GetCodeFunction(nik)) + "' )) "
                    Else
                        sekuel = sekuel + ") "
                    End If
                Case "5", "7", "8" 'PICPDCA
                    If sekuel.IndexOf("'3',") > -1 Then
                        sekuel = sekuel.Replace("'3',", "")
                        sekuel = sekuel + " OR (JenisRap = '3' and KdDepar = '" & GetFunction(GetCodeFunction(nik)) & "')) "
                    Else
                        sekuel = sekuel + ") "
                    End If
                Case Else
                    sekuel = sekuel + ") "
            End Select
        End If

        ' Opsi Pilihan
        ' ------------
        ' Status Issue
        If sts <> "" And sts <> "null" Then
            If sts = "Open" Then
                If sekuel = "" Then
                    sekuel = " StIssue = '0' "
                Else
                    sekuel = sekuel + " AND StIssue = '0' "
                End If
            Else
                If sekuel = "" Then
                    sekuel = " StIssue = '0' "
                Else
                    sekuel = sekuel + " AND StIssue = '1'"
                End If
            End If
        End If

        'overdue or not
        If over <> "" And over <> "null" Then
            If over = "due" Then
                If sekuel = "" Then
                    sekuel = " Flag='R' "
                Else
                    sekuel = sekuel + " AND Flag='R' "
                End If
            Else
                If sekuel = "" Then
                    sekuel = " Flag='Y' "
                Else
                    sekuel = sekuel + " AND Flag='Y' "
                End If
            End If
        End If

        Dim squery As String = ""

        squery = squery + "select Idrap,IdIssue,NmIssue,NoAction,kdcabang,kddepar,jenisrap,convert(varchar(12),tglrap,105) as tglrap,"
        squery = squery + "convert(varchar(12),DueDate,105) as DueDate,"
        squery = squery + "stissue,Flag, ROW_NUMBER() OVER(ORDER BY TglRap DESC, IdRap) AS Indeks from ( "
        squery = squery + "select b.IdRap,a.IdIssue,a.NmIssue,a.NoAction,b.kdcabang,b.kddepar,b.jenisrap, b.tglrap, "
        squery = squery + "(SELECT MAX(DueDate) FROM DRAPAT b WHERE b.IdRap+b.IdIssue = a.IdRap+a.IdIssue) as DueDate, "
        squery = squery + "isnull(d.stissue,'0') as stissue, a.NmPIC, a.nmInfo, "
        squery = squery + "Flag = CASE WHEN (DueDate > GetDate() + 3) Then 'B' WHEN DueDate < GetDate() THEN 'R' ELSE 'Y' END "
        squery = squery + "from hrapat b left join drapat a on b.idrap=a.idrap "
        squery = squery + "left join ("
        squery = squery + "select idrap,idissue,min(stissue) as stissue from (select idrap,idissue,noaction,sum(cast(stissue as integer)) as stissue from TRISSUE "
        squery = squery + "group by idrap,idissue,noaction)g "
        squery = squery + "group by idrap,idissue)d "
        squery = squery + "on a.idrap=d.idrap and a.idissue=d.idissue "
        squery = squery + "where a.nikpic is not null and a.nikpic != '' and b.stedit='0' and a.idissue=a.noaction "
        squery = squery + ")FIN  "
        If sekuel.Length > 0 Then
            squery = squery + " where " + sekuel
        End If

        'jenis meeting
        If jnsmtg <> "0" Then
            If sekuel = "" Then
                squery = squery
            Else
                squery = "select * from (select Idrap,IdIssue,NmIssue,NoAction,kdcabang,kddepar,jenisrap,tglrap,DueDate,stissue,Flag,ROW_NUMBER() OVER(ORDER BY TglRap DESC, IdRap) AS Indeks from (" + squery + ")b where jenisrap = '" + jnsmtg + "')c "
            End If
        End If

        Return squery
    End Function

    Function GroupUser(ByVal _iduser As String) As String
        Dim sgroup As String = "3"

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT Grup FROM MUSER WHERE IdUser = '" + _iduser + "'"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            While sdr.Read()
                sgroup = IIf(IsDBNull(sdr("Grup")), "", sdr("Grup"))
            End While

            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return sgroup
    End Function

    Function GetUpGroup(ByVal _iduser As String) As String
        Dim slokasi As String = ""

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT UpGrup FROM MUSER WHERE IdUser = '" + _iduser + "'"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            While sdr.Read()
                slokasi = IIf(IsDBNull(sdr("UpGrup")), "", sdr("UpGrup"))
            End While

            conn.Close()
            Return slokasi
        Catch eks As Exception
            conn.Close()
        End Try

        Return slokasi
    End Function

   Function GetFunction(ByVal _kode As String) As String
        Dim sfungsi As String = _kode

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString)
        Try
            conn.Open()

            Dim squery As String = ""
            'squery = "SELECT abbr FROM mappdep WHERE kode = '" + _kode + "'"
            squery = "SELECT abbr FROM mappdep WHERE NmDepar = '" + _kode + "'"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            While sdr.Read()
                sfungsi = IIf(IsDBNull(sdr("abbr")), "", sdr("abbr"))
            End While

            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return sfungsi
    End Function

    Function GetCodeFunction(ByVal _iduser As String) As String
        Dim sfungsi As String = ""

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
        Try
            If _iduser.IndexOf("10000306") > -1 Or _iduser.IndexOf("10002040") > -1 Or _iduser.IndexOf("10006574") > -1 Or _iduser.IndexOf("10010546") > -1 Or _iduser.IndexOf("10011006") > -1 Or _iduser.IndexOf("10011038") > -1 Then
                sfungsi = "Human Resource & General Affair"
            ElseIf _iduser.IndexOf("10002373") > -1 Or _iduser.IndexOf("10002609") > -1 Or _iduser.IndexOf("10002779") > -1 Or _iduser.IndexOf("10009167") > -1 Or _iduser.IndexOf("10010394") > -1 Or _iduser.IndexOf("10008053") > -1 or  _iduser.IndexOf("10010983") > -1 or _iduser.IndexOf("10000973") > -1  Then
                sfungsi = "Operation"
            ElseIf _iduser.IndexOf("10007237") > -1 Or _iduser.IndexOf("10011034") > -1 Then
                sfungsi = "Plant"
            ElseIf _iduser.IndexOf("10007792") > -1 Then
                sfungsi = "Finance & Accounting"
            ElseIf _iduser.IndexOf("10010983") > -1 Then
                sfungsi = "None Function"
            ElseIf _iduser.IndexOf("10010982") > -1 Then
                sfungsi = "Engineering"
            Else
                conn.Open()

                Dim squery As String = ""
                'squery = "SELECT KdDepar FROM H_A101 WHERE Nik = '" + _iduser + "'"
                'squery = "SELECT ZFUN FROM H_A101_SAP WHERE PERNR = '" + _iduser + "' OR PNALT='" + _iduser + "'"
                squery = "SELECT KdDepar as ZFUN FROM H_A101 WHERE Nik = '" + _iduser + "'"

                Dim scom As SqlCommand = New SqlCommand(squery, conn)
                Dim sdr As SqlDataReader = scom.ExecuteReader()

                While sdr.Read()
                    sfungsi = IIf(IsDBNull(sdr("ZFUN")), "", sdr("ZFUN"))
                End While

                conn.Close()
            End If
        Catch eks As Exception
            conn.Close()
        End Try

        Return sfungsi
    End Function

    Function GetJobSite(ByVal _iduser As String) As String
        Dim slokasi As String = ""

        ''KODE JOBSITE TIDAK AMBIL LAGI DARI HRIS KARENA LEVEL SCTN HEAD UP
        ''KODE JOBSITE DIANGGAP HO - MAKA AMBIL DARI TABEL MUSER

        'KODE JOBSITE AMBIL LAGI DARI HRIS - BASED RAPAT DI RUANG 3B TGL 1/6/2010

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            'JIKA LEVEL E-Up MAKA AMBIL KODE LOKASI DARI H_A101 HRIS
            'SEDANGKAN D-Down AMBIL KODE JOBSITE DARI H_A101 HRIS

            Dim squery As String = ""
            'squery = "SELECT Nik, KdLokasi = case KdLevel when 5 Then KdLokasi when 6 Then KdLokasi When 7 Then KdLokasi else KdSite end "
            'squery = squery + "FROM H_A101 WHERE Nik = '" + _iduser + "'"

            'squery = "SELECT SITE as KdLokasi FROM MAPSITESAP a inner join OPENROWSET('SQLNCLI','SERVER=172.16.0.7\hris;UID=IICS;PWD=$IICS-123-ONLINE$', 'SELECT WERKS,BTRTL from BERP_CENTRAL.dbo.H_A101_SAP where pernr=''" & _iduser & "''') x on a.WERKS=x.WERKS and a.BTRTL=x.BTRTL"
            squery = "SELECT SITE as KdLokasi FROM MAPSITESAP a inner join OPENROWSET('SQLNCLI','SERVER=20.110.1.5;UID=dev;PWD=hrishris', 'SELECT kdsite,'' as BTRTL from JRN_TEST.dbo.H_A101  where nik=''" & _iduser & "''') x on a.site=x.kdsite"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            If sdr.HasRows Then
                While sdr.Read()
                    slokasi = IIf(IsDBNull(sdr("KdLokasi")), "NFO", sdr("KdLokasi"))
                End While
            Else
                slokasi = "NFO"
            End If

            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return slokasi
    End Function

    Function GenQuery(ByVal q As String) As String
        If q = "" Then
            q = q & " "
            Return q
        Else
            q = q & " AND "
            Return q
        End If
    End Function

    Function GetLastProgress(ByVal IdRap As String, ByVal IdIssue As String, ByVal IdAction As String) As String
        Dim sProgress As String = ""

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()
            Dim squery As String = ""
            If IdAction = "" Then
                squery = "SELECT TOP 1 nmprogres FROM TRPROGRESS WHERE tglprogres = "
                squery = squery + "(SELECT MAX(tglprogres) FROM trprogress WHERE IdRap = '" + IdRap + "' "
                squery = squery + "AND IdIssue = '" + IdIssue + "') AND "
                squery = squery + "IdRap = '" + IdRap + "' AND IdIssue = '" + IdIssue + "'"
            Else
                squery = "SELECT TOP 1 nmprogres FROM TRPROGRESS WHERE tglprogres = "
                squery = squery + "(SELECT MAX(tglprogres) FROM trprogress WHERE IdRap = '" + IdRap + "' "
                squery = squery + "AND IdIssue = '" + IdIssue + "' AND NoAction = '" + IdAction + "') AND "
                squery = squery + "IdRap = '" + IdRap + "' AND IdIssue = '" + IdIssue + "' AND NoAction = '" + IdAction + "'"
            End If

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()
            Do While sdr.Read()
                sProgress = sProgress + IIf(IsDBNull(sdr("nmprogres")), "", sdr("nmprogres"))
            Loop
            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return sProgress
    End Function

    Function GetInfoTo(ByVal IdRap As String, ByVal IdIssue As String) As String
        Dim sInfo As String = ""

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT DISTINCT(NmInfo) as NmInfo,IdRap,IdIssue,NoAction FROM DRAPAT WHERE IdRap = '" + IdRap + "' AND IdIssue = '" + IdIssue + "' ORDER BY IdRap, IdIssue, NoAction "

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()
            Do While sdr.Read()
                sInfo = sInfo + IIf(IsDBNull(sdr("NmInfo")), "-", sdr("NmInfo") + ",")
            Loop
            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        sInfo = sInfo.Substring(0, sInfo.Length - 1)
        Return sInfo
    End Function

    ' Mendapatkan Nama PIC per Issue
    Function GetPIC(ByVal IdRap As String, ByVal IdIssue As String) As String
        Dim sPIC As String = ""

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT DISTINCT(NmPIC + ' ' + ISNULL((SELECT CASE WHEN stIssue=1 THEN '[Close]' ELSE '[Open]' END STAT FROM trissue "
            squery = squery + "WHERE IdRap = DRAPAT.IDRAP AND IdIssue = DRAPAT.IDISSUE AND NOACTION=DRAPAT.NOACTION),'[Open]') ) as NmPIC,IdRap,IdIssue,NoAction "
            squery = squery + "FROM DRAPAT WHERE IdRap = '" + IdRap + "' AND IdIssue = '" + IdIssue + "' ORDER BY IdRap, IdIssue, NoAction "

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()
            Do While sdr.Read()
                sPIC = sPIC + IIf(IsDBNull(sdr("NmPIC")), "-", sdr("NmPIC") + ",")
            Loop
            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        sPIC = sPIC.Substring(0, sPIC.Length - 1)
        Return sPIC
    End Function
End Class
