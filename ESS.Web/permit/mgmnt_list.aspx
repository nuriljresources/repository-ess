﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/permit/master.Master" CodeBehind="mgmnt_list.aspx.vb" Inherits="EXCELLENT.mgmnt_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Document Center Admin Rules
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jscal2.js" type ="text/javascript"></script>
    <script src="../Scripts/en.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="../css/win2k/win2k.css" />

    
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="line" style="right:10px; text-align:right;">
    <a href="signout.aspx">Logout</a>
    </div>
    
    <div class="title">
        <strong>Menu</strong>
    </div>
    <div id="list_newest" class="general" style="width:200px;">
        <div id="Div1"><img alt="open document" src="../images/open_doc_small.png" /> <a href="index.aspx">&nbsp;Home</a></div>
        <%=mnentry%>
        <%=mnview%>
        <%=mnmgmt%>
    <div><img alt="open document" src="../images/open_doc_small.png" /> <a href="J Resources Document Center Manual Version 1.pdf" target="_blank">&nbsp;Help</a></div>
    </div>
    <div id="list_permit" class="general" style="width:720px; border-style:none; height:400px; left:220px; top:15px; position:absolute; font-size:0.9em;">
        <table>
        <tr>
        <td class="general" style="padding-left:5px; padding-right:5px;"><a href="mgmnt_user_entry.aspx">Insert User</a></td>
        <td class="general" style="padding-left:5px; padding-right:5px;"><a href="mgmnt_list.aspx">List User</a></td>
        </tr>
        </table>
        
        
        <br />
        <asp:GridView ID="GridView1" runat="server" DataKeyNames="emp_code" AutoGenerateColumns="False"
            AllowPaging="True" PageSize="50" Width="1000px" 
            CssClass="data-table" Font-Size = "1.0em">
            <Columns>
            <asp:BoundField DataField="emp_code" HeaderText="NIK" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="name" HeaderText="NAME" HeaderStyle-Width="200px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="modul" HeaderText="Department" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
             <asp:BoundField DataField="lvl" HeaderText="Level" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="schmail" HeaderText="Reminder" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:CommandField ShowSelectButton="True" SelectText="Edit" ButtonType="Link" HeaderStyle-Width="40px" ItemStyle-VerticalAlign="top" >
                <HeaderStyle Width="40px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:CommandField>
            </Columns>
            <SelectedRowStyle Font-Bold="True" />
            <PagerStyle HorizontalAlign="Left" />
            <HeaderStyle Font-Bold="True" />
            <RowStyle Font-Bold="True" />
        </asp:GridView>
        <div id="idnotif"></div>
        <br />
         <asp:Button ID="Save" runat="server" style="background-image:url('../images/save-icon-small.png'); background-repeat:no-repeat; background-position:center; width:32px; height:32px; visibility:hidden;" />
       <asp:Button ID="Button2" runat="server" Text="" EnableTheming="false" style="background-image:url('../images/cancel.png'); background-repeat:no-repeat; background-position:center; width:32px; height:32px; visibility:hidden;"/>    
    
    </div> 
    
     <center>
          <span id="idbar" style="color: #800000; background-color: #6699FF; font-weight: bold"></span>
    </center>
    
<script type="text/javascript" language="javascript">
    if (BrowserDetect.version != 8) {
        document.getElementById("idnotif").style.display = "block";
        document.getElementById("idnotif").style.textAlign = "center"
        document.getElementById("idnotif").style.fontsize = "large";
        document.getElementById("idnotif").style.fontWeight = "bold";
        document.getElementById("idnotif").style.color = "#FF0000";
        //        document.getElementById("idnotif").innerHTML = "*** Application HRMS not compatible with your browser ***<br />*** Please Update to Internet Explorer 8 ***";
    }
    else {
        document.getElementById("idnotif").style.display = "none";
        document.getElementById("idnotif").style.textAlign = "center"
        document.getElementById("idnotif").style.fontsize = "large";
        document.getElementById("idnotif").style.fontWeight = "bold";
        document.getElementById("idnotif").style.color = "#FFFFFF";
        document.getElementById("idnotif").innerHTML = "";
    }
    document.getElementById("idbar").style.display = "none";

    var cal = Calendar.setup({
        onSelect: function(cal) { cal.hide() },
        showTime: false
    });
    //        cal.manageFields("btdtissue", "dtissue", "%m/%d/%Y");
    //        cal.manageFields("btdtexp", "dtexp", "%m/%d/%Y");
          
</script>


</asp:Content>
