﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="saccount.aspx.vb" Inherits="EXCELLENT.saccount" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
        ul#navigation li a.m7
        {        	
	        background-color:#00CC00;color: #000000;
        }
        .coloring:hover
        {background-color:#ff0; }
        .coloring
        {height:20px; }
 </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<ajaxtoolkit:toolkitscriptmanager  ID="ToolkitScriptManager1" runat="server"/>
<asp:UpdatePanel id="MyUpdatePanel" runat="server">
 <ContentTemplate>
 
<span id="idbar">
</span>
      
<table class="menu-table">
<tr><td><a href="account.aspx">Add User</a></td>
<td><a href="saccount.aspx">List User</a></td></tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
       <caption style="text-align:center;font-size:1.5em">
           FIND USER
       </caption>
       <tr>
      <td colspan="4">
          <asp:Repeater ID="rptSearchResult" runat="server">
                        <HeaderTemplate>
                           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table-inside">
                           <tr> 
                                <th>No.</th>
                                <th>User ID</th>
                                <th>User Name</th>
                                <th>Department</th>
                                <th>Jobsite</th>
                                <th>Group</th>
                                <th>Delete</th> 
                                <th>Edit</th>                
                           </tr>
                        </HeaderTemplate>  
                        <ItemTemplate>
                           <tr class="coloring">
                              <td style="border-bottom:1px solid"><%# Container.ItemIndex + 1 %></td>
                              <td style="border-bottom:1px solid"><%#DataBinder.Eval(Container.DataItem, "IdUser")%></td>
                              <td style="border-bottom:1px solid"><%#DataBinder.Eval(Container.DataItem, "NmUser")%></td>
                              <td align="center" style="border-bottom:1px solid"><%#DataBinder.Eval(Container.DataItem, "Departemen")%></td>
                              <td style="border-bottom:1px solid"><%#DataBinder.Eval(Container.DataItem, "Jobsite")%></td>
                              <td style="border-bottom:1px solid"><%#DataBinder.Eval(Container.DataItem, "Grup")%></td>
                              <td align="center" style="border-bottom:1px solid">
                              <ASP:LinkButton ID="btnDelete" Text="X" CommandName="Delete" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "IdUser")%>'/>
                              </td>
                              <td align="center" style="border-bottom:1px solid">
                              <ASP:LinkButton ID="btnEdit" Text="E" CommandName="Edit" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "IdUser")%>'/>
                              </td>
                           </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                           </table>
                        </FooterTemplate>
          </asp:Repeater>
       </td>      
       </tr>
       <tr>
       <td colspan="4">
           <div id="log" runat="server" style="color:red;font:Trebechuet;"></div>
       </td>
       </tr>
</table>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
