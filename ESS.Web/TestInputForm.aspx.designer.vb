﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.5420
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On



Partial Public Class TestInputForm

    '''<summary>
    '''txtTransNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTransNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtBank control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBank As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCompany control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCompany As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddress As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtDivision control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDivision As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtSite control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSite As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtLocation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtLocation As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtRemark control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRemark As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''hiddenForm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hiddenForm As Global.System.Web.UI.WebControls.HiddenField
End Class
