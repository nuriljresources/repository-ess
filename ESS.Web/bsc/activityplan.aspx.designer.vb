﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.4927
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On



Partial Public Class activityplan

    '''<summary>
    '''Grup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Grup As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Jobsite control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Jobsite As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Divisi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Divisi As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Section control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Section As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''userid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents userid As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''userip control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents userip As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hid As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''imgid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgid As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''ddlYear control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlYear As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''KPILevel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents KPILevel As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''KPIFunc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents KPIFunc As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''KPISection control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents KPISection As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''KPIJobsite control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents KPIJobsite As Global.System.Web.UI.WebControls.DropDownList
End Class
