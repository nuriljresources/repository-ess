﻿Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms

Partial Public Class PrintLogNotulen
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") = "" Or Session("permission") = "M" Then
            Response.Redirect("Login.aspx", False)
            Exit Sub
        End If

        Try
            If PreviousPage.Jenis <> "0" Then
                Me.SqlDataSourceHR.SelectCommand = "SELECT [IdRap], [NmRap], [TmpRap], [WkRap], [SdWkRap], [TglRap], [Agenda], [Pimpinan], [NmPimpinan], [Notulis], [NmNotulis], [JenisRap] FROM HRAPAT where TglRap >='" + DateTime.ParseExact(PreviousPage.STglAwal, "dd/MM/yyyy", Nothing).ToString("MM/dd/yyyy") + "' and tglrap <='" + DateTime.ParseExact(PreviousPage.STglAkhir, "dd/MM/yyyy", Nothing).ToString("MM/dd/yyyy") + "' and JenisRap='" + PreviousPage.Jenis + "'"
            Else
                Dim s As String = ""
                For i As Integer = 1 To Len(Session("jnsmeeting"))
                    If Mid(Session("jnsmeeting"), i, 1) = 1 Then
                        s = s + "" + i.ToString() + ","
                    End If
                Next
                Me.SqlDataSourceHR.SelectCommand = "SELECT [IdRap], [NmRap], [TmpRap], [WkRap], [SdWkRap], [TglRap], [Agenda], [Pimpinan], [NmPimpinan], [Notulis], [NmNotulis], [JenisRap] FROM HRAPAT where TglRap >='" + DateTime.ParseExact(PreviousPage.STglAwal, "dd/MM/yyyy", Nothing).ToString("MM/dd/yyyy") + "' and tglrap <='" + DateTime.ParseExact(PreviousPage.STglAkhir, "dd/MM/yyyy", Nothing).ToString("MM/dd/yyyy") + _
                    "' and JenisRap in (" + Left(s, Len(s) - 1) + ")"
            End If
            ReportViewer.LocalReport.Refresh()
            ReportViewer.ShowRefreshButton = False
            ReportViewer.ShowPrintButton = True
        Catch ex As Exception
            Response.Write("Periksa tanggal yang anda piilh ....")
        End Try
    End Sub
End Class