<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/reminder/master.Master" CodeBehind="supp_permit_lisense_entry.aspx.vb" Inherits="EXCELLENT.supp_permit_lisense_entry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Document Center Entry
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jscal2.js" type ="text/javascript"></script>
    <script src="../Scripts/en.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="../css/win2k/win2k.css" />
    <style type="text/css" >
        #form1
        {
            font-family:Tahoma,Arial;
        }
        #form1 h1 
        {
            color: #FFFFFF;
            font-size:x-large;
        }
        .title
        {
            color: blue;
            font-size: small;
            margin-bottom:10px;
        }
        .line
        {
            width: 100%;
            height: 20px;
            font-family:Tahoma,Arial;
            font-size:0.8em;
        }
        .general
        {
            font-size:0.8em;
            color:#33ADFF;
            font-weight:bold;
            border-style:solid;
            border-width:1px;
            border-color:#33ADFF;
            padding-left:5px;
        }
        
        .general a
        {
            text-decoration:none;
            color:#33ADFF;
        }
        
        .general a:hover
        {
            text-decoration:underline;
            color:#33ADFF;
        }
        
        .menu_list a:hover
        {
            text-decoration:underline;
            color:#33ADFF;
        }
        
        body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
a:link {
	color: #0000FF;
}
a:visited {
	color: #0000FF;
}
a:hover {
	color: #0000FF;
	text-decoration: none;
}
a:active {
	color: #0000FF;
	}
.basix {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.header1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #006699;
}
.lgHeader1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #0066CC;
	background-color: #CEE9FF;
}
    </style>
    <script type ="text/javascript" >
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode

            //alert("keyCode for the key pressed: " + charCode + "\n");

            if (charCode >= 0)
                return false;

            if (charCode == "undefined")
                return false;

            if (charCode == 8)
                return false;
            if (charCode == 46)
                return false;
            //return true;
        }
        function iskeyup(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            switch (charCode) {
                case 8:
                    event.keyCode = 27;
                    //alert("backspace");
                    return false;
                    break;
                case 46:
                    //alert("delete");
                    return false;
                    break;
            }
            if (charCode == 8)
                return false;
        }
        function OpenPopupdel(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("findpic.aspx", "List", "scrollbars=yes,resizable=no,width=600,height=500");
            return false;
        }
        function OpenPopup(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("spic.aspx", "List", "scrollbars=yes,resizable=yes,width=600,height=500");
            return false;
        }
        window.onload = function() {
            var check = document.getElementById('<%=rpt.ClientID%>');

            var check2 = document.getElementById('<%=chkpic1.ClientID%>');
            check2.onchange = function() {
                if (this.checked == true)
                    document.getElementById('<%=txtpic1.ClientID%>').style.visibility = "visible";
                else
                    document.getElementById('<%=txtpic1.ClientID%>').style.visibility = "hidden";
            };
            var check3 = document.getElementById('<%=chkpic2.ClientID%>');
            check3.onchange = function() {
                if (this.checked == true)
                    document.getElementById('<%=txtpic2.ClientID%>').style.visibility = "visible";
                else
                    document.getElementById('<%=txtpic2.ClientID%>').style.visibility = "hidden";
            };
            var check4 = document.getElementById('<%=chkpic3.ClientID%>');
            check4.onchange = function() {
                if (this.checked == true)
                    document.getElementById('<%=txtpic3.ClientID%>').style.visibility = "visible";
                else
                    document.getElementById('<%=txtpic3.ClientID%>').style.visibility = "hidden";
            };
            var check5 = document.getElementById('<%=chkpic4.ClientID%>');
            check5.onchange = function() {
                if (this.checked == true)
                    document.getElementById('<%=txtpic4.ClientID%>').style.visibility = "visible";
                else
                    document.getElementById('<%=txtpic4.ClientID%>').style.visibility = "hidden";
            };
            var check6 = document.getElementById('<%=chkpic5.ClientID%>');
            check6.onchange = function() {
                if (this.checked == true)
                    document.getElementById('<%=txtpic5.ClientID%>').style.visibility = "visible";
                else
                    document.getElementById('<%=txtpic5.ClientID%>').style.visibility = "hidden";
            };
        };

        function change() {
            if (document.getElementById('<%=txtpic1.ClientID%>').style.visibility == "hidden") {
                document.getElementById('<%=txtpic1.ClientID%>').value = "";
            }

            if (document.getElementById('<%=txtpic2.ClientID%>').style.visibility == "hidden") {
                document.getElementById('<%=txtpic2.ClientID%>').value = "";
            }

            if (document.getElementById('<%=txtpic3.ClientID%>').style.visibility == "hidden") {
                document.getElementById('<%=txtpic3.ClientID%>').value = "";
            }

            if (document.getElementById('<%=txtpic4.ClientID%>').style.visibility == "hidden") {
                document.getElementById('<%=txtpic4.ClientID%>').value = "";
            }

            if (document.getElementById('<%=txtpic5.ClientID%>').style.visibility == "hidden") {
                document.getElementById('<%=txtpic5.ClientID%>').value = "";
            }

            var fullPath = "";

            if (fullPath) {
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                var obj1 = document.getElementById("filename");
                var obj2 = document.getElementById("AttachmentTitleTextbox");

                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    obj2.value = filename;
                }
            }
        }

        function getFileExtension(filename) {
            var ext = /^.+\.([^.]+)$/.exec(filename);
            return ext == null ? "" : ext[1];
        }

        function simpan() {
            var fullPath = "";

        }

        function save() {

            if (document.getElementById('<%=chkpic1.ClientID%>').checked == false) {
                document.getElementById('<%=txtpic1.ClientID%>').value = ""
            }

            if (document.getElementById('<%=chkpic2.ClientID%>').checked == false) {
                document.getElementById('<%=txtpic2.ClientID%>').value = ""
            }

            if (document.getElementById('<%=chkpic3.ClientID%>').checked == false) {
                document.getElementById('<%=txtpic3.ClientID%>').value = ""
            }

            if (document.getElementById('<%=chkpic4.ClientID%>').checked == false) {
                document.getElementById('<%=txtpic4.ClientID%>').value = ""
            }

            if (document.getElementById('<%=chkpic5.ClientID%>').checked == false) {
                document.getElementById('<%=txtpic5.ClientID%>').value = ""
            }

            var fullPath = document.getElementById("txturl").value;
            //alert(fullPath);
            if (fullPath.length == 0) {
                document.getElementById('<%= label1.ClientID %>').text = 'You have not specified a file.'
                return;
            }

            var d = document;
            var torpt;
            var s = document.getElementById('<%=doctp.ClientID%>');
            var type = s.options[s.selectedIndex].value;

            var s2 = document.getElementById('<%=doctp.ClientID%>');
            var dtype = s2.options[s2.selectedIndex].value;

            
            var fname = d.getElementById("nmdoc");
            var check = document.getElementById('<%=rpt.ClientID%>');
            if (check.checked == true) {
                torpt = 1
            }

            if (check.checked == false) {
                torpt = 0
            }

            if (d.getElementById("type").value.length == 0) {
                alert("Please Entry Document Type");
                return false;
            }

            d.getElementById('<%=tipe.ClientID%>').value = type
            d.getElementById('<%=doctype2.ClientID%>').value = dtype

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../WS/Service.asmx/Addpermit",
                data: "{'_type':" + JSON.stringify(type) + ",'_subject':" + JSON.stringify(document.getElementById("subject").value) + ",'_year':" + JSON.stringify("") + ",'_category':" + JSON.stringify(document.getElementById("category").value) + ",'_department':" + JSON.stringify(document.getElementById("Department").value) + ",'_subdept':" + JSON.stringify(document.getElementById("subdept").value) + ",'_remdays':" + JSON.stringify(document.getElementById('<%=rem_days.ClientID%>').value) + ",'_pic1':" + JSON.stringify(document.getElementById('<%=txtpic1.ClientID%>').value) + ",'_pic2':" + JSON.stringify(document.getElementById('<%=txtpic2.ClientID%>').value) + ",'_pic3':" + JSON.stringify(document.getElementById('<%=txtpic3.ClientID%>').value) + ",'_pic4':" + JSON.stringify(document.getElementById('<%=txtpic4.ClientID%>').value) + ",'_pic5':" + JSON.stringify(document.getElementById('<%=txtpic5.ClientID%>').value) + ",'_dtissue':" + JSON.stringify(document.getElementById("dtissue").value) + ",'_dtexp':" + JSON.stringify(document.getElementById("dtexp").value) + ",'_url':" + JSON.stringify(fullPath) + ",'_filename':" + JSON.stringify(document.getElementById("nmdoc").value) + "}",
                dataType: "json",
                success: function(res) {
                    alert("Data Saved Successfully");
                    //window.location = "doc_center_entry.aspx";
                },
                error: function(err) {
                    alert(err.toString);
                    //window.location = "entry_permit_doc.aspx";
                }
            });
        }
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="line" style="right:10px; text-align:right;">
        <a href="signout.aspx">Logout</a>
    </div>
    <div class="title">
        <strong>Menu</strong>
    </div>
    
    <div id="list_newest" class="general" style="width:200px; ">
        <div id="Div1"><img alt="open document" src="../images/open_doc_small.png" /> <a href="index.aspx">
            &nbsp;Home</a></div>
        <%=mnentry%>
        <%=mncontract%>
        <%=mnview%>
        <%=mnmgmt%>
        
    <div><img alt="open document" src="../images/open_doc_small.png" /> <a href="J Resources Document Center Manual Version 1.pdf" target="_blank">
        &nbsp;Help</a></div>
    </div>
    <%--color:#33ADFF;--%>
      <div id="list_permit" class="general" style="width:1100px; height:450px; left:240px; top:20px; position:absolute; border-color:#fff;">
        <input type="hidden" id="ctrlToFind" />      
        <asp:Label ID="label1" runat="server" ></asp:Label>
        <asp:Label ID="label2" runat="server" ></asp:Label>
        <asp:HiddenField ID="hidpl" runat="server" />
        <asp:HiddenField ID="hedit" runat="server" />
        <asp:HiddenField ID="hidpl2" runat="server" />
        <table class="data-table">
        <caption style="width:300px; text-align:center;">Document Information</caption>
        
        
        <tr>
            <td class="general" style="height:10px;">
                Type
            </td>
            <td>
                <asp:DropDownList ID="doctp" runat="server" style="width:200px;">
                <asp:ListItem Value="SK">SK</asp:ListItem>
                <asp:ListItem Value="SE">SE</asp:ListItem>
                <asp:ListItem Value="SOP">SOP</asp:ListItem>
                <asp:ListItem Value="WI">Working Instruction</asp:ListItem>
                <asp:ListItem Value="SPP">SPP</asp:ListItem>
                <asp:ListItem Value="IM">Interoffice Memorandum</asp:ListItem>
                <asp:ListItem Value="ITM">Internal Memorandum</asp:ListItem>
                <asp:ListItem Value="SKR">Surat Keluar</asp:ListItem>
                <asp:ListItem Value="PMT">Permit and license</asp:ListItem>
                <asp:ListItem Value="DK">Dokumen Kontrak</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        
        <tr>
            <td class="general" style="height:10px;">
                Subject
            </td>
            <td>
                <asp:TextBox ID="subject" runat="server" ></asp:TextBox>
            </td>
        </tr>
        
        <tr>
            <td class="general" style="height:10px;">
                Entry Date
            </td>
            <td>
                <asp:TextBox ID="txtyearx" runat = "server" Enabled="false" ></asp:TextBox> 
                <input type="hidden" id="category"/>
                  <input type="hidden" id="Department"/>
                <input type="hidden" id="subdept"/>
            </td>
        </tr>
        
        <tr>
            <td class="general" style="height:10px;">
                Supplier
            </td>
            <td>
                <asp:TextBox ID="txtvendor" runat="server"></asp:TextBox>
            </td>
        </tr>
   
        <tr>
            <td class="general" style="height:10px;">
                Reminder
            </td>
            <td>
                <input type="hidden" id="report" />
                <asp:DropDownList ID="rem_days" runat="server" style="width:200px;">
                <asp:ListItem Value="7">1 Week Before Expire</asp:ListItem>
                <asp:ListItem Value="14">2 Week Before Expire</asp:ListItem>
                <asp:ListItem Value="30">1 Month Before Expire</asp:ListItem>
                <asp:ListItem Value="60">2 Month Before Expire</asp:ListItem>
                <asp:ListItem Value="90">3 Month Before Expire</asp:ListItem>
                <asp:ListItem Value="180">6 Month Before Expire</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        
        <tr>
        <td></td>
        <td>
        
        </td>
        </tr>
        
       
       </table>
       <br />
     
       <table>
        <tr>
        
        <td colspan="3" align="left"><table width="300px" class="data-table">
        <caption style="text-align:center">Add Person</caption>
        <tr>
                <td>
                    Name:</td>
                <td style="width: 18px">
                    <asp:TextBox ID="txtUserName" runat="server" Width="140px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    NIK:</td>
                <td style="width: 18px">
                    <asp:TextBox ID="txtnik" runat="server" Width = "100px" onkeypress="return isNumberKey(event)" ></asp:TextBox>
                    <img alt="add" id="Img1" src="../images/Search.gif" align="absmiddle" style="cursor: pointer" onclick="OpenPopup('suer')" />             
                </td>
            </tr>
            <tr>
                <td>
                    Email Schedule:</td>
                <td style="width: 18px">
                    <asp:DropDownList ID="ddlsch" runat = "server" >
                    <asp:ListItem Value = "01">Daily</asp:ListItem>
                    <asp:ListItem Value = "02">Weekly</asp:ListItem>
                    <asp:ListItem Value = "03">Monthly</asp:ListItem>
                    </asp:DropDownList>    
                </td>                
            </tr>
            
             <tr>
                 <td colspan="2" align="center">
                     <center><asp:Button ID="btnAdd" runat="server" Text="Add User To This Document" /></center>
                 </td>
             </tr>
             <tr><td colspan = "2"><asp:Label ID="lblTips" runat="server" ForeColor="Red"></asp:Label></td></tr>
             <tr>
             <td colspan = "2"><asp:Button ID="btnsave" runat = "server" style="background-image:url('../images/save-icon-small.png'); background-repeat:no-repeat; background-position:center; width:32px; height:32px;"/> 
             <asp:button id="btncancel" runat="server" style="background-image:url('../images/cancel.png'); background-repeat:no-repeat; background-position:center; width:32px; height:32px;"/></td>
             
             </tr>
        </table></td></tr> 
        
        <tr>
                 <td>
                    <asp:HiddenField ID="tipe" runat="server" />
                    <select id="type" style="visibility:hidden;">
                    <option value=" "> </option>
                    </select>
                    <asp:HiddenField ID="doctype2" runat="server" />
                    <input type="checkbox" id="rpt" checked="checked" style="visibility:hidden;" runat="server" />
                 </td>
             </tr>
       </table>
       
       <br />
        
        <div style="left:350px; top:0px; position:absolute; width:750px; Height:400px;" >
        
        <asp:GridView runat="server" ID="gvlstPL" AutoGenerateColumns="False" DataKeyNames="No" CssClass="data-table" 
        OnRowCancelingEdit="gvlstPL_RowCancelingEdit" OnRowEditing="gvlstPL_RowEditing" OnRowUpdating="gvlstPL_RowUpdating" 
        OnRowCommand="gvlstPL_RowCommand" OnRowDeleting="gvlstPL_RowDeleting" ShowHeader="True" ShowFooter="True">
        <Columns>
            <asp:TemplateField HeaderText="Permit/Lisense Name">
                <EditItemTemplate>
                    <asp:TextBox ID="txtplnameedit" style="width:60px" runat="server" Text='<%# Eval("pl_name") %>' Font-Size="1.0em"> </asp:TextBox>
                </EditItemTemplate>
                
                <FooterTemplate>
                <div style="width:85px">
                    <asp:TextBox ID="txtlnm" style="width:60px" runat="server" Font-Size="1.0em"> </asp:TextBox> 
                </div> 
                </FooterTemplate>
                
                <ItemTemplate>
                       <asp:Label ID="lblplnm" runat="server" Text='<%# Bind("pl_name") %>'></asp:Label>
                </ItemTemplate>  
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Permit No" HeaderStyle-Width="100px" >
                <EditItemTemplate>
                    <asp:TextBox ID="txtpnoedit" style="width:60px" runat="server" Text='<%# Eval("permit_no") %>' Font-Size="1.0em"> </asp:TextBox>
                </EditItemTemplate>
                
                <FooterTemplate>
                    <asp:TextBox ID="txtpnonew" style="width:60px" runat="server" Font-Size="1.0em"> </asp:TextBox> 
                </FooterTemplate>
                
                <ItemTemplate>
                       <asp:Label ID="lblpno" runat="server" Text='<%# Bind("permit_no") %>'></asp:Label>
                </ItemTemplate>  
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Date Issued" HeaderStyle-Width="100px" >
                <EditItemTemplate>
                    <asp:TextBox ID="txtdtissuededit" style="width:60px" runat="server" Text='<%# Eval("dt_issued") %>' Font-Size="1.0em"> </asp:TextBox>
                    <button id="btn_dt_issuededit" style="height:20px; width:15px;">..</button>
                </EditItemTemplate>
                
                <FooterTemplate>
                    <asp:TextBox ID="txtdtissuenew" style="width:60px" runat="server" onkeypress="return isNumberKey(event)" Font-Size="1.0em"> </asp:TextBox>
                    <button id="btn_dt_issuednew" style="height:20px; width:15px;">..</button> 
                </FooterTemplate>
                
                <ItemTemplate>
                       <asp:Label ID="lbldtissued" runat="server" Text='<%# Bind("dt_issued") %>'></asp:Label>
                </ItemTemplate>  
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Date Expired" HeaderStyle-Width="100px" >
                <EditItemTemplate>
                    <asp:TextBox ID="txtdtexpedit" style="width:60px" runat="server" Text='<%# Eval("dt_expired") %>' Font-Size="1.0em"> </asp:TextBox>
                    <button id="btn_dt_expedit" style="height:20px; width:15px;">..</button>
                </EditItemTemplate>
                
                <FooterTemplate>
                    <asp:TextBox ID="txtdtexpnew" style="width:60px" runat="server" Font-Size="1.0em"> </asp:TextBox> 
                    <button id="btn_dt_expnew" style="height:20px; width:15px;">..</button>
                </FooterTemplate>
                
                <ItemTemplate>
                       <asp:Label ID="lbldtexp" runat="server" Text='<%# Bind("dt_expired") %>'></asp:Label>
                </ItemTemplate>  
            </asp:TemplateField>
            
             <asp:TemplateField HeaderText="" ShowHeader="False" FooterStyle-BorderStyle="None" HeaderStyle-BorderStyle="None" ItemStyle-BorderStyle="None">
               <EditItemTemplate>
                <div style="width:85px;">
                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update">
                   </asp:LinkButton>
                   <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel">
                   </asp:LinkButton>
               </div>
               </EditItemTemplate>
               <FooterTemplate>
               <div style="width:30px;">
                   <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="AddNew" Text="Save"></asp:LinkButton>
                </div> 
               </FooterTemplate>
               <ItemTemplate>
               
                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
               
               </ItemTemplate>
           </asp:TemplateField>   
           
           <asp:CommandField HeaderText="" HeaderStyle-BorderStyle="None" ShowDeleteButton="True" ShowHeader="False" ItemStyle-BorderStyle="None" FooterStyle-BorderStyle="None"/>         
        </Columns> 
        
        </asp:GridView>
        
        <asp:GridView ID="GridView2" runat="server" DataKeyNames="Nik" AutoGenerateColumns="False" Width="346px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" EmptyDataText="No User Associated to this Document." ForeColor="Red">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Name"/>
                <asp:BoundField DataField="Nik" HeaderText="Nik" />
                <asp:BoundField DataField="Schedule" HeaderText="Schedule" />
                <asp:CommandField ShowDeleteButton="true" ButtonType="Link"/>
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <RowStyle ForeColor="#000066" BorderStyle="Groove" BorderColor="#CCCCCC"/>
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <HeaderStyle BackColor="#0066ff" Font-Bold="false" ForeColor="#282828" />
        </asp:GridView>
        </div>
       
        <div style="position:absolute;">
        <asp:Label ID="tny" runat="server" ></asp:Label>
        <asp:Button ID="yes" Text="Yes" runat = "server" style="width:32px; height:32px;"/>
        <asp:Button ID="no" Text = "No" runat = "server" style="width:32px; height:32px;"/>
      <input type="checkbox" checked="checked" id="chkpic1" disabled = "disabled" style="visibility:hidden;" runat="server" />
                <asp:DropDownList ID="txtpic1" runat="server" Enabled="false" style="visibility:hidden; width:200px;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email"></asp:DropDownList>
                <asp:SqlDataSource ID="DsPIC" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:jrn_permit %>"
                    SelectCommand="SELECT [pic_name], [pic_email] FROM [docpermit_pic] ORDER BY [pic_name]">
                </asp:SqlDataSource>
                
                <input type="checkbox" id="chkpic2" runat="server" style="visibility:hidden;" />
                <asp:DropDownList ID="txtpic2" runat="server" style="visibility:hidden; width:200px;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email">
                </asp:DropDownList>
                
                <input type="checkbox" id="chkpic3" runat="server" style="visibility:hidden;" />
                <asp:DropDownList ID="txtpic3" runat="server" style="visibility:hidden; width:200px;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email">
                </asp:DropDownList>
                
                <input type="checkbox" id="chkpic4" runat="server" style="visibility:hidden;" />
                <asp:DropDownList ID="txtpic4" runat="server" style="visibility:hidden; width:200px;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email">
                </asp:DropDownList>
                
                <input type="checkbox" id="chkpic5" runat="server" style="visibility:hidden;" />
                <asp:DropDownList ID="txtpic5" runat="server" style="visibility:hidden; width:200px;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email">
                </asp:DropDownList>
                </div>
              
    </div> 
   
        <%=js%>
        <%=msgbox %>
        
</asp:Content>