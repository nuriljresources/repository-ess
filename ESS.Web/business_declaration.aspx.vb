﻿Imports System.Data.SqlClient

Partial Public Class business_declaration
    Inherits System.Web.UI.Page
    Public decno As String
    Public genid As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnPaymentUsage.Visible = ESS.Common.SITE_SETTINGS.PRINT_PAYMENT_APP

        'Dim dtmnow As String
        'Dim dtynow As String
        'Dim stripno As String
        'Dim i As Integer

        'Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        'Try
        '    conn.Open()

        '    dtmnow = DateTime.Now.ToString("MM")
        '    dtynow = Date.Now.Year.ToString
        '    stripno = dtynow + dtmnow + "/" + "D" + "/"

        '    Dim dtb As DataTable = New DataTable()
        '    Dim strcon As String = "select Decno from V_H002 where Decno like '%" + stripno + "%'"
        '    Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, conn)
        '    sda.Fill(dtb)

        '    If dtb.Rows.Count = 0 Then
        '        decno = stripno + "000001"
        '    ElseIf dtb.Rows.Count > 0 Then
        '        i = 0
        '        i = dtb.Rows.Count + 1
        '        If dtb.Rows.Count > 0 And dtb.Rows.Count < 9 Then
        '            decno = stripno + "00000" + i.ToString
        '        ElseIf dtb.Rows.Count >= 9 And dtb.Rows.Count < 99 Then
        '            decno = stripno + "0000" + i.ToString
        '        ElseIf dtb.Rows.Count >= 99 And dtb.Rows.Count < 999 Then
        '            decno = stripno + "000" + i.ToString
        '        ElseIf dtb.Rows.Count >= 999 And dtb.Rows.Count < 9999 Then
        '            decno = stripno + "00" + i.ToString
        '        ElseIf dtb.Rows.Count >= 9999 And dtb.Rows.Count < 99999 Then
        '            decno = stripno + "0" + i.ToString
        '        ElseIf dtb.Rows.Count >= 99999 And dtb.Rows.Count < 999999 Then
        '            decno = stripno + i.ToString
        '        ElseIf dtb.Rows.Count >= 999999 Then
        '            decno = "Error on generate Tripnum"
        '        End If
        '    End If
        '    conn.Close()
        'Catch ex As Exception

        'End Try
        If Session("permission") = "" Or Session("permission") = Nothing Then
            Response.Redirect("Login.aspx", False)
            Exit Sub
        End If

        decdate.Text = Date.Today().ToString("MM/dd/yyyy")
        Try
            Dim conn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
            conn2.Open()
            Dim dtb2 As DataTable = New DataTable()
            Dim strcon2 As String = "Select max(genid) as genid from V_H00105"
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, conn2)
            sda2.Fill(dtb2)

            If dtb2.Rows.Count = 1 Then
                If genid = Nothing Then
                    genid = 0
                Else
                    genid = dtb2.Rows(0)!genid
                End If
            Else
                genid = 0
            End If

            conn2.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub cetak_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cetak.Click
        Response.Redirect("business_dec_trip_report.aspx?id=" + Session("decno"))
    End Sub

    Protected Sub cetak_Click_Payment(ByVal sender As Object, ByVal e As EventArgs) Handles btnPaymentUsage.Click
        Response.Redirect("payment_application_internal_form.aspx")
    End Sub

End Class