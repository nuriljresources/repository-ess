<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="signin_permit.aspx.vb" Inherits="EXCELLENT.signin_permit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head id="Head1" runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="description" content="Permit" />
<meta name="keywords" content="Permit" />
<script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="../Scripts/jscal2.js" type ="text/javascript"></script>
<script src="../Scripts/en.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="../css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="../css/win2k/win2k.css" />
<title>Document Center Login</title>
<style type="text/css" >
        #form1
        {
            font-family:Tahoma,Arial;
        }
        #form1 h1 
        {
            color: #33ADFF;
            font-size:x-large;
        }
        .title
        {
            color: blue;
            font-size: small;
            margin-bottom:10px;
        }
        .line
        {
            width: 100%;
            height: 20px;
        }
        .general
        {
            font-size:0.8em;
            color:#33ADFF;
            font-weight:bold;
            border-style:solid;
            border-width:1px;
            border-color:#33ADFF;
            padding-left:5px;
        }
        
        .general a
        {
            text-decoration:none;
            color:#33ADFF;
        }
        
        .general a:hover
        {
            text-decoration:underline;
            color:#33ADFF;
        }
    </style>
    <script language="javascript" type="text/javascript">
        var BrowserDetect = {
            init: function() {
                this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
                this.version = this.searchVersion(navigator.userAgent)
			        || this.searchVersion(navigator.appVersion)
			        || "an unknown version";
                this.OS = this.searchString(this.dataOS) || "an unknown OS";
            },
            searchString: function(data) {
                for (var i = 0; i < data.length; i++) {
                    var dataString = data[i].string;
                    var dataProp = data[i].prop;
                    this.versionSearchString = data[i].versionSearch || data[i].identity;
                    if (dataString) {
                        if (dataString.indexOf(data[i].subString) != -1)
                            return data[i].identity;
                    }
                    else if (dataProp)
                        return data[i].identity;
                }
            },
            searchVersion: function(dataString) {
                var index = dataString.indexOf(this.versionSearchString);
                if (index == -1) return;
                return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
            },
            dataBrowser: [
		        {
		            string: navigator.userAgent,
		            subString: "Chrome",
		            identity: "Chrome"
		        },
		        { string: navigator.userAgent,
		            subString: "OmniWeb",
		            versionSearch: "OmniWeb/",
		            identity: "OmniWeb"
		        },
		        {
		            string: navigator.vendor,
		            subString: "Apple",
		            identity: "Safari",
		            versionSearch: "Version"
		        },
		        {
		            prop: window.opera,
		            identity: "Opera"
		        },
		        {
		            string: navigator.vendor,
		            subString: "iCab",
		            identity: "iCab"
		        },
		        {
		            string: navigator.vendor,
		            subString: "KDE",
		            identity: "Konqueror"
		        },
		        {
		            string: navigator.userAgent,
		            subString: "Firefox",
		            identity: "Firefox"
		        },
		        {
		            string: navigator.vendor,
		            subString: "Camino",
		            identity: "Camino"
		        },
		        {		// for newer Netscapes (6+)
		            string: navigator.userAgent,
		            subString: "Netscape",
		            identity: "Netscape"
		        },
		        {
		            string: navigator.userAgent,
		            subString: "MSIE",
		            identity: "Explorer",
		            versionSearch: "MSIE"
		        },
		        {
		            string: navigator.userAgent,
		            subString: "Gecko",
		            identity: "Mozilla",
		            versionSearch: "rv"
		        },
		        { 		// for older Netscapes (4-)
		            string: navigator.userAgent,
		            subString: "Mozilla",
		            identity: "Netscape",
		            versionSearch: "Mozilla"
		        }
	        ],
            dataOS: [
		        {
		            string: navigator.platform,
		            subString: "Win",
		            identity: "Windows"
		        },
		        {
		            string: navigator.platform,
		            subString: "Mac",
		            identity: "Mac"
		        },
		        {
		            string: navigator.userAgent,
		            subString: "iPhone",
		            identity: "iPhone/iPod"
		        },
		        {
		            string: navigator.platform,
		            subString: "Linux",
		            identity: "Linux"
		        }
	        ]

        };
        BrowserDetect.init();
    </script>
    
    <script type="text/javascript">

        /***********************************************
        * Cool DHTML tooltip script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
        * This notice MUST stay intact for legal use
        * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
        ***********************************************/

        var offsetxpoint = -60 //Customize x offset of tooltip
        var offsetypoint = 20 //Customize y offset of tooltip
        var ie = document.all
        var ns6 = document.getElementById && !document.all
        var enabletip = false
        if (ie || ns6)
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : ""

        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body
        }

        function ddrivetip(thetext, thecolor, thewidth) {
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") tipobj.style.width = thewidth + "px"
                if (typeof thecolor != "undefined" && thecolor != "") tipobj.style.backgroundColor = thecolor
                tipobj.innerHTML = thetext
                enabletip = true
                return false
            }
        }

        function positiontip(e) {
            if (enabletip) {
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var rightedge = ie && !window.opera ? ietruebody().clientWidth - event.clientX - offsetxpoint : window.innerWidth - e.clientX - offsetxpoint - 20
                var bottomedge = ie && !window.opera ? ietruebody().clientHeight - event.clientY - offsetypoint : window.innerHeight - e.clientY - offsetypoint - 20

                var leftedge = (offsetxpoint < 0) ? offsetxpoint * (-1) : -1000

                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth)
                //move the horizontal position of the menu to the left by it's width
                    tipobj.style.left = ie ? ietruebody().scrollLeft + event.clientX - tipobj.offsetWidth + "px" : window.pageXOffset + e.clientX - tipobj.offsetWidth + "px"
                else if (curX < leftedge)
                    tipobj.style.left = "5px"
                else
                //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = curX + offsetxpoint + "px"

                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight)
                    tipobj.style.top = ie ? ietruebody().scrollTop + event.clientY - tipobj.offsetHeight - offsetypoint + "px" : window.pageYOffset + e.clientY - tipobj.offsetHeight - offsetypoint + "px"
                else
                    tipobj.style.top = curY + offsetypoint + "px"
                tipobj.style.visibility = "visible"
            }
        }

        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false
                tipobj.style.visibility = "hidden"
                tipobj.style.left = "-1000px"
                tipobj.style.backgroundColor = ''
                tipobj.style.width = ''
            }
        }

        document.onmousemove = positiontip

</script>
</head>
<body>
 <form id="form1" runat="server">
 <center>        
       <div><center><h2>DOCUMENT CENTER</h2><br />
       <table class="data-table" style="border: 1px solid #0066ff; width: 25%; background-color:#33ADFF;" >
            <tr>
            <td colspan="2" bgcolor="#0066ff" 
                    
                    style="text-align:center;font-size: large; font-family: calibri; font-weight: bold; width:100%">
                LOGIN
            </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label1" runat="server" Text="Domain\UserName" Font-Size="Small"></asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox1" runat="server" class="tb10" Width="92%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label2" runat="server" Text="Label" Font-Size="Small">Password</asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox2" TextMode="password" runat="server" class="tb10" Width="92%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>                
            </tr>
            <tr>
                <td style="width:40%">
                    <img alt="" src="../images/Locked.png" />
                </td>
                <td style="width:60%">
                    <asp:Button ID="btnLogin" runat="server" Text="Login" Width="80" Font-Names="Calibri"/>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="80" Font-Names="Calibri" />
                </td>
            </tr>
            <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblConfirm" runat="server" Text="" Font-Size="Small" ForeColor="Red"></asp:Label>
            </td>
            </tr>
        </table> </center>
    </div>
    </center>
 </form> 
 </body> 

</html>
