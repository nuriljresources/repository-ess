﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="transaction.aspx.vb"
   Inherits="EXCELLENT.transaction" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Import Namespace="SquishIt.Framework" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
   <title>BSC Transaction</title>
   <link href="css/transaction.css" rel="stylesheet" type="text/css" />

   <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>

   <script src="../Scripts/json2.js" type="text/javascript"></script>

   <script src="scripts/transaction.js" type="text/javascript"></script>

   <script src="scripts/jquery.metadata.js" type="text/javascript"></script>

   <script src="scripts/autonumeric.js" type="text/javascript"></script>

   <script src="scripts/decimalmask.js" type="text/javascript"></script>

   <style type="text/css">
      ul.dropdown *.dir1
      {
         padding-right: 20px;
         background-image: url("/remo/css/dropdown/themes/default/images/nav-arrow-down.png");
         background-position: 100% 50%;
         background-repeat: no-repeat;
         background-color: #FFA500;
      }
      body
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
      }
      input
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         font-size: 12px;
         text-align: right;
      }
      .ach
      {
         color: black;
         background: transparent;
         width: 85%;
      }
      input.btn
      {
         color: #050;
         font: bold 90% 'trebuchet ms' ,helvetica,sans-serif;
         background-color: #fed;
         border: 1px solid;
         border-color: #696 #363 #363 #696;
         filter: progid:DXImageTransform.Microsoft.Gradient (GradientType=0,StartColorStr= '#ffffffff' ,EndColorStr= '#ffeeddaa' );
         padding: 0 10px 0 10px;
         cursor: hand;
      }
      select.plains
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }
   </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="Release">
   </asp:ToolkitScriptManager>
   <asp:HiddenField ID="userid" runat="server" />
   <asp:HiddenField ID="userip" runat="server" />
   <asp:HiddenField ID="aapproved" runat="server" />
   <asp:HiddenField ID="param" runat="server" />
   <asp:HiddenField ID="idtorecalc" runat="Server" />
   <div>
      <table class="bsc" width="100%">
         <tr>
            <td class="style5" colspan="6">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Parameter
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
         </tr>
         <tr>
            <td class="style9" style="width: 5%">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tahun
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            <td class="style10" style="width: 10%">
               <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="true" CssClass="plains">
                  <asp:ListItem>2012</asp:ListItem>
                  <asp:ListItem>2013</asp:ListItem>
                  <asp:ListItem>2014</asp:ListItem>
               </asp:DropDownList>
            </td>
            <td class="style11" style="width: 5%">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bulan
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            <td class="style6" style="width: 10%">
               <asp:DropDownList ID="ddlMonth" runat="server" AutoPostBack="true" CssClass="plains">
                  <asp:ListItem Value="1">January</asp:ListItem>
                  <asp:ListItem Value="2">February</asp:ListItem>
                  <asp:ListItem Value="3">March</asp:ListItem>
                  <asp:ListItem Value="4">April</asp:ListItem>
                  <asp:ListItem Value="5">May</asp:ListItem>
                  <asp:ListItem Value="6">June</asp:ListItem>
                  <asp:ListItem Value="7">July</asp:ListItem>
                  <asp:ListItem Value="8">August</asp:ListItem>
                  <asp:ListItem Value="9">September</asp:ListItem>
                  <asp:ListItem Value="10">October</asp:ListItem>
                  <asp:ListItem Value="11">November</asp:ListItem>
                  <asp:ListItem Value="12">Desember</asp:ListItem>
               </asp:DropDownList>
            </td>
            <td class="style7" style="width: 5%">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Level
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            <td class="style8" style="width: 60%">
               <asp:DropDownList ID="KPILevel" runat="server" AutoPostBack="true" CssClass="plains">
                  <asp:ListItem Value="0" Text="JRN"></asp:ListItem>
                  <asp:ListItem Value="4" Text="Direktorat"></asp:ListItem>
                  <asp:ListItem Value="1" Text="Function"></asp:ListItem>
                  <asp:ListItem Value="2" Text="Section"></asp:ListItem>
                  <asp:ListItem Value="3" Text="Jobsite"></asp:ListItem>
               </asp:DropDownList>
               <asp:DropDownList ID="KPIDirektorat" runat="server" CssClass="plains">
               </asp:DropDownList>
               <asp:DropDownList ID="KPIFunc" runat="server" Visible="False" CssClass="plains">
               </asp:DropDownList>
               <asp:DropDownList ID="KPISection" runat="server" Visible="False" CssClass="plains">
               </asp:DropDownList>
               <asp:DropDownList ID="KPIJobsite" runat="server" Visible="False" CssClass="plains">
               </asp:DropDownList>
               <asp:DropDownList ID="KPIViewType" runat="server" CssClass="plains" AutoPostBack="true">
                  <asp:ListItem Value="0" Text="MONTHLY"></asp:ListItem>
                  <asp:ListItem Value="1" Text="YEARLY"></asp:ListItem>
                  <asp:ListItem Value="2" Text="YTD"></asp:ListItem>
               </asp:DropDownList>
               <asp:Button ID="btnSearch" runat="server" Text="Search" class="btn" />
               <asp:Button ID="btnCreate" runat="server" Text="REFRESH" class="btn" />
               <asp:Button ID="btnCalc" runat="server" Text="KONSOLIDASI" class="btn" OnClientClick="dokonsolidasi();return false;" />
               <%--<asp:Button ID="btnYTD" runat="server" Text="YTD" />--%></td>
         </tr>
      </table>
      <br />
      <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
      <%--<asp:Button ID="btnCreate" runat="server" Text="Create" Visible="False" />
                <%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" Visible="False" />--%>
      <br />
      <table width="100%">
         <tr>
            <td class="style5">
               <table style="width: 100%">
                  <tr>
                     <td>
                        <div id="header1" runat="Server" style="border: solid 1 black; width: 100%; font-size: 20px;
                           vertical-align: bottom" align="center">
                        </div>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <input type="button" id="btnPrint" runat="Server" style="visibility: hidden" value="View Report"
                           onclick="viewrpt();" class="btn" />
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td>
               <div id="result" runat="Server" style="width: 100%">
               </div>
            </td>
         </tr>
         <tr>
            <td align="right">
               <input type="button" id="btnSaveNew" style="visibility: hidden; background-image: url(../images/save-icon-small.png);
                  background-repeat: no-repeat; cursor: hand; width: 55px; height: 55px; background-color: transparent;
                  border: 0" runat="Server" onclick="javascript:save();" />
               <input type="button" id="btnApprove" style="visibility: hidden; background-image: url(../images/approve.png);
                  background-repeat: no-repeat; cursor: hand; width: 55px; height: 55px; background-color: transparent;
                  border: 0" runat="Server" onclick="javascript:saveapprove();" />
               <input type="button" id="btnUnApprove" style="visibility: hidden; background-image: url(../images/unapproved.png);
                  background-repeat: no-repeat; cursor: hand; width: 55px; height: 55px; background-color: transparent;
                  border: 0" runat="Server" onclick="javascript:unapprove();" />
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
         </tr>
      </table>
      <%--<asp:Repeater ID="rptTrans" runat="server">
                    <HeaderTemplate>
                        <table style="border:1px solid black;font-family: Arial, Helvetica, sans-serif; font-size: small;" border="1">
                            <tr style="background-color:#00CC00;color:White;">
                                <td>Strategic Theme</td>
                                <td>Strategic Objective</td>
                                <td>SO Leader</td>
                                <td>KPI</td>
                                <td>KPI Owner</td>
                                <td>UoM</td>
                                <td>Formula</td>
                                <td>Target</td>
                                <td>Actual</td>
                                <td>Achievement</td>
                                <td>Trend</td>
                                <td>Create PICA</td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                           
                            <tr>
                                <td><%#RenderGroup(DataBinder.Eval(Container.DataItem, "st"))%></td>
                                <td style="background-color:#CCCCCC"><%#DataBinder.Eval(Container.DataItem, "so")%></td>
                                <td style="background-color:#CCCCCC"><%#DataBinder.Eval(Container.DataItem, "soleader")%></td>
                                <td style="background-color:#CCCCCC"><%#DataBinder.Eval(Container.DataItem, "kpi")%></td>
                                <td style="background-color:#CCCCCC"><%#DataBinder.Eval(Container.DataItem, "kpiowner")%></td>
                                <td style="background-color:#CCCCCC"><%#DataBinder.Eval(Container.DataItem, "uom")%></td>
                                <td style="background-color:#CCCCCC"><%#DataBinder.Eval(Container.DataItem, "formula")%></td>
                                <td><asp:textbox ID="target" runat="Server" Columns="10" Text='<%#replacenull(DataBinder.Eval(Container.DataItem, "target"))%>'></asp:textbox></td>
                                <td><asp:textbox ID="actual" runat="Server" Columns="10" Text='<%#replacenull(DataBinder.Eval(Container.DataItem, "actual"))%>'></asp:textbox></td>
                                <td style="background-color:Gray"><asp:HiddenField id="idhidden" runat="Server" value='<%#DataBinder.Eval(Container.DataItem, "id")%>'></asp:HiddenField><asp:HiddenField id="oldach" runat="Server" value='<%#DataBinder.Eval(Container.DataItem, "oldach")%>'></asp:HiddenField><asp:textbox ID="achievement" runat="Server" Columns="10" Enabled="false" text='<%#replacenull(DataBinder.Eval(Container.DataItem, "achievement"))%>'></asp:textbox></td>
                                <td><asp:ImageButton ID="imgtrend" runat="server" CommandArgument="viewdetail" CommandName="viewdetail"/></td>
                                <td align="center">
                                    <!--<asp:Button ID="btn" runat="server" Text="Create PICA" CommandArgument="pica" CommandName="pica" />!-->
                                    <!--<asp:ImageButton id="imgbtn" runat="Server" imageurl="images/add.gif" />!-->
                                    <input type="button" id="btnAdd" value="Add Detail" onclick="javascript:openmodal('<%#DataBinder.Eval(Container.DataItem, "id")%>','<%#DataBinder.Eval(Container.DataItem, "level")%>','<%#DataBinder.Eval(Container.DataItem, "year")%>','<%#DataBinder.Eval(Container.DataItem, "month")%>','<%#DataBinder.Eval(Container.DataItem, "kpi")%>')" />
                                </td>
                            </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>--%>
      <br />
      <%-- <asp:Button ID="btnSave" runat="server" Text="Save" />--%>
      <br />
      <!--
                <asp:Panel ID="Panel1" runat="server" Visible="false">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Label ID="Label1" runat="server"></asp:Label>
                <asp:PlaceHolder ID="PlaceHolder1" runat="server">
                <asp:Table runat="server" ID="PICAtabel">
                    <asp:TableRow>
                        <asp:TableCell>Tanggal PICA</asp:TableCell>
                        <asp:TableCell>PI</asp:TableCell>
                        <asp:TableCell>CA</asp:TableCell>
                        <asp:TableCell>Target Hasil</asp:TableCell>
                        <asp:TableCell>PIC</asp:TableCell>
                        <asp:TableCell>Due Date</asp:TableCell>
                        <asp:TableCell>Status</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top"> 
                        <asp:TextBox ID="txtTglPICA1" runat="server" CssClass="newStyle1" Columns="14"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" 
                                Enabled="True" TargetControlID="txtTglPICA1">
                            </asp:CalendarExtender></asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                             <asp:TextBox ID="txtPI1" runat="server" Columns="30" CssClass="newStyle1" 
                                Rows="5" TextMode="MultiLine"></asp:TextBox>
                            <asp:Button ID="btnAddPI1" runat="server" CssClass="newStyle1" Text="Add" />
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                             <asp:TextBox ID="txtCA1" runat="server" Columns="30" CssClass="newStyle1" 
                                Rows="5" TextMode="MultiLine"></asp:TextBox>
                            <asp:Button ID="btnAddCA1" runat="server" CssClass="newStyle1" Text="Add" />
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                            <asp:TextBox ID="txtTarget1" runat="server" CssClass="newStyle1"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                            <asp:TextBox ID="txtPIC1" runat="server" CssClass="newStyle1" Columns="20"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                            <asp:TextBox ID="txtDueDate1" runat="server" CssClass="newStyle1" Columns="14"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" 
                                Enabled="True" TargetControlID="txtDueDate1">
                            </asp:CalendarExtender>
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                             <asp:DropDownList ID="ddlStatus1" runat="server" CssClass="newStyle1">
                                <asp:ListItem Selected="True" Value="0">Open</asp:ListItem>
                                <asp:ListItem Value="1">Close</asp:ListItem>
                            </asp:DropDownList>
                            </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                </asp:PlaceHolder>
                <br />
                
                
                <br />
                <asp:HiddenField ID="hdnval" runat="server" />
                <br />
            </ContentTemplate>
        </asp:UpdatePanel>
                </asp:Panel>
                !-->
   </div>

   <script type="text/javascript">
      calc();
   </script>

</asp:Content>
