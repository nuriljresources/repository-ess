﻿Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms

Partial Public Class emp_permit_form
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strTransId As String = Request.QueryString("Id")
        Dim message As String = String.Empty

        If Not IsPostBack Then
            message = String.Empty

            If Not String.IsNullOrEmpty(strTransId) Then

                Dim conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
                conn.Open()
                Dim strQueryDocument As String
                strQueryDocument = <![CDATA[
                    SELECT PERMIT.[transid],                 
                    PERMIT.[nik],                 
                    EMP.Nama,                
                    PERMIT.[LType],                 
                    ISNULL(PERMIT.[ltypec], '') AS [ltypec],                 
                    ISNULL(CSUB.CatRemark, '') AS  [TypeSub],                
                    PERMIT.[trans_date],                 
                    PERMIT.[trans_date2],                 
                    PERMIT.[totleave],                 
                    (CASE ISNULL(CSUB.CatRemark,'-') WHEN '-' THEN PERMIT.[remarks] ELSE CSUB.CatRemark + '-----' + PERMIT.[remarks] END) AS [remarks],
                    PERMIT.[fstatus],                 
                    PERMIT.[kdsite],                 
                    PERMIT.[StEdit],                
                    PERMIT.[CreatedBy]

                    FROM [L_H006] PERMIT                 
                    INNER JOIN [H_A101] EMP ON PERMIT.nik = EMP.NIK                
                    LEFT JOIN [L_A006C] CSUB ON PERMIT.ltypec = CSUB.CatCode 
                    WHERE PERMIT.transid = '{0}'
                    ]]>.Value

                strQueryDocument = String.Format(strQueryDocument, strTransId)

                Dim dtRow As DataTable = New DataTable
                Dim sqlTotalAdapter As SqlDataAdapter = New SqlDataAdapter(strQueryDocument, conn)
                sqlTotalAdapter.Fill(dtRow)
                conn.Close()

                If dtRow.Rows.Count > 0 Then
                    Dim Item As System.Data.DataRow = dtRow.Rows(0)
                    If Item("CreatedBy") = Session("niksite") Or Item("Nik") = Session("niksite") Then

                        ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local
                        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Laporan/rpt_employee_permit.rdlc")
                        Dim dsEmployeePermit As DataSetReport = New DataSetReport

                        Dim conString As String = ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString
                        Dim cmd As New SqlCommand(String.Format("EXEC sp_employee_permit '{0}'", strTransId))
                        Using con As New SqlConnection(conString)
                            Using sda As New SqlDataAdapter()
                                cmd.Connection = con
                                sda.SelectCommand = cmd
                                sda.Fill(dsEmployeePermit, "sp_employee_permit")
                            End Using
                        End Using

                        Dim datasource As New ReportDataSource("EmployeePermit_sp_employee_permit", dsEmployeePermit.Tables(0))
                        ReportViewer1.LocalReport.DataSources.Clear()
                        ReportViewer1.LocalReport.DataSources.Add(datasource)


                    Else
                        message = "Document " + strTransId + " User Not Authorized'"
                    End If
                Else
                    message = "Document " + strTransId + " Not Found'"
                End If
            Else
                message = "Document Not Found"
            End If


            Label1.Text = message

        End If


    End Sub

End Class