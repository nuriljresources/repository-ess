﻿Public Partial Class _defaultbsc
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Function MAP(ByVal s As String) As String
        Select Case s
            Case "1"
                Return "MSD"
            Case "2"
                Return "MSD Officer"
            Case "3"
                Return "BOD"
            Case "4"
                Return "GM"
            Case "5"
                Return "MGR"
            Case "6"
                Return "PM/DPM"
            Case "7"
                Return "S. Head"
            Case "8"
                Return "PIC PDCA"
        End Select
        Return "Others"
    End Function
End Class