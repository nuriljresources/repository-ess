﻿Imports System.Data.SqlClient
Partial Public Class mgmnt_user_edit
    Inherits System.Web.UI.Page
    Public msgbox As String
    Public mnentry As String
    Public mnview As String
    Public mnmgmt As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        sqlConn.Open()

        If Session("otorisasi_permit") = Nothing Or Session("otorisasi_permit") = "" Then
            Response.Redirect("signin_permit.aspx")
        End If

        If Trim(Session("permission_permit")) = "1" Or Trim(Session("permission_permit")) = "2" Then
            mnentry = "<div id='mnentry'><img alt='entry document' src='../images/open_doc_small.png' /> <a href='doc_center_entry.aspx'>&nbsp;Set Document</a></div>"
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View List</a></div>"
        End If

        If Trim(Session("permission_permit")) = "1" Then
            mnmgmt = "<div id='mnmgmt'> <img alt='Management User' src='../images/open_doc_small.png' /> <a href='mgmnt_list.aspx'>&nbsp;Management User</a></div>"
        End If

        If Trim(Session("permission_permit")) = "3" Or Trim(Session("permission_permit")) = "4" Then
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View List</a></div>"
        End If

        If Not IsPostBack Then
            If Session("docedituser") <> "" Then
                Dim strdocuser As String = "select * from ms_user where emp_code = '" + Session("docedituser") + "' and modul = '" + Session("modul_permit") + "'"

                Dim dtb As DataTable = New DataTable
                Dim sda As SqlDataAdapter = New SqlDataAdapter(strdocuser, sqlConn)

                sda.Fill(dtb)

                If dtb.Rows.Count >= 0 Then
                    name.Text = Trim(dtb.Rows(0)!name.ToString)
                    nik.Text = Trim(dtb.Rows(0)!emp_code.ToString)
                    nm.Text = Trim(dtb.Rows(0)!name.ToString)
                    ddllvl.SelectedValue = Trim(dtb.Rows(0)!lvl.ToString)
                    ddlmodul.SelectedValue = Trim(dtb.Rows(0)!modul.ToString)
                    'ddlmailsch.SelectedValue = Trim(dtb.Rows(0)!schmail.ToString)
                End If
            End If
            sqlConn.Close()
        End If

        If Session("modul_permit") <> "IT" Then
            ddlmodul.SelectedValue = Session("modul_permit")
            ddlmodul.Enabled = False
        Else
            ddlmodul.SelectedValue = Session("modul_permit")
            ddllvl.Items.Add(New ListItem("Reminder Only", "R"))
        End If

    End Sub

    Protected Sub Save_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Save.Click
        Dim dtbfindemp As DataTable = New DataTable
        Dim cmd As SqlCommand
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Try
            sqlConn.Open()
            Dim sqlQuery As String = "UPDATE ms_user SET emp_code = '" + nik.Text + "', employee_nik = '" + nik.Text + "', name = '" + nm.Text + "', lvl = '" + ddllvl.SelectedValue + "', modul = '" + ddlmodul.SelectedValue + "' where emp_code = '" + nik.Text + "'"
            cmd = New SqlCommand(sqlQuery, sqlConn)
            cmd.ExecuteScalar()
            msgbox = "<script language='javascript' type='text/javascript'> alert('data has been updated') </script>"
            Session("docedituser") = ""

            Response.Redirect("mgmnt_list.aspx")
        Catch ex As Exception

        Finally
            sqlConn.Close()
        End Try
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        tny.Text = "Are you sure want to remove this user?"
        yes.Visible = True
        no.Visible = True
    End Sub

    Protected Sub yes_Click(ByVal sender As Object, ByVal e As EventArgs) Handles yes.Click
        Dim dtbfindemp As DataTable = New DataTable
        Dim cmd As SqlCommand
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Try
            sqlConn.Open()
            Dim sqlQuery As String = "UPDATE ms_user SET stedit = '2' where emp_code = '" + nik.Text + "' and modul = '" + ddlmodul.SelectedValue + "'"
            cmd = New SqlCommand(sqlQuery, sqlConn)
            cmd.ExecuteScalar()
            msgbox = "<script language='javascript' type='text/javascript'> alert('data has been deleted') </script>"
            Session("docedituser") = ""

            Response.Redirect("mgmnt_list.aspx")
        Catch ex As Exception
            msgbox = "<script language='javascript' type='text/javascript'> alert('Failed to delete data') </script>"
        Finally
            sqlConn.Close()
        End Try
    End Sub

    Protected Sub no_Click(ByVal sender As Object, ByVal e As EventArgs) Handles no.Click
        tny.Text = ""
        yes.Visible = False
        no.Visible = False
    End Sub
End Class