﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="spl.aspx.vb" Inherits="EXCELLENT.spl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Overtime Request Form
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

<script src="Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="Scripts/jscal2.js" type ="text/javascript"></script>
    <script src="Scripts/en.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="css/win2k/win2k.css" />
    <link rel="stylesheet" href="css/jquery.tooltip/jquery.tooltip2.css" type="text/css" />
    <script type="text/javascript" src="Scripts/tooltip/jquery.min.js"></script>
    <script type="text/javascript" src="Scripts/tooltip/jquery.tooltip.js"></script>
    <script src="Scripts/jquery.timeentry.js" type="text/javascript"></script>
    
    <script type="text/javascript" >
        function OpenPopup(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("searchemployeespl.aspx?k=0", "List", "scrollbars=yes,resizable=no,width=600,height=500");
            return false;
        }

        function OpenPopupk(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("searchemployeespl.aspx?k=1", "List", "scrollbars=yes,resizable=no,width=600,height=500");
            return false;
        }

        function OpenPopupspv(key) {
            document.getElementById("ctrlToFind").value = key;
            dnik = document.getElementById("ctl00_ContentPlaceHolder1_hnik").value;
            dkddep = document.getElementById("ctl00_ContentPlaceHolder1_hkddepar").value;
            dkdjab = document.getElementById("ctl00_ContentPlaceHolder1_hkdjabat").value;
            dkdsite = document.getElementById("ctl00_ContentPlaceHolder1_txtsite").value;
            window.open("searchemployeesplapp.aspx?n=" + dnik + "&kdd=" + dkddep + "&kdj=" + dkdjab + "&kds=" + dkdsite + "", "List", "scrollbars=yes,resizable=no,width=600,height=500");
            return false;
        }

        function OpenPopupspv2(key) {
            document.getElementById("ctrlToFind").value = key;
            dkddep = document.getElementById("ctl00_ContentPlaceHolder1_hkddepar").value;
            dnik = document.getElementById("ctl00_ContentPlaceHolder1_hnik").value;
            dkdjab = document.getElementById("ctl00_ContentPlaceHolder1_hkdjabat").value;
            dkdsite = document.getElementById("ctl00_ContentPlaceHolder1_txtsite").value;
            dspv2 = document.getElementById("ctl00_ContentPlaceHolder1_hnikspv2").value;
            dhisgm = document.getElementById("ctl00_ContentPlaceHolder1_hisgm").value;
            window.open("searchemployeesplapp2.aspx?n=" + dnik + "&kdj=" + dkdjab + "&kds=" + dkdsite + "&kdd=" + dkddep + "&dspv2=" + dspv2 + "&dhisgm=" + dhisgm + "", "List", "scrollbars=yes,resizable=no,width=600,height=500");
            return false;
        }

        $(function() {
        $('#ctl00_ContentPlaceHolder1_txtstart').timeEntry({ show24Hours: true, spinnerImage: '' });
        $('#ctl00_ContentPlaceHolder1_txtfinish').timeEntry({ show24Hours: true, spinnerImage: '' });
        });

        
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="ctrlToFind" />
    <input type="hidden" id="ssite" value=<%=ssite %> />
    <asp:HiddenField id="recordid" runat="server" /> <asp:HiddenField id="regno" runat="server" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
    <caption style="text-align: center; font-size: 1.5em; color:White;">Overtime Request Form</caption>
    
    </table>
    <div style="border-style:solid; border-color:Gray; border-width:thin;">
    <table class="accordionContent" style="margin-left:10px;">
        <caption class="accordionHeader">Requestor (Minimum Supervisor)</caption>
        <tr>
            <td> Site </td>
            <td><asp:TextBox ID="txtsitereq" runat="server" ></asp:TextBox></td>
            <td> Costcode </td>
            <td> <asp:TextBox ID="txtcostcodereq" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="padding:1px 60px 1px 1px;">
                 Nik
            </td>
            <td>
                 <asp:TextBox ID="txtnikreq" runat="server"></asp:TextBox>    
                 <img alt="add" id="Img1" src="images/Search.gif" align="absmiddle" style="cursor: pointer;" onclick="OpenPopup('splreq')" />      
            </td>
            <td style="padding:1px 60px 1px 1px;">
                 Name       
            </td>
            <td>
                 <asp:HiddenField ID="hnikreq" runat="server" /> <asp:HiddenField ID="hkdsitereq" runat="server" /> <asp:HiddenField ID="hkddeparreq" runat="server" /> <asp:HiddenField ID="hkdjabatreq" runat="server" />
                 <asp:TextBox ID="txtnamareq" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                 Department
            </td>
            <td>
                 <asp:TextBox ID="txtdeptreq" runat="server"></asp:TextBox>       
            </td>
             <td>
                 
            </td>
            <td>
                 <asp:TextBox ID="txtpositionreq" Visible="false" runat="server"></asp:TextBox>       
            </td>
        </tr>
    </table>
    <br />
    </div> 
    <br />
    
    <div style="border-style:solid; border-color:Gray; border-width:thin;">
    <label class="accordionContent data-table-inside" style="font-size:1.2em;">&nbsp; Request Overtime for :</label>
    <table class="accordionContent" style="margin-left:10px;">
        <caption class="accordionHeader">Employee</caption>
        <tr>
            <td> Site </td>
            <td><asp:TextBox ID="txtsite" runat="server" ></asp:TextBox></td>
            <td> Costcode </td>
            <td> <asp:TextBox ID="txtcostcode" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="padding:1px 60px 1px 1px;">
                 Nik
            </td>
            <td>
                 <asp:TextBox ID="txtnik" runat="server"></asp:TextBox> 
                 <img alt="add" id="Img2" src="images/Search.gif" align="absmiddle" style="cursor: pointer; visibility:visible" onclick="OpenPopupk('spl')" />         
            </td>
            <td style="padding:1px 60px 1px 1px;">
                 Name       
            </td>
            <td>
                 <asp:HiddenField ID="hnik" runat="server" /> <asp:HiddenField ID="hkdsite" runat="server" /> <asp:HiddenField ID="hkddepar" runat="server" /> <asp:HiddenField ID="hkdjabat" runat="server" />
                 <asp:TextBox ID="txtnama" runat="server"></asp:TextBox>    
            </td>
        </tr>
        <tr>
            <td>
                 Department
            </td>
            <td>
                 <asp:TextBox ID="txtdept" runat="server"></asp:TextBox>       
            </td>
            <td>
            </td>
            <td>
                 <asp:TextBox ID="txtposition" Visible="false" runat="server"></asp:TextBox>       
            </td>
        </tr>
    </table>
    <br />
    <table class="data-table DynarchCalendar" style="margin-left:10px;">
    <tr><th>Request Date</th><th>Date Start</th><th>Time Start</th><th>Date Finish</th><th>Time Finish</th><th>Task</th></tr>
    <tr>
    <td><asp:TextBox ID="txtdate" BackColor="#B9B9BD" runat="server" style="text-align:center;"></asp:TextBox>      </td>
    <td><asp:TextBox ID="txtdatestart" runat="server" style="text-align:center;"></asp:TextBox><button id="btn2" style="height:20px; width:15px;">..</button></td> 
    <td><asp:TextBox ID="txtstart" runat="server" style="text-align:center;"></asp:TextBox></td>
    <td><asp:TextBox ID="txtdatefinish" runat="server" style="text-align:center;"></asp:TextBox><button id="btn3" style="height:20px; width:15px;">..</button></td>
    <td><asp:TextBox ID="txtfinish" runat="server" style="text-align:center;"></asp:TextBox></td>
   <%-- <td style="background-color:White;"><asp:Label ID="lbltotal" runat="server" style="text-align:center; width:10px; height:10px; background-color:White;"></asp:Label></td>--%>
    <td><asp:TextBox ID="txtket" Width="200px" runat="server" ></asp:TextBox></td>
    </tr>
    <tr><td style="text-align:right;">Approval 1 (Superintendent)</td>
    <td><asp:TextBox ID="txtspv1" runat="server" ></asp:TextBox> <asp:HiddenField ID="hnikspv1" runat="server" />
    <img alt="add" id="Img3" src="images/Search.gif" align="absmiddle" style="cursor: pointer; " onclick="OpenPopupspv('splspv1')" />
    </td>
    <td style="text-align:right;">Approval 2 (Manager)</td>
    <td><asp:TextBox ID="txtspv2" runat="server"></asp:TextBox> <asp:HiddenField ID="hnikspv2" runat="server" /> <asp:HiddenField ID="hisgm" runat="server" />
    <img alt="add" id="Img4" src="images/Search.gif" align="absmiddle" style="cursor: pointer;" onclick="OpenPopupspv2('splspv2')" />
    </td>
    <td><img alt="add" id="Img5" src="images/btn1.png" align="absmiddle" style="cursor: pointer; border:solid 0.1em grey; visibility:hidden" onclick="OpenPopupspv2('splspv2')" /></td>
    </tr>
    </table>
    <br />
    </div>
    <asp:Button ID="btnsave" Width="50px" Height="20px" Font-Size="0.8em" runat ="server" Text=" SAVE " class="button" />
    <asp:Button ID="btnsubmit" Width="110px" Height="20px" Font-Size="0.8em" runat ="server" Text="Send For Approval"  OnClick="btnsubmit_Click" OnClientClick="ctl00_ContentPlaceHolder1_btnsubmit.style.display = 'none'; ctl00_ContentPlaceHolder1_btnsave.style.display = 'none'" class="button" />
    <asp:Button ID="btnprint" Width="50px" Height="20px" Font-Size="0.8em" runat ="server" Text=" PRINT " class="button" />
    <br /> <br />
    <%--<table width="100%">
        <tr>
            <td>
                <strong style="color:red;">*Hasil Kerja</strong>
            </td>
            <td> </td>
        </tr>
        <tr>
            <td colspan ="2">
                <asp:TextBox ID="txthslkerja" runat="server" TextMode="MultiLine" Rows="10" Columns="70" Height="100px"></asp:TextBox>
            </td>
        </tr>
    </table>--%>
    
  <script type="text/javascript" >
      var y = '';
      var n = '';
      var today = new Date();
      today = 
      $now = new Date(today);
      $yesterday = new Date(today);
      $yesterday.setDate(today.getDate() - 2);
      var $yyyy = $yesterday.getFullYear();
      var $mm = $yesterday.getMonth() + 1;
      var $dd = $yesterday.getDate()
      if ($dd < 10) { $dd = '0' + $dd } if ($mm < 10) { $mm = '0' + $mm } y = $yyyy.toString() + $mm.toString() + $dd.toString();
      var sdate = parseInt(y);
      var d = document.getElementById("ctl00_ContentPlaceHolder1_txtdate").value
      var ssite = document.getElementById("ssite").value

      if (ssite == "BAK" || ssite == "LAN" || ssite == "JKT" || ssite == "PEN") {
          $now.setDate(today.getDate() - 2);
      } else {
        $now.setDate(today.getDate() - 1);
      }
      
      var $yyyy1 = $now.getFullYear();
      var $mm1 = $now.getMonth() + 1;
      var $dd1 = $now.getDate()
      if ($dd1 < 10) { $dd1 = '0' + $dd1 } if ($mm1 < 10) { $mm1 = '0' + $mm1 } n = $yyyy1.toString() + $mm1.toString() + $dd1.toString();
      var sdate1 = parseInt(n);
      
      if (today.getDay() == 1) {
          var cal = Calendar.setup({
              onSelect: function(cal) { cal.hide() },
              showTime: false,
              min: sdate
          });
      } else {
          var cal = Calendar.setup({
              onSelect: function(cal) { cal.hide() },
              showTime: false,
              min: sdate1
              
          });
      }
      cal.manageFields("btn2", "ctl00_ContentPlaceHolder1_txtdatestart", "%m/%d/%Y");
      cal.manageFields("btn3", "ctl00_ContentPlaceHolder1_txtdatefinish", "%m/%d/%Y");

      if (!$('#ctl00_ContentPlaceHolder1_txtspv1').val()) {
          document.getElementById("Img3").style.visibility = "hidden";
      }
      else {
          document.getElementById("Img3").style.visibility = "visible";
      }

      if (!$('#ctl00_ContentPlaceHolder1_txtspv2').val()) {
          document.getElementById("Img4").style.visibility = "hidden";
      }
      else {
          document.getElementById("Img4").style.visibility = "visible";
      }

      // new line, kondisi jika salah satu spv ada yang terisi, maka kedua button open popup dibuka
      var spv1Val = $('#ctl00_ContentPlaceHolder1_txtspv1');
      var spv2Val = $('#ctl00_ContentPlaceHolder1_txtspv2');
      if ((spv1Val != undefined && spv1Val.val() != '') || (spv2Val != undefined && spv2Val.val() != '')) {
          document.getElementById("Img3").style.visibility = "visible";
          document.getElementById("Img4").style.visibility = "visible";
      }

      function showbtn() {
          if (document.getElementById("ctl00_ContentPlaceHolder1_txtspv1").value == "") {
              document.getElementById("Img3").style.visibility = "hidden";
          }
          else {
              document.getElementById("Img3").style.visibility = "visible";
          }
      }

      function showbtn2() {
          if (document.getElementById("ctl00_ContentPlaceHolder1_txtspv1").value == "") {
              document.getElementById("Img4").style.visibility = "hidden";
          }
          else {
              document.getElementById("Img4").style.visibility = "visible";
          }
      }
 </script>
    <%=msg%>
</asp:Content>