﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PopUpTarget.aspx.vb" Inherits="EXCELLENT.PopUpTarget" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Setting Target</title>
    <meta http-equiv="Expires" content="0"/>
    <meta http-equiv="Cache-Control" content="no-cache"/>
    <meta http-equiv="Pragma" content="no-cache"/>
       
    <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/json2.js" type="text/javascript"></script>
    <script src="scripts/jquery.metadata.js" type="text/javascript"></script>
    <script src="scripts/autonumeric.js" type="text/javascript"></script>
    <script src="scripts/decimalmask.js" type="text/javascript"></script>
    <script type="text/javascript">
       function getQuerystring(key, default_) {
          if (default_ == null) default_ = "";
          key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
          var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
          var qs = regex.exec(window.location.href);
          if (qs == null)
             return default_;
          else
             return qs[1];
       }

       function stripNonNumeric(str) {
          str += '';
          var rgx = /^\d|\.|-$/;
          var out = '';
          for (var i = 0; i < str.length; i++) {
             if (rgx.test(str.charAt(i))) {
                if (!((str.charAt(i) == '.' && out.indexOf('.') != -1) ||
             (str.charAt(i) == '-' && out.length != 0))) {
                   out += str.charAt(i);
                }
             }
          }
          return out;
       }
       
       function recalc1() {
          switch (document.getElementById('calctype').options[document.getElementById('calctype').selectedIndex].value) {
             case "0": //SUM
                document.getElementById("at").disabled = "DISABLED";
                document.getElementById("at").value = parseFloat((document.getElementById("jan").value == '') ? 0 : stripNonNumeric(document.getElementById("jan").value)) +
                parseFloat((document.getElementById("feb").value == '') ? 0 : stripNonNumeric(document.getElementById("feb").value)) + parseFloat((document.getElementById("mar").value == '') ? 0 : stripNonNumeric(document.getElementById("mar").value)) +
                parseFloat((document.getElementById("apr").value == '') ? 0 : stripNonNumeric(document.getElementById("apr").value)) + parseFloat((document.getElementById("mei").value == '') ? 0 : stripNonNumeric(document.getElementById("mei").value)) +
                parseFloat((document.getElementById("jun").value == '') ? 0 : stripNonNumeric(document.getElementById("jun").value)) + parseFloat((document.getElementById("jul").value == '') ? 0 : stripNonNumeric(document.getElementById("jul").value)) +
                parseFloat((document.getElementById("agu").value == '') ? 0 : stripNonNumeric(document.getElementById("agu").value)) + parseFloat((document.getElementById("sep").value == '') ? 0 : stripNonNumeric(document.getElementById("sep").value)) +
                parseFloat((document.getElementById("okt").value == '') ? 0 : stripNonNumeric(document.getElementById("okt").value)) + parseFloat((document.getElementById("nov").value == '') ? 0 : stripNonNumeric(document.getElementById("nov").value)) +
                parseFloat((document.getElementById("des").value=='')?0:stripNonNumeric(document.getElementById("des").value));
                break;
             case "1": //AVG
                document.getElementById("at").disabled = "DISABLED";
                document.getElementById("at").value = parseFloat((document.getElementById("jan").value == '') ? 0 : stripNonNumeric(document.getElementById("jan").value)) +
                parseFloat((document.getElementById("feb").value == '') ? 0 : stripNonNumeric(document.getElementById("feb").value)) + parseFloat((document.getElementById("mar").value == '') ? 0 : stripNonNumeric(document.getElementById("mar").value)) +
                parseFloat((document.getElementById("apr").value == '') ? 0 : stripNonNumeric(document.getElementById("apr").value)) + parseFloat((document.getElementById("mei").value == '') ? 0 : stripNonNumeric(document.getElementById("mei").value)) +
                parseFloat((document.getElementById("jun").value == '') ? 0 : stripNonNumeric(document.getElementById("jun").value)) + parseFloat((document.getElementById("jul").value == '') ? 0 : stripNonNumeric(document.getElementById("jul").value)) +
                parseFloat((document.getElementById("agu").value == '') ? 0 : stripNonNumeric(document.getElementById("agu").value)) + parseFloat((document.getElementById("sep").value == '') ? 0 : stripNonNumeric(document.getElementById("sep").value)) +
                parseFloat((document.getElementById("okt").value == '') ? 0 : stripNonNumeric(document.getElementById("okt").value)) + parseFloat((document.getElementById("nov").value == '') ? 0 : stripNonNumeric(document.getElementById("nov").value)) +
                parseFloat((document.getElementById("des").value == '') ? 0 : stripNonNumeric(document.getElementById("des").value));
                var divider = 12;
                if (document.getElementById("jan").value=='') divider=divider-1;
                if (document.getElementById("feb").value=='') divider=divider-1;
                if (document.getElementById("mar").value=='') divider=divider-1;
                if (document.getElementById("apr").value=='') divider=divider-1;
                if (document.getElementById("mei").value=='') divider=divider-1;
                if (document.getElementById("jun").value=='') divider=divider-1;
                if (document.getElementById("jul").value=='') divider=divider-1;
                if (document.getElementById("agu").value=='') divider=divider-1;
                if (document.getElementById("sep").value=='') divider=divider-1;
                if (document.getElementById("okt").value=='') divider=divider-1;
                if (document.getElementById("nov").value=='') divider=divider-1;
                if (document.getElementById("des").value == '') divider = divider - 1;
                if (divider==0)
                  document.getElementById("at").value = 0
                else
                  document.getElementById("at").value = stripNonNumeric(document.getElementById("at").value) / divider;
                break;
             case "2":
                document.getElementById("at").disabled = "DISABLED";
                if (document.getElementById("jan").value != '') document.getElementById("at").value = stripNonNumeric(document.getElementById("jan").value);
                if (document.getElementById("feb").value != '') document.getElementById("at").value = stripNonNumeric(document.getElementById("feb").value);
                if (document.getElementById("mar").value != '') document.getElementById("at").value = stripNonNumeric(document.getElementById("mar").value);
                if (document.getElementById("apr").value != '') document.getElementById("at").value = stripNonNumeric(document.getElementById("apr").value);
                if (document.getElementById("mei").value != '') document.getElementById("at").value = stripNonNumeric(document.getElementById("mei").value);
                if (document.getElementById("jun").value != '') document.getElementById("at").value = stripNonNumeric(document.getElementById("jun").value);
                if (document.getElementById("jul").value != '') document.getElementById("at").value = stripNonNumeric(document.getElementById("jul").value);
                if (document.getElementById("agu").value != '') document.getElementById("at").value = stripNonNumeric(document.getElementById("agu").value);
                if (document.getElementById("sep").value != '') document.getElementById("at").value = stripNonNumeric(document.getElementById("sep").value);
                if (document.getElementById("okt").value != '') document.getElementById("at").value = stripNonNumeric(document.getElementById("okt").value);
                if (document.getElementById("nov").value != '') document.getElementById("at").value = stripNonNumeric(document.getElementById("nov").value);
                if (document.getElementById("des").value != '') document.getElementById("at").value = stripNonNumeric(document.getElementById("des").value);
                break;
             case "3":
                document.getElementById("at").disabled = "";
                break;
          }
       }

       function fncInputNumericValuesOnly() { if (!(event.keyCode == 45 || event.keyCode == 46 || event.keyCode == 48 || event.keyCode == 49 || event.keyCode == 50 || event.keyCode == 51 || event.keyCode == 52 || event.keyCode == 53 || event.keyCode == 54 || event.keyCode == 55 || event.keyCode == 56 || event.keyCode == 57)) { event.returnValue = false; } }
       function IsNumeric(input) { return (input - 0) == input && input.length > 0; }
       function doSave() {
          var answer = confirm("Simpan Data?");
          if (answer) {
             if (document.getElementById("jan").value == '' && document.getElementById("feb").value == '' && document.getElementById("mar").value == '' && document.getElementById("apr").value == ''
             && document.getElementById("mei").value == '' && document.getElementById("jun").value == '' && document.getElementById("jul").value == '' && document.getElementById("agu").value == ''
             && document.getElementById("sep").value == '' && document.getElementById("okt").value == '' && document.getElementById("nov").value == '' && document.getElementById("des").value == ''
             ) {
                 alert("Belum ada target yang diisi");
                return;
             }

             if (document.getElementById('calctype').options[document.getElementById('calctype').selectedIndex].value == '1' &&
             (document.getElementById("jan").value == '' && document.getElementById("feb").value == '' && document.getElementById("mar").value == '' && document.getElementById("apr").value == ''
             && document.getElementById("mei").value == '' && document.getElementById("jun").value == '' && document.getElementById("jul").value == '' && document.getElementById("agu").value == ''
             && document.getElementById("sep").value == '' && document.getElementById("okt").value == '' && document.getElementById("nov").value == '' && document.getElementById("des").value == '')) {
                alert("Perhitungan dengan Average tidak boleh semua target kosong");
                return;
             }

             if (document.getElementById('calctype').options[document.getElementById('calctype').selectedIndex].value == '2' &&
             (document.getElementById("jan").value == '' && document.getElementById("feb").value == '' && document.getElementById("mar").value == '' && document.getElementById("apr").value == ''
             && document.getElementById("mei").value == '' && document.getElementById("jun").value == '' && document.getElementById("jul").value == '' && document.getElementById("agu").value == ''
             && document.getElementById("sep").value == '' && document.getElementById("okt").value == '' && document.getElementById("nov").value == '' && document.getElementById("des").value == '')) {
                alert("Perhitungan dengan Last Value tidak boleh semua target kosong");
                return;
             }
             
             if (IsNumeric(stripNonNumeric(document.getElementById("jan").value)) == false && document.getElementById("jan").value != '') {
                alert("Target Januari Harus Berupa Numerik");
                return;
             } else if (IsNumeric(stripNonNumeric(document.getElementById("feb").value)) == false && document.getElementById("feb").value !=  '') {
                alert("Target Februari Harus Berupa Numerik");
                return;
             } else if (IsNumeric(stripNonNumeric(document.getElementById("mar").value)) == false && document.getElementById("mar").value !=  '') {
                alert("Target Maret Harus Berupa Numerik");
                return;
             } else if (IsNumeric(stripNonNumeric(document.getElementById("apr").value)) == false && document.getElementById("apr").value !=  '') {
                alert("Target April Harus Berupa Numerik");
                return;
             } else if (IsNumeric(stripNonNumeric(document.getElementById("mei").value)) == false && document.getElementById("mei").value !=  '') {
                alert("Target Mei Harus Berupa Numerik");
                return;
             } else if (IsNumeric(stripNonNumeric(document.getElementById("jun").value)) == false && document.getElementById("jun").value !=  '') {
                alert("Target Juni Harus Berupa Numerik");
                return;
             } else if (IsNumeric(stripNonNumeric(document.getElementById("jul").value)) == false && document.getElementById("jul").value !=  '') {
                alert("Target Juli Harus Berupa Numerik");
                return;
             } else if (IsNumeric(stripNonNumeric(document.getElementById("agu").value)) == false && document.getElementById("agu").value !=  '') {
                alert("Target Agustus Harus Berupa Numerik");
                return;
             } else if (IsNumeric(stripNonNumeric(document.getElementById("sep").value)) == false && document.getElementById("sep").value !=  '') {
                alert("Target September Harus Berupa Numerik");
                return;
             } else if (IsNumeric(stripNonNumeric(document.getElementById("okt").value)) == false && document.getElementById("okt").value !=  '') {
                alert("Target Oktober Harus Berupa Numerik");
                return;
             } else if (IsNumeric(stripNonNumeric(document.getElementById("nov").value)) == false && document.getElementById("nov").value !=  '') {
                alert("Target November Harus Berupa Numerik");
                return;
             } else if (IsNumeric(stripNonNumeric(document.getElementById("des").value)) == false && document.getElementById("des").value != '') {
                alert("Target Desember Harus Berupa Numerik");
                return;
             }
             recalc1();
             var x = new otarget(document.getElementById("versi").value,
             document.getElementById('cmbYear').options[document.getElementById('cmbYear').selectedIndex].value,
             document.getElementById("kpiid").value,
             document.getElementById('calctype').options[document.getElementById('calctype').selectedIndex].value,
             stripNonNumeric(document.getElementById("jan").value),
             stripNonNumeric(document.getElementById("feb").value),
             stripNonNumeric(document.getElementById("mar").value),
            stripNonNumeric( document.getElementById("apr").value),
             stripNonNumeric(document.getElementById("mei").value),
             stripNonNumeric(document.getElementById("jun").value),
             stripNonNumeric(document.getElementById("jul").value),
             stripNonNumeric(document.getElementById("agu").value),
             stripNonNumeric(document.getElementById("sep").value),
             stripNonNumeric(document.getElementById("okt").value),
             stripNonNumeric(document.getElementById("nov").value),
             stripNonNumeric(document.getElementById("des").value),
             document.getElementById("createdby").value,
             document.getElementById("createdin").value,
             stripNonNumeric(document.getElementById("at").value))
             document.getElementById("mySave").disabled = "disabled";
             $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "WS/BSCService.asmx/SaveTarget",
                data: "{'_target':" + JSON.stringify(x) + "}",
                dataType: "json",
                success: function(res) {
                   if (res.d == "OK") {
                      alert('Data Tersimpan');
                      document.getElementById("mySave").disabled = "";
                      window.close();
                   } else {
                      alert('Save Error, Harap Print Screen dan Hubungi IT');
                   }
                },
                error: function(err) {
                   alert(err.responseText);
                }
             });
          }
       }

       function otarget(Versi, Tahun, Kpiid, Calctype, Jan, Feb, Mar, Apr, Mei, Jun, Jul, Agu, Sep, Okt, Nov, Des, Createdby, Createdin, at) {
          this.Versi = Versi; this.Tahun = Tahun; this.Kpiid = Kpiid;
          this.Calctype = Calctype; this.Jan = Jan; this.Feb = Feb; this.Mar = Mar;
          this.Apr = Apr; this.Mei = Mei; this.Jun = Jun; this.Jul = Jul;
          this.Agu = Agu; this.Sep = Sep; this.Okt = Okt; this.Nov = Nov;
          this.Des = Des; this.Createdby = Createdby; this.Createdin = Createdin;this.at = at;
       }

       function checkpermission() {
          if (document.getElementById("permission").value == 'A' || document.getElementById("permission").value == '5') {
             document.getElementById("mySave").style.visibility = "hidden";
          }

          if (document.getElementById("permission").value == "2" && document.getElementById("level").value == 2 &&  (document.getElementById("sectiondepar").value!= 9))
          {
            document.getElementById("mySave").style.visibility = "hidden";
          }
       }

       function doCopy() {
         var x = document.getElementById("pasteinput").value;
         if (window.clipboardData) window.clipboardData.setData("Text", x);
         alert("Target Ter-Copy");
       }
       
       function doPaste(){
         if (window.clipboardData)
            pastedata = window.clipboardData.getData("Text");
         if (pastedata.split("\t")[0] != undefined && IsNumeric(stripNonNumeric(pastedata.split("\t")[0])))
            document.getElementById("jan").value = stripNonNumeric(pastedata.split("\t")[0]);
         if (pastedata.split("\t")[1] != undefined && IsNumeric(stripNonNumeric(pastedata.split("\t")[1])))
            document.getElementById("feb").value = stripNonNumeric(pastedata.split("\t")[1]);
         if (pastedata.split("\t")[2] != undefined && IsNumeric(stripNonNumeric(pastedata.split("\t")[2])))
            document.getElementById("mar").value = stripNonNumeric(pastedata.split("\t")[2]);
         if (pastedata.split("\t")[3] != undefined && IsNumeric(stripNonNumeric(pastedata.split("\t")[3])))
            document.getElementById("apr").value = stripNonNumeric(pastedata.split("\t")[3]);
         if (pastedata.split("\t")[4] != undefined && IsNumeric(stripNonNumeric(pastedata.split("\t")[4])))
            document.getElementById("mei").value = stripNonNumeric(pastedata.split("\t")[4]);
         if (pastedata.split("\t")[5] != undefined && IsNumeric(stripNonNumeric(pastedata.split("\t")[5])))
            document.getElementById("jun").value = stripNonNumeric(pastedata.split("\t")[5]);
         if (pastedata.split("\t")[6] != undefined && IsNumeric(stripNonNumeric(pastedata.split("\t")[6])))
            document.getElementById("jul").value = stripNonNumeric(pastedata.split("\t")[6]);
         if (pastedata.split("\t")[7] != undefined && IsNumeric(stripNonNumeric(pastedata.split("\t")[7])))
            document.getElementById("agu").value = stripNonNumeric(pastedata.split("\t")[7]);
         if (pastedata.split("\t")[8] != undefined && IsNumeric(stripNonNumeric(pastedata.split("\t")[8])))
            document.getElementById("sep").value = stripNonNumeric(pastedata.split("\t")[8]);
         if (pastedata.split("\t")[9] != undefined && IsNumeric(stripNonNumeric(pastedata.split("\t")[9])))
            document.getElementById("okt").value = stripNonNumeric(pastedata.split("\t")[9]);
         if (pastedata.split("\t")[10] != undefined && IsNumeric(stripNonNumeric(pastedata.split("\t")[10])))
            document.getElementById("nov").value = stripNonNumeric(pastedata.split("\t")[10]);
         if (pastedata.split("\t")[11] != undefined && IsNumeric(stripNonNumeric(pastedata.split("\t")[11])))
            document.getElementById("des").value = stripNonNumeric(pastedata.split("\t")[11]);         
         recalc1();
       }
    </script>
    <style type="text/css">
       input.btn{
           color:#050;
           font: bold 90% 'trebuchet ms',helvetica,sans-serif;
           background-color:#fed;
           border: 1px solid;
           border-color: #696 #363 #363 #696;
           filter:progid:DXImageTransform.Microsoft.Gradient
           (GradientType=0,StartColorStr='#ffffffff',EndColorStr='#ffeeddaa');
           padding:0 10px 0 10px;
           cursor:hand;
       }
       body{
         font-family:"trebuchet MS", verdana, sans-serif;
         font-size:small;
       }
       #wrapper{
         font-family:"trebuchet MS", verdana, sans-serif;
         font-size:small;
       }    
        td.styleHeader
        {
          background-color:#00CC00;
          color:white;
          font-weight:bold;
          font-size:medium;
        }
      table.act {
	      border-width: 1px;
	      border-spacing: 0px;
	      border-color: #C0C0C0;
	      border-collapse: collapse;
	      background-color: white;
	      width:100%;
         }
      table.act thead {
	      border-width: 1px;
	      padding: 1px;
	      border-color: #C0C0C0;
      }
      table.act td {
	      border-width: 1px;
	      padding: 1px;
	      border-color: #C0C0C0;
      }
      .style1
       {
          height: 23px;
       }
      input.plain
      {
         font-family:"trebuchet MS", verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }
      .plain:hover
      {
         background-color:#FFFFA8;
      }
      input.printe
      {
         font-family:"trebuchet MS", verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }
      .printe:hover
      {
         background-color:#FFFFA8;
      }
      select.plains{
         font-family:"trebuchet MS", verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }  
      textarea{
         font-family:"trebuchet MS", verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }   
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
         <asp:HiddenField id="kpiid" runat="Server" />
         <asp:HiddenField id="createdby" runat="Server" />
         <asp:HiddenField id="createdin" runat="Server" />
         <asp:HiddenField id="permission" runat="Server" />
         <asp:HiddenField id="level" runat="Server" />
         <asp:HiddenField id="bscdepar" runat="Server" />
         <asp:HiddenField id="sectiondepar" runat="Server" />
         <asp:HiddenField id="pasteinput" runat="Server" />
    <table border="1" class="act">
      <tr><td><asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></td></tr>
      <tr><td class="styleHeader">Setting Target <asp:label runat="server" id="lblJudul"/></td></tr>
      <tr>
         <td>Year &nbsp;
         <asp:DropDownList ID="cmbYear" runat="server" AutoPostBack="true" CssClass="plains" enabled="false">
                        <asp:ListItem Value="2010" Text="2010"></asp:ListItem>
                        <asp:ListItem Value="2011" Text="2011"></asp:ListItem>
                        <asp:ListItem Value="2012" Text="2012"></asp:ListItem>
                        <asp:ListItem Value="2012" Text="2013"></asp:ListItem>
                        <asp:ListItem Value="2012" Text="2014"></asp:ListItem>
                     </asp:DropDownList>
         </td>        
      </tr>
      <%--<tr>
         <td colspan="2">Annual Target &nbsp; <asp:TextBox id="txtAnnual" runat="server" Enabled="False" CssClass="plain"/><asp:Button id="btnCalc" runat="server" Text="Kalkulasi" Visible="false"/></td> 
      </tr>    --%>
      <tr>
         <td>Unit Of Measurement &nbsp; <asp:TextBox id="txtUoM" runat="server" Enabled="False" CssClass="plain"/></td>
      </tr>
      <tr>
         <td><div id="result" runat="Server"></div></td>
      </tr>
      <tr>
      <td><input type="button" id="mySave" value="Save" class="btn" onclick="doSave();"/></td>
      </tr>
    </table>
      </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
    <script type="text/javascript">
       checkpermission();
    </script>
</body>
</html>
