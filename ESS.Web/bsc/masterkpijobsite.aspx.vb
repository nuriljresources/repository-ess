﻿Imports System.Data.SqlClient

Partial Public Class masterkpijobsite
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") = "" Or Session("permission") = Nothing Then
            Response.Redirect("../Login.aspx", False)
            Exit Sub
        ElseIf ("1;2").IndexOf(Session("permission")) = -1 Then 'MDV + SEKPRO
            Response.Redirect("../default.aspx", False)
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim dr As SqlDataReader = Nothing
            Try
                Dim type As String = ""
                type = Request.QueryString("type")
                Dim cmd As SqlCommand
                Dim query As String = "select id,kdsite,active from site order by id"
                Dim dt As New DataTable
                cmd = New SqlCommand(query, conn)
                conn.Open()
                'Initialize Jobsite
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Then 'GRUP MDV
                        KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "2" Then 'GRUP SEK PRO
                        If Session("jobsite") = dr(1) Then
                            KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                    'KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                    'If dr(2) = 0 Then
                    '    KPIJobsite.Items(KPIJobsite.Items.Count - 1).Enabled = False
                    'End If
                End While
                KPIJobsite.Visible = True
                dr.Close()
                dr = Nothing
                'Initialize SO
                query = "select ID,SO from MSO order by ID"
                cmd = New SqlCommand(query, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    SO.Items.Add(New ListItem(dr(1), dr(0)))
                End While
                dr.Close()
                dr = Nothing
                KPISection.Visible = False
            Catch ex As Exception
                lblMessage.Text = ex.Message
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                If dr IsNot Nothing Then
                    dr = Nothing
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Value="0" Text=BUMA
    ''' Value="1" Text="Function"
    ''' Value="2" Text="Section"
    ''' Value="3" Text="Jobsite"
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub KPILevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles KPILevel.SelectedIndexChanged
        Try
            Select Case KPILevel.SelectedValue
                Case 0
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                Case 1
                    KPIFunc.Visible = True
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                Case 2
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = True
                Case 3
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = False
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim query As String = ""
            Dim result As String
            Dim cmd As SqlCommand
            If KPIID.Value = "[AUTO]" Then
                query = "BEGIN TRANSACTION; BEGIN TRY "
                query += "insert into MKPI " _
                        & " (Level,SOID,Name,Func,Jobsite,Section,UoM,Owner,Type,[Desc],Period,Leads,Formula,FormulaDesc,RedParam,GreenParam,BasedOn,CalcType,Parent,CreatedBy,CreatedIn) values " _
                        & "('" & KPILevel.SelectedValue & "','" & SO.SelectedValue & "','" & KPIName.Text & "'," _
                        & "'','" & KPIJobsite.SelectedValue & "','','" _
                        & KPIUOM.Text & "','" & KPIOwner.Text & "','" & KPIType.SelectedValue() & "','" _
                        & KPIDesc.Text & "','" & KPIPeriod.SelectedValue & "','" & KPILeads.Text & "','" _
                        & KPIFormula.SelectedValue & "','" & KPIFDesc.Text & "','" & KPIRed.Text & "','" _
                        & KPIGreen.Text & "','" & KPIBased.SelectedValue & "','" & KPICalcType.SelectedValue & "','','" & Request.UserHostName & "','" & Request.UserHostName & "');"
                query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select IDENT_CURRENT('MKPI');end "
            Else
                query = "BEGIN TRANSACTION; BEGIN TRY "
                query += "Update MKPI Set " _
                        & "SOID='" & SO.SelectedValue & "',Name='" & KPIName.Text & "'," _
                        & "UoM='" & KPIUOM.Text & "', Owner='" & KPIOwner.Text & "'," _
                        & "Type='" & KPIType.SelectedValue() & "', [Desc]='" & KPIDesc.Text & "'," _
                        & "Period='" & KPIPeriod.SelectedValue & "',Leads='" & KPILeads.Text & "'," _
                        & "Formula='" & KPIFormula.SelectedValue & "',FormulaDesc='" & KPIFDesc.Text & "'," _
                        & "RedParam=" & KPIRed.Text & ",GreenParam=" & KPIGreen.Text & "," _
                        & "BasedOn='" & KPIBased.SelectedValue & "',CalcType='" & KPICalcType.SelectedValue & "'," _
                        & "ModifiedBy='" & Request.UserHostName & "', modifiedtime=getdate() where " _
                        & "ID='" & KPIID.Value & "'"
                query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "

            End If

            cmd = New SqlCommand(query, conn)
            conn.Open()
            result = cmd.ExecuteScalar()
            If IsNumeric(result) Then
                If result = 0 Then
                    lblMessage.Text = "**Update Berhasil**ID KPI LEVEL BUMA:" & CStr(result)
                Else
                    KPIID.Value = result
                    lblMessage.Text = "**Save Berhasil**ID KPI LEVEL BUMA:" & CStr(KPIID.Value)
                End If
            Else
                lblMessage.Text = result
            End If
        Catch ex As Exception
            lblMessage.Text = ex.Message
        Finally
            conn.Close()
        End Try
    End Sub

    ''' <summary>
    ''' Value="0" Text=BUMA
    ''' Value="1" Text="Function"
    ''' Value="2" Text="Section"
    ''' Value="3" Text="Jobsite"
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GenerateFunction() As String
        Try
            Dim result As String = ""
            Select Case KPILevel.SelectedValue
                Case 0
                    Return ""
                Case 1
                    For Each c As ListItem In KPIFunc.Items
                        If c.Selected Then
                            result += "1"
                        Else
                            result += "0"
                        End If
                    Next
                    Return result
                Case 2
                    Return ""
                Case 3
                    Return ""
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
        Return ""
    End Function

    ''' <summary>
    ''' Value="0" Text=BUMA
    ''' Value="1" Text="Function"
    ''' Value="2" Text="Section"
    ''' Value="3" Text="Jobsite"
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GenerateSection() As String
        Try
            Dim result As String = ""
            Select Case KPILevel.SelectedValue
                Case 0
                    Return ""
                Case 1
                    Return ""
                Case 2
                    For Each c As ListItem In KPISection.Items
                        If c.Selected Then
                            result += "1"
                        Else
                            result += "0"
                        End If
                    Next
                    Return result
                Case 3
                    Return ""
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
        Return ""
    End Function

    ''' <summary>
    ''' Value="0" Text=BUMA
    ''' Value="1" Text="Function"
    ''' Value="2" Text="Section"
    ''' Value="3" Text="Jobsite"
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GenerateJobsite() As String
        Try
            Dim result As String = ""
            Select Case KPILevel.SelectedValue
                Case 0
                    Return ""
                Case 1
                    Return ""
                Case 2
                    For Each c As ListItem In KPIJobsite.Items
                        If c.Selected Then
                            result += "1"
                        Else
                            result += "0"
                        End If
                    Next
                    Return result
                Case 3
                    For Each c As ListItem In KPIJobsite.Items
                        If c.Selected Then
                            result += "1"
                        Else
                            result += "0"
                        End If
                    Next
                    Return result
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
        Return ""
    End Function

    Sub GenerateKPILevel(ByVal type As String)
        Try
            Select Case type
                Case "0" 'BUMA
                    For Each l As ListItem In KPILevel.Items
                        If l.Value = "0" Or l.Value = "3" Then
                            l.Enabled = False
                        End If
                    Next
                    KPIFunc.Visible = True
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                Case "1" 'Function
                    For Each l As ListItem In KPILevel.Items
                        If l.Value = "0" Or l.Value = "1" Or l.Value = "3" Then
                            l.Enabled = False
                        End If
                    Next
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = False
                Case "2" 'Section
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = True
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class