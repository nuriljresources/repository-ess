﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="leave_entry.aspx.vb"
    Inherits="EXCELLENT.leave_entry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    entry leave
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script src="Scripts/jsm01.js" type="text/javascript"></script>

    <script src="Scripts/jscal2.js" type="text/javascript"></script>

    <script src="Scripts/en.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="css/gold/gold.css" />
    <style type="text/css">
        .style4
        {
            height: 23px;
        }
    </style>

    <script type="text/javascript">
        function chkchar(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode == 46) //decimal
                return true
            if (charCode == 39 || (charCode > 31 && charCode < 48) || (charCode > 57 && charCode < 127))
                return false;
            document.getElementById("lbltext").value = 'Please Re-Calculate date leave changed';
            document.getElementById("lbltext").disabled = "";
            return true;
        }

        function chkremark(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode == 46) //decimal
                return true
            if (charCode == 39)
                return false;

            return true;
        }

        function OpenPopupdel(key) {
            resetDayOffTrip();
            document.getElementById("ctrlToFind").value = key;
            window.open("Searchleavedel.aspx", "List", "scrollbars=yes,resizable=no,width=500,height=400");
            return false;
        }

        function OpenPopupleave(key) {
            resetDayOffTrip();
            document.getElementById("ctrlToFind").value = key;
            window.open("leavetype.aspx", "List", "scrollbars=yes,resizable=no,width=500,height=400");
            return false;
        }

        function OpenPopuppaidleave(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("paidleave.aspx", "List", "scrollbars=yes,resizable=no,width=500,height=400");
            return false;
        }

        function OpenPopupDayOffleave(key) {
            var dtFrom = document.getElementById("ctl00_ContentPlaceHolder1_txtdtreq").value;
            var nikSite = document.getElementById("ctl00_ContentPlaceHolder1_txtnik").value;

            if (dtFrom != '' && nikSite != '') {
                document.getElementById("ctrlToFind").value = key;
                window.open("leave_day_off.aspx?nik=" + nikSite + "&id=" + dtFrom, "List", "scrollbars=yes,resizable=no,width=500,height=400");
            } else {
                alert('Nik & Date from must be input');
            }
            return false;
        }

        function Openempy(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("searchemployee.aspx", "List", "scrollbars=yes,resizable=no,width=500,height=400");
            return false;
        }

        function Openempy2(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("searchhr.aspx", "List", "scrollbars=yes,resizable=no,width=500,height=400");
            return false;
        }

        function Openempy3(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("searchspv.aspx", "List", "scrollbars=yes,resizable=no,width=500,height=400");
            return false;
        }


        function save() {
            $.ajax({
                //            window:location = "account.aspx",

                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "WS/UserMgmt.asmx/AddLeave",
                //data: "{'_tripno':" + JSON.stringify(document.getElementById("tripno").value) + ",'_site':" + JSON.stringify(document.getElementById("site").value) + ",'_nik':" + JSON.stringify(document.getElementById("nik").value) + ",'_costcode':" + JSON.stringify(document.getElementById("costcode").value) + ",'_snik':" + JSON.stringify(document.getElementById("snik").value) + ",'_kddepar':" + JSON.stringify(document.getElementById("kddepar").value) + ",'_nmjabat':" + JSON.stringify(document.getElementById("nmjabat").value) + ",'_consultant':" + JSON.stringify(document.getElementById("consultant").value) + ",'_consultantype':" + JSON.stringify(document.getElementById("consultantype").value) + ",'_tripfrom':" + JSON.stringify(document.getElementById("tripfrom").value) + ",'_tripto':" + JSON.stringify(document.getElementById("tripto").value) + ",'_tripday':" + JSON.stringify(document.getElementById("tripday").value) + "}",
                data: "{'_txtnik':" + JSON.stringify(document.getElementById("txtnik").value) + ",'_txtnama':" + JSON.stringify(document.getElementById("txtnama").value) + "}",
                dataType: "json",
                success: function(res) {

                    alert("Data Saved Successfully");

                    //               document.getElementById("log").innerHTML = res.d;
                    //tripno, site, nik, costcode, nama, input, kddepar, nmjabat, consultant, consultantype, tripfrom, tripto, tripday
                    //window.location = "bussiness_trip_report_.aspx?id=" + d.getElementById("tripno").value;
                },
                error: function(err) {
                    //               document.getElementById("log").innerHTML = "error: " + err.responseText;
                    window.location = "home.aspx";
                }
            });
        }
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="ctrlToFind" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table"
        style="padding-left: 120px;">
        <caption style="text-align: center; font-size: 1.5em; color: White;">
            Entry Leave
        </caption>
        <tr>
            <td>
            </td>
            <td colspan="2">
                <input style="width: 190px; visibility: hidden;" type="text" id="txtnobukti" />
                <asp:TextBox ID="txtkdsite" Style="width: 90px; margin-left: 630px;" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Nik*
            </td>
            <td>
                <asp:TextBox ID="txtnik" Style="width: 90px" runat="server"></asp:TextBox>
                <img alt="add" id="Img4" src="images/Search.gif" style="cursor: pointer; height: 15px;"
                    onclick="OpenPopupdel('leave1')" />
                <asp:TextBox ID="txtnama" Style="width: 800px" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Department*
            </td>
            <td>
                <asp:TextBox ID="txtdepar" Style="width: 917px" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Position*
            </td>
            <td>
                <asp:TextBox ID="txtposition" Style="width: 917px" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Type Leave*
            </td>
            <td>
                <asp:TextBox ID="txtleavetype" runat="server" Style="width: 300px"></asp:TextBox>
                <img alt="add" id="Img1" src="images/Search.gif" style="cursor: pointer; height: 15px;"
                    onclick="OpenPopupleave('leatype')" />
                <asp:HiddenField ID="leavetype" runat="server" />
                <a style="margin-left: 205px;">Paid Leave</a>
                <asp:TextBox ID="txtpaidleave" Style="width: 300px;" runat="server"></asp:TextBox>
                <img alt="add" id="Img2" src="images/Search.gif" style="cursor: pointer; height: 15px;"
                    onclick="OpenPopuppaidleave('paidlea')" />
                <asp:HiddenField ID="paidleave" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Date Request*
            </td>
            <td>
                <asp:TextBox ID="txtdtreq" runat="server" Style="width: 90px;"></asp:TextBox>
                <button id="btfroms" disabled="disabled" style="height: 22px">
                    ...</button>
                <strong style="color: Red; margin-left: 100px;">
                    <input type="text" disabled="disabled" id="lbltext" style="width: 5px; color: Red;
                        font-weight: bold; border-style: none;" value="" />
                </strong>
                
                <a style="margin-left: 245px;">Reference Field Break</a>
                <asp:TextBox ID="txtdayoff" Style="width: 120px;" runat="server" ReadOnly Enabled=false></asp:TextBox>
                <img alt="add" id="Img6" src="images/Search.gif" style="cursor: pointer; height: 15px;"
                    onclick="OpenPopupDayOffleave('paidlea')" />
                    
                    <a style="margin-left: 5px;">Date Expired</a>
                    <asp:TextBox ID="dayoff_dateex" Style="width: 100px;" runat="server"></asp:TextBox>
                    
                <asp:HiddenField ID="dayoff_tripid" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Leave From*
            </td>
            <td>
                <asp:TextBox ID="txtleafrom" runat="server" Style="width: 90px;"></asp:TextBox>
                <button id="btfrom" style="height: 22px">
                    ...</button>
                <a>to</a>
                <asp:TextBox ID="txtleato" runat="server" Style="width: 90px;"></asp:TextBox>
                <button id="btto" style="height: 22px">
                    ...</button>
                <a style="margin-left: 100px;">Holiday </a>
                <asp:TextBox Style="width: 90px;" ID="txtholiday" runat="server" onkeypress="return chkchar(event)"
                    onmouseover="ddrivetip('Public Holiday, Saturday, Sunday, Day Off','White', 300)"
                    onmouseout="hideddrivetip()" target="_blank"></asp:TextBox>
                days
                <a style="margin-left: 220px;">Total Balance</a>
                <asp:TextBox ID="dayoff_usage" Style="width: 100px;" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <% 
                    If Session("kdsite").ToString() = "PEN" Or Session("kdsite").ToString() = "BAK" Or Session("kdsite").ToString() = "LAN" Then
                        txtremleave.Visible = False
                        Response.Write("<asp:HiddenField ID='txtremleave' runat='server' /> ")
                        lblwu.Visible = False
                    Else
                        Response.Write("Remaining Leave")
                        txtremleave.Visible = True
                    End If
        
                %>
            </td>
            <td>
                <asp:TextBox ID="txtremleave" runat="server" Style="width: 90px;"></asp:TextBox>
                <asp:HiddenField ID="txtremleaveact" runat="server" />
                <asp:Label ID="lblwu" runat="server">working days</asp:Label>
                <asp:Button ID="btncalc" Text="Calculate" Style="margin-left: 100px; margin-right: 21px;"
                    runat="server" />
                Leave
                <asp:TextBox ID="txttotleave" runat="server" Style="width: 90px;"></asp:TextBox>
                working days
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:TextBox ID="txtleaeli" runat="server" Style="width: 90px;" Text="0"></asp:TextBox>
                <a style="margin-left: 142px"></a>
                <asp:TextBox ID="txtcutabs" runat="server" Style="width: 90px;" Text="0"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Remarks
            </td>
            <td>
                <asp:TextBox ID="txarem" runat="server" TextMode="MultiLine" onkeypress="return chkremark(event)"
                    Style="width: 920px; height: 100px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                <div style="border-style: solid; border-color: Blue; border-width: thin; width: 1035px;"
                    title="During Leave Duties will be act by">
                    <table>
                        <tr>
                            <td colspan="2" style="color: Red;">
                                <strong>During Leave Task Delegated To</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Nik*
                            </td>
                            <td>
                                <asp:TextBox ID="txtnikdut" runat="server" Style="width: 90px;"></asp:TextBox>
                                <img alt="add" id="Img3" src="images/Search.gif" style="cursor: pointer; height: 15px;"
                                    onclick="Openempy('leave2')" />
                                <asp:TextBox ID="txtnmdut" runat="server" Style="width: 800px;"></asp:TextBox>
                                <asp:HiddenField ID="txtniksitedut" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Position*
                            </td>
                            <td>
                                <asp:TextBox ID="txtposdut" runat="server" Style="width: 917px;"></asp:TextBox>
                                <asp:HiddenField ID="posdut" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                <asp:HiddenField ID="txtnikx" runat="server" />
                <asp:HiddenField ID="txtnikverby" runat="server" />
                <asp:HiddenField ID="posverby" runat="server" />
                <%--<div style="border-style:solid; border-color:Blue; border-width:thin; width:515px; float:left; visibility:hidden;" title="Verified By">
                <table>
                <tr>
                        <td colspan="2" style="color:Red;">
                            <strong>Verified By</strong>
                        </td>
                     </tr>
                    <tr>
                        <td>
                            Nik*
                        </td>
                        <td>
                            <asp:TextBox ID="txtverby" runat="server" style="width:420px;"></asp:TextBox> 
                            <img alt="add" id="Img6" src="images/Search.gif" style="cursor: pointer; height:15px;" onclick="Openempy2('leave3')" />
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Position
                        </td>
                        <td>
                            <asp:TextBox ID="txtposverby" runat="server" style="width:440px;"></asp:TextBox>
                            
                        </td>
                    </tr>
                </table>
            </div>--%>
                <div style="border-style: solid; border-color: Blue; border-width: thin; width: 515px;
                    float: left; margin-left: 0px;" title="Approved By">
                    <table>
                        <tr>
                            <td colspan="2" style="color: Red;">
                                <strong>Approved By</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Nik*
                            </td>
                            <td>
                                <asp:TextBox ID="txtnikappby" runat="server" Style="width: 420px;"></asp:TextBox>
                                <img alt="add" id="Img5" src="images/Search.gif" style="cursor: pointer; height: 15px;"
                                    onclick="Openempy3('leave4')" />
                                <asp:HiddenField ID="txtappby" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Position
                            </td>
                            <td>
                                <asp:TextBox ID="txtposappby" runat="server" Style="width: 440px;"></asp:TextBox>
                                <asp:HiddenField ID="posappby" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                Note :
                <br />
                Please Calculate after changing date from date to and holiday between range date
                from to leave
                <br />
                Before saving data (especialy for annual leave)<br />
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="btnsave" runat="server" Text="Save" Style="width: 50px; height: 20px" />
                <asp:Button ID="btncancel" runat="server" Text="Cancel" Style="width: 50px; height: 20px;
                    margin-left: 5px" />
                   <asp:Button ID="btnSubmit" runat="server" Text="Submit" 
                    Style="width: 50px; height: 20px" Enabled="False" />
                <%--<asp:Button ID="btnprint" runat = "server" Text="Print" OnClick="btnprint_Click" OnClientClick="aspnetForm.target ='_blank';" style="width:50px; height:20px; margin-left:5px" />--%>
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hyeara" runat="server" />
                <asp:HiddenField ID="hyearb" runat="server" />
                <asp:HiddenField ID="htota" runat="server" Visible="true" />
                <asp:HiddenField ID="htotb" runat="server" Visible="true" />
            </td>
        </tr>
    </table>

    <script type="text/javascript">

        //<![CDATA[
        var cal = Calendar.setup({
            onSelect: function(cal) { cal.hide() },
            showTime: false
        });
        cal.manageFields("btfrom", "ctl00_ContentPlaceHolder1_txtleafrom", "%m/%d/%Y");
        cal.manageFields("btto", "ctl00_ContentPlaceHolder1_txtleato", "%m/%d/%Y");

        //]]

        function resetDayOffTrip() {
            document.getElementById("ctl00_ContentPlaceHolder1_txtdayoff").value = "";
            document.getElementById("ctl00_ContentPlaceHolder1_dayoff_tripid").value = "";
            document.getElementById("ctl00_ContentPlaceHolder1_dayoff_dateex").value = "";
            document.getElementById("ctl00_ContentPlaceHolder1_dayoff_usage").value = "";
        }
        
    
    </script>

    <%=alert%>
</asp:Content>
