﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="medic_frames.aspx.vb" Inherits="EXCELLENT.medic_frames" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Frames Entry
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script src="Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="Scripts/jscal2.js" type ="text/javascript"></script>
    <script src="Scripts/en.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="css/win2k/win2k.css" />
    <link rel="stylesheet" href="css/jquery.tooltip/jquery.tooltip2.css" type="text/css" />
    <script type="text/javascript" src="Scripts/tooltip/jquery.min.js"></script>
    <script type="text/javascript" src="Scripts/tooltip/jquery.tooltip.js"></script>
    <script type="text/javascript" >
        function OpenPopup(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("srcfamily.aspx", "List", "scrollbars=yes,resizable=no,width=600,height=500");
            return false;
        }

        function OpenPopupd(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("searchmedicdel.aspx", "List", "scrollbars=yes,resizable=no,width=600,height=500");
            return false;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode < 45 || charCode > 57 || charCode == 47 || charCode == 45)
                return false;

            return true;
        }

        function readonly(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 00)
                return false;

            return true;
        }

        $j = jQuery.noConflict();
        $j(document).ready(function() {
            $j("div.item").tooltip();
        });
    </script>
    <style type="text/css">
        .style4
        {
            width: 100px;
        }
        .style5
        {
            width: 135px;
        }
        /** { font-family: Times New Roman; }*/
      div.item { width:150px; height:27px; background-color: Transparent; text-align:center; padding-top:0px; color: white;}
      div#item_1 { position: absolute; top: 105px; left: 350px; color: white; font-family:arial; background-color:#E38695;}
      div#item_2 { position: absolute; top: 250px; left: 649px; color: black; font-family:arial;}      
      div#item_3 { position: absolute; top: 250px; left: 955px; color: black; font-family:arial;}            
      div#item_4 { position: absolute; top: 250px; left: 1155px; color: black; font-family:arial;}
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<input type="hidden" id="ctrlToFind" /> <asp:HiddenField ID="hprint" runat="server" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table"> 
        <caption style="text-align: center; font-size: 1.5em; color:White;">Glasses Claim</caption>
        <tr>
            <td style="width:10%;">
                Trans No
            </td>
            <td class="style5">
                <asp:TextBox ID="txtnoreg" Text="Auto" Enabled="false" runat="server" 
                    Font-Size="1.0em">Auto</asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Site
            </td>
            <td class="style5">
                <asp:TextBox ID="txtnmsite" runat="server" Font-Size="1.0em" ></asp:TextBox>
                <asp:HiddenField ID="hnmsite" runat="server" />
            </td>
            <td>
                costcode
            </td>
            <td>
                <asp:TextBox ID="txtcostcode" runat="server" Width="163px" Font-Size="1.0em"></asp:TextBox>
            </td>
            <td>  </td>
        </tr>
        <tr>
            <td>
                Nik <strong style="color:Red;">*</strong>
            </td>
            <td colspan="4">
                <asp:TextBox ID="txtnik" runat="server" Font-Size="1.0em"></asp:TextBox>
                <img alt="add" id="Img1" src="images/Search.gif" align="absmiddle" style="cursor: pointer" onclick="OpenPopupd('srcd')" />
                <asp:HiddenField ID="hnik" runat="server" />
                <asp:TextBox ID="txtnama" runat="server" Width="270px" Font-Size="1.0em"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Position
            </td>
            <td class="style5">
                <asp:TextBox ID="txtlevel" runat="server" Font-Size="1.0em" ></asp:TextBox>
                <asp:HiddenField ID="hkdlevel" runat="server" />
            </td>
            <%--<td>Outpatient max</td>
            <td>
                <asp:TextBox ID="txtoutpatientmax" runat="server" Font-Size="1.0em" ></asp:TextBox>
            </td>--%>
        </tr>
        <tr>
        <td style="width:10%;">Date</td>
         <td colspan="4">
                <asp:TextBox ID="txtdate" runat="server" Font-Size="1.0em"></asp:TextBox> <button id="btn1">...</button>
            </td>
        </tr>
         <tr>
        <td style="width:10%;">Lens</td>
         <td class="style5">
                <asp:TextBox ID="txtlens" runat="server" onkeypress="return isNumberKey(event)" OnTextChanged="txtlens_TextChanged" Font-Size="1.0em" AutoPostBack="true" ></asp:TextBox>
         </td>
        </tr>
         <tr>
        <td>Frame</td>
         <td class="style5">
                <asp:TextBox ID="txtframe" runat="server" onkeypress="return isNumberKey(event)" OnTextChanged="txtframe_TextChanged" Font-Size="1.0em" AutoPostBack="true"></asp:TextBox>
         </td>
         <td class="style4">Total Claim</td>
         <td>
                <asp:TextBox ID="txttotal" Width="163px" onkeypress="return readonly(event)" runat="server" Font-Size="1.0em"></asp:TextBox>
         </td>
        </tr>
        <tr>
            <td>
                Remark
            </td>
            <td colspan="5">
                <asp:TextBox ID="txtremarks" runat="server" TextMode="MultiLine" Width="404px" Font-Size="1.0em"></asp:TextBox>
                 <div id="item_1" class="item" style="font-weight:bold;">
                    Medical Benefit Policy <img alt="info" src="images/alert.png" width="20px" height="20px" />
                    <div class="tooltip_description" style="display:none;" title="Medical Benefit Policy">
                        Annual Limit <%=benefit%>
                    </div> 
                 </div> 
            </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="lblinfo" runat="server" style="color:Red;"></asp:Label>
    <br />
    <asp:Button ID="btnsave" runat="server" Text="SAVE" /> 
    <asp:Button ID="btnprint" runat="server" Text="PRINT"/> 
    <asp:Button ID="btnPaymentUsage" runat="server" Text="PRINT PA" /> 
    <asp:Button ID="btndelete" runat="server" Text="DELETE"/> 
    <asp:HiddenField ID="hamount1" runat="server" />
    <asp:HiddenField ID="hamount2" runat="server" />
    <asp:HiddenField ID="hamount3" runat="server" />
<script type ="text/javascript" >
    //<![CDATA[
    var cal = Calendar.setup({
        onSelect: function(cal) { cal.hide() },
        showTime: false
    });
    cal.manageFields("btn1", '<%=txtdate.ClientID%>', "%m/%d/%Y");

    //]]

    
        </script>
        <%=msg%>
</asp:Content>
