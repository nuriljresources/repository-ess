<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/reminder/master.Master" CodeBehind="edit_permit_doc.aspx.vb" Inherits="EXCELLENT.edit_permit_doc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Document Center Edit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="../Scripts/jscal2.js" type ="text/javascript"></script>
<script src="../Scripts/en.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="../css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="../css/win2k/win2k.css" />
<style type="text/css" >
        #form1
        {
            font-family:Tahoma,Arial;
        }
        #form1 h1 
        {
            color: #FFFFFF;
            font-size:x-large;
        }
        .title
        {
            color: blue;
            font-size: small;
            margin-bottom:10px;
        }
        .line
        {
            width: 100%;
            height: 20px;
            font-family:Tahoma,Arial;
            font-size:0.8em;
        }
        .general
        {
            font-size:0.8em;
            color:#33ADFF;
            font-weight:bold;
            border-style:solid;
            border-width:1px;
            border-color:#33ADFF;
            padding-left:5px;
        }
        
        .general a
        {
            text-decoration:none;
            color:#33ADFF;
        }
        
        .general a:hover
        {
            text-decoration:underline;
            color:#33ADFF;
        }
    </style>
    
    <script type="text/javascript">

        /***********************************************
        * Cool DHTML tooltip script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
        * This notice MUST stay intact for legal use
        * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
        ***********************************************/

        var offsetxpoint = -60 //Customize x offset of tooltip
        var offsetypoint = 20 //Customize y offset of tooltip
        var ie = document.all
        var ns6 = document.getElementById && !document.all
        var enabletip = false
        if (ie || ns6)
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : ""

        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body
        }

        function ddrivetip(thetext, thecolor, thewidth) {
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") tipobj.style.width = thewidth + "px"
                if (typeof thecolor != "undefined" && thecolor != "") tipobj.style.backgroundColor = thecolor
                tipobj.innerHTML = thetext
                enabletip = true
                return false
            }
        }

        function positiontip(e) {
            if (enabletip) {
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var rightedge = ie && !window.opera ? ietruebody().clientWidth - event.clientX - offsetxpoint : window.innerWidth - e.clientX - offsetxpoint - 20
                var bottomedge = ie && !window.opera ? ietruebody().clientHeight - event.clientY - offsetypoint : window.innerHeight - e.clientY - offsetypoint - 20

                var leftedge = (offsetxpoint < 0) ? offsetxpoint * (-1) : -1000

                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth)
                //move the horizontal position of the menu to the left by it's width
                    tipobj.style.left = ie ? ietruebody().scrollLeft + event.clientX - tipobj.offsetWidth + "px" : window.pageXOffset + e.clientX - tipobj.offsetWidth + "px"
                else if (curX < leftedge)
                    tipobj.style.left = "5px"
                else
                //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = curX + offsetxpoint + "px"

                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight)
                    tipobj.style.top = ie ? ietruebody().scrollTop + event.clientY - tipobj.offsetHeight - offsetypoint + "px" : window.pageYOffset + e.clientY - tipobj.offsetHeight - offsetypoint + "px"
                else
                    tipobj.style.top = curY + offsetypoint + "px"
                tipobj.style.visibility = "visible"
            }
        }

        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false
                tipobj.style.visibility = "hidden"
                tipobj.style.left = "-1000px"
                tipobj.style.backgroundColor = ''
                tipobj.style.width = ''
            }
        }

        document.onmousemove = positiontip

        function OpenPopup(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("spic.aspx", "List", "scrollbars=yes,resizable=yes,width=600,height=500");
            return false;
        }

        
        function save() {
                
            var d = document;
            var torpt;
            var s = document.getElementById('type');
            var type = s.options[s.selectedIndex].value;

            var s2 = document.getElementById('doctype');
            var dtype = s2.options[s2.selectedIndex].value;

            var check = document.getElementById('<%=rpt.ClientID%>');
            if (check.checked == true) {
                torpt = 1
            }

            if (check.checked == false) {
                torpt = 0
            }

            if (d.getElementById("type").value.length == 0) {
                alert("Please Entry Document Type");
                return false;
            }
            var fpath = dtype + "_" + d.getElementById('<%=subject.ClientID%>').value + "_" + d.getElementById('<%=ddlcomp.ClientID%>').value + "_" + d.getElementById('<%=ddlsite.ClientID%>').value + "_" + d.getElementById('<%=txtyear.ClientID%>').value + "_" + d.getElementById('<%=filename.ClientID%>').value;
            d.getElementById('<%=tipe.ClientID%>').value = type
            d.getElementById('<%=doctype2.ClientID%>').value = dtype

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../WS/Service.asmx/Editpermit",
                data: "{'_type':" + JSON.stringify(type) + ",'_subject':" + JSON.stringify(document.getElementById('<%=subject.ClientID%>').value) + ",'_comp':" + JSON.stringify(document.getElementById('<%=ddlcomp.ClientID%>').value) + ",'_site':" + JSON.stringify(document.getElementById('<%=ddlsite.ClientID%>').value) + ",'_year':" + JSON.stringify(document.getElementById('<%=txtyear.ClientID%>').value) + ",'_category':" + JSON.stringify(document.getElementById('<%=category.ClientID%>').value) + ",'_doctype':" + JSON.stringify(dtype) + ",'_department':" + JSON.stringify(document.getElementById('<%=Department.ClientID%>').value) + ",'_subdept':" + JSON.stringify(document.getElementById('<%=subdept.ClientID%>').value) + ",'_number':" + JSON.stringify(document.getElementById('<%=Number.ClientID%>').value) + ",'_report':" + JSON.stringify(torpt) + ",'_dtissue':" + JSON.stringify(document.getElementById('<%=dtissue.ClientID%>').value) + ",'_dtexp':" + JSON.stringify(document.getElementById('<%=dtexp.ClientID%>').value) + ",'_fpath':" + JSON.stringify(fpath) + ",'_fname':" + JSON.stringify(document.getElementById('<%=filename.ClientID%>').value) + ",'_rem_days':" + JSON.stringify(document.getElementById('<%=rem_days.ClientID%>').value) + "}",
                dataType: "json",
                success: function(res) {
                    alert("Data Saved Successfully");
                },
                error: function(err) {
                    window.location = "permit_list.aspx";
                }
            });
        }
</script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <input type="hidden" id="ctrlToFind" />  
    <%--<asp:Button ID="Button1" runat="server" OnClientClick="javascript:return save()" Text="" EnableTheming="false" style="background-image:url('../images/save-icon-small.png'); background-repeat:no-repeat; background-position:center; width:32px; height:32px;"/>--%>
    <%--<div style="padding-left:211px; padding-top:10px; position:absolute;">
       <input type="button" id="Save" value="" onclick="save()" style="background-image:url('../images/save-icon-small.png'); background-repeat:no-repeat; background-position:center; width:32px; height:32px;" />
       <asp:Button ID="Button2" runat="server" Text="" EnableTheming="false" style="background-image:url('../images/cancel.png'); background-repeat:no-repeat; background-position:center; width:32px; height:32px;"/>    
    </div>--%>
    <div class="line" style="right:10px; text-align:right;">
    <a href="signout.aspx">Logout</a>
    </div>
    <div class="title">
        <strong>Menu</strong>
    </div>
    <div id="list_newest" class="general" style="width:200px; color:#33ADFF;">
        <div id="Div1"><img alt="open document" src="../images/open_doc_small.png" /> <a href="index.aspx">&nbsp;Home</a></div>
        <%=mnentry%>
        <%=mncontract%>
        <%=mnview%>
        <%=mnmgmt%>
    <div><img alt="open document" src="../images/open_doc_small.png" /> <a href="J Resources Document Center Manual Version 1.pdf" target="_blank">&nbsp;Help</a></div>
    </div>
    
    <div id="list_permit" class="general" style="width:1100px; height:450px; left:240px; top:20px; position:absolute; font:0.8em; border-color:#fff;">
        <asp:Label ID="label1" runat="server" ></asp:Label>
        <table class="data-table" style="height:100%;">
        <caption style="width:300px; text-align:center;">Document Information</caption>
        <tr>
            <td class="general" style="height:10px; width:40px">
                File Name
            </td>
            <td>
            <input type="text" id="filename" runat="server" value="" style="width:200px;" disabled="disabled"/>
            <input type="hidden" id="AttachmentTitleTextbox" />
                
            </td>
        </tr>
        <tr>
            <td class="general" style="height:10px;">
                Type
            </td>
            <td>
            
            <%--<%=doctype%>--%>
                
                <asp:DropDownList ID="ltype" runat="server">
                 <asp:ListItem Value="SK">SK</asp:ListItem>
                <asp:ListItem Value="SE">SE</asp:ListItem>
                <asp:ListItem Value="SOP">SOP</asp:ListItem>
                <asp:ListItem Value="WI">Working Instruction</asp:ListItem>
                <asp:ListItem Value="SPP">SPP</asp:ListItem>
                <asp:ListItem Value="IM">Interoffice Memorandum</asp:ListItem>
                <asp:ListItem Value="ITM">Internal Memorandum</asp:ListItem>
                <asp:ListItem Value="SKR">Surat Keluar</asp:ListItem>
                <asp:ListItem Value="DK">Dokumen Kontrak</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="general" style="height:10px;">
                Subject
            </td>
            <td>
                <input type="text" id="subject" name="subject" runat="server" style="width:200px;"/>
            </td>
        </tr>
        <tr>
            <td class="general" style="height:10px;">
                Entry Date
            </td>
            <td>
                <input type="text" id="txtyear" runat="server" disabled = "disabled" style="width:200px;"/>
            </td>
        </tr>
        <tr>
            <td class="general" style="height:10px;">
                Vendor
            </td>
            <td>
                <asp:TextBox ID="txtvendor" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="general" style="height:10px;">
                Company
            </td>
            <td>
                <asp:DropDownList ID="ddlcomp" runat="server" DataSourceID="SqlDataSource1" 
                    DataTextField="NmCompany" DataValueField="KdCompany" style="width:200px;"></asp:DropDownList> 
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:jrn_permit %>" 
                    SelectCommand="SELECT KdCompany, NmCompany FROM JRN_TEST.dbo.H_A110">
                </asp:SqlDataSource>
                <%--<input type="text" id="comp" runat="server" />--%>
            </td>
        </tr>
        <tr>
            <td class="general" style="height:10px;">
                Site
            </td>
            <td>
                 <asp:DropDownList ID="ddlsite" runat="server" DataSourceID="SqlDataSource2" 
                    DataTextField="NmSite" DataValueField="KdSite" style="width:200px;"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:jrn_permit %>" 
                    SelectCommand="SELECT KdSite, NmSite FROM JRN_TEST.dbo.H_A120">
                </asp:SqlDataSource>
                <%--<input type="text" id="site" runat="server" />--%>
            </td>
        </tr>
        
        <tr>
            <td class="general" style="height:10px;">
                Reminder
            </td>
            <td>
                
                <asp:DropDownList ID="rem_days" runat="server" style="width:200px;">
                <%--<asp:ListItem Value="0">-</asp:ListItem>--%>
                <asp:ListItem Value="7">1 Week Before Expire</asp:ListItem>
                <asp:ListItem Value="14">2 Week Before Expire</asp:ListItem>
                <asp:ListItem Value="30">1 Bulan Sebelum Expire</asp:ListItem>
                <asp:ListItem Value="60">2 Bulan Sebelum Expire</asp:ListItem>
                <asp:ListItem Value="90">3 Bulan Sebelum Expire</asp:ListItem>
                <asp:ListItem Value="180">6 Bulan Sebelum Expire</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        
        <tr>
            <td class="general" style="height:10px;">
                Date Issue
            </td>
            <td>
                <input type="text" id="dtissue" runat="server" style="width:180px; color:Gray;" /><button id="btdtissue">
                    ..</button>
            </td>
        </tr>
        <tr>
            <td class="general" style="height:10px;">
                Date Expired
            </td>
            <td>
                <input type="text" id="dtexp" runat="server" style="width:180px; color:Gray;" /><button id="btdtexp">
                    ..</button>
            </td>
        </tr>
        
             <tr><td colspan = "3">
        <table class="data-table" width = "300px">
        <caption style="text-align:center">Add Person</caption>
        <tr>
                <td style="width: 100px; height:10px;">
                    Name:</td>
                <td style="width: 18px">
                    <asp:TextBox ID="txtUserName" runat="server" Width = "140px"></asp:TextBox>
                    </td>                
            </tr>
            <tr>
                <td style="width: 100px; height:10px;" >
                    Nik:</td>
                <td style="width: 18px">
                    <asp:TextBox ID="txtnik" runat="server" Width = "100px"></asp:TextBox>
                    <img alt="add" id="Img1" src="../images/Search.gif" align="absmiddle" style="cursor: pointer" onclick="OpenPopup('suer')" />             
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height:10px;">
                    Email Schedule:</td>
                <td style="width: 18px">
                    <asp:DropDownList ID="ddlsch" runat = "server" >
                    <asp:ListItem Value = "01">Daily</asp:ListItem>
                    <asp:ListItem Value = "02">Weekly</asp:ListItem>
                    <asp:ListItem Value = "03">Monthly</asp:ListItem>
                    </asp:DropDownList>    
                </td>                
            </tr>
             <tr>
                 <td colspan="2" align="center">
                     <center><asp:Button ID="btnAdd" runat="server" Text="Add User To This Document" /></center> <br />
                 </td>
             </tr>
             <tr><td><asp:Label ID="lblTips" runat="server" ForeColor="Red"></asp:Label></td>
             
             </tr>
        </table>
        </td></tr> 
        <tr>
            <td colspan = "2">
                <%--<input type="button" id="btnsave" onclick="save();" style="background-image:url('../images/save-icon-small.png'); background-repeat:no-repeat; background-position:center; width:32px; height:32px;"/>--%> 
                            
            <%--<input type="button" id="btncancel" onclick = "window.location = 'permit_list.aspx'" style="width:50px; height:20px;" value="Cancel"/>     --%>  
        
        
        <%=opttype%>
                <asp:HiddenField ID="tipe" runat="server" />
                <asp:HiddenField ID="category" runat="server" />
                <%--<asp:DropDownList ID="ldoctype" runat="server" >
                <asp:ListItem Text="Peraturan / SK" Value="Peraturan / SK"></asp:ListItem>
                <asp:ListItem Text="Dokumen" Value="Dokumen"></asp:ListItem>
                <asp:ListItem Text="Surat" Value="Surat"></asp:ListItem>
                <asp:ListItem Text="Peta" Value="Peta"></asp:ListItem>
                </asp:DropDownList>--%>
                <asp:HiddenField ID="doctype2" runat="server" />
                <input type="text" id="Department" runat="server" style="visibility:hidden;"/>
                <asp:HiddenField ID="subdept" runat="server" />
                <input type="text" id="Number" runat="server" style="visibility:hidden;"/>
                <input type="checkbox" id="rpt" runat="server" style="visibility:hidden;" /> 
                <input type="text" id="report" runat="server" style="visibility:hidden;" />
        </td>
        </tr>
        <tr>
        <td>
        
        </td>
        </tr>     
        </table>
        <div style="position:relative; top:-70px;">
        <asp:Label ID="tny" runat="server" ></asp:Label>
        <asp:Button ID="yes" Text="Yes" runat = "server" Visible="false" style="width:32px; height:32px;"/> <asp:Button ID="no" Text = "No" runat = "server" Visible="false" style="width:32px; height:32px;"/>
                
        <asp:Button ID="btnsave2" runat ="server" style="background-image:url('../images/save-icon-small.png'); background-repeat:no-repeat; background-position:center; width:32px; height:32px;"/>
                <asp:Button ID="Button2" runat="server" Text="" EnableTheming="false" style="background-image:url('../images/cancel.png'); background-repeat:no-repeat; background-position:center; width:32px; height:32px;"/>
        </div>
         <div style="left:380px; top:0px; position:absolute; width:750px; Height:400px;" >
         <asp:GridView ID="GridView2" runat="server" DataKeyNames="Nik" AutoGenerateColumns="False" Width="346px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" EmptyDataText="No User Associated to this Document." ForeColor="Red">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Name"/>
                <asp:BoundField DataField="Nik" HeaderText="Nik" />
                <asp:BoundField DataField="Schedule" HeaderText="Schedule" />
                <asp:CommandField ShowDeleteButton="true" ButtonType="Link"/>
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <HeaderStyle BackColor="#0066ff" Font-Bold="False" ForeColor="#282828" />
        </asp:GridView>
        </div>
    </div>
   
    
    <%--<asp:DropDownList ID="txtpic1" runat="server" style="visibility:hidden;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email"></asp:DropDownList>
                <asp:SqlDataSource ID="DsPIC" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:jrn_permit %>" 
                    
                    SelectCommand="SELECT [pic_name], [pic_email] FROM [docpermit_pic] ORDER BY [pic_name]">
                </asp:SqlDataSource>
                <input type="checkbox" id="chkpic1" runat="server" style="visibility:hidden;"/>
                
    <input type="checkbox" id="chkpic2" runat="server" style="visibility:hidden;"/>
                <asp:DropDownList ID="txtpic2" runat="server" style="visibility:hidden;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email">
                </asp:DropDownList>--%>
                

    
     <%--<input type="checkbox" id="chkpic3" runat="server" style="visibility:hidden;"/>
                <asp:DropDownList ID="txtpic3" runat="server" style="visibility:hidden;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email">
                </asp:DropDownList>
                
    <input type="checkbox" id="chkpic4" runat="server" style="visibility:hidden;" />
                <asp:DropDownList ID="txtpic4" runat="server" style="visibility:hidden;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email">
                </asp:DropDownList>
    
    <input type="checkbox" id="chkpic5" runat="server" style="visibility:hidden;"/>
                <asp:DropDownList ID="txtpic5" runat="server" style="visibility:hidden;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email">
                </asp:DropDownList>--%>
    <div id="idnotif">        
        
    </div>   
    
    <center>
          <span id="idbar" style="color: #800000; background-color: #6699FF; font-weight: bold"></span>
    </center>
    
    <%=msgbox %>
    
    <script type="text/javascript" language="javascript">
        if (BrowserDetect.version != 8) {
            document.getElementById("idnotif").style.display = "block";
            document.getElementById("idnotif").style.textAlign = "center"
            document.getElementById("idnotif").style.fontsize = "large";
            document.getElementById("idnotif").style.fontWeight = "bold";
            document.getElementById("idnotif").style.color = "#FF0000";
            //        document.getElementById("idnotif").innerHTML = "*** Application HRMS not compatible with your browser ***<br />*** Please Update to Internet Explorer 8 ***";
        }
        else {
            document.getElementById("idnotif").style.display = "none";
            document.getElementById("idnotif").style.textAlign = "center"
            document.getElementById("idnotif").style.fontsize = "large";
            document.getElementById("idnotif").style.fontWeight = "bold";
            document.getElementById("idnotif").style.color = "#FFFFFF";
            document.getElementById("idnotif").innerHTML = "";
        }
        document.getElementById("idbar").style.display = "none";

        var cal = Calendar.setup({
            onSelect: function(cal) { cal.hide() },
            showTime: false
        });
        cal.manageFields("btdtissue", '<%=dtissue.ClientID%>', "%m/%d/%Y");
        cal.manageFields("btdtexp", '<%=dtexp.ClientID%>', "%m/%d/%Y");
          
</script>
</asp:Content>