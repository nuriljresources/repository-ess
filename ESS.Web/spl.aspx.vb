﻿Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports System.IO
Imports System.Net.Mail
Partial Public Class spl
    Inherits System.Web.UI.Page
    Public msg As String
    Public ssite As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("niksite") = Nothing Or Session("niksite") = "" Then Response.Redirect("login.aspx")
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim jam1 As String
        Dim menit1 As String
        Dim jam2 As String
        Dim menit2 As String

        ssite = Session("kdsite").ToString

        If Not IsPostBack Then
            If Session("splrecordidst") = "Editspl" Then
                Try
                    sqlConn.Open()
                    Dim strcon As String = "select periode, nik, (select nama from H_A101 where nik = H_H110.nik) as nama, (select niksite from H_A101 where nik = H_H110.nik) as niksite, Tanggal, jam_start, jam_end, (select nama from H_A101 where nik = H_H110.NIK) as nama, remarks, kdsite, kddepar, (select nmdepar from H_A130 where kddepar = H_H110.kddepar) as nmdepar, kdjabat, (select nmjabat from H_A150 where kdjabat = H_H110.kdjabat) as nmjabat, (select pycostcode from H_A101 where nik = H_H110.nik) as pycostcode,"
                    strcon = strcon + "app1,(select nama from H_A101 where nik = H_H110.app1) as app1name, (select niksite from H_A101 where nik = H_H110.app1) as app1niksite,(select nmdepar from H_A130 where kddepar = (select kddepar from H_A101 where nik = H_H110.app1)) as app1nmdepar, (select nmjabat from H_A150 where kdjabat = (select kdjabatan from H_A101 where nik = H_H110.app1)) as app1nmjabat,(select kdsite from H_A101 where nik = H_H110.app1) as app1kdsite,(select pycostcode from H_A101 where nik = H_H110.app1) as app1costcode, "
                    strcon = strcon + "app2,(select nama from H_A101 where nik = H_H110.app2) as app2name, (select niksite from H_A101 where nik = H_H110.app2) as app2niksite, (select nmdepar from H_A130 where kddepar = (select kddepar from H_A101 where nik = H_H110.app2)) as app2nmdepar, (select nmjabat from H_A150 where kdjabat = (select kdjabatan from H_A101 where nik = H_H110.app2)) as app2nmjabat,(select kdsite from H_A101 where nik = H_H110.app2) as app2kdsite,(select pycostcode from H_A101 where nik = H_H110.app2) as app2costcode, "
                    strcon = strcon + "requestby,(select nama from H_A101 where nik = H_H110.requestby) as requestbyname, (select niksite from H_A101 where nik = H_H110.requestby) as requestbyniksite, (select kddepar from H_A130 where kddepar = (select kddepar from H_A101 where nik = H_H110.requestby)) as requestbykddepar, (select nmdepar from H_A130 where kddepar = (select kddepar from H_A101 where nik = H_H110.requestby)) as requestbynmdepar, (select nmjabat from H_A150 where kdjabat = (select kdjabatan from H_A101 where nik = H_H110.requestby)) as requestbynmjabat,(select kdsite from H_A101 where nik = H_H110.requestby) as requestbykdsite,(select pycostcode from H_A101 where nik = H_H110.requestby) as requestbycostcode, checkapp1, checkapp2, stedit "
                    strcon = strcon + "from H_H110 where recordid = '" + Session("splrecordid") + "' "
                    Dim dtb As DataTable = New DataTable
                    Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)

                    sda.Fill(dtb)

                    If dtb.Rows.Count > 0 Then
                        jam1 = CDate(dtb.Rows(0)!jam_end).Hour
                        menit1 = CDate(dtb.Rows(0)!jam_end).Minute
                        jam2 = CDate(dtb.Rows(0)!jam_start).Hour
                        menit2 = CDate(dtb.Rows(0)!jam_start).Minute
                        txtdate.Text = CDate(dtb.Rows(0)!Tanggal).ToString("MM/dd/yyyy")
                        txtdept.Text = dtb.Rows(0)!nmdepar.ToString
                        hkddepar.Value = dtb.Rows(0)!kddepar.ToString
                        txtdatefinish.Text = CDate(dtb.Rows(0)!jam_end).ToString("MM/dd/yyyy")
                        'txtfinish.Text = CDate(dtb.Rows(0)!jam_end).ToString("hh:mm")
                        txtfinish.Text = jam1 + ":" + menit1
                        txtdatestart.Text = CDate(dtb.Rows(0)!jam_start).ToString("MM/dd/yyyy")
                        txtstart.Text = jam2 + ":" + menit2
                        'txtstart.Text = CDate(dtb.Rows(0)!jam_start).ToString
                        txtket.Text = dtb.Rows(0)!remarks.ToString
                        txtsite.Text = dtb.Rows(0)!kdsite.ToString
                        txtposition.Text = (dtb.Rows(0)!nmjabat.ToString)
                        txtcostcode.Text = dtb.Rows(0)!pycostcode.ToString
                        txtnama.Text = dtb.Rows(0)!nama.ToString
                        txtnik.Text = dtb.Rows(0)!niksite.ToString
                        hnik.Value = dtb.Rows(0)!nik.ToString
                        txtnikreq.Text = dtb.Rows(0)!requestbyniksite.ToString
                        hnikreq.Value = dtb.Rows(0)!requestby.ToString
                        hkdsitereq.Value = dtb.Rows(0)!requestbykdsite.ToString
                        txtdeptreq.Text = dtb.Rows(0)!requestbynmdepar.ToString
                        hkddeparreq.Value = dtb.Rows(0)!requestbykddepar.ToString
                        txtnamareq.Text = dtb.Rows(0)!requestbyname.ToString
                        txtsitereq.Text = dtb.Rows(0)!requestbykdsite.ToString
                        txtcostcodereq.Text = dtb.Rows(0)!requestbycostcode.ToString
                        txtpositionreq.Text = dtb.Rows(0)!requestbynmjabat.ToString
                        txtspv2.Text = dtb.Rows(0)!app2name.ToString
                        hnikspv2.Value = dtb.Rows(0)!app2.ToString
                        txtspv1.Text = dtb.Rows(0)!app1name.ToString
                        hnikspv1.Value = dtb.Rows(0)!app1.ToString
                        recordid.Value = Session("splrecordid").ToString
                        If dtb.Rows(0)!checkapp1.ToString <> "" Or dtb.Rows(0)!checkapp2.ToString <> "" Then
                            If dtb.Rows(0)!stedit.ToString = "3" Then
                                btnprint.Visible = True
                            Else
                                btnprint.Visible = False
                            End If
                        End If
                        If dtb.Rows(0)!stedit.ToString = "0" Then
                            btnsubmit.Visible = True
                        Else
                            btnsubmit.Visible = False
                            btnsave.Visible = False
                        End If
                    End If

                Catch ex As Exception

                End Try
                recordid.Value = Session("splrecordid").ToString
                Session("splrecordidst") = ""
            Else
                recordid.Value = "Auto"
                txtdate.Text = Today.Date.ToString("MM/dd/yyyy")
                btnprint.Visible = False
                btnsubmit.Visible = False
            End If
            txtdate.Attributes.Add("readonly", "readonly")
            'txtket.Attributes.Add("readonly", "readonly")
            txtnama.Attributes.Add("readonly", "readonly")
            txtnamareq.Attributes.Add("readonly", "readonly")
            txtnik.Attributes.Add("readonly", "readonly")
            txtposition.Attributes.Add("readonly", "readonly")
            txtpositionreq.Attributes.Add("readonly", "readonly")
            txtsite.Attributes.Add("readonly", "readonly")
            txtsitereq.Attributes.Add("readonly", "readonly")
            txtspv1.Attributes.Add("readonly", "readonly")
            txtspv2.Attributes.Add("readonly", "readonly")
            'txtstart.Attributes.Add("readonly", "readonly")
            'txtfinish.Attributes.Add("readonly", "readonly")
            txtdeptreq.Attributes.Add("readonly", "readonly")
            txtdept.Attributes.Add("readonly", "readonly")
            txtdatestart.Attributes.Add("readonly", "readonly")
            txtdate.Attributes.Add("readonly", "readonly")
            txtcostcodereq.Attributes.Add("readonly", "readonly")
            txtcostcode.Attributes.Add("readonly", "readonly")
            txtnikreq.Attributes.Add("readonly", "readonly")
            txtdatefinish.Attributes.Add("readonly", "readonly")
        End If
    End Sub

    Public Sub txtfinish_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtfinish.TextChanged
        'Dim dt1 As Date = txtdate.Text + " " + txtstart.Text
        'Dim dt2 As Date = txtdate.Text + " " + txtfinish.Text
        ''Dim timestr As String
        'Dim a As String
        'Dim lspembulatan As String

        'a = (dt2 - dt1).TotalHours
        ''timestr = dt2.ToString("HH:mm")
        'lspembulatan = Math.Round(CDbl(a), 2)
        'lbltotal.Text = lspembulatan + " " + "Jam"
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsave.Click
        Dim sqlinsert As String
        Dim cmdinsert As SqlCommand
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim year As String
        Dim month As String
        Dim periode, createdin, stransno, transno As String
        Dim dt1 As Date
        Dim dt2 As Date
        Dim dtb As DataTable = New DataTable()
        Dim dtbcheck As DataTable = New DataTable()
        Dim dtbperiod As DataTable = New DataTable()
        Dim dtbdel As DataTable = New DataTable()
        Dim dtbdel2 As DataTable = New DataTable()
        Dim dtbchklvl As DataTable = New DataTable()
        Dim dtbnik As DataTable = New DataTable()
        Dim a As String
        Dim jumlahwaktu, pesan As String
        Dim status As Integer

        btnsave.Enabled = False

        status = 0
        pesan = ""
        createdin = System.Net.Dns.GetHostName().ToString
        'year = Today.Year.ToString

        'cek delegasi
        Try
            'jika app2 GM cek departemen yg bisa diapproval
            'Dim strchklvl As String = "select kdlevel from h_A101 where nik = '" + hnikspv2.Value + "'"
            'Dim sdachklvl As SqlDataAdapter = New SqlDataAdapter(strchklvl, sqlConn)
            'sdachklvl.Fill(dtbchklvl)

            'If dtbchklvl.Rows.Count > 0 Then

            'End If

            'delegasi untuk approval
            Dim strdel As String = "select nikdel, (select nama from H_a101 where nik = a.nikdel) as nama from H_H11002 a where nikreq = '" + hnikspv1.Value + "' and date_from <= getdate() and date_to >= getdate() and stedit <> 2"
            Dim sdadel As SqlDataAdapter = New SqlDataAdapter(strdel, sqlConn)
            sdadel.Fill(dtbdel)

            Dim strdel2 As String
            If hisgm.Value = "1" Then
                strdel2 = "select nikdel, (select nama from H_a101 where nik = a.nikdel) as nama from H_H11002 a where nikreq = '" + hnikspv2.Value + "' and date_from <= getdate() and date_to >= getdate() and kddepar = '" + Session("kddepar") + "' and stedit <> 2"
            Else
                strdel2 = "select nikdel, (select nama from H_a101 where nik = a.nikdel) as nama from H_H11002 a where nikreq = '" + hnikspv2.Value + "' and date_from <= getdate() and date_to >= getdate() and stedit <> 2 ORDER BY nikdel DESC"
            End If


            Dim sdadel2 As SqlDataAdapter = New SqlDataAdapter(strdel2, sqlConn)
            sdadel2.Fill(dtbdel2)

            If dtbdel.Rows.Count > 0 Then
                hnikspv1.Value = dtbdel.Rows(0)!nikdel.ToString
                txtspv1.Text = dtbdel.Rows(0)!nama.ToString
            End If

            If dtbdel2.Rows.Count > 0 Then
                hnikspv2.Value = dtbdel2.Rows(0)!nikdel.ToString
                txtspv2.Text = dtbdel2.Rows(0)!nama.ToString


                'Dim nikSpvDefault As String = String.Empty
                'Dim nameSpvDefault As String = String.Empty

                'For Each row As DataRow In dtbdel2.Rows

                '    If hnikspv2.Value = row!nikdel.ToString Then
                '        nikSpvDefault = row!nikdel.ToString
                '        nameSpvDefault = row!nama.ToString
                '    End If

                'Next row

                'hnikspv2.Value = nikSpvDefault
                'txtspv2.Text = nameSpvDefault
            End If

            If hnikspv1.Value = hnikspv2.Value Then
                hnikspv1.Value = ""
                txtspv1.Text = ""
            End If
        Catch ex As Exception

        End Try

        chkdata(status, pesan)
        If hnikspv1.Value = "" Then
            hnikspv1.Value = ""
        End If

        If status = 1 Then
            msg = pesan
            btnsave.Enabled = True
            Return
        End If

        dt1 = txtdatestart.Text + " " + txtstart.Text
        dt2 = txtdatefinish.Text + " " + txtfinish.Text
        year = CDate(txtdate.Text).Year
        month = CDate(txtdate.Text).Month

        If month < 10 Then month = "0" + month

        Dim strperiod As String = "select kdsite,Periode,GDari,gsampai from H_A116 where kdsite ='" + txtsite.Text + "' and GDari  <= '" + txtdate.Text + "'  and GSampai >= '" + txtdate.Text + "'"
        Dim sdaperiod As SqlDataAdapter = New SqlDataAdapter(strperiod, sqlConn)
        sdaperiod.Fill(dtbperiod)

        If dtbperiod.Rows.Count > 0 Then
            periode = dtbperiod.Rows(0)!Periode.ToString
        End If

        'periode = year + month

        a = (dt2 - dt1).TotalMinutes
        jumlahwaktu = Math.Round(CDbl(a), 2)

        If recordid.Value <> "Auto" Then

            Try
                sqlConn.Open()
                sqlinsert = "update H_H110 set periode = '" + periode + "', jam_start = '" + dt1 + "', jam_end = '" + dt2 + "',app1 = '" + hnikspv1.Value + "', app2 = '" + hnikspv2.Value + "', checkapp1 = '" + hnikspv1.Value + "', checkapp2 = '" + hnikspv2.Value + "' where recordid = '" + recordid.Value + "'"
                cmdinsert = New SqlCommand(sqlinsert, sqlConn)
                cmdinsert.ExecuteScalar()
                msg = "<script type='text/javascript'> alert('Data has been update'); </script>"
                btnsave.Enabled = True
            Catch ex As Exception

            Finally
                sqlConn.Close()
            End Try
        Else
            If recordid.Value = "Auto" Then
                Dim strcheck As String = "select nik, jam_start from H_H110 where nik = '" + hnik.Value + "' and jam_start = '" + dt1 + "'"
                Dim sdacheck As SqlDataAdapter = New SqlDataAdapter(strcheck, sqlConn)
                sdacheck.Fill(dtbcheck)

                If dtbcheck.Rows.Count > 0 Then
                    msg = "<script type='text/javascript'> alert('Employee and time for today is already exist'); </script>"
                    btnsave.Enabled = True
                    Return
                End If


                stransno = periode + "/" + txtsite.Text + "/" + txtcostcode.Text + "/"

                Dim strcon As String = "select Noreg from H_H110 where Noreg like '%" + stransno + "%'"
                Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
                sda.Fill(dtb)

                If dtb.Rows.Count = 0 Then
                    transno = stransno + "000001"
                ElseIf dtb.Rows.Count > 0 Then
                    I = 0
                    I = dtb.Rows.Count + 1
                    If dtb.Rows.Count > 0 And dtb.Rows.Count < 9 Then
                        transno = stransno + "00000" + I.ToString
                    ElseIf dtb.Rows.Count >= 9 And dtb.Rows.Count < 99 Then
                        transno = stransno + "0000" + I.ToString
                    ElseIf dtb.Rows.Count >= 99 And dtb.Rows.Count < 999 Then
                        transno = stransno + "000" + I.ToString
                    ElseIf dtb.Rows.Count >= 999 And dtb.Rows.Count < 9999 Then
                        transno = stransno + "00" + I.ToString
                    ElseIf dtb.Rows.Count >= 9999 And dtb.Rows.Count < 99999 Then
                        transno = stransno + "0" + I.ToString
                    ElseIf dtb.Rows.Count >= 99999 And dtb.Rows.Count < 999999 Then
                        transno = stransno + I.ToString
                    ElseIf dtb.Rows.Count >= 999999 Then
                        transno = "Error on generate Noreg"
                    End If
                End If

                Dim strcon2 As String = "select niksite from h_A101 where nik =  '" + hnikspv2.Value + "'"
                Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn)
                sda2.Fill(dtbnik)
                Dim niksiteapp2 = ""

                If dtbnik.Rows.Count > 0 Then
                    niksiteapp2 = dtbnik.Rows(0)!niksite.ToString()
                End If
                Dim stask As String = Replace(txtket.Text, "'", "''")
                Try
                    sqlConn.Open()
                    sqlinsert = "insert into H_H110 (noreg, periode, nik, tanggal, jam_start, jam_end, kdsite, kddepar, kdjabat, remarks, fstatus, createdby, createdin, createdtime, stedit, app1, app2, checkapp1, checkapp2, requestby, SPLActual, freject1, freject2, approval) values ('" + transno + "', '" + periode + "', '" + hnik.Value + "', '" + txtdate.Text + "', '" + dt1 + "', '" + dt2 + "', '" + hkdsite.Value + "', '" + hkddepar.Value + "', '" + hkdjabat.Value + "', '" + stask + "', '0', '" + Session("otorisasi").ToString + "', '" + createdin + "', '" + Date.Now.ToString + "', 0, '" + hnikspv1.Value + "', '" + hnikspv2.Value + "', '" + hnikspv1.Value + "', '" + hnikspv2.Value + "', '" + hnikreq.Value + "', '" + jumlahwaktu + "', 0, 0, '" + niksiteapp2 + "')"
                    cmdinsert = New SqlCommand(sqlinsert, sqlConn)
                    cmdinsert.ExecuteScalar()
                    'recordid.Value = transno
                    regno.Value = transno
                    msg = "<script type='text/javascript'> alert('Data has been save'); </script>"
                    btnsubmit.Visible = True
                Catch ex As Exception

                Finally
                    sqlConn.Close()
                End Try
            End If
        End If

    End Sub

    Private Sub btnprint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnprint.Click
        Response.Redirect("cetak_spl.aspx")
    End Sub

    Public Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        'btnsave.Attributes.Add("onclick", "javascript:" + btnsave.ClientID + ".disabled=false;" + ClientScript.GetPostBackEventReference(btnsave, ""))

        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim sqlinsert As String
        Dim cmdinsert As SqlCommand
        Dim message As New MailMessage()
        Dim message2 As New MailMessage()
        Dim mail As String = ""
        Dim dtbemail As DataTable = New DataTable
        Dim dtbemail2 As DataTable = New DataTable
        Dim status, licount As Integer
        Dim pesan As String

        status = 0
        pesan = ""

        chkdata(status, pesan)

        If status = 1 Then
            msg = pesan
            btnsave.Visible = True
            btnsubmit.Visible = True
            Return
        End If
        'hnikspv1.Value = "0004696"
        'hnikspv2.Value = "0001576"
        Dim stask As String = Replace(txtket.Text, "'", "")
        Try
            sqlConn.Open()

            Dim strcon As String = "select email from H_A101 where nik in ( '" + hnikspv1.Value + "')"
            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtbemail)

            If dtbemail.Rows.Count > 0 Then
                mail = dtbemail.Rows(0)!email.ToString


                message = New MailMessage()
                message.From = New MailAddress("noreply.overtime@jresources.com", "Overtime Request Approval")
                message.Subject = "Overtime Request Reminder " + recordid.Value
                message.IsBodyHtml = True
                message.Body = "Dear, <br><br>"
                message.Body = message.Body + "Please do Overtime Requests Approval of the following : <br><br>"
                message.Body = message.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:10%'>NIK</th><th style='width:30%'>Name</th><th style='width:20%'>Time Start</th><th style='width:20%'>Time End</th><th style='width:20%'>Task</th>"
                message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" + txtnik.Text + "</td><td style='text-align:left;border:solid 1px'>" + txtnama.Text + "</td><td style='text-align:center;border:solid 1px'>" + txtdatestart.Text + " " + txtstart.Text + "</td><td style='text-align:center;border:solid 1px'>" + txtdatefinish.Text + " " + txtfinish.Text + "</td><td style='text-align:left;border:solid 1px'>" + stask + "</td></tr></table>"
                message.Body = message.Body + "<br>To Approval please click here <a href='http://moss.jresources.com/_layouts/forwarder.aspx?FR=BT&page=app1ot'>portal.jresources.com</a>"

                message.Body = message.Body + "<br><br> Regards, <br><br> <strong>Admin</strong>"

                If dtbemail.Rows.Count > 0 Then
                    For licount = 0 To dtbemail.Rows.Count - 1
                        message.To.Add(dtbemail.Rows(licount)!email.ToString)
                    Next
                End If

                Dim smtp As New SmtpClient()
                'smtp.Host = "smtp.gmail.com"
                smtp.Host = "webmail.jresources.com"
                'smtp.Port = 587
                smtp.Port = 25
                smtp.EnableSsl = True
                'smtp.Credentials = New System.Net.NetworkCredential("noreply.overtime@jresources.com", "jrec54321")
                smtp.Credentials = New System.Net.NetworkCredential("noreply.overtime@jresources.com", "jrec12345")

                smtp.Send(message)
            End If

            Dim strcon2 As String = "select email from H_A101 where nik in ('" + hnikspv2.Value + "')"
            Dim dtb2 As DataTable = New DataTable
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn)
            sda2.Fill(dtbemail2)

            If dtbemail2.Rows.Count > 0 Then
                mail = dtbemail2.Rows(0)!email.ToString


                message2 = New MailMessage()
                message2.From = New MailAddress("noreply.overtime@jresources.com", "Overtime Request Approval")
                message2.Subject = "Overtime Request Reminder " + recordid.Value
                message2.IsBodyHtml = True
                message2.Body = "Dear, <br><br>"
                message2.Body = message2.Body + "Please do Overtime Requests Approval of the following : <br><br>"
                message2.Body = message2.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:10%'>NIK</th><th style='width:30%'>Name</th><th style='width:20%'>Time Start</th><th style='width:20%'>Time End</th><th style='width:20%'>Task</th>"
                message2.Body = message2.Body + "<tr><td style='text-align:left;border:solid 1px'>" + txtnik.Text + "</td><td style='text-align:left;border:solid 1px'>" + txtnama.Text + "</td><td style='text-align:center;border:solid 1px'>" + txtdatestart.Text + " " + txtstart.Text + "</td><td style='text-align:center;border:solid 1px'>" + txtdatefinish.Text + " " + txtfinish.Text + "</td><td style='text-align:left;border:solid 1px'>" + stask + "</td></tr></table>"
                message2.Body = message2.Body + "<br>To Approval please click here <a href='http://moss.jresources.com/_layouts/forwarder.aspx?FR=BT&page=app2ot'>portal.jresources.com</a>"

                message2.Body = message2.Body + "<br><br> Regards, <br><br> <strong>Admin</strong>"

                If dtbemail2.Rows.Count > 0 Then
                    For licount = 0 To dtbemail2.Rows.Count - 1
                        message2.To.Add(dtbemail2.Rows(licount)!email.ToString)
                    Next
                End If

                Dim smtp2 As New SmtpClient()
                'smtp.Host = "smtp.gmail.com"
                smtp2.Host = "webmail.jresources.com"
                'smtp.Port = 587
                smtp2.Port = 25
                smtp2.EnableSsl = True
                'smtp.Credentials = New System.Net.NetworkCredential("noreply.overtime@jresources.com", "jrec54321")
                smtp2.Credentials = New System.Net.NetworkCredential("noreply.overtime@jresources.com", "jrec12345")

                smtp2.Send(message2)
            End If

            If recordid.Value = "Auto" Then
                sqlinsert = "update H_H110 set stedit = '1' where noreg = '" + regno.Value + "'"
                cmdinsert = New SqlCommand(sqlinsert, sqlConn)
                cmdinsert.ExecuteScalar()
            Else
                sqlinsert = "update H_H110 set stedit = '1' where recordid = '" + recordid.Value + "'"
                cmdinsert = New SqlCommand(sqlinsert, sqlConn)
                cmdinsert.ExecuteScalar()
            End If

            msg = "<script type='text/javascript'> alert('Data has been submit email has been sent'); </script>"

            btnsubmit.Visible = False
            btnsave.Visible = False
        Catch ex As Exception

        Finally
            sqlConn.Close()
        End Try
    End Sub

    Public Sub chkdata(ByRef status As Integer, ByRef pesan As String)
        'btnsubmit.Attributes.Add("onclick", "javascript:" + btnsubmit.ClientID + ".disabled=true;" + ClientScript.GetPostBackEventReference(btnsubmit, ""))
        If txtcostcode.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Costcode cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        If txtcostcodereq.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Request costcode cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        If txtdate.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Date cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        If txtdatefinish.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Finish Date cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        If txtdatestart.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Start Date cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        If txtdept.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Department cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        If txtdeptreq.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Request Department cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        If txtfinish.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Time Finish cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        If txtket.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Task cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        If txtnama.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Name cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        If txtnamareq.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Request Name cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        If txtnik.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Nik cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        If txtnikreq.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Request Nik cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        'If txtposition.Text = "" Then
        '    pesan = "<script type='text/javascript'> alert('Position cannot be empty, All fields must be filled'); </script>"
        '    status = 1
        '    return
        'End If

        'If txtpositionreq.Text = "" Then
        '    pesan = "<script type='text/javascript'> alert('Request position cannot be empty, All fields must be filled'); </script>"
        '    status = 1
        '   return
        'End If

        If txtsite.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Site cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        If txtsitereq.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Request site cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        'If txtspv1.Text = "" Then
        '    pesan = "<script type='text/javascript'> alert('Approval 1 cannot be empty, All fields must be filled'); </script>"
        '    status = 1
        '    Return
        'End If

        If txtspv2.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Approval 2 cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        'If txtspv1.Text = txtspv2.Text Then
        '    pesan = "<script type='text/javascript'> alert('Save Failed : Approval 1 cannot same with Approval 2'); </script>"
        '    status = 1
        '    Return
        'End If

        If txtstart.Text = "" Then
            pesan = "<script type='text/javascript'> alert('Time Start cannot be empty, All fields must be filled'); </script>"
            status = 1
            Return
        End If

        If CDate(txtdatefinish.Text) < CDate(txtdatestart.Text) Then
            pesan = "<script type='text/javascript'> alert('Save Failed : Date Start cannot larger than date finish'); </script>"
            status = 1
            Return
        End If

        If hkddepar.Value <> hkddeparreq.Value Then
            pesan = "<script type='text/javascript'> alert('Save Failed : Requestor and employee OT must in the same department'); </script>"
            status = 1
            Return
        End If
    End Sub
End Class