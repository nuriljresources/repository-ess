﻿Imports System.Data
Imports System.Data.SqlClient
Public Class medic
    Public Function Fetch() As DataTable
        Dim myDataTable As DataTable = New DataTable()

        Dim myDataColumn As DataColumn

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "TglDet"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "TglDetOut"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "person"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "nama"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "HrgKmr"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "hari"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "persenclaim"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "biayakons"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "biayaobat"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "biayalain"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "biayalab"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "biayatot"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "diagnosa"
        myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn()
        'myDataColumn.DataType = Type.GetType("System.String")
        'myDataColumn.ColumnName = "tdokter"
        'myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn()
        'myDataColumn.DataType = Type.GetType("System.String")
        'myDataColumn.ColumnName = "jdokter"
        'myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn()
        'myDataColumn.DataType = Type.GetType("System.String")
        'myDataColumn.ColumnName = "kdspesialis"
        'myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn()
        'myDataColumn.DataType = Type.GetType("System.String")
        'myDataColumn.ColumnName = "trs"
        'myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn()
        'myDataColumn.DataType = Type.GetType("System.String")
        'myDataColumn.ColumnName = "jrs"
        'myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn()
        'myDataColumn.DataType = Type.GetType("System.String")
        'myDataColumn.ColumnName = "kdrs"
        'myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn()
        'myDataColumn.DataType = Type.GetType("System.String")
        'myDataColumn.ColumnName = "kddiagnosa"
        'myDataTable.Columns.Add(myDataColumn)

        Return myDataTable
    End Function

    Public Function FetchOutpatient() As DataTable
        Dim myDataTable As DataTable = New DataTable()

        Dim myDataColumn As DataColumn

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "TglDet"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "person"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "nama"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "persenclaim"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "biayakons"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "biayaobat"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "biayalain"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "biayalab"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "biayatot"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "tdokter"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "jdokter"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "kdspesialis"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "kddiagnosa"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "dremarks"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "dokter_remarks"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "htdokter"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "jdokternama"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "nmspesialis"
        myDataTable.Columns.Add(myDataColumn)

        Return myDataTable
    End Function

    Public Sub Insert(ByVal medicNewdt As String, ByVal medicNewdtout As String, ByVal medicNewNama As String, ByVal medicnewperson As String, ByVal medicNewHargaKamar As String, ByVal medicNewHari As String, ByVal medicNewperclaim As String, ByVal medicNewbiayakons As String, ByVal medicNewbiayaobat As String, ByVal medicNewbiayalain As String, ByVal medicNewbiayalab As String, ByVal medicNewbiayatot As String, ByVal medicNewdiagnosa As String, ByVal myTable As DataTable)
        Dim row As DataRow
        Dim datenumber As Double

        row = myTable.NewRow()

        Dim no As String

        If myTable.Rows.Count = "1" And myTable.Rows(0)!No.ToString = "1" Then
            no = myTable.Rows.Count + 1
        ElseIf myTable.Rows.Count > "1" Then
            no = myTable.Rows.Count + 1
        Else
            no = myTable.Rows.Count
        End If

        If medicNewHargaKamar = "" Then medicNewHargaKamar = 0
        If medicNewHari = "" Then medicNewHari = 0
        If medicNewbiayakons = "" Then medicNewbiayakons = 0
        If medicNewbiayaobat = "" Then medicNewbiayaobat = 0
        If medicNewbiayalain = "" Then medicNewbiayalain = 0
        If medicNewbiayalab = "" Then medicNewbiayalab = 0

        datenumber = DateDiff("d", CDate(medicNewdt), CDate(medicNewdtout)) + 1
        medicNewbiayatot = (medicNewHargaKamar * datenumber) + medicNewbiayakons + medicNewbiayaobat + medicNewbiayalain + medicNewbiayalab

        row("No") = no
        row("TglDet") = medicNewdt
        row("TglDetOut") = medicNewdtout
        row("person") = medicnewperson
        row("nama") = medicNewNama
        row("HrgKmr") = medicNewHargaKamar
        row("hari") = datenumber
        row("persenclaim") = medicNewperclaim
        row("biayakons") = medicNewbiayakons
        row("biayaobat") = medicNewbiayaobat
        row("biayalain") = medicNewbiayalain
        row("biayalab") = medicNewbiayalab
        row("biayatot") = medicNewbiayatot
        row("diagnosa") = medicNewdiagnosa

        myTable.Rows.Add(row)

        If Convert.IsDBNull(myTable.Rows(0)!TglDet) Or myTable.Rows(0)!TglDet = "1/1/1900" Then
            myTable.Rows.RemoveAt(0)
        End If
    End Sub

    Public Sub InsertOutpatient(ByVal medicNewdt As String, ByVal medicNewNama As String, ByVal medicnewperson As String, ByVal medicNewperclaim As String, ByVal medicNewbiayakons As String, ByVal medicNewbiayaobat As String, ByVal medicNewbiayalain As String, ByVal medicNewbiayalab As String, ByVal medicNewbiayatot As String, ByVal medictdokter As String, ByVal medichtdokter As String, ByVal medicjdoktername As String, ByVal medicjdokter As String, ByVal ddlnewspecialistnama As String, ByVal ddlnewspecialist As String, ByVal medicNewdiagnosa As String, ByVal medicNewdoctorname As String, ByVal medicNewremarks As String, ByVal myTable As DataTable)
        Dim row As DataRow
        Dim nmjdokter(1) As String
        Dim nmsp(0) As String

        row = myTable.NewRow()

        Dim no As String

        If myTable.Rows.Count = "1" And myTable.Rows(0)!No.ToString = "1" Then
            no = myTable.Rows.Count + 1
        ElseIf myTable.Rows.Count > "1" Then
            no = myTable.Rows.Count + 1
        Else
            no = myTable.Rows.Count
        End If

        If medicNewbiayakons = "" Then medicNewbiayakons = 0
        If medicNewbiayaobat = "" Then medicNewbiayaobat = 0
        If medicNewbiayalain = "" Then medicNewbiayalain = 0
        If medicNewbiayalab = "" Then medicNewbiayalab = 0
        medicNewbiayatot = CDec(medicNewbiayakons) + CDec(medicNewbiayaobat) + CDec(medicNewbiayalain) + CDec(medicNewbiayalab)

        'jdoctor(medicjdokter, nmjdokter)
        spdoctor(ddlnewspecialist, nmsp)

        'If medictdokter = "0" Then medictdokter = "Doctor"
        'If medictdokter = "1" Then medictdokter = "Non Doctor"

        row("No") = no
        row("TglDet") = medicNewdt
        row("person") = medicnewperson
        row("nama") = medicNewNama
        row("persenclaim") = medicNewperclaim
        row("biayakons") = medicNewbiayakons
        row("biayaobat") = medicNewbiayaobat
        row("biayalain") = medicNewbiayalain
        row("biayalab") = medicNewbiayalab
        row("biayatot") = medicNewbiayatot
        row("tdokter") = medictdokter
        row("jdokter") = medicjdokter
        row("kdspesialis") = ddlnewspecialist
        row("kddiagnosa") = medicNewdiagnosa
        row("dremarks") = medicNewremarks
        row("dokter_remarks") = medicNewdoctorname
        row("htdokter") = medichtdokter
        row("jdokternama") = medicjdoktername
        row("nmspesialis") = ddlnewspecialistnama
        myTable.Rows.Add(row)

        If Convert.IsDBNull(myTable.Rows(0)!TglDet) = True Or myTable.Rows(0)!TglDet.ToString = "1/1/1900" Then
            myTable.Rows.RemoveAt(0)
        End If
    End Sub

    Public Sub Update(ByVal medicdate As String, ByVal medicdateout As String, ByVal medicperson As String, ByVal medicNama As String, ByVal medicHargaKamar As String, ByVal medicHari As String, ByVal medicperclaim As String, ByVal medicbiayakons As String, ByVal medicbiayaobat As String, ByVal medicbiayalain As String, ByVal medicbiayalab As String, ByVal medicbiayatot As String, ByVal medicdiagnose As String, ByVal myTable As DataTable, ByVal baris As String)
        Dim row As DataRow
        Dim datenumber As Double

        If medicHargaKamar = "" Then medicHargaKamar = 0
        If medicHari = "" Then medicHari = 0
        If medicbiayakons = "" Then medicbiayakons = 0
        If medicbiayaobat = "" Then medicbiayaobat = 0
        If medicbiayalain = "" Then medicbiayalain = 0
        If medicbiayalab = "" Then medicbiayalab = 0

        datenumber = DateDiff("d", CDate(medicdate), CDate(medicdateout)) + 1
        medicbiayatot = (medicHargaKamar * datenumber) + medicbiayakons + medicbiayaobat + medicbiayalain + medicbiayalab

        myTable.Rows(baris)!TglDet = medicdate
        myTable.Rows(baris)!TglDetOut = medicdateout
        myTable.Rows(baris)!Person = medicperson
        myTable.Rows(baris)!nama = medicNama
        myTable.Rows(baris)!hrgkmr = medicHargaKamar
        myTable.Rows(baris)!hari = datenumber
        myTable.Rows(baris)!persenclaim = medicperclaim
        myTable.Rows(baris)!biayakons = medicbiayakons
        myTable.Rows(baris)!biayaobat = medicbiayaobat
        myTable.Rows(baris)!biayalain = medicbiayalain
        myTable.Rows(baris)!biayalab = medicbiayalab
        myTable.Rows(baris)!biayatot = medicbiayatot
        myTable.Rows(baris)!diagnosa = medicdiagnose
    End Sub

    Public Sub UpdateOutpatient(ByVal medicdt As String, ByVal medicNama As String, ByVal medicperson As String, ByVal medicperclaim As String, ByVal medicbiayakons As String, ByVal medicbiayaobat As String, ByVal medicbiayalain As String, ByVal medicbiayalab As String, ByVal medicbiayatot As String, ByVal medictdoktername As String, ByVal medictdokter As String, ByVal medicjdoktername As String, ByVal medicjdokter As String, ByVal ddlspecialistnama As String, ByVal ddlspecialist As String, ByVal medicdiagnosa As String, ByVal medicdoctorname As String, ByVal medicremarks As String, ByVal myTable As DataTable, ByVal baris As String)
        Dim row As DataRow

        If medicbiayakons = "" Then medicbiayakons = 0
        If medicbiayaobat = "" Then medicbiayaobat = 0
        If medicbiayalain = "" Then medicbiayalain = 0
        If medicbiayalab = "" Then medicbiayalab = 0
        medicbiayatot = CDec(medicbiayakons) + CDec(medicbiayaobat) + CDec(medicbiayalain) + CDec(medicbiayalab)

        myTable.Rows(baris)!TglDet = medicdt
        myTable.Rows(baris)!Person = medicNama
        myTable.Rows(baris)!nama = medicperson
        myTable.Rows(baris)!persenclaim = medicperclaim
        myTable.Rows(baris)!biayakons = medicbiayakons
        myTable.Rows(baris)!biayaobat = medicbiayaobat
        myTable.Rows(baris)!biayalain = medicbiayalain
        myTable.Rows(baris)!biayalab = medicbiayalab
        myTable.Rows(baris)!biayatot = medicbiayatot
        myTable.Rows(baris)!tdokter = medictdoktername
        myTable.Rows(baris)!jdokter = medicjdokter
        myTable.Rows(baris)!kdspesialis = ddlspecialist
        myTable.Rows(baris)!kddiagnosa = medicdiagnosa
        myTable.Rows(baris)!dremarks = medicremarks
        myTable.Rows(baris)!dokter_remarks = medicdoctorname
        myTable.Rows(baris)!htdokter = medictdokter
        myTable.Rows(baris)!jdokternama = medicjdoktername
        myTable.Rows(baris)!nmspesialis = ddlspecialistnama
    End Sub

    Public Sub Delete(ByVal myTable As DataTable, ByVal baris As String)
        Dim i As Integer
        myTable.Rows(baris).Delete()
        myTable.AcceptChanges()
    End Sub

    Public Function f_amount_in_word(ByVal ad_amount As Double) As String
        Dim ld_divisor, ld_large_amount, ld_tiny_amount, ld_dividen, ld_dummy As Double
        Dim ls_weight1, ls_weight2, ls_unit, ls_follower, ls_word As String
        Dim ls_prefix(), ls_sufix() As String

        ls_prefix = New String() {" SE", "DUA ", "TIGA ", "EMPAT ", "LIMA ", "ENAM ", "TUJUH ", "DELAPAN ", "SEMBILAN "}
        ls_sufix = New String() {"SATU ", "DUA ", "TIGA ", "EMPAT ", "LIMA ", "ENAM ", "TUJUH ", "DELAPAN ", "SEMBILAN "}

        ls_word = ""

        ad_amount = Math.Truncate(ad_amount)
        ld_large_amount = Math.Abs(Math.Truncate(ad_amount))
        ld_tiny_amount = Math.Round((Math.Abs(ad_amount) - ld_large_amount) * 100, 0)


        ld_divisor = 1000000000000.0

        If ld_large_amount > ld_divisor Then Return "Melebihi Batas Angka yang ditentukan"

        Do While ld_divisor >= 1
            ld_dividen = Math.Truncate(ld_large_amount / ld_divisor)
            ld_large_amount = ld_large_amount Mod ld_divisor

            ls_unit = ""
            If ld_dividen > 0 Then
                Select Case ld_divisor
                    Case 1000000000000.0
                        ls_unit = "TRILYUN "
                    Case 1000000000.0
                        ls_unit = "MILYAR "
                    Case 1000000.0
                        ls_unit = "JUTA "
                    Case 1000.0
                        ls_unit = "RIBU "
                End Select
            End If
            ls_weight1 = ""
            ld_dummy = ld_dividen
            If ld_dummy >= 100 Then ls_weight1 = ls_prefix(Math.Truncate(ld_dummy / 100) - 1) + "RATUS "



            ld_dummy = ld_dividen Mod 100
            Select Case ld_dummy
                Case Is < 10
                    If ld_dummy = 1 And ls_unit = "RIBU " Then
                        ls_weight1 += "SE"
                    ElseIf ld_dummy > 0 Then
                        ls_weight1 += ls_sufix(ld_dummy - 1)
                    End If
                Case 11 To 19
                    ls_weight1 += ls_prefix((ld_dummy Mod 10) - 1) + "BELAS "
                Case Else
                    ls_weight1 += ls_prefix(Math.Truncate(ld_dummy / 10) - 1) + "PULUH "
                    If (ld_dummy Mod 10) > 0 Then ls_weight1 += ls_sufix((ld_dummy Mod 10) - 1)
            End Select


            ls_word += ls_weight1 + ls_unit
            ld_divisor /= 1000.0
        Loop

        If Math.Truncate(ad_amount) = 0 Then ls_word = "NOL "
        ls_follower = ""

        Select Case ld_tiny_amount
            Case Is < 10
                If ld_tiny_amount > 0 Then ls_follower = "KOMA NOL " + ls_sufix(ld_tiny_amount - 1)
            Case Else
                ls_follower = "KOMA " + ls_sufix(Math.Truncate(ld_tiny_amount / 10) - 1)
                If (ld_tiny_amount Mod 10) > 0 Then ls_follower += ls_sufix((ld_tiny_amount Mod 10) - 1)
        End Select


        ls_word += ls_follower
        If ad_amount < 0 Then ls_word = "MINUS " + ls_word


        Return Trim(ls_word)

    End Function

    Public Sub jdoctor(ByVal kddoc As String, ByVal jnsdoc() As String)
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim qry As String
        Dim dtb As DataTable = New DataTable()

        qry = "select jDokter, jNamaDokter, tdokter from H_A21504 where jdokter = '" + kddoc + "'"
        Dim sda As SqlDataAdapter = New SqlDataAdapter(qry, sqlConn)
        sda.Fill(dtb)

        If dtb.Rows.Count > 0 Then
            jnsdoc(0) = dtb.Rows(0)!jNamaDokter.ToString
        End If
    End Sub

    Public Sub spdoctor(ByVal kdsp As String, ByVal nmsp() As String)
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim qry As String
        Dim dtb As DataTable = New DataTable()

        qry = "select kdspesialis, nmspesialis from H_A21505 where kdspesialis = '" + kdsp + "'"
        Dim sda As SqlDataAdapter = New SqlDataAdapter(qry, sqlConn)
        sda.Fill(dtb)

        If dtb.Rows.Count > 0 Then
            nmsp(0) = dtb.Rows(0)!nmspesialis.ToString
        End If
    End Sub
End Class
