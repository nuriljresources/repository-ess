﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/permit/master.Master" CodeBehind="index.aspx.vb" Inherits="EXCELLENT.index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Document Center Home
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {width: 20%;height: 88px;text-align: left;}
        .style2
        {width: 70%;height: 88px;}
        .style3
        {width: 10%;height: 88px;text-align: left;}
        
            #dhtmltooltip{
            position: absolute;
            width: 150px;
            border: 2px solid black;
            padding: 2px;
            background-color: yellow;
            visibility: hidden;
            z-index: 100;
            /*Remove below line to remove shadow. Below line should always appear last within this CSS*/
            /*filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,direction=135);*/
            }
        #form1
        {
            font-family:Tahoma,Arial;
        }
        #form1 h1 
        {
            color: #FFFFFF;
            font-size:x-large;
        }
        .title
        {
            color: blue;
            font-size: small;
            margin-bottom:10px;
        }
        .line
        {
            width: 100%;
            height: 20px;
            font-family:Tahoma,Arial;
        }
        .general
        {
            font-size:0.8em;
            color:#453944;
            font-weight:bold;
            border-style:solid;
            border-width:1px;
            border-color:#33ADFF;
            padding-left:5px;
        }
        
        .general a
        {
            text-decoration:none;
            color:#33ADFF;
        }
        
        .general a:hover
        {
            text-decoration:underline;
            color:#33ADFF;
        }
        .style1
        {
            height: 26px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <%--<div>
        <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtpoint" runat="server"></asp:TextBox>
        <asp:Button ID="btnAdd_Click" runat="server" Text="Add New User" /><asp:Button ID="btnsum" runat="server" Text="Sum" />  <br />
        <asp:Label ID="lblTips" runat="server" ForeColor="Red" Text=""></asp:Label><br />
         <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="346px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" EmptyDataText="There is no any data." ForeColor="Red">
            <Columns>
                <asp:BoundField DataField="id" HeaderText="ID" Visible="False" />
                <asp:BoundField DataField="username" HeaderText="UserName" />
                <asp:BoundField DataField="firstname" HeaderText="First Name" />
                <asp:BoundField DataField="lastname" HeaderText="Last Name" />
                <asp:BoundField DataField="point" HeaderText="Point" />
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
        </asp:GridView>
        
    </div>--%>
    
     <div class="line" style="right:10px; text-align:right;">
    <a href="signout.aspx">Logout</a>
    </div>
    <div class="title">
        <strong>Menu</strong>
    </div>
    <div id="list_newest" class="general" style="width:200px; color:#33ADFF;">
        <div id="Div1"><img alt="open document" src="../images/open_doc_small.png" /> <a href="index.aspx">&nbsp;Home</a></div>
        <%=mnentry%>
        <%=mnview%>
        <%=mnmgmt%>
    <div><img alt="open document" src="../images/open_doc_small.png" /> <a href="J Resources Document Center Manual Version 1.pdf" target="_blank">&nbsp;Help</a></div>
    </div>
    <div id="list_permit" class="general" style="width:1000px; border-style:none; height:400px; left:240px; top:20px; position:absolute;">
        <table>
        <tr>
        <td class="general"> <%=jrnnum%> </td>
        <td class="general"><%=nmuser%></td>
        <td class="general"><%=level%></td>
        <td class="general"><%=depar%></td>
        </tr>
        </table>
        <br />
        <asp:Label ID="top5" Text ="Top 5 Expired Document in next 1 Month" 
            runat="server" ForeColor="Black"></asp:Label><br /><br />
        <asp:GridView ID="GridView1" runat="server" DataKeyNames="id" AutoGenerateColumns="False"
            AllowPaging="True" PageSize="50" Width="1000px" 
            CssClass="data-table">
            <Columns>
            <asp:BoundField DataField="doctype" HeaderText="Type" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="fsubject" HeaderText="Subject" HeaderStyle-Width="200px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="200px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="company_name" HeaderText="Company" HeaderStyle-Width="400px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="400px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="site_name" HeaderText="Site" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
           <%-- <asp:BoundField DataField="fcategory" HeaderText="Category" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>--%>
            <%--<asp:BoundField DataField="createdtime" HeaderText="Created Date" HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top"> 
                <HeaderStyle Width="200px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="cms_start" HeaderText="Start Date" HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top"> 
                <HeaderStyle Width="200px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>--%>
            <asp:BoundField DataField="cms_end" HeaderText="Expired Date" HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top"> 
                <HeaderStyle Width="200px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
           <%-- <asp:BoundField DataField="pic1" HeaderText="Doc Admin" HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top"> 
                <HeaderStyle Width="200px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:TemplateField HeaderText="PIC" HeaderStyle-Width="100px">
                <ItemTemplate>
                    <asp:Label ID="PIC" Text='<%# eval("pic2") + "," + eval("pic3") + "," + eval("pic4") + "," + eval("pic5") %>' runat="server" ></asp:Label>
                </ItemTemplate> 

<HeaderStyle Width="100px"></HeaderStyle>
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="File Name" HeaderStyle-Width="300px">
                <ItemTemplate>
                    <%--<asp:HyperLink ID="HyperLink1" Text='<%# Bind("f_name") %>' NavigateUrl='<%#"~/permit/view_doc.aspx?ID=" + eval("doctype") + "_" + eval("fsubject") + "_" + eval("kdcom") + "_" + eval("kdsite") + "_" + eval("fyear") + "_" + eval("f_name") %>' Target="_blank" runat="server"></asp:HyperLink>--%>
                    <asp:HyperLink ID="HyperLink1" Text='<%# Bind("f_name") %>' NavigateUrl='<%# eval("f_path") %>' Target="_blank" runat="server"></asp:HyperLink>
                </ItemTemplate>

<HeaderStyle Width="300px"></HeaderStyle>
            </asp:TemplateField> 
            </Columns>
            <SelectedRowStyle Font-Bold="True" />
            <PagerStyle HorizontalAlign="Left" />
            <HeaderStyle Font-Bold="True" />
            <RowStyle Font-Bold="True" />
        </asp:GridView>
    </div> 
</asp:Content>