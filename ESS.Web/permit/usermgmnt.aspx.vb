﻿Imports System.Data.SqlClient
Imports System.Net
Imports System.IO

Partial Public Class usermgmnt
    Inherits System.Web.UI.Page
    Public msgbox As String
    Public mnentry As String
    Public mnview As String
    Public mnmgmt As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'mnentry = "<div id='mnentry'><img alt='open document' src='../images/open_doc_small.png' /> <a href='entry_permit_doc.aspx'>&nbsp;Upload Document</a></div>"
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)

        If Session("otorisasi_permit") = Nothing Or Session("otorisasi_permit") = "" Then
            Response.Redirect("signin_permit.aspx")
        End If

        If Session("docedituser") <> "" Then
            Dim strdocuser As String = "select * from ms_user where emp_code = '" + Session("docedituser") + "'"

            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strdocuser, sqlConn)

            sda.Fill(dtb)

            If dtb.Rows.Count >= 0 Then
                DropDownList1.SelectedValue = Trim(dtb.Rows(0)!emp_code.ToString)
                ddllvl.SelectedValue = Trim(dtb.Rows(0)!lvl.ToString)
                ddlmodul.SelectedValue = Trim(dtb.Rows(0)!modul.ToString)
            End If
        End If

        If Trim(Session("permission_permit")) = "1" Or Trim(Session("permission_permit")) = "2" Or Trim(Session("permission_permit")) = "4" Then
            mnentry = "<div id='mnentry'><img alt='entry document' src='../images/open_doc_small.png' /> <a href='doc_center_entry.aspx'>&nbsp;Set Document</a></div>"
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View List</a></div>"
        End If

        If Trim(Session("permission_permit")) = "1" Then
            mnmgmt = "<div id='mnmgmt'> <img alt='Management User' src='../images/open_doc_small.png' /> <a href='mgmnt_list.aspx'>&nbsp;Management User</a></div>"
        End If

        If Trim(Session("permission_permit")) = "3" Or Trim(Session("permission_permit")) = "4" Then
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View List</a></div>"
        End If


        Try
            sqlConn.Open()
            Dim strcon2 As String = "Select * from ms_user where stedit <> '2' and emp_code = '" + Session("otorisasi_permit") + "'"
            Dim dtb2 As DataTable = New DataTable
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn)

            sda2.Fill(dtb2)

            Dim strcon As String = "select emp_code, name, modul, (case lvl when '1' then 'Owner' when '2' then 'Doc Admin' when '3' then 'User' when '4' then 'PIC' end) as lvl, case schmail when '01' then 'Daily' when '02' then 'Weekly' when '03' then 'Monthly' end as schmail from ms_user where stedit <> '2' and modul = '" + dtb2.Rows(0)!modul.ToString + "' order by modul"

            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)

            sda.Fill(dtb)
            GridView1.DataSource = dtb
            GridView1.DataBind()

        Catch ex As Exception

        Finally
            sqlConn.Close()
        End Try

    End Sub

    Dim dtbfindemp As DataTable = New DataTable
    Protected Sub Save_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Save.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim cmd As SqlCommand
        If Session("docedituser") <> "" Or Session("docedituser") <> Nothing Then

            Try
                conn.Open()
                Dim sqlQuery As String = "UPDATE ms_user SET emp_code = '" + DropDownList1.SelectedValue + "', employee_nik = '" + DropDownList1.SelectedValue + "', name = '" + DropDownList1.SelectedItem.ToString + "', lvl = '" + ddllvl.SelectedValue + "', modul = '" + ddlmodul.SelectedValue + "' where emp_code = '" + DropDownList1.SelectedValue + "'"
                cmd = New SqlCommand(sqlQuery, conn)
                cmd.ExecuteScalar()
                msgbox = "<script language='javascript' type='text/javascript'> alert('data has been stored') </script>"
                Session("docedituser") = ""
            Catch ex As Exception

            Finally
                conn.Close()
            End Try

        Else
            Try
                conn.Open()
                Dim sqlfindemp As String = "select emp_code from ms_user where stedit <> '2' and emp_code = '" + DropDownList1.SelectedValue + "'"
                Dim sdafindemp As SqlDataAdapter = New SqlDataAdapter(sqlfindemp, conn)
                sdafindemp.Fill(dtbfindemp)

                If dtbfindemp.Rows.Count > 0 Then
                    msgbox = "<script language='javascript' type='text/javascript'> alert('user already exist in the database') </script>"
                    Exit Sub
                Else
                    Dim sqlQuery As String = "INSERT INTO ms_user (emp_code, employee_nik, name, lvl, stamp, usr, modul) VALUES ('" + DropDownList1.SelectedValue + "', '" + DropDownList1.SelectedValue + "', '" + DropDownList1.SelectedItem.ToString + "', '" + ddllvl.SelectedValue + "', '" + DateTime.Now.ToString + "', '" + Session("otorisasi_permit").ToString + "', '" + ddlmodul.SelectedValue + "')"
                    cmd = New SqlCommand(sqlQuery, conn)
                    cmd.ExecuteScalar()
                    msgbox = "<script language='javascript' type='text/javascript'> alert('data has been stored') </script>"
                End If

            Catch ex As Exception

            Finally
                conn.Close()
            End Try
        End If

    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
    
        GridView1.PageIndex = e.NewPageIndex
        GridView1.DataBind()
      
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        
        'lblinfo.Text = "You dont have access to modify document information"
        'msginfo = "<script language='javascript' type='text/javascript'> alert('You dont have access to modify document information') </script>"

        If GridView1.SelectedRow Is Nothing Then Exit Sub

        Dim row As GridViewRow = GridView1.SelectedRow
        Session("docedituser") = GridView1.DataKeys(row.RowIndex).Value.ToString
        'Response.Redirect("view_trip.aspx?ID=" & GridView1.DataKeys(row.RowIndex).Value.ToString)
        Response.Redirect("usermgmnt.aspx")

    End Sub

    Protected Sub ddlmodul_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlmodul.SelectedIndexChanged
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Try
            Dim strcon As String = "select emp_code, name, modul, (case lvl when '1' then 'Owner' when '2' then 'Doc Admin' when '3' then 'User' when '4' then 'PIC' end) as lvl from ms_user where stedit <> '2' and modul = '" + ddlmodul.SelectedValue + "'"

            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)

            sda.Fill(dtb)
            GridView1.DataSource = dtb
            GridView1.DataBind()

        Catch ex As Exception

        End Try
    End Sub
End Class