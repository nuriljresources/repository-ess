﻿

Partial Class site
    Inherits System.Web.UI.MasterPage
    Public sessionNIK As String

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim cs As ClientScriptManager = Page.ClientScript
            cs.RegisterHiddenField("UserId", "tes")

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Function MAP(ByVal s As String) As String
        Select Case s
            Case "1"
                Return "MSD"
            Case "2"
                Return "MSD Officer"
            Case "3"
                Return "BOD"
            Case "4"
                Return "Func. Head"
            Case "5"
                Return "Dept. Head"
            Case "6"
                Return "PM/DPM"
            Case "7"
                Return "S. Head"
            Case "8"
                'Return "PaDCA"
                Return "Note Taker"
            Case "A"
                Return "SH Admin"
            Case "B"
                Return "MSD Func. Head"
        End Select
        Return "Others"
    End Function
End Class

