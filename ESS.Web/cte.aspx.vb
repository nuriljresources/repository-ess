﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Security.Cryptography
Imports System.IO
Partial Public Class cte
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ec As String
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim empConn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
        Dim dept As String
        Dim lsnik
        
        Try
            If Request.QueryString("EC") <> Nothing Then
                'If Request.QueryString("EC") = Nothing Then

                ec = Replace(Request.QueryString("EC"), " ", "+")

                If ec = "1" Then
                    ec = "r/yf/M4jSDNj+YaWEjqUZA=="
                ElseIf ec = "2" Then
                    ec = "q19zspUWFcg95gjVoNwKMg=="
                ElseIf ec = "3" Then
                    ec = "xjzrFdMwOiwlmL9jwnJVBg=="
                End If




                lsnik = Decrypt(ec)
                'lsnik = ec
                'lsnik = ""
                Dim dtb As DataTable = New DataTable()
                Dim strcon As String = "select kduser,nmuser,(select kdsite from H_A101 where niksite = B_C012.kduser) as kdsite,kddivisi,(select kddepar from H_A101 where niksite = B_C012.kduser) as kddepar, (select kdlevel from H_A101 where niksite = B_C012.kduser) as kdlevel from B_C012 where kduser = '" + Trim(Replace(lsnik, "'", "")) + "' and stedit in ('0','1')"

                Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, empConn)
                sda.Fill(dtb)
                If dtb.Rows.Count > 0 Then
                    If Session("otorisasi") = "" Then
                        Session("otorisasi") = dtb.Rows(0)("kduser").ToString
                        Session("kdsite") = dtb.Rows(0)("kdsite").ToString
                        Session("permission") = "1;B"
                        Session("NmUser") = dtb.Rows(0)("nmuser").ToString
                        Session("medicnoregst") = ""
                        Session("medicnoreg") = ""
                        Session("medicnoregstout") = ""
                        Session("medicnoregout") = ""
                        Session("framenoreg") = ""
                        Session("framenoregst") = ""
                        Session("kddepar") = dtb.Rows(0)("kddepar").ToString
                        Session("kdlevel") = dtb.Rows(0)("kdlevel").ToString
                    End If

                    Dim dtb2 As DataTable = New DataTable()
                    Dim strcon2 As String = "select nik from H_A101 where niksite='" + Session("otorisasi") + "'"
                    Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, empConn)
                    sda2.Fill(dtb2)

                    Session("niksite") = dtb2.Rows(0)("nik").ToString
                    If Request.QueryString("page") <> Nothing Then
                        If Request.QueryString("page") = "app1ot" Then
                            Response.Redirect("spl_approval.aspx")
                        Else
                            Response.Redirect("spl_approval2.aspx")
                        End If
                    Else
                        Response.Redirect("home.aspx")
                    End If

                Else
                    Response.Redirect("http://portal.jresources.com/sitepages/err.aspx?nik=" + lsnik + "&p=notfound")
                End If
            Else
                Response.Redirect("http://portal.jresources.com/sitepages/err.aspx?nik=" + lsnik + "&p=nothing")
            End If
        Catch ex As Exception
            'Response.Redirect("http://moss.jresources.com/sitepages/home4.aspx")
        End Try
    End Sub

    Private Function Decrypt(ByVal input As String) As String
        Return Encoding.UTF8.GetString(Decrypt(Convert.FromBase64String(input)))
    End Function
    Private Function Decrypt(ByVal input As Byte()) As Byte()

        Dim pdb As New PasswordDeriveBytes("hjiweykaksd", New Byte() {&H43, &H87, &H23, &H72, &H45, &H56, _
         &H68, &H14, &H62, &H84})
        Dim ms As New MemoryStream()
        Dim aes As Aes = New AesManaged()
        aes.Key = pdb.GetBytes(aes.KeySize / 8)
        aes.IV = pdb.GetBytes(aes.BlockSize / 8)
        Dim cs As New CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write)
        cs.Write(input, 0, input.Length)
        cs.Close()
        Return ms.ToArray()
    End Function
End Class