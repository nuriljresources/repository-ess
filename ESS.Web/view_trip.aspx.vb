﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.DirectoryServices

Partial Public Class view_trip
    Inherits System.Web.UI.Page
    Public tripno As String
    Public idw As String
    Public nik As String
    Public snik As String
    Public dtrip As String
    Public purpose As String
    Public tfromdt As String
    Public ttodt As String
    Public tday As String
    Public nama As String
    Public snama As String
    Public nmjabat As String
    Public nmdepar As String
    Public datefrom As String
    Public dateto As String
    Public tfrom As String
    Public tto As String
    Public ccode As String
    Public redate As String
    Public consul As String
    Public consultype As String
    Public strtbl As String
    Public fstatus As Integer
    Public editga As Integer
    Public disabled As String
    Public txtareadtl As String
    Public txtareapurp As String
    Public jml As String
    Public kdsite As String
    Public strtbl2 As String
    Public jml2 As String
    Public jvc As String
    Public jvc2 As String
    Public cashadvance As String
    Public currcode As String
    Public txtcurrcode As String
    Public txtcashadvance As String
    Public bsave As String
    Public niksite As String
    Public sup_niksite As String
    Public genid As String
    Public genid2 As String
    Public srcspv As String
    Public cash As String
    Public detsts As String
    Public detsts2 As String
    Public bprint As String
    Public cscd As String
    Public spoh As String
    Public sphone As String
    Public alert As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim seltmpref As String
        Dim seltytrns As String
        Dim seltyclss As String
        Dim strenable As String
        'idw = Request.QueryString("ID")
        idw = Session("vtrip")
        bsave = "<input type='button' id='save' value='save' onclick='save2()' disabled='disabled' style='width: 80px; font-family: Calibri;' />"
        delete.Enabled = False

        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()

            Dim strcon As String = "select tripno, nik,(select niksite from H_A101 where nik = V_H001.nik) as niksite,(select nama from H_A101 where nik = V_H001.nik) as nama, costcode,reqdate, "
            strcon = strcon + "(select NmDepar from H_A130 where kddepar = V_H001.kddepar) as nmdepar, sup_nik, (select niksite from H_A101 where nik = V_H001.sup_nik) as sup_niksite,"
            strcon = strcon + "(select nmjabat from H_A150 where kdjabat = V_H001.kdjabat ) as nmjabat, "
            strcon = strcon + "(select nama from H_A101 where nik = V_H001.sup_nik ) as superior, (select costcoded from H_A140 where KdDivisi = (select kddivisi from H_A130 where KdDepar = (select kddepar from H_A101 where nik = V_H001.nik))) as costcodelm, "
            'strcon = strcon + " (select datefrom from V_H00104 where Tripno = V_H001.TripNO) As datefrom, "
            'strcon = strcon + "(select DateTo from V_H00104 where Tripno = V_H001.TripNO) dateto, "
            strcon = strcon + "detailtrip, purpose, tripfrom, tripto, tripday, apprby, apprdate, consul, consultype, editga, fstatus, kdsite,cashadvan,currcode, dectotal, (select NmPOH from h_a190 where KdPOH = (select PointHire from h_a101 where nik = v_h001.nik)) as nmpoh, (select NoHP from h_a101 where nik = v_h001.nik) as NoHP, ISNULL(StatusWF, '') AS StatusWF from V_H001 where tripno ='" + idw + "' and stedit <> 2"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            If dtb.Rows().Count > 0 Then
                tripno = dtb.Rows(0)!tripno.ToString
                nik = dtb.Rows(0)!nik.ToString
                niksite = dtb.Rows(0)!niksite.ToString
                sup_niksite = dtb.Rows(0)!sup_niksite.ToString
                snik = dtb.Rows(0)!sup_nik.ToString
                dtrip = dtb.Rows(0)!detailtrip.ToString
                purpose = dtb.Rows(0)!purpose.ToString
                'tfromdt = dtb.Rows(0)!tripfrom.ToString
                'ttodt = dtb.Rows(0)!tripto.ToString
                tday = dtb.Rows(0)!tripday.ToString
                nama = dtb.Rows(0)!nama.ToString
                snama = dtb.Rows(0)!sup_nik.ToString
                nmjabat = dtb.Rows(0)!nmjabat.ToString
                nmdepar = dtb.Rows(0)!nmdepar.ToString
                'datefrom = dtb.Rows(0)!datefrom.ToString
                'dateto = dtb.Rows(0)!dateto.ToString
                tripfrom.Text = dtb.Rows(0)!tripfrom.ToString
                fstatus = dtb.Rows(0)!fstatus
                editga = dtb.Rows(0)!editga
                kdsite = dtb.Rows(0)!kdsite
                cscd = dtb.Rows(0)!costcodelm.ToString

                If tripfrom.Text <> "" Or tripfrom.Text <> "null" Then
                    tripfrom.Text = CDate(tripfrom.Text).ToString("MM/dd/yyyy")
                End If

                tripto.Text = dtb.Rows(0)!tripto.ToString

                If tripto.Text <> "" Or tripto.Text <> "null" Then
                    tripto.Text = CDate(tripto.Text).ToString("MM/dd/yyyy")
                End If

                tday = dtb.Rows(0)!tripday.ToString
                ccode = dtb.Rows(0)!costcode.ToString
                reqdate.Text = dtb.Rows(0)!reqdate.ToString

                If reqdate.Text <> "" Or reqdate.Text <> "null" Then
                    reqdate.Text = CDate(reqdate.Text).ToString("MM/dd/yyyy")
                End If

                consul = dtb.Rows(0)!consul.ToString
                consultype = dtb.Rows(0)!consultype.ToString
                cashadvance = dtb.Rows(0)!cashadvan.ToString
                currcode = dtb.Rows(0)!currcode.ToString

                txtcashadvance = "<input type='text' class='entri' id='cashadvan' disabled='disabled' value='" + cashadvance + "' style='visibility:hidden'/>"

                If currcode = "IDR" Then
                    txtcurrcode = "<select id='curcode' disabled='disabled' style='visibility:hidden'><option value='IDR' selected='selected'>IDR</option><option value='USD'>USD</option></select>"
                ElseIf currcode = "USD" Then
                    txtcurrcode = "<select id='curcode' disabled='disabled' style='visibility:hidden'><option value='IDR'>IDR</option><option value='USD' selected='selected'>USD</option></select>"
                Else
                    txtcurrcode = "<select id='curcode' disabled='disabled' style='visibility:hidden'><option value=' '> </option></select>"
                End If

                If dtrip = "01" Then
                    txtareadtl = "<select id='detailtrip' style='width:95px;' onchange='ddldtltrip()' disabled='disabled'><option value='01' selected='selected'>Business Trip</option> <option value='02'>Annual Leave</option> <option value='03'>Periodic Leave</option> <option value='04'>Recruitment</option> <option value='05'>Training</option> <option value='06'>Visitor</option> </select>"
                ElseIf dtrip = "02" Then
                    txtareadtl = "<select id='detailtrip' style='width:95px;' onchange='ddldtltrip()' disabled='disabled'><option value='01'>Business Trip</option> <option value='02' selected='selected'>Annual Leave</option> <option value='03'>Periodic Leave</option> <option value='04'>Recruitment</option> <option value='05'>Training</option> <option value='06'>Visitor</option> </select>"
                ElseIf dtrip = "03" Then
                    txtareadtl = "<select id='detailtrip' style='width:95px;' onchange='ddldtltrip()' disabled='disabled'><option value='01'>Business Trip</option> <option value='02'>Annual Leave</option> <option value='03' selected='selected'>Periodic Leave</option> <option value='04'>Recruitment</option> <option value='05'>Training</option> <option value='06'>Visitor</option> </select>"
                ElseIf dtrip = "04" Then
                    txtareadtl = "<select id='detailtrip' style='width:95px;' onchange='ddldtltrip()' disabled='disabled'> <option value='01'>Business Trip</option> <option value='02'>Annual Leave</option> <option value='03'>Periodic Leave</option> <option value='04' selected='selected'>Recruitment</option> <option value='05'>Training</option> <option value='06'>Visitor</option> </select>"
                ElseIf dtrip = "05" Then
                    txtareadtl = "<select id='detailtrip' style='width:95px;' onchange='ddldtltrip()' disabled='disabled'> <option value='01'>Business Trip</option> <option value='02'>Annual Leave</option> <option value='03'>Periodic Leave</option> <option value='04'>Recruitment</option> <option value='05'  selected='selected'>Training</option> <option value='06'>Visitor</option> </select>"
                ElseIf dtrip = "06" Then
                    txtareadtl = "<select id='detailtrip' style='width:95px;' onchange='ddldtltrip()' disabled='disabled'> <option value='01'>Business Trip</option> <option value='02'>Annual Leave</option> <option value='03'>Periodic Leave</option> <option value='04'>Recruitment</option> <option value='05'>Training</option> <option value='06' selected='selected'>Visitor</option> </select>"
                End If

                'txtareadtl = "<textarea id='detailtrip' disabled= 'disabled' style='width:107%;Height:100px' class='entri' cols='5' rows='5'>" + dtrip + "</textarea>"
                txtareapurp = "<textarea id='purpose' style='width:107%;Height:100px' disabled='disabled' class='entri' cols='5' rows='5'>" + purpose + "</textarea>"

                If dtb.Rows(0)!dectotal = "1" Then
                    cash = "<input id='cash' type='checkbox' checked='checked' />"
                Else
                    cash = "<input id='cash' type='checkbox' />"
                End If

                If dtb.Rows(0)!editGA = "1" Then
                    bprint = "<input type='button' disabled='disabled' value='Print' onclick='frmcetak()' style='width: 80px; font-family: Calibri;' /> "
                Else
                    bprint = "<input type='button' value='Print' onclick='frmcetak()' style='width: 80px; font-family: Calibri;' /> "
                End If

                spoh = dtb.Rows(0)!nmpoh.ToString
                sphone = dtb.Rows(0)!NoHP.ToString

                If dtb.Rows(0)!StatusWF = "INP" Or dtb.Rows(0)!StatusWF = "COMPLT" Then
                    edit.Enabled = False
                End If

                If Session("kdsite") = "JKT" Then
                    If dtb.Rows(0)!StatusWF = "" Or dtb.Rows(0)!StatusWF = "RVS" Then
                        btnSubmit.Enabled = True
                    End If
                End If

            Else
                Response.Redirect("_blank.aspx")
            End If

        Catch ex As Exception
            'MsgBox(e.ToString)
        End Try

        Try
            Dim sqlConn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

            sqlConn2.Open()
            Dim i As Integer
            Dim dtb2 As DataTable = New DataTable()

            Dim strcon2 As String = "select * from V_H00102 where Tripno = '" + idw + "' order by GenID asc"
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn2)
            sda2.Fill(dtb2)

            If dtb2.Rows().Count > 0 Then
                For i = 0 To dtb2.Rows.Count - 1
                    If dtb2.Rows(i)!timeprefer = "01" Then
                        seltmpref = "<option value='01' selected='selected'>Morning</option> <option value='02'>Noon</option> <option value='03'>Afternoon</option> <option value='04'>Night</option>"
                    ElseIf dtb2.Rows(i)!timeprefer = "02" Then
                        seltmpref = "<option value='01'>Morning</option> <option value='02' selected='selected'>Noon</option> <option value='03'>Afternoon</option> <option value='04'>Night</option>"
                    ElseIf dtb2.Rows(i)!timeprefer = "03" Then
                        seltmpref = "<option value='01'>Morning</option> <option value='02'>Noon</option> <option value='03' selected='selected'>Afternoon</option> <option value='04'>Night</option>"
                    ElseIf dtb2.Rows(i)!timeprefer = "04" Then
                        seltmpref = "<option value='01'>Morning</option> <option value='02'>Noon</option> <option value='03'>Afternoon</option> <option value='04'  selected='selected'>Night</option>"
                    Else
                        seltmpref = "<option>01</option>"
                    End If

                    If dtb2.Rows(i)!typetrans = "01" Then
                        seltytrns = "<option value='01' selected='selected'>Air</option> <option value='02'>Land</option> <option value='03'>Sea</option>"
                        strenable = "disabled='disabled'"
                    ElseIf dtb2.Rows(i)!typetrans = "02" Then
                        seltytrns = "<option value='01'>Air</option> <option value='02' selected='selected'>Land</option> <option value='03'>Sea</option>"
                    ElseIf dtb2.Rows(i)!typetrans = "03" Then
                        seltytrns = "<option value='01'>Air</option> <option value='02'>Land</option> <option value='03' selected='selected'>Sea</option>"
                        strenable = " "
                    Else
                        seltytrns = "<option>01</option>"
                        strenable = " "
                    End If

                    If dtb2.Rows(i)!TypeClass.ToString = "01" Then
                        seltyclss = "<option value='01' selected='selected'>Economy</option> <option value='02'>Business</option><option value='00'></option>"
                    ElseIf dtb2.Rows(i)!TypeClass.ToString = "02" Then
                        seltyclss = "<option value='01'>Economy</option> <option value='02' selected='selected'>Business</option><option value='00'></option>"
                    Else
                        seltyclss = "<option value='01'>Economy</option> <option value='02'>Business</option><option value='00' selected='selected'></option>"
                    End If

                    strtbl = strtbl + "<div id='input" + (i + 1).ToString + "' style='margin-bottom:4px; width:100%;' class='clonedInput'>"
                    strtbl = strtbl + " <input type='text' name='no" + (i + 1).ToString + "' id='no" + i.ToString + "' value='" + (i + 1).ToString + "'  style='width:2%;' disabled='disabled'/>"
                    strtbl = strtbl + " <input type='text' name='depfrom" + (i + 1).ToString + "' id='depfrom" + (i + 1).ToString + "' value='" + dtb2.Rows(i)!dep_from + "' style='width:12%;' disabled='disabled'/>"
                    strtbl = strtbl + " <input type='text' name='depdate" + (i + 1).ToString + "' id='depdate" + (i + 1).ToString + "' value='" + CDate(dtb2.Rows(i)!dep_date).ToString("MM/dd/yyyy") + "' style='width:11%;' disabled='disabled'/><button id='bdep" + (i + 1).ToString + "' disabled='disabled'>...</button>"
                    strtbl = strtbl + " <input type='text' name='arriveto" + (i + 1).ToString + "' id='arriveto" + (i + 1).ToString + "' value='" + dtb2.Rows(i)!arrive_to + "' style='width:11%;' disabled='disabled'/>"
                    strtbl = strtbl + " <input type='text' name='arrivedate" + (i + 1).ToString + "' id='arrivedate" + (i + 1).ToString + "' value='" + CDate(dtb2.Rows(i)!arrive_date).ToString("MM/dd/yyy") + "' style='width:11%;' disabled='disabled'/><button id='barrive" + (i + 1).ToString + "' disabled='disabled'>...</button>"
                    strtbl = strtbl + " <input type='text' name='remark" + (i + 1).ToString + "' id='remark" + (i + 1).ToString + "' style='width:30%;' value='" + dtb2.Rows(i)!remark + "' disabled='disabled' />"
                    strtbl = strtbl + " <select name='timeprefer" + (i + 1).ToString + "' id='timeprefer" + (i + 1).ToString + "' style='width:6%;' disabled='disabled'>" + seltmpref + "</select>"
                    strtbl = strtbl + " <select name='typetrans" + (i + 1).ToString + "' id='typetrans" + (i + 1).ToString + "' onchange= 'chktype()' style='width:4%;' disabled='disabled'>" + seltytrns + "</select>"
                    strtbl = strtbl + " <select name='class" + (i + 1).ToString + "' id='class" + (i + 1).ToString + "' style='width:4%;'" + strenable + ">" + seltyclss + "</select>"
                    strtbl = strtbl + " </div>"

                    strtbl = strtbl + "<div>"
                    strtbl = strtbl + "<input type='hidden' name='genid" + (i + 1).ToString + "' id='genid" + (i + 1).ToString + "' value='" + dtb2.Rows(i)!genid + "' />"
                    strtbl = strtbl + "</div>"

                    jvc2 = jvc2 + "cal.manageFields('bdep" + (i + 1).ToString + "', 'depdate" + (i + 1).ToString + "', '%m/%d/%Y');"
                    jvc2 = jvc2 + "cal.manageFields('barrive" + (i + 1).ToString + "', 'arrivedate" + (i + 1).ToString + "', '%m/%d/%Y');"
                Next
                'jvc2 = "<script type ='text/javascript'> var cal = Calendar.setup({ onSelect: function(cal) { cal.hide() }, showTime: false });" + jvc2 + " </script>"
            Else
                If dtb2.Rows.Count = 0 Then
                    seltmpref = "<option value='01' selected='selected'>Morning</option> <option value='02'>Noon</option> <option value='03'>Afternoon</option> <option value='04'>Night</option>"
                    seltytrns = "<option value='01' selected='selected'>Air</option> <option value='02'>Land</option> <option value='03'>Sea</option>"
                    strenable = "disabled='disabled'"
                    seltyclss = "<option value='01' selected='selected'>Economy</option> <option value='02'>Business</option><option value='00'></option>"

                    strtbl = strtbl + "<div id='input1' style='margin-bottom:4px; width:100%;' class='clonedInput'>"
                    strtbl = strtbl + " <input type='text' name='no1' id='no1' value='1'  style='width:2%;' disabled='disabled'/>"
                    strtbl = strtbl + " <input type='text' name='depfrom1' id='depfrom1' value='' style='width:12%;' disabled='disabled'/>"
                    strtbl = strtbl + " <input type='text' name='depdate1' id='depdate1' value='' style='width:11%;' disabled='disabled'/><button id='bdep1' disabled='disabled'>...</button>"
                    strtbl = strtbl + " <input type='text' name='arriveto1' id='arriveto1' value='' style='width:11%;' disabled='disabled'/>"
                    strtbl = strtbl + " <input type='text' name='arrivedate1' id='arrivedate1' value='' style='width:11%;' disabled='disabled'/><button id='barrive1' disabled='disabled'>...</button>"
                    strtbl = strtbl + " <input type='text' name='remark1' id='remark1' style='width:30%;' value='' disabled='disabled' />"
                    strtbl = strtbl + " <select name='timeprefer1' id='timeprefer1' style='width:6%;' disabled='disabled'>" + seltmpref + "</select>"
                    strtbl = strtbl + " <select name='typetrans1' id='typetrans1' onchange= 'chktype()' style='width:4%;' disabled='disabled'>" + seltytrns + "</select>"
                    strtbl = strtbl + " <select name='class1' id='class1' style='width:4%;'" + strenable + ">" + seltyclss + "</select>"
                    strtbl = strtbl + " </div>"

                    strtbl = strtbl + "<div>"
                    strtbl = strtbl + "<input type='hidden' name='genid1' id='genid1' value='' />"
                    strtbl = strtbl + "</div>"

                    jvc2 = jvc2 + "cal.manageFields('bdep1', 'depdate1', '%m/%d/%Y');"
                    jvc2 = jvc2 + "cal.manageFields('barrive1', 'arrivedate1', '%m/%d/%Y');"
                End If
            End If
            sqlConn2.Close()
        Catch ex As Exception

        End Try


        Try
            Dim sqlConn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

            sqlConn2.Open()
            Dim j As Integer
            Dim dtb3 As DataTable = New DataTable()

            Dim strcon2 As String = "select * from V_H00106 where Tripno = '" + idw + "' order by GenID asc"
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn2)
            sda2.Fill(dtb3)
            If dtb3.Rows().Count > 0 Then
                For j = 0 To dtb3.Rows.Count - 1
                    strtbl2 = strtbl2 + "<div id='akoinput" + (j + 1).ToString + "' style='margin-bottom:0px; width:100%' class='akoinput' disabled='disabled'>"
                    strtbl2 = strtbl2 + " <input type='text' name='n" + (j + 1).ToString + "' id='n" + (j + 1).ToString + "' value='" + (j + 1).ToString + "' style='width:2%;' disabled='disabled'/>"
                    strtbl2 = strtbl2 + " <input type='text' name='location" + (j + 1).ToString + "' id='location" + (j + 1).ToString + "' value='" + dtb3.Rows(j)!location + "' style='width:10%;' disabled='disabled'/>"
                    strtbl2 = strtbl2 + " <input type='text' name='datein" + (j + 1).ToString + "' id='datein" + (j + 1).ToString + "' value='" + CDate(dtb3.Rows(j)!datein).ToString("MM/dd/yyy") + "' style='width:10%;' disabled='disabled'/><button id='bin" + (j + 1).ToString + "' disabled='disabled'>...</button>"
                    strtbl2 = strtbl2 + " <input type='text' name='dateout" + (j + 1).ToString + "' id='dateout" + (j + 1).ToString + "' value='" + CDate(dtb3.Rows(j)!dateout).ToString("MM/dd/yyy") + "' style='width:10%;' disabled='disabled'/><button id='bout" + (j + 1).ToString + "' disabled='disabled'>...</button>"
                    strtbl2 = strtbl2 + " <input type='text' name='rem" + (j + 1).ToString + "' id='rem" + (j + 1).ToString + "' value='" + dtb3.Rows(j)!remark + "' style='width:27%;' disabled='disabled'/>"
                    strtbl2 = strtbl2 + " </div>"

                    strtbl2 = strtbl2 + "<div>"
                    strtbl2 = strtbl2 + "<input type='hidden' name='genid2" + (j + 1).ToString + "' id='genid2" + (j + 1).ToString + "' value='" + dtb3.Rows(j)!genid + "' />"
                    strtbl2 = strtbl2 + "</div>"

                    jvc = jvc + "cal.manageFields('bin" + (j + 1).ToString + "', 'datein" + (j + 1).ToString + "', '%m/%d/%Y');"
                    jvc = jvc + "cal.manageFields('bout" + (j + 1).ToString + "', 'dateout" + (j + 1).ToString + "', '%m/%d/%Y');"
                    
                Next
                jvc = "<script type ='text/javascript'> var cal = Calendar.setup({ onSelect: function(cal) { cal.hide() }, showTime: false });" + jvc + jvc2 + " </script>"
            Else
                If dtb3.Rows().Count = 0 Then
                    strtbl2 = strtbl2 + "<div id='akoinput1' style='margin-bottom:0px; width:100%' class='akoinput' disabled='disabled'>"
                    strtbl2 = strtbl2 + " <input type='text' name='n1' id='n1' value='1' style='width:2%;' disabled='disabled'/>"
                    strtbl2 = strtbl2 + " <input type='text' name='location1' id='location1' value='' style='width:10%;' disabled='disabled'/>"
                    strtbl2 = strtbl2 + " <input type='text' name='datein1' id='datein1' value='' style='width:10%;' disabled='disabled'/><button id='bin1' disabled='disabled'>...</button>"
                    strtbl2 = strtbl2 + " <input type='text' name='dateout1' id='dateout1' value='' style='width:10%;' disabled='disabled'/><button id='bout1' disabled='disabled'>...</button>"
                    strtbl2 = strtbl2 + " <input type='text' name='rem1' id='rem1' value='' style='width:27%;' disabled='disabled'/>"
                    strtbl2 = strtbl2 + " </div>"

                    strtbl2 = strtbl2 + "<div>"
                    strtbl2 = strtbl2 + "<input type='hidden' name='genid21' id='genid21' value='' />"
                    strtbl2 = strtbl2 + "</div>"

                    jvc = jvc + "cal.manageFields('bin1', 'datein1', '%m/%d/%Y');"
                    jvc = jvc + "cal.manageFields('bout1', 'dateout1', '%m/%d/%Y');"
                    jvc = "<script type ='text/javascript'> var cal = Calendar.setup({ onSelect: function(cal) { cal.hide() }, showTime: false });" + jvc + jvc2 + " </script>"
                End If
            End If
        Catch ex As Exception

        End Try

        srcspv = ""
    End Sub

    Protected Sub delete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles delete.Click
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        'If GridView1.Rows.Item Is Nothing Then Exit Sub


        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()
            Dim cmd As SqlCommand
            Dim strcon As String = "update V_H001 set stedit = '2' where TripNo = '" + idw + "'"
            cmd = New SqlCommand(strcon, sqlConn)
            cmd.ExecuteScalar()
            sqlConn.Close()
            Response.Redirect("trip_list.aspx")
        Catch ex As Exception

        End Try
    End Sub

    Public Sub edit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles edit.Click
        Dim seltmpref As String
        Dim seltytrns As String
        Dim seltyclss As String
        Dim strenable As String

        If fstatus = 0 Then 'editga = 0 And //perubahan req DESSY 12 nov 2013
            delete.Enabled = True
            bsave = "<input type='button' id='save' value='save' onclick='save2()' style='width: 80px; font-family: Calibri;' />"
            txtcashadvance = "<input type='text' class='entri' id='cashadvan' onkeypress='return isNumberKey(event)' value='" + cashadvance + "' style='visibility:hidden' />"

            If currcode = "IDR" Then
                txtcurrcode = "<select id='curcode' style='visibility:hidden'><option value='IDR' selected='selected'>IDR</option><option value='USD'>USD</option></select>"
            ElseIf currcode = "USD" Then
                txtcurrcode = "<select id='curcode' style='visibility:hidden'><option value='IDR'>IDR</option><option value='USD' selected='selected'>USD</option></select>"
            Else
                txtcurrcode = "<select id='curcode' style='visibility:hidden'><option value=' '> </option></select>"
            End If

            'txtareadtl = "<textarea id='detailtrip' style='width:107%;Height:100px' class='entri' cols='5' rows='5'>" + dtrip + "</textarea>"
            'If dtrip = "01" Then
            '    txtareadtl = "<select id='detailtrip' onchange='ddldtltrip()' style='width:95px;'><option value='01' selected='selected'>Business Trip</option> <option value='02'>Annual Leave</option> <option value='03'>Periodic Leave</option> <option value='04'>Recruitment</option> <option value='05'>Training</option> <option value='06'>Visitor</option> </select>"
            'ElseIf dtrip = "02" Then
            '    txtareadtl = "<select id='detailtrip' onchange='ddldtltrip()' style='width:95px;'><option value='01'>Business Trip</option> <option value='02' selected='selected'>Annual Leave</option> <option value='03'>Periodic Leave</option> <option value='04'>Recruitment</option> <option value='05'>Training</option> <option value='06'>Visitor</option> </select>"
            'ElseIf dtrip = "03" Then
            '    txtareadtl = "<select id='detailtrip' onchange='ddldtltrip()' style='width:95px;'><option value='01'>Business Trip</option> <option value='02'>Annual Leave</option> <option value='03' selected='selected'>Periodic Leave</option> <option value='04'>Recruitment</option> <option value='05'>Training</option> <option value='06'>Visitor</option> </select>"
            'ElseIf dtrip = "04" Then
            '    txtareadtl = "<select id='detailtrip' onchange='ddldtltrip()' style='width:95px;'> <option value='01'>Business Trip</option> <option value='02'>Annual Leave</option> <option value='03'>Periodic Leave</option> <option value='04' selected='selected'>Recruitment</option> <option value='05'>Training</option> <option value='06'>Visitor</option> </select>"
            'ElseIf dtrip = "05" Then
            '    txtareadtl = "<select id='detailtrip' onchange='ddldtltrip()' style='width:95px;'> <option value='01'>Business Trip</option> <option value='02'>Annual Leave</option> <option value='03'>Periodic Leave</option> <option value='04'>Recruitment</option> <option value='05'  selected='selected'>Training</option> <option value='06'>Visitor</option> </select>"
            'ElseIf dtrip = "06" Then
            '    txtareadtl = "<select id='detailtrip' onchange='ddldtltrip()' style='width:95px;'> <option value='01'>Business Trip</option> <option value='02'>Annual Leave</option> <option value='03'>Periodic Leave</option> <option value='04'>Recruitment</option> <option value='05'>Training</option> <option value='06' selected='selected'>Visitor</option> </select>"
            'End If

            If dtrip = "01" Then
                txtareadtl = "<select id='detailtrip' style='width:95px;'><option value='01' selected='selected'>Business Trip</option> <option value='02'>Annual Leave</option> <option value='03'>Periodic Leave</option> <option value='04'>Recruitment</option> <option value='05'>Training</option> <option value='06'>Visitor</option> </select>"
            ElseIf dtrip = "02" Then
                txtareadtl = "<select id='detailtrip' style='width:95px;'><option value='01'>Business Trip</option> <option value='02' selected='selected'>Annual Leave</option> <option value='03'>Periodic Leave</option> <option value='04'>Recruitment</option> <option value='05'>Training</option> <option value='06'>Visitor</option> </select>"
            ElseIf dtrip = "03" Then
                txtareadtl = "<select id='detailtrip' style='width:95px;'><option value='01'>Business Trip</option> <option value='02'>Annual Leave</option> <option value='03' selected='selected'>Periodic Leave</option> <option value='04'>Recruitment</option> <option value='05'>Training</option> <option value='06'>Visitor</option> </select>"
            ElseIf dtrip = "04" Then
                txtareadtl = "<select id='detailtrip' style='width:95px;'> <option value='01'>Business Trip</option> <option value='02'>Annual Leave</option> <option value='03'>Periodic Leave</option> <option value='04' selected='selected'>Recruitment</option> <option value='05'>Training</option> <option value='06'>Visitor</option> </select>"
            ElseIf dtrip = "05" Then
                txtareadtl = "<select id='detailtrip' style='width:95px;'> <option value='01'>Business Trip</option> <option value='02'>Annual Leave</option> <option value='03'>Periodic Leave</option> <option value='04'>Recruitment</option> <option value='05'  selected='selected'>Training</option> <option value='06'>Visitor</option> </select>"
            ElseIf dtrip = "06" Then
                txtareadtl = "<select id='detailtrip' style='width:95px;'> <option value='01'>Business Trip</option> <option value='02'>Annual Leave</option> <option value='03'>Periodic Leave</option> <option value='04'>Recruitment</option> <option value='05'>Training</option> <option value='06' selected='selected'>Visitor</option> </select>"
            End If

            txtareapurp = "<textarea id='purpose' style='width:107%;Height:100px' onkeypress='return chkchar(event)' class='entri' cols='5' rows='5'>" + purpose + "</textarea>"
            Me.edit.Enabled = False
            strtbl = ""
            Try
                Dim sqlConn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

                sqlConn2.Open()
                Dim i As Integer
                Dim dtb2 As DataTable = New DataTable()

                Dim strcon2 As String = "select * from V_H00102 where Tripno = '" + idw + "'"
                Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn2)
                sda2.Fill(dtb2)
                jml = dtb2.Rows.Count.ToString
                If jml = "0" Then jml = "1"

                If dtb2.Rows().Count > 0 Then
                    For i = 0 To dtb2.Rows.Count - 1
                        If dtb2.Rows(i)!timeprefer = "01" Then
                            seltmpref = "<option value='01' selected='selected'>Morning</option> <option value='02'>Noon</option> <option value='03'>Afternoon</option> <option value='04'>Night</option>"
                        ElseIf dtb2.Rows(i)!timeprefer = "02" Then
                            seltmpref = "<option value='01'>Morning</option> <option value='02' selected='selected'>Noon</option> <option value='03'>Afternoon</option> <option value='04'>Night</option>"
                        ElseIf dtb2.Rows(i)!timeprefer = "03" Then
                            seltmpref = "<option value='01'>Morning</option> <option value='02'>Noon</option> <option value='03' selected='selected'>Afternoon</option> <option value='04'>Night</option>"
                        ElseIf dtb2.Rows(i)!timeprefer = "04" Then
                            seltmpref = "<option value='01'>Morning</option> <option value='02'>Noon</option> <option value='03'>Afternoon</option> <option value='04' selected='selected'>Night</option>"
                        Else
                            seltmpref = "<option>01</option>"
                        End If

                        If dtb2.Rows(i)!typetrans = "01" Then
                            seltytrns = "<option value='01' selected='selected'>Air</option> <option value='02'>Land</option> <option value='03'>Sea</option>"

                        ElseIf dtb2.Rows(i)!typetrans = "02" Then
                            seltytrns = "<option value='01'>Air</option> <option value='02' selected='selected'>Land</option> <option value='03'>Sea</option>"
                            strenable = "disabled='disabled'"
                        ElseIf dtb2.Rows(i)!typetrans = "03" Then
                            seltytrns = "<option value='01'>Air</option> <option value='02'>Land</option> <option value='03' selected='selected'>Sea</option>"
                            strenable = " "
                        Else
                            seltytrns = "<option>01</option>"
                            strenable = " "
                        End If

                        If dtb2.Rows(i)!TypeClass.ToString = "01" Then
                            seltyclss = "<option value='01' selected='selected'>Economy</option> <option value='02'>Business</option><option value='00'></option>"
                        ElseIf dtb2.Rows(i)!TypeClass.ToString = "02" Then
                            seltyclss = "<option value='01'>Economy</option> <option value='02' selected='selected'>Business</option><option value='00'></option>"
                        Else
                            seltyclss = "<option value='01'>Economy</option> <option value='02'>Business</option><option value='00' selected='selected'></option>"
                        End If

                        strtbl = strtbl + "<div id='input" + (i + 1).ToString + "' style='margin-bottom:4px; width:100%;' class='clonedInput'>"
                        strtbl = strtbl + " <input type='text' name='no" + i.ToString + "' id='no" + i.ToString + "' value='" + (i + 1).ToString + "'  style='width:2%;' />"
                        strtbl = strtbl + " <input type='text' name='depfrom" + (i + 1).ToString + "' id='depfrom" + (i + 1).ToString + "' onkeypress='return chkchar(event)' value='" + dtb2.Rows(i)!dep_from + "' style='width:12%;'/>"
                        strtbl = strtbl + " <input type='text' name='depdate" + (i + 1).ToString + "' id='depdate" + (i + 1).ToString + "' value='" + CDate(dtb2.Rows(i)!dep_date).ToString("MM/dd/yyy") + "' style='width:11%;' disabled='disabled'/><button id='bdep" + (i + 1).ToString + "'>...</button>"
                        strtbl = strtbl + " <input type='text' name='arriveto" + (i + 1).ToString + "' id='arriveto" + (i + 1).ToString + "' onkeypress='return chkchar(event)' value='" + dtb2.Rows(i)!arrive_to + "' style='width:11%;'/>"
                        strtbl = strtbl + " <input type='text' name='arrivedate" + (i + 1).ToString + "' id='arrivedate" + (i + 1).ToString + "' value='" + CDate(dtb2.Rows(i)!arrive_date).ToString("MM/dd/yyy") + "' style='width:11%;' disabled='disabled'/><button id='barrive" + (i + 1).ToString + "'>...</button>"
                        strtbl = strtbl + " <input type='text' name='remark" + (i + 1).ToString + "' id='remark" + (i + 1).ToString + "' onkeypress='return chkchar(event)' style='width:30%;' value='" + dtb2.Rows(i)!remark + "'/>"
                        strtbl = strtbl + " <select name='timeprefer" + (i + 1).ToString + "' id='timeprefer" + (i + 1).ToString + "' style='width:6%;'>" + seltmpref + "</select>"
                        strtbl = strtbl + " <select name='typetrans" + (i + 1).ToString + "' id='typetrans" + (i + 1).ToString + "' onchange= 'chktype()' style='width:4%;'>" + seltytrns + "</select>"
                        strtbl = strtbl + " <select name='class" + (i + 1).ToString + "' id='class" + (i + 1).ToString + "' style='width:4%;'" + strenable + ">" + seltyclss + "</select>"
                        strtbl = strtbl + " </div>"

                        strtbl = strtbl + "<div>"
                        strtbl = strtbl + "<input type='hidden' name='genid" + (i + 1).ToString + "' id='genid" + (i + 1).ToString + "' value='" + dtb2.Rows(i)!genid + "' />"
                        strtbl = strtbl + "</div>"
                    Next
                    detsts = "0"
                Else
                    If dtb2.Rows.Count = 0 Then
                        seltmpref = "<option value='01' selected='selected'>Morning</option> <option value='02'>Noon</option> <option value='03'>Afternoon</option> <option value='04'>Night</option>"
                        seltytrns = "<option value='01' selected='selected'>Air</option> <option value='02'>Land</option> <option value='03'>Sea</option>"
                        'strenable = "disabled='disabled'"
                        seltyclss = "<option value='01' selected='selected'>Economy</option> <option value='02'>Business</option><option value='00'></option>"

                        strtbl = strtbl + "<div id='input1' style='margin-bottom:4px; width:100%;' class='clonedInput'>"
                        strtbl = strtbl + " <input type='text' name='no1' id='no1' value='1'  style='width:2%;' disabled='disabled'/>"
                        strtbl = strtbl + " <input type='text' name='depfrom1' id='depfrom1' value='' style='width:12%;'/>"
                        strtbl = strtbl + " <input type='text' name='depdate1' id='depdate1' value='' style='width:11%;' disabled='disabled'/><button id='bdep1' >...</button>"
                        strtbl = strtbl + " <input type='text' name='arriveto1' id='arriveto1' value='' style='width:11%;' />"
                        strtbl = strtbl + " <input type='text' name='arrivedate1' id='arrivedate1' value='' style='width:11%;' disabled='disabled'/><button id='barrive1' >...</button>"
                        strtbl = strtbl + " <input type='text' name='remark1' id='remark1' style='width:30%;' value='' />"
                        strtbl = strtbl + " <select name='timeprefer1' id='timeprefer1' style='width:6%;' >" + seltmpref + "</select>"
                        strtbl = strtbl + " <select name='typetrans1' id='typetrans1' onchange= 'chktype()' style='width:4%;' >" + seltytrns + "</select>"
                        strtbl = strtbl + " <select name='class1' id='class1' style='width:4%;'" + strenable + ">" + seltyclss + "</select>"
                        strtbl = strtbl + " </div>"

                        strtbl = strtbl + "<div>"
                        strtbl = strtbl + "<input type='hidden' name='genid1' id='genid1' value='' />"
                        strtbl = strtbl + "</div>"

                        detsts = "1"
                        'jvc2 = jvc2 + "cal.manageFields('bdep1', 'depdate1', '%m/%d/%Y');"
                        'jvc2 = jvc2 + "cal.manageFields('barrive1', 'arrivedate1', '%m/%d/%Y');"
                    End If

                End If
                strtbl = strtbl + "<div style='margin-bottom:4px;'> <input type='button' id='btnAdd' value='add'/> <input type='button' id='btnDel' value='remove'/></div>"
            Catch ex As Exception

            End Try

            strtbl2 = ""

            Try
                Dim sqlConn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

                sqlConn2.Open()
                Dim j As Integer
                Dim dtb3 As DataTable = New DataTable()

                Dim strcon2 As String = "select * from V_H00106 where Tripno = '" + idw + "' order by GenID asc"
                Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn2)
                sda2.Fill(dtb3)

                jml2 = dtb3.Rows.Count.ToString
                If jml2 = "0" Then jml2 = "1"

                If dtb3.Rows().Count > 0 Then
                    For j = 0 To dtb3.Rows.Count - 1
                        strtbl2 = strtbl2 + "<div id='akoinput" + (j + 1).ToString + "' style='margin-bottom:0px; width:100%;' class='akoinput'>"
                        strtbl2 = strtbl2 + " <input type='text' name='n" + (j + 1).ToString + "' id='n" + (j + 1).ToString + "' value='" + (j + 1).ToString + "' style='width:2%;' />"
                        strtbl2 = strtbl2 + " <input type='text' name='location" + (j + 1).ToString + "' id='location" + (j + 1).ToString + "' onkeypress='return chkchar(event)' value='" + dtb3.Rows(j)!location + "' style='width:10%;' />"
                        strtbl2 = strtbl2 + " <input type='text' name='datein" + (j + 1).ToString + "' id='datein" + (j + 1).ToString + "' value='" + CDate(dtb3.Rows(j)!datein).ToString("MM/dd/yyy") + "' style='width:10%;' disabled='disabled'/><button id='bin" + (j + 1).ToString + "'>...</button>"
                        strtbl2 = strtbl2 + " <input type='text' name='dateout" + (j + 1).ToString + "' id='dateout" + (j + 1).ToString + "' value='" + CDate(dtb3.Rows(j)!dateout).ToString("MM/dd/yyy") + "' style='width:10%;' disabled='disabled'/><button id='bout" + (j + 1).ToString + "'>...</button>"
                        strtbl2 = strtbl2 + " <input type='text' name='rem" + (j + 1).ToString + "' id='rem" + (j + 1).ToString + "' onkeypress='return chkchar(event)' value='" + dtb3.Rows(j)!remark + "' style='width:27%;' />"
                        strtbl2 = strtbl2 + " </div>"

                        strtbl2 = strtbl2 + "<div>"
                        strtbl2 = strtbl2 + "<input type='hidden' name='genid2" + (j + 1).ToString + "' id='genid2" + (j + 1).ToString + "' value='" + dtb3.Rows(j)!genid + "' />"
                        strtbl2 = strtbl2 + "</div>"
                    Next
                    detsts2 = "0"
                Else
                    If dtb3.Rows().Count = 0 Then
                        strtbl2 = strtbl2 + "<div id='akoinput1' style='margin-bottom:0px; width:100%' class='akoinput' disabled='disabled'>"
                        strtbl2 = strtbl2 + " <input type='text' name='n1' id='n1' value='1' style='width:2%;' disabled='disabled'/>"
                        strtbl2 = strtbl2 + " <input type='text' name='location1' id='location1' value='' style='width:10%;' />"
                        strtbl2 = strtbl2 + " <input type='text' name='datein1' id='datein1' value='' style='width:10%;' disabled='disabled'/><button id='bin1' >...</button>"
                        strtbl2 = strtbl2 + " <input type='text' name='dateout1' id='dateout1' value='' style='width:10%;' disabled='disabled'/><button id='bout1' >...</button>"
                        strtbl2 = strtbl2 + " <input type='text' name='rem1' id='rem1' value='' style='width:27%;' />"
                        strtbl2 = strtbl2 + " </div>"

                        strtbl2 = strtbl2 + "<div>"
                        strtbl2 = strtbl2 + "<input type='hidden' name='genid21' id='genid21' value='' />"
                        strtbl2 = strtbl2 + "</div>"

                        detsts2 = "1"
                        'jvc = jvc + "cal.manageFields('bin1', 'datein1', '%m/%d/%Y');"
                        'jvc = jvc + "cal.manageFields('bout1', 'dateout1', '%m/%d/%Y');"
                        'jvc = "<script type ='text/javascript'> var cal = Calendar.setup({ onSelect: function(cal) { cal.hide() }, showTime: false });" + jvc + jvc2 + " </script>"
                    End If

                End If
                strtbl2 = strtbl2 + "<div> <input type='button' id='akoadd' value='add'/> <input type='button' id='akodel' value='remove'/></div>"
            Catch ex As Exception

            End Try

            Try
                Dim conn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
                conn2.Open()
                Dim dtb2 As DataTable = New DataTable()
                Dim strcon2 As String = "Select max(genid) as genid from V_H00102"
                Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, conn2)
                sda2.Fill(dtb2)

                If dtb2.Rows.Count = 1 Then
                    genid = dtb2.Rows(0)!genid
                Else
                    genid = 0
                End If

                conn2.Close()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            Try
                Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
                con.Open()
                Dim dtb As DataTable = New DataTable()
                Dim strcon As String = "Select max(genid) as genid from V_H00106"

                Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, con)
                sda.Fill(dtb)

                If dtb.Rows.Count = 1 Then
                    genid2 = dtb.Rows(0)!genid
                Else
                    genid2 = 0
                End If
            Catch ex As Exception

            End Try
        ElseIf fstatus = 1 Then 'editga = 1 Or //PERUBAHAN REQ DESSY 12 NOV 2013
            lblinf.Text = "changes can not be done as the data is processed"
            'MsgBox("changes can not be done as the data is processed", MsgBoxStyle.OkOnly, "Information")
            'txtareadtl = "<textarea id='detailtrip' disabled= 'disabled' style='width:107%;Height:100px' class='entri' cols='5' rows='5'>" + dtrip + "</textarea>"
            'txtareapurp = "<textarea id='purpose' style='width:107%;Height:100px' disabled='disabled' class='entri' cols='5' rows='5'>" + purpose + "</textarea>"
            edit.Enabled = False
            delete.Enabled = False
        End If
        srcspv = "<img alt='add' id='Img5' src='images/Search.gif' align='absmiddle' style='cursor: pointer' onclick='OpenPopup('suser')' />"
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
        Dim conn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)

        Dim sqlQuery As String
        Dim cmd As SqlCommand
        Dim bcheck As Boolean = True

        Try
            conn.Open()

            'Review By (ex: jakarta= Andreas Saragih (JRN0324) nik 0001807)
            Dim ReviewBy As String = "0001807"

            'Proceed By (ex : jakarta= Dessy (JRN0071) nik 0001296)
            Dim ProceedBy As String = "0001296"

            'VP Approval
            Dim VPApproval As String = String.Empty
            Dim sqllh001 As String = "SELECT TOP 1 nik from H_A101 where NIKSITE in (SELECT TOP 1 vpniksite FROM V_ESS_BTR_APPROVAL_VP  where nik=(SELECT TOP 1 nik FROM V_H001 WHERE tripNo = '{0}'))"
            sqllh001 = String.Format(sqllh001, idw)

            Dim dtblh001 As DataTable = New DataTable
            Dim sdalh001 As SqlDataAdapter = New SqlDataAdapter(sqllh001, conn)

            sdalh001.Fill(dtblh001)

            If dtblh001.Rows.Count > 0 Then
                VPApproval = dtblh001.Rows(0)!nik.ToString
            End If
            conn.Close()

            conn2.Open()
            sqlQuery = "update V_H001 set StatusWF = 'INT', reviewby = '{0}', proceedby = '{1}', apprby = '{2}' where tripNo = '{3}'"
            sqlQuery = String.Format(sqlQuery, ReviewBy, ProceedBy, VPApproval, idw)

            cmd = New SqlCommand(sqlQuery, conn2)
            cmd.ExecuteScalar()
            conn2.Close()

        Catch ex As Exception
            bcheck = False
        Finally

            If bcheck = True Then
                alert = "<script type ='text/javascript' > alert('Data has been updated') </script>"
                btnSubmit.Enabled = False
            Else
                alert = "<script type ='text/javascript' > alert('data updated failed') </script>"
            End If
        End Try


    End Sub


End Class