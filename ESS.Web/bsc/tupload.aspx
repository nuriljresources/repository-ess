﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="tupload.aspx.vb" Inherits="EXCELLENT.tupload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>  
    <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>   
</head>
<body>    
    <form id="form1" runat="server">
    <asp:HiddenField runat="Server" id="ipcomp" />
    <asp:HiddenField runat="Server" id="userid" />
    
    <asp:Label ID="LblError" runat="server" Text="" Font-Bold="True" 
        Font-Size="Large" ForeColor="#FF3300"></asp:Label>
    <asp:Label ID="Info" runat="server" Text="" Font-Bold="True" 
        Font-Size="Large" ForeColor="#000000"></asp:Label>
    <div id="divfile" style="border: 1px solid #C0C0C0"> 
        <asp:FileUpload ID="FileChooser" runat="server" /> 
        <br />
        <asp:Button ID="BtnUpload" runat="server" Text="Upload File" />
        <input id="BtnClosed" type="button" value="Tutup" onclick="window.close()"/>
        <span>(Max Upload 600 KB)</span>
        <br />    
    </div> 
       <asp:HiddenField id="year" runat="server"/>
       <asp:HiddenField id="month" runat="server"/>
       <asp:HiddenField id="kpiid" runat="server"/>             
         
    <div id="divdaftar" style="border: 1px solid #C0C0C0; padding: 1px; margin: left; overflow: scroll; position: relative; width: 380px; height: 200px;">
    List Of File ___    
          
    </div>    
    </form>
            
    <script language="javascript" type="text/javascript">
       var tmplamp = new Array();

       function showLampiran(year,month,kpiid) {
          $.ajax({
             type: "POST",
             contentType: "application/json; charset=utf-8",
             url: "WS/BSCService.asmx/ShowLampiran",
             data: "{'year':'" + year + "','month':'" + month + "','kpiid':'" + kpiid + "'}",
             dataType: "json",
             success: function(res) {
                var afile = new Array();
                afile = res.d;

                var shtml = "<span><u><b>Daftar File Lampiran</b></u></span><br/>";
                if (afile.length > 0) {
                   for (var i = 0; i < afile.length; i++) {
                      shtml = shtml + (i + 1) + '.' + afile[i].NmFile + ' __ ' + '<a href="#" onclick="hapusLampiran(' + afile[i].Nomor + ')">[Hapus]</a>';
                      shtml = shtml + '<a href="#" onclick="download(' + afile[i].Nomor + ')">[Download]</a><br/>';
                   }
                }
                else {
                   shtml = shtml + "Tidak Terdapat Lampiran";
                }
                document.getElementById('divdaftar').innerHTML = shtml;
             },
             error: function(err) {
                alert(err.responseText);
             }
          });
       }

       function hapusLampiran(nomor) {
          $.ajax({
             type: "POST",
             contentType: "application/json; charset=utf-8",
             url: "WS/BSCService.asmx/DelLampiran",
             data: "{'nomor':'" + nomor + "','ipcomputer':'" + document.getElementById("ipcomp").value + "','userid':'" + document.getElementById("userid").value  + "'}",
             dataType: "json",
             success: function(res) {
                if (res.d == "SUCCESS") {
                   $.ajax({
                      type: "POST",
                      contentType: "application/json; charset=utf-8",
                      url: "WS/BSCService.asmx/ShowLampiran",
                      data: "{'year':'" + document.getElementById('year').value + "','month':'" + document.getElementById('month').value + "','kpiid':'" + document.getElementById('kpiid').value + "'}",
                      dataType: "json",
                      success: function(res) {
                         var afile = new Array();
                         afile = res.d;

                         var shtml = "<span><u><b>Daftar File Lampiran</b></u></span><br/>";
                         if (afile.length > 0) {
                            for (var i = 0; i < afile.length; i++) {
                               shtml = shtml + (i + 1) + '.' + afile[i].NmFile + ' __ ' + '<a href="#" onclick="hapusLampiran(' + afile[i].Nomor + ')">[Hapus]</a>';
                               shtml = shtml + '<a href="#" onclick="download(' + afile[i].Nomor + ')">[Download]</a><br/>';
                            }
                         }
                         else {
                            shtml = shtml + "Tidak Terdapat Lampiran";
                         }
                         document.getElementById('divdaftar').innerHTML = shtml;
                      },
                      error: function(err) {
                         alert(err.responseText);
                      }
                   });
                } else {
                   alert(res.d);
                }
             },
             error: function(err) {
                alert(err.responseText);
             }
          });
       }

       function download(nomor) {
          window.open("tdownload.aspx?n=" + nomor, "Download", "scrollbars=no,resizable=no,width=1,height=1");
       }
               
        showLampiran(document.getElementById('year').value, document.getElementById('month').value, document.getElementById('kpiid').value);
    </script>    
</body>
</html>
