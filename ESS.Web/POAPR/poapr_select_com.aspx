﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="poapr_select_com.aspx.vb" Inherits="EXCELLENT.poapr_select_com" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Select Company</title>
    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <link href="../css/site.css" rel="stylesheet" type="text/css" media="interactive, braille, emboss, handheld, projection, screen, tty, tv" />
    <link href="../css/dropdown/dropdown.css" media="all" rel="stylesheet" type="text/css" />
    <link href="../css/dropdown/themes/default/default.ultimate.css" media="all" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <center>        
       <div><center>
       <table class="data-table" style="border: 1px solid #0066ff; width: 25%">
            <tr>
            <td colspan="2" bgcolor="#0066ff" 
                    style="text-align:center;font-size: large; font-family: calibri; font-weight: bold; width:100%">
                Select Company
            </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label1" runat="server" Text="User ID" Font-Size="Small">
                </asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox1" runat="server" class="tb10" Width="92%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label2" runat="server" Text="Label" Font-Size="Small">Company</asp:Label>
                </td>
                <td style="width:60%">
                    <%--<asp:TextBox ID="TextBox2" TextMode="password" runat="server" class="tb10" Width="92%"></asp:TextBox>--%>
                    <asp:DropDownList ID="ddl1" runat="server">
                        <asp:ListItem Value=""> </asp:ListItem>
                        <asp:ListItem Value="JRN">JRN</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> </td>                
            </tr>
            <tr>
                <td style="width:40%">
                    <%--<img alt="" src="images/Locked.png" />--%>
                </td>
                <td style="width:60%">
                    <asp:Button ID="btnLogin" runat="server" Text="Login" Width="80" Font-Names="Calibri"/>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="80" Font-Names="Calibri" />
                </td>
            </tr>
            <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblConfirm" runat="server" Text="" Font-Size="Small" ForeColor="Red"></asp:Label>
            </td>
            </tr>
        </table> </center> 
    </div>
    </center>
    </form>
</body>
</html>
