﻿Imports System.Data.SqlClient

Partial Public Class popupnewkpi
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") Is Nothing Then
            Response.Redirect("../Login.aspx", False)
            Exit Sub
        ElseIf Session("permission") = "" Then
            Response.Redirect("../Login.aspx", False)
            Exit Sub
        End If
        Dim lengthFunction As String = "function isMaxLength(txtBox,panjang) {"
        lengthFunction += " if(txtBox) { "
        lengthFunction += "     return ( txtBox.value.length <= panjang-1);"
        lengthFunction += " }"
        lengthFunction += "}"

        Dim blurFunction As String = "function isBlurLength(txtBox,panjang) {"
        blurFunction += " if(txtBox) { "
        blurFunction += " if (txtBox.value.length > panjang) {"
        blurFunction += "  alert('Jumlah karakter tidak boleh lebih dari ' + panjang);"
        blurFunction += "  txtBox.value = txtBox.value.substring(1,panjang);"
        blurFunction += "  }"
        blurFunction += " }"
        blurFunction += "}"

        Me.KPIDesc.Attributes.Add("onkeypress", "return isMaxLength(this,300);")
        Me.KPIDesc.Attributes.Add("onblur", "isBlurLength(this,300);")
        Me.KPIFDesc.Attributes.Add("onkeypress", "return isMaxLength(this,500);")
        Me.KPIFDesc.Attributes.Add("onblur", "isBlurLength(this,500);")
        Me.KPILeads.Attributes.Add("onkeypress", "return isMaxLength(this,500);")
        Me.KPILeads.Attributes.Add("onblur", "isBlurLength(this,500);")
        Me.KPIProcess.Attributes.Add("onkeypress", "return isMaxLength(this,500);")
        Me.KPIProcess.Attributes.Add("onblur", "isBlurLength(this,500);")

        ClientScript.RegisterClientScriptBlock(Me.[GetType](), "txtLength", lengthFunction, True)
        ClientScript.RegisterClientScriptBlock(Me.[GetType](), "txtBlurLength", blurFunction, True)
        If ("5").IndexOf(Session("permission")) <> -1 Then
            btnSave.Visible = False
        End If

        If Session("permission") = "1" Then
            KPIReady.Enabled = True
        Else
            KPIReady.Enabled = False
        End If

        If Not Page.IsPostBack Then
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim dr As SqlDataReader = Nothing
            Try
                Dim type As String = ""
                Dim id, func, soid, dir As String
                KPIRed.Enabled = False
                KPIGreen.Enabled = False
                RedParam.Enabled = False
                GreenParam.Enabled = False
                type = Request.QueryString("type")
                Dim cmd As SqlCommand
                Dim query As String = ""
                conn.Open()
                '----------------------
               

                'GET DATA
                query = "select year,id,dir,func,soid from mkpi where id='" & Request.QueryString("id") & "'"
                cmd = New SqlCommand(query, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    id = dr("id")
                    dir = dr("dir")
                    func = dr("func")
                    soid = dr("soid")
                    KPIYear.SelectedValue = dr("year")
                End While
                dr.Close()
                dr = Nothing

                'Initialize Jobsite
                query = "select id,kdsite,active from site order by id"
                Dim dt As New DataTable
                cmd = New SqlCommand(query, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                    If dr(2) = 0 Then
                        KPIJobsite.Items(KPIJobsite.Items.Count - 1).Enabled = False
                    End If
                End While
                'KPIJobsite.RepeatDirection = RepeatDirection.Horizontal
                KPIJobsite.Visible = False
                cKPIJobsite.Visible = False
                myjobsite.Visible = False
                dr.Close()
                dr = Nothing

                'Initialize Function
                Select Case Request.QueryString("type")
                    Case 4
                        query = "select ID,FunctionName,active from MFunction where active='1' and DirID='" & dir & "' order by ID"
                        cmd = New SqlCommand(query, conn)
                        dr = cmd.ExecuteReader
                        While dr.Read
                            KPIFuncLevel0.Items.Add(New ListItem(dr(1), dr(0)))
                            If dr(2) = 0 Then
                                KPIFuncLevel0.Items(KPIJobsite.Items.Count - 1).Enabled = False
                            End If
                        End While
                        myfunction.Visible = True
                        KPIFuncLevel0.Visible = True
                        cKPIFuncLevel0.Visible = True
                        dr.Close()
                        dr = Nothing
                End Select
                'Initialize Direktorat
                 Select Request.QueryString("type")
                    Case 0
                        query = "select ID,DirektoratName,active from MDirektorat where active='1' order by ID"
                        cmd = New SqlCommand(query, conn)
                        dr = cmd.ExecuteReader
                        While dr.Read
                            If Session("permission") = "1" Or Session("permission") = "B" Then 'GRUP MSD
                                KPIDirektorat.Items.Add(New ListItem(dr(1), dr(0)))
                            End If
                        End While
                        dr.Close()
                        dr = Nothing
                End Select
                'Initialize Section
                If Request.QueryString("type") = "1" Then
                    query = "select ID,SectionName,active from MSection where funcid='" & func & "' order by ID"
                    cmd = New SqlCommand(query, conn)
                    dr = cmd.ExecuteReader
                    While dr.Read
                        KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                        'If dr(2) = 0 Then
                        '    KPISection.Items(KPISection.Items.Count - 1).Enabled = False
                        'End If
                    End While
                    KPISection.Visible = False
                    dr.Close()
                    dr = Nothing
                ElseIf Request.QueryString("type") = "3" Then
                    query = "select ID,SectionName,active from MSection order by ID"
                    cmd = New SqlCommand(query, conn)
                    dr = cmd.ExecuteReader
                    While dr.Read
                        KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                    End While
                    KPISection.Visible = False
                    dr.Close()
                    dr = Nothing
                End If
                'Initialize SO
                query = "select a.id, c.abbr,a.SO from MSO a inner join MST b on a.stid=b.stid inner join mperspective c on b.prid=c.id where a.id='" & soid & "'  order by c.id,SO"
                cmd = New SqlCommand(query, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    SO.Items.Add(New ListItem(dr(1) & " - " & dr(2), dr(0)))
                End While
                dr.Close()
                dr = Nothing
                'Initialize KPI Level
                If type <> "" Or type IsNot Nothing Then
                    GenerateKPILevel(type)
                End If


                'Get Data If Edit KPI
                If Request.QueryString("idtoedit") IsNot Nothing Then
                    KPIID.Value = Request.QueryString("idtoedit")
                    query = "select * from MKPI where id='" & Request.QueryString("idtoedit") & "'"
                    cmd = New SqlCommand(query, conn)
                    dr = cmd.ExecuteReader
                    While dr.Read
                        KPIID.Value = Request.QueryString("idtoedit")
                        KPILevel.SelectedValue = dr("Level")
                        If KPILevel.SelectedValue = "1" Then
                            For Each x As ListItem In KPIFuncLevel0.Items
                                If x.Value = dr("Func") Then
                                    x.Selected = True
                                    x.Enabled = False
                                Else
                                    x.Enabled = False
                                End If
                            Next
                            cKPIFuncLevel0.Attributes.Add("disabled", "disabled")
                            myjobsite.Visible = False
                            KPIJobsite.Visible = False
                            cKPIJobsite.Visible = False
                        ElseIf KPILevel.SelectedValue = "4" Then
                            For Each x As ListItem In KPIDirektorat.Items
                                If x.Value = dr("Dir") Then
                                    x.Selected = True
                                    x.Enabled = False
                                Else
                                    x.Enabled = False
                                End If
                            Next
                            cKPIDirektorat.Attributes.Add("disabled", "disabled")
                            myjobsite.Visible = False
                            KPIJobsite.Visible = False
                            cKPIJobsite.Visible = False
                        ElseIf KPILevel.SelectedValue = "2" Then
                            For Each x As ListItem In KPILevel.Items
                                If x.Value = dr("Level") Then
                                    x.Selected = True
                                Else
                                    x.Enabled = False
                                End If
                            Next
                            For Each x As ListItem In KPISection.Items
                                If x.Value = dr("Section") Then
                                    x.Selected = True
                                Else
                                    x.Enabled = False
                                End If
                            Next
                            For Each x As ListItem In KPIJobsite.Items
                                If x.Value = dr("Jobsite") Then
                                    x.Selected = True
                                    x.Enabled = False
                                Else
                                    x.Enabled = False
                                End If
                            Next
                            KPISection.Visible = True
                            KPIFunc.Visible = False
                            myjobsite.Visible = True
                            KPIJobsite.Visible = True
                            cKPIJobsite.Visible = True
                            cKPIJobsite.Attributes.Add("disabled", "disabled")
                            myfunction.Visible = False
                        End If
                        SO.SelectedValue = dr("SOID")
                        KPIUOM.Text = dr("UoM")
                        KPIName.Text = dr("Name")
                        KPIOwner.Text = dr("Owner")
                        KPIDesc.Text = dr("Desc")
                        KPIFDesc.Text = dr("FormulaDesc")
                        KPIPeriod.SelectedValue = dr("Period")
                        RedParam.SelectedValue = dr("RedCond")
                        GreenParam.SelectedValue = dr("GreenCond")
                        KPIRed.Text = dr("RedParam")
                        KPIGreen.Text = dr("GreenParam")

                        Select Case dr("CalcType")
                            Case 0
                                KPICalcTypeH.SelectedIndex = 0
                                KPICalcTypeD.SelectedValue = dr("CalcType")
                            Case 1
                                KPICalcTypeH.SelectedIndex = 0
                                KPICalcTypeD.SelectedValue = dr("CalcType")
                            Case Else
                                KPICalcTypeH.SelectedIndex = 1
                                KPICalcTypeC.SelectedValue = dr("CalcType")
                        End Select

                        KPIBased.SelectedValue = dr("BasedOn")
                        If IsDBNull(dr("KPIType")) Then
                            KPIType.SelectedValue = 0
                        Else
                            KPIType.SelectedValue = dr("KPIType")
                        End If
                        If Not IsDBNull(dr("KPILeads")) Then
                            KPILeads.Text = dr("KPILeads")
                        End If
                        If Not IsDBNull(dr("Process")) Then
                            KPIProcess.Text = dr("Process")
                        End If
                        If IsDBNull(dr("Targetting")) Then
                            KPITargetting.SelectedValue = 0
                        Else
                            KPITargetting.SelectedValue = dr("Targetting")
                        End If
                        KPIReady.SelectedValue = dr("StEdit").ToString()
                    End While
                    dr.Close()
                    dr = Nothing
                Else
                    KPIID.Value = "[AUTO]"
                End If

            Catch ex As Exception
                lblMessage.Text = ex.Message
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                If dr IsNot Nothing Then
                    dr = Nothing
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Value="0" Text=BUMA
    ''' Value="1" Text="Function"
    ''' Value="2" Text="Section"
    ''' Value="3" Text="Jobsite"
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub KPILevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles KPILevel.SelectedIndexChanged
        Try
            Select Case KPILevel.SelectedValue
                Case 0
                    KPIFunc.Visible = False
                    myjobsite.Visible = False
                    KPIJobsite.Visible = False
                    cKPIJobsite.Visible = False
                    KPISection.Visible = False
                Case 1
                    KPIFunc.Visible = True
                    myjobsite.Visible = False
                    KPIJobsite.Visible = False
                    cKPIJobsite.Visible = False
                    KPISection.Visible = False
                Case 2
                    KPIFunc.Visible = False
                    myjobsite.Visible = True
                    KPIJobsite.Visible = True
                    cKPIJobsite.Visible = True
                    KPISection.Visible = True
                Case 3
                    KPIFunc.Visible = False
                    myjobsite.Visible = True
                    KPIJobsite.Visible = True
                    cKPIJobsite.Visible = True
                    KPISection.Visible = False
                Case 4
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                    KPIDirektorat.Visible = True
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            If Validasi() Then
                Dim query As String = ""
                Dim result As String
                Dim cmd As SqlCommand

                If KPIID.Value = "[AUTO]" Then
                    Select Case Request.QueryString("type")
                        Case "0"
                            If Request.QueryString("id") IsNot Nothing Then
                                query = "BEGIN TRANSACTION; BEGIN TRY "
                                For Each c As ListItem In KPIDirektorat.Items
                                    If c.Selected Then
                                        query += "insert into MKPI " _
                                                & " (Level,SOID,Name,Dir,Func,Jobsite,Section,UoM,Owner,Type,[Desc],Period,Leads,Formula,FormulaDesc,RedParam,GreenParam,BasedOn,CalcType,Parent,CreatedBy,CreatedIn,RedCond,GreenCond,KPIType,KPILeads,Process,Targetting,StEdit,Year, Bobot) values " _
                                                & "('" & KPILevel.SelectedValue & "','" & SO.SelectedValue & "','" & KPIName.Text & "'," _
                                                & "'" & c.Value & "','','','','" _
                                                & KPIUOM.Text & "','" & KPIOwner.Text & "','','" _
                                                & KPIDesc.Text & "','" & KPIPeriod.SelectedValue & "','','0','" & KPIFDesc.Text & "','" & KPIRed.Text & "','" _
                                                & KPIGreen.Text & "','" & KPIBased.SelectedValue & "','" & IIf(KPICalcTypeH.SelectedValue = "0", KPICalcTypeD.SelectedValue, KPICalcTypeC.SelectedValue) & "','','" & Session("NikUser") & "','" & Request.UserHostName & "','" & RedParam.SelectedValue & "','" & GreenParam.SelectedValue & "','" & KPIType.SelectedValue & "','" & KPILeads.Text & "','" & KPIProcess.Text & "','" & KPITargetting.SelectedValue & "','" & KPIReady.SelectedValue & "','" & KPIYear.SelectedValue & "'," & KPIBobot.Text & "); insert into kpirelation values ('" & Request.QueryString("id") & "',IDENT_CURRENT('MKPI'),'0') "
                                    End If
                                Next
                                query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;select 0	end "
                            End If
                        Case "4"
                            If Request.QueryString("id") IsNot Nothing Then
                                query = "BEGIN TRANSACTION; BEGIN TRY "
                                For Each c As ListItem In KPIFuncLevel0.Items
                                    If c.Selected Then
                                        query += "insert into MKPI " _
                                                & " (Level,SOID,Name,Dir,Func,Jobsite,Section,UoM,Owner,Type,[Desc],Period,Leads,Formula,FormulaDesc,RedParam,GreenParam,BasedOn,CalcType,Parent,CreatedBy,CreatedIn,RedCond,GreenCond,KPIType,KPILeads,Process,Targetting,StEdit,Year, Bobot) values " _
                                                & "('" & KPILevel.SelectedValue & "','" & SO.SelectedValue & "','" & KPIName.Text & "'," _
                                                & "'','" & c.Value & "','','','" _
                                                & KPIUOM.Text & "','" & KPIOwner.Text & "','','" _
                                                & KPIDesc.Text & "','" & KPIPeriod.SelectedValue & "','','0','" & KPIFDesc.Text & "','" & KPIRed.Text & "','" _
                                                & KPIGreen.Text & "','" & KPIBased.SelectedValue & "','" & IIf(KPICalcTypeH.SelectedValue = "0", KPICalcTypeD.SelectedValue, KPICalcTypeC.SelectedValue) & "','','" & Session("NikUser") & "','" & Request.UserHostName & "','" & RedParam.SelectedValue & "','" & GreenParam.SelectedValue & "','" & KPIType.SelectedValue & "','" & KPILeads.Text & "','" & KPIProcess.Text & "','" & KPITargetting.SelectedValue & "','" & KPIReady.SelectedValue & "','" & KPIYear.SelectedValue & "'," & KPIBobot.Text & "); insert into kpirelation values ('" & Request.QueryString("id") & "',IDENT_CURRENT('MKPI'),'0') "
                                    End If
                                Next
                                query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;select 0	end "
                            End If
                    End Select
                Else
                    query = "BEGIN TRANSACTION; BEGIN TRY "
                    query += "Update MKPI Set " _
                            & "SOID='" & SO.SelectedValue & "',Name='" & KPIName.Text & "'," _
                            & "UoM='" & KPIUOM.Text & "', Owner='" & KPIOwner.Text & "'," _
                            & "Type='', [Desc]='" & KPIDesc.Text & "'," _
                            & "Period='" & KPIPeriod.SelectedValue & "',Leads='',FormulaDesc='" & KPIFDesc.Text & "'," _
                            & "RedParam=" & KPIRed.Text & ",GreenParam=" & KPIGreen.Text & "," _
                            & "BasedOn='" & KPIBased.SelectedValue & "',CalcType='" & IIf(KPICalcTypeH.SelectedValue = "0", KPICalcTypeD.SelectedValue, KPICalcTypeC.SelectedValue) & "'," _
                            & "RedCond='" & RedParam.SelectedValue & "',GreenCond='" & GreenParam.SelectedValue & "'," _
                            & "KPIType='" & KPIType.SelectedValue & "',KPILeads='" & KPILeads.Text & "'," _
                            & "Process='" & KPIProcess.Text & "',Targetting='" & KPITargetting.SelectedValue & "'," _
                            & "StEdit='" & KPIReady.SelectedValue & "',Bobot=" & KPIBobot.Text & "," _
                            & "ModifiedBy='" & Session("NikUser") & "', modifiedtime=getdate() where " _
                            & "ID='" & KPIID.Value & "';"
                    If KPILevel.SelectedValue = "0" Then 'BUMA
                        query += "update mkpi set soid='" & SO.SelectedValue & "',stedit='" & KPIReady.SelectedValue & "' where ID in (select idchild from KPIRELATION where IDParent='" & KPIID.Value & "');"
                    ElseIf KPILevel.SelectedValue = "1" Then 'FUNCTION
                        query += "update mkpi set soid='" & SO.SelectedValue & "',stedit='" & KPIReady.SelectedValue & "' where ID in (select idchild from KPIRELATION where IDParent='" & KPIID.Value & "');"
                        query += "update mkpi set soid='" & SO.SelectedValue & "',stedit='" & KPIReady.SelectedValue & "' where ID in (select idchild from KPIRelation where idparent in (select idchild from KPIRELATION where IDParent='" & KPIID.Value & "'));"
                    End If
                    query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
                End If

                cmd = New SqlCommand(query, conn)
                conn.Open()
                result = cmd.ExecuteScalar()
                If IsNumeric(result) Then
                    If KPIID.Value = "[AUTO]" Then
                        KPIID.Value = result
                        lblMessage.Text = "**Save Berhasil**"
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes2", "window.close();", True)
                    Else
                        cmd = New SqlCommand()
                        cmd.CommandText = "sp_recount"
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Connection = conn
                        cmd.Parameters.AddWithValue("kpiid", KPIID.Value)
                        cmd.ExecuteScalar()
                        cmd = New SqlCommand()
                        cmd.CommandText = "sp_recount_parent"
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Connection = conn
                        cmd.Parameters.AddWithValue("@kpiid", KPIID.Value)
                        cmd.ExecuteScalar()
                        cmd = New SqlCommand()
                        cmd.CommandText = "sp_recount_grandparent"
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Connection = conn
                        cmd.Parameters.AddWithValue("@kpiid", KPIID.Value)
                        cmd.ExecuteScalar()
                        lblMessage.Text = "**Update Berhasil**"
                    End If
                Else
                    lblMessage.Text = result
                End If
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes4", "onchangekpicalch();", True)
            End If
        Catch ex As Exception
            lblMessage.Text = ex.Message
        Finally
            conn.Close()
        End Try
    End Sub

    ''' <summary>
    ''' Value="0" Text=BUMA
    ''' Value="1" Text="Function"
    ''' Value="2" Text="Section"
    ''' Value="3" Text="Jobsite"
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GenerateFunction() As String
        Try
            Dim result As String = ""
            Select Case KPILevel.SelectedValue
                Case 0
                    Return ""
                Case 1
                    For Each c As ListItem In KPIFunc.Items
                        If c.Selected Then
                            result += "1"
                        Else
                            result += "0"
                        End If
                    Next
                    Return result
                Case 2
                    Return ""
                Case 3
                    Return ""
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
        Return ""
    End Function

    ''' <summary>
    ''' Value="0" Text=BUMA
    ''' Value="1" Text="Function"
    ''' Value="2" Text="Section"
    ''' Value="3" Text="Jobsite"
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GenerateSection() As String
        Try
            Dim result As String = ""
            Select Case KPILevel.SelectedValue
                Case 0
                    Return ""
                Case 1
                    Return ""
                Case 2
                    For Each c As ListItem In KPISection.Items
                        If c.Selected Then
                            result += "1"
                        Else
                            result += "0"
                        End If
                    Next
                    Return result
                Case 3
                    Return ""
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
        Return ""
    End Function

    ''' <summary>
    ''' Value="0" Text=BUMA
    ''' Value="1" Text="Function"
    ''' Value="2" Text="Section"
    ''' Value="3" Text="Jobsite"
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GenerateJobsite() As String
        Try
            Dim result As String = ""
            Select Case KPILevel.SelectedValue
                Case 0
                    Return ""
                Case 1
                    Return ""
                Case 2
                    For Each c As ListItem In KPIJobsite.Items
                        If c.Selected Then
                            result += "1"
                        Else
                            result += "0"
                        End If
                    Next
                    Return result
                Case 3
                    For Each c As ListItem In KPIJobsite.Items
                        If c.Selected Then
                            result += "1"
                        Else
                            result += "0"
                        End If
                    Next
                    Return result
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
        Return ""
    End Function

    Sub GenerateKPILevel(ByVal type As String)
        Try
            Select Case type
                Case "0" 'BUMA
                    For Each l As ListItem In KPILevel.Items
                        If l.Value = "0" Or l.Value = "3" Or l.Value = "2" Or l.Value = "1" Then
                            l.Enabled = False
                        End If
                    Next
                    KPIFunc.Visible = False
                    myfunction.Visible = False
                    KPIFuncLevel0.Visible = False
                    cKPIFuncLevel0.Visible = False
                    KPIJobsite.Visible = False
                    cKPIJobsite.Visible = False
                    myjobsite.Visible = False
                    KPISection.Visible = False
                Case "4" 'Direktorat
                    For Each l As ListItem In KPILevel.Items
                        If l.Value = "0" Or l.Value = "4" Or l.Value = "3" Or l.Value = "2" Then
                            l.Enabled = False
                        Else
                            l.Enabled = True
                        End If
                    Next
                    mydirektorat.Visible = False
                    cKPIDirektorat.Visible = False
                    KPIDirektorat.Visible = False
                    KPIFunc.Visible = False
                    myfunction.Visible = True
                    KPIFuncLevel0.Visible = True
                    cKPIFuncLevel0.Visible = True
                    KPIJobsite.Visible = False
                    cKPIJobsite.Visible = False
                    myjobsite.Visible = False
                    KPISection.Visible = False
                Case "1" 'Function
                    For Each l As ListItem In KPILevel.Items
                        If l.Value = "0" Or l.Value = "1" Or l.Value = "3" Then
                            l.Enabled = False
                        End If
                    Next
                    KPIFunc.Visible = False
                    myfunction.Visible = False
                    KPIFuncLevel0.Visible = False
                    cKPIFuncLevel0.Visible = False
                    KPIJobsite.Visible = True
                    cKPIJobsite.Visible = True
                    myjobsite.Visible = True
                    KPISection.Visible = True
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Function Validasi() As Boolean
        Try
            If Trim(KPIName.Text) = "" Then
                lblMessage.Text = "***Nama KPI Wajib Diisi"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                Return False
            ElseIf Trim(KPIUOM.Text) = "" Then
                lblMessage.Text = "***Unit Of Measurement KPI Wajib Diisi"
                                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)

                Return False
            ElseIf Trim(KPIOwner.Text) = "" Then
                lblMessage.Text = "***KPI Owner Wajib Diisi"
                                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)

                Return False
            ElseIf Trim(KPIRed.Text) = "" Then
                lblMessage.Text = "***Red Parameter Wajib Diisi"

                Return False
            ElseIf Trim(KPIBobot.Text) = "" Then
                lblMessage.Text = "***Bobot KPI Wajib Diisi"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                Return False
            ElseIf Trim(KPIGreen.Text) = "" Then
                lblMessage.Text = "***Green Parameter Wajib Diisi"
                Return False
            ElseIf Trim(KPIDesc.Text) = "" Then
                lblMessage.Text = "***Deskripsi KPI Wajib Diisi"
                Return False
            ElseIf Not IsNumeric(KPIRed.Text) Then
                lblMessage.Text = "***Nilai Red Parameter Harus Berupa Numerik"
                Return False
            ElseIf Not IsNumeric(KPIGreen.Text) Then
                lblMessage.Text = "***Nilai Green Parameter Harus Berupa Numerik"
                Return False
            ElseIf Not IsNumeric(KPIBobot.Text) Then
                lblMessage.Text = "***Bobot KPI Harus Berupa Numerik"
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                Return False
            End If
            KPIName.Text = KPIName.Text.Replace("'", "`")
            KPIUOM.Text = KPIUOM.Text.Replace("'", "`")
            KPIOwner.Text = KPIOwner.Text.Replace("'", "`")
            KPIDesc.Text = KPIDesc.Text.Replace("'", "`")
            KPIFDesc.Text = KPIFDesc.Text.Replace("'", "`")
            If Len(KPIDesc.Text) > 300 Then
                lblMessage.Text = "***Panjang Karakter KPI Description Maks. 300"
                                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)

                Return False
            ElseIf Len(KPIFDesc.Text) > 500 Then
                lblMessage.Text = "***Panjang Karakter KPI Description Maks. 500"
                                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)

                Return False
            End If
            Dim adaisi As Boolean = False
            If KPIID.Value = "[AUTO]" Then
                Select Case Request.QueryString("type")
                    Case "0"
                        If Request.QueryString("id") IsNot Nothing Then
                            For Each c As ListItem In KPIDirektorat.Items
                                If c.Selected Then
                                    adaisi = True
                                End If
                            Next
                        End If
                    Case "4"
                        If Request.QueryString("id") IsNot Nothing Then
                            For Each c As ListItem In KPIFuncLevel0.Items
                                If c.Selected Then
                                    adaisi = True
                                End If
                            Next
                        End If
                End Select
                If Not (adaisi) Then
                    Select Case Request.QueryString("type")
                        Case "0"
                            lblMessage.Text = "***Harap Pilih salah satu direktorat"
                            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                            Return False
                        Case "4"
                            lblMessage.Text = "***Harap Pilih salah satu function"
                            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                            Return False
                    End Select
                    Return False
                End If
            Else
                Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
                Dim sqlQuery As String
                Dim cmd As SqlCommand
                Dim result As Integer = 0
                sqlQuery = "select IsNull(Max(StEdit),0) as StEdit from MKPI where id in (select id from mkpi where id in (select idparent from KPIRELATION where idchild='" & KPIID.Value & "')  and level <> '3') and level <> '3'"
                cmd = New SqlCommand(sqlQuery, conn)
                result = 0
                conn.Open()
                result = cmd.ExecuteScalar
                conn.Close()
                If result = 1 And KPIReady.SelectedValue = 0 Then
                    lblMessage.Text = "***KPI Parent dalam keadaan OFF"
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "erroralert", "alert('" & lblMessage.Text & "')", True)
                    Return False
                End If
            End If
            
            Return True
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Function
End Class