﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="gauge.aspx.vb"
   Inherits="EXCELLENT.gauge" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
   <title>Gauge</title>

   <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>

   <!--[if IE]><script type="text/javascript" src="../Scripts/excanvas.js"></script><![endif]-->

   <script type="text/javascript" src="../Scripts/gauge.js"></script>

   <script src="../Scripts/json2.js" type="text/javascript"></script>

   <style type="text/css">
      .judullead
      {
         font-weight: bold;
         font-size: 10px;
      }
      .footerlead
      {
         font-size: 8px;
      }
      .judulwig
      {
         font-weight: bold;
         font-size: 14px;
      }
      .footerwig
      {
         font-size: 10px;
      }
      #divprogress
      {
         z-index: 1000;
         position: absolute;
         top: 0;
         bottom: 0;
         left: 0;
         width: 100%;
         background: #000;
         opacity: 0.55;
         -moz-opacity: 0.55;
         filter: alpha(opacity=55);
         visibility: hidden;
      }
   </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <asp:HiddenField runat="server" ID="permission" />
   <asp:HiddenField runat="server" ID="jobsite" />
   <div id="divprogress">
      <center>
         <img id="imgloading" alt="" src="/memo/images/tloader.gif" style="margin-top: 100px" /></center>
   </div>
   <div id="sizingTests">
      <table style="font-size: 12px">
         <tr>
            <td>
               <span id="judul"></span>
            </td>
            <td>
               <asp:DropDownList ID="ddlYear" runat="server">
                  <asp:ListItem>2010</asp:ListItem>
                  <asp:ListItem>2011</asp:ListItem>
                  <asp:ListItem>2012</asp:ListItem>
               </asp:DropDownList>
            </td>
            <td>
               Bulan
            </td>
            <td>
               <asp:DropDownList ID="ddlMonth" runat="server">
                  <asp:ListItem Value="1">January</asp:ListItem>
                  <asp:ListItem Value="2">February</asp:ListItem>
                  <asp:ListItem Value="3">March</asp:ListItem>
                  <asp:ListItem Value="4">April</asp:ListItem>
                  <asp:ListItem Value="5">May</asp:ListItem>
                  <asp:ListItem Value="6">June</asp:ListItem>
                  <asp:ListItem Value="7">July</asp:ListItem>
                  <asp:ListItem Value="8">August</asp:ListItem>
                  <asp:ListItem Value="9">September</asp:ListItem>
                  <asp:ListItem Value="10">October</asp:ListItem>
                  <asp:ListItem Value="11">November</asp:ListItem>
                  <asp:ListItem Value="12">Desember</asp:ListItem>
               </asp:DropDownList>
            </td>
            <td>
               <input type="button" id="btnRefresh" value="Refresh" onclick="execRefresh();" />
            </td>
         </tr>
      </table>
      <table align="center">
         <tr align="center"><td colspan="2" id="judultabel" style="font-weight: bold;text-decoration:underline;font-size:14px"></td></tr>
         <tr align="center">
            <td>
               <span id="judulwig1" class="judulwig"></span>
            </td>
            <td>
               <span id="judulwig2" class="judulwig"></span>
            </td>
            <% If Session("Permission") = "2" Or Session("Permission") = "6" Or Session("Permission") = "7" Or Session("Permission") = "A" Then%>
            <td>
               <span id="judulwig3" class="judulwig"></span>
            </td>
            <% End If%>
         </tr>
         <tr>
            <td align="center">
               <canvas id="WIG1" width="225" height="225"></canvas>
            </td>
            <td align="center">
               <canvas id="WIG2" width="225" height="225"></canvas>
            </td>
            <% If Session("Permission") = "2" Or Session("Permission") = "6" Or Session("Permission") = "7" Or Session("Permission") = "A" Then%>
            <td align="center">
               <canvas id="WIG3" width="230" height="230"></canvas>
            </td>
            <% End If%>
         </tr>
         <tr>
            <td>
               <span id="footerwig1" class="footerwig"></span>
            </td>
            <td>
               <span id="footerwig2" class="footerwig"></span>
            </td>
            <% If Session("Permission") = "2" Or Session("Permission") = "6" Or Session("Permission") = "7" Or Session("Permission") = "A" Then%>
            <td>
               <span id="footerwig3" class="footerwig"></span>
            </td>
            <% End If%>
         </tr>
      </table>
      <table align="center">
         <tr align="center">
            <td>
               <span id="judullead1" class="judullead"></span>
            </td>
            <td>
               <span id="judullead2" class="judullead"></span>
            </td>
            <td>
               <span id="judullead3" class="judullead"></span>
            </td>
            <td>
               <span id="judullead4" class="judullead"></span>
            </td>
            <td>
               <span id="judullead5" class="judullead"></span>
            </td>
            <td>
               <span id="judullead6" class="judullead"></span>
            </td>
         </tr>
         <tr align="center">
            <td align="center">
               <canvas id="LEAD1" width="120" height="120"></canvas>
            </td>
            <td align="center">
               <canvas id="LEAD2" width="120" height="120"></canvas>
            </td>
            <td align="center">
               <canvas id="LEAD3" width="120" height="120"></canvas>
            </td>
            <td align="center">
               <canvas id="LEAD4" width="120" height="120"></canvas>
            </td>
            <td align="center">
               <canvas id="LEAD5" width="120" height="120"></canvas>
            </td>
            <td align="center">
               <canvas id="LEAD6" width="120" height="120"></canvas>
            </td>
         </tr>
         <tr align="center">
            <td>
               <span id="footerlead1" class="footerlead"></span>
            </td>
            <td>
               <span id="footerlead2" class="footerlead"></span>
            </td>
            <td>
               <span id="footerlead3" class="footerlead"></span>
            </td>
            <td>
               <span id="footerlead4" class="footerlead"></span>
            </td>
            <td>
               <span id="footerlead5" class="footerlead"></span>
            </td>
            <td>
               <span id="footerlead6" class="footerlead"></span>
            </td>
         </tr>
      </table>
   </div>

   <script type="text/javascript">

      // Helper to execute a function after the window is loaded,
      // see http://www.google.com/search?q=addLoadEvent
      function addLoadEvent(func) {
         var oldonload = window.onload;
         if (typeof window.onload != 'function') {
            window.onload = func;
         } else {
            window.onload = function() {
               if (oldonload) {
                  oldonload();
               }
               func();
            }
         }
      }

      function execRefresh() {
         if (document.getElementById("ctl00_ContentPlaceHolder1_permission").value == "1" || document.getElementById("ctl00_ContentPlaceHolder1_permission").value == "B" || document.getElementById("ctl00_ContentPlaceHolder1_permission").value == "3" || document.getElementById("ctl00_ContentPlaceHolder1_permission").value == "4" || document.getElementById("ctl00_ContentPlaceHolder1_permission").value == "5" || document.getElementById("ctl00_ContentPlaceHolder1_permission").value == "8" || document.getElementById("ctl00_ContentPlaceHolder1_permission").value == "9") {
            document.getElementById("judul").innerHTML = "Performance JResources Nusantara Tahun ";
            getDataKPIHO('0', document.getElementById("ctl00_ContentPlaceHolder1_ddlYear").value, document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value, '123');
         }
         else {
            document.getElementById("judul").innerHTML = "Performance Jobsite " + document.getElementById("ctl00_ContentPlaceHolder1_jobsite").value + " Tahun ";
            getDataKPIHO('3', document.getElementById("ctl00_ContentPlaceHolder1_ddlYear").value, document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value, document.getElementById("ctl00_ContentPlaceHolder1_jobsite").value);
         }
      }

      addLoadEvent(function() {
         var options, gm, gms, testOverflowMin, testOverflowMax, periodicRand;
         if (document.getElementById("ctl00_ContentPlaceHolder1_permission").value == "1" || document.getElementById("ctl00_ContentPlaceHolder1_permission").value == "B" || document.getElementById("ctl00_ContentPlaceHolder1_permission").value == "3" || document.getElementById("ctl00_ContentPlaceHolder1_permission").value == "4" || document.getElementById("ctl00_ContentPlaceHolder1_permission").value == "5" || document.getElementById("ctl00_ContentPlaceHolder1_permission").value == "8" || document.getElementById("ctl00_ContentPlaceHolder1_permission").value == "9") {
             document.getElementById("judul").innerHTML = "Performance JResources Nusantara Tahun ";
            getDataKPIHO('0', document.getElementById("ctl00_ContentPlaceHolder1_ddlYear").value, document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value, '123');
         }
         else {
            document.getElementById("judul").innerHTML = "Performance Jobsite " + document.getElementById("ctl00_ContentPlaceHolder1_jobsite").value + " Tahun ";
            getDataKPIHO('3', document.getElementById("ctl00_ContentPlaceHolder1_ddlYear").value, document.getElementById("ctl00_ContentPlaceHolder1_ddlMonth").value, document.getElementById("ctl00_ContentPlaceHolder1_jobsite").value);
         }
      });

      function getDataKPIHO(level, year, month, jobsite) {
         // Draw the gauge using custom settings
         document.getElementById("divprogress").style.visibility = 'visible';
         var selObj = document.getElementById('ctl00_ContentPlaceHolder1_ddlMonth');
         var selIndex = selObj.selectedIndex;
         document.getElementById("judultabel").innerText = document.getElementById("judul").innerText + '' + document.getElementById("ctl00_ContentPlaceHolder1_ddlYear").value + ' bulan ' + selObj[selIndex].innerText;
         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/getDataKPIFull",
            data: "{'level':" + level + ",'year':" + year + ",'month':" + month + ",'jobsite':" + JSON.stringify(jobsite) + "}",
            dataType: "json",
            success: function(res) {
               document.getElementById("divprogress").style.visibility = 'hidden';
               var result = res.d;
               var wcounter = 1;
               var lcounter = 1;
               if (res.d.split("#~#")[0] == "SUCCESS") {
                  for (i = 1; i <= res.d.split("#~#").length - 2; i++) {
                     if (res.d.split("#~#")[i].split("~#~")[0] == "W") {
                        document.getElementById("judulwig" + wcounter).innerHTML = res.d.split("#~#")[i].split("~#~")[1];
                        document.getElementById("footerwig" + wcounter).innerHTML = "Plan :" + res.d.split("#~#")[i].split("~#~")[2] + " Actual :" + res.d.split("#~#")[i].split("~#~")[3];
                        options = {
                           value: parseFloat(res.d.split("#~#")[i].split("~#~")[4]),
                           unitsLabel: ' %',
                           min: 0,
                           max: 105,
                           majorTicks: 2,
                           minorTicks: 20, // small ticks inside each major tick
                           bands: [{ color: '#FF0000', from: 0, to: 15 },
	                                { color: '#FF1414', from: 15, to: 30 },
	                                { color: '#FF2828', from: 30, to: 45 },
	                                { color: '#FF3D3D', from: 45, to: 60 },
	                                { color: '#FF5151', from: 60, to: 75 },
	                                { color: '#FF6666', from: 75, to: 90 },
	                                { color: '#FFFF00', from: 90, to: 95 },
	                                { color: '#00FF00', from: 95, to: 100 },
	                                { color: '#00FFFF', from: 100, to: 105 }
	                                ]
                        };
                        new Gauge(document.getElementById('WIG' + wcounter), options);
                        wcounter += 1;
                     } else {
                        document.getElementById("judullead" + lcounter).innerHTML = res.d.split("#~#")[i].split("~#~")[1];
                        document.getElementById("footerlead" + lcounter).innerHTML = "Plan :" + res.d.split("#~#")[i].split("~#~")[2] + " Actual :" + res.d.split("#~#")[i].split("~#~")[3];
                        options = {
                           value: parseFloat(res.d.split("#~#")[i].split("~#~")[4]),
                           unitsLabel: ' %',
                           min: 0,
                           max: 105,
                           majorTicks: 2,
                           minorTicks: 20, // small ticks inside each major tick
                           bands: [{ color: '#FF0000', from: 0, to: 15 },
	                                { color: '#FF1414', from: 15, to: 30 },
	                                { color: '#FF2828', from: 30, to: 45 },
	                                { color: '#FF3D3D', from: 45, to: 60 },
	                                { color: '#FF5151', from: 60, to: 75 },
	                                { color: '#FF6666', from: 75, to: 90 },
	                                { color: '#FFFF00', from: 90, to: 95 },
	                                { color: '#00FF00', from: 95, to: 100 },
	                                { color: '#00FFFF', from: 100, to: 105 }
	                                ]
                        };
                        new Gauge(document.getElementById('LEAD' + lcounter), options);
                        lcounter += 1;
                     }
                  }
               } else {
                  new Gauge(document.getElementById('WIG1'), null);
                  document.getElementById("judulwig1").innerHTML = "";
                  document.getElementById("footerwig1").innerHTML = "";
                  new Gauge(document.getElementById('WIG2'), null);
                  document.getElementById("judulwig2").innerHTML = "";document.getElementById("footerwig2").innerHTML = "";
                  if (document.getElementById('WIG3') != null) { new Gauge(document.getElementById('WIG3'), null); document.getElementById("judulwig3").innerHTML = ""; document.getElementById("footerwig3").innerHTML = ""; }
                  new Gauge(document.getElementById('LEAD1'), null);
                  document.getElementById("judullead1").innerHTML = ""
                  document.getElementById("footerlead1").innerHTML = "";
                  new Gauge(document.getElementById('LEAD2'), null);
                  document.getElementById("judullead2").innerHTML = ""
                  document.getElementById("footerlead2").innerHTML = "";
                  new Gauge(document.getElementById('LEAD3'), null);
                  document.getElementById("judullead3").innerHTML = ""
                  document.getElementById("footerlead3").innerHTML = "";
                  new Gauge(document.getElementById('LEAD4'), null);
                  document.getElementById("judullead4").innerHTML = ""
                  document.getElementById("footerlead4").innerHTML = "";
                  new Gauge(document.getElementById('LEAD5'), null);
                  document.getElementById("judullead5").innerHTML = ""
                  document.getElementById("footerlead5").innerHTML = "";
                  new Gauge(document.getElementById('LEAD6'), null);
                  document.getElementById("judullead6").innerHTML = ""
                  document.getElementById("footerlead6").innerHTML = "";
               }
            },
            error: function(err) {
               document.getElementById("divprogress").style.visibility = 'hidden';
               alert(err.responseText);
            }
         });
      }
	   
   </script>

</asp:Content>
