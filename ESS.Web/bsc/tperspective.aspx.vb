﻿Imports System.Data.SqlClient
Imports System.Web

Partial Public Class tperspective
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") = "" Or Session("permission") = Nothing Then
            Response.Redirect("../Login.aspx", False)
            Exit Sub
        ElseIf ("1;B").IndexOf(Session("permission")) = -1 Then
            Response.Redirect("../default.aspx", False)
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            For Each x As ListItem In ddlYear.Items
                If x.Text = Date.Now.Year Then
                    x.Selected = True
                End If
            Next
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim dr As SqlDataReader = Nothing
            Try
                Dim l As String = Request.QueryString("l")
                Dim y As String = Request.QueryString("y")
                Dim sqlQuery As String = ""
                Dim cmd As SqlCommand

                conn.Open()
                'Initialize Function
                sqlQuery = "select ID,FunctionName,active from MFunction order by dirid,id"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Then 'GRUP MDV
                        KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                        'If Session("bscdivisi") = dr(0) Then
                        '    KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                        'End If
                    ElseIf Session("permission") = "8" Then 'PIC PDCA
                        If Session("bscdivisi") = dr(0) Then
                            KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Direktorat
                sqlQuery = "select ID,DirektoratName,active from MDirektorat where active='1' order by ID"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Then 'GRUP MDV
                        KPIDirektorat.Items.Add(New ListItem(dr(1), dr(0)))
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Section
                sqlQuery = "select ID,SectionName,active,funcid from MSection order by ID"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Then 'GRUP MDV
                        'KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                        If Session("bscdivisi") = dr(3) Then
                            KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    ElseIf Session("permission") = "7" Then 'GRUP S HEAD
                        If Session("bscdepar") = dr(0) Then
                            KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    ElseIf Session("permission") = "8" Then 'PIC PDCA
                        If Session("bscdivisi") = dr(3) Then
                            KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Jobsite
                sqlQuery = "select id,kdsite,active from site order by id"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "8" Or Session("permission") = "B" Then 'GRUP MDV & PIC PDCA
                        KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "7" Then 'GRUP S HEAD
                        If Session("jobsite") = dr(1) Then
                            KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                    'If dr(2) = 0 Then
                    '    KPIJobsite.Items(KPIJobsite.Items.Count - 1).Enabled = False
                    'End If
                End While
                dr.Close()
                dr = Nothing

                'Isi KPI Level
                KPILevel.Items.Clear()
                Select Case Session("permission")
                    Case "B"
                        KPILevel.Items.Add(New ListItem("JRN / Jobsite", "0"))
                        KPILevel.Items.Add(New ListItem("Direktorat", "4"))
                        KPILevel.Items.Add(New ListItem("Function", "1"))
                    Case "1"
                        KPILevel.Items.Add(New ListItem("JRN / Jobsite", "0"))
                        KPILevel.Items.Add(New ListItem("Direktorat", "4"))
                        KPILevel.Items.Add(New ListItem("Function", "1"))
                End Select
                'Selected Index Change
                Select Case KPILevel.SelectedValue
                    Case 0
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                        KPIDirektorat.Visible = False
                    Case 1
                        KPIFunc.Visible = True
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                        KPIDirektorat.Visible = False
                    Case 4
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                        KPIDirektorat.Visible = True
                End Select
            Catch ex As Exception
                lblMessage.Text = ex.Message
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        End If
    End Sub

    Protected Sub KPILevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles KPILevel.SelectedIndexChanged
        Try
            Select Case KPILevel.SelectedValue
                Case 0
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                    KPIDirektorat.Visible = False
                Case 1
                    KPIFunc.Visible = True
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                    KPIDirektorat.Visible = False
                Case 2
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = True
                    KPIDirektorat.Visible = False
                Case 3
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = False
                    KPIDirektorat.Visible = False
                Case 4
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                    KPIDirektorat.Visible = True
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub

End Class