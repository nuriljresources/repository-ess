﻿Partial Public Class MyIssueList
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("otorisasi") = "" Or Session("otorisasi") = Nothing Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If
        HNikUser.Value = Session("NikUser")
        HPermission.Value = Session("permission")
    End Sub
End Class