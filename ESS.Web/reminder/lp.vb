﻿Imports System.Data
Imports System.Data.SqlClient

Public Class lp
    Public Shared lptyp As String
    Public Shared lpsubject As String
    Public Shared lpentrydt As String
    Public Shared lpsupplier As String
    Public Shared lpreminder As String
    Public Shared lpremday As String

    Public Function Fetch() As DataTable
        Dim myDataTable As DataTable = New DataTable()

        Dim myDataColumn As DataColumn

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "id"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "no"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "pl_name"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "permit_no"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "dt_issued"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "dt_expired"
        myDataTable.Columns.Add(myDataColumn)

        Return myDataTable
    End Function

    Public Sub addlp(ByVal txtplnamenew As String, ByVal txtpnonew As String, ByVal txtdtissuenew As String, ByVal txtdtexpnew As String, ByVal myTable As DataTable)
        Dim row As DataRow
        row = myTable.NewRow()
        Dim no As String

        If myTable.Rows.Count = "1" And myTable.Rows(0)!No.ToString = "1" Then
            no = myTable.Rows.Count + 1
        ElseIf myTable.Rows.Count > "1" Then
            no = myTable.Rows.Count + 1
        Else
            no = myTable.Rows.Count
        End If

        row("No") = no
        row("pl_name") = txtplnamenew
        row("permit_no") = txtpnonew
        row("dt_issued") = txtdtissuenew
        row("dt_expired") = txtdtexpnew

        myTable.Rows.Add(row)
    End Sub

    Public Sub Updatelp(ByVal txtplnameedit As String, ByVal txtpnoedit As String, ByVal txtdtissueedit As String, ByVal txtdtexpedit As String, ByVal myTable As DataTable, ByVal baris As String)
        myTable.Rows(baris)!pl_name = txtplnameedit
        myTable.Rows(baris)!permit_no = txtpnoedit
        myTable.Rows(baris)!dt_issued = txtdtissueedit
        myTable.Rows(baris)!dt_expired = txtdtexpedit
    End Sub

    Public Sub Delete(ByVal myTable As DataTable, ByVal baris As String)

        myTable.Rows(baris).Delete()
        myTable.AcceptChanges()
    End Sub
End Class
