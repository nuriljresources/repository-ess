﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="cetak_spl.aspx.vb" Inherits="EXCELLENT.cetak_spl" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" Height="484px" Width="1047px">
            <LocalReport ReportPath="Laporan\cetak_spl.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="spl_H_H110" />
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" 
                        Name="SPLDataSet_H_H110" />
                </DataSources>
            </LocalReport>
            
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
            DeleteMethod="Delete" InsertMethod="Insert" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            TypeName="EXCELLENT.SPLDataSetTableAdapters.H_H110TableAdapter" 
            UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_NIK" Type="String" />
                <asp:Parameter Name="Original_Jam_start" Type="DateTime" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Periode" Type="String" />
                <asp:Parameter DbType="Guid" Name="RecordID" />
                <asp:Parameter Name="Tanggal" Type="DateTime" />
                <asp:Parameter Name="Jam_end" Type="DateTime" />
                <asp:Parameter Name="approval" Type="String" />
                <asp:Parameter Name="Kdsite" Type="String" />
                <asp:Parameter Name="KdDepar" Type="String" />
                <asp:Parameter Name="KdJabat" Type="String" />
                <asp:Parameter Name="FDayOff" Type="Byte" />
                <asp:Parameter Name="FixOTActual" Type="Decimal" />
                <asp:Parameter Name="SPLActual" Type="Decimal" />
                <asp:Parameter Name="FOTActual" Type="Decimal" />
                <asp:Parameter Name="OTOverwork" Type="Decimal" />
                <asp:Parameter Name="OTCalculated" Type="Decimal" />
                <asp:Parameter Name="OTApproved" Type="Decimal" />
                <asp:Parameter Name="OTPaid1" Type="Decimal" />
                <asp:Parameter Name="OTPaid2" Type="Decimal" />
                <asp:Parameter Name="OTPaid3" Type="Decimal" />
                <asp:Parameter Name="OTPaid4" Type="Decimal" />
                <asp:Parameter Name="OTPaid" Type="Decimal" />
                <asp:Parameter Name="RpOTPaid" Type="Decimal" />
                <asp:Parameter Name="OtherPayment" Type="Decimal" />
                <asp:Parameter Name="TotalPayment" Type="Decimal" />
                <asp:Parameter Name="Remarks" Type="String" />
                <asp:Parameter Name="DayOffName" Type="String" />
                <asp:Parameter Name="FStatus" Type="Byte" />
                <asp:Parameter Name="CreatedBy" Type="String" />
                <asp:Parameter Name="CreatedIn" Type="String" />
                <asp:Parameter Name="CreatedTime" Type="DateTime" />
                <asp:Parameter Name="ModifiedBy" Type="String" />
                <asp:Parameter Name="ModifiedIn" Type="String" />
                <asp:Parameter Name="ModifiedTime" Type="DateTime" />
                <asp:Parameter Name="stedit" Type="String" />
                <asp:Parameter Name="app1" Type="String" />
                <asp:Parameter Name="app2" Type="String" />
                <asp:Parameter Name="checkapp1" Type="String" />
                <asp:Parameter Name="checkapp2" Type="String" />
                <asp:Parameter Name="noreg" Type="String" />
                <asp:Parameter Name="requestby" Type="String" />
                <asp:Parameter Name="Original_NIK" Type="String" />
                <asp:Parameter Name="Original_Jam_start" Type="DateTime" />
            </UpdateParameters>
            <SelectParameters>
                <asp:SessionParameter Name="noreg" SessionField="splrecordid" Type="String" />
            </SelectParameters>
            <InsertParameters>
                <asp:Parameter Name="Periode" Type="String" />
                <asp:Parameter DbType="Guid" Name="RecordID" />
                <asp:Parameter Name="NIK" Type="String" />
                <asp:Parameter Name="Tanggal" Type="DateTime" />
                <asp:Parameter Name="Jam_start" Type="DateTime" />
                <asp:Parameter Name="Jam_end" Type="DateTime" />
                <asp:Parameter Name="approval" Type="String" />
                <asp:Parameter Name="Kdsite" Type="String" />
                <asp:Parameter Name="KdDepar" Type="String" />
                <asp:Parameter Name="KdJabat" Type="String" />
                <asp:Parameter Name="FDayOff" Type="Byte" />
                <asp:Parameter Name="FixOTActual" Type="Decimal" />
                <asp:Parameter Name="SPLActual" Type="Decimal" />
                <asp:Parameter Name="FOTActual" Type="Decimal" />
                <asp:Parameter Name="OTOverwork" Type="Decimal" />
                <asp:Parameter Name="OTCalculated" Type="Decimal" />
                <asp:Parameter Name="OTApproved" Type="Decimal" />
                <asp:Parameter Name="OTPaid1" Type="Decimal" />
                <asp:Parameter Name="OTPaid2" Type="Decimal" />
                <asp:Parameter Name="OTPaid3" Type="Decimal" />
                <asp:Parameter Name="OTPaid4" Type="Decimal" />
                <asp:Parameter Name="OTPaid" Type="Decimal" />
                <asp:Parameter Name="RpOTPaid" Type="Decimal" />
                <asp:Parameter Name="OtherPayment" Type="Decimal" />
                <asp:Parameter Name="TotalPayment" Type="Decimal" />
                <asp:Parameter Name="Remarks" Type="String" />
                <asp:Parameter Name="DayOffName" Type="String" />
                <asp:Parameter Name="FStatus" Type="Byte" />
                <asp:Parameter Name="CreatedBy" Type="String" />
                <asp:Parameter Name="CreatedIn" Type="String" />
                <asp:Parameter Name="CreatedTime" Type="DateTime" />
                <asp:Parameter Name="ModifiedBy" Type="String" />
                <asp:Parameter Name="ModifiedIn" Type="String" />
                <asp:Parameter Name="ModifiedTime" Type="DateTime" />
                <asp:Parameter Name="stedit" Type="String" />
                <asp:Parameter Name="app1" Type="String" />
                <asp:Parameter Name="app2" Type="String" />
                <asp:Parameter Name="checkapp1" Type="String" />
                <asp:Parameter Name="checkapp2" Type="String" />
                <asp:Parameter Name="noreg" Type="String" />
                <asp:Parameter Name="requestby" Type="String" />
            </InsertParameters>
        </asp:ObjectDataSource>
    </div>
    </form>
</body>
</html>
