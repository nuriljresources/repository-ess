﻿Imports System.Data.SqlClient

Partial Public Class popuptrans
    Inherits System.Web.UI.Page

    Private Sub popuptrans_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim param As String = Request.QueryString("p")
            Dim viewtype As String = param.Split(";")(0)
            Dim year As String = param.Split(";")(1)
            Dim month As Integer = param.Split(";")(2)
            Dim level As Integer = param.Split(";")(3)
            Dim func As String = ""
            Dim jobsite As String = ""
            Dim section As String = ""
            Dim sumach As Decimal = 0
            Dim sumpach As Decimal = 0
            Dim pengurangrow As Integer = 0

            Dim totkpi As Decimal = 0
            Dim bobot As Decimal = 0
            Dim counter As Integer = 0
            Dim totach As Decimal
            Dim ytdach As String = ""
            Dim funcname As String
            Dim secname As String
            Dim jobsname As String

            Select Case viewtype
                Case "0" 'MTD
                    Dim sqlQuery As String = ""
                    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
                    Dim beforeyear As String
                    Dim beforemonth As String
                    Dim cmd As SqlCommand
                    Dim approved As Integer = 0
                    Dim p As Decimal = 0
                    If month = 1 Then
                        beforeyear = year - 1
                        beforemonth = 12
                    Else
                        beforeyear = year
                        beforemonth = month - 1
                    End If

                    Select Case level
                        Case "0"
                            sqlQuery = "Select count(*) from TBSCAPPR where level='0' and month ='" & month & "' and year='" & year & "'"
                        Case "1"
                            func = param.Split(";")(4)
                            cmd = New SqlCommand("select FunctionName from MFUNCTION where ID='" & Replace(func, "'", "") & "'", conn)
                            conn.Open()
                            funcname = cmd.ExecuteScalar()
                            conn.Close()
                            sqlQuery = "Select count(*) from TBSCAPPR where level='1' and month ='" & month & "' and year='" & year & "' and func='" & func & "'"
                        Case "2"
                            jobsite = param.Split(";")(4)
                            section = param.Split(";")(5)
                            cmd = New SqlCommand("select SectionName from MSECTION where ID='" & Replace(section, "'", "") & "'", conn)
                            conn.Open()
                            secname = cmd.ExecuteScalar()
                            conn.Close()
                            cmd = New SqlCommand("select kdsite from SITE where ID='" & Replace(jobsite, "'", "") & "'", conn)
                            conn.Open()
                            jobsname = cmd.ExecuteScalar()
                            conn.Close()
                            sqlQuery = "Select count(*) from TBSCAPPR where level='2' and month ='" & month & "' and year='" & year & "' and jobsite='" & jobsite & "' and section='" & section & "'"
                        Case "3"
                            jobsite = param.Split(";")(4)
                            cmd = New SqlCommand("select kdsite from SITE where ID='" & Replace(jobsite, "'", "") & "'", conn)
                            conn.Open()
                            jobsname = cmd.ExecuteScalar()
                            conn.Close()
                            sqlQuery = "Select count(*) from TBSCAPPR where level='3' and month ='" & month & "' and year='" & year & "' and jobsite='" & jobsite & "'"
                    End Select

                    cmd = New SqlCommand(sqlQuery, conn)
                    conn.Open()
                    approved = cmd.ExecuteScalar
                    conn.Close()

                    sqlQuery = "select a.year,a.month,a.kpiid as id,b.Level,'' as workunit,d.STName as st,c.SO,isnull(e.jabatan,'-') as soleader," & _
                            "b.Name as kpi,b.Owner as kpiowner,b.UoM,b.Formula,b.RedParam,b.GreenParam," & IIf(approved = 0, "isnull(dd." & GetMonth(month) & ",'-')", "isnull(cast(a.target as varchar(60)),'-')") & " as target,a.actual as actual,isnull(a.achievement,0) as achievement, " & _
                            "ff.kpiid,isnull(ff.achievement,-9999) as oldach,p.perspective,isnull(ff.actual,-9999) as oldact " & _
                            ",isnull(b.RedCond,'0') as RedCond,isnull(b.GreenCond,'0') as GreenCond,b.calctype,b.basedon, isnull(tp.value,0) as bobot, (select count(Nomor) from dbo.TBSCLAMPIRAN l where l.kpiid=a.kpiid) as jmlhattachment, " & _
                            "isnull(dd." & GetMonth(month) & ",'-') as ttarget, isnull(cast(a.target as varchar(60)),'-') as atarget, cc.*, a.SAPFlag, b.StEdit " & _
                            "from tbsc a inner join MKPI b on a.kpiid = b.id " & _
                            "inner join MSO c on b.SOID =c.ID  " & _
                            "inner join MST d on c.STID = d.STID  " & _
                            "left join MJABATAN e on e.id = c.BUMALeader  " & _
                            "left join CALCCONTENT cc on b.id=cc.kpiid and cc.calctype=b.calctype " & _
                            "left join " & _
                            "(select kpiid,achievement,actual from TBSC t where t.year='" & beforeyear & "' and t.month='" & beforemonth & "') ff on ff.kpiid =a.kpiid  left join mperspective p on d.PRID=p.id " & _
                            "left join " & _
                            "(select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & year & "' group by kpiid,year) b on a.versi=b.versi and a.kpiid = b.kpiid where a.year='" & year & "')dd on a.kpiid=dd.kpiid  " & _
                            "left join tperspective tp on p.id=tp.pid and tp.year=a.year and tp.level=" & getMapLevel(level) & IIf(level = 0 Or level = 3, "", IIf(level = "1", " and tp.funcid=b.func ", " and tp.funcid= (select funcid from msection where id=b.section) ")) & _
                            "where a.year='" & year & "' and a.month='" & month & "' and b.Level='" & level & "'  and b.stedit='0' "

                    Select Case level
                        Case "0"
                            sqlQuery += " order by p.id,d.stid,c.ID,b.name"
                        Case "1"
                            sqlQuery += " and b.func='" & func & "' order by p.id,d.stid,c.ID,b.name"
                        Case "2"
                            sqlQuery += " and b.jobsite='" & jobsite & "' and b.section='" & section & "' order by p.id,d.stid,c.ID,b.name"
                        Case "3"
                            sqlQuery += " and b.jobsite='" & jobsite & "' order by p.id,d.stid,c.ID,b.name"
                    End Select

                    Dim da As New SqlDataAdapter(sqlQuery, conn)
                    Dim dt As New DataTable()
                    da.Fill(dt)

                    '-----------------------------------
                    Dim mytableresult As String = ""
                    Dim perspective As String = ""
                    Dim perspectiveid As String = ""
                    Dim totperspective As Decimal = 0
                    'Dim perspectiveid as string = ""
                    Dim bobotbefore As Decimal = 0
                    Dim so As String = ""
                    Dim socount As Integer = 1
                    Dim row As Integer = 1
                    Dim ach As Decimal = 0


                    If dt.Rows.Count = 0 Then
                        lblError.Text = "No Record Found"
                    Else
                        Select Case level
                            Case "0"
                                result.InnerHtml = "<table style='width:100%'><tr align='center'><td style='width:90%;font-weight:bold;font-size:22px;'>" & "BUMA PERFORMANCE MTD " & GetFullMonth(month).ToUpper() & " " & year & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                            Case "1"
                                result.InnerHtml = "<table style='width:100%'><tr align='center'><td style='width:90%;font-weight:bold;font-size:22px;'>" & funcname & " PERFORMANCE MTD " & GetFullMonth(month).ToUpper() & " " & year & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                            Case "2"
                                result.InnerHtml = "<table style='width:100%'><tr align='center'><td style='width:90%;font-weight:bold;font-size:22px;'>" & secname & " " & jobsname & " PERFORMANCE MTD " & GetFullMonth(month).ToUpper() & " " & year & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                            Case "3"
                                result.InnerHtml = "<table style='width:100%'><tr align='center'><td style='width:90%;font-weight:bold;font-size:22px;'>" & jobsname & " PERFORMANCE MTD " & GetFullMonth(month).ToUpper() & " " & year & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                        End Select
                    End If
                    mytableresult += "<div style='font-weight:bold;font-size:18px;'>@@@</div>"
                    mytableresult += "<table border='1' id='BSCTABLE' class='bsc' style='width:100%;font-size:11px'>"
                    mytableresult += "<thead class='headbsctable'><td style='width:18%' align='center'>Strategic Objective</td><td style='width:30%' align='center'>KPI</td><td align='center' style='width:7%'>KPI Owner</td><td align='center'>UoM</td>" & _
                                     "<td align='center'>Target</td><td  align='center'>Actual</td><td  align='center'>Achievement <br> (Act Vs Target)</td><td align='center'>Trend</td></thead>"
                    For Each dr As DataRow In dt.Rows
                        If perspective <> dr("perspective") Then
                            perspective = dr("perspective") '<div id='p" & row & "'>VAL</div>
                            If totkpi <> 0 Then
                                sumpach += sumach / totkpi
                                counter += 1
                            End If
                            If counter <> 0 Then
                                mytableresult = mytableresult.Replace("###", System.Math.Round(sumpach / counter, 2))
                                mytableresult = mytableresult.Replace("$$$", System.Math.Round(sumpach / counter * bobot / 100, 2) & " %")
                                totach += sumpach / counter * bobot / 100
                            Else
                                mytableresult = mytableresult.Replace("###", "")
                                mytableresult = mytableresult.Replace("$$$", "")
                                totach += 0
                            End If
                            If totkpi <> 0 Then
                                mytableresult = mytableresult.Replace("VAL", System.Math.Round(sumach / totkpi, 2) & " %")
                            Else
                                mytableresult = mytableresult.Replace("VAL", "-")
                            End If
                            If dr("target") <> "-" And dr("target") <> "" And dr("StEdit") <> "1" Then
                                perspectiveid = dr("perspective")
                                If totkpi <> 0 Then
                                    'totperspective += dr("bobot")
                                    totperspective += bobotbefore
                                End If
                            End If
                            mytableresult += "<tr><td colspan='12' align='center' style='background-color:#EACB92;'>" & perspective & "<br> ### % X " & System.Math.Round(dr("bobot"), 2) & " &nbsp; = $$$</td></tr>"
                            counter = 0
                            sumpach = 0
                            totkpi = 0
                            bobot = dr("bobot")
                        End If
                        If perspectiveid <> dr("perspective") Then
                            If dr("target") <> "-" And dr("target") <> "" And dr("StEdit") <> "1" Then
                                perspectiveid = dr("perspective")
                                If totkpi <> 0 Then
                                    'totperspective += dr("bobot")
                                    totperspective += bobotbefore
                                End If
                            End If
                        End If
                        bobotbefore = dr("bobot")
                        mytableresult += "<tr>"

                        '''''''''''''''''''''''''
                        If so <> dr("SO") Then
                            ''''''''''''''''''''''''''''''
                            If totkpi <> 0 Then
                                mytableresult = mytableresult.Replace("VAL", System.Math.Round(sumach / totkpi, 2) & " %")
                                sumpach += sumach / totkpi
                                counter += 1
                            Else
                                mytableresult = mytableresult.Replace("VAL", "-")
                            End If
                            ''''''''''''''''''''''''''''''
                            mytableresult = mytableresult.Replace("x&x", socount)
                            so = dr("SO")
                            socount = 1
                            sumach = 0
                            totkpi = 0
                            mytableresult += "<td rowspan='x&x'>" & Replace(dr("SO"), "'", "`") & "<br/><div style='font-size:16px;text-align:center;padding-top:5px' id='so" & row & "'>VAL</div></td>"
                        Else
                            socount += 1
                        End If
                        '''''''''''''''''''''''''''
                        mytableresult += "<td align='left'>" & Replace(dr("kpi"), """", "`")
                        mytableresult += "</td><td>" & dr("kpiowner") & "</td><td align='center'>" & dr("UoM") & "</td>"

                        If dr("StEdit") = "1" Then
                            mytableresult += "<td><input type='text' style='border-style:none;width:2px' onChange='calc();' id='kpit" & row & "' value='' DISABLED/></td>"
                            mytableresult += "<td align='right'><img src='../images/off.png' style='float:left' alt='OFF'/><input type='text' class='printsap' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' DISABLED style='border:0;'" & "/></td><td style='background:#77FFB6'><input type='hidden' id='hkpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "'/><input type='text' size=3  onChange='calc();' style='width:0px;border-style:none;text-align:right;background:transparent;color:black' id='kpiach" & row & "' value='" & System.Math.Round(dr("achievement"), 2) & "' DISABLED />" & IIf(dr("target") = "", "", "<center>-</center>") & "</td>"
                        Else
                            If dr("target") = "-" Or dr("target") = "" Then
                                mytableresult += "<td>" & dr("target") & "</td>"
                            Else
                                mytableresult += "<td align='right'>" & Format(System.Math.Round(CDec(dr("target")), 2), "#,##0.00") & "</td>"
                            End If
                            If dr("target") = "-" Or dr("target") = "" Or IsDBNull(dr("actual")) Then
                                'mytableresult += "<td></td><td align='right'>" & Format(System.Math.Round(dr("achievement"), 2), "#,##0.00") & " %</td>"
                                mytableresult += "<td></td><td align='right'>%</td>"
                            Else
                                If dr("SAPFlag") <> True Then
                                    mytableresult += "<td align='right'>" & Format(System.Math.Round(dr("actual"), 2), "#,##0.00") & "</td><td align='right'>" & Format(System.Math.Round(dr("achievement"), 2), "#,##0.00") & " %</td>"
                                Else
                                    mytableresult += "<td align='right'><table style='width:100%' border='0'><tr><td align='left'><img src='../images/sap.png' alt='SAP'/></td><td>" & Format(System.Math.Round(dr("actual"), 2), "#,##0.00") & "</td></tr></table></td><td align='right'>" & Format(System.Math.Round(dr("achievement"), 2), "#,##0.00") & " %</td>"
                                End If
                                'mytableresult += "<td align='right'>" & IIf(dr("SAPFlag") = True, "<img src='../images/sap.png' alt='SAP'/>&nbsp;&nbsp;&nbsp;", "") & Format(System.Math.Round(dr("actual"), 2), "#,##0.00") & "</td><td align='right'>" & Format(System.Math.Round(dr("achievement"), 2), "#,##0.00") & " %</td>"
                            End If
                        End If

                        If dr("target") = "-" Or dr("target") = "" Or dr("StEdit") = "1" Or IsDBNull(dr("actual")) Then
                            ach = 0
                        Else
                            totkpi += 1
                            sumach += System.Math.Round(dr("achievement"), 2)
                            ach = System.Math.Round(dr("achievement"), 2)
                        End If

                        If dr("target") = "-" Or dr("target") = "" Then
                            mytableresult += "<td align='center'></td>"
                        ElseIf dr("oldact") = -9999 Or IsDBNull(dr("actual")) Then
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "equal.png' style='width:100%'/></td>"
                        ElseIf dr("oldact") > dr("actual") Then
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "down.png' style='width:100%'/></td>"
                        ElseIf dr("oldact") < dr("actual") Then
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "up.png' style='width:100%'/></td>"
                        Else
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "equal.png' style='width:100%'/></td>"
                        End If
                        row += 1

                    Next
                    '''''''''''''''''''''''
                    If totkpi <> 0 Then
                        mytableresult = mytableresult.Replace("VAL", System.Math.Round(sumach / totkpi, 2) & " %")
                        sumpach += sumach / totkpi
                        counter += 1
                    Else
                        mytableresult = mytableresult.Replace("VAL", "-")
                    End If
                    If counter <> 0 Then
                        mytableresult = mytableresult.Replace("###", System.Math.Round(sumpach / counter, 2))
                        mytableresult = mytableresult.Replace("$$$", System.Math.Round(sumpach / counter * bobot / 100, 2) & " %")
                        totach += sumpach / counter * bobot / 100
                    Else
                        mytableresult = mytableresult.Replace("###", "")
                        mytableresult = mytableresult.Replace("$$$", "")
                        totach += 0
                    End If
                    ''''''''''''''''''''''
                    mytableresult = mytableresult.Replace("x&x", socount)
                    mytableresult = mytableresult.Replace("@@@", Request.QueryString("a"))
                    'mytableresult = mytableresult.Replace("@@@", "Total Achievement :" & System.Math.Round(totach / totperspective * 100, 2).ToString() & " %")
                    result.InnerHtml += mytableresult + "</table>"
                    result.InnerHtml += "<br/><table border=""1"" style=""font-size:10px;border-collapse: collapse;border-width: 1px;"">"
                    result.InnerHtml += "<tr><td colspan=""6"">Keterangan</td></tr>"
                    result.InnerHtml += "<tr><td><img src=""../images/greendown.png""/></td><td>pencapaian n > 95%, dan lebih rendah dari bulan lalu</td><td><img src=""../images/greenequal.png""/></td><td>pencapaian n > 95%, dan sama dengan bulan lalu</td><td><img src=""../images/greenup.png""/></td><td>pencapaian n > 95%, dan lebih tinggi dari bulan lalu</td></tr>"
                    result.InnerHtml += "<tr><td><img src=""../images/yellowdown.png""/></td><td>pencapaian 95% > n > 90%, dan lebih rendah dari bulan lalu</td><td><img src=""../images/yellowequal.png""/></td><td>pencapaian 95% > n > 90%, dan sama dengan bulan lalu</td><td><img src=""../images/yellowup.png""/></td><td>pencapaian 95% > n > 90%, dan lebih tinggi dari bulan lalu</td></tr>"
                    result.InnerHtml += "<tr><td><img src=""../images/reddown.png""/></td><td>pencapaian n < 85%, dan lebih rendah dari bulan lalu</td><td><img src=""../images/redequal.png""/></td><td>pencapaian n < 85%, dan sama dengan bulan lalu</td><td><img src=""../images/redup.png""/></td><td>pencapaian n < 85%, dan lebih tinggi dari bulan lalu</td></tr>"
                    result.InnerHtml += "</table>"
                Case "1" 'YEARLY
                    Dim sqlQuery As String = ""
                    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
                    Dim beforeyear As String
                    Dim beforemonth As String
                    Dim cmd As SqlCommand
                    Dim approved As Integer = 0
                    Dim p As Decimal = 0
                    Dim perspectiveid As String = ""
                    Dim totperspective As Decimal = 0
                    If month = 1 Then
                        beforeyear = year - 1
                        beforemonth = 12
                    Else
                        beforeyear = year
                        beforemonth = month - 1
                    End If

                    Select Case level
                        Case "0"
                            sqlQuery = "Select count(*) from TBSCAPPR where level='0' and month ='" & month & "' and year='" & year & "'"
                        Case "1"
                            func = param.Split(";")(4)
                            cmd = New SqlCommand("select FunctionName from MFUNCTION where ID='" & Replace(func, "'", "") & "'", conn)
                            conn.Open()
                            funcname = cmd.ExecuteScalar()
                            conn.Close()
                            sqlQuery = "Select count(*) from TBSCAPPR where level='1' and month ='" & month & "' and year='" & year & "' and func='" & func & "'"
                        Case "2"
                            jobsite = param.Split(";")(4)
                            section = param.Split(";")(5)
                             cmd = New SqlCommand("select SectionName from MSECTION where ID='" & Replace(section, "'", "") & "'", conn)
                            conn.Open()
                            secname = cmd.ExecuteScalar()
                            conn.Close()
                            cmd = New SqlCommand("select kdsite from SITE where ID='" & Replace(jobsite, "'", "") & "'", conn)
                            conn.Open()
                            jobsname = cmd.ExecuteScalar()
                            conn.Close()
                            sqlQuery = "Select count(*) from TBSCAPPR where level='2' and month ='" & month & "' and year='" & year & "' and jobsite='" & jobsite & "' and section='" & section & "'"
                        Case "3"
                            jobsite = param.Split(";")(4)
                             cmd = New SqlCommand("select kdsite from SITE where ID='" & Replace(jobsite, "'", "") & "'", conn)
                            conn.Open()
                            jobsname = cmd.ExecuteScalar()
                            conn.Close()
                            sqlQuery = "Select count(*) from TBSCAPPR where level='3' and month ='" & month & "' and year='" & year & "' and jobsite='" & jobsite & "'"
                    End Select

                    cmd = New SqlCommand(sqlQuery, conn)
                    conn.Open()
                    approved = cmd.ExecuteScalar
                    conn.Close()
                    'sqlQuery = "select a.year,a.month,a.kpiid as id,b.Level,'' as workunit,d.STName as st,c.SO,isnull(e.jabatan,'-') as soleader," & _
                    '            "b.Name as kpi,b.Owner as kpiowner,b.UoM,b.Formula,b.RedParam,b.GreenParam,isnull(cast(a.target as varchar(50)),'-') as target,isnull(cast(a.actual  as varchar(50)),'') as actual,isnull(cast(xxx.target as varchar(60)),'-') as targetytd,xxx.actual as actualytd,isnull(a.achievement,0) as achievement, " & _
                    '            "ff.kpiid,isnull(ff.achievement,-9999) as oldach,p.perspective,isnull(ff.actual,-9999) as oldact,dbo.getTargetString('2011',a.kpiid,1) as targetsekarang " & _
                    '            ",isnull(b.RedCond,'0') as RedCond,isnull(b.GreenCond,'0') as GreenCond,b.calctype,b.basedon, isnull(tp.value,0) as bobot, (select count(Nomor) from dbo.TBSCLAMPIRAN l where l.kpiid=a.kpiid and l.year='" & year & "' and l.month='" & month & "') as jmlhattachment, cc.*,isnull(XXX.targetcalc,'9') as  targetcalc, dbo.getTargetString('" & year & "',a.kpiid," & month & ") as targetsekarang,b.StEdit " & _
                    '            "from tbsc a inner join MKPI b on a.kpiid = b.id " & _
                    '            "inner join MSO c on b.SOID =c.ID  " & _
                    '            "inner join MST d on c.STID = d.STID  " & _
                    '            " left join ( " & _
                    '            "select kpiid,calctype as targetcalc,case(calctype) when '0' then SUM(target) when '1' then AVG(target) end as target,case(calctype) when '0' then SUM(actual) when '1' then AVG(actual) end as actual " & _
                    '            "from( " & _
                    '            "select a.kpiid,a.month,a.target,a.actual,tt.calctype from TBSC a inner join MKPI b on a.kpiid = b.id " & _
                    '            "left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & year & "' group by kpiid,year)  b on a.versi=b.versi and a.kpiid = b.kpiid and a.year='" & year & "') tt on a.kpiid=tt.kpiid " & _
                    '            "where a.year='" & year & "' and a.month <= " & month & " and b.Level='" & level & "' and tt.calctype in ('0','1') " & _
                    '            ")dd group by kpiid,calctype " & _
                    '            "        union " & _
                    '            "select kpiid,calctype as targetcalc,case when calctype is null then null else target end as target,actual as actual from( " & _
                    '            "select ROW_NUMBER() OVER(PARTITION BY a.KPIID ORDER BY a.KPIID,MONTH DESC) as rownum, a.kpiid,a.month,a.target,a.actual,tt.calctype " & _
                    '            "from TBSC a inner join MKPI b on a.kpiid = b.id " & _
                    '            "left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & year & "' group by kpiid,year)  b on a.versi=b.versi and a.kpiid = b.kpiid and a.year='" & year & "') tt on a.kpiid=tt.kpiid  " & _
                    '            "where a.year='" & year & "' and a.month <= " & month & " and b.Level='" & level & "' and isnull(tt.calctype,'3') in ('2','3') " & _
                    '            ")dd where rownum=1 " & _
                    '            ")XXX on a.kpiid=xxx.kpiid " & _
                    '            "left join MJABATAN e on e.id = c.BUMALeader  " & _
                    '            "left join CALCCONTENT cc on b.id=cc.kpiid and cc.calctype=b.calctype " & _
                    '            "left join " & _
                    '            "(select kpiid,achievement,actual from TBSC t where t.year='" & beforeyear & "' and t.month='" & beforemonth & "') ff on ff.kpiid =a.kpiid  left join mperspective p on d.PRID=p.id " & _
                    '            "left join " & _
                    '            "(select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & year & "' group by kpiid,year) b on a.versi=b.versi and a.kpiid = b.kpiid where a.year='" & year & "')dd on a.kpiid=dd.kpiid  " & _
                    '            "left join tperspective tp on p.id=tp.pid and tp.year=a.year and tp.level=" & getMapLevel(level) & IIf(level = 0 Or level = 3, "", IIf(level = "1", " and tp.funcid=b.func ", " and tp.funcid= (select funcid from msection where id=b.section) ")) & _
                    '            "where a.year='" & year & "' and a.month='" & month & "' and b.Level='" & level & "' "

                    sqlQuery = "select a.year,a.month,a.kpiid as id,b.Level,'' as workunit,d.STName as st,c.SO,isnull(e.jabatan,'-') as soleader," & _
                                "b.Name as kpi,b.Owner as kpiowner,b.UoM,b.Formula,b.RedParam,b.GreenParam,isnull(cast(a.target as varchar(50)),'-') as target,isnull(cast(a.actual  as varchar(50)),'') as actual,dd.annual as targetytd,xxx.actual as actualytd,isnull(a.achievement,0) as achievement, " & _
                                "ff.kpiid,isnull(ff.achievement,-9999) as oldach,p.perspective,isnull(ff.actual,-9999) as oldact,dbo.getTargetString('" & year & "',a.kpiid,1) as targetsekarang " & _
                                ",isnull(b.RedCond,'0') as RedCond,isnull(b.GreenCond,'0') as GreenCond,b.calctype,b.basedon, isnull(tp.value,0) as bobot, (select count(Nomor) from dbo.TBSCLAMPIRAN l where l.kpiid=a.kpiid and l.year='" & year & "' and l.month='" & month & "') as jmlhattachment, cc.*,isnull(XXX.targetcalc,'9') as  targetcalc, dbo.getTargetString('" & year & "',a.kpiid," & month & ") as targetsekarang,b.StEdit,XXX.mynull,XXX.mynullt  " & _
                                "from tbsc a inner join MKPI b on a.kpiid = b.id " & _
                                "inner join MSO c on b.SOID =c.ID  " & _
                                "inner join MST d on c.STID = d.STID  " & _
                                " left join ( " & _
                                " select a.*,count(tt.kpiid) as mynullt  from ( " & _
                                "select kpiid,calctype as targetcalc,SUM(MYNULL) as MYNULL ,case(calctype) when '0' then SUM(actual) when '1' then AVG(actual) end as actual from( " & _
                                "select a.kpiid,a.month,case when a.target is null then null else a.actual end as actual,tt.calctype,case when actual is null then 1 else 0 end as MYNULL from TBSC a inner join MKPI b on a.kpiid = b.id left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & year & "' group by kpiid,year)  b on a.versi=b.versi and a.kpiid = b.kpiid and a.year='" & year & "') tt on a.kpiid=tt.kpiid where a.year='" & year & "' and a.month <= " & month & " and b.Level='" & level & "' and tt.calctype in ('0','1') " & _
                                ")dd group by kpiid,calctype  " & _
                                "        union " & _
                                "select kpiid,calctype as targetcalc,MYNULL,actual from( " & _
                                "select ROW_NUMBER() OVER(PARTITION BY a.KPIID ORDER BY a.KPIID,MONTH DESC) as rownum, a.kpiid,a.month,a.actual as actual,tt.calctype,case when actual is null then 1 else 0 end as MYNULL from TBSC a inner join MKPI b on a.kpiid = b.id left join (select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & year & "' group by kpiid,year)  b on a.versi=b.versi and a.kpiid = b.kpiid and a.year='" & year & "') tt on a.kpiid=tt.kpiid  where a.year='" & year & "' and a.month <= " & month & " and b.Level='" & level & "' and isnull(tt.calctype,'3') in ('2','3') )dd where rownum=1 " & _
                                " )a left join TBSC tt on a.kpiid=tt.kpiid and tt.target is null and tt.year='" & year & "'" & _
                                " group by a.kpiid,a.targetcalc,a.MYNULL,a.actual " & _
                                ")XXX on a.kpiid=xxx.kpiid " & _
                                "left join MJABATAN e on e.id = c.BUMALeader  " & _
                                "left join CALCCONTENT cc on b.id=cc.kpiid and cc.calctype=b.calctype " & _
                                "left join " & _
                                "(select kpiid,achievement,actual from TBSC t where t.year='" & beforeyear & "' and t.month='" & beforemonth & "') ff on ff.kpiid =a.kpiid  left join mperspective p on d.PRID=p.id " & _
                                "left join " & _
                                "(select a.* from TTARGET a inner join (select MAX(versi) as versi,kpiid from TTARGET where year='" & year & "' group by kpiid,year) b on a.versi=b.versi and a.kpiid = b.kpiid where a.year='" & year & "')dd on a.kpiid=dd.kpiid  " & _
                                "left join tperspective tp on p.id=tp.pid and tp.year=a.year and tp.level=" & getMapLevel(level) & IIf(level = 0 Or level = 3, "", IIf(level = "1", " and tp.funcid=b.func ", " and tp.funcid= (select funcid from msection where id=b.section) ")) & _
                                "where a.year='" & year & "' and a.month='" & month & "' and b.Level='" & level & "'  and b.stedit='0' "

                    
                    Select Case level
                        Case "0"
                            sqlQuery += " order by p.id,d.stid,c.ID,b.name"
                        Case "1"
                            sqlQuery += " and b.func='" & func & "' order by p.id,d.stid,c.ID,b.name"
                        Case "2"
                            sqlQuery += " and b.jobsite='" & jobsite & "' and b.section='" & section & "' order by p.id,d.stid,c.ID,b.name"
                        Case "3"
                            sqlQuery += " and b.jobsite='" & jobsite & "' order by p.id,d.stid,c.ID,b.name"
                    End Select

                    Dim da As New SqlDataAdapter(sqlQuery, conn)
                    Dim dt As New DataTable()
                    da.Fill(dt)

                    '-----------------------------------
                    Dim mytableresult As String = ""
                    Dim perspective As String = ""
                    Dim so As String = ""
                    Dim socount As Integer = 1
                    Dim row As Integer = 1
                    Dim ach As Decimal = 0


                    If dt.Rows.Count = 0 Then
                        lblError.Text = "No Record Found"
                    Else
                        Select Case level
                            Case "0"
                                result.InnerHtml = "<table style='width:100%'><tr align='center'><td style='width:90%;font-weight:bold;font-size:22px;'>" & "BUMA PERFORMANCE YEARLY " & GetFullMonth(month).ToUpper() & " " & year & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                            Case "1"
                                result.InnerHtml = "<table style='width:100%'><tr align='center'><td style='width:90%;font-weight:bold;font-size:22px;'>" & funcname & " PERFORMANCE YEARLY " & GetFullMonth(month).ToUpper() & " " & year & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                            Case "2"
                                result.InnerHtml = "<table style='width:100%'><tr align='center'><td style='width:90%;font-weight:bold;font-size:22px;'>" & secname & " " & jobsname & " PERFORMANCE YEARLY " & GetFullMonth(month).ToUpper() & " " & year & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                            Case "3"
                                result.InnerHtml = "<table style='width:100%'><tr align='center'><td style='width:90%;font-weight:bold;font-size:22px;'>" & jobsname & " PERFORMANCE YEARLY " & GetFullMonth(month).ToUpper() & " " & year & "</td><td style='width:10%'>" & getApproval(approved) & "</td></tr></table>"
                        End Select
                    End If
                    mytableresult += "<div style='font-weight:bold;font-size:18px;'>@@@</div>"
                    mytableresult += "<table border='1' id='BSCTABLE' class='bsc' style='width:100%;font-size:11px'>"
                    mytableresult += "<thead class='headbsctable'><td style='width:10%' align='center'>Strategic Objective</td><td style='width:30%' align='center'>KPI</td><td align='center' style='width:5%'>KPI Owner</td><td align='center' style='width:5%'>UoM</td>" & _
                                     "<td align='center' style='width:25%'>Target</td><td  align='center' style='width:5%'>Actual</td><td  align='center' style='width:15%'>Achievement <br> (Act Vs Target)</td><td align='center' style='width:5%'>Trend</td></thead>"
                    For Each dr As DataRow In dt.Rows
                        If perspective <> dr("perspective") Then
                            perspective = dr("perspective") '<div id='p" & row & "'>VAL</div>
                            If totkpi <> 0 Then
                                mytableresult = mytableresult.Replace("VAL", System.Math.Round(sumach / totkpi, 2) & " %")
                                sumpach += sumach / totkpi
                                counter += 1
                            End If
                            If counter <> 0 Then
                                mytableresult = mytableresult.Replace("###", System.Math.Round(sumpach / counter, 2))
                                mytableresult = mytableresult.Replace("$$$", System.Math.Round(sumpach / counter * bobot / 100, 2) & " %")
                                totach += sumpach / counter * bobot / 100
                            Else
                                mytableresult = mytableresult.Replace("###", "")
                                mytableresult = mytableresult.Replace("$$$", "")
                                totach += 0
                            End If
                            If dr("target") <> "-" And dr("target") <> "" And dr("StEdit") <> "1" Then
                                perspectiveid = dr("perspective")
                                totperspective += dr("bobot")
                            End If
                            mytableresult += "<tr><td colspan='12' align='center' style='background-color:#EACB92;'>" & perspective & "<br> ### % X " & System.Math.Round(dr("bobot"), 2) & " &nbsp; = $$$</td></tr>"
                            counter = 0
                            sumpach = 0
                            totkpi = 0
                            bobot = dr("bobot")
                        End If
                        If perspectiveid <> dr("perspective") Then
                            If dr("target") <> "-" And dr("target") <> "" And dr("StEdit") <> "1" Then
                                perspectiveid = dr("perspective")
                                totperspective += dr("bobot")
                            End If
                        End If
                        mytableresult += "<tr>"

                        '''''''''''''''''''''''''
                        If so <> dr("SO") Then
                            ''''''''''''''''''''''''''''''
                            If totkpi <> 0 Then
                                mytableresult = mytableresult.Replace("VAL", System.Math.Round(sumach / totkpi, 2) & " %")
                                sumpach += sumach / totkpi
                                counter += 1
                            Else
                                mytableresult = mytableresult.Replace("VAL", "-")
                            End If
                            ''''''''''''''''''''''''''''''
                            mytableresult = mytableresult.Replace("x&x", socount)
                            so = dr("SO")
                            socount = 1
                            sumach = 0
                            totkpi = 0
                            mytableresult += "<td rowspan='x&x'>" & Replace(dr("SO"), "'", "`") & "<br/><div style='font-size:16px;text-align:center;padding-top:5px' id='so" & row & "'>VAL</div></td>"
                        Else
                            socount += 1
                        End If
                        '''''''''''''''''''''''''''
                        mytableresult += "<td align='left'>" & Replace(dr("kpi"), """", "`")
                        mytableresult += "</td><td>" & dr("kpiowner") & "</td><td align='center'>" & dr("UoM") & "</td>"

                        If dr("StEdit") = "1" Then
                            mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='' DISABLED/><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='-' DISABLED/></td>"
                            mytableresult += "<td align='right'><img src='../images/off.png' alt='OFF'/><input type='text' style='border-style:none;width:80%' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' /></td><td style='background:#77FFB6'><input type='text' size=3  onChange='calc();' style='border-style:none;text-align:right;background:transparent;color:black;width:0' id='kpiach" & row & "' value='' READONLY/></td>"
                        Else
                            If IsDBNull(dr("targetytd")) Then
                                mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='-' DISABLED/><span style='font-size:10px;color:gray;font-style:italic;margin:0;padding:0'>please set target</span><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='-' DISABLED/></td>"
                            ElseIf dr("targetytd").ToString() = "-" Then
                                If IsDBNull(dr("targetsekarang")) Then
                                    mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='-' DISABLED/><span style='font-size:10px;color:gray;font-style:italic;margin:0;padding:0'>please set target</span><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='-' DISABLED/></td>"
                                Else
                                    mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='' DISABLED/><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='-' DISABLED/></td>"
                                End If
                            Else
                                If dr("target").ToString() = "-" Then
                                    mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='" & Format(System.Math.Round(CDec(dr("targetytd")), 2), "#,##0.00") & "' DISABLED/><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='-' DISABLED/></td>"
                                Else
                                    mytableresult += "<td><input type='text' style='border-style:none;width:99%' onChange='calc();' id='kpit" & row & "' value='" & Format(System.Math.Round(CDec(dr("targetytd")), 2), "#,##0.00") & "' DISABLED/><input type='hidden' style='border-style:none' size=6 onChange='calc();' id='kpitmtd" & row & "' value='" & System.Math.Round(CDec(dr("target")), 2) & "' DISABLED/></td>"
                                End If
                            End If

                            If dr("targetytd").ToString() = "-" Then
                                If IsDBNull(dr("actualytd")) Then
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' /></td>"
                                Else
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='" & Format(System.Math.Round(dr("actualytd"), 2), "#,##0.00") & "' READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0'/></td>"
                                End If
                            ElseIf dr("target").ToString() = "-" Then
                                If IsDBNull(dr("actualytd")) Then
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0'/></td>"
                                Else
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='" & Format(System.Math.Round(dr("actualytd"), 2), "#,##0.00") & "' READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' /></td>"
                                End If
                            ElseIf dr("actual").ToString() = "-" Then
                                If IsDBNull(dr("actualytd")) Then
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='' READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' /></td>"
                                Else
                                    mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='" & Format(System.Math.Round(dr("actualytd"), 2), "#,##0.00") & "' READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='0' /></td>"
                                End If
                            Else
                                If IsDBNull(dr("actualytd")) Then
                                    If dr("actual").ToString() = "" Then
                                        mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value=''  READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='' /></td>"
                                    Else
                                        mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value=''  READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='" & IIf(dr("actual") = "", "", System.Math.Round(CDec(dr("actual")), 2)) & "' /></td>"
                                    End If
                                Else
                                    If dr("actual").ToString() = "" Then
                                        mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='" & Format(System.Math.Round(dr("actualytd"), 2), "#,##0.00") & "'  READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='' /></td>"
                                    Else
                                        mytableresult += "<td ><input type='text' style='border-style:none' class='printe' onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpia" & row & "' value='" & Format(System.Math.Round(dr("actualytd"), 2), "#,##0.00") & "'  READONLY/><input type='hidden' class='print' size=6 onkeypress='fncInputNumericValuesOnly();' onChange='calc();' id='kpiamtd" & row & "' value='" & IIf(dr("actual") = "", "", System.Math.Round(CDec(dr("actual")), 2)) & "' /></td>"
                                    End If

                                End If
                            End If
                        End If

                        'COUNT ACH
                        If dr("targetytd").ToString() = "-" Or dr("targetytd").ToString() = "" Or dr("StEdit") = "1" Then
                            ytdach = "-"
                            mytableresult += "<td>-</td>"
                            ach = 0
                        ElseIf IsDBNull(dr("actualytd")) Then
                            ytdach = "-"
                            mytableresult += "<td align='right'></td>"
                            ach = 0
                        Else
                            If dr("calctype") = "0" Then 'MIN
                                If dr("actualytd").ToString() = "-" Then
                                    ach = 0
                                ElseIf dr("actualytd") = 0 Then
                                    If dr("targetytd") = 0 Then
                                        ach = 100
                                    Else
                                        ach = 105
                                    End If
                                Else
                                    ach = dr("targetytd") / dr("actualytd") * 100
                                End If
                            ElseIf dr("calctype") = "1" Then 'MAX
                                If dr("actualytd").ToString() = "-" Then
                                    ach = 0
                                ElseIf dr("targetytd") = 0 Then
                                    If dr("actualytd") = 0 Then
                                        ach = 100
                                    Else
                                        ach = 105
                                    End If
                                Else
                                    ach = dr("actualytd") / dr("targetytd") * 100
                                End If
                            ElseIf dr("calctype") = "2" Then
                                If dr("actualytd").ToString() = "-" Then
                                    ach = 0
                                Else
                                    Select Case dr("type")
                                        Case 0 'Max
                                            If (dr("actualytd") <= dr("e2") And dr("actualytd") >= dr("e1")) Then
                                                ach = dr("ea")
                                            ElseIf (dr("actualytd") <= dr("mr2") And dr("actualytd") >= dr("mr1")) Then
                                                ach = dr("mra")
                                            ElseIf (dr("actualytd") <= dr("bl2") And dr("actualytd") >= dr("bl1")) Then
                                                ach = dr("bla")
                                            ElseIf (dr("actualytd") <= dr("p2")) Then
                                                ach = dr("pa")
                                            Else
                                                ach = 100
                                            End If
                                        Case 1 'Min
                                            If (dr("actualytd") <= dr("e2") And dr("actualytd") >= dr("e1")) Then
                                                ach = dr("ea")
                                            ElseIf (dr("actualytd") <= dr("mr2") And dr("actualytd") >= dr("mr1")) Then
                                                ach = dr("mra")
                                            ElseIf (dr("actualytd") <= dr("bl2") And dr("actualytd") >= dr("bl1")) Then
                                                ach = dr("bla")
                                            ElseIf (dr("actualytd") > dr("p2")) Then
                                                ach = dr("pa")
                                            Else
                                                ach = 100
                                            End If
                                    End Select
                                End If
                            ElseIf dr("calctype") = "3" Then
                                If dr("actualytd").ToString() = "-" Then
                                    ach = 0
                                Else
                                    If (dr("actualytd") <= dr("e2") And dr("actualytd") >= dr("e1")) Then
                                        ach = dr("ea")
                                    ElseIf (dr("actualytd") <= dr("mr2") And dr("actualytd") >= dr("mr1")) Or (dr("actualytd") <= dr("mr4") And dr("actualytd") >= dr("mr3")) Then
                                        ach = dr("mra")
                                    ElseIf (dr("actualytd") <= dr("bl2") And dr("actualytd") >= dr("bl1")) Or (dr("actualytd") <= dr("bl4") And dr("actualytd") >= dr("bl3")) Then
                                        ach = dr("bla")
                                    ElseIf (dr("actualytd") > dr("p2") Or dr("actualytd") < dr("p1")) Then
                                        ach = dr("pa")
                                    Else
                                        ach = 100
                                    End If
                                End If
                            End If
                            If ach > 105 Then
                                ach = 105
                            End If
                            ytdach = ach.ToString()
                            mytableresult += "<td align='right'>" & Format(ach, "#,##0.00") & "</td>"
                        End If
                        '''''''''''''''''''''''''

                        If dr("targetytd").ToString() = "-" Or dr("targetytd").ToString() = "" Or dr("StEdit") = "1" Or IsDBNull(dr("actualytd")) Then
                        Else
                            totkpi += 1
                            sumach += System.Math.Round(ach, 2)
                        End If

                        Dim myactual As Decimal = 0
                        If IsDBNull(dr("actual")) Then
                            myactual = 0
                        ElseIf dr("actual") = "" Then
                            myactual = 0
                        Else
                            myactual = dr("actual")
                        End If
                        If dr("targetytd").ToString() = "-" Or dr("target").ToString() = "" Or dr("target").ToString() = "-" Or dr("actual").ToString() = "-" Or dr("StEdit") = "1" Then
                            mytableresult += "<td align='center'></td>"
                        ElseIf dr("oldact") = -9999 Then
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "equal.png' style='width:100%'/></td>"
                        ElseIf dr("oldact") > myactual Then
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "down.png' style='width:100%'/></td>"
                        ElseIf dr("oldact") < myactual Then
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "up.png' style='width:100%'/></td>"
                        Else
                            mytableresult += "<td align='center'><img id='img" & row & "' src='../images/" & getImage(ach, dr("redCond"), dr("redParam"), dr("greenCond"), dr("greenParam")) & "equal.png' style='width:100%'/></td>"
                        End If

                        row += 1

                    Next
                    '''''''''''''''''''''''
                    If totkpi <> 0 Then
                        mytableresult = mytableresult.Replace("VAL", System.Math.Round(sumach / totkpi, 2) & " %")
                        sumpach += sumach / totkpi
                        counter += 1
                    Else
                        mytableresult = mytableresult.Replace("VAL", "-")
                    End If
                    If counter <> 0 Then
                        mytableresult = mytableresult.Replace("###", System.Math.Round(sumpach / counter, 2))
                        mytableresult = mytableresult.Replace("$$$", System.Math.Round(sumpach / counter * bobot / 100, 2) & " %")
                        totach += sumpach / counter * bobot / 100
                    Else
                        mytableresult = mytableresult.Replace("###", "")
                        mytableresult = mytableresult.Replace("$$$", "")
                        totach += 0
                    End If
                    ''''''''''''''''''''''
                    mytableresult = mytableresult.Replace("x&x", socount)
                    mytableresult = mytableresult.Replace("@@@", Request.QueryString("a"))
                    'mytableresult = mytableresult.Replace("@@@", "Total Achievement :" & System.Math.Round(totach / totperspective * 100, 2).ToString() & " %")
                    result.InnerHtml += mytableresult + "</table>"
                    result.InnerHtml += "<br/><table border=""1"" style=""font-size:10px;border-collapse: collapse;border-width: 1px;"">"
                    result.InnerHtml += "<tr><td colspan=""6"">Keterangan</td></tr>"
                    result.InnerHtml += "<tr><td><img src=""../images/greendown.png""/></td><td>pencapaian n > 95%, dan lebih rendah dari bulan lalu</td><td><img src=""../images/greenequal.png""/></td><td>pencapaian n > 95%, dan sama dengan bulan lalu</td><td><img src=""../images/greenup.png""/></td><td>pencapaian n > 95%, dan lebih tinggi dari bulan lalu</td></tr>"
                    result.InnerHtml += "<tr><td><img src=""../images/yellowdown.png""/></td><td>pencapaian 95% > n > 90%, dan lebih rendah dari bulan lalu</td><td><img src=""../images/yellowequal.png""/></td><td>pencapaian 95% > n > 90%, dan sama dengan bulan lalu</td><td><img src=""../images/yellowup.png""/></td><td>pencapaian 95% > n > 90%, dan lebih tinggi dari bulan lalu</td></tr>"
                    result.InnerHtml += "<tr><td><img src=""../images/reddown.png""/></td><td>pencapaian n < 85%, dan lebih rendah dari bulan lalu</td><td><img src=""../images/redequal.png""/></td><td>pencapaian n < 85%, dan sama dengan bulan lalu</td><td><img src=""../images/redup.png""/></td><td>pencapaian n < 85%, dan lebih tinggi dari bulan lalu</td></tr>"
                    result.InnerHtml += "</table>"
            End Select

        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Function GetMonth(ByVal s As String) As String
        Select Case s
            Case "1"
                Return "jan"
            Case "2"
                Return "feb"
            Case "3"
                Return "mar"
            Case "4"
                Return "apr"
            Case "5"
                Return "mei"
            Case "6"
                Return "jun"
            Case "7"
                Return "jul"
            Case "8"
                Return "agu"
            Case "9"
                Return "sep"
            Case "10"
                Return "okt"
            Case "11"
                Return "nov"
            Case "12"
                Return "des"
        End Select
    End Function

    Function getApproval(ByVal approved As Integer) As String
        If approved = 0 Then 'Draft
            Return "<img src='../images/draft.png' align='right'/>"
        Else
            Return "<img src='../images/approved.png'  align='right'/>"
        End If
    End Function

    Function getMapLevel(ByVal level As String) As String
        If level = "3" Then
            Return "0"
        ElseIf level = "2" Then
            Return "1"
        Else
            Return level
        End If
        Return level
    End Function

    Function getImage(ByVal value As String, ByVal redCond As String, ByVal redVal As Decimal, ByVal greenCond As String, ByVal greenVal As Decimal) As String
        Try
            If value = "-999999.314" Then
                Return "yellow"
            End If
            Select Case redCond
                Case "0"
                    If value > redVal Then
                        Return "red"
                    End If
                Case "1"
                    If value >= redVal Then
                        Return "red"
                    End If
                Case "2"
                    If value < redVal Then
                        Return "red"
                    End If
                Case "3"
                    If value <= redVal Then
                        Return "red"
                    End If
            End Select
            Select Case greenCond
                Case "0"
                    If value > greenVal Then
                        Return "green"
                    End If
                Case "1"
                    If value >= greenVal Then
                        Return "green"
                    End If
                Case "2"
                    If value < greenVal Then
                        Return "green"
                    End If
                Case "3"
                    If value <= greenVal Then
                        Return "green"
                    End If
            End Select
            Return "yellow"
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Function

    Function GetFullMonth(ByVal s As String) As String
        Select Case s
            Case "1"
                Return "JANUARI"
            Case "2"
                Return "februari"
            Case "3"
                Return "maret"
            Case "4"
                Return "april"
            Case "5"
                Return "mei"
            Case "6"
                Return "juni"
            Case "7"
                Return "juli"
            Case "8"
                Return "agustus"
            Case "9"
                Return "september"
            Case "10"
                Return "oktober"
            Case "11"
                Return "november"
            Case "12"
                Return "desember"
        End Select
    End Function
End Class