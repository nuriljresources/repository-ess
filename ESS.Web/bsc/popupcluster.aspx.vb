﻿Imports System.Data.SqlClient

Partial Public Class popupcluster
    Inherits System.Web.UI.Page

    Private Sub popupcluster_InitComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.InitComplete
        Dim type As String
        KPIID.Value = Request.QueryString("id")

        'Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        'Try
        '    Dim query As String = ""
        '    Dim cnt As Integer = 1
        '    query = "select * from calccontent where kpiid='" & KPIID.Value & "' order by nomor"
        '    Dim cmd As New SqlCommand(query, conn)
        '    Dim dr As SqlDataReader
        '    conn.Open()
        '    dr = cmd.ExecuteReader()
        '    If dr.HasRows Then
        '        While dr.Read
        '            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes2", "addRowToFTable()", True)
        '            cnt += 1
        '        End While
        '    End If
        '    conn.Close()
        'Catch ex As Exception
        '    lblError.Text = ex.Message
        'End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim addQuery As String = ""
            Dim result As String = ""
            Dim sqlsplit() As String = Split(SQLTOSEND.Value, "~#~")
            For Each s As String In sqlsplit
                Select Case Left(s, 3)
                    Case "DEL"
                        addQuery += "delete calccontent where id='" & Split(s, "*#*")(1) & "';"
                    Case "INS"
                        addQuery += "INSERT INTO calccontent (kpiid,nomor,actual,achievement,createdby,createdin) values " & _
                               "(" & KPIID.Value & ",'" & Split(s, "*#*")(1) & "','" & Split(s, "*#*")(2) & "','" & Split(s, "*#*")(3) & "','" & Session("NikUser") & "','" & Request.UserHostName & "');"
                    Case "UPD"
                        addQuery += "UPDATE calccontent set nomor='" & Split(s, "*#*")(1) & "'," & _
                                    "modifiedby='" & Session("NikUser") & "',modifiedtime=getdate(),modifiedin='" & Request.UserHostName & "'," & _
                                    "actual='" & Split(s, "*#*")(2) & "',achievement='" & Split(s, "*#*")(3) & "'," & _
                                    "' where id=" & Split(s, "*#*")(0) & ";"
                End Select
            Next
            Dim cmd As New SqlCommand(addQuery, conn)
            conn.Open()
            result = cmd.ExecuteScalar()
            conn.Close()
            If IsNumeric(result) Then
                lblError.Text = "**Save Berhasil**"
                'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "tes2", "window.close();", True)
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

  
End Class