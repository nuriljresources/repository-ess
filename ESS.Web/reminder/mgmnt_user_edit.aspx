﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/reminder/master.Master" CodeBehind="mgmnt_user_edit.aspx.vb" Inherits="EXCELLENT.mgmnt_user_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Document Center Admin
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="../Scripts/jscal2.js" type ="text/javascript"></script>
<script src="../Scripts/en.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="../css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="../css/win2k/win2k.css" />

    <style type="text/css" >
        .style1
        {width: 20%;height: 88px;text-align: left;}
        .style2
        {width: 70%;height: 88px;}
        .style3
        {width: 10%;height: 88px;text-align: left;}
        
            #dhtmltooltip{
            position: absolute;
            width: 150px;
            border: 2px solid black;
            padding: 2px;
            background-color: yellow;
            visibility: hidden;
            z-index: 100;
            /*Remove below line to remove shadow. Below line should always appear last within this CSS*/
            /*filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,direction=135);*/
            }
        #form1
        {
            font-family:Tahoma,Arial;
        }
        #form1 h1 
        {
            color: #FFFFFF;
            font-size:x-large;
        }
        .title
        {
            color: blue;
            font-size: small;
            margin-bottom:10px;
        }
        .line
        {
            width: 100%;
            height: 20px;
            font-family:Tahoma,Arial;
        }
        .general
        {
            font-size:0.8em;
            color:#453944;
            font-weight:bold;
            border-style:solid;
            border-width:1px;
            border-color:#33ADFF;
            padding-left:5px;
        }
        
        .general a
        {
            text-decoration:none;
            color:#453944;
        }
        
        .general a:hover
        {
            text-decoration:underline;
            color:#33ADFF;
        }
        .style1
        {
            height: 26px;
        }
    </style>
    <script type ="text/javascript" >
        function OpenPopupdel(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("srcuser.aspx", "List", "scrollbars=yes,resizable=no,width=500,height=400");
            return false;
        }
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div class="line" style="right:10px; text-align:right;">
    <a href="signout.aspx">Logout</a>
    </div>
    
    <div class="title">
        <strong>Menu</strong>
    </div>
    <div id="list_newest" class="general" style="width:200px;">
        <div id="Div1"><img alt="open document" src="../images/open_doc_small.png" /> <a href="index.aspx">&nbsp;Home</a></div>
        <%=mnentry%>
        <%=mnview%>
        <%=mnmgmt%>
    <div><img alt="open document" src="../images/open_doc_small.png" /> <a href="J Resources Document Center Manual Version 1.pdf" target="_blank">&nbsp;Help</a></div>
    </div>
    <div id="list_permit" class="general" style="width:720px; border-style:none; height:400px; left:220px; top:15px; position:absolute; font-size:0.8em;">
        
        <table>
        <tr>
        <td class="general" style="padding-left:5px; padding-right:5px;"><a href="mgmnt_user_entry.aspx">Insert User</a></td>
        <td class="general" style="padding-left:5px; padding-right:5px;"><a href="mgmnt_list.aspx">List User</a></td>
        </tr>
        </table>
        <table width="100%">
            <tr>
                <td>
                    Name
                </td>
                <td>
                    
                    <asp:SqlDataSource ID="DSemp" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:EMPLOYEEConn %>" 
                        
                        SelectCommand="SELECT Nik, NIKSITE, Nama FROM H_A101 where stedit &lt;&gt; 2 ORDER BY Nama"></asp:SqlDataSource>
                <asp:TextBox ID = "name" runat = "server" Enabled = "false" ></asp:TextBox>
                <%--<img alt="add" id="Img4" src="../images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopupdel('user')" />--%> 
               <asp:TextBox ID = "nik" runat = "server" style="visibility : hidden;"></asp:TextBox> 
               <asp:TextBox ID = "nm" runat = "server" style="visibility : hidden;"></asp:TextBox> 
                </td>
            </tr>
            <tr>
                <td>
                    level
                </td>
                <td>
                            <asp:DropDownList ID='ddllvl' runat='server'>
                            <asp:ListItem Value="1">Owner</asp:ListItem>
                            <asp:ListItem Value="2">Doc Admin</asp:ListItem>
                            <asp:ListItem Value="4">PIC</asp:ListItem>
                            <%--<asp:ListItem Value="R">Reminder Only</asp:ListItem>--%>
                            <asp:ListItem Value="3">Cross</asp:ListItem>
                            </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Department
                </td>
                <td>
                    <asp:DropDownList ID="ddlmodul" runat="server">
                    <asp:ListItem Value=" "> </asp:ListItem>
                    <asp:ListItem Value="IT">IT</asp:ListItem>
                    <asp:ListItem Value="HRCA">HRCA</asp:ListItem>
                    <asp:ListItem Value="Legal">Legal</asp:ListItem>
                    <asp:ListItem Value="Plant">Plant</asp:ListItem>
                    <asp:ListItem Value="FINACC">Finance Accounting</asp:ListItem>
                    <asp:ListItem Value="IAS">IAS</asp:ListItem>
                    <asp:ListItem Value="Finance">Finance</asp:ListItem>
                    <asp:ListItem Value="HR">HR</asp:ListItem>
                    <asp:ListItem Value="Accounting">Accounting</asp:ListItem>
                    <asp:ListItem Value="Exploration">Exploration</asp:ListItem>
                    <asp:ListItem Value="TD">Technical Development</asp:ListItem>
                    <asp:ListItem Value="SCM-CCMS">SCM Contract/CMS</asp:ListItem>
                    <asp:ListItem Value="SCM-PROC">SCM Procurement</asp:ListItem>
                    <asp:ListItem Value="SOP">SOP</asp:ListItem>
                    <asp:ListItem Value="Risk">Risk</asp:ListItem>
                    <asp:ListItem Value="Mining">Mining/Operation</asp:ListItem>
                    <asp:ListItem Value="Project">Project</asp:ListItem>
                    <asp:ListItem Value="Legal">Legal</asp:ListItem>
                    <asp:ListItem Value="IA">Internal Audit</asp:ListItem>
                    <asp:ListItem Value="Logs">Logs</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            
            <tr>
            <td>
               
            </td>
            <td>
                <%--<asp:DropDownList ID="ddlmailsch" runat="server">
                    <asp:ListItem Value="01">Daily</asp:ListItem>
                    <asp:ListItem Value="02">Weekly</asp:ListItem>
                    <asp:ListItem Value="03">Montly</asp:ListItem>
                </asp:DropDownList>  --%>               
            </td> 
            </tr>
        </table>
        
        <br />
        
        <div id="idnotif"></div>
        <br />
         <asp:Button ID="Save" runat="server" style="background-image:url('../images/save-icon-small.png'); background-repeat:no-repeat; background-position:center; width:32px; height:32px;" />
       <asp:Button ID="Button2" runat="server" Text="" EnableTheming="false" style="background-image:url('../images/cancel.png'); background-repeat:no-repeat; background-position:center; width:32px; height:32px;"/>    
     <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="DSemp" DataTextField="Nama" DataValueField="NIKSITE" style="visibility:hidden;">
     </asp:DropDownList>
     <input type ="hidden" id="ctrlToFind" />
     <br />
     <asp:Label ID="tny" runat="server" ></asp:Label> <asp:Button ID="yes" Text="Yes" runat = "server" Visible="false" style="width:32px; height:32px;"/> <asp:Button ID="no" Text = "No" runat = "server" Visible="false" style="width:32px; height:32px;"/>
    </div> 
    
     <center>
          <span id="idbar" style="color: #800000; background-color: #6699FF; font-weight: bold"></span>
    </center>
    
<script type="text/javascript" language="javascript">
    if (BrowserDetect.version != 8) {
        document.getElementById("idnotif").style.display = "block";
        document.getElementById("idnotif").style.textAlign = "center"
        document.getElementById("idnotif").style.fontsize = "large";
        document.getElementById("idnotif").style.fontWeight = "bold";
        document.getElementById("idnotif").style.color = "#FF0000";
        //        document.getElementById("idnotif").innerHTML = "*** Application HRMS not compatible with your browser ***<br />*** Please Update to Internet Explorer 8 ***";
    }
    else {
        document.getElementById("idnotif").style.display = "none";
        document.getElementById("idnotif").style.textAlign = "center"
        document.getElementById("idnotif").style.fontsize = "large";
        document.getElementById("idnotif").style.fontWeight = "bold";
        document.getElementById("idnotif").style.color = "#FFFFFF";
        document.getElementById("idnotif").innerHTML = "";
    }
    document.getElementById("idbar").style.display = "none";

    var cal = Calendar.setup({
        onSelect: function(cal) { cal.hide() },
        showTime: false
    });
    //        cal.manageFields("btdtissue", "dtissue", "%m/%d/%Y");
    //        cal.manageFields("btdtexp", "dtexp", "%m/%d/%Y");
          
</script>
<%=msgbox%>


</asp:Content>