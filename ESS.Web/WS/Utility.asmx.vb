﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web.Script.Services
Imports System.Data.SqlClient

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Utility
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    <ScriptMethod()> _
    Function ShowLampiran(ByVal _Nomor As String, ByVal _IdRap As String, ByVal _IdIssue As String, ByVal _NoAction As String) As List(Of Lampiran)
        Dim aLampiran As List(Of Lampiran) = New List(Of Lampiran)

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT Nomor, Idrap, IdIssue, NoAction, NmFile FROM TRLAMPIRAN WHERE NoProgres = " + _Nomor + " AND "
            squery = squery + "IdRap = '" + _IdRap + "' AND IdIssue = '" + _IdIssue + "' AND NoAction = '" + _NoAction + "'"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            Dim lamp As Lampiran
            Do While sdr.Read()
                lamp = New Lampiran()
                lamp.Nomor = sdr("Nomor")
                lamp.IdRap = sdr("Idrap")
                lamp.IdIssue = sdr("IdIssue")
                lamp.NoAction = sdr("NoAction")
                lamp.NmFile = sdr("NmFile")
                aLampiran.Add(lamp)
            Loop

            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return aLampiran
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Function ShowAllLampiran(ByVal _IdRap As String, ByVal _IdIssue As String, ByVal _NoAction As String) As List(Of Lampiran)
        Dim aLampiran As List(Of Lampiran) = New List(Of Lampiran)

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT Nomor, Idrap, IdIssue, NoAction, NmFile FROM TRLAMPIRAN WHERE "
            squery = squery + "IdRap = '" + _IdRap + "' AND IdIssue = '" + _IdIssue + "' AND NoAction = '" + _NoAction + "'"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            Dim lamp As Lampiran
            Do While sdr.Read()
                lamp = New Lampiran()
                lamp.Nomor = sdr("Nomor")
                lamp.IdRap = sdr("Idrap")
                lamp.IdIssue = sdr("IdIssue")
                lamp.NoAction = sdr("NoAction")
                lamp.NmFile = sdr("NmFile")
                aLampiran.Add(lamp)
            Loop

            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try

        Return aLampiran
    End Function
End Class

Public Class Lampiran
    Private _nomor As String
    Private _IdRap As String
    Private _IdIssue As String
    Private _NoAction As String
    Private _NmFile As String

    Public Property Nomor() As String
        Get
            Return _nomor
        End Get
        Set(ByVal value As String)
            _nomor = value
        End Set
    End Property
    Public Property IdRap() As String
        Get
            Return _IdRap
        End Get
        Set(ByVal value As String)
            _IdRap = value
        End Set
    End Property
    Public Property IdIssue() As String
        Get
            Return _IdIssue
        End Get
        Set(ByVal value As String)
            _IdIssue = value
        End Set
    End Property
    Public Property NoAction() As String
        Get
            Return _NoAction
        End Get
        Set(ByVal value As String)
            _NoAction = value
        End Set
    End Property
    Public Property NmFile() As String
        Get
            Return _NmFile
        End Get
        Set(ByVal value As String)
            _NmFile = value
        End Set
    End Property
End Class