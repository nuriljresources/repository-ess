﻿Imports System.Data.SqlClient

Partial Public Class popupdiscrete
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Try
                Grup.Value = Session("permission")
                Dim query As String = ""
                Dim dr As SqlDataReader
                KPIID.Value = Request.QueryString("id")
                KPICALCTYPE.Value = Request.QueryString("c")
                lblNama.Text = Request.QueryString("n")
                Select Case KPICALCTYPE.Value
                    Case "4" 'MAX
                        lblKet.Text = " DISCRETE MAX "
                        P1.Text = "<="
                        MR2.Attributes.Add("readonly", "readonly")
                        BL2.Attributes.Add("readonly", "readonly")
                        MR2.BackColor = Drawing.Color.Silver
                        BL2.BackColor = Drawing.Color.Silver
                        MR1.Attributes.Remove("readonly")
                        BL1.Attributes.Remove("readonly")
                        MR1.BackColor = Drawing.Color.White
                        BL1.BackColor = Drawing.Color.White
                        P2.Attributes.Add("readonly", "readonly")
                        P2.BackColor = Drawing.Color.Silver
                    Case "2" 'MIN
                        lblKet.Text = " DISCRETE MIN "
                        P1.Text = ">"
                        MR2.Attributes.Remove("readonly")
                        BL2.Attributes.Remove("readonly")
                        MR2.BackColor = Drawing.Color.White
                        BL2.BackColor = Drawing.Color.White
                        MR1.Attributes.Add("readonly", "readonly")
                        BL1.Attributes.Add("readonly", "readonly")
                        MR1.BackColor = Drawing.Color.Silver
                        BL1.BackColor = Drawing.Color.Silver
                        P2.Attributes.Add("readonly", "readonly")
                        P2.BackColor = Drawing.Color.Silver
                End Select
                P1.Enabled = False
                query = "select * from calccontent where kpiid='" & KPIID.Value & "' and calctype='" & KPICALCTYPE.Value & "'"
                Dim cmd As New SqlCommand(query, conn)
                conn.Open()
                dr = cmd.ExecuteReader
                If dr.HasRows Then
                    While dr.Read
                        E1.Text = dr("e1")
                        E2.Text = dr("e2")
                        EA.Text = dr("ea")
                        MR1.Text = dr("mr1")
                        MR2.Text = dr("mr2")
                        MRA.Text = dr("mra")
                        BL1.Text = dr("bl1")
                        BL2.Text = dr("bl2")
                        BLA.Text = dr("bla")
                        Select Case dr("calctype")
                            Case "4"
                                P1.Text = "<="
                            Case "2"
                                P2.Text = ">"
                        End Select
                        P2.Text = dr("p2")
                        PA.Text = dr("pa")
                    End While
                End If
            Catch ex As Exception
                lblError.Text = ex.Message
            End Try
        End If
    End Sub

    'Private Sub ddlTipe_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTipe.SelectedIndexChanged
    '    Try
    '        Select Case ddlTipe.SelectedIndex
    '            Case 0 'Max
    '                P1.Text = "<="
    '                MR2.Attributes.Add("readonly", "readonly")
    '                BL2.Attributes.Add("readonly", "readonly")
    '                MR2.BackColor = Drawing.Color.Silver
    '                BL2.BackColor = Drawing.Color.Silver
    '                MR1.Attributes.Remove("readonly")
    '                BL1.Attributes.Remove("readonly")
    '                MR1.BackColor = Drawing.Color.White
    '                BL1.BackColor = Drawing.Color.White
    '            Case 1 'Min
    '                P1.Text = ">"
    '                MR2.Attributes.Remove("readonly")
    '                BL2.Attributes.Remove("readonly")
    '                MR2.BackColor = Drawing.Color.White
    '                BL2.BackColor = Drawing.Color.White
    '                MR1.Attributes.Add("readonly", "readonly")
    '                BL1.Attributes.Add("readonly", "readonly")
    '                MR1.BackColor = Drawing.Color.Silver
    '                BL1.BackColor = Drawing.Color.Silver
    '        End Select
    '    Catch ex As Exception
    '        lblError.Text = ex.Message
    '    End Try
    'End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            If Validasi() Then
                Dim query As String = ""
                Dim result As Integer = 0
                query = "select count(*) from calccontent where kpiid='" & KPIID.Value & "'"
                Dim cmd As New SqlCommand(query, conn)
                conn.Open()
                result = cmd.ExecuteScalar()
                query = "BEGIN TRANSACTION; BEGIN TRY "
                If result = 0 Then 'Data belum ada sebelumnya
                    query += "insert into calccontent(kpiid,calctype,e1,e2,ea,mr1,mr2,mra,bl1,bl2,bla,p2,pa,createdby,createdin) values ( " & _
                            "'" & KPIID.Value & "','" & KPICALCTYPE.Value & "'," & E1.Text & "," & E2.Text & "," & EA.Text & _
                            "," & MR1.Text & "," & MR2.Text & "," & MRA.Text & "," & BL1.Text & "," & BL2.Text & "," & BLA.Text & _
                            "," & P2.Text & "," & PA.Text & ",'" & Session("NikUser") & "','" & Request.UserHostName & "');"
                Else
                    query += "update calccontent set calctype=" & KPICALCTYPE.Value & ",e1=" & E1.Text & ",e2=" & E2.Text & ",ea=" & EA.Text & _
                            ",mr1=" & MR1.Text & ",mr2=" & MR2.Text & ",mr3=null,mr4=null,mra=" & MRA.Text & ",bl1=" & BL1.Text & ",bl2=" & BL2.Text & ",bl3=null,bl4=null,bla=" & BLA.Text & _
                            ",p1=null,p2=" & P2.Text & ",pa=" & PA.Text & ",modifiedtime=getdate(),modifiedby='" & Session("NikUser") & "',modifiedin='" & Request.UserHostName & "' where kpiid='" & KPIID.Value & "';"
                End If
                query += " update mkpi set calctype=" & KPICALCTYPE.Value & " where id='" & KPIID.Value & "';"
                query += Calculate(KPICALCTYPE.Value,KPIID.Value)
                query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 1;end "
                '-------------------------------------------------------------------------------------------------------
                result = 0
                cmd = New SqlCommand(query, conn)
                result = cmd.ExecuteScalar()
                If result <> 0 Then
                    lblError.Text = "Save Berhasil"
                Else
                    lblError.Text = "Save Gagal"
                End If
                conn.Close()
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Function Validasi() As Boolean
        Try
            If Not IsNumeric(E1.Text) Then
                lblError.Text = "Field E1 bukan merupakan angka"
                Return False
            ElseIf Not IsNumeric(E2.Text) Then
                lblError.Text = "Field E2 bukan merupakan angka"
                Return False
            ElseIf Not IsNumeric(EA.Text) Then
                lblError.Text = "Field Achievement E bukan merupakan angka"
                Return False
            ElseIf Not IsNumeric(MR1.Text) Then
                lblError.Text = "Field MR1 bukan merupakan angka"
                Return False
            ElseIf Not IsNumeric(MR2.Text) Then
                lblError.Text = "Field MR2 bukan merupakan angka"
                Return False
            ElseIf Not IsNumeric(MRA.Text) Then
                lblError.Text = "Field MRA bukan merupakan angka"
                Return False
            ElseIf Not IsNumeric(BL1.Text) Then
                lblError.Text = "Field BL1 bukan merupakan angka"
                Return False
            ElseIf Not IsNumeric(BL2.Text) Then
                lblError.Text = "Field BL2 bukan merupakan angka"
                Return False
            ElseIf Not IsNumeric(BLA.Text) Then
                lblError.Text = "Field BLA bukan merupakan angka"
                Return False
            ElseIf Not IsNumeric(P2.Text) Then
                lblError.Text = "Field P2 bukan merupakan angka"
                Return False
            ElseIf Not IsNumeric(PA.Text) Then
                lblError.Text = "Field PA bukan merupakan angka"
                Return False
            End If
            Return True
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try

    End Function

    Function Calculate(ByVal CalcType As String, ByVal kpiid As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim sqlquery As String = ""
            Dim cmd As New SqlCommand(sqlquery, conn)
            conn.Open()
                Dim dt As New DataTable()
                sqlquery = "select * from TBSC where kpiid='" & kpiid & "' and approved='0'"
                Dim reader As SqlDataReader
                cmd = New SqlCommand(sqlquery, conn)
                reader = cmd.ExecuteReader()
                sqlquery = ""
                If reader.HasRows Then
                    Dim returnQuery As String = ""
                    While reader.Read
                        If IsDBNull(reader("target")) And IsDBNull(reader("Actual")) Then
                        ElseIf CalcType = "0" Or CalcType = "1" Then 'MIN OR MAX
                            If IsDBNull(reader("target")) Or IsDBNull(reader("Actual")) Then
                            Else
                                If CalcType = "0" Then
                                sqlquery += "update TBSC set achievement='" & IIf(Math.Round(CDec(reader("target")) / CDec(reader("actual")) * 100, 2) > 105, 105, Math.Round(CDec(reader("target")) / CDec(reader("actual")) * 100, 2)) & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "' and approved='0';"
                                Else
                                sqlquery += "update TBSC set achievement='" & IIf(Math.Round(CDec(reader("actual")) / CDec(reader("target")) * 100, 2) > 105, 105, Math.Round(CDec(reader("actual")) / CDec(reader("target")) * 100, 2)) & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "' and approved='0';"
                                End If
                            End If
                        Else
                            Select Case CalcType
                                Case 2
                                    If (reader("actual") <= cdec(e2.tEXT) And reader("actual") >= cdec(e1.text)) Then
                                    sqlquery += "update TBSC set achievement='" & EA.Text & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "' and approved='0';"
                                    ElseIf (reader("actual") <= cdec(mr2.text) And reader("actual") >= cdec(mr1.text)) Then
                                    sqlquery += "update TBSC set achievement='" & MRA.Text & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "' and approved='0';"
                                    ElseIf (reader("actual") <= cdec(bl2.text) And reader("actual") >= cdec(bl1.text)) Then
                                    sqlquery += "update TBSC set achievement='" & BLA.Text & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "' and approved='0';"
                                    ElseIf (reader("actual") > cdec(p2.text)) Then
                                    sqlquery += "update TBSC set achievement='" & PA.Text & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "' and approved='0';"
                                    Else
                                    sqlquery += "update TBSC set achievement='" & 100 & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "' and approved='0';"
                                    End If
                                Case 4
                                    If (reader("actual") <= cdec(e2.tEXT) And reader("actual") >= cdec(e1.text)) Then
                                    sqlquery += "update TBSC set achievement='" & EA.Text & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "' and approved='0';"
                                    ElseIf (reader("actual") <= cdec(mr2.text) And reader("actual") >= cdec(mr1.text)) Then
                                    sqlquery += "update TBSC set achievement='" & MRA.Text & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "' and approved='0';"
                                    ElseIf (reader("actual") <= cdec(bl2.text) And reader("actual") >= cdec(bl1.text)) Then
                                    sqlquery += "update TBSC set achievement='" & BLA.Text & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "' and approved='0';"
                                    ElseIf (reader("actual") <= cdec(p2.text)) Then
                                    sqlquery += "update TBSC set achievement='" & PA.Text & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "' and approved='0';"
                                    Else
                                    sqlquery += "update TBSC set achievement='" & 100 & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "' and approved='0';"
                                    End If
                            End Select
                        End If
                    End While
                    Return sqlquery
                Else
                    conn.Close()
                    Return ""
                End If

        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            Return ""
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

End Class