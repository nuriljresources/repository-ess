﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="medic_outpatient.aspx.vb" Inherits="EXCELLENT.medic_outpatient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Outpatient Entry
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script src="Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="Scripts/jscal2.js" type ="text/javascript"></script>
    <script src="Scripts/en.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="css/win2k/win2k.css" />
    <link rel="stylesheet" href="css/jquery.tooltip/jquery.tooltip2.css" type="text/css" />
    <script type="text/javascript" src="Scripts/tooltip/jquery.min.js"></script>
    <script type="text/javascript" src="Scripts/tooltip/jquery.tooltip.js"></script>
    <script type="text/javascript" >
        function OpenPopup(key) {
            var d = document.getElementById("ctl00_ContentPlaceHolder1_hnik").value
            document.getElementById("ctrlToFind").value = key;
            window.open("srcfamily.aspx?id=" + d, "List", "scrollbars=yes,resizable=no,width=600,height=500");
            return false;
        }

        function OpenPopupd(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("searchmedicdel.aspx", "List", "scrollbars=yes,resizable=no,width=600,height=500");
            return false;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode < 45 || charCode > 57 || charCode == 47 || charCode == 45)
                return false;

            return true;
        }

        function readonly(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 00)
                return false;

            return true;
        }

        $j = jQuery.noConflict();
        $j(document).ready(function() {
            $j("div.item").tooltip();
        });
    </script>
    
    <style type="text/css" >
    /** { font-family: Times New Roman; }*/
      div.item { width:150px; height:25px; background-color: Transparent; text-align:center; padding-top:0px; color: white;}
      div#item_1 { position: absolute; top: 95px; left: 350px; color: white; font-family:arial; background-color:#E38695;}
      div#item_2 { position: absolute; top: 250px; left: 649px; color: black; font-family:arial;}      
      div#item_3 { position: absolute; top: 250px; left: 955px; color: black; font-family:arial;}            
      div#item_4 { position: absolute; top: 250px; left: 1155px; color: black; font-family:arial;}
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <input type="hidden" id="ctrlToFind" /> <asp:HiddenField ID="hprint" runat="server" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
        <caption style="text-align: center; font-size: 1.5em; color:White;">Outpatient Medical Claim</caption>
        <tr>
            <td style="width:10%;">
                Trans No
            </td>
            <td style="width:15%;">
                <asp:TextBox ID="txtnoreg" Width="150px" runat="server" Text="Auto" Enabled="false" ></asp:TextBox>
            </td>
            <td style="width:10%;">Date</td>
            <td colspan="2">
                <asp:TextBox ID="txtdate" Width="200px" runat="server" Font-Size="1.0em"></asp:TextBox> 
            </td>
        </tr>
        <tr>
            <td>
                Site
            </td>
            <td>
                <asp:TextBox ID="txtnmsite" Width="150px" runat="server" ></asp:TextBox>
                <asp:HiddenField ID="hnmsite" runat="server" />
            </td>
            <td>
                costcode
            </td>
            <td>
                <asp:TextBox ID="txtcostcode" Width="200px" runat="server" Font-Size="1.0em"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Nik <strong style="color:Red;">*</strong>
            </td>
            <td>
                <asp:TextBox ID="txtnik" Width="150px" runat="server" Font-Size="1.0em"></asp:TextBox> 
                <img alt="add" id="Img1" src="images/Search.gif" align="absmiddle" style="cursor: pointer" onclick="OpenPopupd('srcd')" />
                <asp:HiddenField ID="hnik" runat="server" />
            </td>
            <td>
            Name
            </td>
            <td>
            <asp:TextBox ID="txtnama" runat="server" Width="200px" Font-Size="1.0em"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Position
            </td>
            <td>
                <asp:TextBox ID="txtlevel" Width="150px" runat="server" Font-Size="1.0em"></asp:TextBox>
                <asp:HiddenField ID="hkdlevel" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
               
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtremarks" Visible="false" runat="server" TextMode="MultiLine" Width="483px" Font-Size="1.0em"></asp:TextBox>
                 <div id="item_1" class="item" style="font-weight:bold;">
                    Medical Benefit Policy <img alt="info" src="images/alert.png" width="20px" height="20px" />
                    <div class="tooltip_description" style="display:none;" title="Medical Benefit Policy">
                        Annual Limit IDR. <%=benefit%>
                    </div> 
                 </div> 
            </td>
        </tr>
    </table>
    <br />
    
    <%--<div style="width:90%;">
        <table class="data-table" style="border-style:none; padding: 0 0 0 0;" width="100%"><tr><th rowspan="2" style="width:80px;">Date</th><th rowspan="2" style="width:125px;">Name</th><th rowspan="2" style="width:60px;">Doctor</th><th rowspan="2" style="width:70px;">Gen/ Spc/Den</th> <th rowspan="2" style="width:80px;">Specialist</th> <th rowspan="2" style="width:75px;">Diagnose</th> <th rowspan="2" style="width:75px;">Dr/Hospital/ Clinic Name</th> <th rowspan="2" style="width:75px;">Remarks</th> <th colspan="5">Expense</th></tr>
    <tr> <th style="width:80px;">Consultation</th> <th style="width:80px;">Medicine</th> <th style="width:80px;">Lab</th> <th style="width:80px;">Other</th> <th style="width:80px;">Total</th></tr></table>
    </div>--%>
    
    <div id="grid" style="width:80%">
    <div style="background-color:#d1ffc1; margin-left:702px; text-align:center; font-weight:bold; font-size:0.9em; width:433px; height:18px"> Expenses </div>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="No" OnRowCancelingEdit="GridView1_RowCancelingEdit"
       OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" OnRowCommand="GridView1_RowCommand"
       ShowFooter="True" OnRowDeleting="GridView1_RowDeleting" CssClass="data-table" ShowHeader = "True" >
       <Columns>
           <asp:TemplateField HeaderText="Receipt Date">
               <EditItemTemplate>
               <div style="width:85px">
                   <asp:HiddenField ID="No" runat="server" />
                   <asp:TextBox ID="txtDateinv" style="width:60px" runat="server" onkeypress="return isNumberKey(event)" Text='<%# Eval("TglDet") %>' Font-Size="1.0em"> </asp:TextBox>
                   <button id="btn2" style="height:20px; width:15px;">..</button>
                </div> 
               </EditItemTemplate>
               <FooterTemplate>
                   <div style="width:85px">
                   <asp:TextBox ID="txtNewdt" runat="server" onkeypress="return isNumberKey(event)" style="width:60px" Font-Size="1.0em"> </asp:TextBox> 
                   <button id="btn1" style="height:20px; width:15px;">..</button>
                   </div>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label6" runat="server" Text='<%# Bind("TglDet") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>         
           <asp:TemplateField HeaderText="Name" HeaderStyle-Width="100px" >
               <EditItemTemplate>
               <div style="width:130px">
                   <asp:HiddenField ID="hperson" runat="server" Value='<%# Bind("person") %>' />
                   <asp:TextBox ID="txtNama" runat="server" Width="100px" Text='<%# Bind("nama") %>' Font-Size="1.0em" onkeypress="return readonly(event)"></asp:TextBox>
                   <img alt="add" id="Img1" src="images/Search.gif" align="absmiddle" style="cursor: pointer" onclick="OpenPopup('srcfamily')" />
               </div> 
               </EditItemTemplate>
               <FooterTemplate>
               <div style="width:130px">
                   <asp:HiddenField ID="hnewperson" runat="server" />
                   <asp:TextBox ID="txtNewNama" Width="100px" runat="server" Font-Size="1.0em" onkeypress="return readonly(event)"></asp:TextBox>
                   <img alt="add" id="Img1" src="images/Search.gif" align="absmiddle" style="cursor: pointer" onclick="OpenPopup('srcfamily')" />
               </div>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label3" runat="server" Text='<%# Bind("nama") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           
           
           <%--<asp:TemplateField HeaderText="%Claim">
               <EditItemTemplate>
                   <asp:TextBox ID="txtperclaim" runat="server" Width="40px" Text='<%# Eval("persenclaim") %>' Font-Size="1.0em"></asp:TextBox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewperclaim" runat="server" Text="100%" Width="40px" Font-Size="1.0em"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label8" runat="server" Width="40px" Text='<%# Bind("persenclaim") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>--%>
           
                      <asp:TemplateField HeaderText="Treatment By" Visible="false" >
                <EditItemTemplate>
                    <asp:HiddenField ID="htdokter" runat = "server" />
                    <asp:DropDownList ID="ddltdokter" runat ="server" Width="60px" Font-Size="1.0em" AutoPostBack="true" selectedvalue='<%# eval("htdokter")%>' OnSelectedIndexChanged="ddltdokter_SelectedIndexChanged">
                        <%--<asp:ListItem Value = ""> </asp:ListItem>--%>
                        <asp:ListItem Value = "0">Doctor</asp:ListItem>
                        <%--<asp:ListItem Value = "1">Non Doctor</asp:ListItem>--%>
                    </asp:DropDownList>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:DropDownList ID="ddlnewtdokter" runat ="server" Width="60px" Font-Size="1.0em" AutoPostBack = "true" OnSelectedIndexChanged="ddlnewtdokter_SelectedIndexChanged">
                        <%--<asp:ListItem Value = ""> </asp:ListItem>--%>
                        <asp:ListItem Value = "0">Doctor</asp:ListItem>
                        <%--<asp:ListItem Value = "1">Non Doctor</asp:ListItem>--%>
                    </asp:DropDownList>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label30" Width="60px" runat="server" Text='<%# Bind("tdokter") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Doctor">
                <EditItemTemplate>
                    <%--<asp:textbox ID="txtdoctortype" Width="80px" runat="server" Text='<%# Eval("jdokter") %>'></asp:textbox>--%>
                    <asp:DropDownList ID="ddldoctortype" runat = "server" Width="70px" Font-Size="1.0em" AutoPostBack="true" OnSelectedIndexChanged = "ddldoctortype_SelectedIndexChanged">
                    
                    <asp:ListItem Value= "1" >General</asp:ListItem>
                    <asp:ListItem Value= "2" >Specialist</asp:ListItem>
                    <asp:ListItem Value= "3" >Dentist</asp:ListItem>
                    </asp:DropDownList>
                    
                <%--<asp:SqlDataSource ID="ddldoctype" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:jrn_citrixcon %>" 
                    SelectCommand="SELECT jDokter, jNamaDokter, tdokter FROM H_A21504 WHERE (tdokter = @tdokter)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddltdokter" Name="tdokter" 
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>--%>
                </EditItemTemplate>
                <FooterTemplate>
                   <%--<asp:TextBox ID="txtNewdoctortype" runat="server" style="width:80px"></asp:TextBox>--%>
                   <asp:DropDownList ID="ddlnewdoctortype" runat ="server" Width="70px" AutoPostBack = "true" OnSelectedIndexChanged = "ddlnewdoctortype_SelectedIndexChanged">
                    <asp:ListItem Value= "1" >General</asp:ListItem>
                    <asp:ListItem Value= "2" >Specialist</asp:ListItem>
                    <asp:ListItem Value= "3" >Dentist</asp:ListItem>
                   </asp:DropDownList>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label32" runat="server" Text='<%# Bind("jdokternama") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Specialist">
                <EditItemTemplate>
                    <%--<asp:textbox ID="txtspecialist" Width="80px" runat="server" Text='<%# Eval("kdspesialis") %>'></asp:textbox>--%>
                    <asp:DropDownList ID="ddlspecialist" runat = "server" Width="150px" Font-Size="1.0em">
                        <%--<asp:ListItem Value = ""> </asp:ListItem>
                        <asp:ListItem Value = "01">Penyakit Dalam</asp:ListItem>
                        <asp:ListItem Value = "02">Bedah Ortopedi</asp:ListItem>
                        <asp:ListItem Value = "03">Kebidanan dan kandungan</asp:ListItem>
                        <asp:ListItem Value = "04">Anak</asp:ListItem>
                        <asp:ListItem Value = "05">Mata</asp:ListItem>
                        <asp:ListItem Value = "06">THT</asp:ListItem>
                        <asp:ListItem Value = "07">Saraf</asp:ListItem>
                        <asp:ListItem Value = "08">Kulit dan Kelamin</asp:ListItem>--%>
                    </asp:DropDownList>
                <%--<asp:SqlDataSource ID="dtspecialist" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:jrn_citrixcon %>" 
                    SelectCommand="select kdspesialis,nmspesialis from H_A21505 where stedit <> '2'">
                </asp:SqlDataSource>--%>
                </EditItemTemplate>
                <FooterTemplate>
                   <%--<asp:TextBox ID="txtNewspecialist" runat="server" style="width:80px"></asp:TextBox>--%>
                <asp:DropDownList ID="ddlnewspecialist" Width="150px" runat ="server" Font-Size="1.0em">
                </asp:DropDownList>
               <%-- <asp:SqlDataSource ID="dtnewspecialist" runat="server" ConnectionString="<%$ ConnectionStrings:jrn_citrixcon %>" 
                    SelectCommand="select kdspesialis,nmspesialis from H_A21505 where stedit <> '2'">
                </asp:SqlDataSource>--%>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label34" runat="server" Text='<%# Bind("nmspesialis") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Diagnose">
                <EditItemTemplate>
                    <asp:textbox ID="txtdiagnose" Width="75px" runat="server" Text='<%# Eval("kddiagnosa") %>' Font-Size="1.0em" MaxLength="30"></asp:textbox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtNewdiagnose" runat="server" style="width:75px" Font-Size="1.0em" MaxLength="30"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label36" runat="server" Text='<%# Bind("kddiagnosa") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Doctor/Hospital/ Clinic Name">
                <EditItemTemplate>
                    <asp:textbox ID="txtdoctorname" Width="75px" runat="server" Text='<%# Eval("dokter_remarks") %>' Font-Size="1.0em"></asp:textbox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtNewdoctorname" runat="server" style="width:75px" Font-Size="1.0em"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label38" runat="server" Text='<%# Bind("dokter_remarks") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Remarks">
                <EditItemTemplate>
                    <asp:textbox ID="txtremarks" Width="75px" runat="server" Text='<%# Eval("dremarks") %>' Font-Size="1.0em"></asp:textbox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtNewremarks" runat="server" style="width:75px" Font-Size="1.0em"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label40" runat="server" Text='<%# Bind("dremarks") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Consultation<br>(IDR)" HeaderStyle-Width="300px">
                <EditItemTemplate>
                   <asp:textbox ID="txtbiayakons" Width="80px" runat="server" onkeypress="return isNumberKey(event)" Text='<%# Eval("biayakons") %>' Font-Size="1.0em"></asp:textbox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewbiayakons" runat="server" onkeypress="return isNumberKey(event)" style="width:80px" Font-Size="1.0em"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label10" runat="server" Text='<%# Bind("biayakons", "{0:#,##.00}") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Medicine<br>(IDR)" HeaderStyle-Width="80px">
                <EditItemTemplate>
                   <asp:textbox ID="txtbiayaobat" Width="80px" runat="server" onkeypress="return isNumberKey(event)" Text='<%# Eval("biayaobat") %>' Font-Size="1.0em"></asp:textbox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewbiayaobat" runat="server" onkeypress="return isNumberKey(event)" style="width:80px" Font-Size="1.0em"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label20" runat="server" Text='<%# Bind("biayaobat", "{0:#,##.00}") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Laboratory<br>(IDR)" HeaderStyle-Width="80px">
                <EditItemTemplate>
                   <asp:textbox ID="txtbiayalab" Width="80px" runat="server" onkeypress="return isNumberKey(event)" Text='<%# Eval("biayalab") %>' Font-Size="1.0em"></asp:textbox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewbiayalab" runat="server" onkeypress="return isNumberKey(event)" style="width:80px" Font-Size="1.0em"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label24" runat="server" Text='<%# Bind("biayalab", "{0:#,##.00}") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Other<br>(IDR)" HeaderStyle-Width="80px">
                <EditItemTemplate>
                   <asp:textbox ID="txtbiayalain" Width="80px" runat="server" onkeypress="return isNumberKey(event)" Text='<%# Eval("biayalain") %>' Font-Size="1.0em"></asp:textbox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:TextBox ID="txtNewbiayalain" runat="server" onkeypress="return isNumberKey(event)" style="width:80px" Font-Size="1.0em"></asp:TextBox>  
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label22" runat="server" Text='<%# Bind("biayalain", "{0:#,##.00}") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Total<br>(IDR)" HeaderStyle-Width="80px">
                <EditItemTemplate>
                   <asp:textbox ID="txtbiayatot" Width="80px" runat="server" onkeypress="return readonly(event)" Text='<%# Eval("biayatot") %>' Font-Size="1.0em"></asp:textbox>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:Label ID="lblnewbiaya" runat="server" Width="80px" BackColor="#FCFFA3">  </asp:Label>
                   <asp:TextBox ID="txtNewbiayatot" runat="server" Visible="false" onkeypress="return readonly(event)" style="width:80px" Font-Size="1.0em"></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label26" runat="server" Text='<%# Bind("biayatot", "{0:#,##.00}") %>'></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="" ShowHeader="False" FooterStyle-BorderStyle="None" HeaderStyle-BorderStyle="None" ItemStyle-BorderStyle="None">
               <EditItemTemplate>
                <div style="width:85px;">
                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update">
                   </asp:LinkButton>
                   <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel">
                   </asp:LinkButton>
               </div>
               </EditItemTemplate>
               <FooterTemplate>
               <div style="width:30px;">
                   <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="AddNew" Text="Save"></asp:LinkButton>
                </div> 
               </FooterTemplate>
               <ItemTemplate>
               
                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
               
               </ItemTemplate>
           </asp:TemplateField>
           
                <asp:CommandField HeaderText="" HeaderStyle-BorderStyle="None" ShowDeleteButton="True" ShowHeader="False" ItemStyle-BorderStyle="None" FooterStyle-BorderStyle="None"/>
           
       </Columns>
        <FooterStyle Width="300px" />
   </asp:GridView>
   
   <div style="width:106%; border-top-style:solid; border-top-color:Black; margin-top: 20px;">
        <table class="data-table" style="width:100%;">
            <tr>
                <td style="text-align:right;">
                    <asp:Label ID="lblinfo" runat="server" style="color:Red;"></asp:Label>
                </td>
                <td style="text-align:right;">
                   <strong>Previous Balance</strong> <asp:TextBox Width="120px" BackColor="#FCFFA3" ID="txtplafon" runat="server" style="text-align:right;"></asp:TextBox>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                   <strong>Total</strong> 
                   <asp:TextBox Width="120px" BackColor="#FCFFA3" ID="txttotal" runat="server" style="text-align:right;"></asp:TextBox>       
                </td>
            </tr>
        </table>
   </div>
   <asp:HiddenField ID="cnt" runat = "server" /> <asp:HiddenField ID="cnt2" runat = "server" />
   </div>
   
   <asp:Button ID="btnsave" runat="server" Text="SAVE" Font-Size="1.0em"/> 
   <asp:Button ID="btnprint" runat="server" Text="PRINT" Font-Size="1.0em"/> 
   <asp:Button ID="btndelete" runat="server" Text="DELETE" Font-Size="1.0em"/> 
   <asp:Button ID="btnPaymentUsage" runat="server" Text="PRINT PA" Font-Size="1.0em" /> 
   <asp:HiddenField runat="server" ID="IS_PENJOM" Value="False" />
   
   <%=js%>
   <%=msg%>
   <script>
       var IS_PENJOM = $('#ctl00_ContentPlaceHolder1_IS_PENJOM').val();

       if (IS_PENJOM == 'True') {
           var thTable = $('#ctl00_ContentPlaceHolder1_GridView1 tbody tr th');
           for (i = 0; i < thTable.length; i++) {
               $(thTable[i]).html($(thTable[i]).text().replace('(IDR)', '<br />(MYR)'));
           }
       }
       
   </script>
</asp:Content>