﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="popupcluster.aspx.vb" Inherits="EXCELLENT.popupcluster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <script type="text/javascript">
       var counter = 1;
       function addRowToFTable(num1,id, no, source, responsibility, prepared, cutofdate) {
          var tbl = document.getElementById("cluster");
          var nextRow = tbl.tBodies[0].rows.length;
          var num = nextRow;
          if (num1 != undefined)num = document.getElementById(num1).value;  
          var row = tbl.tBodies[0].insertRow(num);

          var cell0 = row.insertCell(0);
          var txtInp = document.createElement('input');
          txtInp.setAttribute('type', 'hidden');
          txtInp.setAttribute('id', 'cid' + counter + 1);
          if (id != undefined) {
             txtInp.setAttribute('value', id);
          }
          cell0.appendChild(txtInp);
          var txtInp = document.createElement('input');
          txtInp.setAttribute('type', 'hidden');
          txtInp.setAttribute('id', 'cstate' + counter + 1);
          txtInp.setAttribute('value', '');
          if (id != undefined) {
             txtInp.setAttribute('value', id);
          }
          cell0.appendChild(txtInp);
          var txtInp = document.createElement('input');
          txtInp.setAttribute('type', 'text');
          txtInp.setAttribute('name', 'cnomor' + counter + 1);
          txtInp.setAttribute('id', 'cnomor' + counter + 1);
          txtInp.setAttribute('style', 'width:90%');
          txtInp.setAttribute('class', 'plain');
          txtInp.setAttribute('value', counter + 1);
          cell0.appendChild(txtInp);

          var cell1 = row.insertCell(1);
          var txtInp = document.createElement('input');
          txtInp.setAttribute('type', 'text');
          txtInp.setAttribute('name', 'cactual' + counter + 1);
          txtInp.setAttribute('id', 'cactual' + counter + 1);
          txtInp.setAttribute('style', 'width:99%');
          txtInp.setAttribute('class', 'plain');
          if (source != undefined) {
             txtInp.setAttribute('value', cutofdate);
          }
          cell1.appendChild(txtInp);

          var cell2 = row.insertCell(2);
          var txtInp = document.createElement('input');
          txtInp.setAttribute('type', 'text');
          txtInp.setAttribute('name', 'cach' + counter + 1);
          txtInp.setAttribute('id', 'cach' + counter + 1);
          txtInp.setAttribute('style', 'width:99%');
          txtInp.setAttribute('class', 'plain');
          if (responsibility != undefined) {
             txtInp.setAttribute('value', responsibility);
          }
          cell2.appendChild(txtInp);

          var cell3 = row.insertCell(3);
          var txtInp = document.createElement('input');
          txtInp.setAttribute('type', 'button');
          txtInp.setAttribute('name', 'cadd' + counter + 1);
          txtInp.setAttribute('id', 'cadd' + counter + 1);
          txtInp.setAttribute('style', 'background-image: url(../images/add.gif); background-repeat: no-repeat;cursor:hand;Width:15px;Height:15px;background-color:transparent;border:0');
          txtInp.setAttribute('onclick', 'addRowToFTable(\'' + 'cnomor' + counter + 1 + '\')');
          cell3.appendChild(txtInp);

          var cell4 = row.insertCell(4);
          var txtInp = document.createElement('input');
          txtInp.setAttribute('type', 'button');
          txtInp.setAttribute('name', 'cdel' + counter + 1);
          txtInp.setAttribute('id', 'cdel' + counter + 1);
          txtInp.setAttribute('style', 'background-image: url(../images/delete.gif); background-repeat: no-repeat;cursor:hand;Width:15px;Height:15px;background-color:transparent;border:0');
          txtInp.setAttribute('onclick', 'delFRow(' + parseFloat(counter) + ',\'' + 'cid' + counter + 1 + '\',\'' + 'cstate' + counter + 1 + '\',\'' + 'cnomor' + counter + 1 + '\')');
          cell4.appendChild(txtInp);
          counter++;
          reorderRowsF(document.getElementById("cluster"));
       }      

       function delFRow(rownum, id, state, fnomor) {
          var answer = confirm("Anda Yakin Menghapus Data ini?");
          if (answer) {
             var tbl = document.getElementById("cluster");
             if (document.getElementById(id).value == '') {
                tbl.tBodies[0].deleteRow(document.getElementById(fnomor).value - 1);
                if (tbl.tBodies[0].rows.length == 0)
                   addRowToFTable();
                reorderRowsF(tbl);
             } else {
                tbl.tBodies[0].rows[rownum].style.backgroundColor = "gray";
                document.getElementById(state).value = "DEL";
                tbl.tBodies[0].rows[rownum].cells[0].getElementsByTagName('input')[3].readOnly = true;
                tbl.tBodies[0].rows[rownum].cells[0].getElementsByTagName('input')[3].style.backgroundColor = "gray";
                tbl.tBodies[0].rows[rownum].cells[1].getElementsByTagName('input')[0].readOnly = true;
                tbl.tBodies[0].rows[rownum].cells[1].getElementsByTagName('input')[0].style.backgroundColor = "gray";
                tbl.tBodies[0].rows[rownum].cells[2].getElementsByTagName('input')[0].readOnly = true;
                tbl.tBodies[0].rows[rownum].cells[2].getElementsByTagName('input')[0].style.backgroundColor = "gray";
             }             
          }
       }

       function reorderRowsF(tbl) {
          if (tbl.tBodies[0].rows[0]) {
             var count = 1;
             for (var i = 0; i < tbl.tBodies[0].rows.length; i++) {
                if (tbl.tBodies[0].rows[i].style.display != 'none') {
                   tbl.tBodies[0].rows[i].cells[0].getElementsByTagName('input')[2].value = count;
                   count++;
                }
             }
          }
       }

       function save() {
          var sql = "";
          var table = document.getElementById("cluster");
          if (validasi() == true) {
             for (var r = 1, n = table.rows.length; r < n; r++) {
                if (table.rows[r].cells[0].getElementsByTagName('input')[1].value == "DEL") {
                   sql = sql + 'DEL*#*' + table.rows[r].cells[0].getElementsByTagName('input')[0].value + "~#~";
                } else if (table.rows[r].cells[0].getElementsByTagName('input')[1].value == "") { //new
                sql = sql + 'INS*#*' + table.rows[r].cells[0].getElementsByTagName('input')[2].value + '*#*'
                         + table.rows[r].cells[1].getElementsByTagName('input')[0].value + '*#*'
                         + table.rows[r].cells[2].getElementsByTagName('input')[0].value + '~#~';
                } else {
                sql = sql + 'UPD*#*' + table.rows[r].cells[0].getElementsByTagName('input')[0].value + table.rows[r].cells[0].getElementsByTagName('input')[2].value + '*#*'
                         + table.rows[r].cells[1].getElementsByTagName('input')[0].value + '*#*'
                         + table.rows[r].cells[2].getElementsByTagName('input')[0].value + '~#~';
                }
             }
          } else {
             return false;
          }
          document.getElementById("SQLTOSEND").value = sql;
          return true;
       }

       function validasi() {
          return true;
       }     
    </script>
    
    <style type="text/css">
     body{
      font-family:"trebuchet MS", verdana, sans-serif;
         font-size:small;
         }
       #wrapper{
         font: small Arial, Helvetica, sans-serif;
         font-size:small;
       }    
        td.styleHeader
        {
          background-color:#00CC00;
          color:white;
          font-weight:bold;
          font-size:medium;
        }
        input.plain
      {
         border: 1px solid #C0C0C0;
      }
      .plain:hover
      {
         background-color:#FFFFA8;
      }
      select.plains{
         border: 1px solid #C0C0C0;
      }  
      textarea{
         border: 1px solid #C0C0C0;
      }   
      table.act {
	      border-width: 1px;
	      border-spacing: 0px;
	      border-color: #C0C0C0;
	      border-collapse: collapse;
	      background-color: white;
         }
      table.act thead {
	      border-width: 1px;
	      padding: 1px;
	      border-color: #C0C0C0;
      }
      table.act td {
	      border-width: 1px;
	      padding: 1px;
	      border-color: #C0C0C0;
      }
    </style>
</head>
<body>
    <form id="form1" runat="server">
       <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
       </asp:ToolkitScriptManager>
       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
             <asp:Label id="lblError" runat="server" />
             <asp:HiddenField id="SQLTOSEND" runat="server" />
             <asp:HiddenField id="KPIID" runat="server" />
             <div id="wrapper">
               <table border="1" class="act" width="100%">
                  <tr>
                     <td colspan="2" class="styleHeader">Setting Cluster</td>
                  </tr>
                  <tr>
                     <td>KPI</td><td></td>
                  </tr>
                  <tr>
                     <td colspan="2">
                        <table id="cluster" style="width:50%" >
                           <thead>
                              <tr>
                                 <th style="width:1%;background-color:#EAE8E4">No.</th>
                                 <th style="width:20%;background-color:#EAE8E4">Actual</th>
                                 <th style="width:20%;background-color:#EAE8E4">Achievement</th>
                                 <th style="width:10%;background-color:#EAE8E4">Add</th>
                                 <th style="width:10%;background-color:#EAE8E4">Delete</th>
                              </tr>
                           </thead>
                           <tbody></tbody>
                        </table>
                     </td>
                  </tr>
                  <tr>
                      <td colspan="2"><asp:button runat="server" onClientClick="return save();" Width="55px" Height="55px" Style="background-image: url(../images/save-icon-small.png); background-repeat: no-repeat;cursor:hand;vertical-align:bottom" BackColor="Transparent" BorderStyle="None" id="btnSave"/></td>
                  </tr>
               </table>
             </div>
             </ContentTemplate>         
        </asp:UpdatePanel>
             <script type="text/javascript">addRowToFTable(); alert('afterload');</script>
             
    </form>
</body>
</html>
