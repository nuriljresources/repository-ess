﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Public Class edit_permit_doc
    Inherits System.Web.UI.Page
    Public opttype As String
    Public fsubject As String
    Public doctype As String
    Public rems As String
    Public mnentry As String
    Public mnview As String
    Public mnmgmt As String
    Public msgbox As String
    Dim myDt2 As DataTable
    Public mncontract As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("otorisasi_permit") = Nothing Or Session("otorisasi_permit") = "" Then
            Response.Redirect("signin_permit.aspx")
        End If

        'myDt2 = New DataTable()
        'myDt2 = CreateDataTable2()
        'Session("myDatatable2") = myDt2
        If Not IsPostBack Then
            Dim pid As String
            pid = Session("Pid")

            Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
            Dim strsql As String = "select id,ftype,fsubject,kdcom, company_name,kdsite,site_name,fyear,fcategory,doctype,department,subdepartment,number,must_report,cms_start,cms_end,f_path,f_name, rem_code, pic1_mail, pic2_mail, pic3_mail, pic4_mail, pic5_mail, createdtime, vendor from docpermit_trans where id='" + pid + "'"
            Dim ls_sql As String = "select id, nik, schedule, (select top 1 name from ms_user where ms_user.emp_code = docpermit_trans_sch.nik) as name from docpermit_trans_sch where id = '" + pid + "'"

            Dim dtb As DataTable = New DataTable
            Dim dtb2 As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strsql, sqlConn)
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(ls_sql, sqlConn)

            sda.Fill(dtb)
            sda2.Fill(dtb2)
            Session("datatable") = dtb2

            Dim ftype As String
            Dim dtype As String

            Me.GridView2.DataSource = dtb2
            Me.GridView2.DataBind()

            If dtb.Rows.Count > 0 Then
                ddlcomp.SelectedValue = dtb.Rows(0)!kdcom.ToString
                ddlsite.SelectedValue = dtb.Rows(0)!kdsite.ToString
                ftype = dtb.Rows(0)!ftype.ToString
                'If ftype = "Surat Masuk" Then
                'ltype.SelectedIndex = 0
                'opttype = "<select id='type'><option value='Surat Masuk' selected='selected'>Surat Masuk</option><option value='Surat Keluar'>Surat Keluar</option></select>"
                'ElseIf ftype = "Surat Keluar" Then
                'ltype.SelectedIndex = 1
                opttype = "<select id='type' style='visibility:hidden;'><option value=' '> </option></select>"
                ' End If

                fsubject = dtb.Rows(0)!fsubject.ToString
                subject.Value = fsubject

                'comp.Value = dtb.Rows(0)!company_name.ToString
                'site.Value = dtb.Rows(0)!site_name.ToString
                txtyear.Value = CDate(dtb.Rows(0)!createdtime).ToString("MM/dd/yyyy")
                category.Value = dtb.Rows(0)!fcategory.ToString
                dtype = dtb.Rows(0)!doctype.ToString
                txtvendor.Text = dtb.Rows(0)!vendor.ToString
                If dtype = "SK" Then
                    ltype.SelectedIndex = 0
                    'doctype = "<select id='doctype' style='width:200px;'><option value='SK'>SK</option><option value='SE'>SE</option><option value='SOP'>SOP</option><option value='WI'>WI</option><option value='SPP'>SPP</option><option value='IM'>Interoffice Memorandum</option><option value='ITM'>Internal Memorandum</option><option value='SKR'>Surat Keluar</option></select>"
                ElseIf dtype = "SE" Then
                    ltype.SelectedIndex = 1
                    'doctype = "<select id='doctype' style='width:200px;'><option value='SK' selected='selected'>SK</option><option value='SE' selected='selected'>SE</option><option value='SOP'>SOP</option><option value='WI'>WI</option><option value='SPP'>SPP</option><option value='IM'>Interoffice Memorandum</option><option value='ITM'>Internal Memorandum</option><option value='SKR'>Surat Keluar</option></select>"
                ElseIf dtype = "SOP" Then
                    ltype.SelectedIndex = 2
                    'doctype = "<select id='doctype' style='width:200px;'><option value='SK' selected='selected'>SK</option><option value='SE'>SE</option><option value='SOP' selected='selected'>SOP</option><option value='WI'>WI</option><option value='SPP'>SPP</option><option value='IM'>Interoffice Memorandum</option><option value='ITM'>Internal Memorandum</option><option value='SKR'>Surat Keluar</option></select>"
                ElseIf dtype = "WI" Then
                    ltype.SelectedIndex = 3
                    'doctype = "<select id='doctype' style='width:200px;'><option value='SK' selected='selected'>SK</option><option value='SE'>SE</option><option value='SOP'>SOP</option><option value='WI' selected='selected'>WI</option><option value='SPP'>SPP</option><option value='IM'>Interoffice Memorandum</option><option value='ITM'>Internal Memorandum</option><option value='SKR'>Surat Keluar</option></select>"
                ElseIf dtype = "SPP" Then
                    ltype.SelectedIndex = 4
                    'doctype = "<select id='doctype' style='width:200px;'><option value='SK' selected='selected'>SK</option><option value='SE'>SE</option><option value='SOP'>SOP</option><option value='WI'>WI</option><option value='SPP' selected='selected'>SPP</option><option value='IM'>Interoffice Memorandum</option><option value='ITM'>Internal Memorandum</option><option value='SKR'>Surat Keluar</option></select>"
                ElseIf dtype = "IM" Then
                    ltype.SelectedIndex = 5
                    'doctype = "<select id='doctype' style='width:200px;'><option value='SK' selected='selected'>SK</option><option value='SE'>SE</option><option value='SOP'>SOP</option><option value='WI'>WI</option><option value='SPP'>SPP</option><option value='IM' selected='selected'>Interoffice Memorandum</option><option value='ITM'>Internal Memorandum</option><option value='SKR'>Surat Keluar</option></select>"
                ElseIf dtype = "ITM" Then
                    ltype.SelectedIndex = 6
                    'doctype = "<select id='doctype' style='width:200px;'><option value='SK' selected='selected'>SK</option><option value='SE'>SE</option><option value='SOP'>SOP</option><option value='WI'>WI</option><option value='SPP'>SPP</option><option value='IM'>Interoffice Memorandum</option><option value='ITM' selected='selected'>Internal Memorandum</option><option value='SKR'>Surat Keluar</option></select>"
                ElseIf dtype = "SKR" Then
                    ltype.SelectedIndex = 7
                    'doctype = "<select id='doctype' style='width:200px;'><option value='SK' selected='selected'>SK</option><option value='SE'>SE</option><option value='SOP'>SOP</option><option value='WI'>WI</option><option value='SPP'>SPP</option><option value='IM'>Interoffice Memorandum</option><option value='ITM'>Internal Memorandum</option><option value='SKR' selected='selected'>Surat Keluar</option></select>"
                ElseIf dtype = "PMT" Then
                    ltype.SelectedIndex = 8
                ElseIf dtype = "DK" Then
                    ltype.SelectedIndex = 9
                End If

                Department.Value = dtb.Rows(0)!department.ToString
                subdept.Value = dtb.Rows(0)!subdepartment.ToString
                Number.Value = dtb.Rows(0)!number.ToString
                report.Value = dtb.Rows(0)!must_report.ToString
                dtissue.Value = dtb.Rows(0)!cms_start.ToString
                dtexp.Value = dtb.Rows(0)!cms_end.ToString

                dtissue.Value = CDate(dtissue.Value).ToString("MM/dd/yyyy")
                dtexp.Value = CDate(dtexp.Value).ToString("MM/dd/yyyy")

                If report.Value = 1 Then
                    rpt.Checked = True
                Else
                    rpt.Checked = False
                End If


                filename.Value = dtb.Rows(0)!f_name

                rem_days.SelectedValue = dtb.Rows(0)!rem_code
                'If Trim(dtb.Rows(0)!pic1_mail) <> "" Then
                '    txtpic1.SelectedValue = dtb.Rows(0)!pic1_mail
                '    chkpic1.Checked = True
                'Else
                '    txtpic1.Style.Add("visibility", "hidden")
                'End If
                'If Trim(dtb.Rows(0)!pic2_mail) <> "" Then
                '    txtpic2.SelectedValue = dtb.Rows(0)!pic2_mail
                '    chkpic2.Checked = True
                'Else
                '    txtpic2.Style.Add("visibility", "hidden")
                'End If
                'If Trim(dtb.Rows(0)!pic3_mail) <> "" Then
                '    txtpic3.SelectedValue = dtb.Rows(0)!pic3_mail
                '    chkpic3.Checked = True
                'Else
                '    txtpic3.Style.Add("visibility", "hidden")
                'End If
                'If Trim(dtb.Rows(0)!pic4_mail) <> "" Then
                '    txtpic4.SelectedValue = dtb.Rows(0)!pic4_mail
                '    chkpic4.Checked = True
                'Else
                '    txtpic4.Style.Add("visibility", "hidden")
                'End If
                'If Trim(dtb.Rows(0)!pic5_mail) <> "" Then
                '    txtpic5.SelectedValue = dtb.Rows(0)!pic5_mail
                '    chkpic5.Checked = True
                'Else
                '    txtpic5.Style.Add("visibility", "hidden")
                'End If
                txtUserName.Attributes.Add("readonly", "readonly")
                txtnik.Attributes.Add("readonly", "readonly")
                dtissue.Attributes.Add("readonly", "readonly")
                dtexp.Attributes.Add("readonly", "readonly")
            End If
        End If

        If Trim(Session("permission_permit")) = "1" Or Trim(Session("permission_permit")) = "2" Then
            mnentry = "<div id='mnentry'><img alt='entry document' src='../images/open_doc_small.png' /> <a href='doc_center_entry.aspx'>&nbsp;Set Document</a></div>"
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"

            If Session("dept").ToString().ToLower = "contract" Or Session("dept").ToString().ToLower() = "contract management system" Then
                mncontract = "<div id='mncontract'><img alt='view document' src='../images/open_doc_small.png' /> <a href='supp_permit_lisense_entry.aspx'>&nbsp;Set Permit & License</a></div>"
            End If
        End If

        If Trim(Session("permission_permit")) = "1" Then
            mnmgmt = "<div id='mnmgmt'> <img alt='Management User' src='../images/open_doc_small.png' /> <a href='mgmnt_list.aspx'>&nbsp;Management User</a></div>"
        End If

        If Trim(Session("permission_permit")) = "3" Or Trim(Session("permission_permit")) = "4" Then
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
        End If
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        tny.Text = "Are you sure want to remove this document reminder?"
        yes.Visible = True
        no.Visible = True
        btnsave2.Visible = False
        Button2.Visible = False
    End Sub

    Protected Sub yes_Click(ByVal sender As Object, ByVal e As EventArgs) Handles yes.Click
        Dim dtbfindemp As DataTable = New DataTable
        Dim cmd As SqlCommand
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Try
            sqlConn.Open()
            Dim sqlQuery As String = "UPDATE docpermit_trans SET stedit = '2', deleteby = '" + Session("otorisasi_permit") + "', deletetime = '" + DateTime.Now + "' where id = '" + Session("Pid").ToString + "'"
            cmd = New SqlCommand(sqlQuery, sqlConn)
            cmd.ExecuteScalar()
            msgbox = "<script language='javascript' type='text/javascript'> alert('data has been deleted') </script>"
            Session("docedituser") = ""

            Response.Redirect("permit_list.aspx")
        Catch ex As Exception
            msgbox = "<script language='javascript' type='text/javascript'> alert('Failed to delete data') </script>"
        Finally
            sqlConn.Close()
        End Try
    End Sub

    Protected Sub no_Click(ByVal sender As Object, ByVal e As EventArgs) Handles no.Click
        tny.Text = ""
        yes.Visible = False
        no.Visible = False
        btnsave2.Visible = True
        Button2.Visible = True
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

    Dim li_count As Integer

        If GridView2.Rows.Count > 0 Then
            For li_count = 0 To GridView2.Rows.Count - 1
                If GridView2.DataKeys(li_count).Value.ToString = txtnik.Text Then
                    lblTips.Text = "User already exists in the list"
                    Exit Sub
                End If
            Next
        End If

        If Me.txtUserName.Text.Trim().Equals("") Then

            Me.lblTips.Text = "You must fill a username."

        Else

            AddDataToTable2(Me.txtUserName.Text.Trim(), Me.txtnik.Text.Trim(), Me.ddlsch.SelectedValue.Trim(), CType(Session("datatable"), DataTable))

            Me.GridView2.DataSource = CType(Session("datatable"), DataTable).DefaultView

            Me.GridView2.DataBind()

            Me.txtUserName.Text = ""
            Me.txtnik.Text = ""
            Me.lblTips.Text = ""
            Me.ddlsch.SelectedValue = ""

        End If


    End Sub

    Private Function CreateDataTable2() As DataTable
        Dim myDataTable2 As DataTable = New DataTable()

        Dim myDataColumn2 As DataColumn

        myDataColumn2 = New DataColumn()
        myDataColumn2.DataType = Type.GetType("System.String")
        myDataColumn2.ColumnName = "Name"
        myDataTable2.Columns.Add(myDataColumn2)

        myDataColumn2 = New DataColumn()
        myDataColumn2.DataType = Type.GetType("System.String")
        myDataColumn2.ColumnName = "Nik"
        myDataTable2.Columns.Add(myDataColumn2)

        myDataColumn2 = New DataColumn()
        myDataColumn2.DataType = Type.GetType("System.String")
        myDataColumn2.ColumnName = "Schedule"
        myDataTable2.Columns.Add(myDataColumn2)

        Return myDataTable2
    End Function

    Private Sub AddDataToTable2(ByVal name As String, ByVal nik As String, ByVal Schedule As String, ByVal myTable2 As DataTable)
        Dim row2 As DataRow

        row2 = myTable2.NewRow()

        row2("Name") = name
        row2("Nik") = nik
        row2("Schedule") = Schedule

        myTable2.Rows.Add(row2)

    End Sub

    Private Sub GridView2_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridView2.RowDeleting
        Dim row As GridViewRow = GridView2.SelectedRow
        'Dim pid As String
        Dim i As Integer

        i = e.RowIndex
        'pid = GridView2.DataKeys(i).Value.ToString

        'GridView2.DeleteRow(i)
        Dim dtblist As DataTable = New DataTable()
        dtblist.Merge(Session("datatable"))
        dtblist.Rows.RemoveAt(i)
        Me.GridView2.DataSource = dtblist
        Session("datatable") = dtblist
        Me.GridView2.DataBind()
    End Sub

    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
    '    Dim pid As String
    '    pid = Session("Pid")
    '    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

    '    If rpt.Checked = True Then
    '        report.Value = 1
    '    Else
    '        report.Value = 0
    '    End If
    '    conn.Open()
    '    Try
    '        Dim sqlquery As String = "update docpermit_trans set ftype= '" + ltype.SelectedValue + "', fsubject='" + subject.Value + "', company_name='" + comp.Value + "',site_name='" + site.Value + "', fyear='" + txtyear.Value + "', fcategory = '" + category.Value + "',doctype = '" + ldoctype.SelectedValue + "', department = '" + Department.Value + "', subdepartment = '" + subdept.Value + "', number = '" + Number.Value + "', must_report = '" + report.Value + "', cms_start = '" + dtissue.Value + "', cms_end='" + dtexp.Value + "' where id = '" + pid + "'"
    '        Dim cmd As SqlCommand
    '        cmd = New SqlCommand(sqlquery, conn)
    '        cmd.ExecuteScalar()
    '    Catch ex As Exception

    '    Finally
    '        conn.Close()
    '    End Try

    'End Sub

    'Protected Sub uploaddoc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles uploaddoc.Click
    '    Response.Redirect("entry_permit_doc.aspx")
    'End Sub

    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
    'Dim pid As String
    'pid = Session("Pid")
    'Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
    'Dim strsql As String = "select f_name from docpermit_trans where id='" + pid + "'"

    'Dim dtb As DataTable = New DataTable
    'Dim sda As SqlDataAdapter = New SqlDataAdapter(strsql, sqlConn)
    'sda.Fill(dtb)

    'Dim filepath As String = "C:\" + f_name

    'If File.Exists(filepath) Then
    '    ' Give a new name
    '    My.Computer.FileSystem.RenameFile(filepath, "")
    'Else
    '    ' Use existing name
    'End If
    'End Sub

    Private Sub btnsave2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave2.Click
        Dim pid As String
        Dim nmcomp As String
        Dim nmsite As String
        Dim dttm As String
        Dim nik1 As String
        Dim nik2 As String
        Dim nik3 As String
        Dim nik4 As String
        Dim nik5 As String
        Dim li_count As Integer
        Dim lcom As SqlCommand
        Dim dtblist As DataTable = New DataTable()
        dtblist.Merge(Session("datatable"))

        pid = Session("Pid")
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)

        conn.Open()

        Try
            Dim strcon2 As String = "select Nmcompany from jrn_test.dbo.H_A110 where kdcompany = '" + ddlcomp.SelectedValue + "'"
            Dim dtb2 As DataTable = New DataTable
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, conn)
            sda2.Fill(dtb2)

            nmcomp = dtb2.Rows(0)!Nmcompany.ToString

            Dim strcon3 As String = "select Nmsite from jrn_test.dbo.H_A120 where kdsite = '" + ddlsite.SelectedValue + "'"
            Dim dtb3 As DataTable = New DataTable
            Dim sda3 As SqlDataAdapter = New SqlDataAdapter(strcon3, conn)
            sda3.Fill(dtb3)

            nmsite = dtb3.Rows(0)!Nmsite.ToString
            dttm = Date.Now.ToString

            'Dim sqlquery As String = "update docpermit_trans set ftype= '" + _type + "', fsubject='" + _subject + "', kdcom ='" + _comp + "', company_name='" + nmcomp + "', kdsite ='" + _site + "', site_name='" + nmsite + "', fyear='" + _year + "', fcategory = '" + _category + "',doctype = '" + _doctype + "', department = '" + _department + "', subdepartment = '" + _subdept + "', number = '" + _number + "', must_report = '" + _report + "', cms_start = '" + _dtissue + "', cms_end='" + _dtexp + "', f_name = '" + _fname + "', modifiedtime = '" + dttm + "', rem_code = '" + _rem_days + "' where [id] = '" + pid + "'"

            Dim sqlquery As String = "update docpermit_trans set fsubject ='" + subject.Value + "', kdcom ='" + ddlcomp.SelectedValue + "', company_name='" + nmcomp + "', kdsite ='" + ddlsite.SelectedValue + "', site_name='" + nmsite + "',doctype = '" + ltype.SelectedValue + "', must_report = '1', cms_start = '" + dtissue.Value + "', cms_end='" + dtexp.Value + "', f_name = '" + filename.Value + "', modifiedtime = '" + dttm + "', rem_code = '" + rem_days.SelectedValue + "', modifiedby = '" + Session("otorisasi_permit") + "', vendor = '" + txtvendor.Text + "' where [id] = '" + pid + "'"
            'Dim sqlquery As String = "update docpermit_trans set ftype= '" + _type + "', fsubject='" + _subject + "', kdcom ='" + _comp + "', company_name='" + nmcomp + "', kdsite ='" + _site + "', site_name='" + nmsite + "', fyear='" + _year + "', fcategory = '" + _category + "',doctype = '" + _doctype + "', department = '" + _department + "', subdepartment = '" + _subdept + "', number = '" + _number + "', must_report = '" + _report + "', cms_start = '" + _dtissue + "', cms_end='" + _dtexp + "', f_name = '" + _fname + "', f_path = 'C:\uploads\" + _type + "_" + _subject + "_" + _comp + "_" + _site + "_" + _year + "_" + _fname + "' where [id] = '" + pid + "'"

            Dim cmd As SqlCommand
            cmd = New SqlCommand(sqlquery, conn)
            cmd.ExecuteScalar()

            Dim ls_delquery As String = "Delete docpermit_trans_sch where id = '" + pid + "'"
            Dim ls_delcmd As SqlCommand 
            ls_delcmd = New SqlCommand(ls_delquery, conn)
            ls_delcmd.ExecuteScalar()

            Dim ls_insquery As String
            For li_count = 0 To dtblist.Rows.Count - 1
                ls_insquery = "insert into docpermit_trans_sch (id, nik, schedule) values ('" + pid + "', '" + dtblist.Rows(li_count)!Nik.ToString + "', '" + dtblist.Rows(li_count)!Schedule.ToString + "')"
                lcom = New SqlCommand(ls_insquery, conn)
                lcom.ExecuteScalar()
            Next

            msgbox = "<script type ='text/javascript'> alert('Data has been saved'); </script>"
        Catch ex As Exception
        
        Finally
            conn.Close()
        End Try
        
    End Sub
End Class