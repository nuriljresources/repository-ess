﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Reflection

Public Class TableAdapterHelper

    Public Shared Function BeginTransaction(ByVal tableAdapter As Object) As SqlTransaction
        Return BeginTransaction(tableAdapter, IsolationLevel.ReadUncommitted)
    End Function

    Public Shared Function BeginTransaction(ByVal tableAdapter As Object, ByVal isolationLevel As IsolationLevel) As SqlTransaction
        ' get the table adapter's type
        Dim type As Type = tableAdapter.[GetType]()

        ' get the connection on the adapter
        Dim connection As SqlConnection = GetConnection(tableAdapter)

        ' make sure connection is open to start the transaction
        If connection.State = ConnectionState.Closed Then
            connection.Open()
        End If

        ' start a transaction on the connection
        Dim transaction As SqlTransaction = connection.BeginTransaction(isolationLevel)

        ' set the transaction on the table adapter
        SetTransaction(tableAdapter, transaction)

        Return transaction
    End Function

    ''' <summary>
    ''' Gets the connection from the specified table adapter.
    ''' </summary>
    Private Shared Function GetConnection(ByVal tableAdapter As Object) As SqlConnection
        Dim type As Type = tableAdapter.[GetType]()
        Dim connectionProperty As PropertyInfo = type.GetProperty("Connection", BindingFlags.NonPublic Or BindingFlags.Instance)
        Dim connection As SqlConnection = DirectCast(connectionProperty.GetValue(tableAdapter, Nothing), SqlConnection)
        Return connection
    End Function

    ''' <summary>
    ''' Sets the connection on the specified table adapter.
    ''' </summary>
    Private Shared Sub SetConnection(ByVal tableAdapter As Object, ByVal connection As SqlConnection)
        Dim type As Type = tableAdapter.[GetType]()
        Dim connectionProperty As PropertyInfo = type.GetProperty("Connection", BindingFlags.NonPublic Or BindingFlags.Instance)
        connectionProperty.SetValue(tableAdapter, connection, Nothing)
    End Sub

    ''' <summary>
    ''' Enlists the table adapter in a transaction.
    ''' </summary>
    Public Shared Sub SetTransaction(ByVal tableAdapter As Object, ByVal transaction As SqlTransaction)
        ' get the table adapter's type
        Dim type As Type = tableAdapter.[GetType]()

        ' set the transaction on each command in the adapter
        Dim commandsProperty As PropertyInfo = type.GetProperty("CommandCollection", BindingFlags.NonPublic Or BindingFlags.Instance)
        Dim commands As SqlCommand() = DirectCast(commandsProperty.GetValue(tableAdapter, Nothing), SqlCommand())


        Dim connectionProperty As PropertyInfo = type.GetProperty("Transaction", BindingFlags.NonPublic Or BindingFlags.Instance)
        connectionProperty.SetValue(tableAdapter, transaction, Nothing)

        For Each command As SqlCommand In commands
            command.Transaction = transaction
        Next

        ' set the connection on the table adapter
        SetConnection(tableAdapter, transaction.Connection)
    End Sub
End Class
