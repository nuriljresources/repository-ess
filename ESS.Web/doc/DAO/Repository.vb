﻿Public Class Repository
    Private _dataContext As ExcellentDataContext

    Public Sub New()
        _dataContext = New ExcellentDataContext()
    End Sub

    Public Sub Save(ByVal image As IMAGE)
        Try
            _dataContext.IMAGEs.InsertOnSubmit(image)
            _dataContext.SubmitChanges()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Function Save(ByVal _hfpp As HFPP, ByVal _image As List(Of IMAGE)) As HFPP
        Try
            If _hfpp.NoFPP Is Nothing Then
                Dim max As String = (From x In _dataContext.HFPPs Where x.Tgl.Value.Year = Year(_hfpp.Tgl) And x.Tgl.Value.Month = Month(_hfpp.Tgl) Select x.NoFPP).Max()
                If max = "" Then
                    _hfpp.NoFPP = "FPP/HO/IT" & Year(_hfpp.Tgl) & "/" & Format(Month(_hfpp.Tgl), "00") & "/" & "0001"
                Else
                    _hfpp.NoFPP = Microsoft.VisualBasic.Left(max, 12) & Format(CInt(Microsoft.VisualBasic.Right(max, 4)) + 1, "0000")
                End If
                _hfpp.CreatedTime = Now
                For Each odfpp As DFPP In _hfpp.DFPPs
                    If odfpp.StEdit = "5" Then
                        odfpp.StEdit = "0"
                    End If
                Next
                _dataContext.HFPPs.InsertOnSubmit(_hfpp)
                _dataContext.SubmitChanges()
            ElseIf _hfpp.NoFPP = "" Then
                Dim max As String = (From x In _dataContext.HFPPs Where x.Tgl.Value.Year = Year(_hfpp.Tgl) And x.Tgl.Value.Month = Month(_hfpp.Tgl) Select x.NoFPP).Max()
                If max = "" Then
                    _hfpp.NoFPP = "FPP/" & Year(_hfpp.Tgl) & "/" & Format(Month(_hfpp.Tgl), "00") & "/" & "0001"
                Else
                    _hfpp.NoFPP = Microsoft.VisualBasic.Left(max, 12) & Format(CInt(Microsoft.VisualBasic.Right(max, 4)) + 1, "0000")
                End If
                _hfpp.CreatedTime = Now
                For Each odfpp As DFPP In _hfpp.DFPPs
                    If odfpp.StEdit = "5" Then
                        odfpp.StEdit = "0"
                    End If
                Next
                _dataContext.HFPPs.InsertOnSubmit(_hfpp)
                _dataContext.SubmitChanges()
            Else
                _dataContext = New ExcellentDataContext()
                _dataContext.ObjectTrackingEnabled = False
                Dim originHFPP As HFPP = (From x In _dataContext.HFPPs Where x.NoFPP = _hfpp.NoFPP Select x).Single()
                Dim originDFPP As List(Of DFPP) = (From x In _dataContext.DFPPs Where x.NoFPP = _hfpp.NoFPP Select x).ToList()
                originHFPP.DFPPs.AddRange(originDFPP)
                For Each _dfpp As DFPP In _hfpp.DFPPs
                    If _dfpp.NoFPP Is Nothing Then
                        _dfpp.NoFPP = _hfpp.NoFPP
                    ElseIf _dfpp.NoFPP = "" Then
                        _dfpp.NoFPP = _hfpp.NoFPP
                    End If
                Next
                Dim oDFPP As List(Of DFPP)
                oDFPP = _hfpp.DFPPs.ToList()
                _dataContext = New ExcellentDataContext()
                _dataContext.HFPPs.Attach(_hfpp, originHFPP)
                _dataContext.DFPPs.AttachAll(oDFPP.Where(Function(item) item.StEdit = "0"))
                _dataContext.Refresh(Data.Linq.RefreshMode.KeepCurrentValues, oDFPP)
                _dataContext.DFPPs.InsertAllOnSubmit(oDFPP.Where(Function(item) item.StEdit <> "0"))
                _dataContext.SubmitChanges()
            End If
            If Not _image Is Nothing Then
                UpdateImage(_image, _hfpp.NoFPP)
            End If
            Return _hfpp
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Public Sub UpdateImage(ByVal _image As List(Of IMAGE), ByVal noFPP As String)
        Try
            Dim LOfString As List(Of String)
            LOfString = (From p In _image Select p.FileID).ToList()
            Dim _imageForUpdate As List(Of IMAGE) = (From x In _dataContext.IMAGEs Where LOfString.Contains(x.FileID) Select x).ToList()
            For Each _oimage In _imageForUpdate
                _oimage.Reference2 = noFPP
            Next
            _dataContext.SubmitChanges()
        Catch ex As Exception

        End Try
    End Sub

    Public Function SearchHFPP(ByVal tglawal As DateTime, ByVal tglakhir As DateTime) As List(Of HFPP)
        Try
            Return (From x In _dataContext.HFPPs Where x.Tgl.Value >= tglawal And x.Tgl.Value <= tglakhir Select x).ToList()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Public Function DeleteHFPP(ByVal noFPP As String)
        Try
            Dim oHFPP As HFPP = _
            (From x In _dataContext.HFPPs Where x.NoFPP = noFPP).Single()
            Dim oDFPP As List(Of DFPP) = _
            (From x In _dataContext.DFPPs Where x.NoFPP = noFPP).ToList()
            _dataContext.HFPPs.DeleteOnSubmit(oHFPP)
            _dataContext.DFPPs.DeleteAllOnSubmit(oDFPP)
            _dataContext.SubmitChanges()
        Catch ex As Exception

        End Try
    End Function

End Class
