﻿var tail = '<tr><td colspan="4"n align="left"><br/><span>Remark : (<span style="color:#FF0000"><b>Red Bold = Over Due</b></span>, <span style="color:#FF0000">Red = H-3</span>, Black = Normal)<span/></td></tr>';

var gIssue = new Array();var nikpic = "";var tempissue = ""; var tempaction = ""; 
var gpage = 0;var gpageop = 0;var batas = 10; var firstfind = true;var nlast = 0;
var doc = document;var halaman = "";var n1 = "";var n2 = "";var n3 = "";var currpage = 1;var originopsi = 0;

function CompareDates(stgl) {
    var thn = parseInt(stgl.substring(6, 10), 10); var bln = parseInt(stgl.substring(3, 5), 10); var tgl = parseInt(stgl.substring(0, 2), 10);
    var date = new Date(thn, bln, tgl); return date;
}

function retrieveIssueHead(page) {
    var d = document;
    d.getElementById('divmain').innerHTML = "";
    d.getElementById('divprogress').style.display = 'block'; 
    d.getElementById("menutop").style.display = "none";
    d.getElementById("divbutton").style.display = "block";
    
    var nik = d.getElementById('TxtNIK').value;
    nikpic = nik; gpage = page;

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "WS/MyList.asmx/GetMasterList",
        data: "{'_page':'" + gpage + "','_iduser':'" + nikpic + "'}",
        dataType: "json",
        success: function(res) {
            d.getElementById('divprogress').style.display = 'none';
            MyIssue = new Array();
            MyIssue = res.d.MyTaskList;
            gIssue = MyIssue;

            var slist = '<center>';
            slist = slist + '<table id="tabgrid" width="100%" cellpadding="0" cellspacing="2">';
            slist = slist + '<caption class="captiongrid" />LIST ISSUE</caption>';
            slist = slist + '<tr>';
            slist = slist + '<th class="thstart">No</th><th class="thstart">No Notulen</th><th class="thstart">Type</th><th class="thstart">Issue</th>';
            slist = slist + '<th class="thstart">Last Progress</th><th class="thstart">Due To</th><th class="thstart">PIC [Status Action]</th>';
            slist = slist + '<th class="thstart">Status Issue</th><th class="thstart">Info To</th><th class="thstart">Action</th>';
            slist = slist + '</tr>';

            var j = 0;
            for (var i = 0; i < res.d.MyTaskList.length; i++) {
                var ind = MyIssue[i].Indeks;
                var sid = MyIssue[i].IDMeet;
                var sis = MyIssue[i].NoIssue;
                var sia = MyIssue[i].NoRef;
                var pic = MyIssue[i].Nmpic;
                var nin = MyIssue[i].NmInfo;
                var sts = (MyIssue[i].status == '0') ? 'Open' : 'Close';
                var sign;
                
                var due = MyIssue[i].due; var cls = MyIssue[i].closed;
                if (sts == 'Close') {
                    if (CompareDates(due) < CompareDates(cls)){
                        sign = ' style="color: #F00" ';
                    }
                    else{
                        sign = ' style="color: #000" ';
                    }
                }
                else {
                    sign = getColor(MyIssue[i].Flag);
                }

                n1 = MyIssue[0].Indeks; n2 = MyIssue[i].Indeks; n3 = MyIssue[i].MaxRecord;

                slist = slist + '<tr>';
                slist = slist + '<td class="tdstart" align="left"' + sign + ' ;width:1%">' + ind + '</td>';
                slist = slist + '<td class="tdstart" align="left"' + sign + ' ;width:1%">' + MyIssue[i].IDMeet.substring(0, 11) + '<br>' + MyIssue[i].IDMeet.substring(11, 24) + '</td>';
                slist = slist + '<td class="tdstart" align="left"' + sign + ' ;width:14%">' + MyIssue[i].jenis + '</td>';
                slist = slist + '<td class="tdstart" align="left"' + sign + ' ; width:22%">' + MyIssue[i].NmIssue + '</td>';
                slist = slist + '<td class="tdstart" align="left"' + sign + ' ;width:22%">' + MyIssue[i].Output + '</td>';
                slist = slist + '<td class="tdstart" align="center"' + sign + ' ;width:5%">' + MyIssue[i].due + '</td>';
                slist = slist + '<td class="tdstart" align="left"' + sign + ' ;width:17%">' + getSplit(pic) + '</td>';
                slist = slist + '<td class="tdstart" align="center"' + sign + ' ;width:5%">' + sts + '</td>';

                slist = slist + '<td class="tdstart" align="left"' + sign + ' ;width:17%">' + getSplit(nin) + '</td>';
                slist = slist + '<td class="tdstart" align="center"><input id="btntindakan" type="button" value="detail" onclick="retrieveActionHead(\'' + sid + '\',\'' + sis + '\')" style="width: 60px; font-family: Calibri;" ></td>';
                slist = slist + '</tr>';
            }

            halaman = n1 + ' - ' + n2 + ' from ' + n3;

            slist = slist + '<tr><td colspan="9" align="left"><br/>' + halaman + '</td></tr>';
            slist = slist + '<tr><td colspan="9"><br/></td></tr>';
            slist = slist + '<tr><td colspan="9" align="left"><a href="#" onclick="prevPage(\'common\')"><< Prev</a>';
            slist = slist + '                                         ';
            slist = slist + '<a href="#" onclick="nextPage(\'common\')">Next >></a></td></tr>';
            slist = slist + tail;
            slist = slist + '</table></center>';

            d.getElementById('divaction').innerHTML = "";
            d.getElementById('divlampiran').innerHTML = "";
            var tempissue;
            tempissue = slist;

            if (res.d.MyTaskList.length > 0) {
                nlast = 0;
                tmpissue = slist;
                d.getElementById('divmain').innerHTML = tmpissue;
            }
            else {
                nlast = 1;
                gpage = gpage - 1;
                if (page > 0) {
                    retrieveIssueHead(page - 1);
                    currpage = page - 1;
                }
            }
            d.getElementById("currpage").value = currpage;
        },
        error: function(err) {
            alert(err.responseText);
        }
    });    
}

function Back() {
    var cp = document.getElementById("currpage").value;
    if (originopsi == 0) {
        retrieveIssueHead(cp);
    }
    else {
        retrieveIssueOpsi(cp);
        doc.getElementById("menutop").style.display = "none";
        showOption();
        doc.getElementById("divbutton").style.display = "block";
        doc.getElementById("btnhide").style.display = "block";
    }
}

function cari() {
    gpageop = 0;   
    
    if (doc.getElementById('chktgl').checked == false) {
        if (doc.getElementById('cmbjob').value == 'ALL') {
            if (doc.getElementById('cmbsts').value == 'ALL') {
                if (doc.getElementById('cmbover').value == 'ALL') {
                    if (doc.getElementById('cmbdepar').value == 'ALL') {
                        if (doc.getElementById('cmbjenis').value == 'ALL') {
                            retrieveIssueHead(1);
                            originopsi = 0;
                        }
                        else { retrieveIssueOpsi(0); originopsi = 1; }
                    }
                    else { retrieveIssueOpsi(0); originopsi = 1; }
                }
                else { retrieveIssueOpsi(0); originopsi = 1; }
            }
            else { retrieveIssueOpsi(0); originopsi = 1; }
        }
        else { retrieveIssueOpsi(0); originopsi = 1; }
    }
    else { retrieveIssueOpsi(0); originopsi = 1; }     
}

function retrieveIssueOpsi(nflag) {
    var d = document;
    d.getElementById('divmain').innerHTML = "";
    d.getElementById('divprogress').style.display = 'block'; 
    
    doc.getElementById("btnshow").style.display = "none";
    doc.getElementById("btnhide").style.display = "block";

    var argtg1 = doc.getElementById('TxtTgl1').value;
    var argtg2 = doc.getElementById('TxtTgl2').value;
    var argcbg = doc.getElementById('cmbjob').value;
    var argsts = doc.getElementById('cmbsts').value;
    var jnstgl = doc.getElementById('seltanggal').value;
    var argfilter = doc.getElementById('cmbover').value;
    var argfungsi = doc.getElementById('cmbdepar').value;
    var argjenis = doc.getElementById('cmbjenis').value;

    if (doc.getElementById('chktgl').checked == true) {
        if (argtg1 == '') {
            alert('Please Entry Date Start');return false;
        }
        if (argtg2 == '') {
            alert('Please Entry Date End'); return false;
        }
    }

    if (doc.getElementById('chktgl').checked == false) {
        argtg1 = ''; argtg2 = '';
    }
    if (doc.getElementById('cmbjob').value == 'ALL') {
        argcbg = '';
    }
    if (doc.getElementById('cmbsts').value == 'ALL') {
        argsts = '';
    }
    if (doc.getElementById('cmbover').value == 'ALL') {
        argfilter = '';
    }
    if (doc.getElementById('cmbdepar').value == 'ALL') {
         argfungsi = '';
    }
    if (doc.getElementById('cmbjenis').value == 'ALL') {
        argjenis = '0';
    }

    var obCari = new objOpsi(argtg1, argtg2, argcbg, argsts, argfilter, argfungsi, argjenis);    
    var nik = d.getElementById('TxtNIK').value;
    nikpic = nik;

    if (gpageop == 0) { gpageop = 1; }

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "WS/MyList.asmx/GetMasterListOpsi",
        data: "{'_page':" + gpageop + ",'_opsi':" + JSON.stringify(obCari) + ",'_iduser':'" + nikpic + "','_jnstgl':'" + jnstgl + "'}",
        dataType: "json",
        success: function(res) {
            d.getElementById('divprogress').style.display = 'none'; 
            MyIssue = new Array();
            MyIssue = res.d.MyTaskList;
            gIssue = MyIssue;

            var slist = '<center>';
            slist = slist + '<table id="tabgrid" width="100%" cellpadding="0" cellspacing="2">';
            slist = slist + '<caption class="captiongrid" />List Issue according to choosen criteria</caption>';
            slist = slist + '<tr>';
            slist = slist + '<th class="thstart">No</th><th class="thstart">No Notulen</th><th class="thstart">Type</th><th class="thstart">Issue</th>';
            slist = slist + '<th class="thstart">Last Progress</th><th class="thstart">Due To</th><th class="thstart">PIC [Status Action]</th>';
            slist = slist + '<th class="thstart">Status Issue</th><th class="thstart">Info To</th><th class="thstart">Action</th>';
            slist = slist + '</tr>';

            var j = 0;
            for (var i = 0; i < res.d.MyTaskList.length; i++) {
                var ind = MyIssue[i].Indeks;
                var sid = MyIssue[i].IDMeet;
                var sis = MyIssue[i].NoIssue;
                var sia = MyIssue[i].NoRef;
                var pic = MyIssue[i].Nmpic;
                var nin = MyIssue[i].NmInfo;
                var sts = (MyIssue[i].status == '0') ? 'Open' : 'Close';
                var sign;
                
                var due = MyIssue[i].due; var cls = MyIssue[i].closed;
                if (sts == 'Close') {
                    if (CompareDates(due) < CompareDates(cls)){
                        sign = ' style="color: #F00" ';
                    }
                    else{
                        sign = ' style="color: #000" ';
                    }
                }
                else {
                    sign = getColor(MyIssue[i].Flag);
                }
                
                n1 = MyIssue[0].Indeks; n2 = MyIssue[i].Indeks; n3 = MyIssue[i].MaxRecord;

                slist = slist + '<tr>';
                slist = slist + '<td class="tdstart" align="left"' + sign + ' ;width:1%">' + ind + '</td>';
                slist = slist + '<td class="tdstart" align="left"' + sign + ' ;width:1%">' + MyIssue[i].IDMeet.substring(0, 11) + '<br>' + MyIssue[i].IDMeet.substring(11, 24) + '</td>';
                slist = slist + '<td class="tdstart" align="left"' + sign + ' ;width:14%">' + MyIssue[i].jenis + '</td>';
                slist = slist + '<td class="tdstart" align="left"' + sign + ' ; width:22%">' + MyIssue[i].NmIssue + '</td>';
                slist = slist + '<td class="tdstart" align="left"' + sign + ' ;width:22%">' + MyIssue[i].Output + '</td>';
                slist = slist + '<td class="tdstart" align="center"' + sign + ' ;width:5%">' + MyIssue[i].due + '</td>';
                slist = slist + '<td class="tdstart" align="left"' + sign + ' ;width:17%">' + getSplit(pic) + '</td>';
                slist = slist + '<td class="tdstart" align="center"' + sign + ' ;width:5%">' + sts + '</td>';
                slist = slist + '<td class="tdstart" align="left"' + sign + ' ;width:17%">' + getSplit(nin) + '</td>';
                slist = slist + '<td class="tdstart" align="center"><input id="btntindakan" type="button" value="detail" onclick="retrieveActionHead(\'' + sid + '\',\'' + sis + '\')" style="width: 60px; font-family: Calibri;" ></td>';
                slist = slist + '</tr>';
            }
            halaman = n1 + ' - ' + n2 + ' dari ' + n3;

            slist = slist + '<tr><td colspan="8" align="left"><br/>' + halaman + '</td></tr>';
            slist = slist + '<tr><td colspan="8"><br/></td></tr>';
            slist = slist + '<tr><td colspan="8" align="left"><a href="#" onclick="prevPage(\'option\')"><< Prev</a>';
            slist = slist + '                                         ';
            slist = slist + '<a href="#" onclick="nextPage(\'option\')">Next >></a></td></tr>';
            slist = slist + tail;
            slist = slist + '</table></center>';

            d.getElementById('divaction').innerHTML = "";
            d.getElementById('divlampiran').innerHTML = "";

            if (res.d.MyTaskList.length > 0) {
                nlast = 0;
                tmpissue = slist;
                d.getElementById('divmain').innerHTML = tmpissue;
            }
            else {
                //nlast = 1;
                gpageop = gpageop - 1;
                tmpissue = slist;
                if (nflag == 0) {
                    d.getElementById('divmain').innerHTML = getMessage('DATA NOT FOUND');
                }
                else {
                    if (gpageop > 0) {
                        retrieveIssueOpsi(gpageop); currpage = gpageop;
                    }
                }
            }
            d.getElementById("currpage").value = currpage;
        },
        error: function(err) {
            alert(err.responseText);
        }
    });  
}

function retrieveActionHead(idr, idi) {
    var d = document;
    d.getElementById('divmain').innerHTML = "";
    d.getElementById('divprogress').style.display = 'block'; 
    
    hideOption();
    d.getElementById("divbutton").style.display = "none";
    d.getElementById("menutop").style.display = "block";

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "WS/MyList.asmx/GetMasterTindakan",
        data: "{'_IdRap':'" + idr + "','_IdIssue':'" + idi + "'}",
        dataType: "json",
        success: function(res) {
            d.getElementById('divprogress').style.display = 'none'; 
            MyIssue = new Array();
            MyIssue = res.d.MyTaskList;
            gIssue = MyIssue;

            var slist = '<center>';
            slist = slist + '<table id="tabaction" width="100%" cellpadding="0" cellspacing="2">';
            slist = slist + '<caption style="text-align:left"><span style="font-size:1.1em;background-color:#00CC00"><b>Isu > </span><span style="font-size:1.1em;background-color:#64C8FF"> ' + MyIssue[0].NmIssue + ' </span></b></caption>';
            slist = slist + '<col width="30px"><col width="*px"><col width="250px"><col width="90px"><col width="100px"><col width="150px"><col width="90px"><col width="90px">';
            slist = slist + '<tr><th class="thstart">No</th><th class="thstart">Action</th><th class="thstart">Last Progress</th><th class="thstart">Date Finish</th>';
            slist = slist + '<th class="thstart">Due To</th><th class="thstart">Target Result</th><th class="thstart">PIC</th><th class="thstart">Status Action</th></tr>';

            var j = 0;
            for (var i = 0; i < res.d.MyTaskList.length; i++) {
                var sts = (MyIssue[i].status == '0') ? 'Open' : 'Close';

                j = j + 1;
                slist = slist + '<tr>';
                slist = slist + '<td class="tdstart" align="left">' + (j) + '</td>';
                slist = slist + '<td class="tdstart" align="left"><a href="#" onclick="showLampiran(\'' + MyIssue[i].IDMeet + '\',\'' + MyIssue[i].NoIssue + '\',\'' + MyIssue[i].NoRef + '\')">' + MyIssue[i].Action + '</a></td>';
                slist = slist + '<td class="tdstart" align="left">' + MyIssue[i].Output + '</td>';
                slist = slist + '<td class="tdstart" align="center">' + MyIssue[i].closed + '</td>';
                slist = slist + '<td class="tdstart" align="center">' + MyIssue[i].due + '</td>';
                slist = slist + '<td class="tdstart" align="left">' + MyIssue[i].Deliverable + '</td>';
                slist = slist + '<td class="tdstart" align="left">' + MyIssue[i].Nmpic + '</td>';
                if (sts == 'Open') {
                    slist = slist + '<td class="tdstart" align="center">' + sts + '</td>';
                }
                else {
                    slist = slist + '<td class="tdstart" align="center" style="color: #0000FF">' + sts + '</td>';
                }
                slist = slist + '</tr>';
            }
            slist = slist + '</table></center>';

            d.getElementById('divaction').innerHTML = slist;
            d.getElementById('divmain').innerHTML = "";
            d.getElementById('divlampiran').innerHTML = "";
            tempaction = slist;
        },
        error: function(err) {
            alert(err.responseText);
        }
    });
}

function getSplit(argumen) {    
    var myArr = new Array();
    myArr = argumen.split(",");

    var chasil = "";
    for (var i = 0; i < myArr.length-1; i++) {
        chasil = chasil + myArr[i] + "<br/>";
    }
    return chasil;
}

function nextPage(jenis) {
    if (nlast != 1) {    
        if (jenis == "common") {
            gpage = gpage + 1;
            retrieveIssueHead(gpage);
            currpage = gpage;
        }
        if (jenis == "option") {
            gpageop = gpageop + 1;
            retrieveIssueOpsi(gpageop);
            currpage = gpageop;
        }
    }
}

function prevPage(jenis) {  
    // STEP 1
    if (gpage == 1) {
        gpage = 2;
    }
    if ((gpageop == 0) || (gpageop == 1)) {
        gpageop = 2;
    }

    // STEP 2
    if (jenis == "common") {
        gpage = gpage - 1;
        retrieveIssueHead(gpage);
        currpage = gpage;
    }
    if (jenis == "option") {
        gpageop = gpageop - 1;
        retrieveIssueOpsi(gpageop);
        currpage = gpageop;
    }
}

function showLampiran(idr, idi, ida) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "WS/Utility.asmx/ShowAllLampiran",
        data: "{'_IdRap':'" + idr + "','_IdIssue':'" + idi + "','_NoAction':'" + ida + "'}",
        dataType: "json",
        success: function(res) {
            var afile = new Array();
            afile = res.d;

            var shtml = "<br/><br/><span><u><b>List File Lampiran</b></u></span><br/>";
            if (afile.length > 0) {
                for (var i = 0; i < afile.length; i++) {
                    shtml = shtml + (i + 1) + '.' + afile[i].NmFile + ' __ ';
                    shtml = shtml + '<a href="#" onclick="download(' + afile[i].Nomor + ',\'' + afile[i].IdRap + '\',\'' + afile[i].IdIssue + '\',\'' + afile[i].NoAction + '\')">[Download]</a><br/>';
                }
            }
            else {
                shtml = shtml + "Not Found Lampiran";
            }
            shtml = shtml + '<br/>' + '<a href="#" onclick="hideLampiran()">[x Close]</a>';

            document.getElementById('divlampiran').innerHTML = shtml;
        },
        error: function(err) {
            alert(err.responseText);
        }
    });
}

function download(nomor,idrap,idis,idac) {
    doc.getElementById('Nomor').value = nomor;
    doc.getElementById('Id1').value = idrap;
    doc.getElementById('Id2').value = idis;
    doc.getElementById('Id3').value = idac;    
    window.open("DownloadBerkas.aspx", "Download", "scrollbars=no,resizable=no,width=400,height=280");
}

function hideLampiran() {
    doc.getElementById('divlampiran').innerHTML = "";
}

function initPage() {
    doc.getElementById("btnhide").style.display = "none";
    doc.getElementById("divopsi1").style.display = "none";
    doc.getElementById("divopsi2").style.display = "none";
}

function showOption() {
    doc.getElementById("btnhide").style.display = "block";
    doc.getElementById("btnshow").style.display = "none";
    doc.getElementById("btncetak1").style.display = "none";
    doc.getElementById("btncetak2").style.display = "block";
    doc.getElementById("divopsi1").style.display = "block";
    doc.getElementById("divopsi2").style.display = "block";
    doc.getElementById("btncari").style.display = "block";
    doc.getElementById("criteria").style.border = "dashed orange 1px";
    
    if ((doc.getElementById('cmbjob').value != 'JKT') && (doc.getElementById('cmbjob').value != 'ALL')) {
        cari();
    }
}

function hideOption() {
    doc.getElementById("btnshow").style.display = "block";
    doc.getElementById("btncetak1").style.display = "block";
    doc.getElementById("btncetak2").style.display = "none";
    doc.getElementById("btnhide").style.display = "none";
    doc.getElementById("divopsi1").style.display = "none";
    doc.getElementById("divopsi2").style.display = "none";
    doc.getElementById("btncari").style.display = "none";
    doc.getElementById("criteria").style.border = "solid 0px";
}

function objOpsi(Tgl1, Tgl2, KdCab, Status, Over, Fungsi, Jenis) {
    this.Tgl1 = Tgl1; this.Tgl2 = Tgl2; this.KdCab = KdCab; this.Status = Status; this.Over = Over;this.Fungsi = Fungsi;this.Jenis = Jenis;
}

function getMessage(pesan) {
    return '<center><p><b style="font-size: x-large; color: #0066FF">' + pesan + '</b></p></center>';
}

function getColor(flag) {
    /* jangan hilangkan spasi di nilai warna */
    var warna = "";
    switch (flag) {
        case 'R':
            warna = ' style="font-weight:bold;color: #F00 ';
            break;
        case 'Y':
            warna = ' style="color: #F00 ';
            break;
        default:
            warna = ' style="color: #000" ';
    }
    return warna
}

function cetakIsu(arg) {
    var a1 = doc.getElementById('TxtTgl1').value;
    var a2 = doc.getElementById('TxtTgl2').value;
    var bz = doc.getElementById('cmbjob').value;
    var cz = doc.getElementById('cmbsts').value;
    var dz = doc.getElementById('cmbover').value;
    var ez = doc.getElementById('cmbdepar').value;
    var fz = doc.getElementById('cmbjenis').value;
    var zz = doc.getElementById('seltanggal').value;

    if (doc.getElementById('chktgl').checked == true) {
        if (a1 == '') {
            alert('Please Entry Date Start');return false;
        }
        if (a2 == '') {
            alert('Please Entry Date End'); return false;
        }
    }
    if (doc.getElementById('chktgl').checked == false) {
        a1 = ''; a2 = '';
    }
    if (doc.getElementById('cmbjob').value == 'ALL') {
        bz = '';
    }
    if (doc.getElementById('cmbsts').value == 'ALL') {
        cz = '';
    }
    if (doc.getElementById('cmbover').value == 'ALL') {
        dz = '';
    }
    if (doc.getElementById('cmbdepar').value == 'ALL') {
        ez = '';
    }
    if (doc.getElementById('cmbjenis').value == 'ALL') {
        fz = '0';
    }
    var nik = doc.getElementById('TxtNIK').value;
    if (arg == 1) {
        window.open("PrintIssue.aspx?jnslap=1&nik=" + nik, "Cetak", "scrollbars=no,resizable=no,width=900,height=550");
    }
    else {
        window.open("PrintIssue.aspx?jnslap=2&nik=" + nik + "&jnstgl=" + zz + "&tg1=" + a1 + "&tg2=" + a2 + "&job=" + bz + "&sts=" + cz + "&over=" + dz + "&dpr=" + ez + "&jnsmtg=" + fz, "Cetak", "scrollbars=no,resizable=no,width=900,height=550");
    } 
}