/*
Settingsfile for JQuery plugin for rendering gauges, v. 1.0

* Initial create by Henkka, Jan 2010

*/

$(function() {
    // Settings start
    // Few notes:
    // - dots means how many values between the numbers in the gauge
    // - Offsets is for finetuning the placement of the values


    // Data file and messages
    var dataurl = 'client-raw.txt';
    var updates = 20;
    var timeout = 20; // In sec
    var errormsg = "Error with getting new data";
    var pausemsg = "Updates paused, refresh page to continue.\nP&auml;ivitys tauonnut, p&auml;ivit&auml;� sivu jatkaaksesi.";


    // Pressure
    var showbaro = true;
    var barogaugeMax = 1050;
    var barogaugeMin = 0;
    var baronumbersoffset = 11;
    var barouom = 'bcm';  // hpa, in, mb supported
    var barodots = 100;
    var barodotsoffset = 11;
    var barostartcolor = '255,0,0';
    var barostopcolor = '0,255,0';
    var baromiddlecolor = '255,255,0';

    // Few global settings also ;)
    var options = {
        gradient: {
            colors: ["#fafafa", "#ddd", "#ccc", "#bbb"]
        },
        notusebgimage: false
    };

    // Block of used divs, comment out if not needed <-- IMPORTANT
    if (showbaro) { var baro = $("#baro"); }
    var baro2 = $("#baro2");
    var baro3 = $("#baro3");
    var baro4 = $("#baro4");
    var baro5 = $("#baro5");
    var baro6 = $("#baro6");
    var baro7 = $("#baro7");
    var baro8 = $("#baro8");
    var baro9 = $("#baro9");
    var baro10 = $("#baro10");
    
    var clock = $("#clock");

    // END OF BASIC SETTINGS
    //***************************************
    // To change a value in a gauge, you need to go below to approx line 180 and
    // just change the clientraw-value for that gauge
    // To create a new gauge, just copy settings for one gauge,rename and tweak it. ;)
    //***************************************

    // Dummy data as there are instanses what ask for it ;)
    // DON'T TOUCH!!!
    var data = '';
    var alreadyFetched = {};
    var cr = '';
    var iteration = 0;
    var crcache = {};
    update();

    function update() {

        //Reset data
        alreadyFetched = {};

        function onDataReceived(series) {
            iteration++;

            if (iteration == updates) {
                alert(pausemsg);
                return;
            }

            // split clientraw
            var cr = series.split(' ');

            // We need some some values from clientraw
            var crbaro = 100;

            // Example how to do a "normal" ajax-thing
            var cu = 'Upd by Jquery: ' + cr[29] + ':' + cr[30] + ':' + cr[31];
            $("#clock").html(cu);


            // Pressure
            if ((showbaro) && (crbaro != options.barocache)) {
                options.gaugeMax = barogaugeMax;
                options.gaugeMin = barogaugeMin;
                options.numbersoffset = baronumbersoffset;
                options.uom = barouom;
                options.dots = barodots;
                options.dotsoffset = barodotsoffset;
                options.crvalue = 0;
                options.barocache = crbaro;
                options.startcolor = barostartcolor;
                options.stopcolor = barostopcolor;
                options.middlecolor = baromiddlecolor;
                $.plot(baro, data, options);
            }
            options.gaugeMax = 2000;
            options.gaugeMin = 0;
            options.numbersoffset = baronumbersoffset;
            options.uom = barouom;
            options.dots = 200;
            options.dotsoffset = barodotsoffset;
            options.crvalue = 0;
            options.barocache = crbaro;
            options.startcolor = barostartcolor;
            options.stopcolor = barostopcolor;
            options.middlecolor = baromiddlecolor;
            $.plot(baro2, data, options);
            $.plot(baro3, data, options);
            $.plot(baro4, data, options);
            $.plot(baro5, data, options);
            $.plot(baro6, data, options);
            $.plot(baro7, data, options);
            $.plot(baro8, data, options);
            $.plot(baro9, data, options);
            $.plot(baro10, data, options);
            cr = '';
            cr = false;
            //alert(cr);

        }

        $.ajax({
            url: dataurl,
            method: 'GET',
            dataType: 'text/plain',
            cache: false,
            error: function(msg) {
                alert(errormsg);
            },
            success: onDataReceived
        });

        if (iteration < updates) {
            //setTimeout(update, timeout*1000);
        }


    };

});