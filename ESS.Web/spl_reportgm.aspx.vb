﻿Imports Microsoft.Reporting.WebForms

Partial Public Class spl_reportgm
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("niksite") = Nothing Or Session("niksite") = "" Then Response.Redirect("login.aspx")
        If Not IsPostBack Then
            txtdate1.Text = "2014-01-01"
            txtdate2.Text = "2014-01-01"
            hkdsite.Value = Session("kdsite").ToString
        End If
    End Sub

    Private Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnok.Click
        report_retrieve()
    End Sub

    Public Sub report_retrieve()
       
        Dim rds As New ReportDataSource("SPLDataSet_H_H1101", ObjectDataSource1)

        ReportViewer1.LocalReport.DataSources.Clear()

        Dim p1 As New ReportParameter("date1", txtdate1.Text)
        Dim p2 As New ReportParameter("date2", txtdate2.Text)

        Me.ReportViewer1.LocalReport.SetParameters(New ReportParameter() {p1, p2})

        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.Refresh()

    End Sub
End Class