﻿Imports System.Data
Imports System.Data.SqlClient
Partial Public Class leavelist
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("niksite") = Nothing Or Session("niksite") = "" Then Response.Redirect("login.aspx")

        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        'Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("test_con").ConnectionString)
        If Not IsPostBack Then
            Try
                sqlConn.Open()
                Dim dtb As DataTable = New DataTable()

                ddlyear.SelectedValue = Today.Year.ToString

                Dim strcon As String = "SELECT L_H001.transid, L_H001.kdsite,L_H001.nik,(select niksite from H_A101 where nik = L_H001.nik) as niksite,(select nama from H_A101 where nik = L_H001.nik) as name,convert(char(10),L_H001.trans_date,101) as trans_date,L_H001.kddepar,L_H001.kdjabatan,convert(char(10),L_H001.datefrom,101) as datefrom," & _
                                       "convert(char(10),L_H001.dateto,101) as dateto,L_H001.totleave,L_H001.remainleave,L_H001.eligiLeave,L_H001.cutabsent, L_H001.typeLeave, (select typedesc from L_A001 where typeLeave = L_H001.typeLeave) as typedesc ,L_H001.paidltype, (select paidltypeDesc from L_A002 where paidltype = L_H001.paidltype) as paidltypeDesc," & _
                                       "L_H001.remarks, L_H001.pic_nik, L_H001.pic_jabat,L_H001.verify_nik, L_H001.verify_jabat, L_H001.approve_nik, L_H001.approve_jabat," & _
                                       "L_H001.fstatus, L_H001.CreatedBy, L_H001.CreatedIn, L_H001.CreatedTime, L_H001.ModifiedBy,L_H001.ModifiedIn," & _
                                       "L_H001.ModifiedTime, L_H001.StEdit, L_H001.DeleteBy,L_H001.DeleteTime,L_H001.yeara,L_H001.yearb," & _
                                       "L_H001.atot,L_H001.btot,L_H001.holiday, " & _
                                       "ISNULL((SELECT TOP 1 Nama FROM H_A101 WHERE NIKSITE = L_H001.NxtActionr), '') AS NxtAction, " & _
                                       "ISNULL(CASE  " & _
                                        "WHEN L_H001.NxtActionrRole = 'DEL' THEN 'Delegator' " & _
                                        "WHEN L_H001.NxtActionrRole = 'MGR' THEN 'Manager' " & _
                                        "WHEN L_H001.NxtActionrRole = 'HRD' THEN 'In HRD' " & _
                                       "END, '') AS picUserRole " & _
                                       "FROM L_H001 WHERE L_H001.StEdit <> '2' AND L_H001.nik like '" + Session("niksite") + "' and MONTH(trans_date) between '" + Today.Month.ToString + "' and '" + Today.Month.ToString + "' and year(trans_date) = '" + ddlyear.SelectedValue + "' " & _
                                       "union " & _
                                       "SELECT L_H001.transid, L_H001.kdsite,L_H001.nik," & _
                                       "(select niksite from H_A101 where nik = L_H001.nik) as niksite, " & _
                                       "(select nama from H_A101 where nik = L_H001.nik) as name,convert(char(10),L_H001.trans_date,101) as trans_date, " & _
                                       "L_H001.kddepar,L_H001.kdjabatan,convert(char(10),L_H001.datefrom,101) as datefrom," & _
                                       "convert(char(10),L_H001.dateto,101) as dateto, " & _
                                       "L_H001.totleave,L_H001.remainleave,L_H001.eligiLeave,L_H001.cutabsent, L_H001.typeLeave, " & _
                                       "(select typedesc from L_A001 where typeLeave = L_H001.typeLeave) as typedesc , " & _
                                       "L_H001.paidltype, (select paidltypeDesc from L_A002 where paidltype = L_H001.paidltype) as paidltypeDesc, " & _
                                       "L_H001.remarks, L_H001.pic_nik, L_H001.pic_jabat,L_H001.verify_nik, L_H001.verify_jabat, " & _
                                       "L_H001.approve_nik, L_H001.approve_jabat,L_H001.fstatus, L_H001.CreatedBy, L_H001.CreatedIn, " & _
                                       "L_H001.CreatedTime, L_H001.ModifiedBy,L_H001.ModifiedIn,L_H001.ModifiedTime, L_H001.StEdit, " & _
                                       "L_H001.DeleteBy, L_H001.DeleteTime, L_H001.yeara, L_H001.yearb, L_H001.atot, L_H001.btot, L_H001.holiday, " & _
                                       "ISNULL((SELECT TOP 1 Nama FROM H_A101 WHERE NIKSITE = L_H001.NxtActionr), '') AS NxtAction, " & _
                                       "ISNULL(CASE  " & _
                                        "WHEN L_H001.NxtActionrRole = 'DEL' THEN 'Delegator' " & _
                                        "WHEN L_H001.NxtActionrRole = 'MGR' THEN 'Manager' " & _
                                        "WHEN L_H001.NxtActionrRole = 'HRD' THEN 'In HRD' " & _
                                       "END, '') AS picUserRole " & _
                                       "FROM L_H001 " & _
                                       "WHERE L_H001.StEdit <> '2' and nik in (select nikb from V_A004 where nika = '" + Session("niksite") + "' and module = 'LEAVE') and MONTH(trans_date) between '" + Today.Month.ToString + "' and '" + Today.Month.ToString + "' and year(trans_date) = '2013' "

                'AND L_H001.trans_date between :date1 and :date2

                Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
                sda.Fill(dtb)

                GridView1.DataSource = dtb
                GridView1.DataBind()

                'Dim dt1 As Date = Today.Month.ToString



                ddlbulan1.SelectedValue = DateTime.Today.ToString("MM")
                ddlbulan2.SelectedValue = DateTime.Today.ToString("MM")

                Dim th As String = Today.Year.ToString
                Dim licount As Integer
                Dim licount2 As String
                licount2 = 0
                For licount = 2011 To th
                    licount2 += 1
                    ddlyear.Items.Insert(licount2 - 1, New ListItem(licount, licount))
                Next
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()
            'ddlyear.SelectedValue = Today.Year.ToString

            Dim strcon As String = "SELECT L_H001.transid, L_H001.kdsite,L_H001.nik,(select niksite from H_A101 where nik = L_H001.nik) as niksite,(select nama from H_A101 where nik = L_H001.nik) as name,convert(char(10),L_H001.trans_date,101) as trans_date,L_H001.kddepar,L_H001.kdjabatan,convert(char(10),L_H001.datefrom,101) as datefrom," & _
                                   "convert(char(10),L_H001.dateto,101) as dateto,L_H001.totleave,L_H001.remainleave,L_H001.eligiLeave,L_H001.cutabsent, L_H001.typeLeave, (select typedesc from L_A001 where typeLeave = L_H001.typeLeave) as typedesc ,L_H001.paidltype, (select paidltypeDesc from L_A002 where paidltype = L_H001.paidltype) as paidltypeDesc," & _
                                   "L_H001.remarks, L_H001.pic_nik, L_H001.pic_jabat,L_H001.verify_nik, L_H001.verify_jabat, L_H001.approve_nik, L_H001.approve_jabat," & _
                                   "L_H001.fstatus, L_H001.CreatedBy, L_H001.CreatedIn, L_H001.CreatedTime, L_H001.ModifiedBy,L_H001.ModifiedIn," & _
                                   "L_H001.ModifiedTime, L_H001.StEdit, L_H001.DeleteBy,L_H001.DeleteTime,L_H001.yeara,L_H001.yearb," & _
                                   "L_H001.atot,L_H001.btot,L_H001.holiday," & _
                                   "ISNULL((SELECT TOP 1 Nama FROM H_A101 WHERE NIKSITE = L_H001.NxtActionr), '') AS NxtAction, " & _
                                       "ISNULL(CASE  " & _
                                        "WHEN L_H001.NxtActionrRole = 'DEL' THEN 'Delegator' " & _
                                        "WHEN L_H001.NxtActionrRole = 'MGR' THEN 'Manager' " & _
                                        "WHEN L_H001.NxtActionrRole = 'HRD' THEN 'In HRD' " & _
                                       "END, '') AS picUserRole " & _
                                   "FROM L_H001 WHERE L_H001.StEdit <> '2' AND L_H001.nik like '" + Session("niksite") + "' and MONTH(trans_date) between '" + ddlbulan1.SelectedValue.ToString() + "' and '" + ddlbulan2.SelectedValue.ToString() + "' and year(trans_date) = '" + ddlyear.SelectedValue + "' " & _
                                   "union " & _
                                   "SELECT L_H001.transid, L_H001.kdsite,L_H001.nik," & _
                                   "(select niksite from H_A101 where nik = L_H001.nik) as niksite, " & _
                                   "(select nama from H_A101 where nik = L_H001.nik) as name,convert(char(10),L_H001.trans_date,101) as trans_date, " & _
                                   "L_H001.kddepar,L_H001.kdjabatan,convert(char(10),L_H001.datefrom,101) as datefrom," & _
                                   "convert(char(10),L_H001.dateto,101) as dateto, " & _
                                   "L_H001.totleave,L_H001.remainleave,L_H001.eligiLeave,L_H001.cutabsent, L_H001.typeLeave, " & _
                                   "(select typedesc from L_A001 where typeLeave = L_H001.typeLeave) as typedesc , " & _
                                   "L_H001.paidltype, (select paidltypeDesc from L_A002 where paidltype = L_H001.paidltype) as paidltypeDesc, " & _
                                   "L_H001.remarks, L_H001.pic_nik, L_H001.pic_jabat,L_H001.verify_nik, L_H001.verify_jabat, " & _
                                   "L_H001.approve_nik, L_H001.approve_jabat,L_H001.fstatus, L_H001.CreatedBy, L_H001.CreatedIn, " & _
                                   "L_H001.CreatedTime, L_H001.ModifiedBy,L_H001.ModifiedIn,L_H001.ModifiedTime, L_H001.StEdit, " & _
                                   "L_H001.DeleteBy, L_H001.DeleteTime, L_H001.yeara, L_H001.yearb, L_H001.atot, L_H001.btot, L_H001.holiday, " & _
                                   "ISNULL((SELECT TOP 1 Nama FROM H_A101 WHERE NIKSITE = L_H001.NxtActionr), '') AS NxtAction, " & _
                                       "ISNULL(CASE  " & _
                                        "WHEN L_H001.NxtActionrRole = 'DEL' THEN 'Delegator' " & _
                                        "WHEN L_H001.NxtActionrRole = 'MGR' THEN 'Manager' " & _
                                        "WHEN L_H001.NxtActionrRole = 'HRD' THEN 'In HRD' " & _
                                       "END, '') AS picUserRole " & _
                                   "FROM L_H001 " & _
                                   "WHERE L_H001.StEdit <> '2' and nik in (select nikb from V_A004 where nika = '" + Session("niksite") + "' and module = 'LEAVE') and MONTH(trans_date) between '" + ddlbulan1.SelectedValue.ToString() + "' and '" + ddlbulan2.SelectedValue.ToString() + "' and year(trans_date) = '" + ddlyear.SelectedValue.ToString() + "' "

            'AND L_H001.trans_date between :date1 and :date2

            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            GridView1.DataSource = dtb
            GridView1.PageIndex = e.NewPageIndex
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        If GridView1.SelectedRow Is Nothing Then Exit Sub

        Dim row As GridViewRow = GridView1.SelectedRow
        Session("vleave") = GridView1.DataKeys(row.RowIndex).Value.ToString
        'Response.Redirect("view_trip.aspx?ID=" & GridView1.DataKeys(row.RowIndex).Value.ToString)
        Response.Redirect("leave_edit.aspx")
    End Sub

    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As EventArgs) Handles Calendar1.SelectionChanged
        dtfrom.Text = (Calendar1.SelectedDate.ToString)
        Calendar1.Visible = False
        'ddlbulan.SelectedItem = Calendar1.SelectedDate.ToString
    End Sub

    Protected Sub btnsrcdt_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsrcdt.Click
        Calendar1.Visible = True
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()
            Dim strcon As String = "SELECT L_H001.transid, L_H001.kdsite,L_H001.nik, (select NIKSITE from H_A101 where nik = L_H001.nik) as niksite,(select nama from H_A101 where nik = L_H001.nik) as name,convert(char(10),L_H001.trans_date,101) as trans_date,L_H001.kddepar,L_H001.kdjabatan,convert(char(10),L_H001.datefrom,101) as datefrom," & _
                                   "convert(char(10),L_H001.dateto,101) as dateto,L_H001.totleave,L_H001.remainleave,L_H001.eligiLeave,L_H001.cutabsent, L_H001.typeLeave, (select typedesc from L_A001 where typeLeave = L_H001.typeLeave) as typedesc ,L_H001.paidltype, (select paidltypeDesc from L_A002 where paidltype = L_H001.paidltype) as paidltypeDesc," & _
                                   "L_H001.remarks, L_H001.pic_nik, L_H001.pic_jabat,L_H001.verify_nik, L_H001.verify_jabat, L_H001.approve_nik, L_H001.approve_jabat," & _
                                   "L_H001.fstatus, L_H001.CreatedBy, L_H001.CreatedIn, L_H001.CreatedTime, L_H001.ModifiedBy,L_H001.ModifiedIn," & _
                                   "L_H001.ModifiedTime, L_H001.StEdit, L_H001.DeleteBy,L_H001.DeleteTime,L_H001.yeara,L_H001.yearb," & _
                                   "L_H001.atot,L_H001.btot,L_H001.holiday, " & _
                                   "ISNULL((SELECT TOP 1 Nama FROM H_A101 WHERE NIKSITE = L_H001.NxtActionr), '') AS NxtAction, " & _
                                       "ISNULL(CASE  " & _
                                        "WHEN L_H001.NxtActionrRole = 'DEL' THEN 'Delegator' " & _
                                        "WHEN L_H001.NxtActionrRole = 'MGR' THEN 'Manager' " & _
                                        "WHEN L_H001.NxtActionrRole = 'HRD' THEN 'In HRD' " & _
                                       "END, '') AS picUserRole " & _
                                   "FROM L_H001 WHERE L_H001.StEdit <> '2' AND L_H001.nik like '" + Session("niksite") + "' and MONTH(trans_date) between '" + ddlbulan1.SelectedValue + "' and '" + ddlbulan2.SelectedValue + "' and year(trans_date) = '" + ddlyear.SelectedValue + "' " & _
                                   "union " & _
                                   "SELECT L_H001.transid, L_H001.kdsite,L_H001.nik," & _
                                   "(select niksite from H_A101 where nik = L_H001.nik) as niksite, " & _
                                   "(select nama from H_A101 where nik = L_H001.nik) as name,convert(char(10),L_H001.trans_date,101) as trans_date, " & _
                                   "L_H001.kddepar,L_H001.kdjabatan,convert(char(10),L_H001.datefrom,101) as datefrom," & _
                                   "convert(char(10),L_H001.dateto,101) as dateto, " & _
                                   "L_H001.totleave,L_H001.remainleave,L_H001.eligiLeave,L_H001.cutabsent, L_H001.typeLeave, " & _
                                   "(select typedesc from L_A001 where typeLeave = L_H001.typeLeave) as typedesc , " & _
                                   "L_H001.paidltype, (select paidltypeDesc from L_A002 where paidltype = L_H001.paidltype) as paidltypeDesc, " & _
                                   "L_H001.remarks, L_H001.pic_nik, L_H001.pic_jabat,L_H001.verify_nik, L_H001.verify_jabat, " & _
                                   "L_H001.approve_nik, L_H001.approve_jabat,L_H001.fstatus, L_H001.CreatedBy, L_H001.CreatedIn, " & _
                                   "L_H001.CreatedTime, L_H001.ModifiedBy,L_H001.ModifiedIn,L_H001.ModifiedTime, L_H001.StEdit, " & _
                                   "L_H001.DeleteBy, L_H001.DeleteTime, L_H001.yeara, L_H001.yearb, L_H001.atot, L_H001.btot, L_H001.holiday, " & _
                                   "ISNULL((SELECT TOP 1 Nama FROM H_A101 WHERE NIKSITE = L_H001.NxtActionr), '') AS NxtAction, " & _
                                       "ISNULL(CASE  " & _
                                        "WHEN L_H001.NxtActionrRole = 'DEL' THEN 'Delegator' " & _
                                        "WHEN L_H001.NxtActionrRole = 'MGR' THEN 'Manager' " & _
                                        "WHEN L_H001.NxtActionrRole = 'HRD' THEN 'In HRD' " & _
                                       "END, '') AS picUserRole " & _
                                   "FROM L_H001 " & _
                                   "WHERE L_H001.StEdit <> '2' and nik in (select nikb from V_A004 where nika = '" + Session("niksite") + "' and module = 'LEAVE') and MONTH(trans_date) between '" + ddlbulan1.SelectedValue + "' and '" + ddlbulan2.SelectedValue + "' and year(trans_date) = '" + ddlyear.SelectedValue + "'"

            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            GridView1.DataSource = dtb
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub
End Class