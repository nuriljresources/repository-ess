﻿//ByVal Nomor As String, ByVal IdRap As String, ByVal IdIssue As String, ByVal NoAction As String
var tmplamp = new Array();

function showLampiran(no,idr,idi,ida) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "WS/Utility.asmx/ShowLampiran",
        data: "{'_Nomor':'" + no + "','_IdRap':'" + idr + "','_IdIssue':'" + idi + "','_NoAction':'" + ida + "'}",
        dataType: "json",
        success: function(res) {
            var afile = new Array();
            afile = res.d;

            var shtml = "<span><u><b>Daftar File Lampiran</b></u></span><br/>";
            if (afile.length > 0) {
                for (var i = 0; i < afile.length; i++) {
                    shtml = shtml + ( i + 1) + '.' + afile[i].NmFile + ' __ ' + '<a href="#" onclick="hapusLampiran()"> [x Hapus]</a><br/>';
                }
            }
            else {
                shtml = shtml + "Tidak Terdapat Lampiran";
            }

            document.getElementById('divdaftar').innerHTML = shtml;
        },
        error: function(err) {
            alert(err.responseText);
        }
    });
}

function hapusLampiran() {
    alert('Maaf __ Anda tidak diperkenankan menghapus');
}