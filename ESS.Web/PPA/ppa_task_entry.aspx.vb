﻿Imports System.Data.SqlClient
Partial Public Class ppa_task_entry
    Inherits System.Web.UI.Page
    Public mreport
    Public msg As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("ppa_nobuk") = "201311/PPA/000002"

        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("localdev").ConnectionString)
        Dim sqlconn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim qrytask As String = "select nobuk, genid, t_title, t_task, t_target, t_actual, t_score, uom from tr_ppa_task where nobuk = '" + Session("ppa_nobuk") + "' order by genid"
        Dim qrychkadmin As String = "Select nik, nama, admintype from admin_ppa where nik = '" + Session("ppa_niksite") + "'"
        Dim dtbchkadmin As DataTable = New DataTable
        Dim dtbtask As DataTable = New DataTable
        Dim i As Integer

        'If Session("ppa_otorisasi") = "" Then
        '    Response.Redirect("login_ppa.aspx")
        'End If

        If Not IsPostBack Then
            Try
                If Session("ppa_niksite") <> "" Then
                    If Session("ppa_niksite").ToString() = Request.QueryString("nsie") Then

                        sqlConn.Open()
                        Dim dtb_employee As DataTable = New DataTable
                        Dim strsql2 As String = "SELECT  Nik, niksite, Nama,KdSite, kddepar, (select nmdepar from h_a130 where kddepar= H_a101.kddepar) as kddepar,(select nmdepar from h_a130 where kddepar= H_a101.kddepar) + ' [' + (select nmsec from H_A209 ,H_A150 where H_A209.kdsec = H_A150.kdsec and H_A150.kdjabat = H_A101.kdjabatan )  + ']' as NmDepar, (select nmjabat from H_A209 ,H_A150 where H_A209.kdsec = H_A150.kdsec and H_A150.kdjabat = H_A101.kdjabatan ) as nmjabat, (SELECT kdjabat from H_A209 ,H_A150 where H_A209.kdsec = H_A150.kdsec and H_A150.kdjabat = H_A101.kdjabatan) as kdjabat, (select kddepar from h_a130 where kddepar= H_a101.kddepar) as kddepart, (select top 1 nik from H_A101 a where KdJabatan = (select kdjabatsup from H_A150 where kdjabat = H_A101.KdJabatan) and StEdit <> '2' and Active = '1') as nikspv, (select top 1 nama from H_A101 a where KdJabatan = (select kdjabatsup from H_A150 where kdjabat = H_A101.KdJabatan) and StEdit <> '2' and Active = '1') as namaspv FROM H_A101 WHERE active = '1' AND stedit <> '2' AND nik = '" + Session("ppa_niksite") + "'"
                        Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strsql2, sqlconn2)
                        sda2.Fill(dtb_employee)

                        If dtb_employee.Rows.Count > 0 Then
                            txtnama.Text = dtb_employee.Rows(0)!nama.ToString
                            txtnik.Text = dtb_employee.Rows(0)!niksite.ToString
                            txtjabat.Text = dtb_employee.Rows(0)!nmjabat.ToString
                            txtdepar.Text = dtb_employee.Rows(0)!nmdepar.ToString
                            hnik.Value = dtb_employee.Rows(0)!nik.ToString
                            hjabat.Value = dtb_employee.Rows(0)!kdjabat.ToString
                            hdepar.Value = dtb_employee.Rows(0)!kddepar.ToString
                        End If

                        Dim dtbspv As DataTable = New DataTable
                        Dim qryspv As String = "select nik_spv, (select nama from jrn_test..H_A101 where nik = tr_ppa_h.nik_spv) as namaspv from tr_ppa_h where nobuk = '" + Session("ppa_nobuk") + "'"
                        Dim sdaspv As SqlDataAdapter = New SqlDataAdapter(qryspv, sqlConn)
                        sdaspv.Fill(dtbspv)

                        If dtbspv.Rows.Count > 0 Then
                            txtspv.Text = dtbspv.Rows(0)!namaspv.ToString
                            hnikspv.Value = dtbspv.Rows(0)!nik_spv.ToString
                        End If

                        txtnama.Attributes.Add("readonly", "readonly")
                        txtnik.Attributes.Add("readonly", "readonly")
                        txtjabat.Attributes.Add("readonly", "readonly")
                        txtdepar.Attributes.Add("readonly", "readonly")
                        txtspv.Attributes.Add("readonly", "readonly")

                        Dim sdachkadmin As SqlDataAdapter = New SqlDataAdapter(qrychkadmin, sqlConn)
                        sdachkadmin.Fill(dtbchkadmin)

                        If dtbchkadmin.Rows.Count > 0 Then
                            mreport = "<label><a href='ppa_entry_list.aspx'>Report</a></label>"
                        End If

                        Dim sdatask As SqlDataAdapter = New SqlDataAdapter(qrytask, sqlConn)
                        sdatask.Fill(dtbtask)

                        If dtbtask.Rows.Count > 0 Then
                            txtstats.Value = "Edit"
                            For i = 0 To dtbtask.Rows.Count
                                If i = 0 Then
                                    txttitle1.Text = dtbtask.Rows(i)!t_title.ToString
                                    txttask1.Text = dtbtask.Rows(i)!t_task.ToString
                                    txttarget1.Text = dtbtask.Rows(i)!t_target.ToString
                                    txtactual1.Text = dtbtask.Rows(i)!t_actual.ToString
                                    txtuom1.Text = dtbtask.Rows(i)!uom.ToString

                                    If dtbtask.Rows(i)!t_score.ToString = "1" Then
                                        t11.Checked = True
                                    ElseIf dtbtask.Rows(i)!t_score.ToString = "2" Then
                                        t12.Checked = True
                                    ElseIf dtbtask.Rows(i)!t_score.ToString = "3" Then
                                        t13.Checked = True
                                    ElseIf dtbtask.Rows(i)!t_score.ToString = "4" Then
                                        t14.Checked = True
                                    ElseIf dtbtask.Rows(i)!t_score.ToString = "5" Then
                                        t15.Checked = True
                                    End If
                                End If

                                If i = 1 Then
                                    txttitle2.Text = dtbtask.Rows(i)!t_title.ToString
                                    txttask2.Text = dtbtask.Rows(i)!t_task.ToString
                                    txttarget2.Text = dtbtask.Rows(i)!t_target.ToString
                                    txtactual2.Text = dtbtask.Rows(i)!t_actual.ToString
                                    txtuom2.Text = dtbtask.Rows(i)!uom.ToString

                                    If dtbtask.Rows(i)!t_score.ToString = "1" Then
                                        t21.Checked = True
                                    ElseIf dtbtask.Rows(i)!t_score.ToString = "2" Then
                                        t22.Checked = True
                                    ElseIf dtbtask.Rows(i)!t_score.ToString = "3" Then
                                        t23.Checked = True
                                    ElseIf dtbtask.Rows(i)!t_score.ToString = "4" Then
                                        t24.Checked = True
                                    ElseIf dtbtask.Rows(i)!t_score.ToString = "5" Then
                                        t25.Checked = True
                                    End If
                                End If

                                If i = 2 Then
                                    txttitle3.Text = dtbtask.Rows(i)!t_title.ToString
                                    txttask3.Text = dtbtask.Rows(i)!t_task.ToString
                                    txttarget3.Text = dtbtask.Rows(i)!t_target.ToString
                                    txtactual3.Text = dtbtask.Rows(i)!t_actual.ToString
                                    txtuom3.Text = dtbtask.Rows(i)!uom.ToString

                                    If dtbtask.Rows(i)!t_score.ToString = "1" Then
                                        t31.Checked = True
                                    ElseIf dtbtask.Rows(i)!t_score.ToString = "2" Then
                                        t32.Checked = True
                                    ElseIf dtbtask.Rows(i)!t_score.ToString = "3" Then
                                        t33.Checked = True
                                    ElseIf dtbtask.Rows(i)!t_score.ToString = "4" Then
                                        t34.Checked = True
                                    ElseIf dtbtask.Rows(i)!t_score.ToString = "5" Then
                                        t35.Checked = True
                                    End If
                                End If
                            Next
                        Else
                            txtstats.Value = ""
                        End If
                    Else
                        Response.Redirect("ppa_entry.aspx")
                    End If
                Else
                    Response.Redirect("login_ppa.aspx")
                End If

            Catch ex As Exception

            Finally
                sqlConn.Close()
            End Try

        End If
    End Sub

    Private Sub cmdSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSubmit.Click
        Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("localdev").ConnectionString)
        Dim sqlcon2 As New SqlConnection(ConfigurationManager.ConnectionStrings("localdev").ConnectionString)
        Dim i As Integer
        Dim sqlsave_task = ""
        Dim iscore As Integer
        Dim cmd_task As SqlCommand = sqlcon.CreateCommand()
        Dim trans As SqlTransaction = Nothing

        If txttitle1.Text.Contains("'") Or txttask1.Text.Contains("'") Or txttarget1.Text.Contains("'") Or txtactual1.Text.Contains("'") Or txtuom1.Text.Contains("'") Then
            txttitle1.Text = txttitle1.Text.Replace("'", "")
            txttask1.Text = txttask1.Text.Replace("'", "")
            txttarget1.Text = txttarget1.Text.Replace("'", "")
            txtactual1.Text = txtactual1.Text.Replace("'", "")
            txtuom1.Text = txtuom1.Text.Replace("'", "")
            'msg = "<script type ='text/javascript' > alert('Save Failed : please do not use quotation marks') </script>"
            'Return
        End If
        If txttitle2.Text.Contains("'") Or txttask2.Text.Contains("'") Or txttarget2.Text.Contains("'") Or txtactual2.Text.Contains("'") Or txtuom2.Text.Contains("'") Then
            txttitle2.Text = txttitle2.Text.Replace("'", "")
            txttask2.Text = txttask2.Text.Replace("'", "")
            txttarget2.Text = txttarget2.Text.Replace("'", "")
            txtactual2.Text = txtactual2.Text.Replace("'", "")
            txtuom2.Text = txtuom2.Text.Replace("'", "")
            'msg = "<script type ='text/javascript' > alert('Save Failed : please do not use quotation marks') </script>"
            'Return
        End If
        If txttitle3.Text.Contains("'") Or txttask3.Text.Contains("'") Or txttarget3.Text.Contains("'") Or txtactual3.Text.Contains("'") Or txtuom3.Text.Contains("'") Then
            txttitle3.Text = txttitle3.Text.Replace("'", "")
            txttask3.Text = txttask3.Text.Replace("'", "")
            txttarget3.Text = txttarget3.Text.Replace("'", "")
            txtactual3.Text = txtactual3.Text.Replace("'", "")
            txtuom3.Text = txtuom3.Text.Replace("'", "")
            'msg = "<script type ='text/javascript' > alert('Save Failed : please do not use quotation marks') </script>"
            'Return
        End If

        Try
            Dim dtbchk1 As DataTable = New DataTable
            Dim strsqlchk1 As String = "select * from tr_ppa_task where nobuk = '" + Session("ppa_nobuk").ToString + "' and genid = '1'"
            Dim sdachk1 As SqlDataAdapter = New SqlDataAdapter(strsqlchk1, sqlcon2)
            sdachk1.Fill(dtbchk1)

            Dim dtbchk2 As DataTable = New DataTable
            Dim strsqlchk2 As String = "select * from tr_ppa_task where nobuk = '" + Session("ppa_nobuk").ToString + "' and genid = '2'"
            Dim sdachk2 As SqlDataAdapter = New SqlDataAdapter(strsqlchk2, sqlcon2)
            sdachk2.Fill(dtbchk2)

            Dim dtbchk3 As DataTable = New DataTable
            Dim strsqlchk3 As String = "select * from tr_ppa_task where nobuk = '" + Session("ppa_nobuk").ToString + "' and genid = '3'"
            Dim sdachk3 As SqlDataAdapter = New SqlDataAdapter(strsqlchk3, sqlcon2)
            sdachk3.Fill(dtbchk3)

            sqlcon.Open()
            trans = sqlcon.BeginTransaction()
            cmd_task.Transaction = trans

            If txtstats.Value = "Edit" Then 'update
                For i = 1 To 3
                    If i = 1 Then
                        If t11.Checked = True Then
                            iscore = 1
                        ElseIf t12.Checked = True Then
                            iscore = 2
                        ElseIf t13.Checked = True Then
                            iscore = 3
                        ElseIf t14.Checked = True Then
                            iscore = 4
                        ElseIf t15.Checked = True Then
                            iscore = 5
                        Else
                            iscore = 0
                            'msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry Score number 1') </script>"
                            'Return
                        End If

                        'If txttitle1.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry title number 1') </script>"
                        '    Return
                        'End If

                        'If txttask1.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry task number 1') </script>"
                        '    Return
                        'End If

                        'If txttarget1.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry target number 1') </script>"
                        '    Return
                        'End If

                        'If txtactual1.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry target number 1') </script>"
                        '    Return
                        'End If



                        If dtbchk1.Rows.Count > 0 Then
                            sqlsave_task = "update tr_ppa_task set t_title = '" + txttitle1.Text + "', t_task = '" + txttask1.Text + "', t_target = '" + txttarget1.Text + "', t_actual = '" + txtactual1.Text + "', t_score = '" + iscore.ToString + "', uom = '" + txtuom1.Text + "' where nobuk = '" + Session("ppa_nobuk").ToString + "' and genid = '1'"
                            'cmd_task = New SqlCommand(sqlsave_task, sqlcon)
                            cmd_task.CommandText = sqlsave_task
                            cmd_task.ExecuteNonQuery()
                        Else
                            sqlsave_task = "insert into tr_ppa_task (nobuk, genid, t_title, t_task, t_target, t_actual, t_score, uom) values ('" + Session("ppa_nobuk").ToString + "', '1' , '" + txttitle1.Text + "', '" + txttask1.Text + "', '" + txttarget1.Text + "', '" + txtactual1.Text + "', '" + iscore.ToString + "', '" + txtuom1.Text + "')"
                            'cmd_task = New SqlCommand(sqlsave_task, sqlcon)
                            cmd_task.CommandText = sqlsave_task
                            cmd_task.ExecuteNonQuery()
                        End If


                    End If

                    If i = 2 Then
                        If t21.Checked = True Then
                            iscore = 1
                        ElseIf t22.Checked = True Then
                            iscore = 2
                        ElseIf t23.Checked = True Then
                            iscore = 3
                        ElseIf t24.Checked = True Then
                            iscore = 4
                        ElseIf t25.Checked = True Then
                            iscore = 5
                        Else
                            iscore = 0
                            'msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry Score number 2') </script>"
                            'Return
                        End If

                        'If txttitle2.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry title number 2') </script>"
                        '    Return
                        'End If

                        'If txttask2.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry task number 2') </script>"
                        '    Return
                        'End If

                        'If txttarget2.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry target number 2') </script>"
                        '    Return
                        'End If

                        'If txtactual2.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry target number 2') </script>"
                        '    Return
                        'End If



                        If dtbchk2.Rows.Count > 0 Then
                            sqlsave_task = "update tr_ppa_task set t_title = '" + txttitle2.Text + "', t_task = '" + txttask2.Text + "', t_target = '" + txttarget2.Text + "', t_actual = '" + txtactual2.Text + "', t_score = '" + iscore.ToString + "', uom = '" + txtuom2.Text + "' where nobuk = '" + Session("ppa_nobuk").ToString + "' and genid = '2'"
                            'cmd_task = New SqlCommand(sqlsave_task, sqlcon)
                            cmd_task.CommandText = sqlsave_task
                            cmd_task.ExecuteNonQuery()
                        Else
                            sqlsave_task = "insert into tr_ppa_task (nobuk, genid, t_title, t_task, t_target, t_actual, t_score, uom) values ('" + Session("ppa_nobuk").ToString + "', '2', '" + txttitle2.Text + "', '" + txttask2.Text + "', '" + txttarget2.Text + "', '" + txtactual2.Text + "', '" + iscore.ToString + "', '" + txtuom2.Text + "')"
                            'cmd_task = New SqlCommand(sqlsave_task, sqlcon)
                            cmd_task.CommandText = sqlsave_task
                            cmd_task.ExecuteNonQuery()
                        End If


                    End If

                    If i = 3 Then
                        If t31.Checked = True Then
                            iscore = 1
                        ElseIf t32.Checked = True Then
                            iscore = 2
                        ElseIf t33.Checked = True Then
                            iscore = 3
                        ElseIf t34.Checked = True Then
                            iscore = 4
                        ElseIf t35.Checked = True Then
                            iscore = 5
                        Else
                            iscore = 0
                            'msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry Score number 3') </script>"
                            'Return
                        End If

                        'If txttitle3.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry title number 3') </script>"
                        '    Return
                        'End If

                        'If txttask3.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry task number 3') </script>"
                        '    Return
                        'End If

                        'If txttarget3.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry target number 3') </script>"
                        '    Return
                        'End If

                        'If txtactual3.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry target number 3') </script>"
                        '    Return
                        'End If



                        If dtbchk3.Rows.Count > 0 Then
                            sqlsave_task = "update tr_ppa_task set t_title = '" + txttitle3.Text + "', t_task = '" + txttask3.Text + "', t_target = '" + txttarget3.Text + "', t_actual = '" + txtactual3.Text + "', t_score = '" + iscore.ToString + "', uom = '" + txtuom3.Text + "' where nobuk = '" + Session("ppa_nobuk").ToString + "' and genid = '3'"
                            'cmd_task = New SqlCommand(sqlsave_task, sqlcon)
                            cmd_task.CommandText = sqlsave_task
                            cmd_task.ExecuteNonQuery()
                        Else
                            sqlsave_task = "insert into tr_ppa_task (nobuk, genid, t_title, t_task, t_target, t_actual, t_score, uom) values ('" + Session("ppa_nobuk").ToString + "', '3', '" + txttitle3.Text + "', '" + txttask3.Text + "', '" + txttarget3.Text + "', '" + txtactual3.Text + "', '" + iscore.ToString + "', '" + txtuom3.Text + "')"
                            'cmd_task = New SqlCommand(sqlsave_task, sqlcon)
                            cmd_task.CommandText = sqlsave_task
                            cmd_task.ExecuteNonQuery()
                        End If
                    End If
                Next
                trans.Commit()
                msg = "<script type ='text/javascript' > alert('Data has been update') </script>"
            Else 'insert
                For i = 1 To 3
                    If i = 1 Then
                        'If t11.Checked = True Then
                        '    iscore = 1
                        'ElseIf t12.Checked = True Then
                        '    iscore = 2
                        'ElseIf t13.Checked = True Then
                        '    iscore = 3
                        'ElseIf t14.Checked = True Then
                        '    iscore = 4
                        'ElseIf t15.Checked = True Then
                        '    iscore = 5
                        'Else
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry Score number 1') </script>"
                        '    Return
                        'End If

                        'If txttitle1.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry title number 1') </script>"
                        '    Return
                        'End If

                        'If txttask1.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry task number 1') </script>"
                        '    Return
                        'End If

                        'If txttarget1.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry target number 1') </script>"
                        '    Return
                        'End If

                        'If txtactual1.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry target number 1') </script>"
                        '    Return
                        'End If

                        sqlsave_task = "insert into tr_ppa_task (nobuk, genid, t_title, t_task, t_target, t_actual, t_score, uom) values ('" + Session("ppa_nobuk").ToString + "', '1' , '" + txttitle1.Text + "', '" + txttask1.Text + "', '" + txttarget1.Text + "', '" + txtactual1.Text + "', '" + iscore.ToString + "', '" + txtuom1.Text + "')"
                        'cmd_task = New SqlCommand(sqlsave_task, sqlcon)
                        cmd_task.CommandText = sqlsave_task
                        cmd_task.ExecuteNonQuery()
                    End If

                    If i = 2 Then
                        'If t21.Checked = True Then
                        '    iscore = 1
                        'ElseIf t22.Checked = True Then
                        '    iscore = 2
                        'ElseIf t23.Checked = True Then
                        '    iscore = 3
                        'ElseIf t24.Checked = True Then
                        '    iscore = 4
                        'ElseIf t25.Checked = True Then
                        '    iscore = 5
                        'Else
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry Score number 2') </script>"
                        '    Return
                        'End If

                        'If txttitle2.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry title number 2') </script>"
                        '    Return
                        'End If

                        'If txttask2.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry task number 2') </script>"
                        '    Return
                        'End If

                        'If txttarget2.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry target number 2') </script>"
                        '    Return
                        'End If

                        'If txtactual2.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry target number 2') </script>"
                        '    Return
                        'End If

                        sqlsave_task = "insert into tr_ppa_task (nobuk, genid, t_title, t_task, t_target, t_actual, t_score, uom) values ('" + Session("ppa_nobuk").ToString + "', '2', '" + txttitle2.Text + "', '" + txttask2.Text + "', '" + txttarget2.Text + "', '" + txtactual2.Text + "', '" + iscore.ToString + "', '" + txtuom2.Text + "')"
                        'cmd_task = New SqlCommand(sqlsave_task, sqlcon)
                        cmd_task.CommandText = sqlsave_task
                        cmd_task.ExecuteNonQuery()
                    End If

                    If i = 3 Then
                        'If t31.Checked = True Then
                        '    iscore = 1
                        'ElseIf t32.Checked = True Then
                        '    iscore = 2
                        'ElseIf t33.Checked = True Then
                        '    iscore = 3
                        'ElseIf t34.Checked = True Then
                        '    iscore = 4
                        'ElseIf t35.Checked = True Then
                        '    iscore = 5
                        'Else
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry Score number 3') </script>"
                        '    Return
                        'End If

                        'If txttitle3.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry title number 3') </script>"
                        '    Return
                        'End If

                        'If txttask3.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry task number 3') </script>"
                        '    Return
                        'End If

                        'If txttarget3.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry target number 3') </script>"
                        '    Return
                        'End If

                        'If txtactual3.Text = "" Then
                        '    msg = "<script type ='text/javascript' > alert('Save Failed : Please Entry target number 3') </script>"
                        '    Return
                        'End If

                        sqlsave_task = "insert into tr_ppa_task (nobuk, genid, t_title, t_task, t_target, t_actual, t_score, uom) values ('" + Session("ppa_nobuk").ToString + "', '3', '" + txttitle3.Text + "', '" + txttask3.Text + "', '" + txttarget3.Text + "', '" + txtactual3.Text + "', '" + iscore.ToString + "', '" + txtuom3.Text + "')"
                        'cmd_task = New SqlCommand(sqlsave_task, sqlcon)
                        cmd_task.CommandText = sqlsave_task
                        cmd_task.ExecuteNonQuery()
                    End If
                Next
                trans.Commit()
                msg = "<script type ='text/javascript' > alert('Data has been save') </script>"
                txtstats.Value = "Edit"
            End If

        Catch ex As Exception
            trans.Rollback()
        Finally
            sqlcon.Close()
        End Try
    End Sub

    Protected Sub cmdprint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdprint.Click
        Response.Redirect("print.aspx")
    End Sub

    Protected Sub imbtnprev_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbtnprev.Click
        Response.Redirect("print.aspx")
    End Sub
End Class