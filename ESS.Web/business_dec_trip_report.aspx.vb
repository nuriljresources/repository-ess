﻿Imports System.IO

Partial Public Class business_dec_trip_report
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim decno As String
        decno = Request.QueryString("id")

        arg1.Text = decno
        arg2.Text = decno
        arg3.Text = decno
    End Sub

    Protected Sub btnsavepdf_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsavepdf.Click
        Dim mimeType As String
        Dim Encoding As String
        Dim fileNameExtension As String
        Dim streams As String()
        Dim warnings As Microsoft.Reporting.WebForms.Warning()

        Dim pdfContent As Byte() = ReportViewer1.LocalReport.Render("PDF", Nothing, mimeType, Encoding, fileNameExtension, streams, warnings)
        'Creatr PDF file on disk
        Dim pdfPath As String = Server.MapPath("~/export/")
        pdfPath = Path.Combine(pdfPath, String.Format("dec{0}.pdf", Session("otorisasi")))

        'Dim pdfPath As String = "C:\inetpub\wwwroot\HRMS\export\dec" + Session("otorisasi") + ".pdf"
        Dim pdfFile As New System.IO.FileStream(pdfPath, System.IO.FileMode.Create)
        pdfFile.Write(pdfContent, 0, pdfContent.Length)
        pdfFile.Close()


        Response.Redirect("export\dec" + Session("otorisasi") + ".pdf")
    End Sub

End Class