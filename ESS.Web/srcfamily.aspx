﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="srcfamily.aspx.vb" Inherits="EXCELLENT.srcfamily" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <link href="css/site.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function UseSelected(id, nama, status, tgllahir) {
          var pw = window.opener;
          var inputFrm = pw.document.forms['aspnetForm'];
          if (inputFrm.elements['ctrlToFind'].value == 'Originator') {
              inputFrm.elements['ctl00_ContentPlaceHolder1_Originator'].value = id;
              inputFrm.elements['ctl00_ContentPlaceHolder1_OriginatorName'].value = nama;
          } else if (inputFrm.elements['ctrlToFind'].value == 'srcfamily') {
          inputFrm.elements[inputFrm.elements['ctl00_ContentPlaceHolder1_cnt2'].value].value = id;
          inputFrm.elements[inputFrm.elements['ctl00_ContentPlaceHolder1_cnt'].value].value = nama;
          } else {
              inputFrm.elements[inputFrm.elements['ctrlToFind'].value].value = id;
              inputFrm.elements[inputFrm.elements['ctrlToFind'].value + 'name'].value = nama;
          }
          window.close();
       }
       function Left(str, n) {
          if (n <= 0)
             return "";
          else if (n > String(str).length)
             return str;
          else
             return String(str).substring(0, n);
       }

       function Right(str, n) {
          if (n <= 0)
             return "";
          else if (n > String(str).length)
             return str;
          else {
             var iLen = String(str).length;
             return String(str).substring(iLen, iLen - n);
          }
      }
    </script>
</head>
<body>
    <form id="form1" runat="server">
  <ajaxToolkit:ToolkitScriptManager  ID="ToolkitScriptManager1" runat="server"/>
   <asp:UpdatePanel id="MyUpdatePanel" runat="server">
    <ContentTemplate>
      <div>           

      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
         <caption style="text-align:center">Select Name</caption>
         <tr>
            <td style="padding: 5px; width:20%"></td>
            <td style="width:15%"><asp:TextBox ID="SearchKey" runat="server" class="tb10" onkeydown="keyPressed(event);"/></td>
            <td><input type="text" id="hide" style="display:none;"/><asp:Button ID="btnSearch" 
                    runat="server" Text="Cari Nama" Font-Bold="False" ForeColor="#0033CC" 
                    OnClientClick="cekArgumen()" Height="26px" Width="90px"/></td>
         </tr>
         <tr>
         <td>
         </td>
         </tr>
         <tr>
         <td colspan="3">
         <asp:Repeater ID="rptPages" Runat="server">
      <HeaderTemplate>
      <table cellpadding="0" cellspacing="0" border="0">
      <tr>
         <td style="vertical-align:top"><b>Page:</b>&nbsp;</td>
         <td>
      </HeaderTemplate>
      <ItemTemplate>
                        <asp:LinkButton ID="btnPage" CommandName="Page" 
                         CommandArgument="<%# Container.DataItem %>" 
                         CssClass="text" 
                         Runat="server" 
                         Visible='<%# (CInt(Container.DataItem) <> (PageNumber+1)).ToString() %>'>
                         <%#Container.DataItem%> 
                         </asp:LinkButton>
                         <asp:Label ID="lblPage" runat="server" Visible='<%# (CInt(Container.DataItem) = (PageNumber+1)).ToString()%>' 
                         Text="<%#Container.DataItem%>" 
                         ForeColor="black" Font-Bold="true">
                         </asp:Label>
      </ItemTemplate>
      <FooterTemplate>
         </td>
      </tr>
      </td>
      </table>
      </FooterTemplate>
      </asp:Repeater>
         </tr>
         <tr>
            <td colspan="3">
               <asp:Repeater ID="rptResult" runat="server">
                        <HeaderTemplate>
                           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table-inside">
                           <tr>               
                                <th>No.</th>
                                <th>Nik</th>
                                <th>Name</th>
                                <th>Status</th>
                                <%--<th>Department</th>
                                <th>Jobsite</th>
                                <th>Position</th>--%>
                           </tr>
                        </HeaderTemplate>  
                        <ItemTemplate>
                        <tr>
                        <td><%# Container.ItemIndex + 1 %></td>
                        <td><a href="#" onclick="UseSelected('<%#DataBinder.Eval(Container.DataItem, "FamilyID")%>','<%#DataBinder.Eval(Container.DataItem, "FamilyName")%>','<%#DataBinder.Eval(Container.DataItem, "familyStatus")%>','<%#DataBinder.Eval(Container.DataItem, "FamilyTglLahir")%>');"><%#DataBinder.Eval(Container.DataItem, "FamilyID")%></a></td>
                        <td><%#DataBinder.Eval(Container.DataItem, "FamilyName")%></td>
                        <td><%#DataBinder.Eval(Container.DataItem, "familyStatus")%></td>
                        <%--<td align="center"><%#DataBinder.Eval(Container.DataItem, "FamilyTglLahir")%></td>--%>
                        </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                           </table>
                        </FooterTemplate>
               </asp:Repeater>             
            </td>
         </tr>
      
      </table>  
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
     <asp:UpdateProgress ID="progress1" runat="server" DisplayAfter="50">    
                  <ProgressTemplate>
                  <asp:Panel ID="pnlProgressBar" runat="server">    
                     <div><asp:Image ID="Image1" runat="Server" ImageUrl="images/ajax-loader.gif"/>Please 
                         Wait...</div>    
                  </asp:Panel>   
                  </ProgressTemplate> 
                  </asp:UpdateProgress>
    </form>
</body>
</html>
