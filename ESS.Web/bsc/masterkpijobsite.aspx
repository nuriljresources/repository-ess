﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="masterkpijobsite.aspx.vb" Inherits="EXCELLENT.masterkpijobsite" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Master KPI</title>
    <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/json2.js" type="text/javascript"></script>
    <script type="text/javascript">
       var TABLE_NAME = 'childKPI';
       function addRowToTable(kpiid, kpiname, funcname) {
          var tbl = document.getElementById(TABLE_NAME);
          var nextRow = tbl.tBodies[0].rows.length;          
          var num = nextRow;
          var row = tbl.tBodies[0].insertRow(num);
          
          var cell0 = row.insertCell(0);
          var txtInp = document.createElement('input');
          txtInp.setAttribute('type', 'text');
          txtInp.setAttribute('name', 'nox' + num + 1);
          txtInp.setAttribute('id', 'nox' + num + 1);
          txtInp.setAttribute('size', '1');
          txtInp.setAttribute('class', 'tb09');
          txtInp.setAttribute('disabled', 'disabled');
          txtInp.setAttribute('value', num + 1);
          cell0.appendChild(txtInp);
          
          var cell1 = row.insertCell(1);
          var txtInp = document.createElement('input');
          txtInp.setAttribute('type', 'hidden');
          txtInp.setAttribute('name', 'kcid' + num + 1);
          txtInp.setAttribute('id', 'kcid' + num + 1);
          txtInp.setAttribute('value', kpiid);
          cell1.appendChild(txtInp);
          
          var txtInp = document.createElement('input');
          txtInp.setAttribute('type', 'text');
          txtInp.setAttribute('name', 'kcname' + num + 1);
          txtInp.setAttribute('id', 'kcname' + num + 1);
          txtInp.setAttribute('disabled', 'disabled');
          txtInp.setAttribute('style', 'width:99%');
          txtInp.setAttribute('value', kpiname);
          cell1.appendChild(txtInp);

          var cell2 = row.insertCell(2);
          var txtInp = document.createElement('input');
          txtInp.setAttribute('type', 'text');
          txtInp.setAttribute('name', 'funcname' + num + 1);
          txtInp.setAttribute('id', 'funcname' + num + 1);
          txtInp.setAttribute('disabled', 'disabled');
          txtInp.setAttribute('style', 'width:99%');
          txtInp.setAttribute('value', funcname);
          cell2.appendChild(txtInp);          
          
          var cell3 = row.insertCell(3);
          cell3.innerHTML = cell3.innerHTML + '<center><img title="Delete Row" class="tcalIcon" onclick="delRow(document.getElementById(\'nox' + num + 1 + '\').value,' + kpiid  + ')" src="images/cross.gif"/></center>';        
       }

       function delRow(num, kpiid) {
             //alert("{'_idparent':" + JSON.stringify(document.getElementById("KPIID").value) + ",'_idchild':" + JSON.stringify(kpiid) + "}");
          $.ajax({
             type: "POST",
             contentType: "application/json; charset=utf-8",
             url: "WS/BSCService.asmx/DelKPIRelation",
             data: "{'_idparent':" + JSON.stringify(document.getElementById("KPIID").value) + ",'_idchild':" + JSON.stringify(kpiid) + "}",
             dataType: "json",
             success: function(res) {
                if (res.d == "SUCCESS") {
                   var tbl = document.getElementById(TABLE_NAME);
                   tbl.deleteRow(num);
                   reorderRows(tbl);
                } else {
                   alert(res.d);
                }
             },
             error: function(err) {
                alert("error: " + err.responseText);
             }
          });
       }

       function reorderRows(tbl) {
         if (tbl.tBodies[0].rows[0]) {
                var count = 1;
                for (var i = 0; i < tbl.tBodies[0].rows.length; i++) {
                   if (tbl.tBodies[0].rows[i].style.display != 'none') {
                      tbl.tBodies[0].rows[i].cells[0].getElementsByTagName('input')[0].value = count;
                      count++;
                   }                
             }
          }
       }

       function emptyRow() {
          var tbl = document.getElementById(TABLE_NAME);
          for (var i = tbl.rows.length; i > 1; i--) {
             tbl.deleteRow(i - 1);
          }        
       }

       function openSearchModal() {
          var returnVal = window.showModalDialog('PopUpSearch.aspx?type=3&site=' + document.getElementById("KPIJobsite").value, '', 'dialogWidth=575px;dialogHeight=570px;resizable=yes;help=no;unadorned=yes');
          //var kpiid = ''
          if (returnVal != undefined) {
             $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "WS/BSCService.asmx/RetrieveKPI",
                data: "{'_id':" + returnVal + "}",
                dataType: "json",
                success: function(res) {
                   //kpiid = res.d.Id;
                   document.getElementById("KPIID").value = res.d.Id;
                   document.getElementById("KPILevel").value = res.d.Level;
                   document.getElementById("SO").value = res.d.Soid;
                   document.getElementById("KPIName").value = res.d.Name;
                   document.getElementById("KPIUOM").value = res.d.Uom;
                   document.getElementById("KPIOwner").value = res.d.Owner;
                   document.getElementById("KPIType").value = res.d.Type;
                   document.getElementById("KPIDesc").value = res.d.Desc;
                   document.getElementById("KPIPeriod").value = res.d.Periode;
                   document.getElementById("KPILeads").value = res.d.Leads;
                   document.getElementById("KPIFormula").value = res.d.Formula;
                   document.getElementById("KPIFDesc").value = res.d.FormulaDesc;
                   document.getElementById("KPIRed").value = res.d.RedParam;
                   document.getElementById("KPIGreen").value = res.d.GreenParam;
                   document.getElementById("KPIBased").value = res.d.BasedOn;
                   document.getElementById("KPICalcType").value = res.d.CalcType;
                   document.getElementById("KPIJobsite").value = res.d.Jobsite;

                   //GET CHILD
                   $.ajax({
                      type: "POST",
                      contentType: "application/json; charset=utf-8",
                      url: "WS/BSCService.asmx/RetrieveChild",
                      data: "{'_idparent':" + document.getElementById("KPIID").value + ",'_type':3}",
                      dataType: "json",
                      success: function(res) {
                         MyIssue = new Array();
                         MyIssue = res.d;
                         emptyRow();
                         for (var i = 0; i < MyIssue.length; i++) {
                            addRowToTable(MyIssue[i].IdKPI, MyIssue[i].NamaKPI, MyIssue[i].FuncKPI);
                         }
                      },
                      error: function(err) {
                         alert(err.responseText);
                      }
                   });
                   //END GET CHILD   
                 },
                error: function(err) {
                   alert(err.responseText);
                }
             });
             
                    
             
          }         
       }

       function openModal() {
          if (document.getElementById("KPIID").value != "[AUTO]") {
             var selObj = document.getElementById('KPILevel');
             var selIndex = selObj.selectedIndex;
             var returnVal = window.showModalDialog('popupsectionkpi.aspx?type=3&site=' + document.getElementById("KPIJobsite").value, '', 'dialogWidth=575px;dialogHeight=570px;resizable=yes;help=no;unadorned=yes');
             if (returnVal != undefined) {
                $.ajax({
                   type: "POST",
                   contentType: "application/json; charset=utf-8",
                   url: "WS/BSCService.asmx/AddKPIRelation",
                   data: "{'_idparent':" + document.getElementById("KPIID").value + ",'_idchild':" + returnVal  + "}",
                   dataType: "json",
                   success: function(res) {
                            $.ajax({
                               type: "POST",
                               contentType: "application/json; charset=utf-8",
                               url: "WS/BSCService.asmx/RetrieveChild",
                               data: "{'_idparent':" + document.getElementById("KPIID").value + ",'_type':3}",
                               dataType: "json",
                               success: function(res) {
                                  MyIssue = new Array();
                                  MyIssue = res.d;
                                  emptyRow();
                                  for (var i = 0; i < MyIssue.length; i++) {
                                     addRowToTable(MyIssue[i].IdKPI, MyIssue[i].NamaKPI, MyIssue[i].FuncKPI);
                                  }
                         },
                         error: function(err) {
                            alert(err.responseText);
                         }
                      });
                   },
                   error: function(err) {
                      alert(err.responseText);
                   }
                });
             }
          } else {
             alert("Harap Simpan Data KPI Anda terlebih dahulu");
          }
       }
            
    </script>
    <style type="text/css">
       #wrapper{
         font: small Arial, Helvetica, sans-serif;
         font-size:small;
       }    
        td.styleHeader
        {
          background-color:#00CC00;
          color:white;
          font-weight:bold;
          font-size:medium;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </asp:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
            <asp:HiddenField ID="KPIID" runat="server" value="[AUTO]"/>
            <table border="1">
               <tr>
                  <td colspan="4">
                     <asp:Button ID="btnNew" runat="server" Text="New" />
                     <input type="button" id="btnSearch" value="Search" onclick="openSearchModal();"/>
                     <asp:Button ID="btnEdit" runat="server" Text="Edit" />
                     <asp:Button ID="btnDelete" runat="server" Text="Delete" />
                     <asp:Button ID="btnSave" runat="server" Text="Save" />
                  </td>
               </tr>
               <tr>
                  <td colspan="4" class="styleHeader">KPI Definition</td>
               </tr>
               <tr>
                  <td>KPI Level</td>
                  <td colspan="3">
                     <asp:DropDownList ID="KPILevel" runat="server" AutoPostBack=true>
<%--                        <asp:ListItem Value="0" Text="BUMA"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Function"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Section"></asp:ListItem>--%>
                        <asp:ListItem Value="3" Text="Jobsite"></asp:ListItem>
                     </asp:DropDownList>
                        <asp:DropDownList ID="KPIJobsite" runat="server">                                                     
                        </asp:DropDownList>
                        <asp:DropDownList ID="KPISection" runat="server">                                                     
                        </asp:DropDownList>
                     <div id="function" runat="server">
                        <asp:CheckBoxList ID="KPIFunc" runat="server" RepeatDirection="Horizontal">                                                     
                        </asp:CheckBoxList>
                     </div>                  
                  </td>                  
               </tr>
               <tr>
               <td>SO</td>
                  <td colspan="3">
                     <asp:DropDownList ID="SO" runat="server">
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td>KPI Name</td>
                  <td><asp:TextBox ID="KPIName" runat="server" MaxLength="30"></asp:TextBox></td>
                  <td>UoM</td>
                  <td><asp:TextBox ID="KPIUOM" runat="server" MaxLength="10"></asp:TextBox></td>
               </tr>
               <tr>
                  <td>KPI Owner</td>
                  <td><asp:TextBox ID="KPIOwner" runat="server" MaxLength="10"></asp:TextBox></td>
                  <td>KPI Type</td>
                  <td>
                     <asp:DropDownList ID="KPIType" runat="server">
                        <asp:ListItem Text="Lag" Selected="True" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Lead" Value="1"></asp:ListItem>
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td>KPI Description</td>
                  <td colspan="3"><asp:TextBox ID="KPIDesc" runat="server" TextMode="MultiLine" MaxLength="300" rows="5" columns="50"></asp:TextBox></td>
               </tr>
               <tr>
                  <td>Period Of Measurement</td>
                  <td colspan="3">
                   <asp:DropDownList ID="KPIPeriod" runat="server">
                        <asp:ListItem Text="Daily" Selected="True" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Weekly" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Monthly" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Quarterly" Value="3"></asp:ListItem>
                        <asp:ListItem Text="Semester" Value="4"></asp:ListItem>
                        <asp:ListItem Text="Yearly" Value="5"></asp:ListItem>
                     </asp:DropDownList>                 
                  </td>
               </tr>
               <tr>
                  <td>KPI Leads to</td>
                  <td colspan="3"><asp:TextBox ID="KPILeads" runat="server" MaxLength="50"></asp:TextBox></td>
               </tr>               
               <tr>
                  <td colspan="4" class="styleHeader">KPI Parameter</td>
               </tr>
               <tr>
                  <td colspan="2">
                     <table>
                        <tr><td>Red <=</td><td><asp:TextBox ID="KPIRed" runat="server"></asp:TextBox></td></tr>
                        <tr><td>Green >=</td><td><asp:TextBox ID="KPIGreen" runat="server"></asp:TextBox></td></tr>
                     </table>
                  </td>
                  <td colspan="2">
                     <table>
                        <tr><td>Based On</td>
                        <td>
                           <asp:DropDownList ID="KPIBased" runat="server">
                              <asp:ListItem value="0" Text="Achievement" Selected="true"></asp:ListItem>
                              <asp:ListItem value="1" Text="Value"></asp:ListItem>
                           </asp:DropDownList>                           
                        </td>
                        </tr>
                        <tr><td>Calculation Type</td>
                        <td>
                         <asp:DropDownList ID="KPICalcType" runat="server">
                              <asp:ListItem value="0" Text="Small" Selected="true"></asp:ListItem>
                              <asp:ListItem value="1" Text="Big"></asp:ListItem>
                           </asp:DropDownList>  
                        
                        </td>
                        </tr>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td colspan="4" class="styleHeader">KPI Formulation</td>
               </tr>
               <tr>
                  <td>Formula</td>
                  <td colspan="3">
                     <asp:DropDownList ID="KPIFormula" runat="server">
                        <asp:ListItem Text="None" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Equal" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Sum" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Average" Value="3"></asp:ListItem>
                        <asp:ListItem Text="Weighted Average" Value="4"></asp:ListItem>
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td>Description</td>
                  <td colspan="3">
                  <asp:TextBox ID="KPIFDesc" runat="server" TextMode="MultiLine" MaxLength="150" rows="3" columns="50"></asp:TextBox>
                  </td>
               </tr>
              <tr>
                  <td colspan="4" class="styleHeader">Child KPI</td>
               </tr>
              <tr>
                  <td colspan="4">
                     <%--<asp:Button ID="btnNewChild" runat="server" Text="New Child" OnClientClick="addRowToTable('1','1');"/>--%>
                     <input type="button" id="btnNewChild" value="New Child" onclick="openModal();"/>
                     <asp:Button ID="btnSearch" runat="server" Text="Search" />
                   </td>
               </tr>
               <tr>
                  <td colspan="4" style="width:100%">
                     <table id="childKPI" style="width:100%" >
                        <thead>
                           <tr>
                              <th style="width:2%">No.</th>
                              <th style="width:83%">Nama KPI</th>
                              <th style="width:10%">Section</th>
                              <th style="width:5%">Action</th>
                           </tr>
                        </thead>
                        <tbody></tbody>
                     </table>
                  </td>
               </tr>
            </table>
         </ContentTemplate>
        </asp:UpdatePanel>
        
    </div>
    </form>
</body>
</html>
