﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Partial Public Class trip_list
    Inherits System.Web.UI.Page
    Public tglaw As String
    Public th As String
    Public v1 As String
    Public v2 As String
    Public imonth As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") = "" Or Session("permission") = Nothing Then
            Response.Redirect("Login.aspx", False)
            Exit Sub
        End If

        Dim a As String
        tglaw = Request(ddlbulan.UniqueID)
        th = Request(ddltahun.UniqueID)

        If Not IsPostBack Then
            Dim i As Integer
            ddlbulan.Items.Clear()
            ddlbulan.Items.Add("January")
            ddlbulan.Items.Add("February")
            ddlbulan.Items.Add("March")
            ddlbulan.Items.Add("April")
            ddlbulan.Items.Add("May")
            ddlbulan.Items.Add("June")
            ddlbulan.Items.Add("July")
            ddlbulan.Items.Add("August")
            ddlbulan.Items.Add("September")
            ddlbulan.Items.Add("October")
            ddlbulan.Items.Add("November")
            ddlbulan.Items.Add("December")

            Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

            sqlConn.Open()
            Dim strcon As String = "select min(reqdate) as min_date ,max(reqdate) as max_date from V_H001"
            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            v1 = dtb.Rows(0)!min_date.ToString
            v1 = CDate(v1).ToString("yyyy")

            v2 = dtb.Rows(0)!max_date.ToString
            v2 = CDate(v2).ToString("yyyy")

            ddltahun.Items.Clear()
            For i = v1 To v2
                ddltahun.Items.Add(i)
            Next

            ddltahun.SelectedValue = (i - 1).ToString

            'If Not IsPostBack Then
            'If tripfrom.Text = "" And tripto.Text = "" Then
            '    tripfrom.Text = Date.Now.ToString
            '    tripto.Text = Date.Now.ToString
            'End If
            'tripfrom.Text = CDate(tripfrom.Text).ToString("MM/dd/yyyy")
            'tripto.Text = CDate(tripto.Text).ToString("MM/dd/yyyy")
            'tglaw = tripfrom.Text
            'tglak = tripto.Text
            'v1 = tripfrom.Text
            'v2 = tripto.Text

            'Else
            'tglaw = Format(DateTime.ParseExact(Request(tripfrom.UniqueID), "MM/dd/yyyy", Nothing), "MM/dd/yyyy")
            'tglak = Format(DateTime.ParseExact(Request(tripto.UniqueID), "MM/dd/yyyy", Nothing), "MM/dd/yyyy")
            'tglaw = tripfrom.Text
            'tglak = tripto.Text
            'a = ddlbulan.SelectedItem.ToString
            ' a = ddlbulan.SelectedIndex.ToString
            'End If

            'Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

            'Dim strcon As String = "select * from V_H001"

            'Dim dtb As DataTable = New DataTable()
            'Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            'sda.Fill(dtb)

            

            'strcon = "select tripno, nik, sup_nik, detailtrip, purpose, tripfrom, tripto, apprby, apprdate from V_H001"
            'strcon = strcon + " where month(reqdate) = '" + imonth.ToString + "' and YEAR(reqdate) = '" + th + "' and nik = '1231313' and stedit <> 2"

            'sda = New SqlDataAdapter(strcon, sqlConn)
            'sda.Fill(dtb)
            'If ddlbulan.SelectedIndex < 0 Then ddlbulan.SelectedIndex = 0
            'If ddltahun.SelectedIndex < 0 Then ddltahun.SelectedIndex = 0
            'GridView1.DataSource = dtb
            'GridView1.DataBind()
            'Try
            '    sqlConn.Open()
            '    Dim dtb As DataTable = New DataTable()

            '    Dim strcon As String = "select tripno, nik, sup_nik, detailtrip, purpose, tripfrom, tripto, apprby, apprdate from V_H001 "
            '    strcon = strcon + " where reqdate between '" + tglaw + "' and '" + tglak + "'"
            '    Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            '    sda.Fill(dtb)

            '    GridView1.DataSource = dtb
            '    GridView1.DataBind()
            'Catch ex As Exception

            'End Try
        End If
        Select Case tglaw
            Case "January"
                imonth = 1
            Case "February"
                imonth = 2
            Case "March"
                imonth = 3
            Case "April"
                imonth = 4
            Case "May"
                imonth = 5
            Case "June"
                imonth = 6
            Case "July"
                imonth = 7
            Case "August"
                imonth = 8
            Case "September"
                imonth = 9
            Case "October"
                imonth = 10
            Case "November"
                imonth = 11
            Case "December"
                imonth = 12
            Case Else
                imonth = 1
        End Select
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        'Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        'Try
        '    sqlConn.Open()
        '    Dim dtb As DataTable = New DataTable()

        '    Dim strcon As String = "select tripno, nik, sup_nik, detailtrip, purpose, tripfrom, tripto, apprby, apprdate from V_H001 "
        '    strcon = strcon + "where reqdate between '" + tglaw + "' and '" + tglak + "'"
        '    Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
        '    sda.Fill(dtb)

        '    GridView1.DataSource = dtb
       
        'Catch ex As Exception

        'End Try

        'GridView1.DataBind()

        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()

            Dim strcon As String = "select A.tripno, A.nik, (select niksite from H_A101 where nik = A.nik) as niksite, (select nama from H_A101 where nik = A.nik) as nama, (select nama from H_A101 where nik = A.sup_nik ) as superior, A.sup_nik, case A.detailtrip when '01' then 'Business Trip' when '02' then 'Annual Leave' when '03' then 'Periodic Leave' when '04' then 'Recruitment' when '05' then 'Training' when '06' then 'Visitor' end as detailtrip, A.purpose, convert(char(10),A.tripfrom,101) as tripfrom, convert(char(10),A.tripto,101) as tripto, A.apprby, (select nama from H_A101 where nik = A.apprby ) as approve, convert(char(10), A.apprdate, 101) as apprdate, case A.fstatus when 1 then 'Process' when 0 then 'Unprocess' end as fstatus, A.editga, A.reqdate, "
            strcon = strcon + "ISNULL((SELECT TOP 1 Nama FROM H_A101 WHERE NIKSITE = A.NxtActionr), '') AS NxtAction, " & _
                                       "ISNULL(CASE  " & _
                                        "WHEN A.NxtActionrRole = 'DEL' THEN 'Delegator' " & _
                                        "WHEN A.NxtActionrRole = 'MGR' THEN 'Manager' " & _
                                        "WHEN A.NxtActionrRole = 'HRD' THEN 'In HRD' " & _
                                       "END, '') AS picUserRole "
            strcon = strcon + "from V_H001 A where month(A.reqdate) = '" + imonth.ToString + "' and YEAR(A.reqdate) = '" + th + "' and A.stedit <> 2 and A.nik = '" + Session("niksite") + "' UNION "
            strcon = strcon + "select A.tripno, A.nik, (select niksite from H_A101 where nik = A.nik) as niksite, (select nama from H_A101 where nik = A.nik) as nama, (select nama from H_A101 where nik = A.sup_nik ) as superior, A.sup_nik, case A.detailtrip when '01' then 'Business Trip' when '02' then 'Annual Leave' when '03' then 'Periodic Leave' when '04' then 'Recruitment' when '05' then 'Training' when '06' then 'Visitor' end as detailtrip, A.purpose, convert(char(10),A.tripfrom,101) as tripfrom, convert(char(10),A.tripto,101) as tripto, A.apprby, (select nama from H_A101 where nik = A.apprby ) as approve, convert(char(10), A.apprdate, 101) as apprdate, case A.fstatus when 1 then 'Process' when 0 then 'Unprocess' end as fstatus, A.editga, A.reqdate, "
            strcon = strcon + "ISNULL((SELECT TOP 1 Nama FROM H_A101 WHERE NIKSITE = A.NxtActionr), '') AS NxtAction, " & _
                                       "ISNULL(CASE  " & _
                                        "WHEN A.NxtActionrRole = 'DEL' THEN 'Delegator' " & _
                                        "WHEN A.NxtActionrRole = 'MGR' THEN 'Manager' " & _
                                        "WHEN A.NxtActionrRole = 'HRD' THEN 'In HRD' " & _
                                       "END, '') AS picUserRole "
            strcon = strcon + "from V_H001 A, V_A004 B where month(A.reqdate) = '" + imonth.ToString + "' and YEAR(A.reqdate) = '" + th + "' and A.stedit <> 2 and A.nik in(B.nikb) and B.nika ='" + Session("niksite") + "' and B.module='TRAVEL' order by reqdate"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            GridView1.DataSource = dtb
            GridView1.PageIndex = e.NewPageIndex
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        If GridView1.SelectedRow Is Nothing Then Exit Sub

        Dim row As GridViewRow = GridView1.SelectedRow
        Session("vtrip") = GridView1.DataKeys(row.RowIndex).Value.ToString
        'Response.Redirect("view_trip.aspx?ID=" & GridView1.DataKeys(row.RowIndex).Value.ToString)
        Response.Redirect("view_trip.aspx")
    End Sub

    'Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.RowDeleting
    '    Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
    '    'If GridView1.Rows.Item Is Nothing Then Exit Sub


    '    Try
    '        sqlConn.Open()
    '        Dim dtb As DataTable = New DataTable()
    '        Dim cmd As SqlCommand
    '        Dim strcon As String = "update V_H001 set stedit = '2' where TripNo = '" + GridView1.DataKeys(row.RowIndex).Value.ToString + "'"
    '        cmd = New SqlCommand(strcon, sqlConn)
    '        cmd.ExecuteScalar()
    '        sqlConn.Close()
    '    Catch ex As Exception

    '    End Try
    'End Sub



    'Protected Sub btnsrc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsrc.Click
    '    Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
    '    Dim i As Integer
    '    Dim tfrom As String

    '    tfrom = tripfrom.ClientID.ToLower


    '    'If Trim(tripfrom.Text) = "" Then
    '    '    MsgBox("Tanggal awal harus diisi")
    '    'End If

    '    'If Trim(tripto.Text) = "" Then
    '    '    MsgBox("Tanggal Akhir harus diisi")
    '    'End If

    '    Try
    '        sqlConn.Open()
    '        Dim dtb As DataTable = New DataTable()

    '        Dim strcon As String = "select tripno, nik, sup_nik, detailtrip, purpose, tripfrom, tripto, apprby, apprdate from V_H001 "
    '        strcon = strcon + "where reqdate between '" + tripfrom.Text + "' and '" + tripto.Text + "'"
    '        Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
    '        sda.Fill(dtb)

    '        'For i = 0 To dtb.Rows.Count
    '        '    tfrom = dtb.Rows(i)!tripfrom
    '        '    tfrom = CDate(tfrom).ToString("MM-dd-yyyy")

    '        '    dtb.Rows(i)!tripfrom = tfrom
    '        'Next

    '        GridView1.DataSource = dtb
    '        GridView1.DataBind()
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Public Function retrive(ByVal _tripfrom As String, ByVal _tripto As String) As String
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()

            Dim strcon As String = "select tripno, nik, sup_nik, detailtrip, purpose, tripfrom, tripto, apprby, apprdate from V_H001 "
            'strcon = strcon + "where reqdate between '" + tripfrom.Text + "' and '" + tripto.Text + "'"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            'For i = 0 To dtb.Rows.Count
            '    tfrom = dtb.Rows(i)!tripfrom
            '    tfrom = CDate(tfrom).ToString("MM-dd-yyyy")

            '    dtb.Rows(i)!tripfrom = tfrom
            'Next

            GridView1.DataSource = dtb
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Function

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        'Dim dt As DateTime
        'tglaw = Format(DateTime.ParseExact(Request(tripfrom.UniqueID), "MM/dd/yyyy", Nothing), "MM/dd/yyyy")
        'tglak = Format(DateTime.ParseExact(Request(tripto.UniqueID), "MM/dd/yyyy", Nothing), "MM/dd/yyyy")
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()

            Dim strcon As String = "select A.tripno, A.nik, (select niksite from H_A101 where nik = A.nik) as niksite, (select nama from H_A101 where nik = A.nik) as nama, (select nama from H_A101 where nik = A.sup_nik ) as superior, A.sup_nik, case A.detailtrip when '01' then 'Business Trip' when '02' then 'Annual Leave' when '03' then 'Periodic Leave' when '04' then 'Recruitment' when '05' then 'Training' when '06' then 'Visitor' end as detailtrip, A.purpose, convert(char(10),A.tripfrom,101) as tripfrom, convert(char(10),A.tripto,101) as tripto, A.apprby, (select nama from H_A101 where nik = A.apprby ) as approve, convert(char(10), A.apprdate, 101) as apprdate, case A.fstatus when 1 then 'Process' when 0 then 'Unprocess' end as fstatus, A.editga, A.reqdate, "
            strcon = strcon + "ISNULL((SELECT TOP 1 Nama FROM H_A101 WHERE NIKSITE = A.NxtActionr), '') AS NxtAction, " & _
                                       "ISNULL(CASE  " & _
                                        "WHEN A.NxtActionrRole = 'DEL' THEN 'Delegator' " & _
                                        "WHEN A.NxtActionrRole = 'MGR' THEN 'Manager' " & _
                                        "WHEN A.NxtActionrRole = 'HRD' THEN 'In HRD' " & _
                                       "END, '') AS picUserRole "
            strcon = strcon + "from V_H001 A where month(A.reqdate) = '" + imonth.ToString + "' and YEAR(A.reqdate) = '" + th + "' and A.stedit <> 2 and A.nik = '" + Session("niksite") + "' UNION "
            strcon = strcon + "select A.tripno, A.nik, (select niksite from H_A101 where nik = A.nik) as niksite, (select nama from H_A101 where nik = A.nik) as nama, (select nama from H_A101 where nik = A.sup_nik ) as superior, A.sup_nik, case A.detailtrip when '01' then 'Business Trip' when '02' then 'Annual Leave' when '03' then 'Periodic Leave' when '04' then 'Recruitment' when '05' then 'Training' when '06' then 'Visitor' end as detailtrip, A.purpose, convert(char(10),A.tripfrom,101) as tripfrom, convert(char(10),A.tripto,101) as tripto, A.apprby, (select nama from H_A101 where nik = A.apprby ) as approve, convert(char(10), A.apprdate, 101) as apprdate, case A.fstatus when 1 then 'Process' when 0 then 'Unprocess' end as fstatus, A.editga, A.reqdate, "
            strcon = strcon + "ISNULL((SELECT TOP 1 Nama FROM H_A101 WHERE NIKSITE = A.NxtActionr), '') AS NxtAction, " & _
                                       "ISNULL(CASE  " & _
                                        "WHEN A.NxtActionrRole = 'DEL' THEN 'Delegator' " & _
                                        "WHEN A.NxtActionrRole = 'MGR' THEN 'Manager' " & _
                                        "WHEN A.NxtActionrRole = 'HRD' THEN 'In HRD' " & _
                                       "END, '') AS picUserRole "
            strcon = strcon + "from V_H001 A, V_A004 B where month(A.reqdate) = '" + imonth.ToString + "' and YEAR(A.reqdate) = '" + th + "' and A.stedit <> 2 and A.nik in(B.nikb) and B.nika ='" + Session("niksite") + "' and B.module='TRAVEL' order by reqdate"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            GridView1.DataSource = dtb
            'GridView1.PageIndex = 1
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub
End Class