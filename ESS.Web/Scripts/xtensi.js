﻿var pages = 1;
var apage = new Array(4);
apage[0] = 'MEMO merupakan solusi aplikatif berbasis web untuk mengelola penyelesaian isu dalam suatu unit kerja.';
apage[0] = apage[0] + '<br/><br/>Di HO aplikasi ini digunakan oleh pimpinan Unit Kerja beserta jajarannya (Manager level), dan PIC PDCA. Sedangkan di Site digunakan oleh PM/DPM, Section head, dan Sekretariat Project.';
apage[0] = apage[0] + '<br/><br/>Penerapan aplikasi MEMO berdasar kepada SOP_09_009_MDV;  Periodical Review.';
apage[0] = apage[0] + '<br/><br/>Beberapa Fitur yang ada di dalam MEMO antara lain:';
apage[0] = apage[0] + '<br/>1. Meeting: Disini anda dapat membuat meeting dan melihat  Notulen meeting.';
apage[0] = apage[0] + '<br/>2. Tugas Saya: Anda dapat melihat tugas apa saja yang harus anda selesaikan, dan anda dapat meng up-date progress dari tugas tersebut.';
apage[0] = apage[0] + '<br/>3. Daftar Isu: Anda dapat memonitor progress dan status tindakan dari Isu-isu yang ada.';
apage[0] = apage[0] + '<br/>4. Ubah Password: Anda dapat mengubah password anda sesuai keinginan untuk keamanan anda.';
apage[0] = apage[0] + '<br/>5. Dan beberapa fitur lainnya yang dapat anda pelajari dengan lebih rinci dalam REMO User Manual.';
apage[0] = apage[0] + '<br/><br/>';

apage[1] = 'Sesuai SOP Periodical Review, aliran informasi sebagai berikut:';
apage[1] = apage[1] + '<br/>1. Weekly/Monthly Review Jobsite meeting. Isu dari jobsite akan dibahas di Internal Function di HO.';
apage[1] = apage[1] + '<br/>2. Dari internal function meeting (koordinasi) tersebut HO memberikan feedback kepada Jobsite mengenai solusi permasalahan.';
apage[1] = apage[1] + '<br/>3. Jika solusi belum bisa didapat, isu akan dibawa ke Weekly/Monthly Management Meeting, agar kemudian dapat diputuskan solusi yang tepat, dan kemudian diinformasikan kembali kepada Site.';
apage[1] = apage[1] + '<br/><br/>Perbedaan weekly meeting dan monthly meeting sesuai dengan  STD_09_001_MDV;Revisi 0.0 antara lain:';
apage[1] = apage[1] + '<br/>1. Weekly review membahas pencapaian KPI Jobsite  periode 1 minggu sebelumnya, dan perencanaan 1 minggu kedepan.';
apage[1] = apage[1] + '<br/>2. Monthly review membahas pencapaian KPI Jobsite  periode 1 minggu sebelumnya, dan 1 Bulan sebelumnya, dan perencanaan 1 bulan kedepan.<br/>';
apage[1] = apage[1] + '<br/><br/>';

apage[2] = 'REMO dirancang sebagai sistem kelola isu dari OPEN sampai CLOSED serta memonitor progress tindakannya.';
apage[2] = apage[2] + '<br/>Tahapannya yaitu:';
apage[2] = apage[2] + '<br/>1. Create : membuat Notulen meeting';
apage[2] = apage[2] + '<br/>2. Share : memberi informasi tindakan penyelesaian permasalahan kepada PIC yang terkait.';
apage[2] = apage[2] + '<br/>3. Up-date/Resolve: PIC menginformasikan progress tindakan yang dilakukan dalam penyelesaian permasalahan.';
apage[2] = apage[2] + '<br/>4. Monitor:  Memonitor progress tiap tindakan apakah on-schedule, overdue, tidak terselesaikan, berulang, dan lain-lain.';
apage[2] = apage[2] + '<br/><br/>Target hasil yang diharapkan adalah:';
apage[2] = apage[2] + '<br/>1. Meningkatkan efektivitas meeting.';
apage[2] = apage[2] + '<br/>2. Merecord setiap Notulen Meeting.';
apage[2] = apage[2] + '<br/>3. Memudahkan dalam melakukan reporting, up-date progress, dan penyampaian evidence nya.';
apage[2] = apage[2] + '<br/>4. Mengingatkan PIC dalam penyelesaian tugas.';
apage[2] = apage[2] + '<br/>5. Memudahkan pengontrolan progress tindakan.';
apage[2] = apage[2] + '<br/>6. Memberikan Informasi secara up to date kepada pihak terkait dan memungkinkan untuk memberikan feedback/komentar.';
apage[2] = apage[2] + '<br/>7. Memudahkan monitoring issue.';
apage[2] = apage[2] + '<br/><br/>';

apage[3] = '&#9679 Remo 1st version fokus kepada meeting-meeting yang ada dalam <b>SOP Periodical Review</b>. Meeting diluar itu terakomodasi dalam meeting others.';
apage[3] = apage[3] + '<br/>&#9679 Aplikasi compatible bila diakses menggunakan web browser <b>Internet Explorer 8.</b>';
apage[3] = apage[3] + '<br/>&#9679 Meeting yang sudah di kirim ke Email tidak bisa di edit kembali. Oleh karena itu, sebelum meeting ditutup pastikan agar mereview Notulen meeting kembali.';
apage[3] = apage[3] + '<br/>&#9679 Akses untuk personil di site adalah Section Head up.'
apage[3] = apage[3] + '<br/><br/>';

var ajudul = new Array(4);
ajudul[0] = 'Sekilas Seputar MEMO v1';
ajudul[1] = 'Peran MEMO dalam Penerapan SOP Periodical Review  & Siklus PDCA';
ajudul[2] = 'MEMO Sebagai Solusi Kelola ISU';
ajudul[3] = 'MEMO`s 1st version';

var agambar = new Array(4);
agambar[0] = '<img alt="" src="images/about_1b.PNG" style="width: 380px; height: 330px"/>';
agambar[1] = '<img alt="" src="images/about_2a.PNG" style="width: 380px; height: 330px"/>';
agambar[2] = '<img alt="" src="images/about_3a.PNG" style="width: 380px; height: 330px"/>';
agambar[3] = '<img alt="" src="images/about_4a.PNG" style="width: 380px; height: 330px"/>';

function initSlide() {
    var teks = '';
    teks = apage[0];
    var warp = '';
    warp = '<table width="100%"><tr>';
    warp = warp + '<td style="width:60%;font:13px" valign="top">' + teks + '</td>';
    warp = warp + '<td style="width:40%" valign="top"><center>' + agambar[0] + '</center></td>';
    warp = warp + '<tr></table>';
    document.getElementById("divslide").innerHTML = warp;
    document.getElementById("idjudul").innerHTML = ajudul[0];

    document.getElementById("divslide").style.padding = "3px";
    document.getElementById("divslide").style.height = "550px"
} 

function nextSlide() {
    var teks = '';
    if (pages < 4) {
        pages = pages + 1;
    }
    teks = apage[pages - 1];

    var warp = '';
    warp = '<table width="100%"><tr>';
    warp = warp + '<td style="width:60%;font:13px" valign="top">' + teks + '</td>';
    warp = warp + '<td style="width:40%" valign="top"><center>' + agambar[pages - 1] + '</center></td>';
    warp = warp + '<tr></table>';
    document.getElementById("divslide").innerHTML = warp;
    document.getElementById("idjudul").innerHTML = ajudul[pages - 1];
}

function prevSlide() {
    var teks = '';
    if (pages > 1) {
        pages = pages - 1;
    }
    teks = apage[pages - 1];

    var warp = '';
    warp = '<table width="100%"><tr>';
    warp = warp + '<td style="width:60%;font:13px" valign="top">' + teks + '</td>';
    warp = warp + '<td style="width:40%" valign="top"><center>' + agambar[pages - 1] + '</center></td>';
    warp = warp + '<tr></table>';
    document.getElementById("divslide").innerHTML = warp;
    document.getElementById("idjudul").innerHTML = ajudul[pages - 1];
}        