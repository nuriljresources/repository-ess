﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="emp_permit_list.aspx.vb" Inherits="EXCELLENT.emp_permit_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Employee Permit List
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
<caption style="text-align: center; font-size: 1.5em; color:White;">Employee Permit List</caption>
<tr>
    <td colspan=5><br /></td>
</tr>
<tr>
    <td style="width:5%;">Periode</td>
    <td style="width:5%;">
        <asp:DropDownList ID="dMonth" runat="server">
            <asp:ListItem Value="1">January</asp:ListItem>
            <asp:ListItem Value="2">February</asp:ListItem>
            <asp:ListItem Value="3">March</asp:ListItem>
            <asp:ListItem Value="4">April</asp:ListItem>
            <asp:ListItem Value="5">May</asp:ListItem>
            <asp:ListItem Value="6">June</asp:ListItem>
            <asp:ListItem Value="7">July</asp:ListItem>
            <asp:ListItem Value="8">August</asp:ListItem>
            <asp:ListItem Value="9">September</asp:ListItem>
            <asp:ListItem Value="10">October</asp:ListItem>
            <asp:ListItem Value="11">November</asp:ListItem>
            <asp:ListItem Value="12">December</asp:ListItem>
        </asp:DropDownList>
    </td>
    <td style="width:5%;">
        <asp:DropDownList ID="dYear" runat="server">
            <asp:ListItem Value="1">2015</asp:ListItem>
            <asp:ListItem Value="2">2016</asp:ListItem>
        </asp:DropDownList>
    </td>
    <td style="width:5%;"><asp:Button ID="btnSearch" runat="server" Text="Search"/></td>
    <td>
        <asp:TextBox ID="tripto" style="width:70px;" runat="server" ReadOnly="true" visible="false"></asp:TextBox>


<asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="transid" AllowPaging="true" PageSize="100" Width="1200px">
    <RowStyle Font-Size="Smaller" />
<Columns>

<asp:CommandField ShowSelectButton="True" SelectText="Select" ButtonType="Button" HeaderStyle-Width="40px" ItemStyle-VerticalAlign="top" >
    <HeaderStyle Width="40px"></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:CommandField>

<asp:BoundField DataField="transid" HeaderText="Trans ID" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle Width="150px"></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="kdsite" HeaderText="Site" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle Width="150px"></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>


<asp:BoundField DataField="Nama" HeaderText="Nama" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="TypePermit" HeaderText="Type" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="trans_date" HeaderText="Date From" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="trans_date2" HeaderText="Date To" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="totleave" HeaderText="Total Day" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="remarks" HeaderText="Remarks" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="fstatus" HeaderText="Status" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

</Columns> 
    <SelectedRowStyle BackColor="#99CCFF" />
    <HeaderStyle BackColor="#0099FF" />
</asp:GridView>

    </td>
</tr>
</table>

<br />


<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="transid" AllowPaging="true" PageSize="100" Width="1200px">
    <RowStyle Font-Size="Smaller" />
<Columns>

<asp:CommandField ShowSelectButton="True" SelectText="Select" ButtonType="Button" HeaderStyle-Width="40px" ItemStyle-VerticalAlign="top" >
    <HeaderStyle Width="40px"></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:CommandField>

<asp:BoundField DataField="transid" HeaderText="Trans ID" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle Width="150px"></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="kdsite" HeaderText="Site" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle Width="150px"></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>


<asp:BoundField DataField="Nama" HeaderText="Nama" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="TypePermit" HeaderText="Type" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="trans_date" HeaderText="Date From" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="trans_date2" HeaderText="Date To" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="totleave" HeaderText="Total Day" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="remarks" HeaderText="Remarks" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:BoundField DataField="fstatus" HeaderText="Status" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
    <HeaderStyle></HeaderStyle>
    <ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>

<asp:HyperLinkField DataNavigateUrlFields="transid" ControlStyle-CssClass="PopUpViewHistory"
                DataNavigateUrlFormatString="emp_permit_history.aspx?no={0}" 
                Text="View History" />

</Columns> 
    <SelectedRowStyle BackColor="#99CCFF" />
    <HeaderStyle BackColor="#0099FF" />
</asp:GridView>


<script type="text/javascript">

    $('a.PopUpViewHistory').click(function() {


    newwindow = window.open($(this).attr('href'), "History Work Flow", 'height=300,width=800');
        if (window.focus) { newwindow.focus() }
        return false
        ;
    });
    
</script>

</asp:Content>


