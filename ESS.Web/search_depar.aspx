﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="search_depar.aspx.vb" Inherits="EXCELLENT.search_depar" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <link href="css/site.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript">
        function UseSelected(kddepar, nmdepar) {
          var pw = window.opener;
          var inputFrm = pw.document.forms['aspnetForm'];
          if (inputFrm.elements['ctrlToFind'].value == 'spldelgmdepar') {
              inputFrm.elements['ctl00_ContentPlaceHolder1_txtdepar'].value = nmdepar;
              inputFrm.elements['ctl00_ContentPlaceHolder1_hkddepar'].value = kddepar;
          }
          window.close();
       }
       function Left(str, n) {
          if (n <= 0)
             return "";
          else if (n > String(str).length)
             return str;
          else
             return String(str).substring(0, n);
       }

       function Right(str, n) {
          if (n <= 0)
             return "";
          else if (n > String(str).length)
             return str;
          else {
             var iLen = String(str).length;
             return String(str).substring(iLen, iLen - n);
          }
      }

      function keyPressed(e) {
         switch (e.keyCode) {
            case 13:
               {
                  document.getElementById("btnSearch").click();
                  break;
               }
             default:
               {
                  break;
               }
         }
     }

     function cekArgumen() {
        var d = document;
        if ((d.getElementById("SearchKey").value.length < 3) || (d.getElementById("SearchKey").value.length == 0)){
            alert("Please Entry Specific NIK / Name (minimal 3 Character)");
            return false;
        }
     }
    </script>
</head>
<body>
  <form id="form1" runat="server">
  <asp:HiddenField ID="hlvl" runat="server" />  
  <ajaxToolkit:ToolkitScriptManager  ID="ToolkitScriptManager1" runat="server"/>
   <asp:UpdatePanel id="MyUpdatePanel" runat="server">
    <ContentTemplate>
      <div>           

      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
         <caption style="text-align:center">Select Department</caption>
         <tr>
            <td style="padding: 5px; width:20%">NIK / Name</td>
            <td style="width:15%"><asp:TextBox ID="SearchKey" runat="server" class="tb10" style="visibility:hidden;" onkeydown="keyPressed(event);"/></td>
            <td><input type="text" id="hide" style="display:none;"/><asp:Button ID="btnSearch" 
                    runat="server" Text="Cari Nama" Font-Bold="False" ForeColor="#0033CC" 
                    OnClientClick="cekArgumen()" Height="26px" Width="90px" style="visibility:hidden;"/></td>
         </tr>
         <tr>
         <td>
         </td>
         </tr>
         <tr>
         <td colspan="3">
         <asp:Repeater ID="rptPages" Runat="server">
      <HeaderTemplate>
      <table cellpadding="0" cellspacing="0" border="0">
      <tr>
         <td style="vertical-align:top"><b>Page:</b>&nbsp;</td>
         <td>
      </HeaderTemplate>
      <ItemTemplate>
                        <asp:LinkButton ID="btnPage" CommandName="Page" 
                         CommandArgument="<%# Container.DataItem %>" 
                         CssClass="text" 
                         Runat="server" 
                         Visible='<%# (CInt(Container.DataItem) <> (PageNumber+1)).ToString() %>'>
                         <%#Container.DataItem%> 
                         </asp:LinkButton>
                         <asp:Label ID="lblPage" runat="server" Visible='<%# (CInt(Container.DataItem) = (PageNumber+1)).ToString()%>' 
                         Text="<%#Container.DataItem%>" 
                         ForeColor="black" Font-Bold="true">
                         </asp:Label>
      </ItemTemplate>
      <FooterTemplate>
         </td>
      </tr>
      </td>
      </table>
      </FooterTemplate>
      </asp:Repeater>
         </tr>
         <tr>
            <td colspan="3">
               <asp:Repeater ID="rptResult" runat="server">
               <HeaderTemplate>
                           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table-inside">
                           <tr>
                                <th>No.</th>               
                                <th>KdDepartement</th>
                                <th>Department</th>
                           </tr>
                        </HeaderTemplate>  
                        <ItemTemplate>
                       <tr>
                        <td><%# Container.ItemIndex + 1 %></td>
                        <td><a href="#" onclick="UseSelected('<%#DataBinder.Eval(Container.DataItem, "kddepar")%>','<%#DataBinder.Eval(Container.DataItem, "nmdepar")%>');"><%#DataBinder.Eval(Container.DataItem, "kddepar")%></a></td>
                        <td><%#DataBinder.Eval(Container.DataItem, "nmdepar")%></td>
                        </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                           </table>
                        </FooterTemplate>
               </asp:Repeater>             
            </td>
         </tr>
      
      </table>  
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
     <asp:UpdateProgress ID="progress1" runat="server" DisplayAfter="50">    
                  <ProgressTemplate>
                  <asp:Panel ID="pnlProgressBar" runat="server">    
                     <div><asp:Image ID="Image1" runat="Server" ImageUrl="images/ajax-loader.gif"/>Please 
                         Wait...</div>    
                  </asp:Panel>   
                  </ProgressTemplate> 
                  </asp:UpdateProgress>
    </form>
 
</body>
</html>
