﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="cetakoutpatient.aspx.vb" Inherits="EXCELLENT.cetakoutpatient" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Print Outpatient
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="598px" Width="1148px">
            <LocalReport ReportPath="Laporan\cetak_outpatient.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                        Name="medic_DataTable3" />
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" 
                        Name="medic_DataTable1" />
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource3" 
                        Name="medic_DataTable4" />
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource4" 
                        Name="medic_DataTable5" />
                </DataSources>
            </LocalReport>
        
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="ObjectDataSource4" runat="server" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            TypeName="EXCELLENT.medicTableAdapters.DataTable5TableAdapter">
            <SelectParameters>
                <asp:SessionParameter Name="noreg" SessionField="printinoutpatient" 
                    Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            TypeName="EXCELLENT.medicTableAdapters.DataTable4TableAdapter">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="0" Name="noreg" 
                    SessionField="printinoutpatient" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            TypeName="EXCELLENT.medicTableAdapters.DataTable1TableAdapter">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="tipe" Type="String" />
                <asp:SessionParameter DefaultValue="0" Name="noreg" 
                    SessionField="printinoutpatient" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            TypeName="EXCELLENT.medicTableAdapters.DataTable3TableAdapter">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="0" Name="noreg" 
                    SessionField="printinoutpatient" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
</asp:Content>

