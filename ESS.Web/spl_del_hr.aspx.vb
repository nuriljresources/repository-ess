﻿Imports System.Data.SqlClient
Imports System.Globalization

Partial Public Class spl_del_hr
    Inherits System.Web.UI.Page
    Public tglaw As String
    Public th As String
    Public v1 As String
    Public v2 As String
    Public imonth As Integer
    Public js As String
    Public msg As String
    Public msg2 As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

        If Session("niksite") = Nothing Or Session("niksite") = "" Then Response.Redirect("login.aspx")

        If Not IsPostBack Then
            btnsel.Visible = False
            btndsel.Visible = False
            btndelete.Visible = False
            btnchk.Visible = False
            btnSearch.Visible = False
            ddlbulan.Visible = False
            ddltahun.Visible = False
            btnnew.Visible = False
        End If
        
        Try
            If Not IsPostBack Then
                Dim a As String

                tglaw = Request(ddlbulan.UniqueID)
                th = Request(ddltahun.UniqueID)
                Dim i As Integer

                sqlConn.Open()
                Dim strcon As String = "select min(Tanggal) as min_date ,max(Tanggal) as max_date from H_H110"
                Dim dtb As DataTable = New DataTable
                Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
                sda.Fill(dtb)

                v1 = dtb.Rows(0)!min_date.ToString
                If v1.ToString = "" Then
                    v1 = Today.Year.ToString
                Else
                    v1 = CDate(v1).ToString("yyyy")
                End If

                v2 = dtb.Rows(0)!max_date.ToString
                If v2.ToString = "" Then
                    v2 = Today.Year.ToString
                Else
                    v2 = CDate(v2).ToString("yyyy")
                End If

                ddltahun.Items.Clear()
                For i = v1 To v2
                    ddltahun.Items.Add(i)
                Next

                ddltahun.SelectedValue = (i - 1).ToString

                Select Case tglaw
                    Case "January"
                        imonth = 1
                    Case "February"
                        imonth = 2
                    Case "March"
                        imonth = 3
                    Case "April"
                        imonth = 4
                    Case "May"
                        imonth = 5
                    Case "June"
                        imonth = 6
                    Case "July"
                        imonth = 7
                    Case "August"
                        imonth = 8
                    Case "September"
                        imonth = 9
                    Case "October"
                        imonth = 10
                    Case "November"
                        imonth = 11
                    Case "December"
                        imonth = 12
                    Case Else
                        imonth = 1
                End Select

                ddlbulan.SelectedValue = Today.Month

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim sqlinsert As String
        Dim cmdinsert As SqlCommand
        Dim ss As String

        If txtnikreq.Text = "" Or txtnamareq.Text = "" Or txtnikreq2.Text = "" Or txtnamareq2.Text = "" Or txtdtfrom.Text = "" Or Txtdtto.Text = "" Then
            msg = "<script type='text/javascript'> alert('Please complete your data'); </script>"
            Return
        End If
        Dim tgl1 As String
        Dim tgl2 As String
        btnsave.Visible = False

        tgl1 = txtdtfrom.Text
        tgl1 = CDate(tgl1).ToString("yyyy-MM-dd")
        tgl2 = Txtdtto.Text
        tgl2 = CDate(tgl2).ToString("yyyy-MM-dd")
        Try
            sqlConn.Open()

            Dim strcon2 As String = "select recordid,(select niksite from h_a101 where nik = h_h110.nik) as nik, (select nama from h_a101 where nik = h_h110.nik ) as nama from H_H110 where (app1 = '" + hnikreq.Value + "' or app2 = '" + hnikreq.Value + "') and jam_start between '" + tgl1 + " 00:00:00.000" + "' and '" + tgl2 + " 23:59:59.000" + "'"
            Dim dtbchkapp As DataTable = New DataTable
            Dim sdachkapp As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn)
            sdachkapp.Fill(dtbchkapp)

            If dtbchkapp.Rows.Count > 0 Then
                gridview2.DataSource = dtbchkapp
                gridview2.DataBind()
                btnsave.Visible = False
                btnchk.Visible = True
                btnsel.Visible = True
                btndsel.Visible = True
                msg2 = "The following list will approved by <b>" + txtnamareq2.Text + "</b>, please click the check box on the right"
            Else
                If hid.Value = "" Then
                    sqlinsert = "insert into H_H11002 (nikdel, date_from, date_to, nikreq, stedit) values ('" + hnikreq.Value + "', '" + txtdtfrom.Text + "', '" + Txtdtto.Text + "', '" + Session("niksite").ToString + "', '0')"
                    cmdinsert = New SqlCommand(sqlinsert, sqlConn)
                    cmdinsert.ExecuteScalar()
                    'recordid.Value = transno

                    msg = "<script type='text/javascript'> alert('Data has been save'); </script>"
                Else
                    'If hnikreq.Value = "" Then
                    'Else
                    '    sqlinsert = "update H_H11002 set nikdel = '" + hnikreq.Value + "', date_from = '" + txtdtfrom.Text + "', date_to = '" + Txtdtto.Text + "', nikreq = '" + Session("niksite").ToString + "' where delegation_id = '" + hid.Value + "' "
                    '    cmdinsert = New SqlCommand(sqlinsert, sqlConn)
                    '    cmdinsert.ExecuteScalar()
                    '    'recordid.Value = transno
                    'End If

                    msg = "<script type='text/javascript'> alert('Update Failed'); </script>"

                End If
            End If

            btnnew.Visible = True

            'GridView1.DataBind()
        Catch ex As Exception

            ss = ex.Message.Replace("'", "")
            ss = ss.Replace(Environment.NewLine, "")
            msg = "<script type='text/javascript'> alert('Save Failed : " + ss + "'); </script>"

        Finally
            sqlConn.Close()
        End Try
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        
        sqlConn.Open()
        Try
            Dim strcon As String = "select delegation_id, (select nama from H_A101 where nik = H_H11002.nikdel) as nama, convert(varchar(20),date_from, 113) as date_from,  convert(varchar(20),date_to,113) as date_to, (select niksite from H_A101 where nik = H_H11002.nikdel) as niksite from H_H11002 where (month(date_from) = " + ddlbulan.SelectedValue + " or month(date_to) = " + ddlbulan.SelectedValue + ") and nikreq = '" + Session("niksite").ToString + "' and stedit <> 2"
            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            GridView1.DataSource = dtb
            GridView1.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        If GridView1.SelectedRow Is Nothing Then Exit Sub

        Dim row As GridViewRow = GridView1.SelectedRow

        'Session("splrecordid") = GridView1.DataKeys(row.RowIndex).Value.ToString
        txtnikreq.Text = row.Cells(1).Text
        txtnamareq.Text = row.Cells(2).Text
        txtdtfrom.Text = Format(CDate(row.Cells(3).Text), "MM/dd/yyyy")
        Txtdtto.Text = Format(CDate(row.Cells(4).Text), "MM/dd/yyyy")
        hid.Value = GridView1.DataKeys(row.RowIndex).Value.ToString
    End Sub

    Private Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim sqlinsert As String
        Dim cmdinsert As SqlCommand
        Try
            sqlinsert = "update H_H11002 set stedit = '2' where delegation_id = '" + hid.Value + "' "
            cmdinsert = New SqlCommand(sqlinsert, sqlConn)
            cmdinsert.ExecuteScalar()

            msg = "<script type='text/javascript'> alert('Data has been deleted'); </script>"
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnchk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnchk.Click
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim sqlinsert, sqlupdate As String
        Dim cmdinsert As SqlCommand
        Dim cmdupdate As SqlCommand
        Dim licount As Integer
        Dim lschkvalue, lsnoreg As String
        Dim ss As String
        licount = 0
        btnchk.Visible = False
        'Dim tgl1 As String

        'tgl1 = txtdtfrom.Text
        'tgl1 = CDate(tgl1).ToString("yyyy-MM-dd")

        Try
            sqlConn.Open()

            'If dtbchkapp.Rows.Count > 0 Then
            '    gridview2.DataSource = dtbchkapp
            '    gridview2.DataBind()
            'msg2 = "The following list will approved by <b>" + txtnamareq2.Text + "</b>, please click the check box on the right"
            sqlinsert = "insert into H_H11002 (nikdel, date_from, date_to, nikreq, stedit) values ('" + hnikreq.Value + "', '" + txtdtfrom.Text + "', '" + Txtdtto.Text + "', '" + Session("niksite").ToString + "', '0')"
            cmdinsert = New SqlCommand(sqlinsert, sqlConn)
            cmdinsert.ExecuteScalar()

            For Each row As GridViewRow In gridview2.Rows

                Dim chk As CheckBox = DirectCast(gridview2.Rows(licount).Cells(2).FindControl("chkboxss"), CheckBox)
                lsnoreg = gridview2.DataKeys(licount).Value.ToString
                licount = licount + 1

                lschkvalue = chk.Checked.ToString

                Dim strcon2 As String = "select app1,app2 from H_H110 where recordid = '" + lsnoreg + "'"
                Dim dtbapp As DataTable = New DataTable
                Dim sdaapp As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn)
                sdaapp.Fill(dtbapp)

                If dtbapp.Rows.Count > 0 Then
                    If lschkvalue = True Then
                        If dtbapp.Rows(0)!app1.ToString = hnikreq.Value Then
                            sqlupdate = "update H_H110 set app1 = '" + hnikreq2.Value + "', checkapp1 = '" + hnikreq2.Value + "' where recordid = '" + lsnoreg + "' "
                            cmdupdate = New SqlCommand(sqlupdate, sqlConn)
                            cmdupdate.ExecuteScalar()
                        End If

                        If dtbapp.Rows(0)!app2.ToString = hnikreq.Value Then
                            sqlupdate = "update H_H110 set app2 = '" + hnikreq2.Value + "', checkapp2 = '" + hnikreq2.Value + "' where recordid = '" + lsnoreg + "' "
                            cmdupdate = New SqlCommand(sqlupdate, sqlConn)
                            cmdupdate.ExecuteScalar()
                        End If
                        ' ...
                        'If chk IsNot Nothing AndAlso chk.Checked Then
                        'End If
                    End If
                End If
            Next
            msg = "<script type='text/javascript'> alert('Data has been Save'); </script>"
            gridview2.DataSource = ""
            gridview2.DataBind()
            btnnew.Visible = True
            btnsel.Visible = False
            btndsel.Visible = False
            'btnsel.Visible = True
            'btndsel.Visible = True
            'End If
        Catch ex As Exception
            btnchk.Visible = True
            ss = ex.Message.Replace("'", "")
            ss = ss.Replace(Environment.NewLine, "")
            msg = "<script type='text/javascript'> alert('Save Failed : " + ss + "'); </script>"
        Finally
            sqlConn.Close()
        End Try

    End Sub

    Private Sub btnsel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsel.Click
        Dim licount As Integer
        msg2 = "The following list will approved by <b>" + txtnamareq2.Text + "</b>, please click the check box on the right"
        For Each row As GridViewRow In gridview2.Rows
            Dim chk As CheckBox = DirectCast(gridview2.Rows(licount).Cells(2).FindControl("chkboxss"), CheckBox)
            licount = licount + 1
            chk.Checked = True
        Next
    End Sub

    Private Sub btndsel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndsel.Click
        Dim licount As Integer
        msg2 = "The following list will approved by <b>" + txtnamareq2.Text + "</b>, please click the check box on the right"
        For Each row As GridViewRow In gridview2.Rows
            Dim chk As CheckBox = DirectCast(gridview2.Rows(licount).Cells(2).FindControl("chkboxss"), CheckBox)
            licount = licount + 1
            chk.Checked = False
        Next
    End Sub

    Private Sub btnnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Response.Redirect("spl_del_hr.aspx")
    End Sub
End Class