﻿Imports ESS.DataAccess
Imports System.Linq
Imports ESS.DataAccess.ESSData
Imports ESS.DataAccess.ESSDataTableAdapters
Imports System.Data.SqlClient
Imports ESS.Common


Public Class PermitLogic

    Public Shared Function GetPermitByTransId(ByVal transid As String) As PermitModel
        Dim model As PermitModel = New PermitModel()

        Dim PermitAdapter As PermitTableAdapter = New PermitTableAdapter
        Dim Permit As PermitDataTable = PermitAdapter.GetPermitByTransId(transid)
        If Permit.Count > 0 Then
            model.TransID = Permit(0).transid
            model.EmployeeNIK = Permit(0).nik
            model.EmployeeNIKSITE = Permit(0).NIKSITE
            model.EmployeeName = Permit(0).Nama
            model.TypePermit = Permit(0).LType
            model.DatePermit = Permit(0).trans_date
            Dim timePermit As TimeSpan = TimeSpan.MinValue

            If Not Permit(0).trans_date2 = DateTime.MinValue Then
                timePermit = New TimeSpan(Permit(0).trans_date2.Ticks)
            End If

            model.TimePermit = timePermit
            model.Remarks = Permit(0).remarks
            model.StatusEdit = Permit(0).StEdit
        End If

        Return model
    End Function

    Public Shared Function PermitInsert(ByVal Model As PermitModel) As ResponseModel
        Dim response As ResponseModel = New ResponseModel
        Try
            Dim TimePermit As DateTime = Model.DatePermit.AddHours(Model.TimePermit.Hours).AddMinutes(Model.TimePermit.Minutes)
            Dim sqlQuery As String
            sqlQuery = <![CDATA[INSERT INTO [dbo].[L_H006]
                    ([transid], [nik], [LType], [trans_date], [trans_date2], [totleave], [remarks], [fstatus], [CreatedBy], [CreatedIn], [CreatedTime], [StEdit], [kdsite])
                    VALUES 
                    ('{0}', '{1}', '{2}', '{3}', '{4}', {5}, '{6}', {7}, '{8}', '{9}', '{10}', {11}, '{12}')
                    ]]>.Value

            Dim DocNumber As String = GetDocumentNumber()
            sqlQuery = String.Format(sqlQuery, DocNumber, Model.EmployeeNIK, Model.TypePermit, Model.DatePermit.ToString(), TimePermit.ToString(), 1, Model.Remarks, 0, Model.EmployeeNIKSITE, System.Net.Dns.GetHostName(), DateTime.Now.ToString(), 0, Model.Site)
            Model.TransID = DocNumber
            response.SetSuccess("Permit Success")
        Catch ex As Exception
            response.SetError(ex.Message)
        End Try


        Return response
    End Function

    Public Shared Function PermitUpdate(ByVal Model As PermitModel) As ResponseModel
        Dim response As ResponseModel = New ResponseModel


        Return response
    End Function

    Public Shared Function GetDocumentNumber() As String
        Dim DocumentNumber As String = String.Empty
        Dim StripNo As String = String.Format("{0}{1}/S/", DateTime.Now.Year, DateTime.Now.ToString("MM"))
        Dim RunningNumber As Integer = 0

        Dim conn As SqlConnection = New SqlConnection(SITE_SETTINGS.JRN_CITRIXCON)
        Dim dt As New DataTable

        conn.Open()
        Dim strQueryTotal As String = "select MAX(right(transid,6)) from l_h006 where transid like '" + StripNo + "%'"

        Dim dtTotal As DataTable = New DataTable
        Dim sqlTotalAdapter As SqlDataAdapter = New SqlDataAdapter(strQueryTotal, conn)
        sqlTotalAdapter.Fill(dtTotal)
        conn.Close()

        If (dtTotal.Rows.Count > 0 And Not String.IsNullOrEmpty(dtTotal.Rows(0).Item(0).ToString())) Then
            RunningNumber = Integer.Parse(dtTotal.Rows(0).Item(0))
        End If

        DocumentNumber = StripNo + (RunningNumber + 1).ToString("D6")

        Return DocumentNumber
    End Function

End Class
