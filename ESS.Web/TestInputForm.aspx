﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="TestInputForm.aspx.vb" Inherits="EXCELLENT.TestInputForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
Inpatient Medical Claim
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
<style>
    #body-form 
    {
    	padding : 10px;
    }
    
    #body-form h2 
    {
    	padding : 5px;
    	color : #fff;
    	background : #0066FF;
    	text-align : center;
    }
    
    #body-form-content 
    {
    	
    }
    
    #body-form-user tbody tr td
    {
    	height : 25px;
    }
    
    .txtRemark 
    {
    	width : 100%;
    }
    


    table.form-input a:link {
	    color: #666;
	    font-weight: bold;
	    text-decoration:none;
    }
    table.form-input a:visited {
	    color: #999999;
	    font-weight:bold;
	    text-decoration:none;
    }
    table.form-input a:active,
    table.form-input a:hover {
	    color: #bd5a35;
	    text-decoration:underline;
    }
    table.form-input {
	    font-family:Arial, Helvetica, sans-serif;
	    width : 100%;
	    color:#666;
	    font-size:12px;
	    text-shadow: 1px 1px 0px #fff;
	    background:#eaebec;
	    border:#ccc 1px solid;
	    margin-top : 20px;
	    border-spacing: 0;
    }
    table.form-input thead tr th {
	    padding: 5px;
	    border-top:1px solid #fafafa;
	    border-bottom:1px solid #e0e0e0;

	    background: #ededed;
    }
    table.form-input thead tr th:first-child {
	    text-align: left;
	    padding-left:20px;
    }

    table.form-input tbody tr {
	    text-align: center;
	    padding-left:20px;
    }
    table.form-input tbody td:first-child {
	    text-align: left;
	    border-left: 0;
    }
    table.form-input tbody td {
	    padding:5px;
	    border-top: 1px solid #ffffff;
	    border-bottom:1px solid #e0e0e0;
	    border-left: 1px solid #e0e0e0;

	    background: #fafafa;
	    width : 50px;
    }
    table.form-input tbody tr.even td {
	    background: #f6f6f6;
    }
    table.form-input tbody tr:last-child td {
	    border-bottom:0;
    }
    
    table.form-input tbody tr td input {
	    width : 100px;	
	    display : inline;
    }
    
    table.form-input tbody tr td input.datepicker {
	    width : 80px;	
	    display : inline;
	    margin-right : 5px;
    }
    
    table.form-input tbody tr td img {
	    display : inline;
    }
    
    .btn-action 
    {
    	padding : 5px 10px;
    }

</style>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="body-form">
    <h2>INPATIENT MEDICAL CLAIM</h2>
    <div id="body-form-content">
        <table id="body-form-user">
            <tbody>
                <tr>
                    <td style="width : 200px;">Trans No</td>
                    <td><asp:TextBox ID="txtTransNo" runat="server"></asp:TextBox></td>
                    <td style="width : 150px;"></td>
                    <td style="width : 200px;">Date</td>
                    <td><asp:TextBox ID="txtBank" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Site</td>
                    <td><asp:TextBox ID="txtCompany" runat="server"></asp:TextBox></td>
                    <td></td>
                    <td>costcode</td>
                    <td><asp:TextBox ID="txtAddress" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Nik </td>
                    <td><asp:TextBox ID="txtDivision" runat="server"></asp:TextBox></td>
                    <td></td>
                    <td>Name</td>
                    <td><asp:TextBox ID="txtSite" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Position</td>
                    <td><asp:TextBox ID="txtLocation" runat="server"></asp:TextBox></td>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td>Remark</td>
                    <td colspan="3"><asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Rows="3" CssClass="txtRemark"></asp:TextBox></td>
                </tr>
            </tbody>
        </table>
        
        
        <table class="form-input">
            <asp:HiddenField runat="server" ID="hiddenForm" />
            <thead>
                <tr>
                    <th style="width: 110px;">Date In</th>
                    <th style="width: 110px;">Date Out</th>
                    <th>Days</th>
                    <th>Name</th>
                    <th>Room/day (IDR)</th>
                    <th>Consultation (IDR)</th>
                    <th>Medicine (IDR)</th>
                    <th>Lab (IDR)</th>
                    <th>Other (IDR)</th>
                    <th>Total (IDR)</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="text" class="dateIn datepicker" /></td>
                    <td><input type="text" class="dateOut datepicker" /></td>
                    <td><input type="text" class="days" /></td>
                    <td><input type="text" class="name" /></td>
                    <td><input type="text" class="roomDay" /></td>
                    <td><input type="text" class="consultation" /></td>
                    <td><input type="text" class="medicine" /></td>
                    <td><input type="text" class="lab" /></td>
                    <td><input type="text" class="other" /></td>
                    <td><input type="text" class="total" /></td>
                    <td>Action</td>
                </tr>
                <tr>
                    <td><input type="text" class="dateIn datepicker" /></td>
                    <td><input type="text" class="dateOut datepicker" /></td>
                    <td><input type="text" class="days" /></td>
                    <td><input type="text" class="name" /></td>
                    <td><input type="text" class="roomDay" /></td>
                    <td><input type="text" class="consultation" /></td>
                    <td><input type="text" class="medicine" /></td>
                    <td><input type="text" class="lab" /></td>
                    <td><input type="text" class="other" /></td>
                    <td><input type="text" class="total" /></td>
                    <td>Action</td>
                </tr>
            </tbody>
        </table>
        
        <br />
        <button type="button" class="btn-action" name="submit-form">Submit</button>
        <button type="button" class="btn-action" name="submit-print">Print</button>
    </div>
    
</div>



  <script>
  $( function() {
    $( ".datepicker" ).datepicker({
      showOn: "button",
      buttonImage: "http://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
      buttonImageOnly: true,
      buttonText: "Select date"
    });
  } );
  </script>
</asp:Content>
