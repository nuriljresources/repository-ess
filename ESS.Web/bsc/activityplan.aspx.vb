﻿Imports System.Data.SqlClient

Partial Public Class activityplan
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            If Session("permission") = "" Or Session("permission") = Nothing Then
                Response.Redirect("../Login.aspx", False)
                Exit Sub
            ElseIf ("1;2;7;8;B").IndexOf(Session("permission")) = -1 Then 'yang bisa akses cuman mdv,sekpro, S head, PIC PDCA
                Response.Redirect("../default.aspx", False)
                Exit Sub
            End If
            For Each x As ListItem In ddlYear.Items
                If x.Text = Date.Now.Year Then
                    x.Selected = True
                End If
            Next
            Grup.Value = Session("Permission")
            Section.Value = Session("bscdepar")
            Divisi.Value = Session("bscdivisi")
            Jobsite.Value = Session("jobsite")
            userid.Value = Session("NikUser")
            userip.Value = HttpContext.Current.Request.UserHostAddress

            If Not Page.IsPostBack Then
                Dim cmd As SqlCommand
                Dim dr As SqlDataReader = Nothing
                Dim sqlQuery As String = ""
                'Initialize Function
                sqlQuery = "select ID,FunctionName,active from MFunction where active='1' order by ID"
                cmd = New SqlCommand(sqlQuery, conn)
                conn.Open()
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "3" Then 'GRUP BOD dan MSD
                        KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "8" Or Session("permission") = "1" Or Session("permission") = "B" Or Session("permission") = "4" Or Session("permission") = "5" Then 'PIC PDCA dan GM dan Manager
                        If Session("bscdivisi") = dr(0) Then
                            KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Section
                sqlQuery = "select ID,SectionName,active,funcid from MSection order by ID"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Or Session("permission") = "3" Or Session("permission") = "6" Then 'GRUP MDV atau sekpro atau BOD
                        KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "7" Or Session("permission") = "2" Or Session("permission") = "A" Then 'GRUP S HEAD & S HEAD ADMIN
                        If Session("bscdepar") = dr(0) Then
                            KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    ElseIf Session("permission") = "8" Or Session("permission") = "4" Or Session("permission") = "5" Then 'PIC PDCA dan GM dan Manager
                        If Session("bscdivisi") = dr(3) Then
                            KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Jobsite
                sqlQuery = "select id,kdsite,active from site order by id"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Or Session("permission") = "8" Or Session("permission") = "3" Or Session("permission") = "4" Or Session("permission") = "5" Then 'GRUP MDV & PIC PDCA & BOD & GM & MANAGER
                        KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "7" Or Session("permission") = "2" Or Session("permission") = "6" Or Session("permission") = "A" Then 'GRUP S HEAD & SEKPRO & PM & S HEAD ADMIN
                        If Session("jobsite") = dr(1) Then
                            KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                    'If dr(2) = 0 Then
                    '    KPIJobsite.Items(KPIJobsite.Items.Count - 1).Enabled = False
                    'End If
                End While
                dr.Close()
                dr = Nothing
                conn.Close()
                'Isi KPI Level
                KPILevel.Items.Clear()
                Select Case Session("permission")
                    Case "1" 'MSD
                        KPILevel.Items.Add(New ListItem("Function", "1"))
                    Case "B" 'MSD Func. Head
                        KPILevel.Items.Add(New ListItem("Function", "1"))
                    Case "2" 'Sek Pro
                        KPILevel.Items.Add(New ListItem("Section", "2"))
                    Case "7" 'S.Head
                        KPILevel.Items.Add(New ListItem("Section", "2"))
                    Case "8" 'PIC PDCA
                        KPILevel.Items.Add(New ListItem("Function", "1"))
                End Select
                'Selected Index Change
                Select Case KPILevel.SelectedValue
                    Case 0
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                    Case 1
                        KPIFunc.Visible = True
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                    Case 2
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = True
                        KPISection.Visible = True
                    Case 3
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = True
                        KPISection.Visible = False
                End Select
            End If

        Catch ex As Exception
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Private Sub KPILevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles KPILevel.SelectedIndexChanged
        Try
            Select Case KPILevel.SelectedValue
                Case 0
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                Case 1
                    KPIFunc.Visible = True
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                Case 2
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = True
                Case 3
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = False
            End Select
        Catch ex As Exception
        End Try
    End Sub
End Class