﻿Imports System.Data.SqlClient

Partial Public Class emp_permit_history
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.fillgrid()
        End If
    End Sub


    Private Sub fillgrid()

        Dim conn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
        conn2.Open()
        Dim dataTable As DataTable = New DataTable()

        Dim sqlQuery As String = <![CDATA[SELECT 
        --A.transid AS 'LeaveNo',            
        A.datex AS 'Date',            
        CASE WHEN A.StatusWF = 'COMPLT' THEN 'Complete' 
	        WHEN A.StatusWF = 'RVS' THEN 'Revise' 
	        WHEN A.StatusWF = 'INP' THEN 'In Progress'                
	        WHEN A.StatusWF = 'INT' THEN 'Initial'            
        END AS 'Status Work Flow',            
        B.Nik AS 'PIC User NIK',            
        B.Nama AS 'PIC User Name',            
        CASE WHEN A.picUserRole = 'DEL' THEN 'Delegator' 
	        WHEN A.picUserRole = 'MGR' THEN 'Manager'                
	        WHEN A.picUserRole = 'HRD' THEN 'In HRD'            
        END AS 'PIC User Role',            
        CASE WHEN A.picAction = '0' THEN '-' 
        WHEN A.picAction = '1' THEN 'Approved'
        WHEN A.picAction = '2' THEN 'Reject'            
        END AS 'PIC ACTION',            
        A.remarks AS 'Remarks'        
        FROM L_H006_LOGWF A INNER JOIN H_A101 B ON A.picUser = B.Nik        
        WHERE A.transid = '{0}' ORDER BY A.datex ASC]]>.Value

        sqlQuery = String.Format(sqlQuery, Request.QueryString.Item("no"))
        Dim sda2 As SqlDataAdapter = New SqlDataAdapter(sqlQuery, conn2)
        sda2.Fill(dataTable)
        Me.GridView1.DataSource = dataTable
        Me.GridView1.DataBind()
    End Sub



End Class