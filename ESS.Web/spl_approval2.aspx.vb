﻿Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports System.IO
Imports System.Net.Mail
Partial Public Class spl_approval2
    Inherits System.Web.UI.Page
    Public tglaw As String
    Public th As String
    Public v1 As String
    Public v2 As String
    Public imonth As Integer
    Public js As String
    Public msg As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("niksite") = Nothing Or Session("niksite") = "" Then Response.Redirect("login.aspx")
        Dim baris As Integer

        If Not IsPostBack Then
            Dim a As String

            tglaw = Request(ddlbulan.UniqueID)
            th = Request(ddltahun.UniqueID)
            Dim i As Integer

            Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

            sqlConn.Open()
            Dim strcon As String = "select min(Tanggal) as min_date ,max(Tanggal) as max_date from H_H110"
            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            v1 = dtb.Rows(0)!min_date.ToString
            If v1.ToString = "" Then
                v1 = Today.Year.ToString
            Else
                v1 = CDate(v1).ToString("yyyy")
            End If

            v2 = dtb.Rows(0)!max_date.ToString
            If v2.ToString = "" Then
                v2 = Today.Year.ToString
            Else
                v2 = CDate(v2).ToString("yyyy")
            End If

            ddltahun.Items.Clear()
            For i = v1 To v2
                ddltahun.Items.Add(i)
            Next

            ddltahun.SelectedValue = (i - 1).ToString

            Select Case tglaw
                Case "January"
                    imonth = 1
                Case "February"
                    imonth = 2
                Case "March"
                    imonth = 3
                Case "April"
                    imonth = 4
                Case "May"
                    imonth = 5
                Case "June"
                    imonth = 6
                Case "July"
                    imonth = 7
                Case "August"
                    imonth = 8
                Case "September"
                    imonth = 9
                Case "October"
                    imonth = 10
                Case "November"
                    imonth = 11
                Case "December"
                    imonth = 12
                Case Else
                    imonth = 1
            End Select

            Try

                Dim dtb2 As DataTable = New DataTable()

                Dim strcon2 As String
                'strcon2 = strcon2 + "where nik = '" + Session("niksite") + "' and StEdit <> 2 and month(TglTrans) = " + Today.Month.ToString + " and YEAR(TglTrans) = " + Today.Year.ToString + ""
                If Session("kdsite").ToString = "LAN" Or Session("kdsite").ToString = "BAK" Or Session("kdsite").ToString = "JKT" Or Session("kdsite").ToString = "PEN" Then
                    If Today.DayOfWeek = 1 Then
                        'strcon2 = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where checkapp2 = '" + Session("niksite") + "' and freject2 = '0' and checkapp1 = '' and stedit = '1' and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + "  and jam_start >= '" + Today.Date.AddDays(-2) + "'"
                        'perubahan boleh langsung app2 14 april 2014
                        strcon2 = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where checkapp2 = '" + Session("niksite") + "' and freject2 = '0' and stedit = '1' and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + "  and jam_start >= '" + Today.Date.AddDays(-2) + "'"
                    Else
                        'perubahan boleh langsung app2 14 april 2014
                        'strcon2 = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where checkapp2 = '" + Session("niksite") + "' and freject2 = '0' and checkapp1 = '' and stedit = '1' and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + "  and jam_start >= '" + Today.Date + "'"
                        strcon2 = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where checkapp2 = '" + Session("niksite") + "' and freject2 = '0' and stedit = '1' and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + "  and jam_start >= '" + Today.Date.AddDays(-2) + "'"
                    End If
                Else
                    If Today.DayOfWeek = 1 Then
                        'strcon2 = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where checkapp2 = '" + Session("niksite") + "' and freject2 = '0' and checkapp1 = '' and stedit = '1' and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + "  and jam_start >= '" + Today.Date.AddDays(-2) + "'"
                        'perubahan boleh langsung app2 14 april 2014
                        strcon2 = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where checkapp2 = '" + Session("niksite") + "' and freject2 = '0' and stedit = '1' and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + "  and jam_start >= '" + Today.Date.AddDays(-2) + "'"
                    Else
                        'perubahan boleh langsung app2 14 april 2014
                        'strcon2 = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where checkapp2 = '" + Session("niksite") + "' and freject2 = '0' and checkapp1 = '' and stedit = '1' and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + "  and jam_start >= '" + Today.Date + "'"
                        strcon2 = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 where checkapp2 = '" + Session("niksite") + "' and freject2 = '0' and stedit = '1' and StEdit <> 2 and month(Tanggal) = " + Today.Month.ToString + " and YEAR(Tanggal) = " + Today.Year.ToString + "  and jam_start >= '" + Today.Date.AddDays(-1) + "'"
                    End If
                End If

                

                Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn)
                sda2.Fill(dtb2)

                For baris = 0 To dtb2.Rows.Count - 1
                    If dtb2.Rows(baris)!checkapp1.ToString = "" And dtb2.Rows(baris)!checkapp2.ToString = "" Then
                        dtb2.Rows(baris)!status = "Complete"
                    ElseIf dtb2.Rows(baris)!checkapp1.ToString <> "" And dtb2.Rows(baris)!checkapp2.ToString <> "" And dtb2.Rows(baris)!stedit.ToString = "1" Then
                        dtb2.Rows(baris)!status = "Approval 1"
                    ElseIf dtb2.Rows(baris)!checkapp1.ToString = "" And dtb2.Rows(baris)!checkapp2.ToString <> "" And dtb2.Rows(baris)!stedit.ToString = "1" Then
                        dtb2.Rows(baris)!status = "Approval 2"
                    ElseIf dtb2.Rows(baris)!checkapp1.ToString <> "" And dtb2.Rows(baris)!checkapp2.ToString <> "" And dtb2.Rows(baris)!stedit.ToString = "0" Then
                        dtb2.Rows(baris)!status = "Save"
                    End If
                Next
                GridView1.DataSource = dtb2
                GridView1.DataBind()
                ddlbulan.SelectedValue = Today.Month

            Catch ex As Exception
            Finally
                sqlConn.Close()
            End Try
        End If
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim baris As Integer
        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()

            Dim strcon As String

            If Today.DayOfWeek = 1 Then
                strcon = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 "
                'perubahan boleh langsung app2 14 april 2014
                'strcon = strcon + "where checkapp2 = '" + Session("niksite") + "' and freject2 = '0' and stedit = '1' and checkapp1 = '' and StEdit <> 2 and month(Tanggal) = " + ddlbulan.SelectedValue.ToString + " and YEAR(Tanggal) = " + ddltahun.SelectedValue.ToString + " and jam_start >= '" + Today.Date.AddDays(-2) + "'"
                strcon = strcon + "where checkapp2 = '" + Session("niksite") + "' and freject2 = '0' and stedit = '1' and StEdit <> 2 and month(Tanggal) = " + ddlbulan.SelectedValue.ToString + " and YEAR(Tanggal) = " + ddltahun.SelectedValue.ToString + " and jam_start >= '" + Today.Date.AddDays(-2) + "'"
            Else
                strcon = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, convert(varchar(20),jam_start, 113) as jam_start, convert(varchar(20),jam_end, 113) as jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 "
                'perubahan boleh langsung app2 14 april 2014
                'strcon = strcon + "where checkapp2 = '" + Session("niksite") + "' and freject2 = '0' and stedit = '1' and checkapp1 = '' and StEdit <> 2 and month(Tanggal) = " + ddlbulan.SelectedValue.ToString + " and YEAR(Tanggal) = " + ddltahun.SelectedValue.ToString + " and jam_start >= '" + Today.Date + "'"
                strcon = strcon + "where checkapp2 = '" + Session("niksite") + "' and freject2 = '0' and stedit = '1' and StEdit <> 2 and month(Tanggal) = " + ddlbulan.SelectedValue.ToString + " and YEAR(Tanggal) = " + ddltahun.SelectedValue.ToString + " and jam_start >= '" + Today.Date.AddDays(-1) + "'"
            End If

            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)
            For baris = 0 To dtb.Rows.Count - 1
                If dtb.Rows(baris)!checkapp1.ToString = "" And dtb.Rows(baris)!checkapp2.ToString = "" Then
                    dtb.Rows(baris)!status = "Complete"
                ElseIf dtb.Rows(baris)!checkapp1.ToString <> "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "1" Then
                    dtb.Rows(baris)!status = "Approval 1"
                ElseIf dtb.Rows(baris)!checkapp1.ToString = "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "1" Then
                    dtb.Rows(baris)!status = "Approval 2"
                ElseIf dtb.Rows(baris)!checkapp1.ToString <> "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "0" Then
                    dtb.Rows(baris)!status = "Save"
                End If
            Next
            GridView1.DataSource = dtb
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub


    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging

        GridView1.PageIndex = e.NewPageIndex
        GridView1.DataBind()

    End Sub

    Private Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim drv As DataRowView = TryCast(e.Row.DataItem, DataRowView)
            If drv("status").ToString().Equals("Complete") Then
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#11FF66")
            ElseIf drv("status").ToString().Equals("Approval 1") Then
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF85")
            ElseIf drv("status").ToString().Equals("Approval 2") Then
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFE985")
            ElseIf drv("status").ToString().Equals("Save") Then
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#EDE8E8")
            Else
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF3B3B")
            End If
        End If
    End Sub

    Private Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridView1.RowEditing
        Dim row As GridViewRow = GridView1.SelectedRow
        Dim dtbemail As DataTable = New DataTable
        Dim message As New MailMessage()
        Dim mail, niksite, nama, jam_start, jam_end, remarks, reqemail, app2nama, creemail As String
        'Session("splrecordid") = GridView1.DataKeys(row.RowIndex).Value.ToString
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim sqlinsert As String
        Dim cmdinsert As SqlCommand
        Try
            sqlConn.Open()
            sqlinsert = "update H_H110 set freject2 = 1, fdate2 = '" + DateTime.Now + "' where noreg = '" + GridView1.DataKeys(e.NewEditIndex).Value.ToString + "'"
            cmdinsert = New SqlCommand(sqlinsert, sqlConn)
            cmdinsert.ExecuteScalar()

            Dim strcon As String = "select (select niksite from H_A101 where nik = H_H110.nik) as niksite, (select nama from H_A101 where nik = H_H110.nik) as nama, jam_start, jam_end, remarks, (select email from H_A101 where nik = H_H110.requestby) as reqemail, (select email from H_A101 where niksite = H_H110.createdby) as creemail, (select nama from H_A101 where nik = H_H110.app2) as app2nama from H_H110 where noreg = '" + GridView1.DataKeys(e.NewEditIndex).Value.ToString + "'"
            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtbemail)

            If dtbemail.Rows.Count > 0 Then
                reqemail = dtbemail.Rows(0)!reqemail.ToString
                niksite = dtbemail.Rows(0)!niksite.ToString
                nama = dtbemail.Rows(0)!nama.ToString
                jam_start = CDate(dtbemail.Rows(0)!jam_start).ToString("MM/dd/yyyy HH:mm")
                jam_end = CDate(dtbemail.Rows(0)!jam_end).ToString("MM/dd/yyyy HH:mm")
                remarks = dtbemail.Rows(0)!remarks.ToString
                app2nama = dtbemail.Rows(0)!app2nama.ToString
                creemail = dtbemail.Rows(0)!creemail.ToString
            End If

            message = New MailMessage()
            message.From = New MailAddress("noreply.overtime@jresources.com", "Overtime Request Refusal")
            message.Subject = "Overtime Request Refusal App2 " + GridView1.DataKeys(e.NewEditIndex).Value.ToString
            message.IsBodyHtml = True
            message.Body = "Dear, <br><br>"
            message.Body = message.Body + "Overtime Requests: <br><br>"
            message.Body = message.Body + "<table border='0' cellspacing='0' cellpadding='0' style='border=1px;font-size:1em;'><th style='width:10%'>NIK</th><th style='width:30%'>Name</th><th style='width:20%'>Time Start</th><th style='width:20%'>Time End</th><th style='width:20%'>Task</th>"
            message.Body = message.Body + "<tr><td style='text-align:left;border:solid 1px'>" + niksite + "</td><td style='text-align:left;border:solid 1px'>" + nama + "</td><td style='text-align:center;border:solid 1px'>" + jam_start + "</td><td style='text-align:center;border:solid 1px'>" + jam_end + "</td><td style='text-align:left;border:solid 1px'>" + remarks + "</td></tr></table>"
            message.Body = message.Body + "<br>Is Rejected by : <strong>" + app2nama + "</strong>"

            message.Body = message.Body + "<br><br> Regards, <br><br> <strong> </strong>"

            If reqemail = "" And creemail = "" Then
                message.To.Add("syam.kharisman@jresources.com")
            ElseIf reqemail = "" Then
                message.To.Add(creemail)
            ElseIf creemail = "" Then
                message.To.Add(reqemail)
            Else
                message.To.Add(reqemail)
                message.To.Add(creemail)
            End If

            Dim smtp As New SmtpClient()
            'smtp.Host = "smtp.gmail.com"
            smtp.Host = "webmail.jresources.com"
            'smtp.Port = 587
            smtp.Port = 25
            smtp.EnableSsl = True
            'smtp.Credentials = New System.Net.NetworkCredential("noreply.overtime@jresources.com", "jrec54321")
            smtp.Credentials = New System.Net.NetworkCredential("noreply.overtime@jresources.com", "jrec12345")

            smtp.Send(message)

            refresh()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        If GridView1.SelectedRow Is Nothing Then Exit Sub

        Dim row As GridViewRow = GridView1.SelectedRow

        'Session("splrecordid") = GridView1.DataKeys(row.RowIndex).Value.ToString
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim sqlinsert As String
        Dim cmdinsert As SqlCommand
        Try
            sqlConn.Open()
            sqlinsert = "update H_H110 set checkapp2 = '', stedit = '3', fdate2 = '" + DateTime.Now + "' where noreg = '" + GridView1.DataKeys(row.RowIndex).Value.ToString + "'"
            cmdinsert = New SqlCommand(sqlinsert, sqlConn)
            cmdinsert.ExecuteScalar()
            msg = "<script type='text/javascript'> alert('Data has been save'); </script>"
            refresh()
        Catch ex As Exception

        Finally
            sqlConn.Close()
        End Try
    End Sub

    Sub refresh()
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim baris As Integer
        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()

            Dim strcon As String

            If Today.DayOfWeek = 1 Then
                strcon = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, jam_start, jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 "
                strcon = strcon + "where checkapp2 = '" + Session("niksite") + "' and stedit = '1' and freject2 = '0' and checkapp1 = '' and StEdit <> 2 and month(Tanggal) = " + ddlbulan.SelectedValue.ToString + " and YEAR(Tanggal) = " + ddltahun.SelectedValue.ToString + " and jam_start >= '" + Today.Date.AddDays(-2) + "'"
            Else
                strcon = "select noreg,Recordid, periode, nik, (select nama from H_A101 where nik = H_H110.NIK) as nama, (select niksite from H_A101 where nik = H_H110.NIK) as niksite,remarks, jam_start, jam_end, checkapp1, checkapp2, '' as status, stedit from H_H110 "
                strcon = strcon + "where checkapp2 = '" + Session("niksite") + "' and stedit = '1' and freject2 = '0' and checkapp1 = '' and StEdit <> 2 and month(Tanggal) = " + ddlbulan.SelectedValue.ToString + " and YEAR(Tanggal) = " + ddltahun.SelectedValue.ToString + " and jam_start >= '" + Today.Date.AddDays(-1) + "'"
            End If

            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)
            For baris = 0 To dtb.Rows.Count - 1
                If dtb.Rows(baris)!checkapp1.ToString = "" And dtb.Rows(baris)!checkapp2.ToString = "" Then
                    dtb.Rows(baris)!status = "Complete"
                ElseIf dtb.Rows(baris)!checkapp1.ToString <> "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "1" Then
                    dtb.Rows(baris)!status = "Approval 1"
                ElseIf dtb.Rows(baris)!checkapp1.ToString = "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "1" Then
                    dtb.Rows(baris)!status = "Approval 2"
                ElseIf dtb.Rows(baris)!checkapp1.ToString <> "" And dtb.Rows(baris)!checkapp2.ToString <> "" And dtb.Rows(baris)!stedit.ToString = "0" Then
                    dtb.Rows(baris)!status = "Save"
                End If
            Next
            GridView1.DataSource = dtb
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btncomplist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncomplist.Click
        Response.Redirect("spl_approval2_list.aspx")
    End Sub
End Class