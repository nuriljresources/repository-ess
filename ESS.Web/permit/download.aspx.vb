﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Collections.Generic
Imports System.Text
Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Xml.XPath
Imports EXCELLENT.mossservices
Imports sitedatabwebservice
Imports System.Data.SqlClient

Namespace SiteDataWebService


    Class Program


        Public Shared Sub DownLoadAttachment(ByVal strURL As String, ByVal strFileName As String)


            Dim request As HttpWebRequest

            Dim response As HttpWebResponse = Nothing

            Try


                request = DirectCast(WebRequest.Create(strURL), HttpWebRequest)

                'request.Credentials = System.Net.CredentialCache.DefaultCredentials
                request.Credentials = New NetworkCredential("syam.kharisman", "356356")
                request.Timeout = 10000

                request.AllowWriteStreamBuffering = False

                response = DirectCast(request.GetResponse(), HttpWebResponse)

                Dim s As Stream = response.GetResponseStream()

                'Write to disk

                Dim fs As New FileStream("C:\DownLoads\" & strFileName, FileMode.Create)

                Dim read As Byte() = New Byte(255) {}

                Dim count As Integer = s.Read(read, 0, read.Length)

                While count > 0


                    fs.Write(read, 0, count)


                    count = s.Read(read, 0, read.Length)
                End While

                'Close everything

                fs.Close()

                s.Close()


                response.Close()

            Catch ex As Exception


                Console.WriteLine(ex.Message)
            End Try

        End Sub

        Public Shared Sub Main(ByVal args As String())
            Dim i As Integer = 0
            Dim n As Integer
            Dim resdoc As XmlDocument = New System.Xml.XmlDocument()

            Dim resnode As XmlNode = Nothing

            Dim strURL As String = ""

            Dim strFileName As String = ""

            Try


                'Dim objLists As ListsService.Lists = New SiteDataWebService.ListsService.Lists()
                Dim objLists As mossservices.Lists = New Lists
                'Dim objLists As ListsService = New SiteDataWebService.ListsService.Lists()
                'objLists.Credentials = System.Net.CredentialCache.DefaultCredentials
                objLists.Credentials = New NetworkCredential("syam.kharisman", "356356", "jresources")

                'objLists.Url = "http://[SITENAME]:34028/sites/TestSite/_vti_bin/lists.asmx"
                objLists.Url = "http://moss.jresources.com/IT/_vti_bin/lists.asmx"
                ' change the URL to your sharepoint site

                Dim xmlDoc As XmlDocument = New System.Xml.XmlDocument()

                Dim ndQuery As XmlNode = xmlDoc.CreateNode(XmlNodeType.Element, "Query", "")

                Dim ndViewFields As XmlNode = xmlDoc.CreateNode(XmlNodeType.Element, "ViewFields", "")

                Dim ndQueryOptions As XmlNode = xmlDoc.CreateNode(XmlNodeType.Element, "QueryOptions", "")

                ndQueryOptions.InnerXml = "<IncludeAttachmentUrls>TRUE</IncludeAttachmentUrls>"


                'ndQueryOptions.InnerXml = "<ViewAttributes Scope=""Recursive"" />IncludeMandatoryColumns>FALSE</IncludeMandatoryColumns>"
                ndViewFields.InnerXml = ""

                ndQuery.InnerXml = ""


                Try

                    'cnt = 0
                    Dim ndListItems As XmlNode = objLists.GetListItems(args(999).ToString, Nothing, ndQuery, ndViewFields, Nothing, ndQueryOptions, _
                    Nothing)

                    ' you can change the document library name to your custom document library name
                    Dim oNodes As XmlNodeList = ndListItems.ChildNodes

                    For Each node As XmlNode In oNodes


                        Dim objReader As New XmlNodeReader(node)

                        While objReader.Read()
                            If objReader("ows_EncodedAbsUrl") IsNot Nothing AndAlso objReader("ows_LinkFilename") IsNot Nothing Then
                                strURL = objReader("ows_EncodedAbsUrl").ToString()
                                strFileName = objReader("ows_LinkFilename").ToString()
                                DownLoadAttachment(strURL, strFileName)
                                args(i) = strFileName + "," + strURL
                                i = i + 1
                                'cnt(0) = i.ToString
                                'cnt = i.ToString
                            End If

                        End While
                    Next
                    args(1000) = i
                    'cnt(0) = i.ToString
                    Console.ReadLine()

                Catch ex As System.Web.Services.Protocols.SoapException


                    Console.WriteLine((("Message:" & vbLf + ex.Message & vbLf & "Detail:" & vbLf) + ex.Detail.InnerText & vbLf & "StackTrace:" & vbLf) + ex.StackTrace)


                    Console.ReadLine()

                End Try

            Catch ex As Exception


                Console.WriteLine(ex.Message)
            End Try

        End Sub


    End Class

End Namespace


Partial Public Class download
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'SiteDataWebService.Program.DownLoadAttachment("http://moss.jresources.com/IT/IT%20Documents/IST_F-001,%20Rev.%200%20%20IT%20Device%20Requisition.pdf", "IST_F-001,%20Rev.%200%20%20IT%20Device%20Requisition.pdf")
        
    End Sub

    Protected Sub btnupload_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnupload.Click
        Dim str(1000) As String
        Dim i As Integer
        Dim cnt As String
        Dim qrystr As String
        Dim guid As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        conn.Open()
        'qrystr = "Select * from ms_folder"
        'Dim dtb As DataTable = New DataTable
        'Dim sda As SqlDataAdapter = New SqlDataAdapter(qrystr, conn)
        'sda.Fill(dtb)

        guid = ddlupload.SelectedValue
        str(999) = guid
        If guid <> Nothing Or guid <> "" Then
            SiteDataWebService.Program.Main(str)
        Else
            lblinfo.Text = "Empty List"
        End If

        For i = 1 To str(1000) - 1
            'MsgBox(str(i).ToString)

        Next

    End Sub
End Class