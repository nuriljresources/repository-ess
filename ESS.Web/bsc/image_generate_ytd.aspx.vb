﻿Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Imaging

Partial Public Class image_generate_ytd
    Inherits System.Web.UI.Page

    Dim highestval As Decimal = 0

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim id, year As String
        Dim month As Integer
        id = Request.QueryString("id")
        year = Request.QueryString("year")
        month = Request.QueryString("m")
        GenerateGraphic(id, year, month)
    End Sub

    Sub GenerateGraphic(ByVal id As String, ByVal year As String, ByVal month As Integer)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim cmd As SqlCommand
        Try
            Dim sqlQuery As String = "Select month,target,isnull(actual,0) as actual, isnull(b.calctype,0) as calctype from tbsc a left join (select kpiid,calctype from TTARGET a where a.versi = (select max(versi) from ttarget where kpiid='" & id & "' and year='" & year & "') and kpiid='" & id & "' and year='" & year & "')b on a.kpiid=b.kpiid where a.kpiid='" & id & "' and a.year='" & year & "' order by cast(month as integer)"
            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter(sqlQuery, conn)
            conn.Open()
            da.Fill(dt)
            Dim objBitmap As New Bitmap(730, 320)
            Dim objGraphic As Graphics = Graphics.FromImage(objBitmap)
            Dim whiteBrush As New SolidBrush(Color.White)
            Dim blackPen As New Pen(Color.Black, 2)
            Dim redBrush As New SolidBrush(Color.Red)
            Dim blueBrush As New SolidBrush(Color.Blue)

            Dim drawFont As New Font("Verdana", 10)
            Dim drawFontN As New Font("Verdana", 8)
            Dim drawBrush As New SolidBrush(Color.Black)

            'objGraphic.FillRectangle(whiteBrush, 0, 0, 500, 290)
            'objGraphic.DrawLine(blackPen, New Point(60, 195), New Point(500, 195))
            'objGraphic.DrawLine(blackPen, New Point(65, 5), New Point(65, 220))

            'Get Highest Value
            '
            Dim x As Decimal = 70
            'objGraphic.FillRectangle(redBrush, 0, 225, 10, 10)
            'objGraphic.DrawString("ACTUAL", drawFontN, drawBrush, 12, 225)
            'objGraphic.FillRectangle(blueBrush, 0, 235, 10, 10)
            'objGraphic.DrawString("TARGET", drawFontN, drawBrush, 12, 235)
            For Each dr As DataRow In dt.Rows
                GetHighestVal(dr("calctype"), dt, month)
            Next
            If highestval <= 10000 Then
                objGraphic.FillRectangle(whiteBrush, 0, 0, 730, 320)
                objGraphic.DrawLine(blackPen, New Point(60, 235), New Point(730, 235))
                objGraphic.DrawLine(blackPen, New Point(65, 45), New Point(65, 260))
                ''--
                objGraphic.DrawLine(blackPen, New Point(0, 260), New Point(730, 260))
                objGraphic.DrawLine(blackPen, New Point(0, 295), New Point(730, 295))
                ''--
                objGraphic.DrawString("ACTUAL", drawFontN, drawBrush, 12, 265)
                objGraphic.DrawString("TARGET", drawFontN, drawBrush, 12, 275)
                objGraphic.FillRectangle(redBrush, 0, 265, 10, 10)
                objGraphic.FillRectangle(blueBrush, 0, 275, 10, 10)
                objGraphic.DrawString("YEARLY", New Font("Verdana", 14), drawBrush, 245, 5)
            Else
                objBitmap = New Bitmap(1150, 320)
                objGraphic = Graphics.FromImage(objBitmap)
                objGraphic.FillRectangle(whiteBrush, 0, 0, 1150, 320)
                objGraphic.DrawLine(blackPen, New Point(60, 235), New Point(1150, 235))
                objGraphic.DrawLine(blackPen, New Point(65, 45), New Point(65, 260))
                ''--
                objGraphic.DrawLine(blackPen, New Point(0, 262), New Point(1150, 262))
                objGraphic.DrawLine(blackPen, New Point(0, 283), New Point(1150, 283))
                objGraphic.DrawLine(blackPen, New Point(0, 315), New Point(1150, 315))
                ''--
                objGraphic.DrawString("ACTUAL", drawFontN, drawBrush, 12, 285)
                objGraphic.DrawString("TARGET", drawFontN, drawBrush, 12, 295)
                objGraphic.FillRectangle(redBrush, 0, 285, 10, 10)
                objGraphic.FillRectangle(blueBrush, 0, 295, 10, 10)
                objGraphic.DrawString("YTD", New Font("Verdana", 14), drawBrush, 245, 5)
            End If

            Dim tampungtarget As Decimal = 0
            Dim tampungactual As Decimal = 0
            Dim row As Decimal = 1
            Dim cmonth As Integer = 1
            For Each dr As DataRow In dt.Rows
                GetHighestVal(dr("calctype"), dt, month)
                Select Case dr("calctype")
                    Case "0" 'Sum
                        If Not IsDBNull(dr("target")) Then
                            tampungtarget += dr("target")
                        End If
                        tampungactual += dr("actual")
                        objGraphic.FillRectangle(blueBrush, x, 234 - ((tampungtarget / highestval) * 190), 15, ((tampungtarget / highestval) * 190))
                        'objGraphic.DrawString(tampungtarget, drawFontN, drawBrush, x, 194 - ((tampungtarget / highestval) * 190))
                        If cmonth <= month Then
                            objGraphic.FillRectangle(redBrush, x + 15, 234 - ((tampungactual / highestval) * 190), 15, ((tampungactual / highestval) * 190))
                            cmonth += 1
                        End If

                        'objGraphic.DrawString(tampungactual, drawFontN, drawBrush, x + 15, 194 - ((tampungactual / highestval) * 190))

                        objGraphic.DrawString(doMapping(dr("month")), drawFont, drawBrush, x, 245)
                    Case "1" 'AVG
                        If Not IsDBNull(dr("target")) Then
                            tampungtarget += dr("target")
                        End If
                        tampungactual += dr("actual")
                        objGraphic.FillRectangle(blueBrush, x, 234 - (((tampungtarget / row) / highestval) * 190), 15, (((tampungtarget / row) / highestval) * 190))
                        'objGraphic.DrawString((tampungtarget / row), drawFontN, drawBrush, x, 194 - (((tampungtarget / row) / highestval) * 190))
                        If cmonth <= month Then
                            objGraphic.FillRectangle(redBrush, x + 15, 234 - (((tampungactual / row) / highestval) * 190), 15, (((tampungactual / row) / highestval) * 190))
                            cmonth += 1
                        End If

                        'objGraphic.DrawString((tampungactual / row), drawFontN, drawBrush, x + 12, 194 - (((tampungactual / row) / highestval) * 190))

                        objGraphic.DrawString(doMapping(dr("month")), drawFont, drawBrush, x, 245)
                        row += 1
                    Case "2" 'Last Value
                        'For Each dr1 As DataRow In dt.Rows
                        'If dr1("target") <> 0 Then
                        If Not IsDBNull(dr("target")) Then
                            tampungtarget = dr("target")
                        Else
                            tampungtarget = 0
                        End If
                        'End If
                        'Next
                        objGraphic.FillRectangle(blueBrush, x, 234 - ((tampungtarget / highestval) * 190), 15, ((tampungtarget / highestval) * 190))
                        'objGraphic.DrawString(tampungtarget, drawFontN, drawBrush, x, 194 - ((tampungtarget / highestval) * 190))
                        If cmonth <= month Then
                            objGraphic.FillRectangle(redBrush, x + 15, 234 - ((dr("actual") / highestval) * 190), 15, ((dr("actual") / highestval) * 190))
                            cmonth += 1
                        End If

                        'objGraphic.DrawString(dr("actual"), drawFontN, drawBrush, x + 15, 194 - ((dr("actual") / highestval) * 190))

                        objGraphic.DrawString(doMapping(dr("month")), drawFont, drawBrush, x, 245)
                End Select
                x += 55
            Next

            x = 70
            tampungtarget = 0
            tampungactual = 0
            row = 1
            cmonth = 1
            For Each dr As DataRow In dt.Rows
                If highestval <= 10000 Then
                    objGraphic.DrawLine(blackPen, New Point(x - 5, 262), New Point(x - 5, 295))
                    Select Case dr("calctype")
                        Case "0"
                            If Not IsDBNull(dr("target")) Then
                                tampungtarget += dr("target")
                            End If
                            tampungactual += dr("actual")
                            If cmonth <= month Then
                                objGraphic.DrawString(System.Math.Round(tampungactual, 2), drawFontN, drawBrush, x, 265)
                                cmonth += 1
                            End If
                            objGraphic.DrawString(System.Math.Round(tampungtarget, 2), drawFontN, drawBrush, x, 275)
                        Case "1"
                            If Not IsDBNull(dr("target")) Then
                                tampungtarget += dr("target")
                            End If
                            tampungactual += dr("actual")
                            If cmonth <= month Then
                                objGraphic.DrawString(System.Math.Round((tampungactual / row), 2), drawFontN, drawBrush, x, 265)
                                cmonth += 1
                            End If
                            objGraphic.DrawString(System.Math.Round((tampungtarget / row), 2), drawFontN, drawBrush, x, 275)
                            row += 1
                        Case "2"
                            'For Each dr1 As DataRow In dt.Rows
                            '    If dr1("target") <> 0 Then
                            If Not IsDBNull(dr("target")) Then
                                tampungtarget = dr("target")
                            Else
                                tampungtarget = 0
                            End If
                            '                    End If
                            'Next
                            If cmonth <= month Then
                                objGraphic.DrawString(System.Math.Round(dr("actual"), 2), drawFontN, drawBrush, x, 265)
                                cmonth += 1
                            End If
                            objGraphic.DrawString(System.Math.Round(tampungtarget, 2), drawFontN, drawBrush, x, 275)
                    End Select
                    'x += 35
                    x += 55
                Else
                    objGraphic.DrawLine(blackPen, New Point(x - 5, 262), New Point(x - 5, 315))
                    objGraphic.DrawString(doMapping(dr("month")), drawFont, drawBrush, x, 265)
                    Select Case dr("calctype")
                        Case "0"
                            If Not IsDBNull(dr("target")) Then
                                tampungtarget += dr("target")
                            End If
                            tampungactual += dr("actual")
                            If cmonth <= month Then
                                objGraphic.DrawString(System.Math.Round(tampungactual, 2), drawFontN, drawBrush, x, 285)
                                cmonth += 1
                            End If
                            objGraphic.DrawString(System.Math.Round(tampungtarget, 2), drawFontN, drawBrush, x, 295)
                        Case "1"
                            If Not IsDBNull(dr("target")) Then
                                tampungtarget += dr("target")
                            End If
                            tampungactual += dr("actual")
                            If cmonth <= month Then
                                objGraphic.DrawString(System.Math.Round((tampungactual / row), 2), drawFontN, drawBrush, x, 285)
                                cmonth += 1
                            End If
                            objGraphic.DrawString(System.Math.Round((tampungtarget / row), 2), drawFontN, drawBrush, x, 295)
                            row += 1
                        Case "2"
                            'For Each dr1 As DataRow In dt.Rows
                            '    If dr1("target") <> 0 Then
                            If Not IsDBNull(dr("target")) Then
                                tampungtarget = dr("target")
                            Else
                                tampungtarget = 0
                            End If
                            '                    End If
                            'Next
                            If cmonth <= month Then
                                objGraphic.DrawString(System.Math.Round(dr("actual"), 2), drawFontN, drawBrush, x, 285)
                                cmonth += 1
                            End If
                            objGraphic.DrawString(System.Math.Round(tampungtarget, 2), drawFontN, drawBrush, x, 295)
                    End Select
                    'x += 60
                    x += 90
                End If
            Next
            If highestval <= 10000 Then
                objGraphic.DrawLine(blackPen, New Point(730, 262), New Point(730, 295))
            Else
                objGraphic.DrawLine(blackPen, New Point(1150, 262), New Point(1150, 315))
            End If
            Response.ContentType = "image/gif"
            objBitmap.Save(Response.OutputStream, ImageFormat.Gif)
        Catch ex As Exception
            'lblMessage.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Function doMapping(ByVal val As String) As String
        Select Case val
            Case "1"
                Return "JAN"
            Case "2"
                Return "FEB"
            Case "3"
                Return "MAR"
            Case "4"
                Return "APR"
            Case "5"
                Return "MEI"
            Case "6"
                Return "JUN"
            Case "7"
                Return "JUL"
            Case "8"
                Return "AGU"
            Case "9"
                Return "SEP"
            Case "10"
                Return "OKT"
            Case "11"
                Return "NOV"
            Case "12"
                Return "DES"
        End Select
        Return ""
    End Function

    Sub GetHighestVal(ByVal counttype As String, ByVal dt As DataTable, ByVal month As Integer)
        Try
            If highestval <> 0 Then
                Exit Sub
            End If
            Dim target As Decimal = 0
            Dim actual As Decimal = 0
            Select Case counttype
                Case "0" 'SUM
                    For Each dr As DataRow In dt.Rows
                        If Not IsDBNull(dr("target")) Then
                            target += dr("target")
                        End If
                        actual += dr("actual")
                    Next
                    If target > actual Then
                        highestval = target
                    Else
                        highestval = actual
                    End If
                Case "1" 'AVG
                    Dim row As Decimal = 1
                    Dim tampung As Decimal = 0
                    Dim rata As Decimal = 0
                    For Each dr As DataRow In dt.Rows
                        If row = 1 Then
                            If Not IsDBNull(dr("target")) Then
                                If dr("target") > dr("Actual") Then
                                    tampung = dr("target")
                                Else
                                    If dr("month") <= month Then
                                        tampung = dr("actual")
                                    Else
                                        tampung = dr("target")
                                    End If
                                End If
                            End If
                        Else
                            If Not IsDBNull(dr("target")) Then
                                If dr("target") > dr("Actual") Then
                                    tampung += dr("target")
                                Else
                                    If dr("month") <= month Then
                                        tampung += dr("actual")
                                    Else
                                        tampung += dr("target")
                                    End If
                                End If
                                rata = tampung / row
                            End If
                        End If
                        'if tampung > highestval then
                        '    highestval = tampung
                        'End If
                        If rata > highestval Then
                            highestval = rata
                        End If
                        row += 1
                    Next
                Case "2" 'Last Value
                    For Each dr As DataRow In dt.Rows
                        If Not IsDBNull(dr("target")) Then
                            If dr("target") > highestval Then
                                highestval = dr("target")
                            End If
                        End If
                        If dr("actual") > highestval Then
                            highestval = dr("actual")
                        End If
                    Next
            End Select
            If highestval = 0 Then highestval = 1
        Catch ex As Exception

        End Try
    End Sub
End Class