﻿Imports System.Data.SqlClient
Partial Public Class paidleave
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoadData(True)
    End Sub

    Public Property PageNumber() As Integer
        Get
            If ViewState("PageNumber") IsNot Nothing Then
                Return Convert.ToInt32(ViewState("PageNumber"))
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PageNumber") = value
        End Set
    End Property

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        'If (SearchKey.Items. < 3) Then
        'Else
        LoadData(True)
        'End If
    End Sub

    Function LoadData(ByVal bool As Boolean)
        'dnik = Request(SearchKey.UniqueID)
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        'Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("test_con").ConnectionString)
        Try
            Dim dt As New DataTable
            If bool Then
                'Dim myCMD As SqlCommand = New SqlCommand("SELECT  Nik, Nama,KdSite, (select nmdepar from h_a130 where kddepar= H_a101.kddepar) as kddepar,(select nmdepar from h_a130 where kddepar= H_a101.kddepar) + ' [' + (select nmsec from H_A209 ,H_A150 where H_A209.kdsec = H_A150.kdsec and H_A150.kdjabat = H_A101.kdjabatan )  + ']' as NmDepar, (select nmjabat from H_A209 ,H_A150 where H_A209.kdsec = H_A150.kdsec and H_A150.kdjabat = H_A101.kdjabatan ) as nmjabat, (SELECT kdjabat from H_A209 ,H_A150 where H_A209.kdsec = H_A150.kdsec and H_A150.kdjabat = H_A101.kdjabatan) as kdjabat, (select kddepar from h_a130 where kddepar= H_a101.kddepar) as kddepart FROM H_A101 WHERE active = '1' AND stedit <> '2' AND (nama like '%' + @Nama + '%' OR nik like '%' + @Nama + '%') ", sqlConn)
                Dim myCMD As SqlCommand = New SqlCommand("select paidltype, REPLACE(paidltypeDesc,'''', '' ) as paidltypeDesc, (CONVERT(float,leaveday)) as leaveday from L_A002", sqlConn)
                'Dim param As New SqlParameter()
                'param.ParameterName = "@Nama"
                'param.Value = Session("niksite")
                'param.Value = "0123456"
                'myCMD.Parameters.Add(param)
                sqlConn.Open()
                Dim reader As SqlDataReader = myCMD.ExecuteReader()
                dt.Load(reader, LoadOption.OverwriteChanges)
                sqlConn.Close()
                ViewState("dt") = dt
                PageNumber = 0
            End If
            Dim pgitems As New PagedDataSource()
            Dim dv As New DataView(ViewState("dt"))
            pgitems.DataSource = dv
            pgitems.AllowPaging = True
            pgitems.PageSize = 15
            pgitems.CurrentPageIndex = PageNumber

            If pgitems.PageCount > 1 Then
                rptPages.Visible = True
                Dim pages As New ArrayList()
                For i As Integer = 0 To pgitems.PageCount - 1
                    pages.Add((i + 1).ToString())
                Next
                rptPages.DataSource = pages
                rptPages.DataBind()
            Else
                rptPages.Visible = False
            End If
            rptResult.DataSource = pgitems
            rptResult.DataBind()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            If sqlConn.State = ConnectionState.Open Then
                sqlConn.Close()
            End If
        End Try
    End Function

    Private Sub rptPages_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPages.ItemCommand
        PageNumber = Convert.ToInt32(e.CommandArgument) - 1
        LoadData(False)
    End Sub
End Class