﻿Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms

Partial Public Class rtargets
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") = "" Or Session("permission") = Nothing Then
            Response.Redirect("../Login.aspx", False)
            Exit Sub
        ElseIf ("1;2;8;5;B").IndexOf(Session("permission")) = -1 Then
            Response.Redirect("../default.aspx", False)
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim dr As SqlDataReader = Nothing
            Try
                Dim l As String = Request.QueryString("l")
                Dim y As String = Request.QueryString("y")
                Dim sqlQuery As String = ""
                Dim cmd As SqlCommand

                conn.Open()
                'Initialize Function
                sqlQuery = "select ID,FunctionName,active from MFunction where active='1' order by dirid,ID"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Then 'GRUP MSD
                        KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "8" Or Session("permission") = "5" Then 'PIC PDCA 
                        If Session("bscdivisi") = dr(0) Then
                            KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Year
                For Each x As ListItem In ddlYear.Items
                    If x.Text = Date.Now.Year Then
                        x.Selected = True
                    End If
                Next
                'Initialize Direktorat
                sqlQuery = "select ID,DirektoratName,active from MDirektorat where active='1' order by ID"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Then 'GRUP MDV
                        KPIDirektorat.Items.Add(New ListItem(dr(1), dr(0)))
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Section
                sqlQuery = "select ID,SectionName,active,funcid from MSection order by ID"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Or Session("permission") = "2" Or Session("permission") = "3" Or Session("permission") = "6" Then 'GRUP MDV atau sekpro atau BOD
                        KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "7" Then 'GRUP S HEAD
                        If Session("bscdepar") = dr(0) Then
                            KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    ElseIf Session("permission") = "8" Or Session("permission") = "4" Or Session("permission") = "5" Then 'PIC PDCA dan GM dan Manager
                        If Session("bscdivisi") = dr(3) Then
                            KPISection.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                End While
                dr.Close()
                dr = Nothing
                'Initialize Jobsite
                sqlQuery = "select id,kdsite,active from site order by id"
                cmd = New SqlCommand(sqlQuery, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Or Session("permission") = "B" Or Session("permission") = "8" Or Session("permission") = "3" Or Session("permission") = "4" Or Session("permission") = "5" Then 'GRUP MDV & PIC PDCA & BOD & GM & MANAGER
                        KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "7" Or Session("permission") = "2" Or Session("permission") = "6" Then 'GRUP S HEAD & SEKPRO & PM
                        If Session("jobsite") = dr(1) Then
                            KPIJobsite.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                End While
                dr.Close()
                dr = Nothing
                GenerateMenu()
                Select Case KPILevel.SelectedValue
                    Case 0
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                        KPIDirektorat.Visible = False
                    Case 1
                        KPIFunc.Visible = True
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                        KPIDirektorat.Visible = False
                    Case 2
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = True
                        KPISection.Visible = True
                        KPIDirektorat.Visible = False
                    Case 3
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = True
                        KPISection.Visible = False
                        KPIDirektorat.Visible = False
                    Case 4
                        KPIFunc.Visible = False
                        KPIJobsite.Visible = False
                        KPISection.Visible = False
                        KPIDirektorat.Visible = True
                End Select
            Catch ex As Exception
                lblMessage.Text = ex.Message
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
            End Try
        End If
    End Sub

    Protected Sub KPILevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles KPILevel.SelectedIndexChanged
        Try
            Select Case KPILevel.SelectedValue
                Case 0
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                    KPIDirektorat.Visible = False
                Case 1
                    KPIFunc.Visible = True
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                    KPIDirektorat.Visible = False
                Case 2
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = True
                    KPIDirektorat.Visible = False
                Case 3
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = True
                    KPISection.Visible = False
                    KPIDirektorat.Visible = False
                Case 4
                    KPIFunc.Visible = False
                    KPIJobsite.Visible = False
                    KPISection.Visible = False
                    KPIDirektorat.Visible = True
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnView.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim sqlQuery As String = ""
            Select Case KPILevel.SelectedValue
                Case "0" 'Level BUMA
                    sqlQuery = "select a.*,b.* from (select a.Level,a.id,'JRN' as mfunc,a.Name,b.year,b.kpiid,b.calctype,b.versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from mkpi a left join (select row_number() over(partition by kpiid order by kpiid,versi desc) as row,* from TTARGET where kpiid='" & kpiid.Value & "' and year='" & ddlYear.SelectedValue & "') b on a.id=b.kpiid and b.row=1 where a.id='" & kpiid.Value & "' ) a left join ( select kpiid,[1] as ajan,[2] as afeb,[3] as amar,[4] as aapr,[5] as amei,[6] as ajun,[7] as ajul,[8] as aagu,[9] as asep,[10] as aokt,[11] as anov,[12] as ades from ( select b.kpiid,a.MONTHID, b.actual from [month] a left join tbsc b on a.monthid=b.[month] and b.kpiid ='" & kpiid.Value & "' and b.year='" & ddlYear.SelectedValue & "' ) as st PIVOT ( 	AVG(ACTUAL)	FOR MONTHID in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])	)as PT ) b on a.id=b.kpiid " & _
                    "union " & _
                    "select a.*,b.* from ( select a.Level,a.id,FunctionName,a.Name,b.year,b.kpiid,b.calctype,b.versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from MKPI a left join ( " & _
                    "select id,year,kpiid,calctype,versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from( " & _
                    "select row_number() over(partition by kpiid order by kpiid,versi desc) as row,* from TTARGET where kpiid in (select idchild from KPIRELATION where idparent='" & kpiid.Value & "') and YEAR='" & ddlYear.SelectedValue & "' " & _
                    ") aa where row=1 ) b on a.ID=b.kpiid  " & _
                    "inner join MFUNCTION f on a.func = f.ID  " & _
                    "where a.ID in (select idchild from KPIRELATION where idparent='" & kpiid.Value & "')) a left join ( select kpiid,[1] as ajan,[2] as afeb,[3] as amar,[4] as aapr,[5] as amei,[6] as ajun,[7] as ajul,[8] as aagu,[9] as asep,[10] as aokt,[11] as anov,[12] as ades from ( select b.kpiid,a.MONTHID, b.actual from [month] a left join tbsc b on a.monthid=b.[month] and b.kpiid in (select idchild from KPIRELATION where idparent='" & kpiid.Value & "') and b.year='" & ddlYear.SelectedValue & "' ) as st PIVOT ( 	AVG(ACTUAL)	FOR MONTHID in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])	)as PT ) b on a.id=b.kpiid "
                Case "1" 'Level Function
                    sqlQuery = "select a.*,b.* from (select a.Level,a.id,'" & KPIFunc.SelectedItem.Text & "' as mfunc ,a.Name,b.year,b.kpiid,b.calctype,b.versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from mkpi a left join (select row_number() over(partition by kpiid order by kpiid,versi desc) as row,* from TTARGET where kpiid='" & kpiid.Value & "' and year='" & ddlYear.SelectedValue & "') b on a.id=b.kpiid and b.row=1 where a.id='" & kpiid.Value & "' ) a left join ( select kpiid,[1] as ajan,[2] as afeb,[3] as amar,[4] as aapr,[5] as amei,[6] as ajun,[7] as ajul,[8] as aagu,[9] as asep,[10] as aokt,[11] as anov,[12] as ades from ( select b.kpiid,a.MONTHID, b.actual from [month] a left join tbsc b on a.monthid=b.[month] and b.kpiid ='" & kpiid.Value & "' and b.year='" & ddlYear.SelectedValue & "' ) as st PIVOT ( 	AVG(ACTUAL)	FOR MONTHID in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])	)as PT ) b on a.id=b.kpiid " & _
                    "union " & _
                    "select a.*,b.* from (select a.Level,a.id,s.kdsite,a.Name,b.year,b.kpiid,b.calctype,b.versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from MKPI a left join (  " & _
                    "select id,year,kpiid,calctype,versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from(  " & _
                    "select row_number() over(partition by kpiid order by kpiid,versi desc) as row,* from TTARGET where kpiid in (select idchild from KPIRELATION where idparent='" & kpiid.Value & "') and YEAR='" & ddlYear.SelectedValue & "'   " & _
                    ") aa where row=1 ) b on a.ID=b.kpiid   " & _
                    "inner join SITE s on a.Jobsite =s.id   " & _
                    "where a.ID in (select idchild from KPIRELATION where idparent='" & kpiid.Value & "')) a left join ( select kpiid,[1] as ajan,[2] as afeb,[3] as amar,[4] as aapr,[5] as amei,[6] as ajun,[7] as ajul,[8] as aagu,[9] as asep,[10] as aokt,[11] as anov,[12] as ades from ( select b.kpiid,a.MONTHID, b.actual from [month] a left join tbsc b on a.monthid=b.[month] and b.kpiid in (select idchild from KPIRELATION where idparent='" & kpiid.Value & "') and b.year='" & ddlYear.SelectedValue & "' ) as st PIVOT ( 	AVG(ACTUAL)	FOR MONTHID in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])	)as PT ) b on a.id=b.kpiid "
                Case "2" 'Level Section
                    sqlQuery = "select a.*,b.* from (select a.Level,a.id,'" & KPISection.SelectedItem.Text & "' as mfunc ,a.Name,b.year,b.kpiid,b.calctype,b.versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from mkpi a left join (select row_number() over(partition by kpiid order by kpiid,versi desc) as row,* from TTARGET where kpiid='" & kpiid.Value & "' and year='" & ddlYear.SelectedValue & "') b on a.id=b.kpiid and b.row=1 where a.id='" & kpiid.Value & "' ) a left join ( select kpiid,[1] as ajan,[2] as afeb,[3] as amar,[4] as aapr,[5] as amei,[6] as ajun,[7] as ajul,[8] as aagu,[9] as asep,[10] as aokt,[11] as anov,[12] as ades from ( select b.kpiid,a.MONTHID, b.actual from [month] a left join tbsc b on a.monthid=b.[month] and b.kpiid ='" & kpiid.Value & "' and b.year='" & ddlYear.SelectedValue & "' ) as st PIVOT ( 	AVG(ACTUAL)	FOR MONTHID in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])	)as PT ) b on a.id=b.kpiid " & _
"union " & _
"select a.*,b.* from (select a.Level,a.id,s.sectionName,a.Name,b.year,b.kpiid,b.calctype,b.versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from MKPI a left join (  " & _
"select id,year,kpiid,calctype,versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from(  " & _
"select row_number() over(partition by kpiid order by kpiid,versi desc) as row,* from TTARGET where kpiid in (select idchild from KPIRELATION where idparent='" & kpiid.Value & "') and YEAR='" & ddlYear.SelectedValue & "'   " & _
") aa where row=1 ) b on a.ID=b.kpiid   " & _
"inner join MSECTION s on a.section=s.id   " & _
"where a.ID in (select idchild from KPIRELATION where idparent='" & kpiid.Value & "')) a left join ( select kpiid,[1] as ajan,[2] as afeb,[3] as amar,[4] as aapr,[5] as amei,[6] as ajun,[7] as ajul,[8] as aagu,[9] as asep,[10] as aokt,[11] as anov,[12] as ades from ( select b.kpiid,a.MONTHID, b.actual from [month] a left join tbsc b on a.monthid=b.[month] and b.kpiid in (select idchild from KPIRELATION where idparent='" & kpiid.Value & "') and b.year='" & ddlYear.SelectedValue & "' ) as st PIVOT ( 	AVG(ACTUAL)	FOR MONTHID in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])	)as PT ) b on a.id=b.kpiid order by Level desc,id "

                Case "3" 'Level Jobsite
                    sqlQuery = "select a.*,b.* from (select a.Level,a.id,'" & KPIJobsite.SelectedItem.Text & "' as mfunc ,a.Name,b.year,b.kpiid,b.calctype,b.versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from mkpi a left join (select row_number() over(partition by kpiid order by kpiid,versi desc) as row,* from TTARGET where kpiid='" & kpiid.Value & "' and year='" & ddlYear.SelectedValue & "') b on a.id=b.kpiid and b.row=1 where a.id='" & kpiid.Value & "' ) a left join ( select kpiid,[1] as ajan,[2] as afeb,[3] as amar,[4] as aapr,[5] as amei,[6] as ajun,[7] as ajul,[8] as aagu,[9] as asep,[10] as aokt,[11] as anov,[12] as ades from ( select b.kpiid,a.MONTHID, b.actual from [month] a left join tbsc b on a.monthid=b.[month] and b.kpiid ='" & kpiid.Value & "' and b.year='" & ddlYear.SelectedValue & "' ) as st PIVOT ( 	AVG(ACTUAL)	FOR MONTHID in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])	)as PT ) b on a.id=b.kpiid " & _
                    "union " & _
                    "select a.*,b.* from (select a.Level,a.id,s.sectionName,a.Name,b.year,b.kpiid,b.calctype,b.versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from MKPI a left join (  " & _
                    "select id,year,kpiid,calctype,versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from(  " & _
                    "select row_number() over(partition by kpiid order by kpiid,versi desc) as row,* from TTARGET where kpiid in (select idchild from KPIRELATION where idparent='" & kpiid.Value & "') and YEAR='" & ddlYear.SelectedValue & "'   " & _
                    ") aa where row=1 ) b on a.ID=b.kpiid   " & _
                    "inner join MSECTION s on a.section=s.id   " & _
                    "where a.ID in (select idchild from KPIRELATION where idparent='" & kpiid.Value & "')) a left join ( select kpiid,[1] as ajan,[2] as afeb,[3] as amar,[4] as aapr,[5] as amei,[6] as ajun,[7] as ajul,[8] as aagu,[9] as asep,[10] as aokt,[11] as anov,[12] as ades from ( select b.kpiid,a.MONTHID, b.actual from [month] a left join tbsc b on a.monthid=b.[month] and b.kpiid in (select idchild from KPIRELATION where idparent='" & kpiid.Value & "') and b.year='" & ddlYear.SelectedValue & "' ) as st PIVOT ( 	AVG(ACTUAL)	FOR MONTHID in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])	)as PT ) b on a.id=b.kpiid order by Level desc,id "
                Case "4" 'Level Function
                    sqlQuery = "select a.*,b.* from (select a.Level,a.id,'" & KPIDirektorat.SelectedItem.Text & "' as mfunc ,a.Name,b.year,b.kpiid,b.calctype,b.versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from mkpi a left join (select row_number() over(partition by kpiid order by kpiid,versi desc) as row,* from TTARGET where kpiid='" & kpiid.Value & "' and year='" & ddlYear.SelectedValue & "') b on a.id=b.kpiid and b.row=1 where a.id='" & kpiid.Value & "' ) a left join ( select kpiid,[1] as ajan,[2] as afeb,[3] as amar,[4] as aapr,[5] as amei,[6] as ajun,[7] as ajul,[8] as aagu,[9] as asep,[10] as aokt,[11] as anov,[12] as ades from ( select b.kpiid,a.MONTHID, b.actual from [month] a left join tbsc b on a.monthid=b.[month] and b.kpiid ='" & kpiid.Value & "' and b.year='" & ddlYear.SelectedValue & "' ) as st PIVOT ( 	AVG(ACTUAL)	FOR MONTHID in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])	)as PT ) b on a.id=b.kpiid " & _
                    "union " & _
                    "select a.*,b.* from (select a.Level,a.id,s.kdsite,a.Name,b.year,b.kpiid,b.calctype,b.versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from MKPI a left join (  " & _
                    "select id,year,kpiid,calctype,versi,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,annual from(  " & _
                    "select row_number() over(partition by kpiid order by kpiid,versi desc) as row,* from TTARGET where kpiid in (select idchild from KPIRELATION where idparent='" & kpiid.Value & "') and YEAR='" & ddlYear.SelectedValue & "'   " & _
                    ") aa where row=1 ) b on a.ID=b.kpiid   " & _
                    "inner join SITE s on a.Jobsite =s.id   " & _
                    "where a.ID in (select idchild from KPIRELATION where idparent='" & kpiid.Value & "')) a left join ( select kpiid,[1] as ajan,[2] as afeb,[3] as amar,[4] as aapr,[5] as amei,[6] as ajun,[7] as ajul,[8] as aagu,[9] as asep,[10] as aokt,[11] as anov,[12] as ades from ( select b.kpiid,a.MONTHID, b.actual from [month] a left join tbsc b on a.monthid=b.[month] and b.kpiid in (select idchild from KPIRELATION where idparent='" & kpiid.Value & "') and b.year='" & ddlYear.SelectedValue & "' ) as st PIVOT ( 	AVG(ACTUAL)	FOR MONTHID in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])	)as PT ) b on a.id=b.kpiid "


            End Select
            Dim dr As SqlDataReader
            Dim cmd As New SqlCommand(sqlQuery, conn)
            conn.Open()
            dr = cmd.ExecuteReader()
            result.InnerHtml = ""
            If dr.HasRows Then
                result.InnerHtml += "<table border='1' class='act'>"
                Select Case KPILevel.SelectedValue
                    Case "0" 'Level BUMA
                        result.InnerHtml += "<thead><tr style='background-color:#B6DDE8;font-weight:bold'><td style='width:25%'></td><td style='width:1%'>Versi</td><td style='width:5%'>FUNCTION</td><td></td><td>End Of Year</td><td style='width:5%'>JAN</td><td style='width:5%'>FEB</td><td style='width:5%'>MAR</td><td style='width:5%'>APR</td><td style='width:5%'>MEI</td><td style='width:5%'>JUN</td><td style='width:5%'>JUL</td><td style='width:5%'>AGU</td><td style='width:5%'>SEP</td><td style='width:5%'>OKT</td><td style='width:5%'>NOV</td><td style='width:5%'>DES</td></tr></thead>"
                    Case "1" 'Level Function
                        result.InnerHtml += "<thead><tr style='background-color:#B6DDE8;font-weight:bold'><td style='width:25%'></td><td style='width:1%'>Versi</td><td style='width:5%'>SITE</td><td></td><td>End Of Year</td><td style='width:5%'>JAN</td><td style='width:5%'>FEB</td><td style='width:5%'>MAR</td><td style='width:5%'>APR</td><td style='width:5%'>MEI</td><td style='width:5%'>JUN</td><td style='width:5%'>JUL</td><td style='width:5%'>AGU</td><td style='width:5%'>SEP</td><td style='width:5%'>OKT</td><td style='width:5%'>NOV</td><td style='width:5%'>DES</td></tr></thead>"
                    Case "2" 'Level Section
                        result.InnerHtml += "<thead><tr style='background-color:#B6DDE8;font-weight:bold'><td style='width:25%'></td><td style='width:1%'>Versi</td><td style='width:5%'>SECTION</td><td></td><td>End Of Year</td><td style='width:5%'>JAN</td><td style='width:5%'>FEB</td><td style='width:5%'>MAR</td><td style='width:5%'>APR</td><td style='width:5%'>MEI</td><td style='width:5%'>JUN</td><td style='width:5%'>JUL</td><td style='width:5%'>AGU</td><td style='width:5%'>SEP</td><td style='width:5%'>OKT</td><td style='width:5%'>NOV</td><td style='width:5%'>DES</td></tr></thead>"
                    Case "3" 'Level Jobsite
                        result.InnerHtml += "<thead><tr style='background-color:#B6DDE8;font-weight:bold'><td style='width:25%'></td><td style='width:1%'>Versi</td><td style='width:5%'>SITE</td><td></td><td>End Of Year</td><td style='width:5%'>JAN</td><td style='width:5%'>FEB</td><td style='width:5%'>MAR</td><td style='width:5%'>APR</td><td style='width:5%'>MEI</td><td style='width:5%'>JUN</td><td style='width:5%'>JUL</td><td style='width:5%'>AGU</td><td style='width:5%'>SEP</td><td style='width:5%'>OKT</td><td style='width:5%'>NOV</td><td style='width:5%'>DES</td></tr></thead>"
                    Case "4" 'Level Direktori
                        result.InnerHtml += "<thead><tr style='background-color:#B6DDE8;font-weight:bold'><td style='width:25%'></td><td style='width:1%'>Versi</td><td style='width:5%'>DIREKTORAT</td><td></td><td>End Of Year</td><td style='width:5%'>JAN</td><td style='width:5%'>FEB</td><td style='width:5%'>MAR</td><td style='width:5%'>APR</td><td style='width:5%'>MEI</td><td style='width:5%'>JUN</td><td style='width:5%'>JUL</td><td style='width:5%'>AGU</td><td style='width:5%'>SEP</td><td style='width:5%'>OKT</td><td style='width:5%'>NOV</td><td style='width:5%'>DES</td></tr></thead>"
                End Select
                Dim count As Integer = 1
                Dim cc As Integer = 1
                Dim dummy As String = ""
                Dim total As Decimal
                Dim pembagi As Decimal
                Dim lastval As Decimal

                While dr.Read
                    total = 0
                    pembagi = 0
                    lastval = 0
                    If dummy <> dr("Name") Then
                        result.InnerHtml = result.InnerHtml.Replace("&x&", CStr(count))
                        count = 0
                        dummy = dr("Name")
                        result.InnerHtml += "<tr><td rowspan='&x&'>" & dr("Name") & "</td>" & _
                        "<td>" & IIf(IsDBNull(dr("versi")), "-", dr("versi")) & "</td>" & _
                        "<td  rowspan='2' style='background-color:" & IIf(cc = 1, "#FFC000", "#F2DDDC") & "'>" & dr(2) & "</td><td>Plan</td>"
                        If IsDBNull(dr("annual")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("annual").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("annual")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("jan")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("jan").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("jan")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("feb")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("feb").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("feb")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("mar")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("mar").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("mar")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("apr")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("apr").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("apr")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("mei")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("mei").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("mei")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("jun")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("jun").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("jun")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("jul")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("jul").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("jul")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("agu")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("agu").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("agu")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("sep")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("sep").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("sep")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("okt")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("okt").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("okt")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("nov")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("nov").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("nov")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("des")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("des").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("des")), "#,##0.00") & "</td>"
                            End If
                        End If
                        result.InnerHtml += "</tr>"
                        '-------------------------------------------------------------------
                        'add 
                        result.InnerHtml += "<tr>" & _
                      "<td></td>" & _
                      "<td>Actual</td>"
                        result.InnerHtml += "<td>#MYACTA#</td>"
                        If IsDBNull(dr("ajan")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("ajan").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("ajan")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("ajan")), "#,##0.00")
                                lastval = Format(CDec(dr("ajan")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("afeb")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("afeb").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("afeb")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("afeb")), "#,##0.00")
                                lastval = Format(CDec(dr("afeb")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("amar")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("amar").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("amar")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("amar")), "#,##0.00")
                                lastval = Format(CDec(dr("amar")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("aapr")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("aapr").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("aapr")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("aapr")), "#,##0.00") '
                                lastval = Format(CDec(dr("aapr")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("amei")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("amei").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("amei")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("amei")), "#,##0.00") '
                                lastval = Format(CDec(dr("amei")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("ajun")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("ajun").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("ajun")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("ajun")), "#,##0.00") '
                                lastval = Format(CDec(dr("ajun")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("ajul")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("ajul").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("ajul")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("ajul")), "#,##0.00") '
                                lastval = Format(CDec(dr("ajul")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("aagu")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("aagu").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("aagu")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("aagu")), "#,##0.00") '
                                lastval = Format(CDec(dr("aagu")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("asep")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("asep").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("asep")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("asep")), "#,##0.00") '
                                lastval = Format(CDec(dr("asep")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("aokt")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("aokt").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("aokt")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("aokt")), "#,##0.00") '
                                lastval = Format(CDec(dr("aokt")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("anov")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("anov").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("anov")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("anov")), "#,##0.00") '
                                lastval = Format(CDec(dr("anov")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("ades")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("ades").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("ades")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("ades")), "#,##0.00") '
                                lastval = Format(CDec(dr("ades")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        result.InnerHtml += "</tr>"

                        If IsDBNull(dr("calctype")) Then
                            result.InnerHtml = result.InnerHtml.Replace("#MYACTA#", "")
                        Else
                            Select Case dr("calctype")
                                Case "0"
                                    result.InnerHtml = result.InnerHtml.Replace("#MYACTA#", Format(CDec(total), "#,##0.00"))
                                Case "1"
                                    If pembagi = 0 Then
                                        result.InnerHtml = result.InnerHtml.Replace("#MYACTA#", "-")
                                    Else
                                        result.InnerHtml = result.InnerHtml.Replace("#MYACTA#", Format(CDec(total) / pembagi, "#,##0.00"))
                                    End If
                                Case "2"
                                    result.InnerHtml = result.InnerHtml.Replace("#MYACTA#", Format(CDec(lastval), "#,##0.00"))
                                Case "3"
                                    result.InnerHtml = result.InnerHtml.Replace("#MYACTA#", Format(CDec(lastval), "#,##0.00"))
                            End Select
                        End If


                    Else
                        result.InnerHtml += "<tr>" & _
                        "<td>" & IIf(IsDBNull(dr("versi")), "-", dr("versi")) & "</td>" & _
                        "<td rowspan='2' style='background-color:" & IIf(cc = 1, "#FFC000", "#F2DDDC") & "'>" & dr(2) & "</td><td>Plan</td>"
                        If IsDBNull(dr("annual")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("annual").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("annual")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("jan")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("jan") = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("jan")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("feb")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("feb") = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("feb")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("mar")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("mar") = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("mar")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("apr")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("apr") = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("apr")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("mei")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("mei") = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("mei")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("jun")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("jun") = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("jun")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("jul")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("jul") = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("jul")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("agu")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("agu") = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("agu")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("sep")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("sep") = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("sep")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("okt")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("okt") = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("okt")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("nov")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("nov") = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("nov")), "#,##0.00") & "</td>"
                            End If
                        End If
                        If IsDBNull(dr("des")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("des") = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("des")), "#,##0.00") & "</td>"
                            End If
                        End If
                        result.InnerHtml += "</tr>"

                        '-------------------------------------------------------------------
                        'add 
                        result.InnerHtml += "<tr>" & _
                      "<td></td>" & _
                      "<td>Actual</td>"
                        result.InnerHtml += "<td>#MYACTA#</td>"
                        If IsDBNull(dr("ajan")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("ajan").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("ajan")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("ajan")), "#,##0.00")
                                lastval = Format(CDec(dr("ajan")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("afeb")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("afeb").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("afeb")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("afeb")), "#,##0.00")
                                lastval = Format(CDec(dr("afeb")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("amar")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("amar").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("amar")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("amar")), "#,##0.00")
                                lastval = Format(CDec(dr("amar")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("aapr")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("aapr").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("aapr")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("aapr")), "#,##0.00") '
                                lastval = Format(CDec(dr("aapr")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("amei")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("amei").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("amei")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("amei")), "#,##0.00") '
                                lastval = Format(CDec(dr("amei")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("ajun")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("ajun").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("ajun")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("ajun")), "#,##0.00") '
                                lastval = Format(CDec(dr("ajun")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("ajul")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("ajul").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("ajul")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("ajul")), "#,##0.00") '
                                lastval = Format(CDec(dr("ajul")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("aagu")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("aagu").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("aagu")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("aagu")), "#,##0.00") '
                                lastval = Format(CDec(dr("aagu")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("asep")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("asep").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("asep")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("asep")), "#,##0.00") '
                                lastval = Format(CDec(dr("asep")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("aokt")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("aokt").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("aokt")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("aokt")), "#,##0.00") '
                                lastval = Format(CDec(dr("aokt")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("anov")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("anov").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("anov")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("anov")), "#,##0.00") '
                                lastval = Format(CDec(dr("anov")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        If IsDBNull(dr("ades")) Then
                            result.InnerHtml += "<td>-</td>"
                        Else
                            If dr("ades").ToString() = "" Then
                                result.InnerHtml += "<td></td>"
                            Else
                                result.InnerHtml += "<td>" & Format(CDec(dr("ades")), "#,##0.00") & "</td>"
                                total += Format(CDec(dr("ades")), "#,##0.00") '
                                lastval = Format(CDec(dr("ades")), "#,##0.00")
                                pembagi += 1
                            End If
                        End If
                        result.InnerHtml += "</tr>"

                        If IsDBNull(dr("calctype")) Then
                            result.InnerHtml = result.InnerHtml.Replace("#MYACTA#", "")
                        Else
                            Select Case dr("calctype")
                                Case "0"
                                    result.InnerHtml = result.InnerHtml.Replace("#MYACTA#", Format(CDec(total), "#,##0.00"))
                                Case "1"
                                    If pembagi = 0 Then
                                        result.InnerHtml = result.InnerHtml.Replace("#MYACTA#", "-")
                                    Else
                                        result.InnerHtml = result.InnerHtml.Replace("#MYACTA#", Format(CDec(total) / pembagi, "#,##0.00"))
                                    End If
                                Case "2"
                                    result.InnerHtml = result.InnerHtml.Replace("#MYACTA#", Format(CDec(lastval), "#,##0.00"))
                                Case "3"
                                    result.InnerHtml = result.InnerHtml.Replace("#MYACTA#", Format(CDec(lastval), "#,##0.00"))
                            End Select
                        End If
                    End If
                    count += 2
                    cc += 1
                End While
                result.InnerHtml = result.InnerHtml.Replace("&x&", CStr(count))
                result.InnerHtml += "</table>"
                result.InnerHtml += "<input type='button' value='print' onclick='javascript:fnprint()' id='btnprint'/>"
            Else
                result.InnerHtml = "No Data Found"
            End If
            dr.Close()
            dr = Nothing
            conn.Close()
        Catch ex As Exception
            lblMessage.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Sub GenerateMenu()
        Select Case Session("permission")
            Case "1" 'MSD
                For Each x As ListItem In KPILevel.Items
                    If x.Value <> "0" And x.Value <> "1" And x.Value <> "3" And x.Value <> "4" Then
                        x.Enabled = False
                    ElseIf x.Value = "0" Then
                        x.Selected = True
                    End If
                Next
            Case "B" 'MSD Func. Head
                For Each x As ListItem In KPILevel.Items
                    If x.Value <> "0" And x.Value <> "1" And x.Value <> "3" And x.Value <> "4" Then
                        x.Enabled = False
                    ElseIf x.Value = "0" Then
                        x.Selected = True
                    End If
                Next
            Case "2"
                For Each x As ListItem In KPILevel.Items
                    If x.Value <> "2" And x.Value <> "3" Then
                        x.Enabled = False
                    ElseIf x.Value = "2" Then
                        x.Selected = True
                    End If
                Next
            Case "8"
                For Each x As ListItem In KPILevel.Items
                    If x.Value <> "1" Then
                        x.Enabled = False
                    ElseIf x.Value = "1" Then
                        x.Selected = True
                    End If
                Next
            Case "7"
                For Each x As ListItem In KPILevel.Items
                    If x.Value <> "2" Then
                        x.Enabled = False
                    ElseIf x.Value = "2" Then
                        x.Selected = True
                    End If
                Next
        End Select
    End Sub
End Class