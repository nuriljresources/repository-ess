﻿Imports System.Security.Cryptography
Imports System.IO

Partial Public Class ssss
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Function HashString(ByVal Text As String) As String
        Dim hashedPassword As String = ""
        Dim hashProvider As SHA256Managed
        Dim hashdecode As AesManaged
        Try
            Dim passwordBytes() As Byte
            'Dim hashBytes() As Byte

            passwordBytes = System.Text.Encoding.Unicode.GetBytes(Text)
            hashProvider = New SHA256Managed()
            passwordBytes = hashProvider.ComputeHash(passwordBytes)
            hashedPassword = Convert.ToBase64String(passwordBytes)
        Catch ex As Exception
            'logger.LogRequest("At HashString: " + ex.Message)
            MsgBox(ex.Message)
        Finally
            If Not hashProvider Is Nothing Then
                hashProvider.Clear()
                hashProvider = Nothing
            End If
        End Try
        Return hashedPassword
    End Function

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        lbltes.Text = Encrypt(Replace(Me.tes.Text, "'", ""))
    End Sub

    Private Function Encrypt(ByVal input As String) As String
        Return Convert.ToBase64String(Encrypt(Encoding.UTF8.GetBytes(input)))
    End Function
    Private Function Encrypt(ByVal input As Byte()) As Byte()
        'hjiweykaksd
        Dim pdb As New PasswordDeriveBytes("hjiweykaksd", New Byte() {&H43, &H87, &H23, &H72, &H45, &H56, _
         &H68, &H14, &H62, &H84})
        Dim ms As New MemoryStream()
        Dim aes As Aes = New AesManaged()
        aes.Key = pdb.GetBytes(aes.KeySize / 8)
        aes.IV = pdb.GetBytes(aes.BlockSize / 8)
        Dim cs As New CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write)
        cs.Write(input, 0, input.Length)
        cs.Close()
        Return ms.ToArray()
    End Function
    Private Function Decrypt(ByVal input As String) As String
        Return Encoding.UTF8.GetString(Decrypt(Convert.FromBase64String(input)))
    End Function
    Private Function Decrypt(ByVal input As Byte()) As Byte()

        Dim pdb As New PasswordDeriveBytes("hjiweykaksd", New Byte() {&H43, &H87, &H23, &H72, &H45, &H56, _
         &H68, &H14, &H62, &H84})
        Dim ms As New MemoryStream()
        Dim aes As Aes = New AesManaged()
        aes.Key = pdb.GetBytes(aes.KeySize / 8)
        aes.IV = pdb.GetBytes(aes.BlockSize / 8)
        Dim cs As New CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write)
        cs.Write(input, 0, input.Length)
        cs.Close()
        Return ms.ToArray()
    End Function

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        lbltes2.Text = Decrypt(Replace(Me.tes2.Text, "'", ""))
    End Sub
End Class