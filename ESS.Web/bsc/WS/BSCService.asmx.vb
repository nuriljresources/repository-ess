﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Web.Script.Services
Imports System
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
<ScriptService()> _
Public Class BSCService
    Inherits System.Web.Services.WebService
    Private Const saltkey As String = "@dmin1t8um4H0"

    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function

    <WebMethod()> _
  Public Function RetrieveKPI(ByVal _id As String) As KPI
        Try
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim query As String = ""
            query = "select a.*,isnull(b.Type,'55') as typediscrete from MKPI a left join calccontent b on a.calctype=b.calctype and a.ID=b.kpiid where a.id='" & _id & "'"
            Dim cmd As SqlCommand
            cmd = New SqlCommand(query, conn)
            conn.Open()
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()
            Dim _KPI As New KPI()
            While dr.Read
                _KPI.Id = dr("ID")
                _KPI.Level = dr("Level")
                _KPI.Soid = dr("SOID")
                _KPI.Name = dr("Name")
                _KPI.Func = dr("Func")
                _KPI.Uom = dr("UoM")
                _KPI.Owner = dr("Owner")
                _KPI.Type = dr("Type")
                _KPI.Desc = dr("Desc")
                _KPI.Periode = dr("Period")
                _KPI.Leads = dr("Leads")
                _KPI.Formula = dr("Formula")
                _KPI.FormulaDesc = dr("FormulaDesc")
                _KPI.RedParam = dr("RedParam")
                _KPI.GreenParam = dr("GreenParam")
                _KPI.BasedOn = dr("BasedOn")
                _KPI.CalcType = dr("CalcType")
                _KPI.Jobsite = dr("Jobsite")
                _KPI.Section = dr("Section")
                _KPI.GreenCond = dr("GreenCond")
                _KPI.RedCond = dr("RedCond")
                If IsDBNull(dr("Bobot")) Then
                    _KPI.Bobot = 0
                Else
                    _KPI.Bobot = dr("Bobot")
                End If
                If IsDBNull(dr("KPIType")) Then
                    _KPI.KPIType = "0"
                Else
                    _KPI.KPIType = dr("KPIType")
                End If
                If IsDBNull(dr("KPILeads")) Then
                    _KPI.KPILeads = ""
                Else
                    _KPI.KPILeads = dr("KPILeads")
                End If
                If IsDBNull(dr("Process")) Then
                    _KPI.Process = ""
                Else
                    _KPI.Process = dr("Process")
                End If
                If IsDBNull(dr("Targetting")) Then
                    _KPI.Targetting = "0"
                Else
                    _KPI.Targetting = dr("Targetting")
                End If
                _KPI.Typediscrete = dr("typediscrete")
                _KPI.Ready = dr("StEdit")
                _KPI.Year = dr("year")
            End While
            conn.Close()
            Return _KPI
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Function

    <WebMethod()> _
  Public Function RetrieveChild(ByVal _idparent As String, ByVal _type As String) As List(Of ChildKPI)
        Try
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim query As String = ""
            Select Case _type
                Case "0"
                    query = "select a.ID,c.DirektoratName,a.Name, b.includecount,a.uom  from MKPI a " _
                    & "inner join kpirelation b on a.ID = b.idchild " _
                    & "inner join MDIREKTORAT c on a.Dir=c.ID " _
                    & "where b.idparent='" & _idparent & "'"
                Case "4"
                    query = "select a.ID,c.FunctionName,a.Name, b.includecount,a.uom  from MKPI a " _
                  & "inner join kpirelation b on a.ID = b.idchild " _
                  & "inner join MFUNCTION c on a.Func=c.ID " _
                  & "where b.idparent='" & _idparent & "'"
            End Select
            
            Dim cmd As SqlCommand
            cmd = New SqlCommand(query, conn)
            conn.Open()
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()
            Dim _ChildKPIWrap As New List(Of ChildKPI)
            Dim _ChildKPI As ChildKPI
            While dr.Read
                _ChildKPI = New ChildKPI()
                _ChildKPI.IdKPI = dr(0)
                _ChildKPI.FuncKPI = dr(1)
                _ChildKPI.NamaKPI = dr(2)
                _ChildKPI.IncludeCount = dr(3)
                _ChildKPI.Uom = "'" & dr(4) & "'"
                _ChildKPIWrap.Add(_ChildKPI)
            End While
            conn.Close()
            Return _ChildKPIWrap
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try




    End Function

    <WebMethod()> _
  Public Function RetrieveFormula(ByVal _id As String) As List(Of Formula)
        Try
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim query As String = ""
            query = "select * from MKPIFORMULA where kpiid='" & _id & "'"
            Dim cmd As SqlCommand
            cmd = New SqlCommand(query, conn)
            conn.Open()
            Dim dr As SqlDataReader
            dr = cmd.ExecuteReader()
            Dim _FormulaWrap As New List(Of Formula)
            Dim _Formula As Formula
            While dr.Read
                _Formula = New Formula()
                _Formula.Id = dr("id")
                _Formula.Kpiid = dr("kpiid")
                _Formula.Name = Replace(dr("name"), Chr(10), "")
                _Formula.Source = Replace(dr("source"), Chr(10), "")
                _Formula.Responsibility = dr("responsibility")
                _Formula.Prepared = dr("prepared")
                _Formula.Cutofdate = dr("cutofdate").ToString()
                'If Not IsDBNull(dr("cutofdate")) Then
                '    Dim myDate As Date
                '    myDate = dr("cutofdate")
                '    _Formula.Cutofdate = myDate.Day.ToString("00") & "/" & myDate.Month.ToString("00") & "/" & myDate.Year
                'Else
                '    _Formula.Cutofdate = ""
                'End If
                _FormulaWrap.Add(_Formula)
            End While
            conn.Close()
            Return _FormulaWrap
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try




    End Function

    <WebMethod()> _
   Public Function AddKPIRelation(ByVal _idparent As String, ByVal _idchild As String) As String
        Try
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim query As String = "BEGIN TRANSACTION; BEGIN TRY  "
            query += "insert into kpirelation values ('" & _idparent & "','" & _idchild & "','0')"
            query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;select 0;	end "
            Dim cmd As SqlCommand
            cmd = New SqlCommand(query, conn)
            conn.Open()
            Dim result As String = cmd.ExecuteScalar()
            If result = 0 Then
                conn.Close()
                Return "SUCCESS"
            Else
                conn.Close()
                Return result
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    <WebMethod()> _
   Public Function DelKPIRelation(ByVal _idparent As String, ByVal _idchild As String) As String
        Try
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim query As String = "BEGIN TRANSACTION; BEGIN TRY  delete kpirelation where idparent='" & _idparent & "' and idchild='" & _idchild & "'"
            query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;select 0;	end "
            Dim cmd As SqlCommand
            cmd = New SqlCommand(query, conn)
            conn.Open()
            Dim result As String = cmd.ExecuteScalar()
            If result = 0 Then
                Return "SUCCESS"
            Else
                Return result
            End If
        Catch ex As Exception
            Return ex.Message
        End Try




    End Function

    <WebMethod()> _
   Public Function UpdateKPIRelation(ByVal _idparent As String, ByVal _idchild As String, ByVal _checked As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim myquery As String = ""
            Dim cmd As SqlCommand
            conn.Open()
            Dim query As String = "BEGIN TRANSACTION; BEGIN TRY  UPDATE kpirelation set IncludeCount='" & IIf(_checked = "True", "1", "0") & "' where idparent='" & _idparent & "' and idchild='" & _idchild & "'"
            query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;select 0;	end "
            cmd = New SqlCommand(query, conn)
            Dim result As String = cmd.ExecuteScalar()
            If result = 0 Then
                Return "SUCCESS"
            Else
                Return result
            End If
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
   Public Function DelKPI(ByVal _idparent As String, ByVal _idchild As String, ByVal _nikuser As String, ByVal _hostname As String) As String
        Try
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim cmd As SqlCommand
            Dim stedit As String
            Dim query As String = "select stedit from mkpi where id='" & _idchild & "'"
            cmd = New SqlCommand(query, conn)
            conn.Open()
            stedit = cmd.ExecuteScalar()
            If stedit = "0" Then
                conn.Close()
                Return "ACTIVE"
            Else
                query = "BEGIN TRANSACTION; BEGIN TRY  " & _
                    "INSERT INTO DMKPI SELECT * FROM MKPI WHERE MKPI.ID='" & _idchild & "';" & _
                    "UPDATE DMKPI SET DELETEDBY='" & _nikuser & "',DELETEDTIME=GETDATE(),DELETEDIN='" & _hostname & "' WHERE ID='" & _idchild & "';" & _
                    "delete mkpi where id='" & _idchild & "';" & _
                    "INSERT INTO  DTBSC SELECT * FROM TBSC WHERE TBSC.KPIID='" & _idchild & "';" & _
                    "delete tbsc where kpiid='" & _idchild & "';" & _
                    "INSERT INTO DMKPI SELECT * FROM MKPI WHERE MKPI.ID in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT='" & _idchild & "');" & _
                    "delete mkpi where id in (select idchild from kpirelation where idparent='" & _idchild & "');" & _
                    "INSERT INTO DTBSC SELECT * FROM TBSC WHERE TBSC.KPIID in (SELECT IDCHILD FROM KPIRELATION WHERE IDPARENT='" & _idchild & "');" & _
                    "delete tbsc where kpiid in  (select idchild from kpirelation where idparent='" & _idchild & "');" & _
                    "INSERT INTO DKPIRELATION SELECT * FROM KPIRELATION WHERE KPIRELATION.IDPARENT='" & _idchild & "';" & _
                    "delete kpirelation where idparent='" & _idchild & "';" & _
                    "INSERT INTO DKPIRELATION SELECT * FROM KPIRELATION WHERE KPIRELATION.IDPARENT='" & _idparent & "' and KPIRELATION.IDCHILD='" & _idchild & "';" & _
                    "delete kpirelation where idparent='" & _idparent & "' and idchild='" & _idchild & "'"
                query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;select 0;	end "
                cmd = New SqlCommand(query, conn)
                Dim result As String = cmd.ExecuteScalar()
                If result = 0 Then
                    Return "SUCCESS"
                Else
                    Return result
                End If
            End If
            Return ""
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    <WebMethod()> _
    Public Function GetChild(ByVal id As String, ByVal year As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim cmd As SqlCommand
        Try
            Dim da As SqlDataAdapter
            Dim dt As New DataTable()
            sqlQuery = "select b.id,b.Name,d.month as bulan,isnull(d.achievement,0) as achievement,isnull(d.actual,0) as actual,RedParam,RedCond,GreenParam,greencond,basedon,b.stedit from kpirelation a inner join mkpi b on a.idchild=b.id " & _
                        "left join TBSC d on b.ID=d.kpiid  " & _
                        "where b.id in (select idchild from kpirelation where idparent='" & id & "') and d.year='" & year & "'"
            cmd = New SqlCommand()
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "crosstab2"
            cmd.Parameters.AddWithValue("@SQL", sqlQuery)
            cmd.Parameters.AddWithValue("@tpivotCol", "bulan")
            cmd.Parameters.AddWithValue("@Summaries", "MAX(achievement else null) ACH],Max(Actual else null)")
            cmd.Parameters.AddWithValue("@GroupBy", "id,Name")
            cmd.Parameters.AddWithValue("@OrderBy ", "Name")
            cmd.Parameters.AddWithValue("@OtherFields", "Max(BasedOn) as BasedOn,Max(RedParam) as RedParam,Max(RedCond) as RedCond,Max(GreenParam) as GreenParam,Max(GreenCond) as GreenCond,Max(StEdit) as StEdit")
            cmd.Connection = conn
            conn.Open()
            da = New SqlDataAdapter(cmd)
            da.Fill(dt)
            conn.Close()
            Dim tampung As String = ""
            Dim resultsec As String = ""
            If dt.Rows.Count <> 0 Then
                resultsec = "<table border='1'>"
            End If
            Dim colspan As Integer = 0
            For Each dc As DataColumn In dt.Columns
                Console.WriteLine(dc.ColumnName)
                If IsNumeric(dc.ColumnName) Then
                    colspan += 1
                End If
            Next
            colspan += 1
            For Each dr As DataRow In dt.Rows
                If tampung <> dr("Name") Then
                    tampung = dr("Name")
                    If resultsec <> "" Then
                        resultsec += "</table></div><br/>"
                    End If
                    resultsec += "<a href='#' onclick='javascript:showhidediv(""c" & dr("id") & """,""a" & dr("id") & """)' id='a" & dr("id") & "' style='font-size:14px'>[+] KPI Name : " & tampung & "</a>"
                    resultsec += "<div id='c" & dr("id") & "' style='display:none'>"
                    If dt.Rows.Count <> 0 Then
                        resultsec += "<table style='font-size:14px' border='1'><td colspan='13' align='center'>ACTUAL DATA</td><tr>"
                        For Each dc As DataColumn In dt.Columns
                            If IsNumeric(dc.ColumnName) Then
                                resultsec += "<td>" & doMapping(dc.ColumnName) & "</td>"
                            End If
                        Next
                        resultsec += "</tr>"
                    End If
                End If
              
                If dr("StEdit") = 1 Then 'OFF
                    resultsec += "<tr style=""color:gray"">"
                    resultsec += "<td>" & dr("KdSite") & "</td>"
                    resultsec += "<td colspan=""12"" style=""text-align:center"">OFF</td>"
                Else
                    resultsec += "<tr>"
                    'resultsec += "<td>" & dr("KdSite") & "</td>"
                    For Each dc As DataColumn In dt.Columns
                        If dc.ColumnName.IndexOf("ACH") = -1 And IsNumeric(dc.ColumnName) Then
                            If IsDBNull(dr(dc.ColumnName)) Then
                                resultsec += "<td style='background-color:" & getColor(IIf(dr("BasedOn") = 0, 0, 0), dr("RedCond"), dr("RedParam"), dr("GreenCond"), dr("GreenParam")) & ";'></td>"
                            Else
                                resultsec += "<td style='background-color:" & getColor(IIf(dr("BasedOn") = 0, dr(Trim(dc.ColumnName) & " ACH"), dr(dc.ColumnName.Split(" ")(0))), dr("RedCond"), dr("RedParam"), dr("GreenCond"), dr("GreenParam")) & ";'>" & dr(dc.ColumnName) & "</td>"
                            End If
                        End If

                    Next
                End If
                resultsec += "</tr>"
            Next
            If dt.Rows.Count <> 0 Then
                resultsec += "</table></div><br/>"
            End If
            Return resultsec
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    Function getColor(ByVal value As String, ByVal redCond As String, ByVal redVal As Decimal, ByVal greenCond As String, ByVal greenVal As Decimal) As String
        Try
            Select Case redCond
                Case "0"
                    If value > redVal Then
                        Return "red"
                    End If
                Case "1"
                    If value >= redVal Then
                        Return "red"
                    End If
                Case "2"
                    If value < redVal Then
                        Return "red"
                    End If
                Case "3"
                    If value <= redVal Then
                        Return "red"
                    End If
            End Select
            Select Case greenCond
                Case "0"
                    If value > greenVal Then
                        Return "green"
                    End If
                Case "1"
                    If value >= greenVal Then
                        Return "green"
                    End If
                Case "2"
                    If value < greenVal Then
                        Return "green"
                    End If
                Case "3"
                    If value <= greenVal Then
                        Return "green"
                    End If
            End Select
            Return "yellow"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    <WebMethod()> _
    Public Function SaveKomentar(ByVal id As String, ByVal year As String, ByVal month As String, ByVal PI As String, ByVal createdby As String, ByVal createdin As String, ByVal CA As String, ByVal IDPICA As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim cmd As SqlCommand
        Try
            If Trim(IDPICA) = "" Or Trim(IDPICA) = "null" Then
                sqlQuery = "BEGIN TRANSACTION; BEGIN TRY "
                sqlQuery += "insert into TCOMMENT(year,month,kpiid,createdby,createdin,PI,CA) values " & _
                           "('" & year & "','" & month & "','" & id & "','" & createdby & "','" & createdin & "','" & PI & "','" & CA & "')"
                sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
            Else
                sqlQuery = "BEGIN TRANSACTION; BEGIN TRY "
                sqlQuery += "UPDATE TCOMMENT set PI='" & Replace(PI, "'", "`") & "',CA='" & Replace(CA, "'", "`") & "' " & _
                            ", modifiedby='" & createdby & "',modifiedin='" & createdin & "',modifieddate=getdate() " & _
                            " where id='" & IDPICA & "'"
                sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "

            End If
            cmd = New SqlCommand(sqlQuery, conn)
            conn.Open()
            Dim result As String = ""
            result = cmd.ExecuteScalar
            If result = "0" Then
                Return "SUKSES"
            End If
            Return result
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function GetKomentar(ByVal id As String, ByVal year As String, ByVal month As String, ByVal createdby As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim cmd As SqlCommand
        Try
            sqlQuery = "BEGIN TRANSACTION; BEGIN TRY "
            'sqlQuery += "select * from TCOMMENT t left outer join  OPENROWSET('SQLNCLI','SERVER=172.16.0.7\hris;UID=IICS;PWD=$IICS-123-ONLINE$','SELECT nik, nama from BERP_CENTRAL.dbo.H_A101') aa on t.createdby=aa.nik where year='" & year & "' and month='" & month & "' and kpiid='" & id & "' order by id desc"
            'sqlQuery += "select * from TCOMMENT t left outer join  OPENROWSET('SQLNCLI','SERVER=localhost;UID=sa;PWD=buma12345!@#$%','SELECT nik, nama from REMO.dbo.H_A101') aa on t.createdby=aa.nik where year='" & year & "' and month='" & month & "' and kpiid='" & id & "' order by id desc"
            sqlQuery += "select * from TCOMMENT t left outer join  OPENROWSET('SQLNCLI','SERVER=20.110.1.5;UID=dev;PWD=hrishris','SELECT nik, nama from JRN_TEST.dbo.H_A101') aa on t.createdby=aa.nik where year='" & year & "' and month='" & month & "' and kpiid='" & id & "' order by id desc"
            sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
            cmd = New SqlCommand(sqlQuery, conn)
            conn.Open()
            Dim count As Integer = 0
            Dim header As String
            Dim result As String = ""
            Dim reader As SqlDataReader
            reader = cmd.ExecuteReader()

            result = "<div id='divkomen' style='display:none;font-size:12px;'><table border=""0"" cellspacing=""0"" cellpadding=""0"">"
            If reader.HasRows Then
                While reader.Read
                    count += 1
                    If count = 1 Then
                        result += "<tr valign=""top""><td style=""background-color:gray"">" & reader("nama") & " " & CDate(reader("createddate")).ToString("dd/MM/yyyy hh:mm") & IIf(reader("createdby") = createdby, " <a href=""#"" onclick=""javascript:editcomment(" & reader("id") & ")"">[Edit]</a>", "") & "</td></tr>"
                        result += "<tr valign=""top""><td>PI : <div id=""pi" & reader("id") & """>" & Replace(reader("PI").ToString(), Chr(13), "<br>") & "</div></td>"
                        result += "<tr valign=""top""><td>CA : <div id=""ca" & reader("id") & """>" & Replace(reader("CA").ToString(), Chr(13), "<br>") & "</div></td>"
                    Else
                        result += "<tr><td>&nbsp;</td></tr>"
                        result += "<tr valign=""top""><td style=""background-color:gray"">" & reader("nama") & " " & CDate(reader("createddate")).ToString("dd/MM/yyyy hh:mm") & IIf(reader("createdby") = createdby, " <a href=""#"" onclick=""javascript:editcomment(" & reader("id") & ")"">[Edit]</a>", "") & "</td></tr>"
                        result += "<tr valign=""top""><td>PI : <div id=""pi" & reader("id") & """>" & Replace(reader("PI").ToString(), Chr(13), "<br>") & "</div></td>"
                        result += "<tr valign=""top""><td>CA : <div id=""ca" & reader("id") & """>" & Replace(reader("CA").ToString(), Chr(13), "<br>") & "</div></td>"
                    End If

                End While
            End If
            result += "</table></div>"
            header = "<a href=""#"" style='font-size:12px;' onclick=""showhidedivsucks('divkomen')"">[+] Lihat Semua Komentar (" & count & ")</a>"
            Return header & result
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
 Public Function SaveActivity(ByVal x As List(Of objact), ByVal userid As String, ByVal userip As String, ByVal _param As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim tes As String = ""
        Try
            Dim result As String = ""
            Dim noprogram As Integer = 0
            Dim noactivity As Integer = 0
            Dim notask As Integer = 0
            Dim grup As Integer = 0

            Dim kpiid, myyear, mymonth As String
            Dim nomor As Integer = 1
            'Urutin Nomor Lagi
            For Each c As objact In x
                Select Case c.tipe
                    Case "PROJECT"
                        If c.del = "False" Then
                            c.nomor = nomor
                            c.Grup = nomor
                            grup = nomor
                            nomor += 1
                        End If
                    Case "PROGRAM"
                        If c.del = "False" Then
                            kpiid = c.kpiid
                            c.nomor = nomor
                            c.Grup = grup
                            nomor += 1
                        End If
                    Case "TASK"
                        If c.del = "False" Then
                            c.kpiid = kpiid
                            c.nomor = nomor
                            c.Grup = grup
                            nomor += 1
                        End If
                End Select
            Next
            'sqlQuery = "BEGIN TRANSACTION; BEGIN TRY "
            For Each c As objact In x
                If c.kpiid = "" Then
                    c.kpiid = "0"
                End If
                If c.start = "null" Then
                    c.start = ""
                End If
                If c.enddate = "null" Then
                    c.enddate = ""
                End If
                If c.deliverable = "null" Then
                    c.deliverable = ""
                End If
                If c.del = "False" And c.state = "NEW" Then
                    kpiid = c.kpiid
                    myyear = c.year
                    If c.tipe = "PROJECT" Then
                        sqlQuery += "insert into MACTIVITY(year,kpiid,nomor,jenis,activity,PIC,deliverables,startdate,enddate,createdby,createdin,param,grup) values ('" & _
                                c.year & "','" & c.kpiid & "','" & c.nomor & "','" & c.tipe & "','" & c.activity.Replace("'", "`").Replace(Chr(10), "") & "',''" & _
                                 ",'',NULL,NULL,'" & userid & "','" & userip & "','" & _param & "'," & c.Grup & ");"
                    Else
                        If (c.start = "" Or c.start = "null") And (c.enddate = "" Or c.enddate = "null") Then
                            sqlQuery += "insert into MACTIVITY(year,kpiid,nomor,jenis,activity,PIC,deliverables,startdate,enddate,createdby,createdin,param,grup) values ('" & _
                                    c.year & "','" & c.kpiid & "','" & c.nomor & "','" & c.tipe & "','" & c.activity.Replace("'", "`").Replace(Chr(10), "") & "','" & c.pic & "'" & _
                                     ",'" & c.deliverable.Replace("'", "`").Replace(Chr(10), "") & "',NULL,NULL,'" & userid & "','" & userip & "','" & _param & "'," & c.Grup & ");"
                        ElseIf (c.start = "" Or c.start = "null") Then
                            sqlQuery += "insert into MACTIVITY(year,kpiid,nomor,jenis,activity,PIC,deliverables,startdate,enddate,createdby,createdin,param,grup) values ('" & _
                                    c.year & "','" & c.kpiid & "','" & c.nomor & "','" & c.tipe & "','" & c.activity.Replace("'", "`").Replace(Chr(10), "") & "','" & c.pic & "'" & _
                                     ",'" & c.deliverable.Replace("'", "`").Replace(Chr(10), "") & "',NULL" & _
                                    ",'" & DateTime.Parse(c.enddate, Globalization.CultureInfo.CreateSpecificCulture("en-CA")).ToString("MM/dd/yyyy") & "','" & userid & "','" & userip & "','" & _param & "'," & c.Grup & ");"
                        ElseIf (c.enddate = "" Or c.enddate = "null") Then
                            sqlQuery += "insert into MACTIVITY(year,kpiid,nomor,jenis,activity,PIC,deliverables,startdate,enddate,createdby,createdin,param,grup) values ('" & _
                                    c.year & "','" & c.kpiid & "','" & c.nomor & "','" & c.tipe & "','" & c.activity.Replace("'", "`").Replace(Chr(10), "") & "','" & c.pic & "'" & _
                                     ",'" & c.deliverable.Replace("'", "`").Replace(Chr(10), "") & "','" & DateTime.Parse(c.start, Globalization.CultureInfo.CreateSpecificCulture("en-CA")).ToString("MM/dd/yyyy") & "',NULL,'" & userid & "','" & userip & "','" & _param & "'," & c.Grup & ");"
                        Else
                            sqlQuery += "insert into MACTIVITY(year,kpiid,nomor,jenis,activity,PIC,deliverables,startdate,enddate,createdby,createdin,param,grup) values ('" & _
                                    c.year & "','" & c.kpiid & "','" & c.nomor & "','" & c.tipe & "','" & c.activity.Replace("'", "`").Replace(Chr(10), "") & "','" & c.pic & "'" & _
                                     ",'" & c.deliverable.Replace("'", "`").Replace(Chr(10), "") & "','" & DateTime.Parse(c.start, Globalization.CultureInfo.CreateSpecificCulture("en-CA")).ToString("MM/dd/yyyy") & "','" & DateTime.Parse(c.enddate, Globalization.CultureInfo.CreateSpecificCulture("en-CA")).ToString("MM/dd/yyyy") & "','" & userid & "','" & userip & "','" & _param & "'," & c.Grup & ");"
                        End If
                    End If
                ElseIf c.del = "True" And c.state <> "NEW" Then
                    sqlQuery += "INSERT INTO DMACTIVITY SELECT *,'" & userid & "','" & userip & "',getdate() FROM MACTIVITY where id='" & c.state & "';"
                    'sqlQuery += "UPDATE DMACTIVITY set deletedby='" & userid & "', deletedin='" & userip & "', deletedtime=getdate() where id='" & c.state & "';"
                    sqlQuery += "delete MACTIVITY where id='" & c.state & "';"
                    sqlQuery += "delete mactivitysecure where activityid='" & c.state & "';"
                ElseIf c.del = "False" And c.state <> "NEW" Then
                    If c.tipe = "PROJECT" Then
                        sqlQuery += "Update MACTIVITY set grup=" & c.Grup & ",modifieddate=getdate(),modifiedby='" & userid & "',modifiedin='" & userip & "',nomor='" & c.nomor & "', activity='" & c.activity.Replace("'", "`").Replace(Chr(10), "") & "' " & _
                              " where id='" & c.state & "'"
                    Else
                        If c.start = "" And c.enddate = "" Then
                            sqlQuery += "Update MACTIVITY set grup=" & c.Grup & ", modifieddate=getdate(),modifiedby='" & userid & "',modifiedin='" & userip & "', nomor='" & c.nomor & "',pic='" & c.pic & "', activity='" & c.activity.Replace("'", "`").Replace(Chr(10), "") & "'," & _
                                    "deliverables='" & c.deliverable.Replace("'", "`").Replace(Chr(10), "") & "',startdate=null,enddate=null,kpiid='" & c.kpiid & "' where id='" & c.state & "'"
                        ElseIf c.start = "" Then
                            sqlQuery += "Update MACTIVITY set grup=" & c.Grup & ",modifieddate=getdate(),modifiedby='" & userid & "',modifiedin='" & userip & "',nomor='" & c.nomor & "',pic='" & c.pic & "', activity='" & c.activity.Replace("'", "`").Replace(Chr(10), "") & "'," & _
                                    "deliverables='" & c.deliverable.Replace("'", "`").Replace(Chr(10), "") & "',startdate=null,enddate='" & DateTime.Parse(c.enddate, Globalization.CultureInfo.CreateSpecificCulture("en-CA")).ToString("MM/dd/yyyy") & "',kpiid='" & c.kpiid & "'  where id='" & c.state & "'"
                        ElseIf c.enddate = "" Then
                            sqlQuery += "Update MACTIVITY set grup=" & c.Grup & ",modifieddate=getdate(),modifiedby='" & userid & "',modifiedin='" & userip & "',nomor='" & c.nomor & "',pic='" & c.pic & "', activity='" & c.activity.Replace("'", "`").Replace(Chr(10), "") & "'," & _
                                    "deliverables='" & c.deliverable.Replace("'", "`").Replace(Chr(10), "") & "',startdate='" & DateTime.Parse(c.start, Globalization.CultureInfo.CreateSpecificCulture("en-CA")).ToString("MM/dd/yyyy") & "',enddate=null,kpiid='" & c.kpiid & "'  where id='" & c.state & "'"
                        Else
                            sqlQuery += "Update MACTIVITY set grup=" & c.Grup & ",modifieddate=getdate(),modifiedby='" & userid & "',modifiedin='" & userip & "',nomor='" & c.nomor & "', pic='" & c.pic & "', activity='" & c.activity.Replace("'", "`").Replace(Chr(10), "") & "'," & _
                                "deliverables='" & c.deliverable.Replace("'", "`").Replace(Chr(10), "") & "',startdate='" & DateTime.Parse(c.start, Globalization.CultureInfo.CreateSpecificCulture("en-CA")).ToString("MM/dd/yyyy") & "',enddate='" & DateTime.Parse(c.enddate, Globalization.CultureInfo.CreateSpecificCulture("en-CA")).ToString("MM/dd/yyyy") & "',kpiid='" & c.kpiid & "'  where id='" & c.state & "'"
                        End If
                    End If
                End If
            Next
            'sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
            If sqlQuery = "" Then
                Return "NODATA"
            Else
                sqlQuery = "BEGIN TRANSACTION; BEGIN TRY " + sqlQuery
                sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
            End If
            Dim cmd As New SqlCommand(sqlQuery, conn)
            conn.Open()
            result = cmd.ExecuteScalar()
            conn.Close()
            If result = 0 Then
                Return "SUKSES"
            Else
                Return result
            End If
            Return Nothing
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
 Public Function DeleteTask(ByVal id As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Try
            Dim result As String = ""
            sqlQuery = "BEGIN TRANSACTION; BEGIN TRY "
            sqlQuery += "delete MACTIVITY where id='" & id & "'"
            sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
            Dim cmd As New SqlCommand(sqlQuery, conn)
            conn.Open()
            result = cmd.ExecuteScalar()
            conn.Close()
            If result = 0 Then
                Return "SUKSES"
            Else
                Return result
            End If
            Return Nothing
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function RetrieveActivity(ByVal _param As String, ByVal _year As String) As List(Of objact)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim param() As String
            Dim sqlQuery As String = ""
            param = _param.Split("#")
            'sqlQuery = "select a.*,isnull(x.nama,'') as nama,isnull(b.name,'') as name,ISNULL(total.totalsecure,0) as totalsecure from MACTIVITY a left join mkpi b on a.kpiid=b.id left outer join  OPENROWSET('SQLNCLI','SERVER=172.16.0.7\hris;UID=IICS;PWD=$IICS-123-ONLINE$','SELECT pernr as nik, cname as nama,pnalt from BERP_CENTRAL.dbo.H_A101_SAP') x on (a.pic=x.nik or a.pic=x.pnalt) and a.pic <> '' left join (select activityid,count(nik) as totalsecure from MACTIVITYSECURE where activityid in (select id from MACTIVITY where param='" & _param & "' and year='" & _year & "')group by activityid) total on a.id=total.activityid where param='" & _param & "' and a.year='" & _year & "' order by nomor"
            sqlQuery = "select a.*,isnull(x.nama,'') as nama,isnull(b.name,'') as name,ISNULL(total.totalsecure,0) as totalsecure from MACTIVITY a left join mkpi b on a.kpiid=b.id left outer join  OPENROWSET('SQLNCLI','SERVER=20.110.1.5;UID=dev;PWD=hrishris','SELECT  nik,  nama,'' as pnalt from JRN_TEST.dbo.H_A101') x on (a.pic=x.nik ) and a.pic <> '' left join (select activityid,count(nik) as totalsecure from MACTIVITYSECURE where activityid in (select id from MACTIVITY where param='" & _param & "' and year='" & _year & "')group by activityid) total on a.id=total.activityid where param='" & _param & "' and a.year='" & _year & "' order by nomor"

            Dim da As New SqlDataAdapter(sqlQuery, conn)
            Dim dt As New DataTable()
            da.Fill(dt)
            Dim _objWrap As New List(Of objact)
            For Each dr As DataRow In dt.Rows
                Dim obj As New objact()
                obj.id = dr("id")
                obj.nomor = dr("nomor") - 1
                obj.tipe = dr("jenis")
                obj.activity = dr("activity")
                obj.pic = dr("pic")
                obj.namapic = dr("nama")
                obj.deliverable = dr("deliverables")
                obj.kpiid = dr("kpiid")
                If Not IsDBNull(dr("startdate")) Then
                    Dim myDate As Date
                    myDate = dr("startdate")
                    obj.start = myDate.Day.ToString("00") & "/" & myDate.Month.ToString("00") & "/" & myDate.Year
                Else
                    obj.start = ""
                End If
                If Not IsDBNull(dr("enddate")) Then
                    Dim myDate As Date
                    myDate = dr("enddate")
                    obj.enddate = myDate.Day.ToString("00") & "/" & myDate.Month.ToString("00") & "/" & myDate.Year
                Else
                    obj.enddate = ""
                End If
                obj.state = dr("id")
                obj.kpiname = dr("name")
                obj.totalsecure = dr("totalsecure")
                _objWrap.Add(obj)
            Next
            Return _objWrap
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

    End Function

    <WebMethod()> _
    Public Function RetTActivity(ByVal kpiid As String, ByVal year As String, ByVal week As String) As List(Of tobjact)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim sqlQuery As String = ""
            Dim weekbefore As Integer = 52
            If week <> 1 Then
                weekbefore = CInt(week) - 1
            End If
            If Not kpiid = "" Then
                sqlQuery = "select xx.*,dd.progress as progressbefore,x.*  from (" & _
                            "select ISNULL(b.week,'" & week & "') as week,a.id as ida,b.id as idb, a.nomor,a.jenis,a.activity,a.pic,a.deliverables,a.startdate,a.enddate,b.progress,b.accuracy,b.pi,b.ca from MACTIVITY a left join TACTIVITY b on a.id=b.id and b.week='" & week & "' where a.kpiid='" & kpiid & "' and YEAR='" & year & "') xx left join (" & _
                            "select a.id as ida,b.id as idb, a.nomor,a.jenis,a.activity,a.pic,a.deliverables,a.startdate,a.enddate,b.progress,b.accuracy,b.pi,b.ca from MACTIVITY a left join TACTIVITY b on a.id=b.id where a.kpiid='" & kpiid & "' and YEAR='" & year & "' and b.week='" & weekbefore & "' ) dd on xx.ida=dd.ida left outer join  OPENROWSET('SQLNCLI','SERVER=20.110.1.5;UID=dev;PWD=hrishris','SELECT  nik,  nama, '' as pnalt from JRN_TEST.dbo.H_A101') x on (xx.pic=x.nik ) and xx.pic <> '' where xx.week='" & week & "' order by nomor"
                'sqlQuery = "select xx.*,dd.progress as progressbefore,x.*  from (" & _
                '            "select ISNULL(b.week,'" & week & "') as week,a.id as ida,b.id as idb, a.nomor,a.jenis,a.activity,a.pic,a.deliverables,a.startdate,a.enddate,b.progress,b.accuracy,b.pi,b.ca from MACTIVITY a left join TACTIVITY b on a.id=b.id and b.week='" & week & "' where a.kpiid='" & kpiid & "' and YEAR='" & year & "') xx left join (" & _
                '            "select a.id as ida,b.id as idb, a.nomor,a.jenis,a.activity,a.pic,a.deliverables,a.startdate,a.enddate,b.progress,b.accuracy,b.pi,b.ca from MACTIVITY a left join TACTIVITY b on a.id=b.id where a.kpiid='" & kpiid & "' and YEAR='" & year & "' and b.week='" & weekbefore & "' ) dd on xx.ida=dd.ida left outer join  OPENROWSET('SQLNCLI','SERVER=localhost;UID=sa;PWD=buma12345!@#$%','SELECT nik, nama from REMO.dbo.H_A101') x on xx.pic=x.nik where xx.week='" & week & "' order by nomor"

                Dim da As New SqlDataAdapter(sqlQuery, conn)
                Dim dt As New DataTable()
                da.Fill(dt)
                Dim _tobjWrap As New List(Of tobjact)
                For Each dr As DataRow In dt.Rows
                    Dim obj As New tobjact()
                    obj.id = ""
                    obj.nomor = dr("nomor")
                    obj.tipe = dr("jenis")
                    obj.activity = dr("activity")
                    obj.pic = dr("pic")
                    obj.namapic = dr("nama")
                    obj.deliverable = dr("deliverables")
                    If Not IsDBNull(dr("startdate")) Then
                        Dim myDate As Date
                        myDate = dr("startdate")
                        obj.start = myDate.Day.ToString("00") & "/" & myDate.Month.ToString("00") & "/" & myDate.Year
                    Else
                        obj.start = ""
                    End If
                    If Not IsDBNull(dr("enddate")) Then
                        Dim myDate As Date
                        myDate = dr("enddate")
                        obj.enddate = myDate.Day.ToString("00") & "/" & myDate.Month.ToString("00") & "/" & myDate.Year
                    Else
                        obj.enddate = ""
                    End If
                    obj.state = dr("ida")
                    obj.week = dr("week")
                    If IsDBNull(dr("progress")) Then
                        obj.progress = ""
                    Else
                        obj.progress = dr("progress")
                    End If
                    If IsDBNull(dr("accuracy")) Then
                        obj.accuracy = ""
                    Else
                        obj.accuracy = dr("accuracy")
                    End If
                    If IsDBNull(dr("pi")) Then
                        obj.pi = ""
                    Else
                        obj.pi = dr("pi")
                    End If
                    If IsDBNull(dr("ca")) Then
                        obj.ca = ""
                    Else
                        obj.ca = dr("ca")
                    End If
                    If IsDBNull(dr("progressbefore")) Then
                        obj.progressbefore = 0
                    Else
                        obj.progressbefore = dr("progressbefore")
                    End If
                    If IsDBNull(dr("idb")) Then
                        obj.tstate = "NEW"
                    Else
                        obj.tstate = dr("idb")
                    End If
                    _tobjWrap.Add(obj)
                Next
                Return _tobjWrap
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    Function doMapping(ByVal val As String) As String
        Select Case val
            Case "1"
                Return "JAN"
            Case "2"
                Return "FEB"
            Case "3"
                Return "MAR"
            Case "4"
                Return "APR"
            Case "5"
                Return "MEI"
            Case "6"
                Return "JUN"
            Case "7"
                Return "JUL"
            Case "8"
                Return "AGU"
            Case "9"
                Return "SEP"
            Case "10"
                Return "OKT"
            Case "11"
                Return "NOV"
            Case "12"
                Return "DES"
        End Select
        Return ""
    End Function

    <WebMethod()> _
 Public Function SaveTActivity(ByVal x As List(Of tobjact), ByVal userid As String, ByVal userip As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim result As String = ""
            Dim sqlQuery As String = ""
            sqlQuery = "BEGIN TRANSACTION; BEGIN TRY "
            For Each c As tobjact In x
                If c.tstate = "NEW" Then
                    sqlQuery += "insert into TACTIVITY (id,week,progress,accuracy,PI,CA,createdby,createdin) values (" & _
                                c.id & ",'" & c.week & "','" & c.progress.Replace("'", "`") & "','" & c.accuracy & "','" & c.pi.Replace("'", "`").Replace(Chr(10), "") & "','" & c.ca.Replace("'", "`").Replace(Chr(10), "") & "','" & userid & "','" & userip & "')"
                Else
                    sqlQuery += "Update TACTIVITY set modifiedby='" & userid & "',modifieddate=getdate(),modifiedin='" & userip & "',progress='" & c.progress.Replace("'", "`") & "',accuracy='" & c.accuracy & "', PI='" & c.pi.Replace("'", "`").Replace(Chr(10), "") & "'," & _
        "ca='" & c.ca.Replace("'", "`").Replace(Chr(10), "") & "' where id='" & c.tstate & "' and week='" & c.week & "'"
                End If
            Next
            sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
            Dim cmd As New SqlCommand(sqlQuery, conn)
            conn.Open()
            result = cmd.ExecuteScalar()
            conn.Close()

            If result = 0 Then
                Return "SUKSES"
            Else
                Return result
            End If
            Return Nothing
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
 Public Function GetActivityPlanForKPI(ByVal id As String, ByVal year As String, ByVal month As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim o As Object = Nothing
            Dim maxweek As String = ""
            Dim sqlQuery As String = ""
            Dim result As String = ""
            Dim progress As String
            Dim jumlahtask As Decimal
            Dim progresstask As Decimal
            sqlQuery = "select max(isnull(b.week,0)) from MACTIVITY a inner join TACTIVITY b on a.id=b.id where kpiid='" & id & "' and a.year='" & year & "'"
            Dim cmd As New SqlCommand(sqlQuery, conn)
            conn.Open()
            o = cmd.ExecuteScalar()
            conn.Close()
            If IsDBNull(o) Then
                result += "<table border='1' cellpadding='0' cellspacing='0' width='100%'>"
                result += "<tr><td colspan='2' style='background-color:#D9D9D9;'>ACTIVITY PLAN</td></tr>"
                result += "<tr><td colspan='2'>Tidak ada data</td></tr>"
                result += "</table>"
                Return result
            Else
                maxweek = o.ToString()
            End If
            If maxweek = 0 Then
                result += "<table border='1' cellpadding='0' cellspacing='0'>"
                result += "<tr><td colspan='2' style='background-color:#00CC00;'>ACTIVITY PLAN</td></tr>"
                result += "<tr><td colspan='2'>Tidak ada data</td></tr>"
                result += "</table>"
                Return result
            Else
                result += "<table border='1' cellpadding='0' cellspacing='0'>"
                result += "<tr><td colspan='2' style='background-color:#00CC00;'>ACTIVITY PLAN WEEK " & maxweek & "</td></tr>"
                result += "<tr><td>PROGRAM</td><td>% Progress</td></tr>"
                sqlQuery = "select xx.*,x.*  from (select ISNULL(b.week,'" & maxweek & "') as week,a.id as ida,b.id as idb, a.nomor,a.jenis,a.activity,a.pic,a.deliverables,a.startdate,a.enddate,b.progress,b.accuracy,b.pi,b.ca from MACTIVITY a left join TACTIVITY b on a.id=b.id and b.week='" & maxweek & "' where a.kpiid='" & id & "' and YEAR='" & year & "') xx left outer join  OPENROWSET('SQLNCLI','SERVER=20.110.1.5;UID=dev;PWD=hrishris','SELECT nik, nama from JRN_TEST.dbo.H_A101') x on xx.pic=x.nik where xx.week='" & maxweek & "' order by nomor"

                'sqlQuery = "select xx.*,x.*  from (select ISNULL(b.week,'" & maxweek & "') as week,a.id as ida,b.id as idb, a.nomor,a.jenis,a.activity,a.pic,a.deliverables,a.startdate,a.enddate,b.progress,b.accuracy,b.pi,b.ca from MACTIVITY a left join TACTIVITY b on a.id=b.id and b.week='" & maxweek & "' where a.kpiid='" & id & "' and YEAR='" & year & "') xx left outer join  OPENROWSET('SQLNCLI','SERVER=172.16.0.7\hris;UID=IICS;PWD=$IICS-123-ONLINE$','SELECT nik, nama from BERP_CENTRAL.dbo.H_A101') x on xx.pic=x.nik where xx.week='" & maxweek & "' order by nomor"
                'sqlQuery = "select xx.*,x.*  from (select ISNULL(b.week,'" & maxweek & "') as week,a.id as ida,b.id as idb, a.nomor,a.jenis,a.activity,a.pic,a.deliverables,a.startdate,a.enddate,b.progress,b.accuracy,b.pi,b.ca from MACTIVITY a left join TACTIVITY b on a.id=b.id and b.week='" & maxweek & "' where a.kpiid='" & id & "' and YEAR='" & year & "') xx left outer join  OPENROWSET('SQLNCLI','SERVER=localhost;UID=sa;PWD=buma12345!@#$%','SELECT nik, nama from REMO.dbo.H_A101') x on xx.pic=x.nik where xx.week='" & maxweek & "' order by nomor"
                Dim da As New SqlDataAdapter(sqlQuery, conn)
                Dim dt As New DataTable()
                da.Fill(dt)
                For Each dr As DataRow In dt.Rows
                    Select Case dr("jenis")
                        Case "PROGRAM"
                            If jumlahtask <> 0 Then
                                result = result.Replace("???", progresstask / jumlahtask)
                            End If
                            jumlahtask = 0
                            progresstask = 0
                            result += "<tr><td><a href=""#"" onclick=""javascript:viewdetailprogram('" & id & "','" & year & "','" & maxweek & "','" & dr("nomor") & "')"">" & dr("activity") & "</a></td><td>???</td></tr>"
                        Case "ACTIVITY"
                            jumlahtask += 1
                            If Not IsDBNull(dr("progress")) Then
                                progresstask += dr("progress")
                            End If
                            'Case "TASK"
                            '    jumlahtask += 1
                            '    If Not IsDBNull(dr("progress")) Then
                            '        progresstask += dr("progress")
                            '    End If
                    End Select
                Next
                If jumlahtask <> 0 Then
                    result = result.Replace("???", progresstask / jumlahtask)
                End If
                result = result.Replace("???", 0)
                result += "</table>"
                Return result
            End If
            Return Nothing
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function RetTActivityPerProgram(ByVal kpiid As String, ByVal year As String, ByVal week As String, ByVal nomor As String) As List(Of tobjact)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim sqlQuery As String = ""
            Dim weekbefore As Integer = 52
            If week <> 1 Then
                weekbefore = CInt(week) - 1
            End If
            If Not kpiid = "" Then

                sqlQuery = "select xx.*,x.*  from (select ISNULL(b.week,'" & week & "') as week,a.id as ida,b.id as idb, a.nomor,a.jenis,a.activity,a.pic,a.deliverables,a.startdate,a.enddate,b.progress,b.accuracy,b.pi,b.ca from MACTIVITY a left join TACTIVITY b on a.id=b.id and b.week='" & week & "' where a.kpiid='" & kpiid & "' and YEAR='" & year & "') xx left outer join  OPENROWSET('SQLNCLI','SERVER=20.110.1.5;UID=dev;PWD=hrishris','SELECT nik, nama from JRN_TEST.dbo.H_A101') x on xx.pic=x.nik where xx.week='" & week & "' and LEFT(nomor," & nomor.Length & ")='" & nomor & "' order by nomor"
                'sqlQuery = "select xx.*,x.*  from (select ISNULL(b.week,'" & week & "') as week,a.id as ida,b.id as idb, a.nomor,a.jenis,a.activity,a.pic,a.deliverables,a.startdate,a.enddate,b.progress,b.accuracy,b.pi,b.ca from MACTIVITY a left join TACTIVITY b on a.id=b.id and b.week='" & week & "' where a.kpiid='" & kpiid & "' and YEAR='" & year & "') xx left outer join  OPENROWSET('SQLNCLI','SERVER=172.16.0.7\hris;UID=IICS;PWD=$IICS-123-ONLINE$','SELECT nik, nama from BERP_CENTRAL.dbo.H_A101') x on xx.pic=x.nik where xx.week='" & week & "' and LEFT(nomor," & nomor.Length & ")='" & nomor & "' order by nomor"
                'sqlQuery = "select xx.*,x.*  from (select ISNULL(b.week,'" & week & "') as week,a.id as ida,b.id as idb, a.nomor,a.jenis,a.activity,a.pic,a.deliverables,a.startdate,a.enddate,b.progress,b.accuracy,b.pi,b.ca from MACTIVITY a left join TACTIVITY b on a.id=b.id and b.week='" & week & "' where a.kpiid='" & kpiid & "' and YEAR='" & year & "') xx left outer join  OPENROWSET('SQLNCLI','SERVER=localhost;UID=sa;PWD=buma12345!@#$%','SELECT nik, nama from BERP_CENTRAL.dbo.H_A101') x on xx.pic=x.nik where xx.week='" & week & "' and LEFT(nomor," & nomor.Length & ")='" & nomor & "' order by nomor"
                Dim da As New SqlDataAdapter(sqlQuery, conn)
                Dim dt As New DataTable()
                da.Fill(dt)
                Dim _tobjWrap As New List(Of tobjact)
                For Each dr As DataRow In dt.Rows
                    Dim obj As New tobjact()
                    obj.id = ""
                    obj.nomor = dr("nomor")
                    obj.tipe = dr("jenis")
                    obj.activity = dr("activity")
                    obj.pic = dr("pic")
                    obj.namapic = dr("nama")
                    obj.deliverable = dr("deliverables")
                    If Not IsDBNull(dr("startdate")) Then
                        obj.start = Format(dr("startdate"), "dd/MM/yyyy")
                    Else
                        obj.start = ""
                    End If
                    If Not IsDBNull(dr("enddate")) Then
                        obj.enddate = Format(dr("enddate"), "dd/MM/yyyy")
                    Else
                        obj.enddate = ""
                    End If
                    obj.state = dr("ida")
                    obj.week = dr("week")
                    If IsDBNull(dr("progress")) Then
                        obj.progress = ""
                    Else
                        obj.progress = dr("progress")
                    End If
                    If IsDBNull(dr("accuracy")) Then
                        obj.accuracy = ""
                    Else
                        obj.accuracy = dr("accuracy")
                    End If
                    If IsDBNull(dr("pi")) Then
                        obj.pi = ""
                    Else
                        obj.pi = dr("pi")
                    End If
                    If IsDBNull(dr("ca")) Then
                        obj.ca = ""
                    Else
                        obj.ca = dr("ca")
                    End If
                    'If IsDBNull(dr("progressbefore")) Then
                    '    obj.progressbefore = 0
                    'Else
                    '    obj.progressbefore = dr("progressbefore")
                    'End If
                    If IsDBNull(dr("idb")) Then
                        obj.tstate = "NEW"
                    Else
                        obj.tstate = dr("idb")
                    End If
                    _tobjWrap.Add(obj)
                Next
                Return _tobjWrap
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function SaveBSC(ByVal mybsc As List(Of BSC), ByVal userid As String, ByVal userip As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim result As String = ""
            Dim sqlQuery As String = ""
            sqlQuery = "BEGIN TRANSACTION; BEGIN TRY "
            For Each c As BSC In mybsc
                If c.Target = "" Or c.Target = "null" Then
                    sqlQuery += "update tbsc set modifieddate=getdate(),modifiedby='" & userid & "',modifiedin='" & userip & "',target=NULL,actual=NULL" & _
                                  ",achievement=NULL where kpiid='" & c.Kpiid & "' " & _
                                  "and year='" & c.Year & _
                                  "' and month='" & c.Month & "';"
                ElseIf c.Actual = -88.314098314 Then
                    sqlQuery += "update tbsc set modifieddate=getdate(),modifiedby='" & userid & "',modifiedin='" & userip & "',target=" & CDec(c.Target) & ",actual=NULL" & _
                                 ",achievement=NULL where kpiid='" & c.Kpiid & "' " & _
                                 "and year='" & c.Year & _
                                 "' and month='" & c.Month & "';"
                Else
                    sqlQuery += "update tbsc set modifieddate=getdate(),modifiedby='" & userid & "',modifiedin='" & userip & "',target=" & CDec(c.Target) & ",actual=" & _
                                  CDec(c.Actual) & ",achievement=" & CDec(c.Achievement) & " where kpiid='" & c.Kpiid & "' " & _
                                  "and year='" & c.Year & _
                                  "' and month='" & c.Month & "';"
                End If
            Next
            sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
            Dim cmd As New SqlCommand(sqlQuery, conn)
            conn.Open()
            result = cmd.ExecuteScalar()
            conn.Close()
            If result = 0 Then
                Return "SUKSES"
            Else
                Return result
            End If
            Return Nothing
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function retrievep(ByVal level As String, ByVal year As String, ByVal funcid As String) As List(Of TP)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim query As String = ""
        Try
            Select Case level
                Case "0"
                    query = "select a.id,a.perspective,b.value,b.id as tid from MPERSPECTIVE a left join (select * from TPERSPECTIVE where YEAR='" & year & "' and level='" & level & "') b on a.id=b.pid"
                Case "1"
                    query = "select a.id,a.perspective,b.value,b.id as tid from MPERSPECTIVE a left join (select * from TPERSPECTIVE where YEAR='" & year & "' and level='" & level & "' and dirid='" & funcid & "') b on a.id=b.pid"
                Case "4"
                    query = "select a.id,a.perspective,b.value,b.id as tid from MPERSPECTIVE a left join (select * from TPERSPECTIVE where YEAR='" & year & "' and level='" & level & "' and dirid='" & funcid & "') b on a.id=b.pid"
            End Select
            Dim da As New SqlDataAdapter(query, conn)
            Dim dt As New DataTable()
            Dim lst As New List(Of TP)
            Dim tp As TP
            da.Fill(dt)
            For i As Integer = 0 To dt.Rows.Count - 1
                tp = New TP()
                tp.Pid = dt.Rows(i)("id")
                tp.Pname = dt.Rows(i)("perspective")
                If IsDBNull(dt.Rows(i)("value")) Then
                    tp.Pbobot = 0
                    tp.Pstate = "NEW"
                Else
                    tp.Pbobot = dt.Rows(i)("value")
                    tp.Pstate = dt.Rows(i)("tid")
                End If
                lst.Add(tp)
            Next
            Return lst
        Catch ex As Exception
            Return Nothing
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function savep(ByVal level As String, ByVal year As String, ByVal funcid As String, ByVal p As List(Of TP)) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim query As String = ""
        Try
            query = "BEGIN TRANSACTION; BEGIN TRY "
            Select Case level
                Case "0"
                    For Each o As TP In p
                        If o.Pstate = "NEW" Then
                            query += "insert into tperspective (year,level,pid,value) values ('" & _
                                     year & "','" & level & "','" & o.Pid & "'," & o.Pbobot & ");"
                        Else
                            query += "update tperspective set value=" & o.Pbobot & " where id=" & o.Pstate & ";"
                        End If
                    Next
                Case "1"
                    For Each o As TP In p
                        If o.Pstate = "NEW" Then
                            query += "insert into tperspective (year,level,pid,value,dirid) values ('" & _
                                     year & "','" & level & "','" & o.Pid & "'," & o.Pbobot & ",'" & funcid & "');"
                        Else
                            query += "update tperspective set value=" & o.Pbobot & " where id=" & o.Pstate & ";"
                        End If
                    Next
                Case "4"
                    For Each o As TP In p
                        If o.Pstate = "NEW" Then
                            query += "insert into tperspective (year,level,pid,value,dirid) values ('" & _
                                     year & "','" & level & "','" & o.Pid & "'," & o.Pbobot & ",'" & funcid & "');"
                        Else
                            query += "update tperspective set value=" & o.Pbobot & " where id=" & o.Pstate & ";"
                        End If
                    Next
            End Select
            query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "

            Dim cmd As New SqlCommand(query, conn)
            Dim result As String
            conn.Open()
            result = cmd.ExecuteScalar
            conn.Close()
            Return result
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function ApproveBSC(ByVal level As String, ByVal year As String, ByVal month As String, ByVal dir As String, ByVal func As String, ByVal sec As String, ByVal jobsite As String, ByVal user As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim cmd As SqlCommand
        Dim result As String
        Try
            sqlQuery = "BEGIN TRANSACTION; BEGIN TRY "
            Select Case level
                Case "0"
                    sqlQuery += "insert into TBSCAPPR(level,year,month,approvedby,approveddate) values ('" & level & "','" & year & "','" & month & "','" & user & "',getdate());"
                    sqlQuery += "update TBSC set approved=1 where kpiid in (select id from mkpi where level='0') and year='" & year & "' and month='" & month & "';"
                Case "1"
                    sqlQuery += "insert into TBSCAPPR(level,year,month,func,approvedby,approveddate) values ('" & level & "','" & year & "','" & month & "','" & func & "','" & user & "',getdate());"
                    sqlQuery += "update TBSC set approved=1 where kpiid in (select id from mkpi where level='1' and func='" & func & "') and year='" & year & "' and month='" & month & "';"
                Case "2"
                    sqlQuery += "insert into TBSCAPPR(level,year,month,jobsite,section,approvedby,approveddate) values ('" & level & "','" & year & "','" & month & "','" & jobsite & "','" & sec & "','" & user & "',getdate());"
                    sqlQuery += "update TBSC set approved=1 where kpiid in (select id from mkpi where level='2' and jobsite='" & jobsite & "' and section='" & sec & "') and year='" & year & "' and month='" & month & "';"
                Case "3"
                    sqlQuery += "insert into TBSCAPPR(level,year,month,jobsite,approvedby,approveddate) values ('" & level & "','" & year & "','" & month & "','" & jobsite & "','" & user & "',getdate());"
                    sqlQuery += "update TBSC set approved=1 where kpiid in (select id from mkpi where level='3' and jobsite='" & jobsite & "') and year='" & year & "' and month='" & month & "';"
                Case "4"
                    sqlQuery += "insert into TBSCAPPR(level,year,month,dir,approvedby,approveddate) values ('" & level & "','" & year & "','" & month & "','" & dir & "','" & user & "',getdate());"
                    sqlQuery += "update TBSC set approved=1 where kpiid in (select id from mkpi where level='4' and dir='" & dir & "') and year='" & year & "' and month='" & month & "';"
            End Select
            sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
            cmd = New SqlCommand(sqlQuery, conn)
            conn.Open()
            result = cmd.ExecuteScalar()
            conn.Close()
            Return result
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Function ShowLampiran(ByVal year As String, ByVal month As String, ByVal kpiid As String) As List(Of BSCLampiran)
        Dim aLampiran As List(Of BSCLampiran) = New List(Of BSCLampiran)

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT Nomor,nmfile,tglupload FROM TBSCLAMPIRAN WHERE year = " + year + " AND month=" + month + " AND "
            squery = squery + " kpiid = " + kpiid + " order by tglupload desc"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            Dim bsclamp As BSCLampiran
            Do While sdr.Read()
                bsclamp = New BSCLampiran
                Dim x As String = sdr("Nomor")
                Dim dummy As String = ""
                For i As Integer = 1 To Len(x)
                    dummy += MAPNUMBER(Mid(x, i, 1))
                Next
                bsclamp.Nomor = "'" & RandomString(5, False) & dummy & "'"
                bsclamp.NmFile = sdr("nmfile") & " [Uploaded Time :" & Format(sdr("tglupload"), "dd/MM/yyyy hh:mm") & "]"
                aLampiran.Add(bsclamp)
            Loop
            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try
        Return aLampiran
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Function DelLampiran(ByVal nomor As String, ByVal ipcomputer As String, ByVal userid As String) As String

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try

            Dim squery As String = ""

            Dim n As String = nomor
            Dim dummy As String = ""
            n = Right(n, Len(n) - 5)
            For i As Integer = 1 To Len(n)
                dummy += MAPBALIKNUMBER(Mid(n, i, 1))
            Next
            squery = "BEGIN TRANSACTION; BEGIN TRY  "
            squery += "INSERT INTO DTBSCLAMPIRAN (nomor, year, month, kpiid, nmfile, tglupload, mimetype, mimelength, mimedata, createdby, createdin) SELECT * FROM TBSCLAMPIRAN WHERE NOMOR=" + dummy + ";UPDATE DTBSCLAMPIRAN set DeletedTime=getdate(), DeletedIn='" & ipcomputer & "', DeletedBy='" & userid & "' where nomor=" + dummy & ";DELETE TBSCLAMPIRAN WHERE nomor = " + dummy
            squery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;select 0;	end "

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            conn.Open()
            scom.ExecuteReader()
            conn.Close()
        Catch eks As Exception
            conn.Close()
            Return eks.ToString()
        End Try
        Return "SUCCESS"
    End Function

    Public Shared Function Encrypt(ByVal plaintext As String, Optional ByVal password As String = saltkey) As String
        Dim rijndaelCipher As New RijndaelManaged()
        rijndaelCipher.Padding = PaddingMode.PKCS7

        Dim plaintextByte As Byte() = System.Text.Encoding.Unicode.GetBytes(plaintext)
        Dim saltByte As Byte() = Encoding.ASCII.GetBytes(password)
        Dim secretKey As New Rfc2898DeriveBytes(password, saltByte)
        Dim encryptor As ICryptoTransform = rijndaelCipher.CreateEncryptor(secretKey.GetBytes(32), secretKey.GetBytes(16))
        Dim memoryStream As New MemoryStream()
        Dim cryptoStream As New CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)

        cryptoStream.Write(plaintextByte, 0, plaintextByte.Length)
        cryptoStream.FlushFinalBlock()

        Dim cipherBytes As Byte() = memoryStream.ToArray()

        memoryStream.Close()
        cryptoStream.Close()
        encryptor.Dispose()

        Return Convert.ToBase64String(cipherBytes)
    End Function

    Public Shared Function Decrypt(ByVal cipher As String, Optional ByVal password As String = saltkey) As String
        Dim ciphertext As String
        ciphertext = cipher

        Try
            Dim rijndaelCipher As New RijndaelManaged()
            rijndaelCipher.Padding = PaddingMode.PKCS7

            Dim ciphertextByte As Byte() = Convert.FromBase64String(ciphertext)
            Dim saltByte As Byte() = Encoding.ASCII.GetBytes(password)

            Dim secretKey As New Rfc2898DeriveBytes(password, saltByte)
            Dim decryptor As ICryptoTransform = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16))
            Dim memoryStream As New MemoryStream(ciphertextByte)
            Dim cryptoStream As New CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)

            Dim plainText As Byte() = New Byte(ciphertextByte.Length - 1) {}

            Dim decryptedCount As Integer = cryptoStream.Read(plainText, 0, plainText.Length)

            memoryStream.Close()
            cryptoStream.Close()

            Return Encoding.Unicode.GetString(plainText, 0, decryptedCount)
        Catch e As Exception
            Throw New InvalidDataException("[" + e.ToString() + " : " + e.Message + "]  Data corrupt")
        End Try
    End Function

    Private Function RandomString(ByVal size As Integer, ByVal lowerCase As Boolean) As String
        Dim builder As New StringBuilder()
        Dim random As New Random()
        Dim ch As Char
        For i As Integer = 0 To size - 1
            ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)))
            builder.Append(ch)
        Next
        If lowerCase Then
            Return builder.ToString().ToLower()
        End If
        Return builder.ToString()
    End Function

    Function MAPNUMBER(ByVal x As Integer) As String
        Select Case x
            Case 0
                Return "X"
            Case 1
                Return "Y"
            Case 2
                Return "Z"
            Case 3
                Return "A"
            Case 4
                Return "B"
            Case 5
                Return "C"
            Case 6
                Return "D"
            Case 7
                Return "E"
            Case 8
                Return "F"
            Case 9
                Return "G"
        End Select
    End Function

    Function MAPBALIKNUMBER(ByVal x As String) As String
        Select Case x
            Case "X"
                Return "0"
            Case "Y"
                Return "1"
            Case "Z"
                Return "2"
            Case "A"
                Return "3"
            Case "B"
                Return "4"
            Case "C"
                Return "5"
            Case "D"
                Return "6"
            Case "E"
                Return "7"
            Case "F"
                Return "8"
            Case "G"
                Return "9"
        End Select
    End Function

    <WebMethod()> _
    Function SaveTarget(ByVal _target As otarget) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim query As String = ""
            Dim cmd As SqlCommand
            Dim dr As SqlDataReader
            Dim level = "", func = "", jobsite = "",dir="", section As String = ""
            Dim calctype As String = ""
            Dim month As New ArrayList
            query = "select level,dir,func,jobsite,section,calctype from mkpi where id='" & _target.Kpiid & "'"
            cmd = New SqlCommand(query, conn)
            conn.Open()
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    level = dr("level")
                    dir = dr("dir")
                    func = dr("func")
                    jobsite = dr("jobsite")
                    section = dr("section")
                    calctype = dr("calctype")
                End While
            End If
            dr.Close()
            dr = Nothing

            Select Case level
                Case "0" 'BUMA
                    query = "select month from TBSCAPPR where YEAR='" & _target.Tahun & "' and level='0' order by month"
                Case "1" 'FUNCTION
                    query = "select month from TBSCAPPR where YEAR='" & _target.Tahun & "' and level='1' and func='" & func & "' order by month"
                Case "2" 'Section
                    query = "select month from TBSCAPPR where YEAR='" & _target.Tahun & "' and level='2' and jobsite='" & jobsite & "' and section='" & section & "' order by month"
                Case "3" 'Jobsite
                    query = "select month from TBSCAPPR where YEAR='" & _target.Tahun & "' and level='3' and jobsite='" & jobsite & "' order by month"
                Case "4" 'Direktorat
                    query = "select month from TBSCAPPR where YEAR='" & _target.Tahun & "' and level='4' and dir='" & dir & "' order by month"
            End Select
            cmd = New SqlCommand(query, conn)
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    month.Add(dr("month"))
                End While
            End If
            dr.Close()
            dr = Nothing
            query = "BEGIN TRANSACTION; BEGIN TRY "
            query += "insert into ttarget (year,kpiid,versi,calctype,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,stedit,CreatedBy,CreatedIn,annual) values('" & _
                _target.Tahun & "','" & _target.Kpiid & "','" & _target.Versi & _
                "','" & _target.Calctype & "','" & IIf(_target.Jan = "", "", Replace(CDbl(IIf(_target.Jan = "", 0, _target.Jan)).ToString(), ",", ".")) & "','" & IIf(_target.Feb = "", "", Replace(CDbl(IIf(_target.Feb = "", 0, _target.Feb)).ToString(), ",", ".")) & "','" & _
                IIf(_target.Mar = "", "", Replace(CDbl(IIf(_target.Mar = "", 0, _target.Mar)).ToString(), ",", ".")) & "','" & IIf(_target.Apr = "", "", Replace(CDbl(IIf(_target.Apr = "", 0, _target.Apr)).ToString(), ",", ".")) & "','" & IIf(_target.Mei = "", "", Replace(CDbl(IIf(_target.Mei = "", 0, _target.Mei)).ToString(), ",", ".")) & "','" & IIf(_target.Jun = "", "", Replace(CDbl(IIf(_target.Jun = "", 0, _target.Jun)).ToString(), ",", ".")) & "','" & _
                IIf(_target.Jul = "", "", Replace(CDbl(IIf(_target.Jul = "", 0, _target.Jul)).ToString(), ",", ".")) & "','" & IIf(_target.Agu = "", "", Replace(CDbl(IIf(_target.Agu = "", 0, _target.Agu)).ToString(), ",", ".")) & "','" & IIf(_target.Sep = "", "", Replace(CDbl(IIf(_target.Sep = "", 0, _target.Sep)).ToString(), ",", ".")) & "','" & IIf(_target.Okt = "", "", Replace(CDbl(IIf(_target.Okt = "", 0, _target.Okt)).ToString(), ",", ".")) & "','" & _
                IIf(_target.Nov = "", "", Replace(CDbl(IIf(_target.Nov = "", 0, _target.Nov)).ToString(), ",", ".")) & "','" & IIf(_target.Des = "", "", Replace(CDbl(IIf(_target.Des = "", 0, _target.Des)).ToString(), ",", ".")) & "','0','" & _target.Createdby & "','" & _target.Createdin & "'," & IIf(_target.At = "", "", Replace(CDbl(IIf(_target.At = "", 0, _target.At)).ToString(), ",", ".")) & ");SELECT IDENT_CURRENT('ttarget');"
            For i As Integer = 1 To 12
                If month.IndexOf(i.ToString()) = -1 Then
                    Select Case i
                        Case 1
                            query += "UPDATE TBSC SET TARGET=" & IIf(_target.Jan = "", "null, achievement=null ", _target.Jan) & " where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            If calctype = "0" Then
                                'query += "update tbsc set achievement = ROUND(target/actual*100,2) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when actual=0 then 100 else ROUND(target/actual*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            ElseIf calctype = "1" Then
                                'query += "update tbsc set achievement = ROUND(actual/target*100,2) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when target=0 then 0 else ROUND(actual/target*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            End If
                        Case 2
                            query += "UPDATE TBSC SET TARGET=" & IIf(_target.Feb = "", "null, achievement=null ", _target.Feb) & " where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            If calctype = "0" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when actual=0 then 100 else ROUND(target/actual*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            ElseIf calctype = "1" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when target=0 then 0 else ROUND(actual/target*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            End If
                        Case 3
                            query += "UPDATE TBSC SET TARGET=" & IIf(_target.Mar = "", "null, achievement=null ", _target.Mar) & " where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            If calctype = "0" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when actual=0 then 100 else ROUND(target/actual*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            ElseIf calctype = "1" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when target=0 then 0 else ROUND(actual/target*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            End If
                        Case 4
                            query += "UPDATE TBSC SET TARGET=" & IIf(_target.Apr = "", "null, achievement=null ", _target.Apr) & " where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            If calctype = "0" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when actual=0 then 100 else ROUND(target/actual*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            ElseIf calctype = "1" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when target=0 then 0 else ROUND(actual/target*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            End If
                        Case 5
                            query += "UPDATE TBSC SET TARGET=" & IIf(_target.Mei = "", "null, achievement=null ", _target.Mei) & " where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            If calctype = "0" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when actual=0 then 100 else ROUND(target/actual*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            ElseIf calctype = "1" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when target=0 then 0 else ROUND(actual/target*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            End If
                        Case 6
                            query += "UPDATE TBSC SET TARGET=" & IIf(_target.Jun = "", "null, achievement=null ", _target.Jun) & " where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            If calctype = "0" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when actual=0 then 100 else ROUND(target/actual*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            ElseIf calctype = "1" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when target=0 then 0 else ROUND(actual/target*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            End If
                        Case 7
                            query += "UPDATE TBSC SET TARGET=" & IIf(_target.Jul = "", "null, achievement=null ", _target.Jul) & " where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            If calctype = "0" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when actual=0 then 100 else ROUND(target/actual*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            ElseIf calctype = "1" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when target=0 then 0 else ROUND(actual/target*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            End If
                        Case 8
                            query += "UPDATE TBSC SET TARGET=" & IIf(_target.Agu = "", "null, achievement=null ", _target.Agu) & " where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            If calctype = "0" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when actual=0 then 100 else ROUND(target/actual*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            ElseIf calctype = "1" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when target=0 then 0 else ROUND(actual/target*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            End If
                        Case 9
                            query += "UPDATE TBSC SET TARGET=" & IIf(_target.Sep = "", "null, achievement=null ", _target.Sep) & " where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            If calctype = "0" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when actual=0 then 100 else ROUND(target/actual*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            ElseIf calctype = "1" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when target=0 then 0 else ROUND(actual/target*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            End If
                        Case 10
                            query += "UPDATE TBSC SET TARGET=" & IIf(_target.Okt = "", "null, achievement=null ", _target.Okt) & " where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            If calctype = "0" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when actual=0 then 100 else ROUND(target/actual*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            ElseIf calctype = "1" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when target=0 then 0 else ROUND(actual/target*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            End If
                        Case 11
                            query += "UPDATE TBSC SET TARGET=" & IIf(_target.Nov = "", "null, achievement=null ", _target.Nov) & " where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            If calctype = "0" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when actual=0 then 100 else ROUND(target/actual*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            ElseIf calctype = "1" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when target=0 then 0 else ROUND(actual/target*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            End If
                        Case 12
                            query += "UPDATE TBSC SET TARGET=" & IIf(_target.Des = "", "null, achievement=null ", _target.Des) & " where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            If calctype = "0" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when actual=0 then 100 else ROUND(target/actual*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            ElseIf calctype = "1" Then
                                query += "update tbsc set achievement = (case when (target=0 and actual=0) then 100 when target=0 then 0 else ROUND(actual/target*100,2) end) where year='" & _target.Tahun & "' and month=" & i & " and kpiid=" & _target.Kpiid & " and approved='0';"
                            End If
                    End Select
                End If
            Next
            query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
            cmd = New SqlCommand(query, conn)
            Dim result As Integer = cmd.ExecuteScalar
            If IsNumeric(result) Then
                Return "OK"
            End If
            conn.Close()
            Return query
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    <WebMethod()> _
    Function copycalctype(ByVal kpiparent As String, ByVal nikuser As String, ByVal compname As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim query As String = "select isnull(a.calctype,'999') as calctype,cast(b.type as CHAR(1)) as type from mkpi a left join calccontent b on a.id=b.kpiid and a.calctype=b.calctype where a.id='" & kpiparent & "'"
            Dim cmd As New SqlCommand(query, conn)
            Dim dr As SqlDataReader
            Dim calctype As String
            Dim type As String
            Dim myresult As Integer
            conn.Open()
            dr = cmd.ExecuteReader()
            While dr.Read
                calctype = dr("calctype")
                If Not IsDBNull(dr("type")) Then
                    type = dr("type")
                End If
            End While
            dr.Close()
            dr = Nothing
            query = "select count(idchild) from KPIRELATION where IDParent='" & kpiparent & "' and includecount=1"
            cmd = New SqlCommand(query, conn)
            myresult = cmd.ExecuteScalar()
            conn.Close()
            If myresult = 0 Then
                Return "n"
            End If
            If calctype <> "999" Then
                query = "BEGIN TRANSACTION; BEGIN TRY "
                query += "update mkpi set calctype='" & calctype & "' where ID in (select idchild from KPIRELATION where IDParent='" & kpiparent & "' and includecount=1);"
                If calctype = "2" Or calctype = "4" Then
                    Dim checkQuery As String = "select a.idchild,a.bkpiid,b.* from (select a.*,b.kpiid as bkpiid from (select a.idchild,a.idparent,a.includecount,b.calctype from KPIRELATION a, (select " & calctype & " as calctype)b) a left join calccontent b on a.idchild =b.kpiid where IDParent='" & kpiparent & "' and includecount='1')a, (select * from calccontent where kpiid='" & kpiparent & "') b "
                    cmd = New SqlCommand(checkQuery, conn)
                    conn.Open()
                    dr = cmd.ExecuteReader
                    While dr.Read
                        If IsDBNull(dr("bkpiid")) Then
                            query += "insert into calccontent(kpiid,calctype,e1,e2,ea,mr1,mr2,mra,bl1,bl2,bla,p2,pa,createdby,createdin) values ( " & _
                                "'" & dr("idchild") & "','" & dr("calctype") & "'," & dr("E1") & "," & dr("E2") & "," & dr("EA") & _
                                "," & dr("MR1") & "," & dr("MR2") & "," & dr("MRA") & "," & dr("BL1") & "," & dr("BL2") & "," & dr("BLA") & _
                                "," & dr("P2") & "," & dr("PA") & ",'" & nikuser & "','" & compname & "');"
                        Else
                            query += "update calccontent set calctype=" & dr("calctype") & ",e1=" & dr("E1") & ",e2=" & dr("E2") & ",ea=" & dr("EA") & _
                                ",mr1=" & dr("MR1") & ",mr2=" & dr("MR2") & ",mr3=null,mr4=null,mra=" & dr("MRA") & ",bl1=" & dr("BL1") & ",bl2=" & dr("BL2") & ",bl3=null,bl4=null,bla=" & dr("BLA") & _
                                ",p1=null,p2=" & dr("P2") & ",pa=" & dr("PA") & ",modifiedtime=getdate(),modifiedby='" & nikuser & "',modifiedin='" & compname & "' where kpiid='" & dr("idchild") & "';"
                        End If
                    End While
                    dr.Close()
                    dr = Nothing
                    conn.Close()
                ElseIf calctype = "3" Then
                    Dim checkQuery As String = "select a.idchild,a.bkpiid,b.* from (select a.*,b.kpiid as bkpiid from (select a.idchild,a.idparent,a.includecount,b.calctype from KPIRELATION a, (select " & calctype & " as calctype)b) a left join calccontent b on a.idchild =b.kpiid where IDParent='" & kpiparent & "' and includecount='1')a, (select * from calccontent where kpiid='" & kpiparent & "') b "
                    cmd = New SqlCommand(checkQuery, conn)
                    conn.Open()
                    dr = cmd.ExecuteReader
                    While dr.Read
                        If IsDBNull(dr("bkpiid")) Then
                            query += "insert into calccontent(kpiid,calctype,e1,e2,ea,mr1,mr2,mr3,mr4,mra,bl1,bl2,bl3,bl4,bla,p1,p2,pa,createdby,createdin) values ( " & _
                                "'" & dr("idchild") & "','" & dr("calctype") & "'," & dr("E1") & "," & dr("E2") & "," & dr("EA") & _
                                "," & dr("MR1") & "," & dr("MR2") & "," & dr("MR3") & "," & dr("MR4") & "," & dr("MRA") & "," & dr("BL1") & "," & dr("BL2") & "," & dr("BL3") & "," & dr("BL4") & "," & dr("BLA") & _
                                "," & dr("P1") & "," & dr("P2") & "," & dr("PA") & ",'" & nikuser & "','" & compname & "');"
                        Else
                            query += "update calccontent set calctype=" & dr("calctype") & ",e1=" & dr("E1") & ",e2=" & dr("E2") & ",ea=" & dr("EA") & _
                                ",mr1=" & dr("MR1") & ",mr2=" & dr("MR2") & ",mr3=" & dr("MR3") & ",mr4=" & dr("MR4") & ",mra=" & dr("MRA") & ",bl1=" & dr("BL1") & ",bl2=" & dr("BL2") & ",bl3=" & dr("BL3") & ",bl4=" & dr("BL4") & ",bla=" & dr("BLA") & _
                                ",p1=" & dr("P1") & ",p2=" & dr("P2") & ",pa=" & dr("PA") & ",modifiedtime=getdate(),modifiedby='" & nikuser & "',modifiedin='" & compname & "' where kpiid='" & dr("idchild") & "';"
                        End If
                    End While
                    dr.Close()
                    dr = Nothing
                    conn.Close()
                End If
                query += Calculate(calctype, kpiparent)
                query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 1;end "
                cmd = New SqlCommand(query, conn)
                conn.Open()
                cmd.ExecuteScalar()
                conn.Close()
                Return "s"
            End If
            Return "n"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    <WebMethod()> _
    Function copytarget(ByVal kpiparent As String, ByVal tahun As String, ByVal nikuser As String, ByVal compname As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim cmd As New SqlCommand()
        Dim query As String
        Try
            '---------------------------------- UPDATE CHILD KPI TARGET
            Dim myquery As String = "select zz.*,kpi.calctype as kpicalctype from (select a.*,b.year,b.kpiid,b.calctype,b.jan,b.feb,b.mar,b.apr,b.mei,b.jun,b.jul,b.agu,b.sep,b.okt,b.nov,b.des,b.annual from (select idchild,MAX(Versi) as versi from ( select idchild,isnull(b.versi,0) as versi from KPIRELATION a left join TTARGET b on a.idchild = b.kpiid and b.year='" & tahun & "' left join mkpi kpi on a.idchild=kpi.id where IDParent='" & kpiparent & "' and includecount='1' )cc group by idchild)a, (select top 1 * from ttarget where kpiid='" & kpiparent & "' and year='" & tahun & "' order by versi desc)b)zz left join mkpi kpi on zz.idchild=kpi.id"
            Dim dr As SqlDataReader
            cmd = New SqlCommand(myquery, conn)
            conn.Open()
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                query = "BEGIN TRANSACTION; BEGIN TRY "
                While dr.Read
                    query += "insert into ttarget (year,kpiid,versi,calctype,jan,feb,mar,apr,mei,jun,jul,agu,sep,okt,nov,des,stedit,CreatedBy,CreatedIn,annual) values('" & _
                                tahun & "','" & dr("idchild") & "','" & (dr("versi") + 1) & _
                                "','" & dr("Calctype") & "','" & dr("JAN") & "','" & dr("FEB") & "','" & _
                                dr("MAR") & "','" & dr("APR") & "','" & dr("MEI") & "','" & dr("JUN") & "','" & _
                                dr("JUL") & "','" & dr("AGU") & "','" & dr("SEP") & "','" & dr("OKT") & "','" & _
                                dr("NOV") & "','" & dr("DES") & "','0','" & nikuser & "','" & compname & "'," & dr("ANNUAL") & ");"
                    For i As Integer = 1 To 12
                        Select Case i
                            Case 1
                                query += "UPDATE TBSC SET TARGET=" & IIf(dr("JAN") = "", "NULL", dr("JAN")) & " where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                If dr("kpicalctype") = "0" Then 'Min
                                    query += "update tbsc set achievement = (case when actual=0 then 100 else ROUND(target/actual*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                ElseIf dr("kpicalctype") = "1" Then 'Max
                                    query += "update tbsc set achievement = (case when target=0 and actual <> 0 then 0 when target=0 and actual=0 then 100 else ROUND(actual/target*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                End If
                            Case 2
                                query += "UPDATE TBSC SET TARGET=" & IIf(dr("FEB") = "", "NULL", dr("FEB")) & " where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                If dr("kpicalctype") = "0" Then
                                    query += "update tbsc set achievement = (case when actual=0 then 100 else ROUND(target/actual*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                ElseIf dr("kpicalctype") = "1" Then
                                    query += "update tbsc set achievement = (case when target=0 and actual <> 0 then 0 when target=0 and actual=0 then 100 else ROUND(actual/target*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                End If
                            Case 3
                                query += "UPDATE TBSC SET TARGET=" & IIf(dr("MAR") = "", "NULL", dr("MAR")) & " where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                If dr("kpicalctype") = "0" Then
                                    query += "update tbsc set achievement = (case when actual=0 then 100 else ROUND(target/actual*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                ElseIf dr("kpicalctype") = "1" Then
                                    query += "update tbsc set achievement =(case when target=0 and actual <> 0 then 0 when target=0 and actual=0 then 100 else ROUND(actual/target*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                End If
                            Case 4
                                query += "UPDATE TBSC SET TARGET=" & IIf(dr("APR") = "", "NULL", dr("APR")) & " where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                If dr("kpicalctype") = "0" Then
                                    query += "update tbsc set achievement = (case when actual=0 then 100 else ROUND(target/actual*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                ElseIf dr("kpicalctype") = "1" Then
                                    query += "update tbsc set achievement = (case when target=0 and actual <> 0 then 0 when target=0 and actual=0 then 100 else ROUND(actual/target*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                End If
                            Case 5
                                query += "UPDATE TBSC SET TARGET=" & IIf(dr("MEI") = "", "NULL", dr("MEI")) & " where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                If dr("kpicalctype") = "0" Then
                                    query += "update tbsc set achievement = (case when actual=0 then 100 else ROUND(target/actual*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                ElseIf dr("kpicalctype") = "1" Then
                                    query += "update tbsc set achievement = (case when target=0 and actual <> 0 then 0 when target=0 and actual=0 then 100 else ROUND(actual/target*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                End If
                            Case 6
                                query += "UPDATE TBSC SET TARGET=" & IIf(dr("JUN") = "", "NULL", dr("JUN")) & " where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                If dr("kpicalctype") = "0" Then
                                    query += "update tbsc set achievement = (case when actual=0 then 100 else ROUND(target/actual*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                ElseIf dr("kpicalctype") = "1" Then
                                    query += "update tbsc set achievement = (case when target=0 and actual <> 0 then 0 when target=0 and actual=0 then 100 else ROUND(actual/target*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                End If
                            Case 7
                                query += "UPDATE TBSC SET TARGET=" & IIf(dr("JUL") = "", "NULL", dr("JUL")) & " where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                If dr("kpicalctype") = "0" Then
                                    query += "update tbsc set achievement = (case when actual=0 then 100 else ROUND(target/actual*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                ElseIf dr("kpicalctype") = "1" Then
                                    query += "update tbsc set achievement = (case when target=0 and actual <> 0 then 0 when target=0 and actual=0 then 100 else ROUND(actual/target*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                End If
                            Case 8
                                query += "UPDATE TBSC SET TARGET=" & IIf(dr("AGU") = "", "NULL", dr("AGU")) & " where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                If dr("kpicalctype") = "0" Then
                                    query += "update tbsc set achievement = (case when actual=0 then 100 else ROUND(target/actual*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                ElseIf dr("kpicalctype") = "1" Then
                                    query += "update tbsc set achievement = (case when target=0 and actual <> 0 then 0 when target=0 and actual=0 then 100 else ROUND(actual/target*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                End If
                            Case 9
                                query += "UPDATE TBSC SET TARGET=" & IIf(dr("SEP") = "", "NULL", dr("SEP")) & " where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                If dr("kpicalctype") = "0" Then
                                    query += "update tbsc set achievement = (case when actual=0 then 100 else ROUND(target/actual*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                ElseIf dr("kpicalctype") = "1" Then
                                    query += "update tbsc set achievement = (case when target=0 and actual <> 0 then 0 when target=0 and actual=0 then 100 else ROUND(actual/target*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                End If
                            Case 10
                                query += "UPDATE TBSC SET TARGET=" & IIf(dr("OKT") = "", "NULL", dr("OKT")) & " where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                If dr("kpicalctype") = "0" Then
                                    query += "update tbsc set achievement = (case when actual=0 then 100 else ROUND(target/actual*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                ElseIf dr("kpicalctype") = "1" Then
                                    query += "update tbsc set achievement = (case when target=0 and actual <> 0 then 0 when target=0 and actual=0 then 100 else ROUND(actual/target*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                End If
                            Case 11
                                query += "UPDATE TBSC SET TARGET=" & IIf(dr("NOV") = "", "NULL", dr("NOV")) & " where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                If dr("kpicalctype") = "0" Then
                                    query += "update tbsc set achievement = (case when actual=0 then 100 else ROUND(target/actual*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                ElseIf dr("kpicalctype") = "1" Then
                                    query += "update tbsc set achievement = (case when target=0 and actual <> 0 then 0 when target=0 and actual=0 then 100 else ROUND(actual/target*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                End If
                            Case 12
                                query += "UPDATE TBSC SET TARGET=" & IIf(dr("DES") = "", "NULL", dr("DES")) & " where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                If dr("kpicalctype") = "0" Then
                                    query += "update tbsc set achievement = (case when actual=0 then 100 else ROUND(target/actual*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                ElseIf dr("kpicalctype") = "1" Then
                                    query += "update tbsc set achievement = (case when target=0 and actual <> 0 then 0 when target=0 and actual=0 then 100 else ROUND(actual/target*100,2) end) where approved='0' and year='" & tahun & "' and month=" & i & " and kpiid=" & dr("idchild") & ";"
                                End If
                        End Select
                    Next
                End While
                dr.Close()
                dr = Nothing
                query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 1;end "
                cmd = New SqlCommand(query, conn)
                Dim myresult As String = ""
                myresult = cmd.ExecuteScalar()
                conn.Close()
                If myresult = "1" Then
                    Return "s"
                Else
                    Return myresult
                End If
            End If
            Return "n"
            '----------------------------------
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Function Calculate(ByVal CalcType As String, ByVal kpiid As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim cmd As New SqlCommand()
            Dim sqlquery As String
            conn.Open()
            Dim dt As New DataTable()
            If CalcType = "2" Or CalcType = "3" Or CalcType = "4" Then
                sqlquery = "Select * from dbo.CALCCONTENT where kpiid='" & kpiid & "' and CalcType='" & CalcType & "'"
                Dim da As New SqlDataAdapter(sqlquery, conn)
                da.Fill(dt)
            End If
            sqlquery = "select * from TBSC where kpiid in (select idchild from KPIRELATION where IDParent='" & kpiid & "' and includecount=1) and approved='0'"
            Dim reader As SqlDataReader
            cmd = New SqlCommand(sqlquery, conn)
            reader = cmd.ExecuteReader()
            sqlquery = ""
            If reader.HasRows Then
                Dim returnQuery As String = ""
                While reader.Read
                    If IsDBNull(reader("target")) And IsDBNull(reader("Actual")) Then
                    ElseIf CalcType = "0" Or CalcType = "1" Then 'MIN OR MAX
                        If IsDBNull(reader("target")) Or IsDBNull(reader("Actual")) Then
                        Else
                            If CalcType = "0" Then 'MIN
                                If CDec(reader("actual")) = 0 Then
                                    sqlquery += "update TBSC set achievement='100' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                Else
                                    sqlquery += "update TBSC set achievement='" & IIf(Math.Round(CDec(reader("target")) / CDec(reader("actual")) * 100, 2) > 105, 105, Math.Round(CDec(reader("target")) / CDec(reader("actual")) * 100, 2)) & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                End If
                                'sqlquery += "update TBSC set achievement='" & IIf(Math.Round(CDec(reader("target")) / CDec(reader("actual")) * 100, 2) > 105, 105, Math.Round(CDec(reader("target")) / CDec(reader("actual")) * 100, 2)) & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                            Else
                                If CDec(reader("target")) = 0 And CDec(reader("actual")) = 0 Then 'MAX
                                    sqlquery += "update TBSC set achievement='100' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                ElseIf CDec(reader("target")) = 0 Then
                                    sqlquery += "update TBSC set achievement='0' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                Else
                                    sqlquery += "update TBSC set achievement='" & IIf(Math.Round(CDec(reader("actual")) / CDec(reader("target")) * 100, 2) > 105, 105, Math.Round(CDec(reader("actual")) / CDec(reader("target")) * 100, 2)) & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                End If
                                'sqlquery += "update TBSC set achievement='" & IIf(Math.Round(CDec(reader("actual")) / CDec(reader("target")) * 100, 2) > 105, 105, Math.Round(CDec(reader("actual")) / CDec(reader("target")) * 100, 2)) & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                            End If
                        End If
                    Else
                        Select Case CalcType
                            Case 2
                                If (reader("actual") <= dt.Rows(0)("e2") And reader("actual") >= dt.Rows(0)("e1")) Then
                                    sqlquery += "update TBSC set achievement='" & dt.Rows(0)("ea") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                ElseIf (reader("actual") <= dt.Rows(0)("mr2") And reader("actual") >= dt.Rows(0)("mr1")) Then
                                    sqlquery += "update TBSC set achievement='" & dt.Rows(0)("mra") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                ElseIf (reader("actual") <= dt.Rows(0)("bl2") And reader("actual") >= dt.Rows(0)("bl1")) Then
                                    sqlquery += "update TBSC set achievement='" & dt.Rows(0)("bla") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                ElseIf (reader("actual") > dt.Rows(0)("p2")) Then
                                    sqlquery += "update TBSC set achievement='" & dt.Rows(0)("pa") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                Else
                                    sqlquery += "update TBSC set achievement='" & 100 & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                End If
                            Case 3
                                If (reader("actual") <= dt.Rows(0)("e2") And reader("actual") >= dt.Rows(0)("e1")) Then
                                    sqlquery += "update TBSC set achievement='" & dt.Rows(0)("ea") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                ElseIf (reader("actual") <= dt.Rows(0)("mr2") And reader("actual") >= dt.Rows(0)("mr1")) Or (reader("actual") <= dt.Rows(0)("mr4") And reader("actual") >= dt.Rows(0)("mr3")) Then
                                    sqlquery += "update TBSC set achievement='" & dt.Rows(0)("mra") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                ElseIf (reader("actual") <= dt.Rows(0)("bl2") And reader("actual") >= dt.Rows(0)("bl1")) Or (reader("actual") <= dt.Rows(0)("bl4") And reader("actual") >= dt.Rows(0)("bl3")) Then
                                    sqlquery += "update TBSC set achievement='" & dt.Rows(0)("bla") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                ElseIf (reader("actual") > dt.Rows(0)("p2") Or reader("actual") < dt.Rows(0)("p1")) Then
                                    sqlquery += "update TBSC set achievement='" & dt.Rows(0)("pa") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                Else
                                    sqlquery += "update TBSC set achievement='" & 100 & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                End If
                            Case 4
                                If (reader("actual") <= dt.Rows(0)("e2") And reader("actual") >= dt.Rows(0)("e1")) Then
                                    sqlquery += "update TBSC set achievement='" & dt.Rows(0)("ea") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                ElseIf (reader("actual") <= dt.Rows(0)("mr2") And reader("actual") >= dt.Rows(0)("mr1")) Then
                                    sqlquery += "update TBSC set achievement='" & dt.Rows(0)("mra") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                ElseIf (reader("actual") <= dt.Rows(0)("bl2") And reader("actual") >= dt.Rows(0)("bl1")) Then
                                    sqlquery += "update TBSC set achievement='" & dt.Rows(0)("bla") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                ElseIf (reader("actual") <= dt.Rows(0)("p2")) Then
                                    sqlquery += "update TBSC set achievement='" & dt.Rows(0)("pa") & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                Else
                                    sqlquery += "update TBSC set achievement='" & 100 & "' where year='" & reader("year") & "' and month='" & reader("month") & "' and kpiid='" & reader("kpiid") & "';"
                                End If
                        End Select
                    End If
                End While
                Return sqlquery
            Else
                conn.Close()
                Return ""
            End If
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            Return ""
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function UnApproveBSC(ByVal level As String, ByVal year As String, ByVal month As String, ByVal dir As String, ByVal func As String, ByVal sec As String, ByVal jobsite As String, ByVal user As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim connsp As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim cmd As SqlCommand
        Dim result As String
        Dim dr As SqlDataReader
        Try
            sqlQuery = "BEGIN TRANSACTION; BEGIN TRY; declare @myid as decimal(18,0) ; "
            Select Case level
                Case "0" 'BUMA
                    sqlQuery += "insert into TBSCUAPPR (refid,level,year,month,dir,func,jobsite,section,versi,unapprovedby,unapproveddate) select a.id,a.level,a.year,a.month,a.dir,a.func,a.jobsite,a.section,MAX(isnull(b.versi,0))+1 as versi,'" & user & "',GETDATE() from TBSCAPPR a left join TBSCUAPPR b on a.id=b.id where a.level='0' and a.year='" & year & "' and a.month='" & month & "' group by a.id,a.level,a.year,a.month,a.dir,a.func,a.jobsite,a.section; SET @myid = IDENT_CURRENT('TBSCUAPPR'); "
                    sqlQuery += "insert into UTBSC select YEAR,month,kpiid,@myid,target,actual,achievement,picaref,approved,sapflag,'" & user & "','',GETDATE(),null,null,null from TBSC where kpiid in (select id from MKPI where level='0') and month='" & month & "' and year='" & year & "';"
                    sqlQuery += "update tbsc set approved='0' where kpiid in (select id from MKPI where level='0') and month='" & month & "' and year='" & year & "';"
                    sqlQuery += "DELETE TBSCAPPR where level='0' and year='" & year & "' and month='" & month & "'"
                Case "1" 'FUNC
                    sqlQuery += "insert into TBSCUAPPR (refid,level,year,month,dir,func,jobsite,section,versi,unapprovedby,unapproveddate) select a.id,a.level,a.year,a.month,a.dir,a.func,a.jobsite,a.section,MAX(isnull(b.versi,0))+1 as versi,'" & user & "',GETDATE() from TBSCAPPR a left join TBSCUAPPR b on a.id=b.id where a.level='1' and a.year='" & year & "' and a.month='" & month & "' and a.func='" & func & "' group by a.id,a.level,a.year,a.month,a.dir,a.func,a.jobsite,a.section; SET @myid = IDENT_CURRENT('TBSCUAPPR');"
                    sqlQuery += "insert into UTBSC select YEAR,month,kpiid,@myid,target,actual,achievement,picaref,approved,sapflag,'" & user & "','',GETDATE(),null,null,null from TBSC where kpiid in (select id from MKPI where level='1' and func='" & func & "') and year='" & year & "' and month='" & month & "' "
                    sqlQuery += "update tbsc set approved='0' where kpiid in (select id from MKPI where level='1' and func='" & func & "') and year='" & year & "' and month='" & month & "' "
                    sqlQuery += "DELETE TBSCAPPR where level='1' and year='" & year & "' and month='" & month & "' and func='" & func & "'"
                Case "2" 'SEC
                    sqlQuery += "insert into TBSCUAPPR (refid,level,year,month,dir,func,jobsite,section,versi,unapprovedby,unapproveddate) select a.id,a.level,a.year,a.month,a.dir,a.func,a.jobsite,a.section,MAX(isnull(b.versi,0))+1 as versi,'" & user & "',GETDATE() from TBSCAPPR a left join TBSCUAPPR b on a.id=b.id where a.level='2' and a.year='" & year & "' and a.month='" & month & "' and a.jobsite='" & jobsite & "' and a.section='" & sec & "' group by a.id,a.level,a.year,a.month,a.dir,a.func,a.jobsite,a.section; SET @myid = IDENT_CURRENT('TBSCUAPPR');"
                    sqlQuery += "insert into UTBSC select YEAR,month,kpiid,@myid,target,actual,achievement,picaref,approved,sapflag,'" & user & "','',GETDATE(),null,null,null from TBSC where kpiid in (select id from MKPI where level='2' and jobsite='" & jobsite & "' and section='" & sec & "') and year='" & year & "' and month='" & month & "' "
                    sqlQuery += "update tbsc set approved='0' where kpiid in (select id from MKPI where level='2' and jobsite='" & jobsite & "' and section='" & sec & "') and year='" & year & "' and month='" & month & "' "
                    sqlQuery += "DELETE TBSCAPPR where level='2' and year='" & year & "' and month='" & month & "' and jobsite='" & jobsite & "' and section='" & sec & "'"
                Case "3" 'JOBS
                    sqlQuery += "insert into TBSCUAPPR (refid,level,year,month,dir,func,jobsite,section,versi,unapprovedby,unapproveddate) select a.id,a.level,a.year,a.mont,a.dirh,a.func,a.jobsite,a.section,MAX(isnull(b.versi,0))+1 as versi,'" & user & "',GETDATE() from TBSCAPPR a left join TBSCUAPPR b on a.id=b.id where a.level='3' and a.year='" & year & "' and a.month='" & month & "' and a.jobsite='" & jobsite & "' group by a.id,a.level,a.year,a.month,a.dir,a.func,a.jobsite,a.section; SET @myid = IDENT_CURRENT('TBSCUAPPR');"
                    sqlQuery += "insert into UTBSC select YEAR,month,kpiid,@myid,target,actual,achievement,picaref,approved,sapflag,'" & user & "','',GETDATE(),null,null,null from TBSC where  kpiid in (select id from MKPI where level='3' and jobsite='" & jobsite & "') and year='" & year & "' and month='" & month & "'"
                    sqlQuery += "update tbsc set approved='0' where kpiid in (select id from MKPI where level='3' and jobsite='" & jobsite & "') and year='" & year & "' and month='" & month & "'"
                    sqlQuery += "DELETE TBSCAPPR where level='3' and year='" & year & "' and month='" & month & "' and jobsite='" & jobsite & "'"
                Case "4" 'Direktorat
                    sqlQuery += "insert into TBSCUAPPR (refid,level,year,month,dir,func,jobsite,section,versi,unapprovedby,unapproveddate) select a.id,a.level,a.year,a.month,a.dir,a.func,a.jobsite,a.section,MAX(isnull(b.versi,0))+1 as versi,'" & user & "',GETDATE() from TBSCAPPR a left join TBSCUAPPR b on a.id=b.id where a.level='4' and a.year='" & year & "' and a.month='" & month & "' and a.dir='" & dir & "' group by a.id,a.level,a.year,a.month,a.dir,a.func,a.jobsite,a.section; SET @myid = IDENT_CURRENT('TBSCUAPPR'); "
                    sqlQuery += "insert into UTBSC select YEAR,month,kpiid,@myid,target,actual,achievement,picaref,approved,sapflag,'" & user & "','',GETDATE(),null,null,null from TBSC where kpiid in (select id from MKPI where level='0') and month='" & month & "' and year='" & year & "';"
                    sqlQuery += "update tbsc set approved='0' where kpiid in (select id from MKPI where level='4' and dir='" & dir & "') and month='" & month & "' and year='" & year & "';"
                    sqlQuery += "DELETE TBSCAPPR where level='4' and year='" & year & "' and month='" & month & "' and dir='" & dir & "'"

            End Select
            sqlQuery += ""
            sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "
            cmd = New SqlCommand(sqlQuery, conn)
            conn.Open()
            result = cmd.ExecuteScalar()
            'Recalculate Data-----------------------------------------------------------------------------------------
            If result = 0 Then
                Select Case level
                    Case "0" 'BUMA
                        cmd = New SqlCommand("select kpiid from tbsc where kpiid in (select id from MKPI where level='0')  and month='" & month & "' and year='" & year & "'", conn)
                    Case "1" 'FUNC
                        cmd = New SqlCommand("select kpiid from tbsc where kpiid in (select id from MKPI where level='1' and func='" & func & "')  and month='" & month & "' and year='" & year & "'", conn)
                    Case "2" 'SEC
                        cmd = New SqlCommand("select kpiid from tbsc where kpiid in (select id from MKPI where level='2' and jobsite='" & jobsite & "' and section='" & sec & "')  and month='" & month & "' and year='" & year & "'", conn)
                    Case "3" 'JOBS
                        cmd = New SqlCommand("select kpiid from tbsc where kpiid in (select id from MKPI where level='3' and jobsite='" & jobsite & "')  and month='" & month & "' and year='" & year & "'", conn)
                End Select
                dr = cmd.ExecuteReader
                If dr.HasRows Then
                    connsp.Open()
                    While dr.Read
                        cmd = New SqlCommand()
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.CommandText = "sp_recountme"
                        cmd.Connection = connsp
                        cmd.Parameters.AddWithValue("@kpiid", dr("kpiid"))
                        cmd.Parameters.AddWithValue("@month", month)
                        cmd.Parameters.AddWithValue("@year", year)
                        cmd.ExecuteScalar()
                    End While
                    dr = Nothing
                    connsp.Close()
                End If
            End If
            conn.Close()
            '----------------------------------------------------------------
            Return result
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            If connsp.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function getDataKPIFull(ByVal level As String, ByVal year As String, ByVal month As String, ByVal jobsite As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim cmd As SqlCommand
        Dim result As String
        Dim reader As SqlDataReader
        Try
            sqlQuery = "select d.Name,ISNULL(target,0) as target,ISNULL(actual,0) as actual,ISNULL(achievement,0) as ach, d.type, urutan from DASHBOARD d inner join  mkpi a on d.kpiid=a.ID left join tbsc b on a.ID=b.kpiid where  b.year='" & year & "' and b.month='" & month & "' and d.level='" & IIf(level = "0", "0'", "3' and a.jobsite=(select id from SITE where kdsite='" & jobsite & "')") & " order by urutan"
            cmd = New SqlCommand(sqlQuery, conn)
            conn.Open()
            reader = cmd.ExecuteReader
            If reader.HasRows Then
                result = "SUCCESS" + "#~#"
                While reader.Read
                    result += reader("type") + "~#~" + reader("Name") + "~#~" + Format(CDbl(reader("target")), "#,##0.00") + "~#~" + Format(CDbl(reader("actual")), "#,##0.00") + "~#~" + reader("ach").ToString() + "#~#"
                End While
            Else
                result = "NOTFOUND" + "#~#"
            End If
            conn.Close()
            Return result
        Catch ex As Exception
            Return "ERROR#~#"" & ex.Message"
        End Try
    End Function

    <SoapDocumentMethod(OneWay:=True)> _
    <WebMethod()> _
    Public Function Recount(ByVal month As String, ByVal year As String, ByVal level As String, ByVal func As String, ByVal jobsite As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim cmd As New SqlCommand()
            conn.Open()
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "sp_mass_recount"
            cmd.Parameters.AddWithValue("@month",month)
            cmd.Parameters.AddWithValue("@year",year)
            cmd.Parameters.AddWithValue("@level",level)
            cmd.Parameters.AddWithValue("@func",func)
            cmd.Parameters.AddWithValue("@jobsite",jobsite)
            cmd.Connection = conn
            cmd.ExecuteScalar()
            cmd = new sqlcommand()
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "sp_mass_recount_parent"
            cmd.Parameters.AddWithValue("@month",month)
            cmd.Parameters.AddWithValue("@year",year)
            cmd.Parameters.AddWithValue("@level",level)
            cmd.Parameters.AddWithValue("@func",func)
            cmd.Parameters.AddWithValue("@jobsite",jobsite)
            cmd.Connection = conn
            cmd.ExecuteScalar()
            'result = cmd.ExecuteScalar()
            'conn.Close()
            'If result = 0 Then
            '    Return "SUKSES"
            'Else
            '    Return result
            'End If
            Return Nothing
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <SoapDocumentMethod(OneWay:=True)> _
    <WebMethod()> _
    Public Function RecountKPI(ByVal kpiid As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim cmd As New SqlCommand()
            conn.Open()
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "sp_recount"
            cmd.Parameters.AddWithValue("@kpiid", kpiid)
            cmd.Connection = conn
            cmd.ExecuteScalar()
            cmd = New SqlCommand()
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "sp_recount_parent"
            cmd.Parameters.AddWithValue("@kpiid", kpiid)
            cmd.Connection = conn
            cmd.ExecuteScalar()
            'result = cmd.ExecuteScalar()
            'conn.Close()
            'If result = 0 Then
            '    Return "SUKSES"
            'Else
            '    Return result
            'End If
            Return Nothing
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <SoapDocumentMethod(OneWay:=True)> _
   <WebMethod()> _
   Public Function RecountKPIFromParent(ByVal kpiid As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim cmd As New SqlCommand()
            conn.Open()
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "sp_recount"
            cmd.Parameters.AddWithValue("@kpiid", kpiid)
            cmd.Connection = conn
            cmd.ExecuteScalar()
            cmd = New SqlCommand()
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "sp_recount_parent"
            cmd.Parameters.AddWithValue("@kpiid", kpiid)
            cmd.Connection = conn
            cmd.ExecuteScalar()
            Return Nothing
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function
  
    <WebMethod()> _
    Public Function DoRecountBSC(ByVal level As String, ByVal year As String, ByVal month As String, ByVal func As String, ByVal sec As String, ByVal jobsite As String, ByVal user As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim connsp As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        Try
            'Recalculate Data-----------------------------------------------------------------------------------------
            Select Case level
                Case "0" 'BUMA
                    cmd = New SqlCommand("select kpiid from tbsc where kpiid in (select id from MKPI where level='0')  and month='" & month & "' and year='" & year & "'", conn)
                Case "1" 'FUNC
                    cmd = New SqlCommand("select kpiid from tbsc where kpiid in (select id from MKPI where level='1' and func='" & func & "')  and month='" & month & "' and year='" & year & "'", conn)
                Case "2" 'SEC
                    cmd = New SqlCommand("select kpiid from tbsc where kpiid in (select id from MKPI where level='2' and jobsite='" & jobsite & "' and section='" & sec & "')  and month='" & month & "' and year='" & year & "'", conn)
                Case "3" 'JOBS
                    cmd = New SqlCommand("select kpiid from tbsc where kpiid in (select id from MKPI where level='3' and jobsite='" & jobsite & "')  and month='" & month & "' and year='" & year & "'", conn)
            End Select
            conn.Open()
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                connsp.Open()
                While dr.Read
                    cmd = New SqlCommand()
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "sp_recountme"
                    cmd.Connection = connsp
                    cmd.Parameters.AddWithValue("@kpiid", dr("kpiid"))
                    cmd.Parameters.AddWithValue("@month", month)
                    cmd.Parameters.AddWithValue("@year", year)
                    cmd.ExecuteScalar()
                End While
                connsp.Close()
                dr = Nothing
            End If
            conn.Close()
            '----------------------------------------------------------------
            Return "0"
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            If connsp.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function DoSecureActivity(ByVal id As String, ByVal nik As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim cmd As New SqlCommand
        Try
            sqlQuery = "BEGIN TRANSACTION; BEGIN TRY  "
            sqlQuery += " DELETE MACTIVITYSECURE WHERE activityid = " & id & ";"
            If Not nik.Length = 0 Then
                For Each s As String In Left(nik, nik.Length - 1).Split(";")
                    sqlQuery += "INSERT INTO MACTIVITYSECURE VALUES (" & id & ",'" & s & "');"
                Next
            End If
            sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;select 0;	end "
            Dim scom As SqlCommand = New SqlCommand(sqlQuery, conn)
            conn.Open()
            scom.ExecuteReader()
            conn.Close()
            Return "0"
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function RetSecureActivity(ByVal id As String) As List(Of Employee)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        Try
            sqlQuery = "select a.nik,x.nama from mactivitysecure a left outer join OPENROWSET('SQLNCLI','SERVER=20.110.1.5;UID=dev;PWD=hrishris', 'SELECT  nik,'' as pnalt,  nama from JRN_TEST.dbo.H_A101') x on a.nik=x.nik  where a.activityid=" & id
            'sqlQuery = "select a.nik,x.nama from mactivitysecure a left outer join OPENROWSET('SQLNCLI','SERVER=172.16.0.7\hris;UID=IICS;PWD=$IICS-123-ONLINE$', 'SELECT pernr as nik,pnalt, cname as nama from BERP_CENTRAL.dbo.H_A101_SAP') x on a.nik=x.nik or a.nik=x.pnalt where a.activityid=" & id

            Dim scom As SqlCommand = New SqlCommand(sqlQuery, conn)
            Dim lEmployee As New List(Of Employee)
            Dim emp As Employee
            conn.Open()
            dr = scom.ExecuteReader
            If dr.HasRows Then
                While dr.Read()
                    emp = New Employee()
                    emp.Nik = dr("nik")
                    emp.Name = dr("nama")
                    lEmployee.Add(emp)
                End While
            End If
            conn.Close()
            Return lEmployee
        Catch ex As Exception
            Return Nothing
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function InsertActivityProg(ByVal id As String, ByVal progress As String, ByVal nik As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim cmd As New SqlCommand
        Try
            sqlQuery = "BEGIN TRANSACTION; BEGIN TRY  "
            sqlQuery += " INSERT INTO TACTIVITYprog (activityid,progress,createdby,createdin) VALUES (" & id & ",'" & progress & "','" & nik & "','" & HttpContext.Current.Request.UserHostAddress & "');"
            sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;select 0;	end "
            Dim scom As SqlCommand = New SqlCommand(sqlQuery, conn)
            conn.Open()
            scom.ExecuteReader()
            conn.Close()
            Return "0"
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function RetrieveStatus(ByVal id As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        Dim result As String = "0"
        Try
            sqlQuery += " select top 1 status from tactivitystat where activityid=" & id & " order by id desc"
            Dim scom As SqlCommand = New SqlCommand(sqlQuery, conn)
            conn.Open()
            dr = scom.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    result = dr("status")
                End While
            End If
            conn.Close()
            Return result
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function RetrieveActivityProg(ByVal id As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        Dim result As String = ""
        Try
            sqlQuery += " select top 3 createdtime,progress from TACTIVITYprog where activityid=" & id & " order by id desc"
            Dim scom As SqlCommand = New SqlCommand(sqlQuery, conn)
            conn.Open()
            dr = scom.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    result += CDate(dr("createdtime")).ToString("dd/MM/yyyy") + " " + Replace(dr("progress"), "%0D%0A", "<br>") + "<br>"
                End While
            End If
            conn.Close()
            Return result
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function UpdateStatus(ByVal id As String, ByVal stat As String, ByVal tanggal As String, ByVal nik As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        Dim result As String = "0"
        Try
            sqlQuery = "BEGIN TRANSACTION; BEGIN TRY  "
            sqlQuery += "UPDATE MACTIVITY set status='" & stat & "' where id=" & id
            sqlQuery += " INSERT INTO TACTIVITYSTAT (activityid,status,tanggal,createdby,createdin) VALUES (" & id & ",'" & stat & "','" & DateTime.Parse(tanggal, Globalization.CultureInfo.CreateSpecificCulture("en-CA")).ToString("MM/dd/yyyy") & "','" & nik & "','" & HttpContext.Current.Request.UserHostAddress & "');"
            sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;select 0;	end "
            Dim scom As SqlCommand = New SqlCommand(sqlQuery, conn)
            conn.Open()
            result = scom.ExecuteScalar()
            conn.Close()
            If result = "0" Then
                Return "0"
            Else
                Return result
            End If
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function GetGantt(ByVal param As String, ByVal nik As String, ByVal year As String) As List(Of GanttData)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        Dim g As GanttData
        Dim lock As Boolean = False
        Dim result As New List(Of GanttData)
        Try
            cmd.CommandText = "sp_getgantt"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@param", param)
            cmd.Parameters.AddWithValue("@nik", nik)
            cmd.Parameters.AddWithValue("@year", year)
            cmd.Connection = conn
            conn.Open()
            dr = cmd.ExecuteReader()
            If dr.HasRows Then
                While dr.Read
                    If (dr("jenis") = "PROGRAM" And dr("securecount") > 0 And dr("nik") = nik) Then
                        lock = False
                    ElseIf (dr("jenis") = "PROGRAM" And dr("securecount") = 0) Then
                        lock = False
                    ElseIf dr("jenis") = "PROJECT" Then
                        lock = False
                    ElseIf dr("jenis") = "TASK" Then
                    Else
                        lock = True
                    End If
                    If Not lock Then
                        g = New GanttData()
                        g.Jenis = dr("jenis")
                        'g.Activity = IIf(Len(dr("activity")) > 30, Left(dr("activity"), 27) & "...", dr("activity"))
                        g.Activity = dr("activity")
                        If IsDBNull(dr("startdate")) Then
                            g.Ptglstart = ""
                        Else
                            g.Ptglstart = Format(dr("startdate"), "MM/dd/yyyy")
                        End If
                        If IsDBNull(dr("enddate")) Then
                            g.Ptglend = ""
                        Else
                            g.Ptglend = Format(dr("enddate"), "MM/dd/yyyy")
                        End If
                        If IsDBNull(dr("tanggalstart")) Then
                            g.Atglstart = ""
                        Else
                            g.Atglstart = dr("tanggalstart")
                        End If
                        If IsDBNull(dr("tanggalend")) Then
                            g.Atglend = ""
                        Else
                            g.Atglend = dr("tanggalend")
                        End If
                        g.Pic = dr("nama")
                        g.Kpi = dr("name")
                        g.Status = dr("status")
                        'g.Logprogress = IIf(Len(Replace(Replace(dr("logprogress"), "&lt;br&gt;", "<br>"), "%0D%0A", "<br>")) > 100, Left(Replace(Replace(dr("logprogress"), "&lt;br&gt;", "<br>"), "%0D%0A", "<br>"), 97) & "...", Replace(Replace(dr("logprogress"), "&lt;br&gt;", "<br>"), "%0D%0A", "<br>"))
                        g.Logprogress = dr("logprogress")
                        'g.Deliverables = IIf(Len(Replace(Replace(dr("Deliverables"), "&lt;br&gt;", "<br>"), "%0D%0A", "<br>")) > 25, Left(Replace(Replace(dr("Deliverables"), "&lt;br&gt;", "<br>"), "%0D%0A", "<br>"), 22) & "...", Replace(Replace(dr("Deliverables"), "&lt;br&gt;", "<br>"), "%0D%0A", "<br>"))
                        g.Deliverables = dr("Deliverables")
                        g.No = dr("id")
                        result.Add(g)
                    End If
                End While
            End If
            conn.Close()
            Return result
        Catch ex As Exception
            Return Nothing
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    Public Function GetGanttFull(ByVal param As String, ByVal nik As String, ByVal year As String) As List(Of GanttData)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim cmd As New SqlCommand
        Dim dr As SqlDataReader
        Dim g As GanttData
        Dim lock As Boolean = False
        Dim result As New List(Of GanttData)
        Try
            cmd.CommandText = "sp_getgantt_with_cross"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@param", param)
            cmd.Parameters.AddWithValue("@nik", nik)
            cmd.Parameters.AddWithValue("@year", year)
            cmd.Connection = conn
            conn.Open()
            dr = cmd.ExecuteReader()
            If dr.HasRows Then
                While dr.Read
                    If (dr("jenis") = "PROGRAM" And dr("securecount") > 0 And dr("nik") = nik) Then
                        lock = False
                    ElseIf (dr("jenis") = "PROGRAM" And dr("securecount") = 0) Then
                        lock = False
                    ElseIf dr("jenis") = "PROJECT" Then
                        lock = False
                    ElseIf dr("jenis") = "TASK" Then
                    Else
                        lock = True
                    End If
                    If Not lock Then
                        g = New GanttData()
                        g.Jenis = dr("jenis")
                        'g.Activity = IIf(Len(dr("activity")) > 30, Left(dr("activity"), 27) & "...", dr("activity"))
                        g.Activity = dr("activity")
                        If IsDBNull(dr("startdate")) Then
                            g.Ptglstart = ""
                        Else
                            g.Ptglstart = Format(dr("startdate"), "MM/dd/yyyy")
                        End If
                        If IsDBNull(dr("enddate")) Then
                            g.Ptglend = ""
                        Else
                            g.Ptglend = Format(dr("enddate"), "MM/dd/yyyy")
                        End If
                        If IsDBNull(dr("tanggalstart")) Then
                            g.Atglstart = ""
                        Else
                            g.Atglstart = dr("tanggalstart")
                        End If
                        If IsDBNull(dr("tanggalend")) Then
                            g.Atglend = ""
                        Else
                            g.Atglend = dr("tanggalend")
                        End If
                        g.Pic = dr("nama")
                        g.Kpi = dr("name")
                        g.Status = dr("status")
                        'g.Logprogress = IIf(Len(Replace(Replace(dr("logprogress"), "&lt;br&gt;", "<br>"), "%0D%0A", "<br>")) > 100, Left(Replace(Replace(dr("logprogress"), "&lt;br&gt;", "<br>"), "%0D%0A", "<br>"), 97) & "...", Replace(Replace(dr("logprogress"), "&lt;br&gt;", "<br>"), "%0D%0A", "<br>"))
                        g.Logprogress = dr("logprogress")
                        'g.Deliverables = IIf(Len(Replace(Replace(dr("Deliverables"), "&lt;br&gt;", "<br>"), "%0D%0A", "<br>")) > 25, Left(Replace(Replace(dr("Deliverables"), "&lt;br&gt;", "<br>"), "%0D%0A", "<br>"), 22) & "...", Replace(Replace(dr("Deliverables"), "&lt;br&gt;", "<br>"), "%0D%0A", "<br>"))
                        g.Deliverables = dr("Deliverables")
                        g.No = dr("id")
                        result.Add(g)
                    End If
                End While
            End If
            conn.Close()
            Return result
        Catch ex As Exception
            Return Nothing
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
 Public Function UpdateActivity(ByVal x As List(Of ProgressAct), ByVal userid As String, ByVal userip As String) As String
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Dim result As String = ""
        Try
            For Each c As ProgressAct In x
                If Not Trim(c.Progress) = "" Then
                    sqlQuery += " INSERT INTO TACTIVITYprog (activityid,progress,createdby,createdin) VALUES (" & c.Id & ",'" & c.Progress & "','" & userid & "','" & HttpContext.Current.Request.UserHostAddress & "');"
                End If
            Next

            If sqlQuery <> "" Then
                sqlQuery = "BEGIN TRANSACTION; BEGIN TRY  " + sqlQuery
                sqlQuery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;select 0;	end "
            End If
            If sqlQuery <> "" Then
                Dim scom As SqlCommand = New SqlCommand(sqlQuery, conn)
                conn.Open()
                result = scom.ExecuteScalar()
                conn.Close()
            Else
                result = "BLANK"
            End If
            Return result
        Catch ex As Exception
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Function ShowActLampiran(ByVal actid As String) As List(Of BSCLampiran)
        Dim aLampiran As List(Of BSCLampiran) = New List(Of BSCLampiran)

        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            conn.Open()

            Dim squery As String = ""
            squery = "SELECT Nomor,nmfile,tglupload FROM TACTLAMPIRAN WHERE activityid=" + actid + ""
            squery += " order by tglupload desc"

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            Dim sdr As SqlDataReader = scom.ExecuteReader()

            Dim bsclamp As BSCLampiran
            Do While sdr.Read()
                bsclamp = New BSCLampiran
                Dim x As String = sdr("Nomor")
                Dim dummy As String = ""
                For i As Integer = 1 To Len(x)
                    dummy += MAPNUMBER(Mid(x, i, 1))
                Next
                bsclamp.Nomor = "'" & RandomString(5, False) & dummy & "'"
                bsclamp.NmFile = sdr("nmfile") & " [Uploaded Time :" & Format(sdr("tglupload"), "dd/MM/yyyy hh:mm") & "]"
                aLampiran.Add(bsclamp)
            Loop
            conn.Close()
        Catch eks As Exception
            conn.Close()
        End Try
        Return aLampiran
    End Function

    <WebMethod()> _
    <ScriptMethod()> _
    Function DelActLampiran(ByVal nomor As String, ByVal ipcomputer As String, ByVal userid As String) As String
        Dim arg = ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString
        Dim conn As New SqlConnection(arg)
        Try
            Dim squery As String = ""
            Dim n As String = nomor
            Dim dummy As String = ""
            n = Right(n, Len(n) - 5)
            For i As Integer = 1 To Len(n)
                dummy += MAPBALIKNUMBER(Mid(n, i, 1))
            Next
            squery = "BEGIN TRANSACTION; BEGIN TRY  "
            squery += "INSERT INTO DTACTLAMPIRAN (nomor, activityid, nmfile, tglupload, mimetype, mimelength, mimedata, createdby, createdin) SELECT * FROM TACTLAMPIRAN WHERE NOMOR=" + dummy + ";UPDATE DTACTLAMPIRAN set DeletedTime=getdate(), DeletedIn='" & ipcomputer & "', DeletedBy='" & userid & "' where nomor=" + dummy & ";DELETE TACTLAMPIRAN WHERE nomor = " + dummy
            squery += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;select 0;	end "

            Dim scom As SqlCommand = New SqlCommand(squery, conn)
            conn.Open()
            scom.ExecuteReader()
            conn.Close()
        Catch eks As Exception
            conn.Close()
            Return eks.ToString()
        End Try
        Return "SUCCESS"
    End Function
End Class

Public Class ChildKPIWrap
    Private _ChildKPI As List(Of ChildKPI)
    Public Property ChildKPI() As List(Of ChildKPI)
        Get
            Return _ChildKPI
        End Get
        Set(ByVal value As List(Of ChildKPI))
            _ChildKPI = value
        End Set
    End Property

End Class

Public Class ChildKPI
    Private _IdKPI As String
    Private _NamaKPI As String
    Private _FuncKPI As String
    Private _IncludeCount As String
    Private _uom As String

    Public Property IdKPI() As String
        Get
            Return _IdKPI
        End Get
        Set(ByVal value As String)
            _IdKPI = value
        End Set
    End Property
    Public Property NamaKPI() As String
        Get
            Return _NamaKPI
        End Get
        Set(ByVal value As String)
            _NamaKPI = value
        End Set
    End Property
    Public Property FuncKPI() As String
        Get
            Return _FuncKPI
        End Get
        Set(ByVal value As String)
            _FuncKPI = value
        End Set
    End Property
    Public Property IncludeCount() As String
        Get
            Return _IncludeCount
        End Get
        Set(ByVal value As String)
            _IncludeCount = value
        End Set
    End Property
    Public Property Uom() As String
        Get
            Return _uom
        End Get
        Set(ByVal value As String)
            _uom = value
        End Set
    End Property

End Class

Public Class KPI
    Private _id As String
    Private _level As String
    Private _soid As String
    Private _name As String
    Private _func As String
    Private _uom As String
    Private _owner As String
    Private _type As String
    Private _desc As String
    Private _periode As String
    Private _leads As String
    Private _formula As String
    Private _formulaDesc As String
    Private _redParam As String
    Private _greenParam As String
    Private _basedOn As String
    Private _calcType As String
    Private _section As String
    Private _jobsite As String
    Private _greenCond As String
    Private _redCond As String
    Private _KPIType As String
    Private _KPILeads As String
    Private _Process As String
    Private _Targetting As String
    Private _typediscrete As String
    Private _ready As String
    Private _year As String
    Private _bobot As Decimal

    Public Property Id() As String
        Get
            Return _id
        End Get
        Set(ByVal value As String)
            _id = value
        End Set
    End Property
    Public Property Level() As String
        Get
            Return _level
        End Get
        Set(ByVal value As String)
            _level = value
        End Set
    End Property
    Public Property Soid() As String
        Get
            Return _soid
        End Get
        Set(ByVal value As String)
            _soid = value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property
    Public Property Func() As String
        Get
            Return _func
        End Get
        Set(ByVal value As String)
            _func = value
        End Set
    End Property
    Public Property Uom() As String
        Get
            Return _uom
        End Get
        Set(ByVal value As String)
            _uom = value
        End Set
    End Property
    Public Property Owner() As String
        Get
            Return _owner
        End Get
        Set(ByVal value As String)
            _owner = value
        End Set
    End Property
    Public Property Type() As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            _type = value
        End Set
    End Property
    Public Property Desc() As String
        Get
            Return _desc
        End Get
        Set(ByVal value As String)
            _desc = value
        End Set
    End Property
    Public Property Periode() As String
        Get
            Return _periode
        End Get
        Set(ByVal value As String)
            _periode = value
        End Set
    End Property
    Public Property Leads() As String
        Get
            Return _leads
        End Get
        Set(ByVal value As String)
            _leads = value
        End Set
    End Property
    Public Property Formula() As String
        Get
            Return _formula
        End Get
        Set(ByVal value As String)
            _formula = value
        End Set
    End Property
    Public Property FormulaDesc() As String
        Get
            Return _formulaDesc
        End Get
        Set(ByVal value As String)
            _formulaDesc = value
        End Set
    End Property
    Public Property RedParam() As String
        Get
            Return _redParam
        End Get
        Set(ByVal value As String)
            _redParam = value
        End Set
    End Property
    Public Property GreenParam() As String
        Get
            Return _greenParam
        End Get
        Set(ByVal value As String)
            _greenParam = value
        End Set
    End Property
    Public Property BasedOn() As String
        Get
            Return _basedOn
        End Get
        Set(ByVal value As String)
            _basedOn = value
        End Set
    End Property
    Public Property CalcType() As String
        Get
            Return _calcType
        End Get
        Set(ByVal value As String)
            _calcType = value
        End Set
    End Property
    Public Property Section() As String
        Get
            Return _section
        End Get
        Set(ByVal value As String)
            _section = value
        End Set
    End Property
    Public Property Jobsite() As String
        Get
            Return _jobsite
        End Get
        Set(ByVal value As String)
            _jobsite = value
        End Set
    End Property
    Public Property GreenCond() As String
        Get
            Return _greenCond
        End Get
        Set(ByVal value As String)
            _greenCond = value
        End Set
    End Property
    Public Property RedCond() As String
        Get
            Return _redCond
        End Get
        Set(ByVal value As String)
            _redCond = value
        End Set
    End Property
    Public Property KPIType() As String
        Get
            Return _KPIType
        End Get
        Set(ByVal value As String)
            _KPIType = value
        End Set
    End Property
    Public Property KPILeads() As String
        Get
            Return _KPILeads
        End Get
        Set(ByVal value As String)
            _KPILeads = value
        End Set
    End Property
    Public Property Process() As String
        Get
            Return _Process
        End Get
        Set(ByVal value As String)
            _Process = value
        End Set
    End Property
    Public Property Targetting() As String
        Get
            Return _Targetting
        End Get
        Set(ByVal value As String)
            _Targetting = value
        End Set
    End Property
    Public Property Typediscrete() As String
        Get
            Return _typediscrete
        End Get
        Set(ByVal value As String)
            _typediscrete = value
        End Set
    End Property
    Public Property Ready() As String
        Get
            Return _ready
        End Get
        Set(ByVal value As String)
            _ready = value
        End Set
    End Property
    Public Property Year() As String
        Get
            Return _year
        End Get
        Set(ByVal value As String)
            _year = value
        End Set
    End Property
    Public Property Bobot() As Decimal
        Get
            Return _bobot
        End Get
        Set(ByVal value As Decimal)
            _bobot = value
        End Set
    End Property
End Class

Public Class objact
    Private _id As String
    Private _nomor As String
    Private _tipe As String
    Private _activity As String
    Private _pic As String
    Private _deliverable As String
    Private _start As String
    Private _enddate As String
    Private _state As String
    Private _del As String
    Private _kpiid As String
    Private _year As String
    Private _namapic As String
    Private _kpiname As String
    Private _grup As String
    Private _totalsecure As String

    Public Property id() As String
        Get
            Return _id
        End Get
        Set(ByVal value As String)
            Me._id = value
        End Set
    End Property
    Public Property nomor() As String
        Get
            Return _nomor
        End Get
        Set(ByVal value As String)
            Me._nomor = value
        End Set
    End Property
    Public Property tipe() As String
        Get
            Return _tipe
        End Get
        Set(ByVal value As String)
            Me._tipe = value
        End Set
    End Property
    Public Property activity() As String
        Get
            Return _activity
        End Get
        Set(ByVal value As String)
            Me._activity = value
        End Set
    End Property
    Public Property pic() As String
        Get
            Return _pic
        End Get
        Set(ByVal value As String)
            Me._pic = value
        End Set
    End Property
    Public Property deliverable() As String
        Get
            Return _deliverable
        End Get
        Set(ByVal value As String)
            Me._deliverable = value
        End Set
    End Property
    Public Property start() As String
        Get
            Return _start
        End Get
        Set(ByVal value As String)
            Me._start = value
        End Set
    End Property
    Public Property enddate() As String
        Get
            Return _enddate
        End Get
        Set(ByVal value As String)
            Me._enddate = value
        End Set
    End Property
    Public Property state() As String
        Get
            Return _state
        End Get
        Set(ByVal value As String)
            Me._state = value
        End Set
    End Property
    Public Property del() As String
        Get
            Return _del
        End Get
        Set(ByVal value As String)
            Me._del = value
        End Set
    End Property
    Public Property kpiid() As String
        Get
            Return _kpiid
        End Get
        Set(ByVal value As String)
            Me._kpiid = value
        End Set
    End Property
    Public Property year() As String
        Get
            Return _year
        End Get
        Set(ByVal value As String)
            Me._year = value
        End Set
    End Property
    Public Property kpiname() As String
        Get
            Return _kpiname
        End Get
        Set(ByVal value As String)
            _kpiname = value
        End Set
    End Property
    Public Property namapic() As String
        Get
            Return _namapic
        End Get
        Set(ByVal value As String)
            _namapic = value
        End Set
    End Property
    Public Property Grup() As String
        Get
            Return _grup
        End Get
        Set(ByVal value As String)
            _grup = value
        End Set
    End Property
    Public Property totalsecure() As String
        Get
            Return _totalsecure
        End Get
        Set(ByVal value As String)
            _totalsecure = value
        End Set
    End Property

End Class

Public Class tobjact
    Private _id As String
    Private _nomor As String
    Private _tipe As String
    Private _activity As String
    Private _pic As String
    Private _deliverable As String
    Private _start As String
    Private _enddate As String
    Private _state As String
    Private _del As String
    Private _kpiid As String
    Private _year As String
    Private _namapic As String
    Private _week As String
    Private _progress As String
    Private _accuracy As String
    Private _pi As String
    Private _ca As String
    Private _tstate As String
    Private _progressbefore As String

    Public Property id() As String
        Get
            Return _id
        End Get
        Set(ByVal value As String)
            Me._id = value
        End Set
    End Property
    Public Property nomor() As String
        Get
            Return _nomor
        End Get
        Set(ByVal value As String)
            Me._nomor = value
        End Set
    End Property
    Public Property tipe() As String
        Get
            Return _tipe
        End Get
        Set(ByVal value As String)
            Me._tipe = value
        End Set
    End Property
    Public Property activity() As String
        Get
            Return _activity
        End Get
        Set(ByVal value As String)
            Me._activity = value
        End Set
    End Property
    Public Property pic() As String
        Get
            Return _pic
        End Get
        Set(ByVal value As String)
            Me._pic = value
        End Set
    End Property
    Public Property deliverable() As String
        Get
            Return _deliverable
        End Get
        Set(ByVal value As String)
            Me._deliverable = value
        End Set
    End Property
    Public Property start() As String
        Get
            Return _start
        End Get
        Set(ByVal value As String)
            Me._start = value
        End Set
    End Property
    Public Property enddate() As String
        Get
            Return _enddate
        End Get
        Set(ByVal value As String)
            Me._enddate = value
        End Set
    End Property
    Public Property state() As String
        Get
            Return _state
        End Get
        Set(ByVal value As String)
            Me._state = value
        End Set
    End Property
    Public Property del() As String
        Get
            Return _del
        End Get
        Set(ByVal value As String)
            Me._del = value
        End Set
    End Property
    Public Property kpiid() As String
        Get
            Return _kpiid
        End Get
        Set(ByVal value As String)
            Me._kpiid = value
        End Set
    End Property
    Public Property year() As String
        Get
            Return _year
        End Get
        Set(ByVal value As String)
            Me._year = value
        End Set
    End Property
    Public Property namapic() As String
        Get
            Return _namapic
        End Get
        Set(ByVal value As String)
            _namapic = value
        End Set
    End Property
    Public Property week() As String
        Get
            Return _week
        End Get
        Set(ByVal value As String)
            _week = value
        End Set
    End Property
    Public Property progress() As String
        Get
            Return _progress
        End Get
        Set(ByVal value As String)
            _progress = value
        End Set
    End Property
    Public Property accuracy() As String
        Get
            Return _accuracy
        End Get
        Set(ByVal value As String)
            _accuracy = value
        End Set
    End Property
    Public Property pi() As String
        Get
            Return _pi
        End Get
        Set(ByVal value As String)
            _pi = value
        End Set
    End Property
    Public Property ca() As String
        Get
            Return _ca
        End Get
        Set(ByVal value As String)
            _ca = value
        End Set
    End Property
    Public Property tstate() As String
        Get
            Return _tstate
        End Get
        Set(ByVal value As String)
            _tstate = value
        End Set
    End Property
    Public Property progressbefore() As String
        Get
            Return _progressbefore
        End Get
        Set(ByVal value As String)
            _progressbefore = value
        End Set
    End Property
End Class

Public Class BSC
    Private _target As String
    Private _actual As String
    Private _achievement As String
    Private _kpiid As String
    Private _year As String
    Private _month As String
    Public Property Target() As String
        Get
            Return _target
        End Get
        Set(ByVal value As String)
            _target = value
        End Set
    End Property
    Public Property Actual() As String
        Get
            Return _actual
        End Get
        Set(ByVal value As String)
            _actual = value
        End Set
    End Property
    Public Property Achievement() As String
        Get
            Return _achievement
        End Get
        Set(ByVal value As String)
            _achievement = value
        End Set
    End Property
    Public Property Kpiid() As String
        Get
            Return _kpiid
        End Get
        Set(ByVal value As String)
            _kpiid = value
        End Set
    End Property
    Public Property Year() As String
        Get
            Return _year
        End Get
        Set(ByVal value As String)
            _year = value
        End Set
    End Property
    Public Property Month() As String
        Get
            Return _month
        End Get
        Set(ByVal value As String)
            _month = value
        End Set
    End Property


End Class

Public Class TP
    Private _pid As String
    Private _pname As String
    Private _pbobot As Decimal
    Private _pstate As String
    Public Property Pid() As String
        Get
            Return _pid
        End Get
        Set(ByVal value As String)
            _pid = value
        End Set
    End Property
    Public Property Pname() As String
        Get
            Return _pname
        End Get
        Set(ByVal value As String)
            _pname = value
        End Set
    End Property
    Public Property Pbobot() As Decimal
        Get
            Return _pbobot
        End Get
        Set(ByVal value As Decimal)
            _pbobot = value
        End Set
    End Property
    Public Property Pstate() As String
        Get
            Return _pstate
        End Get
        Set(ByVal value As String)
            _pstate = value
        End Set
    End Property
End Class

Public Class BSCLampiran
    Private _NmFile As String
    Private _Nomor As String

    Public Property NmFile() As String
        Get
            Return _NmFile
        End Get
        Set(ByVal value As String)
            _NmFile = value
        End Set
    End Property
    Public Property Nomor() As String
        Get
            Return _Nomor
        End Get
        Set(ByVal value As String)
            _Nomor = value
        End Set
    End Property
End Class

Public Class otarget
    Private _versi As String
    Private _tahun As String
    Private _kpiid As String
    Private _calctype As String
    Private _jan As String
    Private _feb As String
    Private _mar As String
    Private _apr As String
    Private _mei As String
    Private _jun As String
    Private _jul As String
    Private _agu As String
    Private _sep As String
    Private _okt As String
    Private _nov As String
    Private _des As String
    Private _createdby As String
    Private _createdin As String
    Private _at As String

    Public Property Versi() As String
        Get
            Return _versi
        End Get
        Set(ByVal value As String)
            _versi = value
        End Set
    End Property
    Public Property Tahun() As String
        Get
            Return _tahun
        End Get
        Set(ByVal value As String)
            _tahun = value
        End Set
    End Property
    Public Property Kpiid() As String
        Get
            Return _kpiid
        End Get
        Set(ByVal value As String)
            _kpiid = value
        End Set
    End Property
    Public Property Calctype() As String
        Get
            Return _calctype
        End Get
        Set(ByVal value As String)
            _calctype = value
        End Set
    End Property
    Public Property Jan() As String
        Get
            Return _jan
        End Get
        Set(ByVal value As String)
            _jan = value
        End Set
    End Property
    Public Property Feb() As String
        Get
            Return _feb
        End Get
        Set(ByVal value As String)
            _feb = value
        End Set
    End Property
    Public Property Mar() As String
        Get
            Return _mar
        End Get
        Set(ByVal value As String)
            _mar = value
        End Set
    End Property
    Public Property Apr() As String
        Get
            Return _apr
        End Get
        Set(ByVal value As String)
            _apr = value
        End Set
    End Property
    Public Property Mei() As String
        Get
            Return _mei
        End Get
        Set(ByVal value As String)
            _mei = value
        End Set
    End Property
    Public Property Jun() As String
        Get
            Return _jun
        End Get
        Set(ByVal value As String)
            _jun = value
        End Set
    End Property
    Public Property Jul() As String
        Get
            Return _jul
        End Get
        Set(ByVal value As String)
            _jul = value
        End Set
    End Property
    Public Property Agu() As String
        Get
            Return _agu
        End Get
        Set(ByVal value As String)
            _agu = value
        End Set
    End Property
    Public Property Sep() As String
        Get
            Return _sep
        End Get
        Set(ByVal value As String)
            _sep = value
        End Set
    End Property
    Public Property Okt() As String
        Get
            Return _okt
        End Get
        Set(ByVal value As String)
            _okt = value
        End Set
    End Property
    Public Property Nov() As String
        Get
            Return _nov
        End Get
        Set(ByVal value As String)
            _nov = value
        End Set
    End Property
    Public Property Des() As String
        Get
            Return _des
        End Get
        Set(ByVal value As String)
            _des = value
        End Set
    End Property
    Public Property Createdby() As String
        Get
            Return _createdby
        End Get
        Set(ByVal value As String)
            _createdby = value
        End Set
    End Property
    Public Property Createdin() As String
        Get
            Return _createdin
        End Get
        Set(ByVal value As String)
            _createdin = value
        End Set
    End Property
    Public Property At() As String
        Get
            Return _at
        End Get
        Set(ByVal value As String)
            _at = value
        End Set
    End Property


End Class

Public Class Formula
    Private _id As String
    Private _kpiid As String
    Private _name As String
    Private _source As String
    Private _responsibility As String
    Private _prepared As String
    Private _cutofdate As String
    Public Property Id() As String
        Get
            Return _id
        End Get
        Set(ByVal value As String)
            _id = value
        End Set
    End Property
    Public Property Kpiid() As String
        Get
            Return _kpiid
        End Get
        Set(ByVal value As String)
            _kpiid = value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property
    Public Property Source() As String
        Get
            Return _source
        End Get
        Set(ByVal value As String)
            _source = value
        End Set
    End Property
    Public Property Responsibility() As String
        Get
            Return _responsibility
        End Get
        Set(ByVal value As String)
            _responsibility = value
        End Set
    End Property
    Public Property Prepared() As String
        Get
            Return _prepared
        End Get
        Set(ByVal value As String)
            _prepared = value
        End Set
    End Property
    Public Property Cutofdate() As String
        Get
            Return _cutofdate
        End Get
        Set(ByVal value As String)
            _cutofdate = value
        End Set
    End Property

End Class

Public Class Employee
    Private _nik As String
    Private _name As String

    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property
    Public Property Nik() As String
        Get
            Return _nik
        End Get
        Set(ByVal value As String)
            _nik = value
        End Set
    End Property
End Class

Public Class ProgressAct
    Private _id As String
    Private _progress As String

    Public Property Id() As String
        Get
            Return _id
        End Get
        Set(ByVal value As String)
            _id = value
        End Set
    End Property
    Public Property Progress() As String
        Get
            Return _progress
        End Get
        Set(ByVal value As String)
            _progress = value
        End Set
    End Property
End Class

Public Class GanttData
    Private _jenis As String
    Private _activity As String
    Private _ptglstart As String
    Private _ptglend As String
    Private _atglstart As String
    Private _atglend As String
    Private _pic As String
    Private _kpi As String
    Private _status As String
    Private _logprogress As String
    Private _deliverables As String
    Private _no As String


    Public Property Deliverables() As String
        Get
            Return _deliverables
        End Get
        Set(ByVal value As String)
            _deliverables = value
        End Set
    End Property
    Public Property No() As String
        Get
            Return _no
        End Get
        Set(ByVal value As String)
            _no = value
        End Set
    End Property
    Public Property Activity() As String
        Get
            Return _activity
        End Get
        Set(ByVal value As String)
            _activity = value
        End Set
    End Property
    Public Property Atglend() As String
        Get
            Return _atglend
        End Get
        Set(ByVal value As String)
            _atglend = value
        End Set
    End Property
    Public Property Atglstart() As String
        Get
            Return _atglstart
        End Get
        Set(ByVal value As String)
            _atglstart = value
        End Set
    End Property
    Public Property Jenis() As String
        Get
            Return _jenis
        End Get
        Set(ByVal value As String)
            _jenis = value
        End Set
    End Property
    Public Property Kpi() As String
        Get
            Return _kpi
        End Get
        Set(ByVal value As String)
            _kpi = value
        End Set
    End Property
    Public Property Logprogress() As String
        Get
            Return _logprogress
        End Get
        Set(ByVal value As String)
            _logprogress = value
        End Set
    End Property
    Public Property Pic() As String
        Get
            Return _pic
        End Get
        Set(ByVal value As String)
            _pic = value
        End Set
    End Property
    Public Property Ptglend() As String
        Get
            Return _ptglend
        End Get
        Set(ByVal value As String)
            _ptglend = value
        End Set
    End Property
    Public Property Ptglstart() As String
        Get
            Return _ptglstart
        End Get
        Set(ByVal value As String)
            _ptglstart = value
        End Set
    End Property
    Sub New()

    End Sub

    Sub New(ByVal jenis As String, ByVal activity As String, ByVal ptglstart As String, ByVal ptglend As String, ByVal atglstart As String, ByVal atglend As String)
        Me._jenis = jenis
        Me._activity = activity
        Me._ptglstart = ptglstart
        Me._ptglend = ptglend
        Me._atglstart = atglstart
        Me._atglend = atglend
    End Sub
    Public Property Status() As String
        Get
            Return _status
        End Get
        Set(ByVal value As String)
            _status = value
        End Set
    End Property



End Class