﻿Imports System.Data.SqlClient

Partial Public Class lockactivityplan
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        BindGrid()
    End Sub

    Private Sub srchResult_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles srchResult.PageIndexChanging
        BindGrid()
        srchResult.PageIndex = e.NewPageIndex
        srchResult.DataBind()
    End Sub

    Sub BindGrid()
        Try
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)

            'Dim query As String = "SELECT Nik as NIK,Nama,NmDepar as Departemen,Site = case KdLevel when 5 Then KdLokasi when 6 Then KdLokasi When 7 Then KdLokasi else KdSite end FROM H_A101 LEFT JOIN H_A130 ON H_A101.KdDepar = H_A130.KdDepar WHERE Active = '1' AND Nama like '%" & Replace(srchTextBox.Text, "'", "") & "%'"
            'Dim query As String = "SELECT PERNR as NIK,Cname as Nama,ZDEP as Departemen,WERKS_TEXT as site FROM H_A101_SAP WHERE PERSG = '1' AND CName like '%" & Replace(srchTextBox.Text, "'", "") & "%'"
            Dim query As String = "SELECT Nik as NIK,Nama,NmDepar as Departemen, KdSite as site FROM H_A101 LEFT JOIN H_A130 ON H_A101.KdDepar = H_A130.KdDepar WHERE Active = '1' AND Nama like '%" & Replace(srchTextBox.Text, "'", "") & "%'"

            Dim cmd As SqlCommand
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(query, conn)
            cmd = New SqlCommand(query, conn)
            conn.Open()
            da.Fill(dt)
            conn.Close()
            srchResult.DataSource = dt
            srchResult.DataBind()
        Catch ex As Exception
            lblKet.Text = ex.Message
        End Try
    End Sub

    Private Sub srchResult_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles srchResult.RowDataBound
        Try
            If e.Row.RowIndex <> -1 Then
                e.Row.Attributes.Add("ondblclick", "javascript:executeClose('" & e.Row.Cells(0).Text & "','" & e.Row.Cells(1).Text & "')")
            End If
        Catch ex As Exception
            lblKet.Text = ex.Message
        End Try
    End Sub
End Class