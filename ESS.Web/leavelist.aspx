﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="leavelist.aspx.vb" Inherits="EXCELLENT.leavelist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Leave List
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
<caption style="text-align: center; font-size: 1.5em; color:White;">Leave List</caption>
<tr>
<td style="width:10%"> From
    <asp:DropDownList ID="ddlbulan1" runat="server">
    <asp:ListItem Value="01">January</asp:ListItem>
    <asp:ListItem Value="02">February</asp:ListItem>
    <asp:ListItem Value="03">March</asp:ListItem>
    <asp:ListItem Value="04">April</asp:ListItem>
    <asp:ListItem Value="05">May</asp:ListItem>
    <asp:ListItem Value="06">June</asp:ListItem>
    <asp:ListItem Value="07">July</asp:ListItem>
    <asp:ListItem Value="08">August</asp:ListItem>
    <asp:ListItem Value="09">September</asp:ListItem>
    <asp:ListItem Value="10">October</asp:ListItem>
    <asp:ListItem Value="11">November</asp:ListItem>
    <asp:ListItem Value="12">December</asp:ListItem>
 </asp:DropDownList>
</td>

<td> <asp:TextBox ID="dtfrom" runat="server" Visible="false" ></asp:TextBox> <asp:Button ID="btnsrcdt" runat = "server" Text = "Find Date" Visible="false"  />
    <asp:Calendar ID="Calendar1" runat="server" Visible="false" style="position:absolute; top:100px; left:200px; background-color:White" ></asp:Calendar>
 </td>
<td style="width:10%"> To
<asp:DropDownList ID="ddlbulan2" runat="server">
    <asp:ListItem Value="01">January</asp:ListItem>
    <asp:ListItem Value="02">February</asp:ListItem>
    <asp:ListItem Value="03">March</asp:ListItem>
    <asp:ListItem Value="04">April</asp:ListItem>
    <asp:ListItem Value="05">May</asp:ListItem>
    <asp:ListItem Value="06">June</asp:ListItem>
    <asp:ListItem Value="07">July</asp:ListItem>
    <asp:ListItem Value="08">August</asp:ListItem>
    <asp:ListItem Value="09">September</asp:ListItem>
    <asp:ListItem Value="10">October</asp:ListItem>
    <asp:ListItem Value="11">November</asp:ListItem>
    <asp:ListItem Value="12">December</asp:ListItem>
 </asp:DropDownList>
</td>
<td>
    
</td>
<td style="width:10%">Year
<asp:DropDownList ID="ddlyear" runat="server" >
        <%--<asp:ListItem Value="2011">2011</asp:ListItem>
        <asp:ListItem Value="2012">2012</asp:ListItem>
        <asp:ListItem Value="2013">2013</asp:ListItem>--%>
    </asp:DropDownList>
</td>
<td>
    
</td>
<td style="width:10%;"><asp:Button ID="btnSearch" runat="server" Text="Search"/></td>
<td style="width:60%;"><input type="button" id="cetak" value="Print" onclick="frmcetak()" style="width: 80px; font-family: Calibri; visibility:hidden;" /></td>
</tr></table>

<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="transid" AllowPaging="true" PageSize="100" Width="1200px" 
        Font-Size="Small">
<Columns>
    <asp:CommandField ShowSelectButton="True" SelectText="Select" ButtonType="Button"  
        HeaderStyle-Width="40px" ItemStyle-VerticalAlign="top" >
    <HeaderStyle Width="40px"></HeaderStyle>
    
    

    <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:CommandField>
   
    
    <asp:BoundField DataField="niksite" HeaderText="NIK" 
        HeaderStyle-Width="50px"  ItemStyle-VerticalAlign="top">
        <HeaderStyle Width="50px"></HeaderStyle>

        <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    <asp:BoundField DataField="Name" HeaderText="Name" 
        HeaderStyle-Width="150px"  ItemStyle-VerticalAlign="top">
        <HeaderStyle Width="150px"></HeaderStyle>

        <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    <asp:BoundField DataField="transid" HeaderText="Transid" 
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
        <HeaderStyle Width="100px"></HeaderStyle>

        <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    <asp:BoundField DataField="trans_date" HeaderText="Date" 
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
        <HeaderStyle Width="100px"></HeaderStyle>

        <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    <asp:BoundField DataField="typedesc" HeaderText="Leave Type" 
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
        <HeaderStyle Width="100px"></HeaderStyle>

        <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    <asp:BoundField DataField="paidltypeDesc" HeaderText="Paid Leave Type" 
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
        <HeaderStyle Width="100px"></HeaderStyle>

        <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    <asp:BoundField DataField="datefrom" HeaderText="From" 
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
        <HeaderStyle Width="100px"></HeaderStyle>

        <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    <asp:BoundField DataField="dateto" HeaderText="To" 
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
        <HeaderStyle Width="100px"></HeaderStyle>

        <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    <asp:BoundField DataField="totleave" HeaderText="Leave" 
        HeaderStyle-Width="50px"  ItemStyle-VerticalAlign="top" DataFormatString="{0:#.##}">
        <HeaderStyle Width="50px"></HeaderStyle>

        <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    <asp:BoundField DataField="fstatus" HeaderText="Status" 
        HeaderStyle-Width="20px"  ItemStyle-VerticalAlign="top">
        <HeaderStyle Width="20px"></HeaderStyle>

        <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
   
    <asp:BoundField DataField="remarks" HeaderText="Remarks" 
        HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
        <HeaderStyle Width="100px"></HeaderStyle>

        <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    <asp:BoundField DataField="kdsite" HeaderText="Site" 
        HeaderStyle-Width="50px"  ItemStyle-VerticalAlign="top">
        <HeaderStyle Width="50px"></HeaderStyle>

        <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    
    <asp:BoundField DataField="NxtAction" HeaderText="Next Action" 
        HeaderStyle-Width="70px"  ItemStyle-VerticalAlign="top">
        <HeaderStyle Width="70px"></HeaderStyle>

        <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    
    <asp:BoundField DataField="picUserRole" HeaderText="PIC User Role" 
        HeaderStyle-Width="70px"  ItemStyle-VerticalAlign="top">
        <HeaderStyle Width="70px"></HeaderStyle>

        <ItemStyle VerticalAlign="Top"></ItemStyle>
    </asp:BoundField>
    
    <asp:HyperLinkField DataNavigateUrlFields="transid" ControlStyle-CssClass="PopUpViewHistory"
                DataNavigateUrlFormatString="leave_history.aspx?no={0}" 
                Text="View History" />
    
</Columns> 
</asp:GridView> 

<script>

    $('a.PopUpViewHistory').click(function() {


    newwindow = window.open($(this).attr('href'), "History Work Flow", 'height=300,width=800');
        if (window.focus) { newwindow.focus() }
        return false
        ;
    });
    
</script>
</asp:Content> 