﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="login_ppa.aspx.vb" Inherits="EXCELLENT.login_ppa" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PPA Login</title>
    <style type="text/css">


body{
  padding: 1em;
  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAAQElEQVQIW2P89OvDfwYo+PHjJ4zJwMHBzsAIk0SXAKkCS2KTAEu++vQSbizIKGQAl0SXAJkGlsQmAbcT2Shk+wH0sCzAEOZW1AAAAABJRU5ErkJggg==);
}
a{
  color: #739931;
}
.page{
  max-width: 70em;
  margin: 0 auto;
}
table th,
table td{
  text-align: left;
}
table.layout{
  width: 100%;
  border-collapse: collapse;
}
table.display{
  margin: 1em 0;
}
table.display th,
table.display td{
  border: 1px solid #B3BFAA;
  padding: .5em 1em;
}

table.display th{ background: #D5E0CC; }
table.display td{ background: #fff; }

table.responsive-table{
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
}

.center{
  text-align:center;
  }
.none{
	border-style:none;
    text-align:center;
  }

@media (max-width: 30em){
    table.responsive-table{
      box-shadow: none;  
    }
    table.responsive-table thead{
      display: none; 
    }
  table.display th,
  table.display td{
    padding: .5em;
  }
    
  table.responsive-table td:nth-child(1):before{
    content: 'Number';
  }
  table.responsive-table td:nth-child(2):before{
    content: 'Name';
  }
  table.responsive-table td:nth-child(1),
  table.responsive-table td:nth-child(2){
    padding-left: 25%;
  }
  table.responsive-table td:nth-child(1):before,
  table.responsive-table td:nth-child(2):before{
    position: absolute;
    left: .5em;
    font-weight: bold;
  }
  
    table.responsive-table tr,
    table.responsive-table td{
        display: block;
    }
    table.responsive-table tr{
        position: relative;
        margin-bottom: 1em;
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
    }
    table.responsive-table td{
        border-top: none;
    }
    table.responsive-table td.organisationnumber{
        background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    table.responsive-table td.actions{
        position: absolute;
        top: 0;
        right: 0;
        border: none;
        background: none;
    }
    table.responsive-table td.top{
    	background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
}

</style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="padding-bottom:20px;">
            <br /><center>        
       <div><center>
       <table class="responsive-table" style="border: 1px solid #D5E0CC; width: 25%">
            <tr>
            <td colspan="2" bgcolor="#D5E0CC" 
                    
                    style="text-align:center;font-size: large; font-family: calibri; font-weight: bold; width:100%">
                PPA LOGIN
            </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label1" runat="server" Text="Domain\UserName" Font-Size="Small">
                </asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox1" runat="server" class="tb10" Width="92%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label2" runat="server" Text="Label" Font-Size="Small">Password</asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox2" TextMode="password" runat="server" class="tb10" Width="92%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">v.1.0.0</td>                
            </tr>
            <tr>    
                <td style="width:40%">
                    <img alt="" src="../images/Locked.png" />
                </td>
                <td style="width:60%">
                    <asp:Button ID="btnLogin" runat="server" Text="Login" Width="80" Font-Names="Calibri"/>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="80" Font-Names="Calibri" />
                </td>
            </tr>
            <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblConfirm" runat="server" Text="" Font-Size="Small" ForeColor="Red"></asp:Label>
            </td>
            </tr>
        </table> </center> 
    </div>
    </center>
    <br />
    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <!-- use jssor.slider.mini.js (40KB) or jssor.sliderc.mini.js (32KB, with caption, no slideshow) or jssor.sliders.mini.js (28KB, no caption, no slideshow) instead for release -->
    <!-- jssor.slider.mini.js = jssor.sliderc.mini.js = jssor.sliders.mini.js = (jssor.core.js + jssor.utils.js + jssor.slider.js) -->
    <script type="text/javascript" src="js/jssor.core.js"></script>
    <script type="text/javascript" src="js/jssor.utils.js"></script>
    <script type="text/javascript" src="js/jssor.slider.js"></script>
    <script type="text/javascript" >
        jQuery(document).ready(function ($) {
            var options = {
                $AutoPlay: false,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 5, 					                //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $ThumbnailNavigatorOptions: {
                    $Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always

                    $ActionMode: 1,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                    $AutoCenter: 3,                             //[Optional] Auto center thumbnail items in the thumbnail navigator container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 3
                    $Lanes: 1,                                      //[Optional] Specify lanes to arrange thumbnails, default value is 1
                    $SpacingX: 0,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    $SpacingY: 0,                                   //[Optional] Vertical space between each thumbnail in pixel, default value is 0
                    $DisplayPieces: 5,                              //[Optional] Number of pieces to display, default value is 1
                    $ParkingPosition: 0,                            //[Optional] The offset position to park thumbnail
                    $Orientation: 1,                                //[Optional] Orientation to arrange thumbnails, 1 horizental, 2 vertical, default value is 1
                    $DisableDrag: true                              //[Optional] Disable drag or not, default value is false
                }
            };

            var jssor_slider2 = new $JssorSlider$("slider2_container", options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var parentWidth = jssor_slider2.$Elmt.parentNode.clientWidth;
                if (parentWidth) {
                    var sliderWidth = parentWidth;

                    //keep the slider width no more than 602
                    sliderWidth = Math.min(sliderWidth, 602);

                    jssor_slider2.$SetScaleWidth(sliderWidth);
                }
                else
                    window.setTimeout(ScaleSlider, 30);
            }

            ScaleSlider();

            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                $(window).bind('resize', ScaleSlider);
            }


            //if (navigator.userAgent.match(/(iPhone|iPod|iPad)/)) {
            //    $(window).bind("orientationchange", ScaleSlider);
            //}
            //responsive code end
        });
    </script>
    <!-- Jssor Slider Begin -->
    <!-- You can move inline styles to css file or css block. -->
    <center>
    <div id="slider2_container" style="position: relative; top: 0px; left: 0px; width:602px; height: 162px; background: #fff; overflow: hidden; ">
        
        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 29px; width: 600px; height: 160px; border: 1px solid gray; -webkit-filter: blur(0px); background-color: #fff; overflow: hidden;">
            <div>
                <div style="margin: 10px; overflow: hidden; color: #000;">
                <center>
                Sample Login HO
                    <table class="responsive-table" style="border: 1px solid #D5E0CC; width: 50%; margin-top:10px;">
            <tr>
            <td colspan="2" bgcolor="#D5E0CC" 
                    
                    style="text-align:center;font-size: large; font-family: calibri; font-weight: bold; width:100%">
                PPA LOGIN
            </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label3" runat="server" Text="Domain\UserName" Font-Size="Small">
                </asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox3" runat="server" class="tb10" Width="92%" Enabled="false">jresources\your.name</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label4" runat="server" Text="Password" Font-Size="Small" ></asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox4" runat="server" class="tb10" Width="92%" Enabled="false" >your password</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">v.1.0.0</td>                
            </tr>
            <tr>    
                <td style="width:40%">
                    <img alt="" src="../images/Locked.png" />
                </td>
               
            </tr>
            <tr>
            <td colspan="2" align="center">
                <asp:Label ID="Label5" runat="server" Text="" Font-Size="Small" ForeColor="Red"></asp:Label>
            </td>
            </tr>
        </table>
            </center>
                </div>
                <div u="thumb">Login HO</div>
            </div>
            <div>
                <div style="margin: 10px; overflow: hidden; color: #000;">
                    <center>
                Sample Login Lanut / Bakan
                    <table class="responsive-table" style="border: 1px solid #D5E0CC; width: 50%; margin-top:10px;">
            <tr>
            <td colspan="2" bgcolor="#D5E0CC" 
                    
                    style="text-align:center;font-size: large; font-family: calibri; font-weight: bold; width:100%">
                PPA LOGIN
            </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label6" runat="server" Text="Domain\UserName" Font-Size="Small">
                </asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox5" runat="server" class="tb10" Width="92%" Enabled="false">JRBM\your.name</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label7" runat="server" Text="Password" Font-Size="Small" ></asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox6" runat="server" class="tb10" Width="92%" Enabled="false" >your password</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">v.1.0.0</td>                
            </tr>
            <tr>    
                <td style="width:40%">
                    <img alt="" src="../images/Locked.png" />
                </td>
               
            </tr>
            <tr>
            <td colspan="2" align="center">
                <asp:Label ID="Label8" runat="server" Text="" Font-Size="Small" ForeColor="Red"></asp:Label>
            </td>
            </tr>
        </table>
            </center>
                </div>
                <div u="thumb">Login Lanut / Bakan</div>
            </div>
            <div>
                <div style="margin: 10px; overflow: hidden; color: #000;">
                    <center>
                Sample Login Seruyung
                    <table class="responsive-table" style="border: 1px solid #D5E0CC; width: 50%; margin-top:10px;">
            <tr>
            <td colspan="2" bgcolor="#D5E0CC" 
                    
                    style="text-align:center;font-size: large; font-family: calibri; font-weight: bold; width:100%">
                PPA LOGIN
            </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label9" runat="server" Text="Domain\UserName" Font-Size="Small">
                </asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox7" runat="server" class="tb10" Width="92%" Enabled="false">SPP\your.name</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label10" runat="server" Text="Password" Font-Size="Small" ></asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox8" runat="server" class="tb10" Width="92%" Enabled="false" >your password</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">v.1.0.0</td>                
            </tr>
            <tr>    
                <td style="width:40%">
                    <img alt="" src="../images/Locked.png" />
                </td>
            </tr>
            <tr>
            <td colspan="2" align="center">
                <asp:Label ID="Label11" runat="server" Text="" Font-Size="Small" ForeColor="Red"></asp:Label>
            </td>
            </tr>
        </table>
            </center>
                </div>
                <div u="thumb">Seruyung</div>
            </div>
            <div>
                <div style="margin: 10px; overflow: hidden; color: #000;">
                    <center>
                Sample Login Penjom
                    <table class="responsive-table" style="border: 1px solid #D5E0CC; width: 50%; margin-top:10px;">
            <tr>
            <td colspan="2" bgcolor="#D5E0CC" 
                    
                    style="text-align:center;font-size: large; font-family: calibri; font-weight: bold; width:100%">
                PPA LOGIN
            </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label12" runat="server" Text="Domain\UserName" Font-Size="Small">
                </asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox9" runat="server" class="tb10" Width="92%" Enabled="false">SRSB\your.name</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label13" runat="server" Text="Password" Font-Size="Small" ></asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox10" runat="server" class="tb10" Width="92%" Enabled="false" >your password</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">v.1.0.0</td>                
            </tr>
            <tr>    
                <td style="width:40%">
                    <img alt="" src="../images/Locked.png" />
                </td>
               
            </tr>
            <tr>
            <td colspan="2" align="center">
                <asp:Label ID="Label14" runat="server" Text="" Font-Size="Small" ForeColor="Red"></asp:Label>
            </td>
            </tr>
        </table>
            </center>
                </div>
                <div u="thumb">Penjom</div>
            </div>
            <div>
                <div style="margin: 10px; overflow: hidden; color: #000;">
                    <center>Best View In :</center>
                        <br />
                    <center>
                        <img src="Image/ie8logo.jpg" alt="ie8-logo"/></center>
                </div>
                <div u="thumb">Best View In :</div>
            </div>
        </div>

        <!-- ThumbnailNavigator Skin Begin -->
        <div u="thumbnavigator" class="jssort12" style="position: absolute; width: 500px; height: 30px; left:0px; top: 0px;">
            <!-- Thumbnail Item Skin Begin -->
            <style>
                /* jssor slider thumbnail navigator skin 12 css */
                /*
                .jssort12 .p            (normal)
                .jssort12 .p:hover      (normal mouseover)
                .jssort12 .pav          (active)
                .jssort12 .pav:hover    (active mouseover)
                .jssort12 .pdn          (mousedown)
                */
                .jssort12 .w, .jssort12 .phv .w
                {
                	cursor: pointer;
                	position: absolute;
                	WIDTH: 99px;
                	HEIGHT: 28px;
                	border: 1px solid gray;
                	top: 0px;
                	left: -1px;
                }
                .jssort12 .pav .w, .jssort12 .pdn .w
                {
                	border-bottom: 1px solid #fff;
                }
                .jssort12 .c
                {
                    color: #000;
                    font-size:13px;             	
                }
                .jssort12 .p .c, .jssort12 .pav:hover .c
                {
                	background-color:#eee;
                }
                .jssort12 .pav .c, .jssort12 .p:hover .c, .jssort12 .phv .c
                {
                	background-color:#fff;
                }
            </style>
            <div u="slides" style="cursor: move; top:0px; left:0px; border-left: 1px solid gray;">
                <div u="prototype" class="p" style="POSITION: absolute; WIDTH: 100px; HEIGHT: 30px; TOP: 0; LEFT: 0; padding:0px;">
                    <div class=w><ThumbnailTemplate class="c" style=" WIDTH: 100%; HEIGHT: 100%; position:absolute; TOP: 0; LEFT: 0; line-height:28px; text-align:center;"></ThumbnailTemplate></div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
        <!-- ThumbnailNavigator Skin End -->

        <a style="display: none" href="http://www.jssor.com">jquery responsive slider</a>
    </div>
    </center>
    <!-- Jssor Slider End -->
    
    <div style="margin-top:-100px; position:relative; top:120px; bottom:0px; clear:both;"><center><img style="width:796px; height:190px;" src="Image/picture1.png"/></center></div>
    </form>
</body>
</html>
