﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="rptOT_plan.aspx.vb" Inherits="EXCELLENT.rptOT_plan" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" Height="811px" Width="734px">
            <LocalReport ReportPath="Laporan\ot_report_person.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                        Name="SPLDataSet_H_H105" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            DeleteMethod="Delete" InsertMethod="Insert" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            TypeName="EXCELLENT.SPLDataSetTableAdapters.H_H105TableAdapter" 
            UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_Nik" Type="String" />
                <asp:Parameter Name="Original_TglSchedule" Type="DateTime" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Periode" Type="String" />
                <asp:Parameter Name="KdSite" Type="String" />
                <asp:Parameter Name="KdGroup" Type="String" />
                <asp:Parameter Name="Shift" Type="String" />
                <asp:Parameter Name="KdDepar" Type="String" />
                <asp:Parameter Name="KdJabat" Type="String" />
                <asp:Parameter Name="KdHari" Type="Int32" />
                <asp:Parameter Name="KdAbsen" Type="String" />
                <asp:Parameter Name="CreatedBy" Type="String" />
                <asp:Parameter Name="CreatedIn" Type="String" />
                <asp:Parameter Name="CreatedTime" Type="DateTime" />
                <asp:Parameter Name="ModifiedBy" Type="String" />
                <asp:Parameter Name="ModifiedIn" Type="String" />
                <asp:Parameter Name="ModifiedTime" Type="DateTime" />
                <asp:Parameter Name="StEdit" Type="String" />
                <asp:Parameter Name="VoidBy" Type="String" />
                <asp:Parameter Name="VoidIn" Type="String" />
                <asp:Parameter Name="VoidTime" Type="DateTime" />
                <asp:Parameter Name="DeleteBy" Type="String" />
                <asp:Parameter Name="DeleteTime" Type="DateTime" />
                <asp:Parameter Name="Original_Nik" Type="String" />
                <asp:Parameter Name="Original_TglSchedule" Type="DateTime" />
            </UpdateParameters>
            <SelectParameters>
                <asp:SessionParameter Name="date1" SessionField="otdate1" Type="DateTime" />
                <asp:SessionParameter Name="date2" SessionField="otdate2" Type="DateTime" />
                <asp:SessionParameter Name="nik" SessionField="otnik" Type="String" />
            </SelectParameters>
            <InsertParameters>
                <asp:Parameter Name="Periode" Type="String" />
                <asp:Parameter Name="Nik" Type="String" />
                <asp:Parameter Name="TglSchedule" Type="DateTime" />
                <asp:Parameter Name="KdSite" Type="String" />
                <asp:Parameter Name="KdGroup" Type="String" />
                <asp:Parameter Name="Shift" Type="String" />
                <asp:Parameter Name="KdDepar" Type="String" />
                <asp:Parameter Name="KdJabat" Type="String" />
                <asp:Parameter Name="KdHari" Type="Int32" />
                <asp:Parameter Name="KdAbsen" Type="String" />
                <asp:Parameter Name="CreatedBy" Type="String" />
                <asp:Parameter Name="CreatedIn" Type="String" />
                <asp:Parameter Name="CreatedTime" Type="DateTime" />
                <asp:Parameter Name="ModifiedBy" Type="String" />
                <asp:Parameter Name="ModifiedIn" Type="String" />
                <asp:Parameter Name="ModifiedTime" Type="DateTime" />
                <asp:Parameter Name="StEdit" Type="String" />
                <asp:Parameter Name="VoidBy" Type="String" />
                <asp:Parameter Name="VoidIn" Type="String" />
                <asp:Parameter Name="VoidTime" Type="DateTime" />
                <asp:Parameter Name="DeleteBy" Type="String" />
                <asp:Parameter Name="DeleteTime" Type="DateTime" />
            </InsertParameters>
        </asp:ObjectDataSource>
    </div>
    </form>
</body>
</html>
