﻿Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Imaging
Imports Microsoft.Reporting.WebForms

Partial Public Class vdetails
    Inherits System.Web.UI.Page

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache)
        Response.Cache.SetNoStore()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("NikUser") Is Nothing Then
            Response.Redirect("../login.aspx")
        End If
        Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache)
        Response.Cache.SetNoStore()
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim cmd As SqlCommand
        Try
            Dim id As String = Request.QueryString("id")
            Dim level As String = Request.QueryString("level")
            Dim year As String = Request.QueryString("year")
            Dim month As String = Request.QueryString("month")
            Dim arrFunc As New ArrayList()
            Dim arrJobsite As New ArrayList()
            kpiid.Value = id
            hyear.Value = year
            hmonth.Value = month
            createdby.Value = Session("NikUser")
            createdin.Value = Request.UserHostAddress
            kpiname.Value = Request.QueryString("kpiname")
            kpilevel.Value = level
            Dim sqlQuery As String = "Select * from MKPI a inner join TBSC b on a.ID = b.kpiid where b.year='" & hyear.Value & "' and b.kpiid='" & kpiid.Value & "' and b.month='" & hmonth.Value & "'"
            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter(sqlQuery, conn)
            conn.Open()
            da.Fill(dt)

            If Not dt.Rows.Count = 0 Then
                resulth.InnerHtml += "<table class=""header"" width=""100%"">"
                Select Case dt.Rows(0)("level")
                    Case "0" 'BUMA
                        resulth.InnerHtml += "<tr><th>BSC JRN</th>"
                    Case "1" 'Function
                        sqlQuery = "select functionname from MFunction where id='" & dt.Rows(0)("func") & "'"
                        cmd = New SqlCommand(sqlQuery, conn)
                        resulth.InnerHtml += "<tr><th>BSC " & cmd.ExecuteScalar() & "</th>"
                    Case "2" 'Section
                        sqlQuery = "select sectionname from MSECTION where id='" & dt.Rows(0)("section") & "'"
                        cmd = New SqlCommand(sqlQuery, conn)
                        resulth.InnerHtml += "<tr><th>BSC SECTION " & cmd.ExecuteScalar()
                        sqlQuery = "select kdsite from site where id='" & dt.Rows(0)("jobsite") & "'"
                        cmd = New SqlCommand(sqlQuery, conn)
                        resulth.InnerHtml += " SITE " & cmd.ExecuteScalar() & "</th>"
                    Case "3" 'Jobsite
                        sqlQuery = "select kdsite from site where id='" & dt.Rows(0)("jobsite") & "'"
                        cmd = New SqlCommand(sqlQuery, conn)
                        resulth.InnerHtml += "<tr><th>BSC " & cmd.ExecuteScalar() & "</th>"
                End Select
                resulth.InnerHtml += "<th>KPI : " & dt.Rows(0)("name") & "</th><th>UoM</td><th>Plan</th><th>Actual</th><th>Achievement</th></tr>"
                resulth.InnerHtml += "<tr><td>Periode :" & doMapping(hmonth.Value) & " " & hyear.Value & "</td><td>KPI Owner : " & dt.Rows(0)("Owner") & "</td><td align='center'>" & dt.Rows(0)("UoM") & "</td><td align='right'>" & dt.Rows(0)("target") & "</td><td align='right'>" & dt.Rows(0)("actual") & "</td><td>" & dt.Rows(0)("achievement") & "</td></tr>"
                resulth.InnerHtml += "</table>"
            End If
            conn.Close()
            If id Is Nothing Then
                lblMessage.Text = "ID KPI Kosong"
            ElseIf id = "" Then
                lblMessage.Text = "ID KPI Kosong"
            Else
                Select Case level
                    Case "0"
                        GetDataDirektorat(id, year, month)
                    Case "1"
                        'get data section
                        GetDataSection2(id, year)
                    Case "2"
                    Case "3"
                        GetDataSection3(id, year)
                    Case "4"
                        GetDataFunction(id, year, month)
                End Select
                GetGraphMTD(id, year, month)
                GetGraphYTD(id, year, month)
            End If
        Catch ex As Exception
            lblMessage.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Public Shared Function Pivot(ByVal dataValues As IDataReader, ByVal keyColumn As String, ByVal pivotNameColumn As String, ByVal pivotValueColumn As String) As DataTable
        Dim tmp As DataTable = New DataTable
        Dim r As DataRow
        Dim LastKey As String = "//dummy//"
        Dim pNameIndex As Integer
        Dim i As Integer
        Dim pValIndex As Integer
        Dim s As String
        Dim FirstRow As Boolean = True
        ' Add non-pivot columns to the data table:
        pValIndex = dataValues.GetOrdinal(pivotValueColumn)
        pNameIndex = dataValues.GetOrdinal(pivotNameColumn)
        i = 0
        Do While (i _
                    <= (dataValues.FieldCount - 1))
            If ((i <> pValIndex) _
                        AndAlso (i <> pNameIndex)) Then
                tmp.Columns.Add(dataValues.GetName(i), dataValues.GetFieldType(i))
            End If
            i = (i + 1)
        Loop
        r = tmp.NewRow
        ' now, fill up the table with the data:

        While dataValues.Read
            ' see if we need to start a new row
            If (dataValues(keyColumn).ToString <> LastKey) Then
                ' if this isn't the very first row, we need to add the last one to the table
                If Not FirstRow Then
                    tmp.Rows.Add(r)
                End If
                r = tmp.NewRow
                FirstRow = False
                ' Add all non-pivot column values to the new row:
                i = 0
                Do While (i _
                            <= (dataValues.FieldCount - 3))
                    r(i) = dataValues(tmp.Columns(i).ColumnName)
                    i = (i + 1)
                Loop
                LastKey = dataValues(keyColumn).ToString
            End If
            ' assign the pivot values to the proper column; add new columns if needed:
            s = dataValues(pNameIndex).ToString
            If Not tmp.Columns.Contains(s) Then
                tmp.Columns.Add(s, dataValues.GetFieldType(pValIndex))
            End If
            r(s) = dataValues(pValIndex)

        End While
        ' add that final row to the datatable:
        tmp.Rows.Add(r)
        ' Close the DataReader
        dataValues.Close()
        ' and that's it!
        Return tmp
    End Function

    Sub GetDataDirektorat(ByVal id As String, ByVal year As String, ByVal month As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Try
            sqlQuery = "select a.idparent,b.id,b.Name,c.DirektoratName,d.month as bulan,d.target as target,isnull(d.actual,0) as actual,count(child.IDChild) as jumlahanak,b.stedit,count(distinct(target.id)) as jumlahtarget from kpirelation a inner join mkpi b on a.idchild=b.id  " & _
                        "inner join MDIREKTORAT c on b.Dir = c.ID " & _
                        "left join TBSC d on b.ID=d.kpiid  left join KPIRELATION child on b.ID = child.IDParent left join TTARGET target on b.ID=target.kpiid and target.year='" & year & "' " & _
                        "where a.idparent='" & id & "' and d.year='" & year & "' and d.month='" & month & "' group by a.idparent,b.id,b.Name,c.DirektoratName,d.month,d.target,isnull(d.actual,0),b.stedit"
            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter(sqlQuery, conn)
            da.Fill(dt)
            result.InnerHtml = "<table class='pica' border=1 cellpadding=0 cellspacing=0><tr><td>Direktorat</td><td>KPI Name</td><td>Target</td><td>Actual</td><td>Action</td></tr>"
            Dim i As Integer = 1
            For Each dr As DataRow In dt.Rows
                If dr("StEdit") = "0" Then
                    If dr("jumlahtarget") = 0 Then
                        result.InnerHtml += "<tr><td>" & dr("DirektoratName") & "</td><td>" & dr("Name") & "</td><td style=""text-align:center"">N A</td><td></td>" & IIf(dr("jumlahanak") = 0, "<td></td>", "<td><a href='#' onclick='javascript:finddetail(""" & dr("id") & """" & "," & """" & year & """" & "," & """" & "divs" & i & """)'>View Detail</a></td>") & "</tr>"
                    Else
                        If IsDBNull(dr("target")) Then
                            'ada target berarti target kosong
                            result.InnerHtml += "<tr><td>" & dr("DirektoratName") & "</td><td>" & dr("Name") & "</td><td></td><td></td>" & IIf(dr("jumlahanak") = 0, "<td></td>", "<td><a href='#' onclick='javascript:finddetail(""" & dr("id") & """" & "," & """" & year & """" & "," & """" & "divs" & i & """)'>View Detail</a></td>") & "</tr>"
                        Else
                            result.InnerHtml += "<tr><td>" & dr("DirektoratName") & "</td><td>" & dr("Name") & "</td><td>" & dr("target") & "</td><td>" & dr("Actual") & "</td>" & IIf(dr("jumlahanak") = 0, "<td></td>", "<td><a href='#' onclick='javascript:finddetail(""" & dr("id") & """" & "," & """" & year & """" & "," & """" & "divs" & i & """)'>View Detail</a></td>") & "</tr>"
                        End If
                    End If
                Else
                    result.InnerHtml += "<tr style=""color:gray""><td>" & dr("DirektoratName") & "</td><td>" & dr("Name") & "</td><td colspan=""3"" style=""text-align:center"">OFF</td></tr>"
                End If

                result.InnerHtml += "<tr><td colspan='6'><div id='divs" & i & "' style='display:block;'></td></tr>"
                i += 1
            Next
            result.InnerHtml += "</table>"
        Catch ex As Exception
            lblMessage.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Sub GetDataFunction(ByVal id As String, ByVal year As String, ByVal month As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Try
            sqlQuery = "select a.idparent,b.id,b.Name,c.FunctionName,d.month as bulan,d.target as target,isnull(d.actual,0) as actual,count(child.IDChild) as jumlahanak,b.stedit,count(distinct(target.id)) as jumlahtarget from kpirelation a inner join mkpi b on a.idchild=b.id  " & _
                        "inner join MFUNCTION c on b.Func = c.ID " & _
                        "left join TBSC d on b.ID=d.kpiid  left join KPIRELATION child on b.ID = child.IDParent left join TTARGET target on b.ID=target.kpiid and target.year='" & year & "' " & _
                        "where a.idparent='" & id & "' and d.year='" & year & "' and d.month='" & month & "' group by a.idparent,b.id,b.Name,c.FunctionName,d.month,d.target,isnull(d.actual,0),b.stedit"
            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter(sqlQuery, conn)
            da.Fill(dt)
            result.InnerHtml = "<table class='pica' border=1 cellpadding=0 cellspacing=0><tr><td>Function</td><td>KPI Name</td><td>Target</td><td>Actual</td><td>Action</td></tr>"
            Dim i As Integer = 1
            For Each dr As DataRow In dt.Rows
                If dr("StEdit") = "0" Then
                    If dr("jumlahtarget") = 0 Then
                        result.InnerHtml += "<tr><td>" & dr("FunctionName") & "</td><td>" & dr("Name") & "</td><td style=""text-align:center"">N A</td><td></td>" & IIf(dr("jumlahanak") = 0, "<td></td>", "<td><a href='#' onclick='javascript:finddetail(""" & dr("id") & """" & "," & """" & year & """" & "," & """" & "divs" & i & """)'>View Detail</a></td>") & "</tr>"
                    Else
                        If IsDBNull(dr("target")) Then
                            'ada target berarti target kosong
                            result.InnerHtml += "<tr><td>" & dr("FunctionName") & "</td><td>" & dr("Name") & "</td><td></td><td></td>" & IIf(dr("jumlahanak") = 0, "<td></td>", "<td><a href='#' onclick='javascript:finddetail(""" & dr("id") & """" & "," & """" & year & """" & "," & """" & "divs" & i & """)'>View Detail</a></td>") & "</tr>"
                        Else
                            result.InnerHtml += "<tr><td>" & dr("FunctionName") & "</td><td>" & dr("Name") & "</td><td>" & dr("target") & "</td><td>" & dr("Actual") & "</td>" & IIf(dr("jumlahanak") = 0, "<td></td>", "<td><a href='#' onclick='javascript:finddetail(""" & dr("id") & """" & "," & """" & year & """" & "," & """" & "divs" & i & """)'>View Detail</a></td>") & "</tr>"
                        End If
                    End If
                Else
                    result.InnerHtml += "<tr style=""color:gray""><td>" & dr("FunctionName") & "</td><td>" & dr("Name") & "</td><td colspan=""3"" style=""text-align:center"">OFF</td></tr>"
                End If

                result.InnerHtml += "<tr><td colspan='6'><div id='divs" & i & "' style='display:block;'></td></tr>"
                i += 1
            Next
            result.InnerHtml += "</table>"
        Catch ex As Exception
            lblMessage.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub



    Sub GetDataComment(ByVal year As String, ByVal month As String, ByVal kpiid As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim sqlQuery As String = ""
        Try
            sqlQuery = "select id,year,month,kpiid,PI,CA from TCOMMENT where kpiid='" & kpiid & "' and YEAR='" & year & "' and month='" & month & "' and stedit='0' order by id"
            Dim dt As New DataTable()
            Dim da As New SqlDataAdapter(sqlQuery, conn)
            da.Fill(dt)
            comment.InnerHtml = ""


        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub

    'Sub GetDataFunction(ByVal id As String, ByVal year As String)
    '    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
    '    Dim cmd As SqlCommand
    '    Dim sqlQuery As String = ""
    '    Dim dt As New DataTable()
    '    Try
    '        Dim da As SqlDataAdapter
    '        sqlQuery = "select b.id,b.Name,c.FunctionName,d.month as bulan,isnull(d.target,0) as target,isnull(d.actual,0) as actual from kpirelation a inner join mkpi b on a.idchild=b.id " & _
    '                    "inner join MFUNCTION c on b.Func = c.ID " & _
    '                    "left join TBSC d on b.ID=d.kpiid where idparent='" & id & "' and d.year='" & year & "'"
    '        cmd = New SqlCommand()
    '        cmd.CommandType = CommandType.StoredProcedure
    '        cmd.CommandText = "crosstab2"
    '        cmd.Parameters.AddWithValue("@SQL", sqlQuery)
    '        cmd.Parameters.AddWithValue("@tpivotCol", "bulan")
    '        cmd.Parameters.AddWithValue("@Summaries", "MAX(Actual else null)")
    '        cmd.Parameters.AddWithValue("@GroupBy", "Name,functionname")
    '        cmd.Parameters.AddWithValue("@OrderBy ", "name, functionname")
    '        cmd.Connection = conn
    '        conn.Open()
    '        da = New SqlDataAdapter(cmd)
    '        da.Fill(dt)
    '        conn.Close()
    '        Dim tampung As String = ""

    '        If dt.Rows.Count <> 0 Then
    '            resultfunc.InnerHtml = "<table border='1'>"
    '        End If
    '        Dim colspan As Integer = 0
    '        For Each dc As DataColumn In dt.Columns
    '            If IsNumeric(dc.ColumnName) Then
    '                colspan += 1
    '            End If
    '        Next
    '        colspan += 1
    '        For Each dr As DataRow In dt.Rows
    '            If tampung <> dr("Name") Then
    '                tampung = dr("Name")
    '                resultfunc.InnerHtml += "<tr><td colspan='" & colspan & "'>KPI Name : " & tampung & "</td></tr>"
    '                If dt.Rows.Count <> 0 Then
    '                    resultfunc.InnerHtml += "<tr><td>Function</td>"
    '                    For Each dc As DataColumn In dt.Columns
    '                        If IsNumeric(dc.ColumnName) Then
    '                            resultfunc.InnerHtml += "<td>" & doMapping(dc.ColumnName) & "</td>"
    '                        End If
    '                    Next
    '                    resultfunc.InnerHtml += "</tr>"
    '                End If
    '            End If
    '            resultfunc.InnerHtml += "<tr>"
    '            resultfunc.InnerHtml += "<td>" & dr("FunctionName") & "</td>"
    '            For Each dc As DataColumn In dt.Columns
    '                If IsNumeric(dc.ColumnName) Then
    '                    resultfunc.InnerHtml += "<td>" & dr(dc.ColumnName) & "</td>"
    '                End If
    '            Next
    '            resultfunc.InnerHtml += "</tr>"
    '        Next
    '        If dt.Rows.Count <> 0 Then
    '            resultfunc.InnerHtml += "</table>"
    '        End If
    '    Catch ex As Exception
    '        lblMessage.Text = ex.Message
    '    Finally
    '        If conn.State = ConnectionState.Open Then
    '            conn.Close()
    '        End If
    '    End Try

    'End Sub

    'Sub GetDataSection(ByVal id As String, ByVal year As String)
    '    Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
    '    Dim cmd As SqlCommand
    '    Dim sqlQuery As String = ""
    '    Dim dt As New DataTable()
    '    Try
    '        Dim da As SqlDataAdapter
    '        sqlQuery = "select b.id,b.Name,c.kdsite  ,d.month as bulan,isnull(d.target,0) as target,isnull(d.actual,0) as actual from kpirelation a inner join mkpi b on a.idchild=b.id " & _
    '                    "inner join SITE c on b.Jobsite=c.id " & _
    '                    "left join TBSC d on b.ID=d.kpiid  " & _
    '                    "where idparent in (select idchild from kpirelation where idparent='" & id & "') and d.year='" & year & "'"
    '        cmd = New SqlCommand()
    '        cmd.CommandType = CommandType.StoredProcedure
    '        cmd.CommandText = "crosstab2"
    '        cmd.Parameters.AddWithValue("@SQL", sqlQuery)
    '        cmd.Parameters.AddWithValue("@tpivotCol", "bulan")
    '        cmd.Parameters.AddWithValue("@Summaries", "MAX(Actual else null)")
    '        cmd.Parameters.AddWithValue("@GroupBy", "Name,kdsite")
    '        cmd.Parameters.AddWithValue("@OrderBy ", "Name,kdsite")
    '        cmd.Connection = conn
    '        conn.Open()
    '        da = New SqlDataAdapter(cmd)
    '        da.Fill(dt)
    '        conn.Close()
    '        Dim tampung As String = ""

    '        If dt.Rows.Count <> 0 Then
    '            result.InnerHtml = "<table border='1'>"
    '        End If
    '        Dim colspan As Integer = 0
    '        For Each dc As DataColumn In dt.Columns
    '            If IsNumeric(dc.ColumnName) Then
    '                colspan += 1
    '            End If
    '        Next
    '        colspan += 1
    '        For Each dr As DataRow In dt.Rows
    '            If tampung <> dr("Name") Then
    '                tampung = dr("Name")
    '                result.InnerHtml += "<tr><td colspan='" & colspan & "'>KPI Name : " & tampung & "</td></tr>"
    '                If dt.Rows.Count <> 0 Then
    '                    result.InnerHtml += "<tr><td>Jobsite</td>"
    '                    For Each dc As DataColumn In dt.Columns
    '                        If IsNumeric(dc.ColumnName) Then
    '                            result.InnerHtml += "<td>" & doMapping(dc.ColumnName) & "</td>"
    '                        End If
    '                    Next
    '                    result.InnerHtml += "</tr>"
    '                End If
    '            End If
    '            result.InnerHtml += "<tr>"
    '            result.InnerHtml += "<td>" & dr("KdSite") & "</td>"
    '            For Each dc As DataColumn In dt.Columns
    '                If IsNumeric(dc.ColumnName) Then
    '                    result.InnerHtml += "<td>" & dr(dc.ColumnName) & "</td>"
    '                End If
    '            Next
    '            result.InnerHtml += "</tr>"
    '        Next
    '        If dt.Rows.Count <> 0 Then
    '            result.InnerHtml += "</table>"
    '        End If
    '    Catch ex As Exception
    '        lblMessage.Text = ex.Message
    '    Finally
    '        If conn.State = ConnectionState.Open Then
    '            conn.Close()
    '        End If
    '    End Try

    'End Sub

    Sub GetDataSection2(ByVal id As String, ByVal year As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim cmd As SqlCommand
        Dim sqlQuery As String = ""
        Dim dt As New DataTable()
        Try
            Dim da As SqlDataAdapter
            sqlQuery = "select b.id,b.Name,c.kdsite  ,d.month as bulan,isnull(d.achievement,0) as achievement,isnull(d.actual,0) as actual,RedParam,RedCond,GreenParam,greencond,basedon,stedit from mkpi b " & _
                        "inner join SITE c on b.Jobsite=c.id " & _
                        "left join TBSC d on b.ID=d.kpiid  " & _
                        "where b.id in (select idchild from kpirelation where idparent='" & id & "') and d.year='" & year & "'"
            cmd = New SqlCommand()
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "crosstab2"
            cmd.Parameters.AddWithValue("@SQL", sqlQuery)
            cmd.Parameters.AddWithValue("@tpivotCol", "bulan")
            cmd.Parameters.AddWithValue("@Summaries", "MAX(achievement else null) ACH],Max(Actual else null) ")
            cmd.Parameters.AddWithValue("@GroupBy", "Name,kdsite")
            cmd.Parameters.AddWithValue("@OrderBy ", "Name,kdsite")
            cmd.Parameters.AddWithValue("@OtherFields", "Max(BasedOn) as BasedOn,Max(RedParam) as RedParam,Max(RedCond) as RedCond,Max(GreenParam) as GreenParam,Max(GreenCond) as GreenCond,Max(Stedit) as stedit")
            cmd.Connection = conn
            conn.Open()
            da = New SqlDataAdapter(cmd)
            da.Fill(dt)
            conn.Close()
            Dim tampung As String = ""

            If dt.Rows.Count <> 0 Then
                result.InnerHtml = "<table class='pica'>"
            End If
            Dim colspan As Integer = 0
            For Each dc As DataColumn In dt.Columns
                Console.WriteLine(dc.ColumnName)
                If IsNumeric(dc.ColumnName) Then
                    colspan += 1
                End If
            Next
            colspan += 1
            Dim count As Integer = 1
            For Each dr As DataRow In dt.Rows
                If tampung <> dr("Name") Then
                    tampung = dr("Name")
                    If result.InnerHtml <> "" Then
                        result.InnerHtml += "</table></div><br/>"
                    End If
                    result.InnerHtml += "<a href='#' onclick='javascript:showhidediv(""c" & count & """,""a" & count & """)' id='a" & count & "' style='font-size:14px'>[+]KPI Name : " & tampung & "</a>"
                    result.InnerHtml += "<div id='c" & count & "' style='display:none'>"
                    If dt.Rows.Count <> 0 Then
                        result.InnerHtml += "<table style='font-size:14px' border='1'><tr><td colspan='13' align='center'>ACTUAL DATA</td></tr><tr><td>Jobsite</td>"
                        For Each dc As DataColumn In dt.Columns
                            If dc.ColumnName.IndexOf("ACH") = -1 And IsNumeric(dc.ColumnName) Then
                                result.InnerHtml += "<td>" & doMapping(dc.ColumnName.Split(" ")(0)) & "</td>"
                            End If
                        Next
                        result.InnerHtml += "</tr>"
                    End If
                    count += 1
                End If

                If dr("StEdit") = 0 Then
                    result.InnerHtml += "<tr>"
                    result.InnerHtml += "<td>" & dr("KdSite") & "</td>"
                    Dim myach As String
                    Dim myact As String
                    For Each dc As DataColumn In dt.Columns
                        If dc.ColumnName.IndexOf("ACH") = -1 And IsNumeric(dc.ColumnName) Then
                            If IsDBNull(dr(Trim(dc.ColumnName) & " ACH")) Then
                                myach = 0
                            Else
                                myach = dr(Trim(dc.ColumnName) & " ACH")
                            End If
                            If IsDBNull(dr(dc.ColumnName.Split(" ")(0) & " ")) Then
                                myact = 0
                            Else
                                myact = dr(dc.ColumnName.Split(" ")(0) & " ")
                            End If
                            result.InnerHtml += "<td style='background-color:" & getColor(IIf(dr("BasedOn") = 0, myach, myact), dr("RedCond"), dr("RedParam"), dr("GreenCond"), dr("GreenParam")) & ";'>" & dr(dc.ColumnName) & "</td>"
                        End If
                    Next
                Else
                    result.InnerHtml += "<tr style=""color:gray"">"
                    result.InnerHtml += "<td>" & dr("KdSite") & "</td><td colspan=""12"" style=""text-align:center"">OFF</td>"
                End If
                result.InnerHtml += "</tr>"
            Next
            If dt.Rows.Count <> 0 Then
                result.InnerHtml += "</table></div><br/>"
            End If
        Catch ex As Exception
            lblMessage.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

    End Sub

    Function getColor(ByVal value As String, ByVal redCond As String, ByVal redVal As Decimal, ByVal greenCond As String, ByVal greenVal As Decimal) As String
        Try
            Select Case redCond
                Case "0"
                    If CDec(value) > redVal Then
                        Return "red"
                    End If
                Case "1"
                    If CDec(value) >= redVal Then
                        Return "red"
                    End If
                Case "2"
                    If CDec(value) < redVal Then
                        Return "red"
                    End If
                Case "3"
                    If CDec(value) <= redVal Then
                        Return "red"
                    End If
            End Select
            Select Case greenCond
                Case "0"
                    If CDec(value) > greenVal Then
                        Return "green"
                    End If
                Case "1"
                    If CDec(value) >= greenVal Then
                        Return "green"
                    End If
                Case "2"
                    If CDec(value) < greenVal Then
                        Return "green"
                    End If
                Case "3"
                    If CDec(value) <= greenVal Then
                        Return "green"
                    End If
            End Select
            Return "yellow"
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Function

    Sub GetDataSection3(ByVal id As String, ByVal year As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim cmd As SqlCommand
        Dim sqlQuery As String = ""
        Dim dt As New DataTable()
        Try
            Dim da As SqlDataAdapter
            sqlQuery = "select b.id,b.Name,c.SectionName  ,d.month as bulan,isnull(d.achievement,0) as achievement,isnull(d.actual,0) as actual,RedParam,RedCond,GreenParam,greencond,basedon,stedit from mkpi b " & _
                        "inner join MSECTION c on c.ID = b.Section  " & _
                        "left join TBSC d on b.ID=d.kpiid  " & _
                        "where b.id in (select idchild from kpirelation where idparent='" & id & "') and d.year='" & year & "'"
            cmd = New SqlCommand()
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "crosstab2"
            cmd.Parameters.AddWithValue("@SQL", sqlQuery)
            cmd.Parameters.AddWithValue("@tpivotCol", "bulan")
            cmd.Parameters.AddWithValue("@Summaries", "MAX(achievement else null) ACH],Max(Actual else null)")
            cmd.Parameters.AddWithValue("@GroupBy", "Name,SectionName")
            cmd.Parameters.AddWithValue("@OrderBy ", "Name,SectionName")
            cmd.Parameters.AddWithValue("@OtherFields", "Max(BasedOn) as BasedOn,Max(RedParam) as RedParam,Max(RedCond) as RedCond,Max(GreenParam) as GreenParam,Max(GreenCond) as GreenCond,Max(StEdit) as StEdit")
            cmd.Connection = conn
            conn.Open()
            da = New SqlDataAdapter(cmd)
            da.Fill(dt)
            conn.Close()
            Dim tampung As String = ""

            If dt.Rows.Count <> 0 Then
                result.InnerHtml = "<table class='pica'>"
            End If
            Dim colspan As Integer = 0
            For Each dc As DataColumn In dt.Columns
                If IsNumeric(dc.ColumnName) Then
                    colspan += 1
                End If
            Next
            colspan += 1
            Dim count As Integer = 1
            For Each dr As DataRow In dt.Rows
                If tampung <> dr("Name") Then
                    tampung = dr("Name")
                    If result.InnerHtml <> "" Then
                        result.InnerHtml += "</table></div><br/>"
                    End If
                    result.InnerHtml += "<a href='#' onclick='javascript:showhidediv(""c" & count & """,""a" & count & """)' id='a" & count & "' style='font-size:14px'>[+]KPI Name : " & tampung & "</a>"
                    result.InnerHtml += "<div id='c" & count & "' style='display:none'>"
                    If dt.Rows.Count <> 0 Then
                        result.InnerHtml += "<table style='font-size:14px' border='1'><tr><td colspan='13' align='center'>ACTUAL DATA</td></tr><tr><td>Section</td>"
                        For Each dc As DataColumn In dt.Columns
                            If dc.ColumnName.IndexOf("ACH") = -1 And IsNumeric(dc.ColumnName) Then
                                result.InnerHtml += "<td>" & doMapping(dc.ColumnName) & "</td>"
                            End If
                        Next
                        result.InnerHtml += "</tr>"
                    End If
                    count += 1
                End If
                If dr("StEdit") = 0 Then
                    result.InnerHtml += "<tr>"
                    result.InnerHtml += "<td>" & dr("SectionName") & "</td>"
                    For Each dc As DataColumn In dt.Columns
                        If dc.ColumnName.IndexOf("ACH") = -1 And IsNumeric(dc.ColumnName) Then
                            result.InnerHtml += "<td style='background-color:" & getColor(IIf(dr("BasedOn") = 0, dr(Trim(dc.ColumnName) & " ACH"), dr(dc.ColumnName.Split(" ")(0) & "")), dr("RedCond"), dr("RedParam"), dr("GreenCond"), dr("GreenParam")) & ";'>" & dr(dc.ColumnName) & "</td>"
                        End If
                    Next
                Else
                    result.InnerHtml += "<tr style=""color:gray"">"
                    result.InnerHtml += "<td>" & dr("SectionName") & "</td><td colspan=""12"" style=""text-align:center"">OFF</td>"
                End If
                result.InnerHtml += "</tr>"
            Next
            If dt.Rows.Count <> 0 Then
                result.InnerHtml += "</table></div><br/>"
            End If
        Catch ex As Exception
            lblMessage.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try

    End Sub

    Sub GenerateGraphic(ByVal id As String, ByVal year As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim cmd As SqlCommand

        Try
            'Dim sqlQuery As String = "Select month,target,actual from tbsc where id='" & id & "' and year='" & year & "'"
            'Dim dt As New DataTable()
            'Dim da As New SqlDataAdapter(sqlQuery, conn)
            'conn.Open()
            'da.Fill(dt)
            Dim objBitmap As New Bitmap(200, 200)
            Dim objGraphic As Graphics = Graphics.FromImage(objBitmap)
            Dim whiteBrush As New SolidBrush(Color.White)
            Dim blackPen As New Pen(Color.Black, 2)

            objGraphic.FillRectangle(whiteBrush, 0, 0, 200, 200)
            objGraphic.DrawLine(blackPen, New Point(0, 195), New Point(195, 195))
            objGraphic.DrawLine(blackPen, New Point(5, 5), New Point(5, 200))
            Response.ContentType = "image/gif"
            objBitmap.Save(Response.OutputStream, ImageFormat.Gif)
        Catch ex As Exception
            lblMessage.Text = ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

    Sub GetGraphMTD(ByVal id As String, ByVal year As String, ByVal month As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        Try
            Dim sqlQuery As String = "Select month,target,actual from tbsc where kpiid='" & id & "' and year='" & year & "' order by cast(month as integer)"
            cmd = New SqlCommand(sqlQuery, conn)
            conn.Open()
            dr = cmd.ExecuteReader
            Dim dt As New BSCDATASET.FULLYEARDataTable()
            If dr.HasRows Then
                Dim row As BSCDATASET.FULLYEARRow
                While dr.Read
                    row = dt.NewFULLYEARRow
                    row.BULAN = doMapping(dr("Month"))
                    If Not IsDBNull(dr("target")) Then
                        row.VALUE = dr("target")
                    End If
                    row.TIPE = "Target"
                    dt.AddFULLYEARRow(row)
                    row = dt.NewFULLYEARRow
                    row.BULAN = doMapping(dr("Month"))
                    If dr("month") <= CInt(month) Then
                        If Not IsDBNull(dr("actual")) Then
                            row.VALUE = dr("actual")
                        End If
                    End If
                    row.TIPE = "Actual"
                    dt.AddFULLYEARRow(row)
                End While
            End If
            dr.Close()
            conn.Close()
            ReportViewer1.LocalReport.ReportPath = "Laporan\rptGraphD.rdlc"
            Dim params(0) As ReportParameter
            params(0) = New ReportParameter("Type", "Monthly")
            ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource("BSCDATASET_FULLYEAR", dt))
            ReportViewer1.LocalReport.SetParameters(params)
            ReportViewer1.AsyncRendering = False
            ReportViewer1.SizeToReportContent = True
            ReportViewer1.LocalReport.Refresh()
        Catch ex As Exception

        End Try


    End Sub

    Sub GetGraphYTD(ByVal id As String, ByVal year As String, ByVal month As String)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Dim cmd As SqlCommand
        Dim dr As SqlDataReader
        Try
            Dim tampungtarget As Decimal = 0
            Dim tampungactual As Decimal = 0
            Dim rcount As Decimal = 0
            Dim sqlQuery As String = "Select month,target,actual, isnull(b.calctype,0) as calctype,isnull(annual,0) as annual from tbsc a left join (select kpiid,calctype,annual from TTARGET a where a.versi = (select max(versi) from ttarget where kpiid='" & id & "' and year='" & year & "') and kpiid='" & id & "' and year='" & year & "')b on a.kpiid=b.kpiid where a.kpiid='" & id & "' and a.year='" & year & "' order by cast(month as integer)"
            cmd = New SqlCommand(sqlQuery, conn)
            conn.Open()
            dr = cmd.ExecuteReader
            Dim dt As New BSCDATASET.FULLYEARDataTable()
            If dr.HasRows Then
                Dim row As BSCDATASET.FULLYEARRow
                While dr.Read
                    Select Case dr("calctype")
                        Case 0 'Sum
                            row = dt.NewFULLYEARRow
                            If Not IsDBNull(dr("target")) Then
                                row.BULAN = doMapping(dr("Month"))
                                row.VALUE = dr("annual")
                                row.TIPE = "Target"
                                dt.AddFULLYEARRow(row)
                                If Not IsDBNull(dr("actual")) Then
                                    tampungactual += dr("actual")
                                    row = dt.NewFULLYEARRow
                                    row.BULAN = doMapping(dr("Month"))
                                    'If CInt(dr("Month")) <= month Then
                                    row.VALUE = tampungactual
                                    'End If
                                    row.TIPE = "Actual"
                                    dt.AddFULLYEARRow(row)
                                Else
                                    tampungactual += 0
                                    row = dt.NewFULLYEARRow
                                    row.BULAN = doMapping(dr("Month"))
                                    row.VALUE = tampungactual
                                    row.TIPE = "Actual"
                                    dt.AddFULLYEARRow(row)
                                End If
                            Else 'IF Target NULL
                                row.BULAN = doMapping(dr("Month"))
                                row.TIPE = "Target"
                                dt.AddFULLYEARRow(row)
                                row = dt.NewFULLYEARRow
                                row.BULAN = doMapping(dr("Month"))
                                row.TIPE = "Actual"
                                dt.AddFULLYEARRow(row)
                            End If
                        Case 1 'AVG
                            row = dt.NewFULLYEARRow
                            If Not IsDBNull(dr("target")) Then
                                row.BULAN = doMapping(dr("Month"))
                                row.VALUE = dr("annual")
                                row.TIPE = "Target"
                                dt.AddFULLYEARRow(row)
                                If Not IsDBNull(dr("actual")) Then
                                    rcount += 1
                                    tampungactual += dr("actual")
                                    row = dt.NewFULLYEARRow
                                    row.BULAN = doMapping(dr("Month"))
                                    'If dr("Month") <= month Then
                                    row.VALUE = tampungactual / rcount
                                    'End If
                                    row.TIPE = "Actual"
                                    dt.AddFULLYEARRow(row)
                                Else
                                    row = dt.NewFULLYEARRow
                                    row.BULAN = doMapping(dr("Month"))
                                    row.TIPE = "Actual"
                                    dt.AddFULLYEARRow(row)
                                End If
                            Else
                                'IF Target NULL
                                row.BULAN = doMapping(dr("Month"))
                                row.TIPE = "Target"
                                dt.AddFULLYEARRow(row)
                                row = dt.NewFULLYEARRow
                                row.BULAN = doMapping(dr("Month"))
                                row.TIPE = "Actual"
                                dt.AddFULLYEARRow(row)
                            End If
                        Case 2 'Last Value
                            row = dt.NewFULLYEARRow
                            row.BULAN = doMapping(dr("Month"))
                            If Not IsDBNull(dr("target")) Then
                                row.VALUE = dr("annual")
                                row.TIPE = "Target"
                                dt.AddFULLYEARRow(row)
                                row = dt.NewFULLYEARRow
                                row.BULAN = doMapping(dr("Month"))
                                'If dr("month") <= CInt(month) Then
                                If Not IsDBNull(dr("actual")) Then
                                    row.VALUE = dr("actual")
                                End If
                                'End If
                                row.TIPE = "Actual"
                                dt.AddFULLYEARRow(row)
                            Else
                                row.TIPE = "Target"
                                dt.AddFULLYEARRow(row)
                                row = dt.NewFULLYEARRow
                                row.BULAN = doMapping(dr("Month"))
                                'If dr("month") <= CInt(month) Then
                                If Not IsDBNull(dr("actual")) Then
                                    row.VALUE = dr("actual")
                                End If
                                'End If
                                row.TIPE = "Actual"
                                dt.AddFULLYEARRow(row)
                            End If
                        Case 3 'Manual Target
                            row = dt.NewFULLYEARRow
                            row.BULAN = doMapping(dr("Month"))
                            If Not IsDBNull(dr("target")) Then
                                row.VALUE = dr("annual")
                                row.TIPE = "Target"
                                dt.AddFULLYEARRow(row)
                                row = dt.NewFULLYEARRow
                                row.BULAN = doMapping(dr("Month"))
                                'If dr("month") <= CInt(month) Then
                                If Not IsDBNull(dr("actual")) Then
                                    row.VALUE = dr("actual")
                                End If
                                'End If
                                row.TIPE = "Actual"
                                dt.AddFULLYEARRow(row)
                            Else
                                row.TIPE = "Target"
                                dt.AddFULLYEARRow(row)
                                row = dt.NewFULLYEARRow
                                row.BULAN = doMapping(dr("Month"))
                                'If dr("month") <= CInt(month) Then
                                If Not IsDBNull(dr("actual")) Then
                                    row.VALUE = dr("actual")
                                End If
                                'End If
                                row.TIPE = "Actual"
                                dt.AddFULLYEARRow(row)
                            End If
                    End Select
                End While
            End If
            dr.Close()
            conn.Close()
            ReportViewer2.LocalReport.ReportPath = "Laporan\rptGraphD.rdlc"
            ReportViewer2.LocalReport.DataSources.Clear()
            Dim params(0) As ReportParameter
            params(0) = New ReportParameter("Type", "Yearly")
            ReportViewer2.LocalReport.DataSources.Add(New ReportDataSource("BSCDATASET_FULLYEAR", dt))
            ReportViewer2.LocalReport.SetParameters(params)
            ReportViewer2.AsyncRendering = False
            ReportViewer2.SizeToReportContent = True
            ReportViewer2.LocalReport.Refresh()
        Catch ex As Exception

        End Try


    End Sub

    Function doMapping(ByVal val As String) As String
        Select Case val
            Case "1"
                Return "JAN"
            Case "2"
                Return "FEB"
            Case "3"
                Return "MAR"
            Case "4"
                Return "APR"
            Case "5"
                Return "MEI"
            Case "6"
                Return "JUN"
            Case "7"
                Return "JUL"
            Case "8"
                Return "AGU"
            Case "9"
                Return "SEP"
            Case "10"
                Return "OKT"
            Case "11"
                Return "NOV"
            Case "12"
                Return "DES"
        End Select
        Return ""
    End Function
End Class