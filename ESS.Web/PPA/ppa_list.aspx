﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ppa_list.aspx.vb" Inherits="EXCELLENT.ppa_list" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Performance Appraisal</title>
<style type="text/css">


body{
  padding: 1em;
  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAAQElEQVQIW2P89OvDfwYo+PHjJ4zJwMHBzsAIk0SXAKkCS2KTAEu++vQSbizIKGQAl0SXAJkGlsQmAbcT2Shk+wH0sCzAEOZW1AAAAABJRU5ErkJggg==);
}
a{
  color: #739931;
}
.page{
  max-width: 70em;
  margin: 0 auto;
}
table th,
table td{
  text-align: left;
}
table.layout{
  width: 100%;
  border-collapse: collapse;
}
table.display{
  margin: 1em 0;
}
table.display th,
table.display td{
  border: 1px solid #B3BFAA;
  padding: .5em 1em;
}

table.display th{ background: #D5E0CC; }
table.display td{ background: #fff; }

table.responsive-table{
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
}

.center{
  text-align:center;
  }
.none{
	border-style:none;
    text-align:center;
  }

@media (max-width: 30em){
    table.responsive-table{
      box-shadow: none;  
    }
    table.responsive-table thead{
      display: none; 
    }
  table.display th,
  table.display td{
    padding: .5em;
  }
    
  table.responsive-table td:nth-child(1):before{
    content: 'Number';
  }
  table.responsive-table td:nth-child(2):before{
    content: 'Name';
  }
  table.responsive-table td:nth-child(1),
  table.responsive-table td:nth-child(2){
    padding-left: 25%;
  }
  table.responsive-table td:nth-child(1):before,
  table.responsive-table td:nth-child(2):before{
    position: absolute;
    left: .5em;
    font-weight: bold;
  }
  
    table.responsive-table tr,
    table.responsive-table td{
        display: block;
    }
    table.responsive-table tr{
        position: relative;
        margin-bottom: 1em;
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
    }
    table.responsive-table td{
        border-top: none;
    }
    table.responsive-table td.organisationnumber{
        background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    table.responsive-table td.actions{
        position: absolute;
        top: 0;
        right: 0;
        border: none;
        background: none;
    }
    table.responsive-table td.top{
    	background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
}

</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    
                </td>
            </tr>
            <%=sdata%>
        </table>
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="nobuk" AllowPaging="true" PageSize="100">
            <Columns>
                 <asp:BoundField DataField="nobuk" HeaderText="nobuk" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                    <HeaderStyle Width="100px"></HeaderStyle>
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                 </asp:BoundField>
                 <asp:BoundField DataField="ms_id" HeaderText="Question Number" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                    <HeaderStyle Width="100px"></HeaderStyle>
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                 </asp:BoundField>
                 <asp:BoundField DataField="score_isi" HeaderText="Score" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                    <HeaderStyle Width="100px"></HeaderStyle>
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                 </asp:BoundField>
            </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
