﻿Imports System.Globalization
Imports System.Data.SqlClient
Imports System.Data

Partial Public Class notactive
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("localdev").ConnectionString)
        Dim sqlqry As String

        Try
            sqlConn.Open()
            sqlqry = "select dtclose from ms_system"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(sqlqry, sqlConn)
            Dim dtb As DataTable = New DataTable

            sda.Fill(dtb)

            If dtb.Rows.Count > 0 Then
                If CDate(dtb.Rows(0)!dtclose.ToString) >= Today.Date Then
                    Response.Redirect("login_ppa.aspx")
                End If
            End If
        Catch ex As Exception

        Finally
            sqlConn.Close()
        End Try
    End Sub

    Public Sub timer1_tick(ByVal sender As Object, ByVal e As EventArgs)
        Label1.Text = DateTime.Now.ToString("ddd d MMM yyyy H:mm:ss", CultureInfo.CreateSpecificCulture("en-US"))
    End Sub

End Class