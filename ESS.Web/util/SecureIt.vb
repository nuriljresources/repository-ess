﻿Imports System
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports System.Net.Mail

Namespace SecureIt

    ''' <summary>
    ''' Class untuk fitur2 keamanan data
    ''' </summary>
    ''' <remarks></remarks>
    Public Class Secure
        Private Const saltkey As String = "@DMINBUMA"
        ''' <summary>
        ''' Fungsi untuk pengacakan data. Metode yang digunakan adalah Rijndael/AES 
        ''' </summary>
        ''' <param name="plaintext">Teks asli yang akan di enkripsi</param>
        ''' <param name="password">Kunci pengacak tambahan</param>
        ''' <returns>Cipher text</returns>
        ''' <remarks></remarks>
        Public Shared Function Encrypt(ByVal plaintext As String, Optional ByVal password As String = saltkey) As String
            Dim rijndaelCipher As New RijndaelManaged()
            rijndaelCipher.Padding = PaddingMode.PKCS7

            Dim plaintextByte As Byte() = System.Text.Encoding.Unicode.GetBytes(plaintext)
            Dim saltByte As Byte() = Encoding.ASCII.GetBytes(password)
            Dim secretKey As New Rfc2898DeriveBytes(password, saltByte)
            Dim encryptor As ICryptoTransform = rijndaelCipher.CreateEncryptor(secretKey.GetBytes(32), secretKey.GetBytes(16))
            Dim memoryStream As New MemoryStream()
            Dim cryptoStream As New CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)

            cryptoStream.Write(plaintextByte, 0, plaintextByte.Length)
            cryptoStream.FlushFinalBlock()

            Dim cipherBytes As Byte() = memoryStream.ToArray()

            memoryStream.Close()
            cryptoStream.Close()
            encryptor.Dispose()

            Return Convert.ToBase64String(cipherBytes)
        End Function

        ''' <summary>
        ''' Prosedur untuk pengacakan data ke file. Metode yang digunakan adalah Rijndael/AES 
        ''' </summary>
        ''' <param name="plaintext">Teks asli yang akan di enkripsi</param>
        ''' <param name="password">Kunci pengacak tambahan</param>
        ''' <param name="filepath">Lokasi penyimpanan file</param>
        ''' <remarks></remarks>
        Public Shared Sub Encrypt(ByVal plaintext As String, ByVal password As String, ByVal filePath As String)
            Using fsout As New StreamWriter(filePath)
                fsout.Write(Encrypt(plaintext, password))
            End Using
        End Sub

        ''' <summary>
        ''' Fungsi untuk penyusun data yang teracak. Metode yang digunakan adalah Rijndael/AES
        ''' </summary>
        ''' <param name="cipher">Teks yang teracak atau nama file lengkap dengan path nya</param>
        ''' <param name="password">Kunci pengacak tambahan</param>
        ''' <returns>Teks asli</returns>
        ''' <remarks></remarks>
        Public Shared Function Decrypt(ByVal cipher As String, Optional ByVal password As String = saltkey) As String
            Dim ciphertext As String
            ciphertext = cipher

            Try
                Dim rijndaelCipher As New RijndaelManaged()
                rijndaelCipher.Padding = PaddingMode.PKCS7

                Dim ciphertextByte As Byte() = Convert.FromBase64String(ciphertext)
                Dim saltByte As Byte() = Encoding.ASCII.GetBytes(password)

                Dim secretKey As New Rfc2898DeriveBytes(password, saltByte)
                Dim decryptor As ICryptoTransform = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16))
                Dim memoryStream As New MemoryStream(ciphertextByte)
                Dim cryptoStream As New CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)

                Dim plainText As Byte() = New Byte(ciphertextByte.Length - 1) {}

                Dim decryptedCount As Integer = cryptoStream.Read(plainText, 0, plainText.Length)

                memoryStream.Close()
                cryptoStream.Close()

                Return Encoding.Unicode.GetString(plainText, 0, decryptedCount)
            Catch e As Exception
                Throw New InvalidDataException("[" + e.ToString() + " : " + e.Message + "]  Data corrupt")
            End Try
        End Function
        ''' <summary>
        ''' Fungsi untuk penyusun data yang teracak. Metode yang digunakan adalah Rijndael/AES
        ''' </summary>
        ''' <param name="cipher">Teks yang teracak atau nama file lengkap dengan path nya</param>
        ''' <param name="password">Kunci pengacak tambahan</param>
        ''' <param name="isfromfile">file (true) atau teks (false) ?</param>
        ''' <returns>Teks asli</returns>
        ''' <remarks></remarks>
        Public Shared Function Decrypt(ByVal cipher As String, ByVal isfromfile As Boolean, Optional ByVal password As String = saltkey) As String
            Dim ciphertext As String

            If isfromfile Then
                Using fsin As New StreamReader(cipher)
                    ciphertext = fsin.ReadToEnd()
                End Using
            Else
                ciphertext = cipher
            End If


            Try
                Dim rijndaelCipher As New RijndaelManaged()
                rijndaelCipher.Padding = PaddingMode.PKCS7

                Dim ciphertextByte As Byte() = Convert.FromBase64String(ciphertext)
                Dim saltByte As Byte() = Encoding.ASCII.GetBytes(password)

                Dim secretKey As New Rfc2898DeriveBytes(password, saltByte)
                Dim decryptor As ICryptoTransform = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16))
                Dim memoryStream As New MemoryStream(ciphertextByte)
                Dim cryptoStream As New CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)

                Dim plainText As Byte() = New Byte(ciphertextByte.Length - 1) {}

                Dim decryptedCount As Integer = cryptoStream.Read(plainText, 0, plainText.Length)

                memoryStream.Close()
                cryptoStream.Close()

                Return Encoding.Unicode.GetString(plainText, 0, decryptedCount)
            Catch e As Exception
                Throw New InvalidDataException("[" + e.ToString() + " : " + e.Message + "]  Data corrupt")
            End Try
        End Function

        Public Shared Sub SendErrorToDeveloper(ByVal ErrQuery As String, ByVal ErrMsg As String)
            Dim message As New MailMessage()
            message = New MailMessage()
            message.From = New MailAddress("MEMO@jresources.com", "Aplikasi MEMO")
            message.Subject = "NOTIFIKASI ERROR MEMO"
            message.IsBodyHtml = True

            message.Body = message.Body + "<b>Query Error & Exception Error</b>"
            message.Body = message.Body + "<br><br>"
            message.Body = message.Body + ErrQuery
            message.Body = message.Body + "<br><br>"
            message.Body = message.Body + ErrMsg
            message.To.Add("robinson@jresources.com")

            'Dim smtp As New SmtpClient("172.16.0.15")
            Dim smtp As New SmtpClient("smtp.gmail.com")
            smtp.Send(message)
        End Sub

    End Class
End Namespace
