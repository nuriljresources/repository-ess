﻿Imports System.Data.SqlClient
Partial Public Class leave_edit
    Inherits System.Web.UI.Page
    Public dtnow As String
    Public alert As String = ""
    Public remain, yeara, yearb, atot, btot As String
    Public btfrom, btto As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("niksite") = Nothing Or Session("niksite") = "" Then Response.Redirect("login.aspx")

        'Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("test_con").ConnectionString)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)

        If Not IsPostBack Then
            Try
                If Request.QueryString("save") = 1 Then alert = "<script type ='text/javascript' > alert('Data has been saved') </script>"

                Dim sqllh001 As String = "select transid, nik, (select niksite from H_A101 where nik = L_H001.nik) as niksite, (select nama from H_A101 where nik = L_H001.nik) as nama, " & _
                "trans_date, kddepar, (select nmdepar from h_a130 where kddepar = L_H001.kddepar) as nmdepar, " & _
                "kdjabatan, " & _
                "(select nmjabat from H_A209 ,H_A150 where H_A209.kdsec = H_A150.kdsec and H_A150.kdjabat = L_H001.kdjabatan ) as nmjabat, " & _
                "datefrom, dateto, totleave, remainleave, eligiLeave, cutabsent, " & _
                "(select typeDesc from L_A001 where typeLeave = L_H001.typeLeave) as decsleave, " & _
                "typeLeave, " & _
                "(select paidltypeDesc from L_A002 where paidltype = L_H001.paidltype) as descpaidl, " & _
                "paidltype, remarks, pic_nik, (select niksite from H_A101 where nik = L_H001.pic_nik) as pic_niksite, (select Nama from H_A101 where NIK = L_H001.pic_nik) as nmpic, " & _
                "pic_jabat, (select nmjabat from H_A209 ,H_A150 where H_A209.kdsec = H_A150.kdsec and H_A150.kdjabat = L_H001.pic_jabat) as picjabat, " & _
                "verify_nik, (select niksite from H_A101 where nik = L_H001.verify_nik) as verify_niksite, (select Nama from H_A101 where NIK = L_H001.verify_nik) as nmverify, " & _
                "verify_jabat, (select nmjabat from H_A209 ,H_A150 where H_A209.kdsec = H_A150.kdsec and H_A150.kdjabat = L_H001.verify_jabat) as verifyjabat, " & _
                "approve_nik, (select niksite from H_A101 where nik = L_H001.approve_nik) as approve_niksite, (select Nama from H_A101 where NIK = L_H001.approve_nik) as nmappby, " & _
                "approve_jabat, (select nmjabat from H_A209 ,H_A150 where H_A209.kdsec = H_A150.kdsec and H_A150.kdjabat = L_H001.approve_jabat) as verifyappby, " & _
                "fstatus, CreatedBy, CreatedIn, CreatedTime, ModifiedBy, ModifiedIn, ModifiedTime, StEdit, DeleteBy, " & _
                "DeleteTime, kdsite, yeara, yearb, aTot, bTot, holiday, ISNULL(ref_fieldbreak, '') as ref_fieldbreak, ISNULL(StatusWF, '') AS StatusWF from L_H001 " & _
                "where transid = '" + Session("vleave") + "'"

                Dim dtblh001 As DataTable = New DataTable
                Dim sdalh001 As SqlDataAdapter = New SqlDataAdapter(sqllh001, conn)

                sdalh001.Fill(dtblh001)

                If dtblh001.Rows.Count > 0 Then
                    txtnobukti.Text = dtblh001.Rows(0)!transid.ToString
                    txtkdsite.Text = dtblh001.Rows(0)!kdsite.ToString
                    txtnik.Text = dtblh001.Rows(0)!niksite.ToString
                    txtniksite.Value = dtblh001.Rows(0)!nik.ToString
                    txtnama.Text = dtblh001.Rows(0)!nama.ToString
                    txtdepar.Text = dtblh001.Rows(0)!nmdepar.ToString
                    txtposition.Text = dtblh001.Rows(0)!nmjabat.ToString
                    txtleavetype.Text = dtblh001.Rows(0)!decsleave.ToString
                    txtpaidleave.Text = dtblh001.Rows(0)!descpaidl.ToString
                    If dtblh001.Rows(0)!CreatedTime.ToString <> "" Then
                        txtdtreq.Text = Format(CDate(dtblh001.Rows(0)!CreatedTime.ToString), "MM/dd/yyyy")
                    Else
                        txtdtreq.Text = ""
                    End If

                    If dtblh001.Rows(0)!datefrom.ToString <> "" Then
                        txtleafrom.Text = Format(CDate(dtblh001.Rows(0)!datefrom.ToString), "MM/dd/yyyy")
                    Else
                        txtleafrom.Text = ""
                    End If

                    If dtblh001.Rows(0)!dateto.ToString <> "" Then
                        txtleato.Text = Format(CDate(dtblh001.Rows(0)!dateto.ToString), "MM/dd/yyyy")
                    Else
                        txtleato.Text = ""
                    End If

                    txtholiday.Text = CDec(dtblh001.Rows(0)!holiday).ToString("#.##")
                    txarem.Text = dtblh001.Rows(0)!remarks.ToString
                    txttotleave.Text = CDec(dtblh001.Rows(0)!totleave).ToString("#.##")
                    txtremleave.Text = CDec(dtblh001.Rows(0)!remainleave).ToString("#.##")

                    txtremleaveact.Value = CDec(dtblh001.Rows(0)!remainleave).ToString("#.##")
                    txtleaeli.Text = dtblh001.Rows(0)!eligiLeave.ToString
                    txtcutabs.Text = dtblh001.Rows(0)!cutabsent.ToString
                    leavetype.Value = dtblh001.Rows(0)!typeLeave.ToString
                    paidleave.Value = dtblh001.Rows(0)!paidltype.ToString
                    posdut.Value = dtblh001.Rows(0)!pic_jabat.ToString
                    txtnikx.Value = dtblh001.Rows(0)!nik.ToString
                    posverby.Value = dtblh001.Rows(0)!verify_jabat.ToString
                    posappby.Value = dtblh001.Rows(0)!approve_jabat.ToString
                    txtniksitedut.Value = dtblh001.Rows(0)!pic_nik.ToString
                    txtnikdut.Text = dtblh001.Rows(0)!pic_niksite.ToString
                    txtnmdut.Text = dtblh001.Rows(0)!nmpic.ToString
                    txtposdut.Text = dtblh001.Rows(0)!picjabat.ToString
                    txtverby.Text = dtblh001.Rows(0)!verify_niksite.ToString
                    txtnikverby.Value = dtblh001.Rows(0)!verify_nik.ToString
                    txtposverby.Text = dtblh001.Rows(0)!verifyjabat.ToString
                    txtnikappby.Text = dtblh001.Rows(0)!approve_niksite.ToString
                    txtappby.Value = dtblh001.Rows(0)!approve_nik.ToString
                    txtposappby.Text = dtblh001.Rows(0)!verifyappby.ToString
                    txtdayoff.Text = dtblh001.Rows(0)!ref_fieldbreak.ToString

                    txtnobukti.Attributes.Add("readonly", "readonly")
                    txtnik.Attributes.Add("readonly", "readonly")
                    txtnama.Attributes.Add("readonly", "readonly")
                    txtdepar.Attributes.Add("readonly", "readonly")
                    txtposition.Attributes.Add("readonly", "readonly")
                    txtkdsite.Attributes.Add("readonly", "readonly")
                    txtleavetype.Attributes.Add("readonly", "readonly")
                    txtpaidleave.Attributes.Add("readonly", "readonly")
                    txtleafrom.Attributes.Add("readonly", "readonly")
                    txtleato.Attributes.Add("readonly", "readonly")
                    'txtleato.Attributes.Add("readonly", "readonly")
                    txttotleave.Attributes.Add("readonly", "readonly")
                    txtcutabs.Attributes.Add("readonly", "readonly")
                    txtremleave.Attributes.Add("readonly", "readonly")
                    txtleaeli.Attributes.Add("readonly", "readonly")
                    txtnikdut.Attributes.Add("readonly", "readonly")
                    txtnmdut.Attributes.Add("readonly", "readonly")
                    txtposdut.Attributes.Add("readonly", "readonly")
                    txtverby.Attributes.Add("readonly", "readonly")
                    txtposverby.Attributes.Add("readonly", "readonly")
                    txtnikappby.Attributes.Add("readonly", "readonly")
                    txtposappby.Attributes.Add("readonly", "readonly")
                    txtdtreq.Attributes.Add("readonly", "readonly")
                    txarem.Enabled = False
                    dayoff_dateex.Attributes.Add("readonly", "readonly")
                    dayoff_usage.Attributes.Add("readonly", "readonly")

                    If txtholiday.Text = "" Then txtholiday.Text = "0"
                    If txtremleave.Text = "" Then txtremleave.Text = "0"
                    If txttotleave.Text = "" Then txttotleave.Text = "0"

                    If dtblh001.Rows(0)!StatusWF = "INP" Or dtblh001.Rows(0)!StatusWF = "COMPLT" Then
                        btnedit.Enabled = False
                    End If


                    If Session("kdsite") = "JKT" Then
                        If dtblh001.Rows(0)!StatusWF = "" Or dtblh001.Rows(0)!StatusWF = "RVS" Then
                            btnSubmit.Enabled = True
                        End If
                    End If



                End If

            Catch ex As Exception

            End Try
            btnsave.Enabled = False
            'txarem.Enabled = False
            txtholiday.Enabled = False
            btncalc.Enabled = False
            btndelete.Enabled = False


            txtcutabs.Visible = False
            txtcutabs.Text = "0"
            txtleaeli.Visible = False
            txtleaeli.Text = "0"
            btfrom = "<button id='btfrom' style='height:22px' disabled='disabled'>...</button>"
            btto = "<button id='btto' style='height:22px' disabled='disabled'>...</button>"
        End If
        btfrom = "<button id='btfrom' style='height:22px' >...</button>"
        btto = "<button id='btto' style='height:22px' >...</button>"

        If Not dayoff_tripid.Value = "" Then
            txtdayoff.Text = dayoff_tripid.Value
            txtdayoff.Enabled = True
        End If

    End Sub

    Public Sub btnprint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnprint.Click
        Response.Redirect("cetak_leave.aspx?id=" + Session("vleave"), False)
    End Sub

    Private Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        'Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("test_con").ConnectionString)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
        Dim sqlQuery As String
        Dim cmd As SqlCommand
        Dim dttm As String
        Dim createdin As String
        Dim bcheck As Boolean = True

        createdin = System.Net.Dns.GetHostName()
        dttm = Date.Now.ToString

        If leavetype.Value = "07" Then
            If (dayoff_tripid.Value = "" Or dayoff_tripid.Value = Nothing) Then
                alert = "<script type ='text/javascript' > alert('Please fill businness trip leave') </script>"
                txtdayoff.BackColor = System.Drawing.ColorTranslator.FromHtml("#FADEE8")
                Return
            Else
                Dim dtExpired As DateTime = dayoff_dateex.Text
                If String.IsNullOrEmpty(txtleafrom.Text) Or String.IsNullOrEmpty(txtleato.Text) Then
                    alert = "<script type ='text/javascript' > alert('Leave From & Leave muse be selected ') </script>"
                    Return
                End If

                Dim dtstart As DateTime = txtleafrom.Text
                Dim dtend As DateTime = txtleato.Text

                If (dtstart > dtExpired) Or (dtend > dtExpired) Then
                    alert = "<script type ='text/javascript' > alert('Leave From & Leave To not in range date business trip ') </script>"
                    Return
                End If

                If Integer.Parse(txttotleave.Text) > Integer.Parse(dayoff_usage.Text.Replace(".00", "")) Then
                    alert = "<script type ='text/javascript' > alert('Total leave cannot more from Total Balance ') </script>"
                    Return
                End If

            End If
        End If

        calc()

        Try
            conn.Open()
            sqlQuery = "update L_H001 set totleave = '" + txttotleave.Text + "', datefrom = '" + txtleafrom.Text + "', dateto = '" + txtleato.Text + "', remainleave = '" + txtremleaveact.Value + "', eligileave = '" + txtleaeli.Text + "', cutabsent = '" + txtcutabs.Text + "', typeleave = '" + leavetype.Value + "', paidltype = '" + paidleave.Value + "', remarks = '" + txarem.Text + "', pic_nik = '" + txtniksitedut.Value + "', pic_jabat = '" + posdut.Value + "', verify_nik = '" + txtnikverby.Value + "', verify_jabat = '" + posverby.Value + "', approve_nik = '" + txtappby.Value + "', approve_jabat = '" + posappby.Value + "', modifiedby = '" + Session("otorisasi") + "', modifiedin = '" + createdin + "', modifiedtime = '" + dttm + "', yeara = '" + hyeara.Value + "', yearb = '" + hyearb.Value + "', atot = '" + htota.Value + "', btot = '" + htotb.Value + "', holiday = '" + txtholiday.Text + "', ref_fieldbreak = '" + dayoff_tripid.Value + "' where transid = '" + txtnobukti.Text + "'"
            cmd = New SqlCommand(sqlQuery, conn)
            cmd.ExecuteScalar()

        Catch ex As Exception
            bcheck = False
        Finally

            If bcheck = True Then
                alert = "<script type ='text/javascript' > alert('Data has been updated') </script>"
                btnsave.Enabled = False
                btndelete.Enabled = False
                btncalc.Enabled = False
            Else
                alert = "<script type ='text/javascript' > alert('data updated failed') </script>"
            End If
            conn.Close()
        End Try

    End Sub

    Private Sub btncalc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncalc.Click
        calc()
    End Sub

    'Public Sub calc()
    '    Dim yr As String = Year(Date.Now).ToString
    '    Dim liyear, liyearb, litot, litotb, liRemainRef, liRemain, liRemainb, liUsed, liUsedb, litotDeduct, liSisa As Decimal
    '    Dim ldatePop As Date
    '    Dim lastUsed, LastBurn As Double

    '    If txtnikx.Value = Nothing Or txtnikx.Value = "" Then
    '        alert = "<script type ='text/javascript' > alert('Nik Can not empty') </script>"
    '    Else

    '        If txtholiday.Text = Nothing Then txtholiday.Text = "0"

    '        'Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("test_con").ConnectionString)
    '        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)

    '        Try
    '            sqlConn.Open()
    '            Dim dtb As DataTable = New DataTable()
    '            Dim dtblinfo As DataTable = New DataTable()

    '            Dim strlinfo As String = " SELECT  H_A101.Nik , H_A101.tglmasuk , H_A101.kdsite, H_A101.kdlokasi, " & _
    '            "(SELECT DATEPOP FROM h_h217 WHERE NIK = '" + txtnikx.Value + "' AND YEAR = " + yr + ") AS 'DateLeavePop', " & _
    '            "isnull((SELECT TOTlEAVE FROM h_h217 WHERE NIK = '" + txtnikx.Value + "' AND YEAR = " + yr + "),0) AS 'Totleave', " & _
    '            "(SELECT DATEEXPIRED FROM h_h217 WHERE NIK = '" + txtnikx.Value + "' AND YEAR = " + yr + ") AS 'ExpiredLeave', " & _
    '            "isnull((select SUM(atot) from L_H001 where stedit <> '2' and fstatus=1 " & _
    '            "and typeleave='00' and nik = '" + txtnikx.Value + "' and yeara =   " + yr + " ),0) + " & _
    '            "isnull((select SUM(btot) from L_H001 where stedit <> '2' and fstatus=1 " & _
    '            "and typeleave='00' and nik =  '" + txtnikx.Value + "' and yearb =  " + yr + " ),0) as 'usedLeave', " & _
    '            "isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='00' " & _
    '            "and nik = '" + txtnikx.Value + "' and YEAR(trans_date) = " + yr + "),0) as 'burnLeave', " & _
    '            "isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='01' " & _
    '            "and nik = '" + txtnikx.Value + "' and YEAR(trans_date) = " + yr + "),0) as 'AddLeave', " & _
    '            "isnull((select sum(TOTlEAVE) from H_H217 where nik = '" + txtnikx.Value + "' " & _
    '            "and year < " + yr + "  and dateExpired  >= getdate()  ),0) as 'LastTot', " & _
    '            "isnull((select SUM(atot) from L_H001 where stedit <> '2' and fstatus=1 " & _
    '            "and typeleave='00' and nik = '" + txtnikx.Value + "' and yeara =   " + yr + " -1 ),0)  + " & _
    '            "isnull((select SUM(btot) from L_H001 where stedit <> '2' and fstatus=1 " & _
    '            "and typeleave='00' and nik =  '" + txtnikx.Value + "' and yearb =  " + yr + " - 1 ),0) as 'LastusedLeave', " & _
    '            "isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='00' " & _
    '            "and nik = '" + txtnikx.Value + "' and YEAR(trans_date) = " + yr + " -1 ),0) as 'LastburnLeave' , " & _
    '            "isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='01' " & _
    '            "and nik = '" + txtnikx.Value + "' and YEAR(trans_date) = " + yr + " -1 ),0) as 'LastAddLeave' " & _
    '            "FROM H_A101 WHERE H_A101.Nik = '" + txtnikx.Value + "'"
    '            Dim sdalinfo As SqlDataAdapter = New SqlDataAdapter(strlinfo, sqlConn)
    '            sdalinfo.Fill(dtblinfo)


    '            Dim strcon As String = "select nik, year, datepop, totleave, remainleave, fhangusbig, fhangussick, fextend, fadd, dateexpired, dateextend, extendref, hangusBref, hangusSref, addref from h_h217 where nik = '" + txtnikx.Value + "'"
    '            'Dim strcon As String = "select nik, year, datepop, totleave, remainleave, fhangusbig, fhangussick, fextend, fadd, dateexpired, dateextend, extendref, hangusBref, hangusSref, addref from h_h217 where nik = '0000002'"
    '            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
    '            sda.Fill(dtb)

    '            If txtleafrom.Text <> Nothing Then
    '                Dim dtstart As DateTime = txtleafrom.Text
    '                Dim dtend As DateTime = txtleato.Text

    '                Dim ts As String = (dtend - dtstart).Days
    '                ts = ts - txtholiday.Text

    '                'txttotleave.Text = ts - txtholiday.Text

    '                litotDeduct = ts + dtblinfo.Rows(0)!LastTot 'total potong cuti

    '                ldatePop = dtblinfo.Rows(0)!dateleavepop
    '                liyear = Year(ldatePop)
    '                liyearb = liyear - 1

    '                'cek date cuti bilamana dibawah date pop maka jatah cuti tahun ini = 0
    '                If txtleafrom.Text < ldatePop Then
    '                    liRemain = 0
    '                Else
    '                    'remain tahun ini
    '                    liRemain = dtblinfo.Rows(0)!totLeave - (dtblinfo.Rows(0)!usedleave + dtblinfo.Rows(0)!burnLeave)
    '                End If

    '                liRemainb = dtblinfo.Rows(0)!LastTot

    '                If liRemainb > 0 Then
    '                    lastUsed = dtblinfo.Rows(0)!LastusedLeave
    '                    LastBurn = dtblinfo.Rows(0)!LastburnLeave
    '                    liRemainb = liRemainb - (lastUsed + LastBurn)
    '                Else
    '                    liRemainb = 0 'sdh abis expired
    '                    'ambil remain minus , akalu ada positiop nolkan krn da expired
    '                    lastUsed = dtblinfo.Rows(0)!LastusedLeave
    '                    LastBurn = dtblinfo.Rows(0)!LastburnLeave
    '                    liRemainb = liRemainb - (lastUsed + LastBurn)

    '                    If liRemainb < 0 Then
    '                    Else
    '                        liRemainb = 0
    '                    End If

    '                End If

    '                If liRemainb > 0 Then
    '                    If litotDeduct > liRemainb Then
    '                        litotb = liRemainb
    '                        liSisa = litotDeduct - litotb
    '                        If liSisa > 0 Then
    '                            'Cek sisa cuti thn ini
    '                            If liSisa > liRemain Then
    '                                'tambahan bole cuti minus
    '                                litot = liSisa
    '                                liSisa = 0
    '                            Else
    '                                litot = liSisa
    '                                liSisa = 0
    '                            End If
    '                        End If
    '                    Else
    '                        litotb = litotDeduct
    '                        liSisa = 0
    '                    End If
    '                Else 'cek klo sisa cuti ga ada cek cuti thn ini
    '                    If litotDeduct > liRemain Then
    '                        'tambahan bole cuti minus
    '                        litot = litotDeduct
    '                        liSisa = 0
    '                    Else
    '                        litot = litotDeduct
    '                        liSisa = 0
    '                    End If
    '                End If

    '                liRemainRef = liRemain + liRemainb
    '                remain = liRemainRef
    '                yeara = liyear
    '                yearb = liyearb
    '                atot = litot
    '                btot = litotb

    '                txtremleave.Text = remain
    '                txttotleave.Text = ts
    '                txtcutabs.Text = 0
    '                txtleaeli.Text = remain

    '                hyeara.Value = yeara
    '                hyearb.Value = yearb
    '                htota.Value = atot
    '                htotb.Value = btot
    '            End If

    '            If dtblinfo.Rows(0)!DateLeavePop.ToString = "" Then
    '                alert = "<script type ='text/javascript' > alert('Nik Leave Pop Date not found, Leave Eligible hasn't Generated') </script>"
    '            End If

    '            If dtb.Rows.Count = Nothing Then
    '                alert = "<script type ='text/javascript' > alert(''Nik Leave Pop Date not found, Leave Eligible hasn't Generated.) </script>"
    '            ElseIf dtb.Rows.Count > 0 Then
    '                Dim dtleafrom As DateTime = txtleafrom.Text
    '                Dim dtpop As DateTime = dtb.Rows(0)!datepop
    '                'If dtleafrom < dtpop Then
    '                '    'txtremleave.Text = "0"
    '                '    txtleaeli.Text = "0"
    '                '    txtremleave.Text = Format(dtb.Rows(0)!totleave, "0.00")
    '                '    txtcutabs.Text = "0"
    '                'Else
    '                '    txtremleave.Text = Format(dtb.Rows(0)!totleave, "0.00")
    '                '    txtleaeli.Text = dtb.Rows(0)!remainleave
    '                '    txtcutabs.Text = "0"
    '                'End If
    '            End If
    '        Catch ex As Exception

    '        End Try
    '        'alert('test');
    '    End If
    'End Sub

    Public Sub calc()
        'Dim dtstart As DateTime = DateTime.ParseExact(txtleafrom.Text, "dd/MM/yyyy", Nothing)
        'Dim dtend As DateTime = DateTime.ParseExact(txtleato.Text, "dd/MM/yyyy", Nothing)
        Dim yr As String = Year(Date.Now).ToString
        Dim liyear, liyearb, litot, litotb, liTotNew, liRemainRef, liRemain, liRemainb, liUsed, liUsedb, litotDeduct, liSisa, liabsen As Decimal
        Dim ldatePop As Date
        Dim lastUsed, LastBurn As Double

        If txtnikx.Value = Nothing Or txtnikx.Value = "" Then
            alert = "<script type ='text/javascript' > alert('Nik can not empty') </script>"
            Return
        ElseIf txtleavetype.Text = Nothing Or txtleavetype.Text = "" Then
            alert = "<script type ='text/javascript' > alert('Leave Type can not empty') </script>"
            Return
        ElseIf txtholiday.Text = Nothing Or txtholiday.Text = "" Then
            alert = "<script type ='text/javascript' > alert('Holiday can not empty') </script>"
            Return
        Else

            If txtholiday.Text = Nothing Then txtholiday.Text = "0"
            If txtcutabs.Text = Nothing Then txtcutabs.Text = "0"

            'Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("test_con").ConnectionString)
            Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)

            Try
                If txtholiday.Text < 0 Then
                    alert = "<script type ='text/javascript' > alert('Holiday value can not minus') </script>"
                    Return
                End If

                sqlConn.Open()
                Dim dtb As DataTable = New DataTable()
                'Dim dtblinfo As DataTable = New DataTable()
                Dim dtbyrend As DataTable = New DataTable()
                'Dim strlinfo As String = " SELECT  H_A101.Nik , H_A101.tglmasuk , H_A101.kdsite, H_A101.kdlokasi, " & _
                '"(SELECT DATEPOP FROM h_h217 WHERE NIK = '" + txtnikx.Value + "' AND YEAR = " + yr + ") AS 'DateLeavePop', " & _
                '"isnull((SELECT TOTlEAVE FROM h_h217 WHERE NIK = '" + txtnikx.Value + "' AND YEAR = " + yr + "),0) AS 'Totleave', " & _
                '"(SELECT DATEEXPIRED FROM h_h217 WHERE NIK = '" + txtnikx.Value + "' AND YEAR = " + yr + ") AS 'ExpiredLeave', " & _
                '"isnull((select SUM(atot) from L_H001 where stedit <> '2' and fstatus=1 " & _
                '"and typeleave='00' and nik = '" + txtnikx.Value + "' and yeara =   " + yr + " ),0) + " & _
                '"isnull((select SUM(btot) from L_H001 where stedit <> '2' and fstatus=1 " & _
                '"and typeleave='00' and nik =  '" + txtnikx.Value + "' and yearb =  " + yr + " ),0) as 'usedLeave', " & _
                '"isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='00' " & _
                '"and nik = '" + txtnikx.Value + "' and YEAR(trans_date) = " + yr + "),0) as 'burnLeave', " & _
                '"isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='01' " & _
                '"and nik = '" + txtnikx.Value + "' and YEAR(trans_date) = " + yr + "),0) as 'AddLeave', " & _
                '"isnull((select sum(TOTlEAVE) from H_H217 where nik = '" + txtnikx.Value + "' " & _
                '"and year < " + yr + "  and dateExpired  >= getdate()  ),0) as 'LastTot', " & _
                '"isnull((select SUM(atot) from L_H001 where stedit <> '2' and fstatus=1 " & _
                '"and typeleave='00' and nik = '" + txtnikx.Value + "' and yeara =   " + yr + " -1 ),0)  + " & _
                '"isnull((select SUM(btot) from L_H001 where stedit <> '2' and fstatus=1 " & _
                '"and typeleave='00' and nik =  '" + txtnikx.Value + "' and yearb =  " + yr + " - 1 ),0) as 'LastusedLeave', " & _
                '"isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='00' " & _
                '"and nik = '" + txtnikx.Value + "' and YEAR(trans_date) = " + yr + " -1 ),0) as 'LastburnLeave' , " & _
                '"isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='01' " & _
                '"and nik = '" + txtnikx.Value + "' and YEAR(trans_date) = " + yr + " -1 ),0) as 'LastAddLeave' " & _
                '"FROM H_A101 WHERE H_A101.Nik = '" + txtnikx.Value + "'"

                Dim strlinfo = "SELECT  H_A101.Nik , H_A101.tglmasuk ,H_A101.kdsite, H_A101.kdlokasi,(SELECT DATEPOP FROM " & _
                "h_h217 WHERE NIK = '" + txtnikx.Value + "' AND YEAR = " + yr + ") AS 'DateLeavePop', " & _
                "isnull((SELECT TOTlEAVE FROM h_h217 WHERE NIK = '" + txtnikx.Value + "' AND YEAR = " + yr + "),0) AS 'Totleave', " & _
                "(SELECT DATEEXPIRED FROM h_h217 WHERE NIK = '" + txtnikx.Value + "' AND YEAR = " + yr + ") AS 'ExpiredLeave', " & _
                "(SELECT DATEEXPIRED FROM h_h217 WHERE NIK = '" + txtnikx.Value + "' AND YEAR = " + yr + " - 1) AS 'LastExpiredLeave', " & _
                "isnull((select SUM(atot) from L_H001 where stedit <> '2' and fstatus=1 and typeleave='00' and nik = '" + txtnikx.Value + "' and yeara =   " + yr + " ),0) + " & _
                "isnull((select SUM(btot) from L_H001 where stedit <> '2' and fstatus=1 and typeleave='00' and nik =  '" + txtnikx.Value + "' and yearb =  " + yr + " ),0) as 'usedLeave', " & _
                "isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='00' and nik = '" + txtnikx.Value + "' and YEAR(trans_date) = " + yr + "),0) as 'burnLeave', " & _
                "isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='01' and nik = '" + txtnikx.Value + "' and YEAR(trans_date) = " + yr + "),0) as 'AddLeave', " & _
                "isnull((select sum(TOTlEAVE) from H_H217 where nik = '" + txtnikx.Value + "' and dateExpired >= GETDATE() and year < " + yr + "   ),0) as 'LastTot', " & _
                "isnull((select SUM(atot) from L_H001 where stedit <> '2' and fstatus=1 and typeleave='00' and nik = '" + txtnikx.Value + "' and yeara in (select year from H_H217 where nik = H_A101.Nik  AND dateExpired >= GETDATE() and year < " + yr + ")  ),0)  + " & _
                "isnull((select SUM(btot) from L_H001 where stedit <> '2' and fstatus=1 and typeleave='00' and nik =  '" + txtnikx.Value + "' and yearb in (select year from H_H217 where nik = H_A101.Nik  AND dateExpired >= GETDATE() and year < " + yr + ")  ),0) as 'LastusedLeave', " & _
                "isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='00' and nik = '" + txtnikx.Value + "' and YEAR(trans_date) <  " + yr + "  ),0) as 'LastburnLeave' ," & _
                "isnull((select SUM(totleave) from L_H002 where stedit <> '2' and fstatus=1 and ltype='01' and nik = '" + txtnikx.Value + "' and YEAR(trans_date)  <  " + yr + "  ),0) as 'LastAddLeave' ," & _
                "" & _
                "isnull((select sum(TOTlEAVE) from H_H217 where nik =  H_A101.Nik and dateExpired < GETDATE() " & _
                "and year < " + yr + "   ),0) as 'LastTotEx', " & _
                "isnull((select SUM(atot) from L_H001 " & _
                "where stedit <> '2' and fstatus=1 " & _
                "and typeleave='00' and nik = H_A101.Nik and yeara in (select year from H_H217 where nik = H_A101.Nik  AND dateExpired < GETDATE() and year < " + yr + "" & _
                "union select YEAR from l_A099 where year < (select min(year) from H_H217 where nik = H_A101.Nik  AND dateExpired < GETDATE() and year < " + yr + ") " & _
                "union select year(h_a101.tglmasuk)  )     ),0)  + " & _
                "isnull((select SUM(btot) from L_H001 " & _
                "where stedit <> '2' and fstatus=1 " & _
                "and typeleave='00' and nik =  H_A101.Nik and yearb  in (select year from H_H217 where nik = H_A101.Nik  AND dateExpired < GETDATE() and year < " + yr + " " & _
                "union select YEAR from l_A099 where year < (select min(year) from H_H217 where nik = H_A101.Nik  AND dateExpired < GETDATE() and year < " + yr + ")  " & _
                "union select year(h_a101.tglmasuk)  )    ),0) as 'LastusedLeaveEx' " & _
                "FROM H_A101 WHERE H_A101.Nik = '" + txtnikx.Value + "'"

                'Dim sdalinfo As SqlDataAdapter = New SqlDataAdapter(strlinfo, sqlConn)
                'sdalinfo.Fill(dtblinfo)


                Dim CMD As New SqlCommand("sp_ESS_LEAVE_CALC")
                CMD.Parameters.AddWithValue("@nik", txtnikx.Value)
                CMD.Parameters.AddWithValue("@argThn", yr)

                'strlinfo = strlinfo.Replace("@NIK", txtnikx.Value)
                'strlinfo = strlinfo.Replace("@YEAR", yr)
                'Dim CMD As New SqlCommand(strlinfo)

                Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
                CMD.Connection = connection
                CMD.CommandType = CommandType.StoredProcedure

                Dim sdalinfo As SqlDataAdapter = New SqlDataAdapter(CMD)
                Dim dtInfo As DataTable = New DataTable()
                sdalinfo.Fill(dtInfo)
                Dim rDB As System.Data.DataRow = dtInfo.Rows(0)

                Dim strcon As String = "select nik, year, datepop, totleave, remainleave, fhangusbig, fhangussick, fextend, fadd, dateexpired, dateextend, extendref, hangusBref, hangusSref, addref from h_h217 where nik = '" + txtnikx.Value + "'"
                'Dim strcon As String = "select nik, year, datepop, totleave, remainleave, fhangusbig, fhangussick, fextend, fadd, dateexpired, dateextend, extendref, hangusBref, hangusSref, addref from h_h217 where nik = '0000002'"
                Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
                sda.Fill(dtb)

                Dim stryrend As String = "select [year], totleave, "
                stryrend = stryrend + "(totleave - ISNULL((select sum(atot) total from L_H001 where stedit <> 2 and fstatus = 1 and typeleave = '00' and (yeara=h_h217.year) and nik = '" + txtnikx.Value + "') + "
                stryrend = stryrend + "(select sum(btot) total from L_H001 where stedit <> 2 and fstatus = 1 and typeleave = '00' and (yearb=h_h217.year) and nik = '" + txtnikx.Value + "'),0)) remainL"
                stryrend = stryrend + " from h_h217 where nik = '" + txtnikx.Value + "' and year(dateexpired) >= " + yr + " order by year asc"
                Dim sdayrend As SqlDataAdapter = New SqlDataAdapter(stryrend, sqlConn)
                sdayrend.Fill(dtbyrend)

                If txtleafrom.Text <> Nothing Then
                    Dim dtstart As DateTime = txtleafrom.Text
                    Dim dtend As DateTime = txtleato.Text


                    'Dim ts As String = (dtend - dtstart).Days
                    'ts = ts - txtholiday.Text

                    'txttotleave.Text = ts - txtholiday.Text

                    'litotDeduct = ts + dtblinfo.Rows(0)!LastTot 'total potong cuti
                    liabsen = txtcutabs.Text

                    litot = CDec(DateDiff(DateInterval.Day, dtstart, dtend)) - CDec(txtholiday.Text) + 1  'hrs cek hari libur?

                    If dtend < dtstart Then
                        alert = "<script type ='text/javascript' > alert('Date from smaller than date to, Please check date leave.') </script>"
                        Return
                    End If

                    If litot <= 0 Then
                        alert = "<script type ='text/javascript' > alert('Total Deduct Leave = 0, Cannot Recalculate, Please check date leave.') </script>"
                        Return
                    End If

                    If liabsen < 0 Then
                        alert = "<script type ='text/javascript' > alert('Value cut from absent must not minus') </script>"
                        Return
                    End If

                    litotDeduct = litot + liabsen


                    If rDB!DateLeavePop.ToString = "" Or rDB!DateLeavePop.ToString = Nothing Then
                        ldatePop = Today
                    Else
                        ldatePop = rDB!DateLeavePop.ToString
                    End If



                    Dim TotLeave As Decimal = 0
                    Dim LeaveRemain As Decimal = 0
                    Dim CarryFRemain As Decimal = 0
                    Dim usedLeave As Decimal = 0

                    If txtleavetype.Text = "Long Service Leave" Then
                        TotLeave = CDec(rDB!ltotleave.ToString)
                        LeaveRemain = CDec(rDB!lLeaveRemain.ToString)
                        CarryFRemain = CDec(rDB!lCarryFRemain.ToString)
                        usedLeave = CDec(rDB!lusedLeave.ToString)
                    Else
                        TotLeave = CDec(rDB!Totleave.ToString)
                        LeaveRemain = CDec(rDB!LeaveRemain.ToString)
                        CarryFRemain = CDec(rDB!CarryFRemain.ToString)
                        usedLeave = CDec(rDB!usedLeave.ToString)
                    End If

                    liyear = Year(ldatePop)
                    liyearb = liyear - 1

                    'cek date cuti bilamana dibawah date pop maka jatah cuti tahun ini = 0
                    If txtleafrom.Text < ldatePop Then
                        liRemain = CarryFRemain - usedLeave
                    Else
                        'remain tahun ini
                        liRemain = LeaveRemain + CarryFRemain
                    End If


                    'liRemainb = TotLeave

                    'If dtblinfo.Rows(0)!lastexpiredleave.ToString = "" Then dtblinfo.Rows(0)!lastexpiredleave = "1/1/1990"
                    'If liRemainb > 0 Then
                    '    lastUsed = dtblinfo.Rows(0)!LastusedLeave
                    '    LastBurn = dtblinfo.Rows(0)!LastburnLeave
                    '    'liRemainb = (liRemainb + dtblinfo.Rows(0)!LastAddLeave) - (lastUsed + LastBurn)

                    '    If dtblinfo.Rows(0)!LastTotEx - dtblinfo.Rows(0)!LastusedLeaveEx < 0 Then
                    '        liRemainb = (dtblinfo.Rows(0)!LastTotEx - dtblinfo.Rows(0)!LastusedLeaveEx) + (liRemainb + dtblinfo.Rows(0)!lastaddleave) - (dtblinfo.Rows(0)!lastusedleave + dtblinfo.Rows(0)!lastburnleave)
                    '    Else
                    '        liRemainb = (liRemainb + dtblinfo.Rows(0)!lastaddleave) - (dtblinfo.Rows(0)!lastusedleave - dtblinfo.Rows(0)!lastburnleave)
                    '    End If

                    '    'diganti dengan rumus diatas
                    '    'If dtblinfo.Rows(0)!lastexpiredleave < txtleafrom.Text Then
                    '    '    If ((liRemainb + dtblinfo.Rows(0)!lastaddleave) - (dtblinfo.Rows(0)!lastusedleave + dtblinfo.Rows(0)!lastburnleave) < 0) Then
                    '    '        liRemainb = (liRemainb + dtblinfo.Rows(0)!lastaddleave) - (dtblinfo.Rows(0)!lastusedleave + dtblinfo.Rows(0)!lastburnleave)
                    '    '    Else
                    '    '        liRemainb = 0
                    '    '    End If
                    '    'Else
                    '    '    liRemainb = (liRemainb + dtblinfo.Rows(0)!lastaddleave) - (dtblinfo.Rows(0)!lastusedleave + dtblinfo.Rows(0)!lastburnleave)
                    '    'End If
                    'Else
                    '    liRemainb = 0 'sdh abis expired
                    '    'ambil remain minus , akalu ada positiop nolkan krn da expired
                    '    lastUsed = dtblinfo.Rows(0)!LastusedLeave
                    '    LastBurn = dtblinfo.Rows(0)!LastburnLeave
                    '    'liRemainb = (liRemainb + dtblinfo.Rows(0)!LastAddLeave) - (lastUsed + LastBurn)
                    '    If dtblinfo.Rows(0)!lastexpiredleave < txtleafrom.Text Then
                    '        If ((liRemainb + dtblinfo.Rows(0)!lastaddleave) - (dtblinfo.Rows(0)!lastusedleave + dtblinfo.Rows(0)!lastburnleave) < 0) Then
                    '            liRemainb = (liRemainb + dtblinfo.Rows(0)!lastaddleave) - (dtblinfo.Rows(0)!lastusedleave + dtblinfo.Rows(0)!lastburnleave)
                    '        Else
                    '            liRemainb = 0
                    '        End If
                    '    Else
                    '        liRemainb = (liRemainb + dtblinfo.Rows(0)!lastaddleave) - (dtblinfo.Rows(0)!lastusedleave + dtblinfo.Rows(0)!lastburnleave)
                    '    End If

                    '    If liRemainb < 0 Then
                    '    Else
                    '        liRemainb = 0
                    '    End If

                    'End If

                    'If liRemainb > 0 Then
                    '    If litotDeduct > liRemainb Then
                    '        litotb = liRemainb
                    '        liSisa = litotDeduct - litotb
                    '        If liSisa > 0 Then
                    '            'Cek sisa cuti thn ini
                    '            If liSisa > liRemain Then
                    '                'tambahan bole cuti minus
                    '                liTotNew = liSisa
                    '                liSisa = 0
                    '            Else
                    '                liTotNew = liSisa
                    '                liSisa = 0
                    '            End If
                    '        End If
                    '    Else
                    '        litotb = litotDeduct
                    '        liSisa = 0
                    '    End If
                    'Else 'cek klo sisa cuti ga ada cek cuti thn ini
                    '    If litotDeduct > liRemain Then
                    '        'tambahan bole cuti minus
                    '        liTotNew = litotDeduct
                    '        liSisa = 0
                    '    Else
                    '        liTotNew = litotDeduct
                    '        liSisa = 0
                    '    End If
                    'End If

                    remain = (liRemain + liRemainb) - litotDeduct 'tambahan syam remain leave bisa minus

                    'perbaikan tahun cuti syam 27 July 2016
                    'Dim iyr As Integer
                    'If dtbyrend.Rows.Count > 0 Then
                    '    If dtbyrend.Rows.Count >= 2 Then
                    '        liyear = dtbyrend.Rows(1)!year.ToString()
                    '        liyearb = dtbyrend.Rows(0)!year.ToString()

                    '        For iyr = 0 To dtbyrend.Rows.Count - 1
                    '            If dtbyrend.Rows(iyr)!remainL > 0 Then
                    '                If dtbyrend.Rows(iyr)!remainL - litotDeduct > 0 Then
                    '                    liyear = (Convert.ToInt32(dtbyrend.Rows(iyr)!year) + 1).ToString()
                    '                    liyearb = dtbyrend.Rows(iyr)!year.ToString()
                    '                    liTotNew = 0
                    '                    litotb = litotDeduct
                    '                    iyr = dtbyrend.Rows.Count
                    '                Else
                    '                    liyear = (Convert.ToInt32(dtbyrend.Rows(iyr)!year) + 1).ToString()
                    '                    liyearb = dtbyrend.Rows(iyr)!year.ToString()
                    '                    litotb = litotDeduct - dtbyrend.Rows(iyr)!remainL
                    '                    liTotNew = dtbyrend.Rows(iyr)!remainL
                    '                    iyr = dtbyrend.Rows.Count
                    '                End If
                    '                'Else
                    '                '    iyr = iyr + 1
                    '            End If
                    '        Next
                    '    ElseIf dtbyrend.Rows.Count = 1 Then
                    '        If dtbyrend.Rows(0)!year = yr Then
                    '            liyear = dtbyrend.Rows(0)!year.ToString()
                    '            liyearb = (dtbyrend.Rows(0)!year - 1).ToString()

                    '            For iyr = 0 To dtbyrend.Rows.Count - 1
                    '                If dtbyrend.Rows(iyr)!remainL > 0 Then
                    '                    If dtbyrend.Rows(iyr)!remainL - litotDeduct > 0 Then
                    '                        liyear = (Convert.ToInt32(dtbyrend.Rows(iyr)!year) + 1).ToString()
                    '                        liyearb = dtbyrend.Rows(iyr)!year.ToString()
                    '                        liTotNew = 0
                    '                        litotb = litotDeduct
                    '                        iyr = dtbyrend.Rows.Count
                    '                    Else
                    '                        liyear = (Convert.ToInt32(dtbyrend.Rows(iyr)!year) + 1).ToString()
                    '                        liyearb = dtbyrend.Rows(iyr)!year.ToString()
                    '                        litotb = litotDeduct - dtbyrend.Rows(iyr)!remainL
                    '                        liTotNew = dtbyrend.Rows(iyr)!remainL
                    '                        iyr = dtbyrend.Rows.Count
                    '                    End If
                    '                    'Else
                    '                    '    iyr = iyr + 1
                    '                End If
                    '            Next
                    '        ElseIf dtbyrend.Rows(0)!year < yr Then
                    '            liyear = (dtbyrend.Rows(0)!year + 1).ToString()
                    '            liyearb = (dtbyrend.Rows(0)!year).ToString()

                    '            For iyr = 0 To dtbyrend.Rows.Count - 1
                    '                If dtbyrend.Rows(iyr)!remainL > 0 Then
                    '                    If dtbyrend.Rows(iyr)!remainL - litotDeduct > 0 Then
                    '                        liyear = (Convert.ToInt32(dtbyrend.Rows(iyr)!year) + 1).ToString()
                    '                        liyearb = dtbyrend.Rows(iyr)!year.ToString()
                    '                        liTotNew = 0
                    '                        litotb = litotDeduct
                    '                        iyr = dtbyrend.Rows.Count
                    '                    Else
                    '                        liyear = (Convert.ToInt32(dtbyrend.Rows(iyr)!year) + 1).ToString()
                    '                        liyearb = dtbyrend.Rows(iyr)!year.ToString()
                    '                        litotb = litotDeduct - dtbyrend.Rows(iyr)!remainL
                    '                        liTotNew = dtbyrend.Rows(iyr)!remainL
                    '                        iyr = dtbyrend.Rows.Count
                    '                    End If
                    '                    'Else
                    '                    '    iyr = iyr + 1
                    '                End If
                    '            Next
                    '        End If
                    '    End If
                    'Else
                    '    alert = "<script type ='text/javascript'> alert('Pop Date not found, Leave Eligible hasn't Generated') </script>"
                    'End If

                    yeara = liyear
                    yearb = liyearb


                    'end perbaikan tahun cuti
                    atot = liTotNew
                    btot = litotb

                    If txtleavetype.Text = "Long Service Leave" Then
                        TotLeave = CDec(rDB!ltotleave.ToString)
                        LeaveRemain = CDec(rDB!lLeaveRemain.ToString)
                        CarryFRemain = CDec(rDB!lCarryFRemain.ToString)
                        usedLeave = CDec(rDB!lusedLeave.ToString)
                    Else
                        TotLeave = CDec(rDB!Totleave.ToString)
                        LeaveRemain = CDec(rDB!LeaveRemain.ToString)
                        CarryFRemain = CDec(rDB!CarryFRemain.ToString)
                        usedLeave = CDec(rDB!usedLeave.ToString)
                    End If

                    If (CarryFRemain < 0) Then
                        CarryFRemain = 0
                    End If


                    txtremleave.Text = CDec(remain).ToString("#.##")
                    txtremleaveact.Value = CDec(remain).ToString("#.##")
                    If txtremleave.Text = "" Then txtremleave.Text = "0"
                    If txtremleaveact.Value = "" Then txtremleaveact.Value = "0"

                    txttotleave.Text = CDec(litotDeduct).ToString("#.##")
                    If txttotleave.Text = "" Then txttotleave.Text = "0"
                    txtcutabs.Text = 0
                    txtleaeli.Text = CDec(liRemain + liRemainb).ToString("#.##")
                    If txtleaeli.Text = "" Then txtleaeli.Text = "0"

                    Dim Liyeara As Integer = Year(dtstart)
                    liyearb = Liyeara - 1
                    yeara = Liyeara
                    yearb = Liyearb

                    If CarryFRemain > 0 Then

                        If CarryFRemain > litotDeduct Then
                            btot = litotDeduct
                            litotDeduct = 0
                        Else
                            btot = CarryFRemain
                            litotDeduct = litotDeduct - CarryFRemain
                        End If

                    End If

                    atot = litotDeduct



                    hyeara.Value = yeara
                    hyearb.Value = yearb
                    htota.Value = atot
                    htotb.Value = btot


                    
                End If

                If rDB!DateLeavePop.ToString = Nothing Or rDB!DateLeavePop.ToString = "" Then
                    alert = "<script type ='text/javascript' > alert('Nik Leave Pop Date not found, Leave Eligible hasn't Generated') </script>"
                End If


                If dtb.Rows.Count = Nothing Then
                    alert = "<script type ='text/javascript' > alert(''Nik Leave Pop Date not found, Leave Eligible hasn't Generated.) </script>"
                ElseIf dtb.Rows.Count > 0 Then
                    Dim dtleafrom As DateTime = CDate(txtleafrom.Text)
                    Dim dtpop As DateTime = CDate(dtb.Rows(0)!datepop)
                    'If dtleafrom < dtpop Then
                    '    'txtremleave.Text = "0"
                    '    txtleaeli.Text = "0"
                    '    txtremleave.Text = Format(dtb.Rows(0)!totleave, "0.00")
                    '    txtcutabs.Text = "0"
                    'Else
                    '    txtremleave.Text = Format(dtb.Rows(0)!totleave, "0.00")
                    '    txtleaeli.Text = dtb.Rows(0)!remainleave
                    '    txtcutabs.Text = "0"
                    'End If
                End If
            Catch ex As Exception
                If ex.Message.Contains("Conversion from string") Then
                    txtholiday.Text = "0"
                End If
            End Try
            'alert('test');
        End If

    End Sub

    Private Sub btnedit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnedit.Click
        'Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("test_con").ConnectionString)
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
        Dim sqllh001 As String = "select fstatus from L_H001 where transid = '" + txtnobukti.Text + "'"
        Dim dtblh001 As DataTable = New DataTable
        Dim sdalh001 As SqlDataAdapter = New SqlDataAdapter(sqllh001, conn)

        sdalh001.Fill(dtblh001)

        If dtblh001.Rows(0)!fstatus = "0" Then
            btnsave.Enabled = True
            btndelete.Enabled = True
            btnedit.Enabled = False

            txarem.Enabled = True
            txtholiday.Enabled = True
            btncalc.Enabled = True
            btfrom = "<button id='btfrom' style='height:22px'>...</button>"
            btto = "<button id='btto' style='height:22px'>...</button>"
        Else
            btfrom = "<button id='btfrom' style='height:22px' disabled='disabled'>...</button>"
            btto = "<button id='btto' style='height:22px' disabled='disabled'>...</button>"
            alert = "<script type ='text/javascript' > alert('changes can not be done as the data is processed') </script>"
        End If
    End Sub

    Private Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        Response.Redirect("leavelist.aspx")
    End Sub

    Private Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
        Dim sqlQuery As String
        Dim cmd As SqlCommand
        Dim bcheck As Boolean = True

        Try
            conn.Open()
            sqlQuery = "update L_H001 set stedit = 2 where transid = '" + txtnobukti.Text + "'"
            cmd = New SqlCommand(sqlQuery, conn)
            cmd.ExecuteScalar()
        Catch ex As Exception
            bcheck = False
        Finally
            conn.Close()
        End Try

        If bcheck = False Then
            alert = "<script type ='text/javascript' > alert('Delete Failed') </script>"
        Else
            Response.Redirect("leavelist.aspx")
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
        Dim sqlQuery As String
        Dim cmd As SqlCommand
        Dim dttm As String
        Dim createdin As String
        Dim bcheck As Boolean = True

        calc()

        Try
            conn.Open()
            sqlQuery = "update L_H001 set StatusWF = 'INT' where transid = '" + txtnobukti.Text + "'"
            cmd = New SqlCommand(sqlQuery, conn)
            cmd.ExecuteScalar()

        Catch ex As Exception
            bcheck = False
        Finally

            If bcheck = True Then
                alert = "<script type ='text/javascript' > alert('Data has been updated') </script>"
                btnSubmit.Enabled = False
            Else
                alert = "<script type ='text/javascript' > alert('data updated failed') </script>"
            End If
            conn.Close()
        End Try


    End Sub

End Class