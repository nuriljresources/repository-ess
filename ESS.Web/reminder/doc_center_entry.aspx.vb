﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Collections.Generic
Imports System.Text
Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Xml.XPath
Imports System.Data.SqlClient
Imports EXCELLENT.Module1
Imports System.Data.Sql
Imports System.Data
Imports compDocCenter
Imports compDocCenter.cDoc

Partial Public Class doc_center_entry
    Inherits System.Web.UI.Page
    Public jrnnum As String
    Public nmuser As String
    Public level As String
    Public mnentry As String
    Public mnview As String
    Public mnmgmt As String
    Public depar As String
    Public alert As String
    Public dtmoss As String
    Public nmfile As String
    Public pathfile As String
    'Private objViewsService As viewservices.Views = New Views
    'Private objListsService As mossservices.Lists = New Lists
    Private strSiteURL As String = "http://moss.jresources.com"
    Private strUserName As String = "user_nik"
    Private strDomain As String = "jresources"
    Private strPassword As String = "jrec12345"
    Private strListName As String = "91F8FF65-CF5E-4AE4-BD7D-EE41E48AFBDD"
    Private strQuery As String = ""
    Private strViewFields As String = ""
    Private strQueryOptions As String = ""
    Dim myDt As DataTable
    Dim myDt2 As DataTable
    Public dtissue As String
    Public msgbox As String
    Public mncontract As String

    Private lstViewID As New ArrayList()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim vwdt As String = "91F8FF65-CF5E-4AE4-BD7D-EE41E48AFBDD"
        Dim s As String = "http://moss.jresources.com/IT"
        Dim depurl As String
        Dim depfolder As String
        
        Dim sarr(2) As String
        Dim i As Integer = 0
        'GetViewData()
        'GetViewQuery(vwdt)
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)

        If Session("otorisasi_permit") = Nothing Or Session("otorisasi_permit") = "" Then
            Response.Redirect("signin_permit.aspx")
        End If

        If Trim(Session("permission_permit")) = "1" Or Trim(Session("permission_permit")) = "2" Then
            mnentry = "<div id='mnentry'><img alt='entry document' src='../images/open_doc_small.png' /> <a href='Doc_center_entry.aspx'>&nbsp;Set Document</a></div>"
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"

            If Session("dept").ToString().ToLower = "contract" Or Session("dept").ToString().ToLower() = "contract management system" Then
                mncontract = "<div id='mncontract'><img alt='view document' src='../images/open_doc_small.png' /> <a href='supp_permit_lisense_entry.aspx'>&nbsp;Set Permit & License</a></div>"
            End If
        End If

        If Trim(Session("permission_permit")) = "1" Then
            mnmgmt = "<div id='mnmgmt'> <img alt='Management User' src='../images/open_doc_small.png' /> <a href='mgmnt_list.aspx'>&nbsp;Management User</a></div>"
        End If

        If Trim(Session("permission_permit")) = "3" Then
            mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
        End If

        If Not IsPostBack Then
            jrnnum = Session("otorisasi_permit")
            nmuser = Session("NmUser_permit")
            'level = Session("permission_permit")

            'If level = "1" Then level = "Admin"
            'If level = "2" Then level = "Doc Admin"
            'If level = "3" Then level = "User"
            'If level = "4" Then level = "PIC"

            Dim qrydocmod As String = "select modul from ms_user where emp_code = '" + Session("otorisasi_permit") + "'"
            Try
                sqlConn.Open()
                Dim dtb As DataTable = New DataTable()
                Dim sda As SqlDataAdapter = New SqlDataAdapter(qrydocmod, sqlConn)
                sda.Fill(dtb)

                depar = dtb.Rows(0)!modul.ToString
                Session("depar") = depar
            Catch ex As Exception
            Finally
                sqlConn.Close()
            End Try

            'sarr(0) = depar
            'Main(sarr)

            'If sarr(2) <> "" Or sarr(2) <> Nothing Then
            '    alert = " <script type = 'text/javascript'> alert('" + sarr(2).ToString + "'); </script>"
            '    Return
            'End If

            txtyearx.Text = Date.Today
            Dim qry As String = "select pic_email from docpermit_pic where pic_code = '" + Session("otorisasi_permit") + "'"
            Try
                sqlConn.Open()
                Dim dtb As DataTable = New DataTable()
                Dim sda As SqlDataAdapter = New SqlDataAdapter(qry, sqlConn)
                sda.Fill(dtb)

                txtpic1.SelectedValue = dtb.Rows(0)!pic_email.ToString

            Catch ex As Exception

            Finally
                sqlConn.Close()
            End Try

            If depar = "IT" Then
                depurl = "http://moss.jresources.com/IT"
                depfolder = "/IT"
            ElseIf depar = "HR" Then
                depurl = "http://moss.jresources.com/hr"
                depfolder = "/hr"
            ElseIf depar = "HRCA" Then
                depurl = "http://moss.jresources.com/HRCA"
                depfolder = "/HRCA"
            ElseIf depar = "Legal" Then
                depurl = "http://moss.jresources.com/legal"
                depfolder = "/legal"
            ElseIf depar = "Plant" Then
                depurl = "http://moss.jresources.com/plant"
                depfolder = "/plant"
            ElseIf depar = "FINACC" Then
                depurl = "http://moss.jresources.com/finacc"
                depfolder = "/finacc"
            ElseIf depar = "IAS" Then
                depurl = "http://moss.jresources.com/ias"
                depfolder = "/ias"
            ElseIf depar = "Finance" Then
                depurl = "http://moss.jresources.com/finance"
                depfolder = "/finance"
            ElseIf depar = "Accounting" Then
                depurl = "http://moss.jresources.com/accounting"
                depfolder = "/accounting"
            ElseIf depar = "Exploration" Then
                depurl = "http://moss.jresources.com/exploration"
                depfolder = "/exploration"
            ElseIf depar = "TD" Then
                depurl = "http://moss.jresources.com/techdev"
                depfolder = "/techdev"
            End If

            'list1.Items.Add(sarr(1))
            'Dim wordColl As MatchCollection = Regex.Matches(sarr(1), " ")
            Dim olist As Microsoft.SharePoint.Client.ListItemCollection
            Dim cclass As cDoc = New cDoc()
            Dim no As Integer = 0

            'myDt = New DataTable()
            'myDt = CreateDataTable()
            'Session("myDatatable") = myDt

            myDt2 = New DataTable()
            myDt2 = CreateDataTable2()
            Session("myDatatable2") = myDt2

            Me.GridView2.DataSource = (CType(Session("myDatatable2"), DataTable)).DefaultView
            Me.GridView2.DataBind()

            'Dim qryflderlist As String = "select modul, doc_folder from ms_folder where modul = '" + depar + "'"
            'Try
            '    sqlConn.Open()
            '    Dim dtbflderlist As DataTable = New DataTable()
            '    Dim sdaflderlist As SqlDataAdapter = New SqlDataAdapter(qryflderlist, sqlConn)
            '    sdaflderlist.Fill(dtbflderlist)

            '    If dtbflderlist.Rows.Count > 0 Then
            '        For irowcount = 0 To dtbflderlist.Rows.Count - 1
            '            cclass.vGetListDocument("Syam.kharisman", "sysyam356", "jresources.com", depurl, dtbflderlist.Rows(irowcount)!doc_folder.ToString, olist, depfolder)
            '            For Each list As Microsoft.SharePoint.Client.ListItem In olist
            '                i = i + 1
            '                AddDataToTable(i, list("FileLeafRef"), list("FileRef"), CType(Session("myDatatable"), DataTable))

            '                Me.GridView1.DataSource = CType(Session("myDatatable"), DataTable).DefaultView
            '                Me.GridView1.DataBind()
            '            Next
            '        Next

            '    End If

            'Catch ex As Exception
            'Finally
            '    sqlConn.Close()
            'End Try

            'Dim node As XmlNode
            'Dim myXmlDocument As XmlDocument = New XmlDocument()
            'myXmlDocument.Load("list.xml")
            

            'node = myXmlDocument.DocumentElement

            'Dim node2 As XmlNode
            'Dim list As String



            'For Each node In node.ChildNodes
            '    For Each node2 In node.ChildNodes
            '        If node2.Name = "Row" Then
            '            'list = node2.InnerText
            '            If node2.OuterXml.ToLower.Contains(".pdf") Or node2.OuterXml.ToLower.Contains(".docx") Or _
            '            node2.OuterXml.ToLower.Contains(".zip") Or node2.OuterXml.ToLower.Contains(".rar") Or node2.OuterXml.ToLower.Contains(".xls") _
            '            Or node2.OuterXml.ToLower.Contains(".xlsx") Or node2.OuterXml.ToLower.Contains(".doc") Or node2.OuterXml.ToLower.Contains(".jpg") _
            '            Or node2.OuterXml.ToLower.Contains(".png") Then
            '                Try
            '                    urlstr = Replace(node2.OuterXml, "<Row ows_EncodedAbsUrl=", "")
            '                    urlstr = Replace(urlstr, "ows_LinkFilename=", "")
            '                    urlstr = Replace(urlstr, " />", "")
            '                    urlstr = Replace(urlstr, """", "")
            '                    'urlstr = Mid(urlstr, 1, urlstr.IndexOf(" "))
            '                    nmfile = Mid(urlstr, urlstr.IndexOf(" ") + 1, urlstr.Length)
            '                    pathfile = Mid(urlstr, 1, urlstr.IndexOf(" "))
            '                    pathfile = Replace(pathfile, "%20", " ")
            '                    'list1.Items.Add(urlstr)
            '                    'dtmoss = dtmoss + nmfile + " - " + pathfile + "<a href='" + pathfile + "'>open</a>" + "<br>"
            '                    AddDataToTable(nmfile, pathfile, CType(Session("myDatatable"), DataTable))

            '                    Me.GridView1.DataSource = CType(Session("myDatatable"), DataTable).DefaultView
            '                    Me.GridView1.DataBind()
            '                Catch ex As Exception

            '                End Try

            '            End If
            '        End If
            '    Next
            'Next
            dtissuex.Attributes.Add("readonly", "readonly")
            dtexp.Attributes.Add("readonly", "readonly")
            txtUserName.Attributes.Add("readonly", "readonly")
            txtnik.Attributes.Add("readonly", "readonly")
            'dtissue = "<script type = 'text/javascript'> var cal = Calendar.setup({onSelect: function(cal) { cal.hide() }, showTime: false }); cal.manageFields('btdtissue', 'ctl00_ContentPlaceHolder1_dtissuex', '%m/%d/%Y'); cal.manageFields('btdtexp', 'dtexp', '%m/%d/%Y'); </script>"
        End If
        'For i = 0 To wordColl.Count

        '    s = Replace(Replace(sarr(1), Chr(10), ""), Chr(13), "")
        '    list1.Items.Add(sarr(1))
        'Next

    End Sub

    Public Sub GetViewQuery(ByVal strListName As String)
        'Try
        '#Region "Source URL"
        'get the lists for the Source URL
        '    Me.objViewsService.Url = Me.strSiteURL.Trim("/"c) & "/_vti_bin/views.asmx"

        '    'get the domain
        '    If Me.strUserName.IndexOf("\") > 0 Then
        '        Me.strDomain = Me.strUserName.Split("\".ToCharArray())(0)
        '        Me.strUserName = Me.strUserName.Split("\".ToCharArray())(1)
        '    End If

        '    Me.objViewsService.Credentials = New System.Net.NetworkCredential(Me.strUserName, Me.strPassword, Me.strDomain)


        '    '#End Region

        '    Dim xnAllView As System.Xml.XmlNode = Me.objViewsService.GetViewCollection(strListName)
        '    For Each node As System.Xml.XmlNode In xnAllView
        '        If node.Name = "View" Then
        '            Me.lstViewID.Add(node.Attributes("Name").Value)
        '        End If
        '    Next

        '    'Get the First View
        '    Dim xnViewData As System.Xml.XmlNode = Me.objViewsService.GetView(Me.strListName, lstViewID(0).ToString())

        '    For Each node As System.Xml.XmlNode In xnViewData
        '        If node.Name = "Query" Then
        '            Me.strQuery = node.InnerXml
        '        End If
        '        If node.Name = "ViewFields" Then
        '            Me.strViewFields = node.InnerXml
        '        End If

        '    Next
        'Catch exp As System.Exception
        '    'MessageBox.Show(exp.ToString())
        'End Try
    End Sub

    Public Sub GetViewData()
        'Dim s As XNamespace = "http://schemas.microsoft.com/sharepoint/soap/"
        'Dim rs As XNamespace = "urn:schemas-microsoft-com:rowset"
        'Dim z As XNamespace = "#RowsetSchema"
        'Dim ds As New DataSet()
        'Dim dt As New DataTable()
        'Dim str As String
        'Dim str2 As String
        'Dim str3 As String
        'Try
        '    '#Region "Source URL"
        '    'get the lists for the Source URL
        '    Me.objListsService.Url = Me.strSiteURL.Trim("/"c) & "/_vti_bin/lists.asmx"

        '    'get the domain
        '    If Me.strUserName.IndexOf("\") > 0 Then
        '        Me.strDomain = Me.strUserName.Split("\".ToCharArray())(0)
        '        Me.strUserName = Me.strUserName.Split("\".ToCharArray())(1)
        '    End If

        '    'Me.objListsService.Credentials = New System.Net.NetworkCredential(Me.strUserName, Me.strPassword, Me.strDomain)
        '    Me.objListsService.Credentials = New System.Net.NetworkCredential(Me.strUserName, Me.strPassword)

        '    '#End Region

        '    'get the Data from the List
        '    Dim xdListData As New System.Xml.XmlDocument()
        '    Dim xnQuery As System.Xml.XmlNode = xdListData.CreateElement("Query")
        '    Dim xnViewFields As System.Xml.XmlNode = xdListData.CreateElement("ViewFields")
        '    Dim xnQueryOptions As System.Xml.XmlNode = xdListData.CreateElement("QueryOptions")

        '    '*Use CAML query*/ 
        '    xnQuery.InnerXml = Me.strQuery
        '    xnViewFields.InnerXml = Me.strViewFields
        '    xnQueryOptions.InnerXml = Me.strQueryOptions

        '    xnQueryOptions.InnerXml = "<IncludeAttachmentUrls>TRUE</IncludeAttachmentUrls>"


        '    Dim xnListData As System.Xml.XmlNode = Me.objListsService.GetListItems(Me.strListName, Nothing, xnQuery, xnViewFields, Nothing, xnQueryOptions, _
        '     Nothing)

        '    'Dim sr As New StringReader(xnListData.OuterXml.Replace("ows_", ""))

        '    'Dim sr As New StringReader(xnListData.OuterXml)
        '    'ds.ReadXml(sr)
        '    'ds.ReadXml(xnListData.OuterXml.Replace("ows_", ""))
        '    Dim oNodes As XmlNodeList = xnListData.ChildNodes
        '    For Each node As XmlNode In oNodes
        '        Dim objReader As New XmlNodeReader(node)

        '        Dim listCollection As XElement = GetXElement(node)
        '        Dim queryOptions = _
        '    <QueryOptions>
        '        <Folder/>
        '        <IncludeMandatoryColumns>false</IncludeMandatoryColumns>
        '    </QueryOptions>

        '        Dim viewFields = <ViewFields/>

        '        Dim report As XElement = <report></report>

        '        While objReader.Read()
        '            If objReader("ows_EncodedAbsUrl") IsNot Nothing AndAlso objReader("ows_LinkFilename") IsNot Nothing Then
        '                str = objReader("ows_EncodedAbsUrl").ToString()
        '                str2 = objReader("ows_LinkFilename").ToString()
        '                str3 = report.ToStringAlignAttributes()
        '                'list1.Items.Add(str + " " + str2)
        '            End If
        '        End While
        '    Next


        'str = ds.Tables.Item(0).ToString

        'str2 = ds.Tables(1).ToString
        'Return
        'Catch exp As System.Exception
        '    'MessageBox.Show(exp.ToString())
        '    Return
        'End Try
    End Sub

    Private Function GetItems(ByVal webPath As String) As System.Xml.XmlNode


        Dim listsWS As mossservices.Lists = New Lists()

        listsWS.Url = webPath & "/_vti_bin/lists.asmx"

        listsWS.Credentials = New System.Net.NetworkCredential(Me.strUserName, Me.strPassword)
        'listsWS.UseDefaultCredentials = True

        Dim doc As New System.Xml.XmlDocument()

        doc.LoadXml("<Document><Query /><ViewFields /><QueryOptions /></Document>")

        Dim listQuery As System.Xml.XmlNode = doc.SelectSingleNode("Query")

        Dim listViewFields As System.Xml.XmlNode = doc.SelectSingleNode("ViewFields")

        Dim listQueryOptions As System.Xml.XmlNode = doc.SelectSingleNode("QueryOptions")

        Dim g As Guid = GetWebID(webPath)

        Dim items As System.Xml.XmlNode = listsWS.GetListItems(strListName, Nothing, listQuery, listViewFields, Nothing, listQueryOptions, _
        g.ToString())
        'Dim items As System.Xml.XmlNode = listsWS.GetListItems(strListName, Nothing, listQuery, listViewFields, Nothing, listQueryOptions, _
        '"3D792D7E-0202-4630-9A04-0C1A50C98936")
        Return items

    End Function


    Private Function GetWebID(ByVal webPath As String) As Guid

        Dim siteDataWS As New sitedataservices.SiteData()

        siteDataWS.Credentials = New System.Net.NetworkCredential(Me.strUserName, Me.strPassword)
        'siteDataWS.UseDefaultCredentials = True

        Dim webMetaData As sitedataservices._sWebMetadata
        Dim arrWebWithTime As sitedataservices._sWebWithTime()
        Dim arrListWithTime As sitedataservices._sListWithTime()
        Dim arrUrls As sitedataservices._sFPUrl()
        Dim roles As String
        Dim roleUsers As String()
        Dim roleGroups As String()

        siteDataWS.Url = webPath & "/_vti_bin/sitedata.asmx"
        Dim i As UInteger = siteDataWS.GetWeb(webMetaData, arrWebWithTime, arrListWithTime, arrUrls, roles, roleUsers, _
         roleGroups)

        Dim g As New Guid(webMetaData.WebID)

        Return g

    End Function

    Private Function CreateDataTable() As DataTable
        Dim myDataTable As DataTable = New DataTable()

        Dim myDataColumn As DataColumn

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "fname"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn()
        myDataColumn.DataType = Type.GetType("System.String")
        myDataColumn.ColumnName = "path"
        myDataTable.Columns.Add(myDataColumn)

        Return myDataTable
    End Function

    Private Sub AddDataToTable(ByVal num As String, ByVal fname As String, ByVal path As String, ByVal myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("No") = num
        row("fname") = fname
        row("path") = path

        myTable.Rows.Add(row)

    End Sub

    'Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
    '    Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
    '    Dim depurl As String
    '    Dim depfolder As String
    '    depar = Session("depar")
    '    myDt = New DataTable()
    '    myDt = CreateDataTable()
    '    Session("myDatatable") = myDt

    '    If depar = "IT" Then
    '        depurl = "http://moss.jresources.com/IT"
    '        depfolder = "/IT"
    '    ElseIf depar = "HR" Then
    '        depurl = "http://moss.jresources.com/hr"
    '        depfolder = "/hr"
    '    ElseIf depar = "HRCA" Then
    '        depurl = "http://moss.jresources.com/HRCA"
    '        depfolder = "/HRCA"
    '    ElseIf depar = "Legal" Then
    '        depurl = "http://moss.jresources.com/legal"
    '        depfolder = "/legal"
    '    ElseIf depar = "Plant" Then
    '        depurl = "http://moss.jresources.com/plant"
    '        depfolder = "/plant"
    '    ElseIf depar = "FINACC" Then
    '        depurl = "http://moss.jresources.com/finacc"
    '        depfolder = "/finacc"
    '    ElseIf depar = "IAS" Then
    '        depurl = "http://moss.jresources.com/ias"
    '        depfolder = "/ias"
    '    ElseIf depar = "Finance" Then
    '        depurl = "http://moss.jresources.com/finance"
    '        depfolder = "/finance"
    '    ElseIf depar = "Accounting" Then
    '        depurl = "http://moss.jresources.com/accounting"
    '        depfolder = "/accounting"
    '    ElseIf depar = "Exploration" Then
    '        depurl = "http://moss.jresources.com/exploration"
    '        depfolder = "/exploration"
    '    ElseIf depar = "techdev" Then
    '        depurl = "http://moss.jresources.com/techdev"
    '        depfolder = "/techdev"
    '    End If

    '    'Me.GridView1.DataSource = (CType(Session("myDatatable"), DataTable)).DefaultView
    '    'Me.GridView1.DataBind()

    '    'Dim node As XmlNode
    '    'Dim myXmlDocument As XmlDocument = New XmlDocument()
    '    ''myXmlDocument.Load("list.xml")
    '    'If Session("depar") = "IT" Then
    '    '    myXmlDocument.Load("C:\inetpub\wwwroot\HRMS\doc\list.xml")
    '    'ElseIf Session("depar") = "HR" Then
    '    '    myXmlDocument.Load("C:\inetpub\wwwroot\HRMS\doc\hrlist.xml")
    '    'ElseIf Session("depar") = "HRCA" Then
    '    '    myXmlDocument.Load("C:\inetpub\wwwroot\HRMS\doc\hrcalist.xml")
    '    'ElseIf Session("depar") = "Legal" Then
    '    '    myXmlDocument.Load("C:\inetpub\wwwroot\HRMS\doc\legallist.xml")
    '    'ElseIf Session("depar") = "Plant" Then
    '    '    myXmlDocument.Load("C:\inetpub\wwwroot\HRMS\doc\plantlist.xml")
    '    'ElseIf Session("depar") = "FINACC" Then
    '    '    myXmlDocument.Load("C:\inetpub\wwwroot\HRMS\doc\finacclist.xml")
    '    'ElseIf Session("depar") = "IAS" Then
    '    '    myXmlDocument.Load("C:\inetpub\wwwroot\HRMS\doc\iaslist.xml")
    '    'ElseIf Session("depar") = "Finance" Then
    '    '    myXmlDocument.Load("C:\inetpub\wwwroot\HRMS\doc\financelist.xml")
    '    'ElseIf Session("depar") = "Accounting" Then
    '    '    myXmlDocument.Load("C:\inetpub\wwwroot\HRMS\doc\accountinglist.xml")
    '    'ElseIf Session("depar") = "Exploration" Then
    '    '    myXmlDocument.Load("C:\inetpub\wwwroot\HRMS\doc\explorationlist.xml")
    '    'ElseIf Session("depar") = "techdev" Then
    '    '    myXmlDocument.Load("C:\inetpub\wwwroot\HRMS\doc\techdevlist.xml")
    '    'End If
    '    'node = myXmlDocument.DocumentElement

    '    'Dim node2 As XmlNode
    '    'Dim list As String

    '    'For Each node In node.ChildNodes
    '    '    For Each node2 In node.ChildNodes
    '    '        If node2.Name = "Row" Then
    '    '            'list = node2.InnerText
    '    '            If node2.OuterXml.ToLower.Contains(".pdf") Or node2.OuterXml.ToLower.Contains(".docx") Or _
    '    '            node2.OuterXml.ToLower.Contains(".zip") Or node2.OuterXml.ToLower.Contains(".rar") Or node2.OuterXml.ToLower.Contains(".xls") _
    '    '            Or node2.OuterXml.ToLower.Contains(".xlsx") Or node2.OuterXml.ToLower.Contains(".doc") Then
    '    '                Try
    '    '                    urlstr = Replace(node2.OuterXml, "<Row>ows_EncodedAbsUrl=", "")
    '    '                    urlstr = Replace(urlstr, "ows_LinkFilename=", "")
    '    '                    urlstr = Replace(urlstr, " />", "")
    '    '                    urlstr = Replace(urlstr, """", "")
    '    '                    'urlstr = Mid(urlstr, 1, urlstr.IndexOf(" "))
    '    '                    nmfile = Mid(urlstr, urlstr.IndexOf(" ") + 1, urlstr.Length)
    '    '                    pathfile = Mid(urlstr, 1, urlstr.IndexOf(" "))
    '    '                    pathfile = Replace(pathfile, "%20", " ")
    '    '                    'list1.Items.Add(urlstr)
    '    '                    'dtmoss = dtmoss + nmfile + " - " + pathfile + "<a href='" + pathfile + "'>open</a>" + "<br>"
    '    '                    'AddDataToTable(nmfile, pathfile, CType(Session("myDatatable"), DataTable))

    '    '                    Me.GridView1.DataSource = CType(Session("myDatatable"), DataTable).DefaultView
    '    '                    Me.GridView1.DataBind()
    '    '                Catch ex As Exception

    '    '                End Try

    '    '            End If
    '    '        End If
    '    '    Next
    '    'Next

    '    Dim olist As Microsoft.SharePoint.Client.ListItemCollection
    '    Dim cclass As cDoc = New cDoc()
    '    Dim no As Integer = 0

    '    Me.GridView1.DataSource = (CType(Session("myDatatable"), DataTable)).DefaultView
    '    Me.GridView1.DataBind()

    '    Dim qryflderlist As String = "select modul, doc_folder from ms_folder where modul = '" + depar + "'"
    '    Try
    '        sqlConn.Open()
    '        Dim dtbflderlist As DataTable = New DataTable()
    '        Dim sdaflderlist As SqlDataAdapter = New SqlDataAdapter(qryflderlist, sqlConn)
    '        sdaflderlist.Fill(dtbflderlist)

    '        If dtbflderlist.Rows.Count > 0 Then
    '            For irowcount = 0 To dtbflderlist.Rows.Count - 1
    '                cclass.vGetListDocument("Syam.kharisman", "sysyam356", "jresources.com", depurl, dtbflderlist.Rows(irowcount)!doc_folder.ToString, olist, depfolder)
    '                For Each list As Microsoft.SharePoint.Client.ListItem In olist
    '                    I = I + 1
    '                    AddDataToTable(I, list("FileLeafRef"), list("FileRef"), CType(Session("myDatatable"), DataTable))

    '                    Me.GridView1.DataSource = CType(Session("myDatatable"), DataTable).DefaultView
    '                    Me.GridView1.DataBind()
    '                Next
    '            Next

    '        End If

    '    Catch ex As Exception
    '    Finally
    '        sqlConn.Close()
    '    End Try

    '    If Trim(Session("permission_permit")) = "1" Or Trim(Session("permission_permit")) = "2" Then
    '        mnentry = "<div id='mnentry'><img alt='entry document' src='../images/open_doc_small.png' /> <a href='Doc_center_entry.aspx'>&nbsp;Set Document</a></div>"
    '        mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
    '    End If

    '    If Trim(Session("permission_permit")) = "1" Then
    '        mnmgmt = "<div id='mnmgmt'> <img alt='Management User' src='../images/open_doc_small.png' /> <a href='mgmnt_list.aspx'>&nbsp;Management User</a></div>"
    '    End If

    '    If Trim(Session("permission_permit")) = "3" Or Trim(Session("permission_permit")) = "4" Then
    '        mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
    '    End If

    '    GridView1.PageIndex = e.NewPageIndex
    '    GridView1.DataBind()

    'End Sub

    'Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GridView1.SelectedIndexChanged
    '    If GridView1.SelectedRow Is Nothing Then Exit Sub

    '    Dim row As GridViewRow = GridView1.SelectedRow
    '    'Session("docedituser") = GridView1.DataKeys(row.RowIndex).Value.ToString

    '    temnmdoc.Value = GridView1.DataKeys(row.RowIndex).Value.ToString
    '    temnmpath.Value = GridView1.SelectedRow.Cells(2).Text
    '    'Response.Redirect("doc_center.aspx")

    '    If Trim(Session("permission_permit")) = "1" Or Trim(Session("permission_permit")) = "2" Then
    '        mnentry = "<div id='mnentry'><img alt='entry document' src='../images/open_doc_small.png' /> <a href='Doc_center_entry.aspx'>&nbsp;Set Document</a></div>"
    '        mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
    '    End If

    '    If Trim(Session("permission_permit")) = "1" Then
    '        mnmgmt = "<div id='mnmgmt'> <img alt='Management User' src='../images/open_doc_small.png' /> <a href='mgmnt_list.aspx'>&nbsp;Management User</a></div>"
    '    End If

    '    If Trim(Session("permission_permit")) = "3" Or Trim(Session("permission_permit")) = "4" Then
    '        mnview = "<div id='mnview'><img alt='view document' src='../images/open_doc_small.png' /> <a href='permit_list.aspx'>&nbsp;View Lists</a></div>"
    '    End If

    '   End Sub

    Private Function CreateDataTable2() As DataTable
        Dim myDataTable2 As DataTable = New DataTable()

        Dim myDataColumn2 As DataColumn

        myDataColumn2 = New DataColumn()
        myDataColumn2.DataType = Type.GetType("System.String")
        myDataColumn2.ColumnName = "Name"
        myDataTable2.Columns.Add(myDataColumn2)

        myDataColumn2 = New DataColumn()
        myDataColumn2.DataType = Type.GetType("System.String")
        myDataColumn2.ColumnName = "Nik"
        myDataTable2.Columns.Add(myDataColumn2)

        myDataColumn2 = New DataColumn()
        myDataColumn2.DataType = Type.GetType("System.String")
        myDataColumn2.ColumnName = "Schedule"
        myDataTable2.Columns.Add(myDataColumn2)

        Return myDataTable2
    End Function

    Private Sub AddDataToTable2(ByVal name As String, ByVal nik As String, ByVal Schedule As String, ByVal myTable2 As DataTable)
        Dim row2 As DataRow

        row2 = myTable2.NewRow()

        row2("Name") = name
        row2("Nik") = nik
        row2("Schedule") = Schedule

        myTable2.Rows.Add(row2)

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim li_count As Integer

        If GridView2.Rows.Count > 0 Then
            For li_count = 0 To GridView2.Rows.Count - 1
                If GridView2.DataKeys(li_count).Value.ToString = txtnik.Text Then
                    lblTips.Text = "User already exists in the list"
                    Exit Sub
                End If
            Next
        End If

        If Me.txtUserName.Text.Trim().Equals("") Then

            Me.lblTips.Text = "You must fill a username."

        Else

            AddDataToTable2(Me.txtUserName.Text.Trim(), Me.txtnik.Text.Trim(), Me.ddlsch.SelectedValue.Trim(), CType(Session("myDatatable2"), DataTable))

            Me.GridView2.DataSource = CType(Session("myDatatable2"), DataTable).DefaultView

            Me.GridView2.DataBind()

            Me.txtUserName.Text = ""
            Me.txtnik.Text = ""
            Me.lblTips.Text = ""
            Me.ddlsch.SelectedValue = ""

        End If
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsave.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim dtblist As DataTable = New DataTable()
        dtblist.Merge(Session("mydatatable2"))
        Dim sqlQuery As String
        Dim cmd As SqlCommand
        Dim lcom As SqlCommand
        Dim dttm As String
        Dim dtmnow As String
        Dim dtynow As String
        Dim stripno As String
        Dim permitid As String
        Dim nmcomp As String
        Dim nmsite As String
        Dim mustreport As String
        Dim i As Integer
        Dim ls_sql As String
        Dim li_count As Integer

        If nmdocx.Text = Nothing Or nmdocx.Text = "" Then
            lblTips.Text = "File Name Cannot be Empty"
            Exit Sub
        End If

        If rem_days.SelectedValue <> 0 Then mustreport = "1"
        dttm = Date.Now.ToString
        Dim div As String = Session("divisi")

        If dtblist.Rows.Count > 0 Then
            Try
                msgbox = ""
                conn.Open()
                Dim strcon As String = "select id from docpermit_trans"
                Dim dtb As DataTable = New DataTable
                Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, conn)

                dtmnow = DateTime.Now.ToString("MM")
                dtynow = Date.Now.Year.ToString
                stripno = dtynow + dtmnow + "/"

                sda.Fill(dtb)

                If dtb.Rows.Count = 0 Then
                    permitid = stripno + "000001"
                ElseIf dtb.Rows.Count > 0 Then
                    i = 0
                    i = dtb.Rows.Count + 1
                    If dtb.Rows.Count > 0 And dtb.Rows.Count < 9 Then
                        permitid = stripno + "00000" + i.ToString
                    ElseIf dtb.Rows.Count >= 9 And dtb.Rows.Count < 99 Then
                        permitid = stripno + "0000" + i.ToString
                    ElseIf dtb.Rows.Count >= 99 And dtb.Rows.Count < 999 Then
                        permitid = stripno + "000" + i.ToString
                    ElseIf dtb.Rows.Count >= 999 And dtb.Rows.Count < 9999 Then
                        permitid = stripno + "00" + i.ToString
                    ElseIf dtb.Rows.Count >= 9999 And dtb.Rows.Count < 99999 Then
                        permitid = stripno + "0" + i.ToString
                    ElseIf dtb.Rows.Count >= 99999 And dtb.Rows.Count < 999999 Then
                        permitid = stripno + i.ToString
                    ElseIf dtb.Rows.Count >= 999999 Then
                        permitid = "Error on generate Tripnum"
                    End If
                End If

                Dim strcon2 As String = "select Nmcompany from jrn_test.dbo.H_A110 where kdcompany = '" + ddlcomp.SelectedValue + "'"
                Dim dtb2 As DataTable = New DataTable
                Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, conn)
                sda2.Fill(dtb2)

                nmcomp = dtb2.Rows(0)!Nmcompany.ToString

                Dim strcon3 As String = "select Nmsite from jrn_test.dbo.H_A120 where kdsite = '" + ddlsite.SelectedValue + "'"
                Dim dtb3 As DataTable = New DataTable
                Dim sda3 As SqlDataAdapter = New SqlDataAdapter(strcon3, conn)
                sda3.Fill(dtb3)

                nmsite = dtb3.Rows(0)!Nmsite.ToString
                Dim createdin As String
                createdin = HttpContext.Current.Request.UserHostName

                sqlQuery = "Insert into docpermit_trans (id,doctype,fsubject,kdcom,company_name,kdsite,site_name,fyear,fcategory,department,subdepartment,cms_start,cms_end, f_path, f_name, stedit,createdtime, must_report, rem_code, kddiv, modul, createdby, createdin, vendor) values ('" + permitid + "' , '" + doctp.SelectedValue + "', '" + subject.Text + "', '" + ddlcomp.SelectedValue + "', '" + nmcomp + "', '" + ddlsite.SelectedValue + "', '" + nmsite + "', '" + "" + "', '" + "" + "', '" + "" + "', '" + "" + "', '" + dtissuex.Text + "', '" + dtexp.Text + "', '" + txturl.Value + "', '" + nmdocx.Text + "', '1','" + dttm + "','" + mustreport + "','" + rem_days.SelectedValue + "','" + div + "' ,'" + Session("modul_permit").ToString + "', '" + Session("otorisasi_permit") + "', '" + createdin + "', '" + txtvendor.Text + "')"
                'sqlQuery = "Insert into docpermit_trans (id,ftype,fsubject,kdcom,company_name,kdsite,site_name,fyear,fcategory,doctype,department,subdepartment,number,must_report,cms_start,cms_end, f_path, f_name, stedit) values ('" + permitid + "' , '" + _type + "', '" + _subject + "', '" + _comp + "', '" + nmcomp + "', '" + _site + "', '" + nmsite + "', '" + _year + "', '" + _category + "', '" + _doctype + "', '" + _department + "', '" + _subdept + "', '" + _number + "', '" + _report + "', '" + _dtissue + "', '" + _dtexp + "', 'C:\uploads\" + _fileupload1 + "', '" + _filename + "', '1' )"
                cmd = New SqlCommand(sqlQuery, conn)
                cmd.ExecuteScalar()

                For li_count = 0 To dtblist.Rows.Count - 1
                    ls_sql = "insert into docpermit_trans_sch (id, nik, schedule) values ('" + permitid + "', '" + dtblist.Rows(li_count)!Nik.ToString + "', '" + dtblist.Rows(li_count)!Schedule.ToString + "')"
                    lcom = New SqlCommand(ls_sql, conn)
                    lcom.ExecuteScalar()
                Next

                msgbox = "<script type ='text/javascript'> alert('Data has been saved'); </script>"
            Catch ex As Exception
                'messege error
            Finally
                conn.Close()
            End Try

            If msgbox <> "" Or msgbox <> Nothing Then
                Response.Redirect("doc_center_entry.aspx")
            End If
        Else
            lblTips.Text = "Please select user to associated with this document"
        End If
    End Sub

    Private Sub GridView2_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridView2.RowDeleting
        Dim row As GridViewRow = GridView2.SelectedRow
        'Dim pid As String
        Dim i As Integer

        i = e.RowIndex
        'pid = GridView2.DataKeys(i).Value.ToString

        'GridView2.DeleteRow(i)
        Dim dtblist As DataTable = New DataTable()
        dtblist.Merge(Session("mydatatable2"))
        dtblist.Rows.RemoveAt(i)
        Me.GridView2.DataSource = dtblist
        Session("myDatatable2") = dtblist
        Me.GridView2.DataBind()
    End Sub
End Class