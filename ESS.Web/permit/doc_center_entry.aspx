﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/permit/master.Master" CodeBehind="doc_center_entry.aspx.vb" Inherits="EXCELLENT.doc_center_entry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Document Center Entry
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="../Scripts/jscal2.js" type ="text/javascript"></script>
<script src="../Scripts/en.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="../css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="../css/win2k/win2k.css" />
<style type="text/css" >
        #form1
        {
            font-family:Tahoma,Arial;
        }
        #form1 h1 
        {
            color: #FFFFFF;
            font-size:x-large;
        }
        .title
        {
            color: blue;
            font-size: small;
            margin-bottom:10px;
        }
        .line
        {
            width: 100%;
            height: 20px;
            font-family:Tahoma,Arial;
            font-size:0.8em;
        }
        .general
        {
            font-size:0.8em;
            color:#33ADFF;
            font-weight:bold;
            border-style:solid;
            border-width:1px;
            border-color:#33ADFF;
            padding-left:5px;
        }
        
        .general a
        {
            text-decoration:none;
            color:#33ADFF;
        }
        
        .general a:hover
        {
            text-decoration:underline;
            color:#33ADFF;
        }
        .style1
        {
            height: 26px;
        }
    </style>
    <script type ="text/javascript" >
        function OpenPopupdel(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("findpic.aspx", "List", "scrollbars=yes,resizable=no,width=600,height=500");
            return false;
        }
        window.onload = function() {
            var check = document.getElementById('<%=rpt.ClientID%>');
            //            check.onchange = function() {
            //                if (this.checked == true)
            //                    document.getElementById('<%=rem_days.ClientID%>').style.visibility = "visible";
            //                else
            //                    document.getElementById('<%=rem_days.ClientID%>').style.visibility = "hidden";
            //            };
            var check2 = document.getElementById('<%=chkpic1.ClientID%>');
            check2.onchange = function() {
                if (this.checked == true)
                    document.getElementById('<%=txtpic1.ClientID%>').style.visibility = "visible";
                else
                    document.getElementById('<%=txtpic1.ClientID%>').style.visibility = "hidden";
            };
            var check3 = document.getElementById('<%=chkpic2.ClientID%>');
            check3.onchange = function() {
                if (this.checked == true)
                    document.getElementById('<%=txtpic2.ClientID%>').style.visibility = "visible";
                else
                    document.getElementById('<%=txtpic2.ClientID%>').style.visibility = "hidden";
            };
            var check4 = document.getElementById('<%=chkpic3.ClientID%>');
            check4.onchange = function() {
                if (this.checked == true)
                    document.getElementById('<%=txtpic3.ClientID%>').style.visibility = "visible";
                else
                    document.getElementById('<%=txtpic3.ClientID%>').style.visibility = "hidden";
            };
            var check5 = document.getElementById('<%=chkpic4.ClientID%>');
            check5.onchange = function() {
                if (this.checked == true)
                    document.getElementById('<%=txtpic4.ClientID%>').style.visibility = "visible";
                else
                    document.getElementById('<%=txtpic4.ClientID%>').style.visibility = "hidden";
            };
            var check6 = document.getElementById('<%=chkpic5.ClientID%>');
            check6.onchange = function() {
                if (this.checked == true)
                    document.getElementById('<%=txtpic5.ClientID%>').style.visibility = "visible";
                else
                    document.getElementById('<%=txtpic5.ClientID%>').style.visibility = "hidden";
            };

            document.getElementById('nmdoc').value = document.getElementById('<%=temnmdoc.ClientID%>').value;
            document.getElementById('txturl').value = document.getElementById('<%=temnmpath.ClientID%>').value;
        };

        function change() {
            if (document.getElementById('<%=txtpic1.ClientID%>').style.visibility == "hidden") {
                document.getElementById('<%=txtpic1.ClientID%>').value = "";
            }

            if (document.getElementById('<%=txtpic2.ClientID%>').style.visibility == "hidden") {
                document.getElementById('<%=txtpic2.ClientID%>').value = "";
            }

            if (document.getElementById('<%=txtpic3.ClientID%>').style.visibility == "hidden") {
                document.getElementById('<%=txtpic3.ClientID%>').value = "";
            }

            if (document.getElementById('<%=txtpic4.ClientID%>').style.visibility == "hidden") {
                document.getElementById('<%=txtpic4.ClientID%>').value = "";
            }

            if (document.getElementById('<%=txtpic5.ClientID%>').style.visibility == "hidden") {
                document.getElementById('<%=txtpic5.ClientID%>').value = "";
            }
            
            var fullPath = document.getElementById('<%= Fileupload1.ClientID %>').value;

            if (fullPath) {
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                var obj1 = document.getElementById("filename");
                var obj2 = document.getElementById("AttachmentTitleTextbox");

                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    obj2.value = filename;
                }
                //                alert(filename);
            }
        }

        function getFileExtension(filename) {
            var ext = /^.+\.([^.]+)$/.exec(filename);
            return ext == null ? "" : ext[1];
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function simpan() {
            var fullPath = document.getElementById('<%= Fileupload1.ClientID %>').value;

        }

        function save() {
        
            if (document.getElementById('<%=chkpic1.ClientID%>').checked == false) {
                document.getElementById('<%=txtpic1.ClientID%>').value = ""
            }

            if (document.getElementById('<%=chkpic2.ClientID%>').checked == false) {
                document.getElementById('<%=txtpic2.ClientID%>').value = ""
            }

            if (document.getElementById('<%=chkpic3.ClientID%>').checked == false) {
                document.getElementById('<%=txtpic3.ClientID%>').value = ""
            }

            if (document.getElementById('<%=chkpic4.ClientID%>').checked == false) {
                document.getElementById('<%=txtpic4.ClientID%>').value = ""
            }

            if (document.getElementById('<%=chkpic5.ClientID%>').checked == false) {
                document.getElementById('<%=txtpic5.ClientID%>').value = ""
            }
            
            var fullPath = document.getElementById("txturl").value;
            //alert(fullPath);
            if (fullPath.length == 0) {
                document.getElementById('<%= label1.ClientID %>').text = 'You have not specified a file.'
                return;
            }

//            if (fullPath) {
//                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
//                var filename = fullPath.substring(startIndex);
//                var obj1 = document.getElementById("filename");
//                var obj2 = document.getElementById("AttachmentTitleTextbox");

//                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
//                    filename = filename.substring(1);
//                    obj2.value = filename;
//                }
//                //                alert(filename);
//            }

//            var extension = getFileExtension(fullPath);

//            if (extension.toLowerCase() != 'pdf') {
//                alert("Only .pdf files allowed!");
//                return;
//            }

            var d = document;
            var torpt;
            var s = document.getElementById('<%=doctp.ClientID%>');
            var type = s.options[s.selectedIndex].value;

            var s2 = document.getElementById('<%=doctp.ClientID%>');
            var dtype = s2.options[s2.selectedIndex].value;

            //var fname = dtype + "_" + d.getElementById("subject").value + "_" + d.getElementById('<%=ddlcomp.ClientID%>').value + "_" + d.getElementById('<%=ddlsite.ClientID%>').value + "_" + d.getElementById("txtyear").value + "_" + filename;
            var fname = d.getElementById("nmdoc");
            var check = document.getElementById('<%=rpt.ClientID%>');
            if (check.checked == true) {
                torpt = 1
            }

            if (check.checked == false) {
                torpt = 0
            }

            if (d.getElementById("type").value.length == 0) {
                alert("Please Entry Document Type");
                return false;
            }

            d.getElementById('<%=tipe.ClientID%>').value = type
            d.getElementById('<%=doctype2.ClientID%>').value = dtype

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../WS/Service.asmx/Addpermit",
                //data: "{'_type':" + JSON.stringify(type) + ",'_subject':" + JSON.stringify(document.getElementById("subject").value) + ",'_comp':" + JSON.stringify(document.getElementById('<%=ddlcomp.ClientID%>').value) + ",'_site':" + JSON.stringify(document.getElementById('<%=ddlsite.ClientID%>').value) + ",'_year':" + JSON.stringify(document.getElementById("txtyear").value) + ",'_category':" + JSON.stringify(document.getElementById("category").value) + ",'_department':" + JSON.stringify(document.getElementById("Department").value) + ",'_subdept':" + JSON.stringify(document.getElementById("subdept").value) + ",'_report':" + JSON.stringify(torpt) + ",'_dtissue':" + JSON.stringify(document.getElementById("dtissue").value) + ",'_dtexp':" + JSON.stringify(document.getElementById("dtexp").value) + ",'_url':" + JSON.stringify(fullPath) + ",'_filename':" + JSON.stringify(fname) + "}",
                data: "{'_type':" + JSON.stringify(type) + ",'_subject':" + JSON.stringify(document.getElementById("subject").value) + ",'_comp':" + JSON.stringify(document.getElementById('<%=ddlcomp.ClientID%>').value) + ",'_site':" + JSON.stringify(document.getElementById('<%=ddlsite.ClientID%>').value) + ",'_year':" + JSON.stringify("") + ",'_category':" + JSON.stringify(document.getElementById("category").value) + ",'_department':" + JSON.stringify(document.getElementById("Department").value) + ",'_subdept':" + JSON.stringify(document.getElementById("subdept").value) + ",'_remdays':" + JSON.stringify(document.getElementById('<%=rem_days.ClientID%>').value) + ",'_pic1':" + JSON.stringify(document.getElementById('<%=txtpic1.ClientID%>').value) + ",'_pic2':" + JSON.stringify(document.getElementById('<%=txtpic2.ClientID%>').value) + ",'_pic3':" + JSON.stringify(document.getElementById('<%=txtpic3.ClientID%>').value) + ",'_pic4':" + JSON.stringify(document.getElementById('<%=txtpic4.ClientID%>').value) + ",'_pic5':" + JSON.stringify(document.getElementById('<%=txtpic5.ClientID%>').value) + ",'_dtissue':" + JSON.stringify(document.getElementById("dtissue").value) + ",'_dtexp':" + JSON.stringify(document.getElementById("dtexp").value) + ",'_url':" + JSON.stringify(fullPath) + ",'_filename':" + JSON.stringify(document.getElementById("nmdoc").value) + "}",
                dataType: "json",
                success: function(res) {
                    alert("Data Saved Successfully");
                    //window.location = "doc_center_entry.aspx";
                },
                error: function(err) {
                    alert(err.toString );
                    //window.location = "entry_permit_doc.aspx";
                }
            });
        }
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="line" style="right:10px; text-align:right;">
    <a href="signout.aspx">Logout</a>
    </div>
    <div class="title">
        <strong>Menu</strong>
    </div>
    <div id="list_newest" class="general" style="width:200px; color:#33ADFF;">
        <div id="Div1"><img alt="open document" src="../images/open_doc_small.png" /> <a href="index.aspx">&nbsp;Home</a></div>
        <%=mnentry%>
        <%=mnview%>
        <%=mnmgmt%>
    <div><img alt="open document" src="../images/open_doc_small.png" /> <a href="J Resources Document Center Manual Version 1.pdf" target="_blank">&nbsp;Help</a></div>
    </div>
    
    <div id="list_permit" class="general" style="width:1100px; height:450px; left:240px; top:20px; position:absolute; border-color:#fff;">
        <input type="hidden" id="ctrlToFind" />      
        <asp:Label ID="label1" runat="server" ></asp:Label>
        <asp:Label ID="label2" runat="server" ></asp:Label>
        <table style="height:100%">
        <tr>
            <td class="general">
                File Upload
            </td>
             <td>
                <input style="width:200px;" disabled="disabled" id="nmdoc"/> <input type="hidden" id="nsite" />
                <asp:HiddenField ID = "temnmdoc" runat = "server" />
                <img alt="add" id="Img4" src="../images/Search.gif" align="absmiddle" style="cursor: pointer" onclick="OpenPopupdel('doc')" /> 
                <input type ="hidden" id="txturl" /><asp:HiddenField ID = "temnmpath" runat = "server" />
            </td>
            <td>
            <input type="hidden" id="filename" />
            <input type="hidden" id="AttachmentTitleTextbox"/>
                <asp:FileUpload ID="FileUpload1" runat="server" style="visibility:hidden "/>
            </td>
        </tr>
        <tr>
            <td class="general">
                Type
            </td>
            <td>
                <asp:HiddenField ID="tipe" runat="server" />
                <asp:DropDownList ID="doctp" runat="server" style="width:200px;">
                <asp:ListItem Value="SK">SK</asp:ListItem>
                <asp:ListItem Value="SE">SE</asp:ListItem>
                <asp:ListItem Value="SOP">SOP</asp:ListItem>
                <asp:ListItem Value="WI">Working Instruction</asp:ListItem>
                <asp:ListItem Value="SPP">SPP</asp:ListItem>
                <asp:ListItem Value="IM">Interoffice Memorandum</asp:ListItem>
                <asp:ListItem Value="ITM">Internal Memorandum</asp:ListItem>
                <asp:ListItem Value="SKR">Surat Keluar</asp:ListItem>
                </asp:DropDownList>
               
                <select id="type" style="visibility:hidden;">
                <option value=" "> </option>
                </select>
                <asp:HiddenField ID="doctype2" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="general">
                Subject
            </td>
            <td>
                <input type="text" id="subject" style="width:200px;"/>
            </td>
        </tr>
         <tr>
            <td class="general">
                Entry Date
            </td>
            <td>
                <input type="text" id="txtyear" runat="server" disabled="disabled" style="width:200px;"/>
                <input type="hidden" id="category"/>
                  <input type="hidden" id="Department"/>
                <input type="hidden" id="subdept"/>
            </td>
        </tr>
        <tr>
            <td class="general">
                Company
            </td>
            <td>
                <asp:DropDownList ID="ddlcomp" runat="server" DataSourceID="SqlDataSource1" 
                    DataTextField="NmCompany" DataValueField="KdCompany" style="width:200px;"></asp:DropDownList> 
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:jrn_permit %>" 
                    SelectCommand="SELECT KdCompany, NmCompany FROM JRN_TEST.dbo.H_A110">
                </asp:SqlDataSource>
                <%--<input type="text" id="comp" runat="server" />--%>
            </td>
        </tr>
        <tr>
            <td class="general">
                Site
            </td>
            <td>
                <asp:DropDownList ID="ddlsite" runat="server" DataSourceID="SqlDataSource2" 
                    DataTextField="NmSite" DataValueField="KdSite" style="width:200px;"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:jrn_permit %>" 
                    SelectCommand="SELECT KdSite, NmSite FROM JRN_TEST.dbo.H_A120">
                </asp:SqlDataSource>
                <%--<input type="text" id="site" runat="server" />--%>
            </td>
        </tr>
       
            
        <%--<tr>
            <td class="general">
                Number
            </td>
            <td>
                <input type="text" id="Number" runat="server" onkeypress="return isNumberKey(event)"/>
            </td>
        </tr>--%>
        <tr>
            <td class="general">
                Reminder
            </td>
            <td>
                
                <input type="hidden" id="report" />
                <asp:DropDownList ID="rem_days" runat="server" style="width:200px;">
                <asp:ListItem Value="0">-</asp:ListItem>
                <asp:ListItem Value="7">1 Week Before Expire</asp:ListItem>
                <asp:ListItem Value="14">2 Week Before Expire</asp:ListItem>
                <asp:ListItem Value="30">1 Month Before Expire</asp:ListItem>
                <asp:ListItem Value="60">2 Month Before Expire</asp:ListItem>
                <asp:ListItem Value="90">3 Month Before Expire</asp:ListItem>
                <asp:ListItem Value="180">6 Month Before Expire</asp:ListItem>
                </asp:DropDownList>
                <input type="checkbox" id="rpt" checked="checked" style="visibility:hidden;" runat="server" />
            </td>
        </tr> 
        
        <tr>
            <td class="general">
                Date Issue
            </td>
            <td>
                <input type="text" id="dtissue" readonly="readonly" style="width:180px;"/><button id="btdtissue">..</button>
            </td>
        </tr>
        <tr>
            <td class="general">
                Date Expired
            </td>
            <td>
                <input type="text" id="dtexp" readonly="readonly" style="width:180px;"/><button id="btdtexp">..</button>
            </td>
        </tr>             
        <tr><td><input type="button" id="btnsave" onclick="save();" style="width:50px; height:20px;" value="Save"/> <input type="button" id="btncancel" onclick = "window.location = 'doc_center_entry.aspx'" style="width:50px; height:20px;" value="Cancel"/></td></tr>
        </table>
        <%--<%=dtmoss %>--%>
        <div style="left:350px; top:10px; position:absolute; width:750px; Height:400px;" >
              <asp:GridView ID="GridView1" runat="server" AllowPaging="True" PageSize="5" 
                Width="100%" Height="80%" CssClass="data-table" AutoGenerateColumns="False" 
                  DataKeyNames = "fname">
            <Columns>
            <asp:CommandField ShowSelectButton="True" SelectText="Select" ButtonType="Link" HeaderStyle-Width="50px" ItemStyle-VerticalAlign="top" >
                <HeaderStyle Width="50px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:CommandField>
                <asp:BoundField DataField="fname" HeaderText="File Name" ItemStyle-Width = "100px" HeaderStyle-Width = "100px" />
                
                <asp:BoundField DataField="path" HeaderText="Path" HeaderStyle-Width = "200px" 
                    ItemStyle-Width = "200px" ItemStyle-Wrap = "true" >
<HeaderStyle Width="200px"></HeaderStyle>

<ItemStyle Width="200px"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderText="File Open" HeaderStyle-Width="50px">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" Text='Open' NavigateUrl='<%# "http://moss.jresources.com" + eval("path") %>' Target="_blank" runat="server"></asp:HyperLink>
                </ItemTemplate>

<HeaderStyle Width="50px"></HeaderStyle>
                </asp:TemplateField> 
            </Columns>
            <SelectedRowStyle Font-Bold="True" />
            <PagerStyle HorizontalAlign="Left" />
            <HeaderStyle Font-Bold="True" Width="100px" />
            <RowStyle Font-Bold="True" Width="100px" Wrap="True" />
        </asp:GridView>
        </div>
        
      <input type="checkbox" checked="checked" id="chkpic1" disabled = "disabled" style="visibility:hidden;" runat="server" />
                <asp:DropDownList ID="txtpic1" runat="server" Enabled="false" style="visibility:hidden; width:200px;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email"></asp:DropDownList>
                <asp:SqlDataSource ID="DsPIC" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:jrn_permit %>"
                    SelectCommand="SELECT [pic_name], [pic_email] FROM [docpermit_pic] ORDER BY [pic_name]">
                </asp:SqlDataSource>
                
                <input type="checkbox" id="chkpic2" runat="server" style="visibility:hidden;" />
                <asp:DropDownList ID="txtpic2" runat="server" style="visibility:hidden; width:200px;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email">
                </asp:DropDownList>
                
                <input type="checkbox" id="chkpic3" runat="server" style="visibility:hidden;" />
                <asp:DropDownList ID="txtpic3" runat="server" style="visibility:hidden; width:200px;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email">
                </asp:DropDownList>
                
                <input type="checkbox" id="chkpic4" runat="server" style="visibility:hidden;" />
                <asp:DropDownList ID="txtpic4" runat="server" style="visibility:hidden; width:200px;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email">
                </asp:DropDownList>
                
                <input type="checkbox" id="chkpic5" runat="server" style="visibility:hidden;" />
                <asp:DropDownList ID="txtpic5" runat="server" style="visibility:hidden; width:200px;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email">
                </asp:DropDownList>
    </div> 
   
        <script type = "text/javascript" >
            var cal = Calendar.setup({
                onSelect: function(cal) { cal.hide() },
                showTime: false
            });
            cal.manageFields("btdtissue","dtissue", "%m/%d/%Y");
            cal.manageFields("btdtexp", "dtexp", "%m/%d/%Y");
        </script>
        <%=alert %>
</asp:Content>