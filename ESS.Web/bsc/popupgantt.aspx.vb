﻿Imports System.Data.SqlClient

Partial Public Class popupgantt
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim id As String = Request.QueryString("id")
            'Dim sqlquery As String = "select * from mactivity a left outer join OPENROWSET('SQLNCLI','SERVER=172.16.0.7\hris;UID=IICS;PWD=$IICS-123-ONLINE$', 'SELECT pernr,pnalt, cname from BERP_CENTRAL.dbo.H_A101_SAP') x on a.pic=x.pernr or a.pic=x.pernr where id='" & id & "'"
            Dim sqlquery As String = "select * from mactivity a left outer join OPENROWSET('SQLNCLI','SERVER=20.110.1.5;UID=dev;PWD=hrishris', 'SELECT nik,'' as pnalt, nama from JRN_TEST.dbo.H_A101') x on a.pic=x.nik or a.pic=x.nik where id='" & id & "'"

            Dim dr As SqlDataReader
            Dim result As String = ""
            Dim scom As New SqlCommand(sqlquery, conn)
            conn.Open()
            dr = scom.ExecuteReader
            If dr.HasRows Then
                'result += "<span style=""background-color:#00CC00;width:100%""><b>Detail Data</b></span>"
                result += "<table class=""bsc"" style=""width:100%"">"
                result += "<tr><td colspan=""3"" style=""background-color:#00CC00;width:100%""><b>Detail Data</b></td></tr>"
                While dr.Read
                    result += "<tr><td style=""vertical-align:top;width:10%;""><b>Activity</b></td><td style=""width:1%"">:</td><td style=""width:89%"">" & dr("activity") & "</td></tr>"
                    result += "<tr><td><b>PIC</b></td><td>:</td><td>" & dr("cname") & "</td></tr>"
                    result += "<tr><td style=""vertical-align:top""><b>Deliverable</b></td><td>:</td><td>" & dr("deliverables") & "</td></tr>"
                End While
            End If
            dr.Close()
            dr = Nothing
            sqlquery = "select top 3 convert(varchar(10),createdtime,103) as tanggal , progress as result  from dbo.TACTIVITYPROG taprog where taprog.activityid = " & id & " order by createdtime desc"
            scom = New SqlCommand(sqlquery, conn)
            dr = scom.ExecuteReader
            If dr.HasRows Then
                result += "<tr><td colspan='3'><b>Last 3 Progress</b></td></tr><tr><td colspan='3'><table border=""1"" style=""width:100%;border-style:solid;border-width:1px;border-color:black;"">"
                While dr.Read
                    result += "<tr><td style=""width:100%;border-style:solid;border-width:1px;border-color:black;"">Tanggal : " & dr("tanggal") & "</td></tr>"
                    result += "<tr><td style=""width:100%;border-style:solid;border-width:1px;border-color:black;"">" & Uri.UnescapeDataString(Replace(Replace(dr("result"), "&lt;br&gt;", "<br>"), "%0D%0A", "<br>")) & "</td></tr>"
                End While
                result += "</table></td></tr>"
            End If

            If result <> "" Then
                result += "</table>"
            End If
            myresult.InnerHtml = result
            conn.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

End Class