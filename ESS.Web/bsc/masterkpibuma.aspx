<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="masterkpibuma.aspx.vb"
   Inherits="EXCELLENT.masterkpibuma" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
   <title>Master KPI</title>
   <link rel="stylesheet" href="../css/calendar-c.css" />

   <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>

   <script src="../Scripts/json2.js" type="text/javascript"></script>

   <script type="text/javascript">
      var TABLE_NAME = 'childKPI';
      var F_TABLE_NAME = 'formula';


      function CountL(text, howlong) {
         var maxlength = new Number(howlong); // Change number to your max length.
         if (document.getElementById(text.id).value.length > maxlength) {
            text.value = text.value.substring(0, maxlength);
            alert("Hanya bisa menampung 100 karakter");
         }
      }

      function addRow(rownum, id) {
         //alert(rownum + '--' + id);
         document.getElementById(id).style.visibility = "hidden";
         addRowToFTable();
      }

      function delFRow(rownum, id, state, fnomor) {
         var answer = confirm("Anda Yakin Menghapus Data ini?");
         if (answer) {
            var tbl = document.getElementById(F_TABLE_NAME);
            if (document.getElementById(id).value == '') {
               tbl.tBodies[0].deleteRow(document.getElementById(fnomor).value - 1);
               if (tbl.tBodies[0].rows.length == 0)
                  addRowToFTable();
               reorderRowsF(tbl);
            } else {
               tbl.tBodies[0].rows[rownum].style.backgroundColor = "gray";
               document.getElementById(state).value = "DEL";
               tbl.tBodies[0].rows[rownum].cells[0].getElementsByTagName('textarea')[0].readOnly = true;
               tbl.tBodies[0].rows[rownum].cells[1].getElementsByTagName('textarea')[0].readOnly = true;
               tbl.tBodies[0].rows[rownum].cells[2].getElementsByTagName('input')[0].readOnly = true;
               tbl.tBodies[0].rows[rownum].cells[3].getElementsByTagName('input')[0].readOnly = true;
               tbl.tBodies[0].rows[rownum].cells[4].getElementsByTagName('input')[0].readOnly = true;
               tbl.tBodies[0].rows[rownum].cells[0].getElementsByTagName('textarea')[0].style.backgroundColor = "gray";
               tbl.tBodies[0].rows[rownum].cells[1].getElementsByTagName('textarea')[0].style.backgroundColor = "gray";
               tbl.tBodies[0].rows[rownum].cells[2].getElementsByTagName('input')[0].style.backgroundColor = "gray";
               tbl.tBodies[0].rows[rownum].cells[3].getElementsByTagName('input')[0].style.backgroundColor = "gray";
               tbl.tBodies[0].rows[rownum].cells[4].getElementsByTagName('input')[0].style.backgroundColor = "gray";
            }
            for (var r = 1, n = tbl.rows.length; r < n; r++) {
               if (r != tbl.rows.length - 1) {
                  tbl.rows[r].cells[0].getElementsByTagName('input')[0].style.visibility = "hidden";
               } else {
                  tbl.rows[r].cells[0].getElementsByTagName('input')[0].style.visibility = "";
               }
            }
         }
      }

      function addRowToFTable(id, name, source, responsibility, prepared, cutofdate) {
         var tbl = document.getElementById(F_TABLE_NAME);
         var nextRow = tbl.tBodies[0].rows.length;
         var num = nextRow;
         var row = tbl.tBodies[0].insertRow(num);

         var cell0 = row.insertCell(0);
         var txtInp = document.createElement('textarea');
         txtInp.setAttribute('name', 'fnama' + num + 1);
         txtInp.setAttribute('id', 'fnama' + num + 1);
         txtInp.setAttribute('style', 'width:90%');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('cols', '33');
         txtInp.setAttribute('rows', '2');
         txtInp.setAttribute('onkeyup', 'javascript:CountL(this, 100);')
         txtInp.setAttribute('onchange', 'javascript:CountL(this, 100);')
         if (name != undefined) {
            var txt = document.createTextNode(name);
            txtInp.appendChild(txt);
         }
         cell0.appendChild(txtInp);

         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'button');
         txtInp.setAttribute('name', 'fadd' + num + 1);
         txtInp.setAttribute('id', 'fadd' + num + 1);
         txtInp.setAttribute('style', 'background-image: url(../images/add.gif); background-repeat: no-repeat;cursor:hand;Width:15px;Height:15px;background-color:transparent;border:0');
         txtInp.setAttribute('onclick', 'addRow(' + parseFloat(num) + ',\'' + 'fadd' + num + 1 + '\')');
         cell0.appendChild(txtInp);

         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'button');
         txtInp.setAttribute('name', 'fdel' + num + 1);
         txtInp.setAttribute('id', 'fdel' + num + 1);
         txtInp.setAttribute('style', 'background-image: url(../images/delete.gif); background-repeat: no-repeat;cursor:hand;Width:15px;Height:15px;background-color:transparent;border:0');
         txtInp.setAttribute('onclick', 'delFRow(' + parseFloat(num) + ',\'' + 'fid' + num + 1 + '\',\'' + 'fstate' + num + 1 + '\',\'' + 'fnomor' + num + 1 + '\')');
         cell0.appendChild(txtInp);

         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'fnomor' + num + 1);
         txtInp.setAttribute('id', 'fnomor' + num + 1);
         txtInp.setAttribute('value', num + 1);
         cell0.appendChild(txtInp);

         var cell1 = row.insertCell(1);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'fid' + num + 1);
         txtInp.setAttribute('id', 'fid' + num + 1);
         if (id != undefined) {
            txtInp.setAttribute('value', id);
         }
         cell1.appendChild(txtInp);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'fstate' + num + 1);
         txtInp.setAttribute('id', 'fstate' + num + 1);
         cell1.appendChild(txtInp);
         var txtInp = document.createElement('textarea');
         txtInp.setAttribute('name', 'fsource' + num + 1);
         txtInp.setAttribute('id', 'fsource' + num + 1);
         txtInp.setAttribute('style', 'width:99%');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('rows', '2');
         txtInp.setAttribute('onkeyup', 'javascript:CountL(this, 100);')
         txtInp.setAttribute('onchange', 'javascript:CountL(this, 100);')
         if (source != undefined) {
            var txt = document.createTextNode(source);
            txtInp.appendChild(txt);
         }
         cell1.appendChild(txtInp);

         var cell2 = row.insertCell(2);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'frespon' + num + 1);
         txtInp.setAttribute('id', 'frespon' + num + 1);
         txtInp.setAttribute('style', 'width:99%');
         txtInp.setAttribute('class', 'plain');
         cell2.setAttribute('valign', 'top');
         if (responsibility != undefined) {
            txtInp.setAttribute('value', responsibility);
         }
         cell2.appendChild(txtInp);

         var cell3 = row.insertCell(3);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'fprep' + num + 1);
         txtInp.setAttribute('id', 'fprep' + num + 1);
         txtInp.setAttribute('style', 'width:99%');
         txtInp.setAttribute('class', 'plain');
         if (prepared != undefined) {
            txtInp.setAttribute('value', prepared);
         }
         cell3.setAttribute('valign', 'top');
         cell3.appendChild(txtInp);

         var cell4 = row.insertCell(4);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('name', 'fcut' + num + 1);
         txtInp.setAttribute('id', 'fcut' + num + 1);
         txtInp.setAttribute('style', 'width:100%');
         if (cutofdate != undefined) {
            txtInp.setAttribute('value', cutofdate);
         }
         cell4.setAttribute('valign', 'top');
         cell4.appendChild(txtInp);
      }

      function addRowToTable(kpiid, kpiname, funcname, checked, uom) {
         var tbl = document.getElementById(TABLE_NAME);
         var nextRow = tbl.tBodies[0].rows.length;
         var num = nextRow;
         var row = tbl.tBodies[0].insertRow(num);

         var cell0 = row.insertCell(0);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'nox' + num + 1);
         txtInp.setAttribute('id', 'nox' + num + 1);
         txtInp.setAttribute('size', '1');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('value', num + 1);
         cell0.setAttribute('style', 'border-top:0;border-left:0;border-right:0;border-color:red');
         cell0.appendChild(txtInp);

         var cell1 = row.insertCell(1);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'kcid' + num + 1);
         txtInp.setAttribute('id', 'kcid' + num + 1);
         txtInp.setAttribute('value', kpiid);
         cell1.setAttribute('style', 'border-top:0;border-left:0;border-right:0;border-color:red');
         cell1.appendChild(txtInp);

         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'kcname' + num + 1);
         txtInp.setAttribute('id', 'kcname' + num + 1);
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('style', 'width:99%');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('value', kpiname);
         cell1.appendChild(txtInp);

         var cell2 = row.insertCell(2);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'funcname' + num + 1);
         txtInp.setAttribute('id', 'funcname' + num + 1);
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('style', 'width:99%');
         txtInp.setAttribute('class', 'plain');
         txtInp.setAttribute('value', funcname);
         cell2.setAttribute('style', 'border-top:0;border-left:0;border-right:0;border-color:red');
         cell2.appendChild(txtInp);

         var cell3 = row.insertCell(3);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'checkbox');
         txtInp.setAttribute('name', 'chkcount' + num + 1);
         txtInp.setAttribute('id', 'chkcount' + num + 1);
         txtInp.setAttribute('style', 'width:99%');
         if (checked == 'True') {
            txtInp.setAttribute('checked', 'checked');
         } else {
         }
         txtInp.setAttribute('onclick', 'checkval("' + kpiid + '","' + 'chkcount' + num + 1 + '")');
         cell3.setAttribute('style', 'border-top:0;border-left:0;border-right:0;border-color:red');
         cell3.appendChild(txtInp);

         var cell4 = row.insertCell(4);
         if (document.getElementById("ctl00_ContentPlaceHolder1_grup").value == "1") {
            cell4.innerHTML = '<img title="Delete Row" style="cursor:hand" onclick="delRow(document.getElementById(\'nox' + num + 1 + '\').value,' + kpiid + ')" src="../images/delete.gif"/>&nbsp;';
         }
         cell4.innerHTML = cell4.innerHTML + '<img title="Edit Row" style="cursor:hand" onclick="editRow(document.getElementById(\'nox' + num + 1 + '\').value,' + kpiid + ')" src="../images/comment_edit.gif"/>&nbsp;';
         //          if (document.getElementById("ctl00_ContentPlaceHolder1_grup").value != "2") {
         //             var addon = '';
         //             switch (document.getElementById("ctl00_ContentPlaceHolder1_KPILevel").value) {
         //                case '0':
         //                   addon = '[Function ' + funcname + '] ' + kpiname;
         //                   break;
         //                case '1':
         //                   var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc');
         //                   var selIndex = selObj.selectedIndex;
         //                   addon = '[' + funcname + ' section ' + mapfunc(selObj[selIndex].innerText) + '] ' + document.getElementById("ctl00_ContentPlaceHolder1_KPIName").value;
         //                   break;
         //             }
         //             cell4.innerHTML = cell4.innerHTML + '<img title="Set Target" style="cursor:hand" onclick="openSetTarP(' + kpiid + ',' + uom + ',\'' + addon + '\')" src="../images/target.png"/>&nbsp;';
         //          } else {
         //
         //          }
         var addon = '';
         switch (document.getElementById("ctl00_ContentPlaceHolder1_KPILevel").value) {
            case '0':
               addon = '[Direktorat ' + funcname + '] ' + kpiname;
               break;
            case '1':
               var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc');
               var selIndex = selObj.selectedIndex;
               addon = '[' + funcname + ' section ' + mapfunc(selObj[selIndex].innerText) + '] ' + document.getElementById("ctl00_ContentPlaceHolder1_KPIName").value;
               break;
         }
         cell4.innerHTML = cell4.innerHTML + '<img title="Set Target" style="cursor:hand" onclick="openSetTarP(' + kpiid + ',' + uom + ',\'' + addon + '\',' + document.getElementById("ctl00_ContentPlaceHolder1_KPILevel").value + ')" src="../images/target.png"/>&nbsp;';
         if (document.getElementById("ctl00_ContentPlaceHolder1_grup").value != "5") {
            cell4.innerHTML = cell4.innerHTML + '<img title="Del Relation" style="cursor:hand" onclick="delRel(document.getElementById(\'nox' + num + 1 + '\').value,' + kpiid + ')" src="../images/broken-link.gif"/>';
         }
         cell4.innerHTML = '<center>' + cell4.innerHTML + '</center>'
         cell4.setAttribute('style', 'border-top:0;border-left:0;border-right:0;border-color:red');
      }

      function mapfunc(value) {
         switch (value) {
            case "OPR":
               return "PRODs";
               break;
            case "ENG":
               return "ENGs";
               break;
            case "PLT":
               return "PLTs";
               break;
            case "HRGA":
               return "PGAs";
               break;
            case "FA":
               return "FAs";
               break;
            case "MM":
               return "LOGs";
               break;
            case "SHE":
               return "SHEs";
               break;
            case "IT":
               return "ITs";
               break;
            default:
               return value + 's';
               break;
         }
      }
      function checkval(kpiidtoedit, id) {
         //alert(document.getElementById(id).checked);
         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/UpdateKPIRelation",
            data: "{'_idparent':" + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + ",'_idchild':" + kpiidtoedit + ",'_checked':" + document.getElementById(id).checked + "}",
            dataType: "json",
            success: function(res) {
               if (res.d == 'SUCCESS') {
               } else {
                  if (document.getElementById(id).checked == true) {
                     document.getElementById(id).checked = false;
                  } else {
                     document.getElementById(id).checked = true;
                  }
               }
            },
            error: function(err) {
               alert(err.responseText);
               if (document.getElementById(id).checked == true) {
                  document.getElementById(id).checked = false;
               } else {
                  document.getElementById(id).checked = true;
               }
            }
         });
      }

      function delvalidate() {
         if (document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value != "[AUTO]") {
            var answer = confirm("Anda Yakin Menghapus KPI ini?");
            if (answer) {
               return true;
            } else {
               return false;
            }
         } else {
            alert("Harap Pilih KPI terlebih dahulu");
            return false;
         }
      }

      function editRow(num, kpiid) {
         var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPILevel');
         var selIndex = selObj.selectedIndex;
         var obj = new Object();
         obj.KPIName = document.getElementById("ctl00_ContentPlaceHolder1_KPIName").value;
         obj.KPIUOM = document.getElementById("ctl00_ContentPlaceHolder1_KPIUOM").value;
         obj.KPIDesc = document.getElementById("ctl00_ContentPlaceHolder1_KPIDesc").value;
         obj.KPIFDesc = document.getElementById("ctl00_ContentPlaceHolder1_KPIFDesc").value;
         obj.KPILeads = document.getElementById("ctl00_ContentPlaceHolder1_KPILeads").value;
         obj.KPIProcess = document.getElementById("ctl00_ContentPlaceHolder1_KPIProcess").value;
         var returnVal = window.showModalDialog('popupnewkpi.aspx?type=' + selObj.options[selIndex].value + '&id=' + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + '&idtoedit=' + kpiid, obj, 'dialogWidth=620px;dialogHeight=570px;resizable=yes;help=no;unadorned=yes');
         if (returnVal != undefined) {
            document.getElementById("divprogress").style.visibility = 'visible';
            $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "WS/BSCService.asmx/RetrieveChild",
               data: "{'_idparent':" + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + ",'_type':" + selObj.options[selIndex].value + "}",
               dataType: "json",
               success: function(res) {
                  MyIssue = new Array();
                  MyIssue = res.d;
                  if (document.getElementById("ctl00_ContentPlaceHolder1_grup").value != "7")
                     emptyRow();
                  for (var i = 0; i < MyIssue.length; i++) {
                     addRowToTable(MyIssue[i].IdKPI, MyIssue[i].NamaKPI, MyIssue[i].FuncKPI, MyIssue[i].IncludeCount, MyIssue[i].Uom);
                  }
                  document.getElementById("divprogress").style.visibility = 'hidden';
               },
               error: function(err) {
                  document.getElementById("divprogress").style.visibility = 'hidden';
                  alert(err.responseText);
               }
            });
         }
      }
      function delRow(num, kpiid) {
         //alert("{'_idparent':" + JSON.stringify(document.getElementById("KPIID").value) + ",'_idchild':" + JSON.stringify(kpiid) + "}");
         var answer = confirm("Anda Yakin Menghapus KPI ini? Data Yang Dihapus tidak dapat di-Restore");
         if (answer) {
            document.getElementById("divprogress").style.visibility = 'visible';
            var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPILevel');
            var selIndex = selObj.selectedIndex;
            if (selObj.options[selIndex].value == 3) {
               $.ajax({
                  type: "POST",
                  contentType: "application/json; charset=utf-8",
                  url: "WS/BSCService.asmx/DelKPIRelation",
                  data: "{'_idparent':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value) + ",'_idchild':" + JSON.stringify(kpiid) + "}",
                  dataType: "json",
                  success: function(res) {
                     if (res.d == "SUCCESS") {
                        var tbl = document.getElementById(TABLE_NAME);
                        tbl.deleteRow(num);
                        reorderRows(tbl);
                        $.ajax({
                           type: "POST",
                           contentType: "application/json; charset=utf-8",
                           url: "WS/BSCService.asmx/RecountKPI",
                           data: "{'kpiid':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value) + "}",
                           dataType: "json",
                           success: function(res) {
                           },
                           error: function(err) {
                              alert("error: " + err.responseText);
                           }
                        });
                     } else {
                        alert(res.d);
                     }
                     document.getElementById("divprogress").style.visibility = 'hidden';
                  },
                  error: function(err) {
                     document.getElementById("divprogress").style.visibility = 'hidden';
                     alert("error: " + err.responseText);
                  }
               });
            } else {
               $.ajax({
                  type: "POST",
                  contentType: "application/json; charset=utf-8",
                  url: "WS/BSCService.asmx/DelKPI",
                  data: "{'_idparent':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value) + ",'_idchild':" + JSON.stringify(kpiid) + ",'_nikuser':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_nikuser").value) + ",'_hostname':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_compname").value) + "}",
                  dataType: "json",
                  success: function(res) {
                     if (res.d == "SUCCESS") {
                        var tbl = document.getElementById(TABLE_NAME);
                        tbl.deleteRow(num);
                        reorderRows(tbl);
                        document.getElementById("divprogress").style.visibility = 'hidden';
                        $.ajax({
                           type: "POST",
                           contentType: "application/json; charset=utf-8",
                           url: "WS/BSCService.asmx/RecountKPI",
                           data: "{'kpiid':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value) + "}",
                           dataType: "json",
                           success: function(res) {
                           },
                           error: function(err) {
                              alert("error: " + err.responseText);
                           }
                        });
                     } else if (res.d == "ACTIVE") {
                        document.getElementById("divprogress").style.visibility = 'hidden';
                        alert("Harap Non Aktifkan KPI terlebih dahulu");
                     } else {
                        document.getElementById("divprogress").style.visibility = 'hidden';
                        alert(res.d);
                     }
                  },
                  error: function(err) {
                     document.getElementById("divprogress").style.visibility = 'hidden';
                     alert("error: " + err.responseText);
                  }
               });
            }
         } else {
            return false;
         }
      }
      function delRel(num, kpiid) {
         //alert("{'_idparent':" + JSON.stringify(document.getElementById("KPIID").value) + ",'_idchild':" + JSON.stringify(kpiid) + "}");
         var answer = confirm("Anda Yakin Menghapus Relasi KPI ini?");
         if (answer) {
            var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPILevel');
            var selIndex = selObj.selectedIndex;
            document.getElementById("divprogress").style.visibility = 'visible';
            $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "WS/BSCService.asmx/DelKPIRelation",
               data: "{'_idparent':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value) + ",'_idchild':" + JSON.stringify(kpiid) + "}",
               dataType: "json",
               success: function(res) {
                  if (res.d == "SUCCESS") {
                     var tbl = document.getElementById(TABLE_NAME);
                     tbl.deleteRow(num);
                     reorderRows(tbl);
                     $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "WS/BSCService.asmx/RecountKPI",
                        data: "{'kpiid':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value) + "}",
                        dataType: "json",
                        success: function(res) {
                        },
                        error: function(err) {
                           alert("error: " + err.responseText);
                        }
                     });
                  } else {
                     alert(res.d);
                  }
                  document.getElementById("divprogress").style.visibility = 'hidden';
               },
               error: function(err) {
                  document.getElementById("divprogress").style.visibility = 'hidden';
                  alert("error: " + err.responseText);
               }
            });
         } else {
            return false;
         }
      }
      function reorderRows(tbl) {
         if (tbl.tBodies[0].rows[0]) {
            var count = 1;
            for (var i = 0; i < tbl.tBodies[0].rows.length; i++) {
               if (tbl.tBodies[0].rows[i].style.display != 'none') {
                  tbl.tBodies[0].rows[i].cells[0].getElementsByTagName('input')[0].value = count;
                  count++;
               }
            }
         }
      }

      function reorderRowsF(tbl) {
         if (tbl.tBodies[0].rows[0]) {
            var count = 1;
            for (var i = 0; i < tbl.tBodies[0].rows.length; i++) {
               if (tbl.tBodies[0].rows[i].style.display != 'none') {
                  tbl.tBodies[0].rows[i].cells[0].getElementsByTagName('input')[2].value = count;
                  count++;
               }
            }
         }
      }

      function emptyRow() {
         var tbl = document.getElementById(TABLE_NAME);
         if (tbl != null) {
            for (var i = tbl.rows.length; i > 1; i--) {
               tbl.deleteRow(i - 1);
            }
         }
      }

      function emptyRowF() {
         var tbl = document.getElementById(F_TABLE_NAME);
         if (tbl != null) {
            for (var i = tbl.rows.length; i > 1; i--) {
               tbl.deleteRow(i - 1);
            }
         }
      }

      function openSetTarP(myid, uom, addon, level) {
         var clevel;
         if (level == '1')
            clevel = '2';
         else if (level == '3')
            clevel = '2';
         var returnVal = window.showModalDialog('PopUpTarget.aspx?kpiid=' + myid + '&uom=' + escape(uom) + '&addon=' + escape(addon) + '&level=' + clevel, '', 'dialogWidth=700px;dialogHeight=250px;resizable=yes;help=no;unadorned=yes');
         return false;
      }

      function openSetTar() {
         if (document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value != "[AUTO]") {
            var addon = '';
            switch (document.getElementById("ctl00_ContentPlaceHolder1_KPILevel").value) {
               case '0':
                  addon = '[JRN] ' + document.getElementById("ctl00_ContentPlaceHolder1_KPIName").value;
                  break;
               case '1':
                  var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc');
                  var selIndex = selObj.selectedIndex;
                  addon = '[Function ' + selObj.options[selIndex].innerText + '] ' + document.getElementById("ctl00_ContentPlaceHolder1_KPIName").value;
                  break;
               case '2':
                  var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite');
                  var selIndex = selObj.selectedIndex;
                  var selObj1 = document.getElementById('ctl00_ContentPlaceHolder1_KPISection');
                  var selIndex1 = selObj1.selectedIndex;
                  addon = '[' + selObj.options[selIndex].innerText + ' section ' + selObj1.options[selIndex1].innerText + '] ' + document.getElementById("ctl00_ContentPlaceHolder1_KPIName").value;
                  break;
               case '3':
                  var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite');
                  var selIndex = selObj.selectedIndex;
                  addon = '[Jobsite ' + selObj.options[selIndex].innerText + '] ' + document.getElementById("ctl00_ContentPlaceHolder1_KPIName").value;
                  break;
            }
            var returnVal = window.showModalDialog('PopUpTarget.aspx?kpiid=' + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + '&uom=' + escape(document.getElementById("ctl00_ContentPlaceHolder1_KPIUOM").value) + '&addon=' + escape(addon) + '&level=' + escape(document.getElementById("ctl00_ContentPlaceHolder1_KPILevel").value), '', 'dialogWidth=700px;dialogHeight=250px;resizable=yes;help=no;unadorned=yes');
         } else {
            alert("Harap Simpan Data KPI Anda terlebih dahulu");
         }
      }

      function setdisabled(x) {
         var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPILevel');
         var selIndex = selObj.selectedIndex;
         if (selObj.options[selIndex].value == '1') {
            document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc').disabled = x;
         } else if (selObj.options[selIndex].value == '4') {
            document.getElementById('ctl00_ContentPlaceHolder1_KPIDirektorat').disabled = x;
         } else if (selObj.options[selIndex].value == '3') {
            document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite').disabled = x;
         }
      }

      function disablenewchild(x) {
         if (x == "1") {
            if (document.getElementById("ctl00_ContentPlaceHolder1_btnNewChild") != null) {
               document.getElementById("ctl00_ContentPlaceHolder1_btnNewChild").disabled = true;
               document.getElementById("ctl00_ContentPlaceHolder1_btnNewChild").title = "KPI dalam keadaaan OFF";
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_searchChild") != null) {
               document.getElementById("ctl00_ContentPlaceHolder1_searchChild").disabled = true;
               document.getElementById("ctl00_ContentPlaceHolder1_searchChild").title = "KPI dalam keadaaan OFF";
            }
         } else {
            if (document.getElementById("ctl00_ContentPlaceHolder1_btnNewChild") != null) {
               document.getElementById("ctl00_ContentPlaceHolder1_btnNewChild").disabled = false;
               document.getElementById("ctl00_ContentPlaceHolder1_btnNewChild").title = "";
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_searchChild") != null) {
               document.getElementById("ctl00_ContentPlaceHolder1_searchChild").disabled = false;
               document.getElementById("ctl00_ContentPlaceHolder1_searchChild").title = "";
            }
         }
      }

      function openSearchModal() {
         var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPILevel');
         var selIndex = selObj.selectedIndex;
         var yearObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIYear');
         var yearIndex = yearObj.selectedIndex;
         if (selObj.options[selIndex].value == '1') {
            var funcObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIFunc');
            var funcselIndex = funcObj.selectedIndex;
            var returnVal = window.showModalDialog('PopUpSearch.aspx?y=' + yearObj.options[yearIndex].value + '&type=' + selObj.options[selIndex].value + '&func=' + funcObj.options[funcselIndex].value, '', 'dialogWidth=350px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         } else if (selObj.options[selIndex].value == '2') {
            var secObj = document.getElementById('ctl00_ContentPlaceHolder1_KPISection');
            var secselIndex = secObj.selectedIndex;
            var jobObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite');
            var jobselIndex = jobObj.selectedIndex;
            var returnVal = window.showModalDialog('PopUpSearch.aspx?y=' + yearObj.options[yearIndex].value + '&type=' + selObj.options[selIndex].value + '&sec=' + secObj.options[secselIndex].value + '&jobsite=' + jobObj.options[jobselIndex].value, '', 'dialogWidth=350px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         } else if (selObj.options[selIndex].value == '3') {
            var jobObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIJobsite');
            var jobselIndex = jobObj.selectedIndex;
            var returnVal = window.showModalDialog('PopUpSearch.aspx?y=' + yearObj.options[yearIndex].value + '&type=' + selObj.options[selIndex].value + '&jobsite=' + jobObj.options[jobselIndex].value, '', 'dialogWidth=350px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         } else if (selObj.options[selIndex].value == '4') {
            var dirObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIDirektorat');
            var dirselIndex = dirObj.selectedIndex;
            var returnVal = window.showModalDialog('PopUpSearch.aspx?y=' + yearObj.options[yearIndex].value + '&type=' + selObj.options[selIndex].value + '&dir=' + dirObj.options[dirselIndex].value, '', 'dialogWidth=350px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         } else {
            var returnVal = window.showModalDialog('PopUpSearch.aspx?y=' + yearObj.options[yearIndex].value + '&type=' + selObj.options[selIndex].value, '', 'dialogWidth=350px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
         }

         //var kpiid = ''
         if (returnVal != undefined) {
            document.getElementById("divprogress").style.visibility = 'visible';
            $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "WS/BSCService.asmx/RetrieveKPI",
               data: "{'_id':" + returnVal + "}",
               dataType: "json",
               success: function(res) {
                  //kpiid = res.d.Id;
                  setdisabled(true);
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value = res.d.Id;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPILevel").value = res.d.Level;
                  document.getElementById("ctl00_ContentPlaceHolder1_SO").value = res.d.Soid;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIName").value = res.d.Name;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIBobot").value = res.d.Bobot;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIUOM").value = res.d.Uom;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIOwner").value = res.d.Owner;
                  //document.getElementById("KPIType").value = res.d.Type;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIDesc").value = res.d.Desc;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIPeriod").value = res.d.Periode;
                  //document.getElementById("KPILeads").value = res.d.Leads;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIFormula").value = res.d.Formula;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIFDesc").value = res.d.FormulaDesc;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIRed").value = res.d.RedParam;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIGreen").value = res.d.GreenParam;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIBased").value = res.d.BasedOn;
                  document.getElementById("ctl00_ContentPlaceHolder1_GreenParam").value = res.d.GreenCond;
                  document.getElementById("ctl00_ContentPlaceHolder1_RedParam").value = res.d.RedCond;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIType").value = res.d.KPIType;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPILeads").value = res.d.KPILeads;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIProcess").value = res.d.Process;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIReady").value = res.d.Ready;
                  disablenewchild(res.d.Ready);
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIYear").value = res.d.Year;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIYear").disabled = true;
                  document.getElementById("bcopynew").style.visibility = "";
                  document.getElementById("ctl00_ContentPlaceHolder1_KPITargetting").value = res.d.Targetting;
                  //                   if (document.getElementById("ctl00_ContentPlaceHolder1_KPICalcType").value == "0" || document.getElementById("ctl00_ContentPlaceHolder1_KPICalcType").value == "1") {
                  //                      document.getElementById("KPICluster").style.visibility = "hidden";
                  //                   } else {
                  //                      document.getElementById("KPICluster").style.visibility = "";
                  //                   }
                  if (res.d.CalcType == 0 || res.d.CalcType == 1) {
                     document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeH").value = 0;
                     onchangekpicalch();
                     document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeD").value = res.d.CalcType;
                  } else {
                     document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeH").value = 1;
                     document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeC").value = res.d.CalcType;
                  }
                  onchangekpicalch();

                  //GET CHILD
                  $.ajax({
                     type: "POST",
                     contentType: "application/json; charset=utf-8",
                     url: "WS/BSCService.asmx/RetrieveChild",
                     data: "{'_idparent':" + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + ",'_type':" + selObj.options[selIndex].value + "}",
                     dataType: "json",
                     success: function(res) {
                        MyIssue = new Array();
                        MyIssue = res.d;
                        if (document.getElementById("ctl00_ContentPlaceHolder1_grup").value != "7")
                           emptyRow();
                        if (MyIssue != null) {
                           for (var i = 0; i < MyIssue.length; i++) {
                              addRowToTable(MyIssue[i].IdKPI, MyIssue[i].NamaKPI, MyIssue[i].FuncKPI, MyIssue[i].IncludeCount, MyIssue[i].Uom);
                           }
                        }
                        if (document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value == '[AUTO]') {
                           document.getElementById("ctl00_ContentPlaceHolder1_lblState").innerText = "Kondisi : Data Baru";
                        } else {
                           document.getElementById("ctl00_ContentPlaceHolder1_lblState").innerText = "Kondisi : Edit Data";
                        }
                        document.getElementById("divprogress").style.visibility = 'hidden';
                     },
                     error: function(err) {
                        document.getElementById("divprogress").style.visibility = 'hidden';
                        alert(err.responseText);
                     }
                  });
                  //END GET CHILD
                  emptyRowF();
                  retrieveFormula();
               },
               error: function(err) {
                  document.getElementById("divprogress").style.visibility = 'hidden';
                  alert(err.responseText);
               }
            });
         }
      }

      function openModalSearchFunction() {
         if (document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value != "[AUTO]") {
            var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPILevel');
            var selIndex = selObj.selectedIndex;
            //var returnVal = window.showModalDialog('popupsectionkpi.aspx?type=1', '', 'dialogWidth=300px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
            switch (document.getElementById("ctl00_ContentPlaceHolder1_KPILevel").value) {
               case "0": //BUMA
                  var returnVal = window.showModalDialog('popupaddkpi.aspx?type=' + document.getElementById("ctl00_ContentPlaceHolder1_KPILevel").value + "&id=" + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value, '', 'dialogWidth=400px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
                  break;
               case "1": //FUNCTION
                  var returnVal = window.showModalDialog('popupaddkpi.aspx?type=' + document.getElementById("ctl00_ContentPlaceHolder1_KPILevel").value + "&id=" + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + "&func=" + document.getElementById("ctl00_ContentPlaceHolder1_KPIFunc").value, '', 'dialogWidth=400px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
                  break;
               case "3": //JOBSITE
                  var returnVal = window.showModalDialog('popupaddkpi.aspx?type=' + document.getElementById("ctl00_ContentPlaceHolder1_KPILevel").value + "&id=" + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + "&site=" + document.getElementById("ctl00_ContentPlaceHolder1_KPIJobsite").value, '', 'dialogWidth=400px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
                  break;
               case "4": //DIREKTORAT
                  var returnVal = window.showModalDialog('popupaddkpi.aspx?type=' + document.getElementById("ctl00_ContentPlaceHolder1_KPILevel").value + "&id=" + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + "&dir=" + document.getElementById("ctl00_ContentPlaceHolder1_KPIDirektorat").value, '', 'dialogWidth=400px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
                  break;
            }
            //var returnVal = window.showModalDialog('popupaddkpi.aspx?type=1', '', 'dialogWidth=300px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
            if (returnVal != undefined) {
               $.ajax({
                  type: "POST",
                  contentType: "application/json; charset=utf-8",
                  url: "WS/BSCService.asmx/RetrieveChild",
                  data: "{'_idparent':" + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + ",'_type':" + selObj.options[selIndex].value + "}",
                  dataType: "json",
                  success: function(res) {
                     MyIssue = new Array();
                     MyIssue = res.d;
                     if (document.getElementById("ctl00_ContentPlaceHolder1_grup").value != "7")
                        emptyRow();
                     for (var i = 0; i < MyIssue.length; i++) {
                        addRowToTable(MyIssue[i].IdKPI, MyIssue[i].NamaKPI, MyIssue[i].FuncKPI, MyIssue[i].IncludeCount, MyIssue[i].Uom);
                     }
                  },
                  error: function(err) {
                     alert(err.responseText);
                  }
               });
            }
         } else {
            alert("Harap Simpan Data KPI Anda terlebih dahulu");
         }
      }


      function openModal() {
         if (document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value != "[AUTO]") {
            var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPILevel');
            var selIndex = selObj.selectedIndex;
            if (selObj.options[selIndex].value == 3) {
               var returnVal = window.showModalDialog('popupsectionkpi.aspx?type=3&site=' + document.getElementById("ctl00_ContentPlaceHolder1_KPIJobsite").value, '', 'dialogWidth=300px;dialogHeight=350px;resizable=yes;help=no;unadorned=yes');
            } else if (selObj.options[selIndex].value == 2) {
               alert('KPI Level Section Tidak Dapat Mempunyai KPI Child');
               return;
            } else {
               var obj = new Object();
               obj.KPIName = document.getElementById("ctl00_ContentPlaceHolder1_KPIName").value;
               obj.KPIUOM = document.getElementById("ctl00_ContentPlaceHolder1_KPIUOM").value;
               obj.KPIDesc = document.getElementById("ctl00_ContentPlaceHolder1_KPIDesc").value;
               obj.KPIFDesc = document.getElementById("ctl00_ContentPlaceHolder1_KPIFDesc").value;
               obj.KPILeads = document.getElementById("ctl00_ContentPlaceHolder1_KPILeads").value;
               obj.KPIProcess = document.getElementById("ctl00_ContentPlaceHolder1_KPIProcess").value;
               obj.KPIReady = document.getElementById("ctl00_ContentPlaceHolder1_KPIReady").value;
               var returnVal = window.showModalDialog('popupnewkpi.aspx?type=' + selObj.options[selIndex].value + '&id=' + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value, obj, 'dialogWidth=620px;dialogHeight=570px;resizable=yes;help=no;unadorned=yes');
            }
            if (returnVal != undefined) {
               document.getElementById("divprogress").style.visibility = 'visible';
               if (selObj.options[selIndex].value == 3) {
                  $.ajax({
                     type: "POST",
                     contentType: "application/json; charset=utf-8",
                     url: "WS/BSCService.asmx/AddKPIRelation",
                     data: "{'_idparent':" + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + ",'_idchild':" + returnVal + "}",
                     dataType: "json",
                     success: function(res) {
                        $.ajax({
                           type: "POST",
                           contentType: "application/json; charset=utf-8",
                           url: "WS/BSCService.asmx/RetrieveChild",
                           data: "{'_idparent':" + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + ",'_type':3}",
                           dataType: "json",
                           success: function(res) {
                              MyIssue = new Array();
                              MyIssue = res.d;
                              if (document.getElementById("ctl00_ContentPlaceHolder1_grup").value != "7")
                                 emptyRow();
                              for (var i = 0; i < MyIssue.length; i++) {
                                 addRowToTable(MyIssue[i].IdKPI, MyIssue[i].NamaKPI, MyIssue[i].FuncKPI, MyIssue[i].IncludeCount, MyIssue[i].Uom);
                              }
                           },
                           error: function(err) {
                              alert(err.responseText);
                           }
                        });
                        document.getElementById("divprogress").style.visibility = 'hidden';
                     },
                     error: function(err) {
                        document.getElementById("divprogress").style.visibility = 'hidden';
                        alert(err.responseText);
                     }
                  });
               } else {
                  $.ajax({
                     type: "POST",
                     contentType: "application/json; charset=utf-8",
                     url: "WS/BSCService.asmx/RetrieveChild",
                     data: "{'_idparent':" + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + ",'_type':" + selObj.options[selIndex].value + "}",
                     dataType: "json",
                     success: function(res) {
                        MyIssue = new Array();
                        MyIssue = res.d;
                        if (document.getElementById("ctl00_ContentPlaceHolder1_grup").value != "7")
                           emptyRow();
                        if (MyIssue != null) {
                           for (var i = 0; i < MyIssue.length; i++) {
                              addRowToTable(MyIssue[i].IdKPI, MyIssue[i].NamaKPI, MyIssue[i].FuncKPI, MyIssue[i].IncludeCount, MyIssue[i].Uom);
                           }
                        }
                        document.getElementById("divprogress").style.visibility = 'hidden';
                     },
                     error: function(err) {
                        document.getElementById("divprogress").style.visibility = 'hidden';
                        alert(err.responseText);
                     }
                  });
               }
            }

         } else {
            alert("Harap Simpan Data KPI Anda terlebih dahulu");
         }
      }

      function fncInputNumericValuesOnly() { if (!(event.keyCode == 46 || event.keyCode == 48 || event.keyCode == 49 || event.keyCode == 50 || event.keyCode == 51 || event.keyCode == 52 || event.keyCode == 53 || event.keyCode == 54 || event.keyCode == 55 || event.keyCode == 56 || event.keyCode == 57)) { event.returnValue = false; } }
      function changeText() {
         var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPILevel');
         var selIndex = selObj.selectedIndex;
         switch (selObj.options[selIndex].value) {
            case "0":
               document.getElementById("btnCopyTarget").style.visibility = "visible";
               document.getElementById("btnCopyCalcType").style.visibility = "visible";
               break;
            case "4":
               document.getElementById("btnCopyTarget").style.visibility = "visible";
               document.getElementById("btnCopyCalcType").style.visibility = "visible";
               break;
            default:
               if (document.getElementById("btnCopyTarget") != null) {
                  document.getElementById("btnCopyTarget").style.visibility = "hidden";
                  document.getElementById("btnCopyCalcType").style.visibility = "hidden";
               }
               break;
         }
         //         if (selObj.options[selIndex].value == 3)
         //            if (document.getElementById("ctl00_ContentPlaceHolder1_searchChild") == null) {
         //            //document.getElementById("ctl00_ContentPlaceHolder1_btnNew").click();
         //            document.getElementById("trchildkpi1").style.visibility = "visible";
         //            document.getElementById("trchildkpi").style.visibility = "visible";
         //            document.getElementById("trchildkpi2").style.visibility = "visible";
         //            document.getElementById("ctl00_ContentPlaceHolder1_KPIFormula").disabled = false;
         //         } else {
         //            //document.getElementById("btnNewChild").value = "Search Child";
         //            document.getElementById("btnNewChild").style.visibility = "hidden";
         //            document.getElementById("ctl00_ContentPlaceHolder1_searchChild").style.position = "absolute";
         //            document.getElementById("ctl00_ContentPlaceHolder1_searchChild").style.left = "15";
         //         }

         if (selObj.options[selIndex].value == 3 || selObj.options[selIndex].value == 1 || selObj.options[selIndex].value == 2) {
            document.getElementById("trchildkpi1").style.visibility = "hidden";
            document.getElementById("trchildkpi").style.visibility = "hidden";
            document.getElementById("trchildkpi2").style.visibility = "hidden";
            document.getElementById("ctl00_ContentPlaceHolder1_KPIFormula").disabled = true;
         }

      }
      function retrieveChild() {
         var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPILevel');
         var selIndex = selObj.selectedIndex;
         if (document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value != "[AUTO]") {
            $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "WS/BSCService.asmx/RetrieveChild",
               data: "{'_idparent':" + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + ",'_type':" + selObj.options[selIndex].value + "}",
               dataType: "json",
               success: function(res) {
                  MyIssue = new Array();
                  MyIssue = res.d;
                  if (document.getElementById("ctl00_ContentPlaceHolder1_grup").value != "7")
                     emptyRow();
                  if (MyIssue != null) {
                     for (var i = 0; i < MyIssue.length; i++) {
                        addRowToTable(MyIssue[i].IdKPI, MyIssue[i].NamaKPI, MyIssue[i].FuncKPI, MyIssue[i].IncludeCount, MyIssue[i].Uom);
                     }
                  }
                  if (document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value == '[AUTO]') {
                     document.getElementById("ctl00_ContentPlaceHolder1_lblState").innerText = "Kondisi : Data Baru";
                  } else {
                     document.getElementById("ctl00_ContentPlaceHolder1_lblState").innerText = "Kondisi : Edit Data";
                  }
               },
               error: function(err) {
                  alert(err.responseText);
               }
            });
            //END GET CHILD 
         }
      }

      function retrieveFormula() {
         var selObj = document.getElementById('ctl00_ContentPlaceHolder1_KPILevel');
         var selIndex = selObj.selectedIndex;
         emptyRowF();
         if (document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value != "[AUTO]") {
            $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "WS/BSCService.asmx/RetrieveFormula",
               data: "{'_id':" + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + "}",
               dataType: "json",
               success: function(res) {
                  document.getElementById("divprogress").style.visibility = 'visible';
                  if (res.d != null) {
                     for (var i = 0; i < res.d.length; i++) {
                        addRowToFTable(res.d[i].Id, res.d[i].Name, res.d[i].Source, res.d[i].Responsibility, res.d[i].Prepared, res.d[i].Cutofdate);
                     }
                     var tbl = document.getElementById(F_TABLE_NAME);
                     for (var r = 1, n = tbl.rows.length; r < n; r++) {
                        if (r != tbl.rows.length - 1) {
                           tbl.rows[r].cells[0].getElementsByTagName('input')[0].style.visibility = "hidden";
                        } else {
                           tbl.rows[r].cells[0].getElementsByTagName('input')[0].style.visibility = "";
                        }
                     }
                  }
                  if (res.d == "") {
                     var tbl = document.getElementById(F_TABLE_NAME);
                     if (tbl.rows.length == 1) {
                        addRowToFTable();
                     }
                  }
                  document.getElementById("divprogress").style.visibility = 'hidden';
               },
               error: function(err) {
                  document.getElementById("divprogress").style.visibility = 'hidden';
                  alert(err.responseText);
               }
            });
            //END GET CHILD 
         }
      }

      function print() {
         if (document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value != "[AUTO]") {
            window.open('printkpi.aspx?id=' + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value, 'Print');
         } else {
            alert('Harap Simpan / Cari KPI yang ingin di print');
         }

      }

      function trim(str, chars) {
         return ltrim(rtrim(str, chars), chars);
      }

      function ltrim(str, chars) {
         chars = chars || "\\s";
         return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
      }

      function rtrim(str, chars) {
         chars = chars || "\\s";
         return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
      }

      function save() {
         var sql = "";
         if (trim(document.getElementById("ctl00_ContentPlaceHolder1_KPIName").value) == '') {
            alert("Field KPI Name belum diisi"); return false;
         } else if (trim(document.getElementById("ctl00_ContentPlaceHolder1_KPIUOM").value) == '') {
            alert("Field KPI UoM belum diisi"); return false;
         } else if (trim(document.getElementById("ctl00_ContentPlaceHolder1_KPIOwner").value) == '') {
            alert("Field KPI Owner belum diisi"); return false;
         } else if (trim(document.getElementById("ctl00_ContentPlaceHolder1_KPIDesc").value) == '') {
            alert("Field KPI Description belum diisi"); return false;
         }
         //TAMBAHAN R 09082011
         var table = document.getElementById(TABLE_NAME);
         var jumlahchecked = 0;
         for (var r = 1, n = table.rows.length; r < n; r++) {
            if (table.rows[r].cells[3].getElementsByTagName('input')[0].checked) {
               jumlahchecked = jumlahchecked + 1;
            }
         }
         if (document.getElementById("ctl00_ContentPlaceHolder1_KPIFormula").value == "1" && jumlahchecked != 1) {
            alert("Formula konsolidasi EQUAL hanya boleh mempunyai CHILD KPI 1 buah"); return false;
         } else if (document.getElementById("ctl00_ContentPlaceHolder1_KPIFormula").value == "2" && jumlahchecked == 0) {
            alert("Formula konsolidasi SUM harus mempunyai CHILD KPI"); return false;
         } else if (document.getElementById("ctl00_ContentPlaceHolder1_KPIFormula").value == "3" && jumlahchecked == 0) {
            alert("Formula konsolidasi AVG harus mempunyai CHILD KPI"); return false;
         }
         //END OF TAMBAHAN R 09082011



         table = document.getElementById(F_TABLE_NAME);
         if (table.rows.length == 2) {
            if (table.rows[1].cells[0].getElementsByTagName('textarea')[0].value == '' && table.rows[1].cells[1].getElementsByTagName('textarea')[0].value == '' && table.rows[1].cells[2].getElementsByTagName('input')[0].value == '' && table.rows[1].cells[3].getElementsByTagName('input')[0].value == '' && table.rows[1].cells[4].getElementsByTagName('input')[0].value == '') {
            } else {
               if (validasi() == true) {
                  for (var r = 1, n = table.rows.length; r < n; r++) {
                     if (table.rows[r].cells[1].getElementsByTagName('input')[1].value == "DEL") {
                        sql = sql + 'DEL*#*' + table.rows[r].cells[1].getElementsByTagName('input')[0].value + "~#~";
                     } else if (table.rows[r].cells[1].getElementsByTagName('input')[0].value == "") { //new
                        sql = sql + 'INS*#*' + table.rows[r].cells[0].getElementsByTagName('textarea')[0].value + '*#*'
                               + table.rows[r].cells[1].getElementsByTagName('textarea')[0].value + '*#*'
                               + table.rows[r].cells[2].getElementsByTagName('input')[0].value + '*#*'
                               + table.rows[r].cells[3].getElementsByTagName('input')[0].value + '*#*'
                               + table.rows[r].cells[4].getElementsByTagName('input')[0].value + "~#~";
                     } else {
                        sql = sql + 'UPD*#*' + table.rows[r].cells[0].getElementsByTagName('textarea')[0].value + '*#*'
                               + table.rows[r].cells[1].getElementsByTagName('textarea')[0].value + '*#*'
                               + table.rows[r].cells[2].getElementsByTagName('input')[0].value + '*#*'
                               + table.rows[r].cells[3].getElementsByTagName('input')[0].value + '*#*'
                               + table.rows[r].cells[4].getElementsByTagName('input')[0].value + '*#*'
                               + table.rows[r].cells[1].getElementsByTagName('input')[0].value + "~#~";
                     }
                  }
               } else {
                  return false;
               }
            }
         } else {
            if (validasi() == true) {
               for (var r = 1, n = table.rows.length; r < n; r++) {
                  if (table.rows[r].cells[1].getElementsByTagName('input')[1].value == "DEL") {
                     sql = sql + 'DEL*#*' + table.rows[r].cells[1].getElementsByTagName('input')[0].value + "~#~";
                  } else if (table.rows[r].cells[1].getElementsByTagName('input')[0].value == "") { //new
                     sql = sql + 'INS*#*' + table.rows[r].cells[0].getElementsByTagName('textarea')[0].value + '*#*'
                               + table.rows[r].cells[1].getElementsByTagName('textarea')[0].value + '*#*'
                               + table.rows[r].cells[2].getElementsByTagName('input')[0].value + '*#*'
                               + table.rows[r].cells[3].getElementsByTagName('input')[0].value + '*#*'
                               + table.rows[r].cells[4].getElementsByTagName('input')[0].value + "~#~";
                  } else {
                     sql = sql + 'UPD*#*' + table.rows[r].cells[0].getElementsByTagName('textarea')[0].value + '*#*'
                               + table.rows[r].cells[1].getElementsByTagName('textarea')[0].value + '*#*'
                               + table.rows[r].cells[2].getElementsByTagName('input')[0].value + '*#*'
                               + table.rows[r].cells[3].getElementsByTagName('input')[0].value + '*#*'
                               + table.rows[r].cells[4].getElementsByTagName('input')[0].value + '*#*'
                               + table.rows[r].cells[1].getElementsByTagName('input')[0].value + "~#~";
                  }
               }
            } else {
               return false;
            }
         }
         document.getElementById("ctl00_ContentPlaceHolder1_SQLTOSEND").value = sql;
         return true;
      }

      function validasi() {
         var table = document.getElementById(F_TABLE_NAME);
         for (var r = 1, n = table.rows.length; r < n; r++) {
            if (table.rows[r].cells[0].getElementsByTagName('textarea')[0].value == '') {
               alert("Field Name Of Data Baris " + r + " kosong!");
               return false;
            } else if (table.rows[r].cells[1].getElementsByTagName('textarea')[0].value == '') {
               alert("Field Source Data Baris " + r + " kosong!");
               return false;
            } else if (table.rows[r].cells[2].getElementsByTagName('input')[0].value == '') {
               alert("Field Data Responsibility Baris " + r + " kosong!");
               return false;
            } else if (table.rows[r].cells[3].getElementsByTagName('input')[0].value == '') {
               alert("Field Data Prepared By Baris " + r + " kosong!");
               return false;
            } else if (table.rows[r].cells[4].getElementsByTagName('input')[0].value == '') {
               alert("Field Cut Of Date By Baris " + r + " kosong!");
               return false;
            }
         }
         return true;
      }

      function onchangekpicalc() {
         //alert('tes');
         //          switch (document.getElementById("ctl00_ContentPlaceHolder1_KPICalcType").value) {
         //             case "2":
         //                document.getElementById("KPICluster").style.visibility = '';
         //                break;
         //             case "3":
         //                document.getElementById("KPICluster").style.visibility = '';
         //                break;
         //             case "4":
         //                document.getElementById("KPICluster").style.visibility = '';
         //                break;
         //             default:
         //                document.getElementById("KPICluster").style.visibility = 'hidden';
         //                break;
         //          }
      }

      function onchangekpicalch() {
         //alert(document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeH").value);
         switch (document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeH").value) {
            case "0":
               document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeD").style.visibility = "visible";
               document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeD").style.width = "100%";
               document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeC").style.visibility = "hidden";
               document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeC").style.width = "0";
               document.getElementById("KPICluster").style.visibility = '';
               document.getElementById("KPICluster").style.visibility = 'hidden';
               break;
            case "1":
               document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeD").style.visibility = "hidden";
               document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeD").style.width = "0";
               document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeC").style.visibility = "visible";
               document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeC").style.width = "100%";
               document.getElementById("KPICluster").style.visibility = '';
               break;
         }
         onchangekpicalc();
      }

      function openpopupcluster() {
         if (document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value == "[AUTO]") {
            alert("Harap Simpan KPI Anda terlebih dahulu!");
            return false;
         } else {
            switch (document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeC").value) {
               case "2":
                  var returnVal = window.open('popupdiscrete.aspx?id=' + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + '&n=' + escape(document.getElementById("ctl00_ContentPlaceHolder1_KPIName").value) + '&c=' + document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeC").value, 'tes', 'width=600px,height=600px,resizable=yes');
                  break;
               case "3":
                  var returnVal = window.open('popupstab.aspx?id=' + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + '&n=' + escape(document.getElementById("ctl00_ContentPlaceHolder1_KPIName").value) + '&c=' + document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeC").value, 'tes', 'scrollbars=yes,resizable=yes,width=1000,height=600');
                  break;
               case "4":
                  var returnVal = window.open('popupdiscrete.aspx?id=' + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + '&n=' + escape(document.getElementById("ctl00_ContentPlaceHolder1_KPIName").value) + '&c=' + document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeC").value, 'tes', 'width=600px,height=600px,resizable=yes');
                  break;
            }
         }
      }

      function copyCalcType() {
         if (document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value == "[AUTO]") {
            alert("Harap Simpan KPI Anda terlebih dahulu!");
            return false;
         } else {
            var wannadoit = confirm("Apakah anda yakin untuk mengcopy Calculation Type dari Parent KPI ke Child KPI?")
            if (wannadoit) {
               document.getElementById("divprogress").style.visibility = 'visible';
               $.ajax({
                  type: "POST",
                  contentType: "application/json; charset=utf-8",
                  url: "WS/BSCService.asmx/copycalctype",
                  data: "{'kpiparent':" + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + ",'nikuser':" + escape(document.getElementById("ctl00_ContentPlaceHolder1_nikuser").value) + ",'compname':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_compname").value) + "}",
                  dataType: "json",
                  success: function(res) {
                     if (res.d == 's')
                        alert("Copy Success");
                     else if (res.d == 'n')
                        alert("Tak ada Child KPI");
                     else
                        alert(res.d);
                     document.getElementById("divprogress").style.visibility = 'hidden';
                  },
                  error: function(err) {
                     document.getElementById("divprogress").style.visibility = 'hidden';
                     alert(err.responseText);
                  }
               });
            }
         }
      }

      function isNumber(n) { return !isNaN(parseFloat(n)) && isFinite(n); }

      function copyTarget() {
         if (document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value == "[AUTO]") {
            alert("Harap Simpan KPI Anda terlebih dahulu!");
            return false;
         } else {
            var wannadoit = confirm("Apakah anda yakin untuk mengcopy target dari Parent KPI ke Child KPI?")
            if (wannadoit) {
               //var answer = prompt("Masukkan Tahun dari target yang ingin dicopy! (Pastikan Tahun Yang Diinput sudah benar)", "2011");
               var yearObj = document.getElementById('ctl00_ContentPlaceHolder1_KPIYear');
               var yearIndex = yearObj.selectedIndex;
               var answer = yearObj.options[yearIndex].value;
               if (!isNumber(answer)) {
                  alert('Input harus berupa angka');
                  return;
               } else if (answer.length != 4) {
                  alert('Input harus 4 digit');
                  return;
               }
               document.getElementById("divprogress").style.visibility = 'visible';
               $.ajax({
                  type: "POST",
                  contentType: "application/json; charset=utf-8",
                  url: "WS/BSCService.asmx/copytarget",
                  data: "{'kpiparent':" + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + ",'tahun':" + answer + ",'nikuser':" + escape(document.getElementById("ctl00_ContentPlaceHolder1_nikuser").value) + ",'compname':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_compname").value) + "}",
                  dataType: "json",
                  success: function(res) {
                     if (res.d == 's')
                        alert("Copy Success");
                     else if (res.d == 'n')
                        alert("Tak ada Child KPI");
                     else
                        alert(res.d);
                     document.getElementById("divprogress").style.visibility = 'hidden';
                  },
                  error: function(err) {
                     document.getElementById("divprogress").style.visibility = 'hidden';
                     alert(err.responseText);
                  }
               });
            }
         }
      }

      function copynew() {
         if (document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value != '[AUTO]') {
            document.getElementById("divprogress").style.visibility = 'visible';
            document.getElementById("bcopynew").style.visibility = "hidden";
            $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               url: "WS/BSCService.asmx/RetrieveKPI",
               data: "{'_id':" + document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value + "}",
               dataType: "json",
               success: function(res) {
                  setdisabled(false);
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value = '[AUTO]';
                  document.getElementById("ctl00_ContentPlaceHolder1_KPILevel").value = res.d.Level;
                  document.getElementById("ctl00_ContentPlaceHolder1_SO").value = res.d.Soid;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIName").value = res.d.Name;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIUOM").value = res.d.Uom;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIOwner").value = res.d.Owner;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIDesc").value = res.d.Desc;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIPeriod").value = res.d.Periode;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIFormula").value = res.d.Formula;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIFDesc").value = res.d.FormulaDesc;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIRed").value = res.d.RedParam;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIGreen").value = res.d.GreenParam;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIBased").value = res.d.BasedOn;
                  document.getElementById("ctl00_ContentPlaceHolder1_GreenParam").value = res.d.GreenCond;
                  document.getElementById("ctl00_ContentPlaceHolder1_RedParam").value = res.d.RedCond;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIType").value = res.d.KPIType;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPILeads").value = res.d.KPILeads;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIProcess").value = res.d.Process;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIReady").value = res.d.Ready;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIYear").value = res.d.Year;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPIYear").disabled = false;
                  document.getElementById("ctl00_ContentPlaceHolder1_KPITargetting").value = res.d.Targetting;
                  if (res.d.CalcType == 0 || res.d.CalcType == 1) {
                     document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeH").value = 0;
                     onchangekpicalch();
                     document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeD").value = res.d.CalcType;
                  } else {
                     document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeH").value = 1;
                     document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeC").value = res.d.CalcType;
                  }
                  onchangekpicalch();
                  emptyRow();
                  emptyRowF();
                  if (document.getElementById("ctl00_ContentPlaceHolder1_KPIID").value == '[AUTO]') {
                     document.getElementById("ctl00_ContentPlaceHolder1_lblState").innerText = "Kondisi : Data Baru";
                  } else {
                     document.getElementById("ctl00_ContentPlaceHolder1_lblState").innerText = "Kondisi : Edit Data";
                  }
                  document.getElementById("divprogress").style.visibility = 'hidden';

               },
               error: function(err) {
                  document.getElementById("divprogress").style.visibility = 'hidden';
                  alert(err.responseText);
               }
            });
         } else {
            alert("Pilih KPI yang mau dicopy");
         }
      }
       
   </script>

   <style type="text/css">
      input.btn
      {
         color: #050;
         font: bold 90% 'trebuchet ms' ,helvetica,sans-serif;
         background-color: #fed;
         border: 1px solid;
         border-color: #696 #363 #363 #696;
         filter: progid:DXImageTransform.Microsoft.Gradient (GradientType=0,StartColorStr= '#ffffffff' ,EndColorStr= '#ffeeddaa' );
         padding: 0 10px 0 10px;
         cursor: hand;
      }
      input.btnonoff
      {
         color: #050;
         font: bold 90% 'trebuchet ms' ,helvetica,sans-serif;
         background-color: #fed;
         border: 1px solid;
         border-color: #696 #363 #363 #696;
         filter: progid:DXImageTransform.Microsoft.Gradient (GradientType=0,StartColorStr= '#ffffffff' ,EndColorStr= '#ffeeddaa' );
         padding: 0 8px 0 8px;
         cursor: hand;
      }
      #divprogress
      {
         z-index: 1000;
         position: absolute;
         top: 0;
         bottom: 0;
         left: 0;
         width: 100%;
         background: #000;
         opacity: 0.55;
         -moz-opacity: 0.55;
         filter: alpha(opacity=55);
         visibility: hidden;
      }
      #wrapper
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         font-size: small;
      }
      td.styleHeader
      {
         background-color: #00CC00;
         color: white;
         font-weight: bold;
         font-size: medium;
      }
      table.act
      {
         border-width: 1px;
         border-spacing: 0px;
         border-color: #C0C0C0;
         border-collapse: collapse;
         background-color: white;
      }
      table.act thead
      {
         border-width: 1px;
         padding: 1px;
         border-color: #C0C0C0;
      }
      table.act td
      {
         border-width: 1px;
         padding: 1px;
         border-color: #C0C0C0;
      }
      input.plain
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }
      .plain:hover
      {
         background-color: #FFFFA8;
      }
      select.plains
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }
      textarea
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }
      ul.dropdown *.dir1
      {
         padding-right: 20px;
         background-image: url("/remo/css/dropdown/themes/default/images/nav-arrow-down.png");
         background-position: 100% 50%;
         background-repeat: no-repeat;
         background-color: #FFA500;
      }
   </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div id="divprogress">
      <center>
         <img id="imgloading" alt="" src="/remo/images/tloader.gif" style="margin-top: 100px" /></center>
   </div>
   <div id="wrapper">
      <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="Release">
      </asp:ToolkitScriptManager>
      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
            <asp:HiddenField ID="SQLTOSEND" runat="Server" />
            <asp:HiddenField ID="grup" runat="Server" />
            <asp:HiddenField ID="nikuser" runat="Server" />
            <asp:HiddenField ID="compname" runat="Server" />
            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
            <asp:HiddenField ID="KPIID" runat="server" Value="[AUTO]" />
            <table border="1" class="act" width="100%">
               <tr>
                  <td colspan="4">
                     <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn" />
                     <asp:Button ID="btnEdit" runat="server" Text="Edit" Visible="False" CssClass="btn" />
                     <input type="button" value="Print" onclick="print()" class="btn" />
                     <input type="button" id="bcopynew" value="Copy New" style="visibility: hidden" onclick="copynew()"
                        class="btn" />
                     <div style="float: right">
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClientClick="return delvalidate();"
                           CssClass="btn" />
                     </span>
                  </td>
               </tr>
               <tr>
                  <td colspan="4">
                     <span style="font-style: italic; color: red;">* : Harus Diisi</span><br />
                     <span style="font-weight: bold; color: blue;">
                        <asp:Label ID="lblState" runat="server" EnableViewState="false" /></span>
                  </td>
               </tr>
               <tr>
                  <td colspan="4" class="styleHeader">
                     KPI Definition
                  </td>
               </tr>
               <tr>
                  <td>
                     Tahun KPI
                  </td>
                  <td colspan="3">
                     <asp:DropDownList ID="KPIYear" runat="server" CssClass="plains">
                        <asp:ListItem Value="2012" Text="2012"></asp:ListItem>
                        <asp:ListItem Value="2013" Text="2013"></asp:ListItem>
                        <asp:ListItem Value="2014" Text="2014"></asp:ListItem>
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td>
                     KPI Level
                  </td>
                  <td colspan="3">
                     <asp:DropDownList ID="KPILevel" runat="server" AutoPostBack="true" CssClass="plains"
                        onchange="changeText()">
                        <asp:ListItem Value="0" Text="JRN"></asp:ListItem>
                        <asp:ListItem Value="4" Text="Direktorat"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Function"></asp:ListItem>
                        <asp:ListItem Value="3" Text="Jobsite"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Section"></asp:ListItem>
                     </asp:DropDownList>
                     <asp:DropDownList ID="KPIDirektorat" runat="server" CssClass="plains">
                     </asp:DropDownList>
                     <asp:DropDownList ID="KPIFunc" runat="server" CssClass="plains">
                     </asp:DropDownList>
                     <asp:DropDownList ID="KPIJobsite" runat="server" CssClass="plains">
                     </asp:DropDownList>
                     <asp:DropDownList ID="KPISection" runat="server" CssClass="plains">
                     </asp:DropDownList>
                     <input type="button" id="btnSearch" value="" onclick="openSearchModal();" style="background-image: url(../images/Search.gif);
                        background-repeat: no-repeat; cursor: hand; vertical-align: top; width: 20px;
                        height: 20px; background-color: transparent; border: 0" />
                  </td>
               </tr>
               <tr>
                  <td>
                     SO
                  </td>
                  <td colspan="3">
                     <asp:DropDownList ID="SO" runat="server" CssClass="plains">
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td>
                     KPI Name
                  </td>
                  <td colspan="3" style="vertical-align: top">
                     <asp:TextBox ID="KPIName" runat="server" MaxLength="100" Columns="80" CssClass="plain"></asp:TextBox>
                     <img alt="required" src="../images/required_star.png" />
                  </td>
               </tr>
               <tr>
                  <td>
                     Bobot
                  </td>
                  <td colspan="3">
                     <asp:TextBox ID="KPIBobot" runat="server" MaxLength="10" CssClass="plain"></asp:TextBox><img
                        alt="required" src="../images/required_star.png" />
                  </td>
               </tr>
               <tr>
                  <td>
                     Ready To Measure ?
                  </td>
                  <td colspan="3">
                     <asp:DropDownList ID="KPIReady" runat="server" CssClass="plains">
                        <asp:ListItem Text="Yes" Selected="True" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Not Yet" Value="1"></asp:ListItem>
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td>
                     UoM
                  </td>
                  <td colspan="3">
                     <asp:TextBox ID="KPIUOM" runat="server" MaxLength="10" CssClass="plain"></asp:TextBox><img
                        alt="required" src="../images/required_star.png" />
                  </td>
               </tr>
               <tr>
                  <td>
                     KPI Owner
                  </td>
                  <td colspan="3">
                     <asp:TextBox ID="KPIOwner" runat="server" MaxLength="100" Columns="80" CssClass="plain"></asp:TextBox><img
                        alt="required" src="../images/required_star.png" />
                  </td>
                  <%-- <td>KPI Type</td>
                  <td>
                     <asp:DropDownList ID="KPIType" runat="server">
                        <asp:ListItem Text="Lag" Selected="True" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Lead" Value="1"></asp:ListItem>
                     </asp:DropDownList>
                  </td>--%>
               </tr>
               <tr>
                  <td>
                     KPI Description
                  </td>
                  <td colspan="3">
                     <asp:TextBox ID="KPIDesc" runat="server" TextMode="MultiLine" MaxLength="10" Rows="10"
                        Columns="120" CssClass="plain"></asp:TextBox><img alt="required" src="../images/required_star.png" />
                  </td>
               </tr>
               <tr>
                  <td>
                     Period Of Measurement
                  </td>
                  <td colspan="3">
                     <asp:DropDownList ID="KPIPeriod" runat="server" CssClass="plains">
                        <%--                        <asp:ListItem Text="Daily" Selected="True" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Weekly" Value="1"></asp:ListItem>--%>
                        <asp:ListItem Text="Monthly" Selected="True" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Quarterly" Value="3"></asp:ListItem>
                        <asp:ListItem Text="Semester" Value="4"></asp:ListItem>
                        <asp:ListItem Text="Yearly" Value="5"></asp:ListItem>
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td>
                     Type Of KPI
                  </td>
                  <td colspan="3">
                     <asp:DropDownList ID="KPIType" runat="server" CssClass="plains">
                        <asp:ListItem Text="Lag" Selected="True" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Lead" Value="1"></asp:ListItem>
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td>
                     KPI Leads to
                  </td>
                  <td colspan="3">
                     <asp:TextBox ID="KPILeads" runat="server" TextMode="MultiLine" MaxLength="10" Rows="3"
                        Columns="50" CssClass="plain"></asp:TextBox>
                  </td>
               </tr>
               <tr>
                  <td colspan="4" class="styleHeader">
                     KPI Formulation
                  </td>
               </tr>
               <tr>
                  <td style="vertical-align: top">
                     Formula
                  </td>
                  <td colspan="3">
                     <asp:TextBox ID="KPIFDesc" runat="server" TextMode="MultiLine" MaxLength="300" Rows="6"
                        Columns="50" CssClass="plain"></asp:TextBox>
                  </td>
               </tr>
               <tr>
                  <td>
                     Formula Konsolidasi dengan Child KPI
                  </td>
                  <td colspan="3">
                     <asp:DropDownList ID="KPIFormula" runat="server" CssClass="plains">
                        <asp:ListItem Text="None" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Equal" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Sum" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Average" Value="3"></asp:ListItem>
                        <%--<asp:ListItem Text="Weighted Average" Value="4"></asp:ListItem>--%>
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr id="trformula">
                  <td colspan="4" style="width: 100%">
                     <table id="formula" style="width: 100%; font-size: 11px; font-family: verdana, sans-serif;">
                        <thead>
                           <tr>
                              <th style="width: 35%; background-color: #EAE8E4">
                                 Name Of Data
                              </th>
                              <th style="width: 35%; background-color: #EAE8E4">
                                 Source Data (Form No.)
                              </th>
                              <th style="width: 10%; background-color: #EAE8E4">
                                 Data Responsibility
                              </th>
                              <th style="width: 10%; background-color: #EAE8E4">
                                 Data Prepared By
                              </th>
                              <th style="width: 10%; background-color: #EAE8E4">
                                 Cut Of Date
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                        </tbody>
                     </table>
                  </td>
               </tr>
               <tr id="trchildkpi2">
                  <td colspan="4" class="styleHeader">
                     Child KPI
                  </td>
               </tr>
               <tr id="trchildkpi1">
                  <td colspan="4">
                     <%--<asp:Button ID="btnNewChild" runat="server" Text="New Child" OnClientClick="addRowToTable('1','1');"/>--%>
                     <input type="button" id="btnNewChild" value="New Child" runat="Server" onclick="openModal();"
                        class="btn" />
                     <input type="button" id="searchChild" value="Search" runat="Server" onclick="openModalSearchFunction();"
                        class="btn" />
                     <input type="button" id="btnCopyTarget" value="Deploy Parent Target To Child" onclick="copyTarget();"
                        class="btn" />
                     <input type="button" id="btnCopyCalcType" value="Deploy Parent Calculation Type To Child"
                        onclick="copyCalcType();" class="btn" />
                  </td>
               </tr>
               <tr id="trchildkpi">
                  <td colspan="4" style="width: 100%">
                     <table id="childKPI" style="width: 100%; font-size: 11px; font-family: verdana, sans-serif;"
                        border="1">
                        <thead>
                           <tr>
                              <th style="width: 2%; border: 0; background-color: #EAE8E4">
                                 No.
                              </th>
                              <th style="width: 65%; border: 0; background-color: #EAE8E4">
                                 Nama KPI
                              </th>
                              <th style="width: 10%; border: 0; background-color: #EAE8E4">
                                 Function / Jobsite
                              </th>
                              <th style="width: 10%; border: 0; background-color: #EAE8E4">
                                 Deploy To
                              </th>
                              <th style="width: 13%; border: 0; background-color: #EAE8E4">
                                 Action
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                        </tbody>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td colspan="4" class="styleHeader">
                     KPI Target Setting
                  </td>
               </tr>
               <tr>
                  <td valign="top">
                     Process To Setting Target
                  </td>
                  <td colspan="3">
                     <asp:TextBox ID="KPIProcess" name="KPIProcess" runat="server" TextMode="MultiLine"
                        MaxLength="10" Rows="5" Columns="50" CssClass="plain"></asp:TextBox>
                  </td>
               </tr>
               <tr>
                  <td valign="top">
                     Targetting Behaviour
                  </td>
                  <td colspan="3">
                     <asp:DropDownList ID="KPITargetting" runat="server" CssClass="plains">
                        <asp:ListItem Text="Flat Target" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Moving Target" Value="1"></asp:ListItem>
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td colspan="4" class="styleHeader">
                     KPI Parameter
                  </td>
               </tr>
               <tr>
                  <td colspan="4">
                     <table>
                        <tr>
                           <td>
                              Based On
                           </td>
                           <td>
                              <asp:DropDownList ID="KPIBased" runat="server" CssClass="plains">
                                 <asp:ListItem Value="0" Text="Achievement" Selected="true"></asp:ListItem>
                                 <%--  <asp:ListItem value="1" Text="Value"></asp:ListItem>--%>
                              </asp:DropDownList>
                           </td>
                        </tr>
                        <tr>
                           <td valign="top">
                              Calculation Type
                           </td>
                           <td valign="top">
                              <asp:DropDownList ID="KPICalcTypeH" runat="server" CssClass="plains" onchange="onchangekpicalch();">
                                 <asp:ListItem Value="0" Text="CONTINOUS"></asp:ListItem>
                                 <asp:ListItem Value="1" Text="DISCRETE"></asp:ListItem>
                              </asp:DropDownList>
                              <asp:DropDownList ID="KPICalcTypeD" runat="server" CssClass="plains">
                                 <asp:ListItem Value="0" Text="MIN"></asp:ListItem>
                                 <asp:ListItem Value="1" Text="MAX" Selected="true"></asp:ListItem>
                              </asp:DropDownList>
                              <asp:DropDownList ID="KPICalcTypeC" runat="server" CssClass="plains">
                                 <asp:ListItem Value="2" Text="MIN"></asp:ListItem>
                                 <asp:ListItem Value="4" Text="MAX"></asp:ListItem>
                                 <asp:ListItem Value="3" Text="STABILIZE"></asp:ListItem>
                              </asp:DropDownList>
                              <input type="button" id="KPICluster" value="Set Data" onclick="openpopupcluster();"
                                 class="btn" />
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td colspan="4">
                     <table>
                        <tr style="background-color: red">
                           <td>
                              Red
                           </td>
                           <td>
                              <asp:DropDownList ID="RedParam" runat="server" CssClass="plains">
                                 <asp:ListItem Value="0" Text="&gt;"></asp:ListItem>
                                 <asp:ListItem Value="1" Text="&ge;"></asp:ListItem>
                                 <asp:ListItem Value="2" Text="&lt;" Selected="true"></asp:ListItem>
                                 <asp:ListItem Value="3" Text="&le;"></asp:ListItem>
                              </asp:DropDownList>
                           </td>
                           <td>
                              <asp:TextBox ID="KPIRed" runat="server" onkeypress="fncInputNumericValuesOnly();"
                                 Text="90" CssClass="plain"></asp:TextBox><img alt="required" src="../images/required_star.png" />
                           </td>
                        </tr>
                        <tr style="background-color: yellow">
                           <td>
                              Yellow
                           </td>
                           <td colspan="2" align="center">
                              BETWEEN
                           </td>
                           <tr style="background-color: green">
                              <td>
                                 Green
                              </td>
                              <td>
                                 <asp:DropDownList ID="GreenParam" runat="server" CssClass="plains">
                                    <asp:ListItem Value="0" Text="&gt;" Selected="true"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="&ge;"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="&lt;"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="&le;"></asp:ListItem>
                                 </asp:DropDownList>
                              </td>
                              <td>
                                 <asp:TextBox ID="KPIGreen" runat="server" onkeypress="fncInputNumericValuesOnly();"
                                    Text="95" CssClass="plain"></asp:TextBox><img alt="required" src="../images/required_star.png" />
                              </td>
                           </tr>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td colspan="4">
                     <input type="button" id="btnSetTarget" value="Setting Target" onclick="openSetTar();"
                        class="btn" />
                  </td>
               </tr>
               <tr>
                  <td colspan="4" align="right">
                     <asp:Button ID="btnSave" Width="55px" Height="55px" Style="background-image: url(../images/save-icon-small.png);
                        background-repeat: no-repeat; cursor: hand; vertical-align: bottom" BackColor="Transparent"
                        BorderStyle="None" runat="server" OnClientClick="return save();" />
                  </td>
               </tr>
            </table>
         </ContentTemplate>
      </asp:UpdatePanel>

      <script type="text/javascript">
         changeText(); addRowToFTable();
         onchangekpicalch();
         //           if (document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeH").value == "0") {
         //              document.getElementById("KPICluster").style.visibility = "visible";
         //           } else {
         //              document.getElementById("KPICluster").style.visibility = "hidden";
         //           }
           
      </script>

   </div>
</asp:Content>
