﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="cash_advance_req.aspx.vb" Inherits="EXCELLENT.cash_advance_req" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 893px">
    <asp:Button ID="btnsavepdf" runat="server" BorderStyle="None" Text="Save to PDF" 
                Font-Underline="True" ForeColor="#3333FF" BackColor="White" /><br />
        <asp:TextBox ID="tripnum" runat="server" Visible="false" ></asp:TextBox>
        <div id="savepdf" style="position:absolute; right:10px; top:20px; height: 22px;">
            </div>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" Height="609px" Width="96%" ShowExportControls="False" 
            ShowPrintButton="False" SizeToReportContent="True">
            <LocalReport ReportPath="Laporan\cash_adv_request.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource2" 
                        Name="DataSet5_V_H0011" />
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="DataSet5_V_H0012" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:jrn_citrixcon %>" 
            SelectCommand="SELECT TripNo, nik, kddepar, kdjabat, kdsite, kdlokasi, costcode, email, sup_nik, sup_email, detailTrip, purpose, tripfrom, tripto, tripday, workflow, workflowstep, reqby, reqdate, apprby, apprdate, reviewby, reviewdate, proceedby, proceeddate, editGA, fflight, fland, landname, landRemark, fhotel, fcomAko, comAkomodate, comAkoStart, comAkoEnd, comAkoRemark, cashadvan, dectotal, decbalance, fConsult, consul, consultype, CreatedBy, CreatedIn, CreatedTime, ModifiedBy, ModifiedIn, ModifiedTime, StEdit, DeleteBy, DeleteTime, fstatus, currcode FROM V_H001 WHERE (TripNo = @argtripno)">
            <SelectParameters>
                <asp:ControlParameter ControlID="tripnum" Name="argtripno" 
                    PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:jrn_citrixcon %>" 
            
            SelectCommand="SELECT TripNo, nik, (SELECT Nama FROM H_A101 WHERE (Nik = V_H001.nik)) AS nama, (SELECT NmDepar FROM H_A130 WHERE (KdDepar = V_H001.kddepar)) AS nmdepar, (SELECT NmJabat FROM H_A150 WHERE (KdJabat = V_H001.kdjabat)) AS nmjabat, costcode, (SELECT Nama FROM H_A101 AS H_A101_5 WHERE (Nik = V_H001.sup_nik)) AS superior, (SELECT DateFrom FROM V_H00104 WHERE (Tripno = V_H001.TripNo)) AS datefrom, (SELECT DateTo FROM V_H00104 AS V_H00104_7 WHERE (Tripno = V_H001.TripNo)) AS dateto, fcomAko, comAkoStart, comAkoEnd, purpose, tripfrom, tripto, tripday, fland, landname, (SELECT FlightName + '-' + FlightNo AS Expr1 FROM V_H00103 WHERE (GenID IN ((SELECT TOP (1) GenID FROM V_H00103 AS V_H00103_1 WHERE (Tripno = V_H001.TripNo)))) AND (Tripno = V_H001.TripNo)) AS 'Flight_Name', (SELECT HotelName FROM V_H00104 AS V_H00104_6 WHERE (GenID IN ((SELECT TOP (1) GenID FROM V_H00104 AS V_H00104_5 WHERE (Tripno = V_H001.TripNo)))) AND (Tripno = V_H001.TripNo)) AS 'hotelname', (SELECT DateFrom FROM V_H00104 AS V_H00104_4 WHERE (GenID IN ((SELECT TOP (1) GenID FROM V_H00104 AS V_H00104_3 WHERE (Tripno = V_H001.TripNo)))) AND (Tripno = V_H001.TripNo)) AS 'hdatefrom', (SELECT DateTo FROM V_H00104 AS V_H00104_2 WHERE (GenID IN ((SELECT TOP (1) GenID FROM V_H00104 AS V_H00104_1 WHERE (Tripno = V_H001.TripNo)))) AND (Tripno = V_H001.TripNo)) AS 'hdateto', reqby, reqdate, apprby, apprdate, reviewby, reviewdate, proceedby, proceeddate, (SELECT Nama FROM H_A101 AS H_A101_4 WHERE (Nik = V_H001.reqby)) AS req, (SELECT Nama FROM H_A101 AS H_A101_3 WHERE (Nik = V_H001.apprby)) AS appr, (SELECT Nama FROM H_A101 AS H_A101_2 WHERE (Nik = V_H001.reviewby)) AS review, (SELECT Nama FROM H_A101 AS H_A101_1 WHERE (Nik = V_H001.proceedby)) AS proceed, cashadvan, currcode, (SELECT H_A110.NmCompany FROM H_A110 INNER JOIN H_A120 ON H_A110.KdCompany = H_A120.KdCompany WHERE (H_A120.KdSite = V_H001.kdsite)) AS nmcompany, kdsite, (SELECT costName FROM H_A221 WHERE (costcode = V_H001.costcode)) AS costname FROM V_H001 WHERE (TripNo = @argtripno)">
            <SelectParameters>
                <asp:ControlParameter ControlID="tripnum" Name="argtripno" 
                    PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        
    </div>
    </form>
</body>
</html>
