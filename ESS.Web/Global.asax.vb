﻿Imports System.Web.SessionState
Imports System.IO.Compression
Imports System.IO
Imports System.Web.Configuration

Public Class Global_asax
    Inherits System.Web.HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
        Dim runTime As HttpRuntimeSection = DirectCast(WebConfigurationManager.GetSection("system.web/httpRuntime"), HttpRuntimeSection)
        Dim maxRequestLength As Integer = (runTime.MaxRequestLength - 100) * 1024
        Dim context As HttpContext = DirectCast(sender, HttpApplication).Context
        If (context.Request.ContentLength > maxRequestLength) Then
            Dim provider As IServiceProvider = DirectCast(context, IServiceProvider)
            Dim workerRequest As HttpWorkerRequest = DirectCast(provider.GetService(GetType(HttpWorkerRequest)), HttpWorkerRequest)
            If workerRequest.HasEntityBody() Then
                Dim requestLength As Integer = workerRequest.GetTotalEntityBodyLength()
                Dim initialBytes As Integer = 0
                If workerRequest.GetPreloadedEntityBody() IsNot Nothing Then
                    initialBytes = workerRequest.GetPreloadedEntityBody().Length
                End If
                If Not workerRequest.IsEntireEntityBodyIsPreloaded() Then
                    'Dim buffer As Byte() = New Byte(511999) {}
                    Dim buffer As Byte() = New Byte(599999) {}
                    Dim receivedBytes As Integer = initialBytes
                    While requestLength - receivedBytes >= initialBytes
                        initialBytes = workerRequest.ReadEntityBody(buffer, buffer.Length)
                        receivedBytes += initialBytes
                    End While
                    initialBytes = workerRequest.ReadEntityBody(buffer, requestLength - receivedBytes)
                End If
            End If
            context.Response.Redirect(Convert.ToString(Me.Request.Url.LocalPath) & "?action=exception")
        End If
    End Sub


    'Protected Sub Application_PreRequestHandlerExecute(ByVal sender As Object, ByVal e As EventArgs)
    '    'Compress all pages with Deflate or Gzip: per http://www.stardeveloper.com/articles/display.html?article=2007110401
    '    Dim app As HttpApplication = sender
    '    Dim acceptEncoding As String = app.Request.Headers("Accept-Encoding")
    '    Dim prevUncompressedStream As Stream = app.Response.Filter

    '    'If Not TypeOf app.Context.CurrentHandler Is System.Web.UI.Page Then Return ' OK for me if non-Page
    '    If app.Request("gzip") = "0" Then Return ' explicit request NOT to compress, for testing

    '    Dim filepath As String = Nothing, lastdot As Integer, ext As String
    '    filepath = app.Request.FilePath
    '    lastdot = filepath.LastIndexOf(".")
    '    If lastdot < 0 Then Return ' no last dot
    '    ext = filepath.Substring(lastdot).ToLower() 'includes dot
    '    If ".htm .html .css .js .aspx .xls .xml".IndexOf(ext) < 0 Then Return

    '    'If app.Request("HTTP_X_MICROSOFTAJAX") IsNot Nothing Then Return ' OK for me if Ajax

    '    If (acceptEncoding Is Nothing OrElse acceptEncoding.Length = 0) Then Return
    '    acceptEncoding = acceptEncoding.ToLower()

    '    If acceptEncoding.Contains("deflate") OrElse acceptEncoding = "*" Then ' deflate 1st choice
    '        app.Response.Filter = New DeflateStream(prevUncompressedStream, CompressionMode.Compress)
    '        app.Response.AppendHeader("Content-Encoding", "deflate")
    '        app.Response.AppendToLog(".deflate")
    '    ElseIf acceptEncoding.Contains("gzip") Then ' gzip
    '        app.Response.Filter = New GZipStream(prevUncompressedStream, CompressionMode.Compress)
    '        app.Response.AppendHeader("Content-Encoding", "gzip")
    '        app.Response.AppendToLog(".gzip") ' so you can see what happened in your log file
    '    End If
    'End Sub ' Application_PreRequestHandlerExecute

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class