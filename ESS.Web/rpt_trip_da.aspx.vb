﻿Imports Microsoft.Reporting.WebForms

Partial Public Class rpt_trip_da
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ReportViewer1.Visible = False
        ReportViewer2.Visible = False
        If Not IsPostBack Then
            Dim newlistitem As ListItem
            txtdate1.Text = Today.Date.ToString("yyyy-MM-dd")
            txtdate2.Text = Today.Date.ToString("yyyy-MM-dd")
            hkdsite.Value = Session("kdsite").ToString

            newlistitem = New ListItem("Departure and arrival", "1")
            ddlrptlist.Items.Add(newlistitem)
            newlistitem = New ListItem("Accomodation", "2")
            ddlrptlist.Items.Add(newlistitem)



        End If
    End Sub

    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnok.Click
        report_retrieve()
    End Sub

    Public Sub report_retrieve()
        If ddlrptlist.SelectedValue = 1 Then
            ReportViewer1.Visible = True
            ReportViewer1.LocalReport.DataSources.Clear()
            
            ReportViewer1.LocalReport.ReportPath = "laporan\rptbtrda.rdlc"
            Dim rds As New ReportDataSource("hr_dttripda", ObjectDataSource1)
            rds.DataSourceId = "ObjectDataSource1"
            rds.Name = "hr_dttripda"

            'Dim p1 As New ReportParameter("kdsite", hkdsite.Value)
            Dim p1 As New ReportParameter("date1", txtdate1.Text)
            Dim p2 As New ReportParameter("date2", txtdate2.Text)

            Me.ReportViewer1.LocalReport.SetParameters(New ReportParameter() {p1, p2})

            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.Refresh()
        Else
            ReportViewer2.Visible = True
            ReportViewer2.LocalReport.DataSources.Clear()
            
            ReportViewer2.LocalReport.ReportPath = "laporan\rptbtraco.rdlc"
            Dim rds As New ReportDataSource("hr_dttripaco", ObjectDataSource2)
            rds.DataSourceId = "ObjectDataSource2"
            rds.Name = "hr_dttripaco"

            'Dim p1 As New ReportParameter("kdsite", hkdsite.Value)
            Dim p1 As New ReportParameter("date1", txtdate1.Text)
            Dim p2 As New ReportParameter("date2", txtdate2.Text)

            Me.ReportViewer2.LocalReport.SetParameters(New ReportParameter() {p1, p2})

            ReportViewer2.LocalReport.DataSources.Add(rds)
            ReportViewer2.LocalReport.Refresh()
        End If

    End Sub
End Class