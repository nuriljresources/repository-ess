﻿Imports System.Data
Imports System.Data.SqlClient
Partial Public Class view_dec
    Inherits System.Web.UI.Page
    Public idw As String
    Public nik As String
    Public tripno As String
    Public transdate As String
    Public remarks As String
    Public reqby As String
    Public reqdate As String
    Public apprby As String
    Public apprdt As String
    Public reviewby As String
    Public reviewdt As String
    Public proceedby As String
    Public proceeddt As String
    Public name As String
    Public kdsite As String
    Public ccd As String
    Public kddep As String
    Public supnik As String
    Public fsts As String
    Public kdjabat As String
    Public strtbl As String
    Public jml As String
    Public btnsavedisable As String
    Public jvc As String
    Public fstattus As String
    Public genid As String
    Public detsts As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnPaymentUsage.Visible = ESS.Common.SITE_SETTINGS.PRINT_PAYMENT_APP

        'idw = Request.QueryString("ID")
        idw = Session("vdec")
        btnsavedisable = "<input type='button' id='btnsave' value='save' onclick='save()' style='width: 80px; font-family: Calibri;' disabled='disabled'/>"
        delete.Enabled = False
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()

            Dim strcon As String = "SELECT  DecNo,NIK,TripNo,TransDate,Remarks,reqby,reqdate,apprby,apprdate,reviewby,"
            strcon = strcon + "reviewdate, proceedby, proceeddate, check_sup, decTotal, currcode, CreatedBy, CreatedIn,"
            strcon = strcon + "CreatedTime, ModifiedBy, ModifiedIn, ModifiedTime, StEdit, DeleteBy, DeleteTime, fstatus, "
            strcon = strcon + "(select kddepar from V_H001 where tripno = V_H002.tripno) as depar,"
            strcon = strcon + "(select kdjabat from V_H001 where tripno = V_H002.tripno) as jabat,"
            strcon = strcon + "(select costcode from V_H001 where tripno = V_H002.tripno) as costcode,"
            strcon = strcon + "(select kdsite from V_H001 where tripno = V_H002.tripno) as site,"
            strcon = strcon + "(select nama from H_A101 where nik = V_H002.nik) as nama,"
            strcon = strcon + "(select sup_nik from V_H001 where tripno = V_H002.tripno) as sup_nik,"
            strcon = strcon + "(select nama from H_A101 where nik = (select sup_nik from V_H001 where TripNo = V_H002.tripno) ) as superior,"
            strcon = strcon + "(select NmDepar from H_A130 where kddepar = (select kddepar from V_H001 where TripNo = V_H002.tripno) ) as nmdepar,"
            strcon = strcon + "(select nmjabat from H_A150 where kdjabat = (select kdjabat from V_H001 where TripNo = V_H002.tripno) ) as nmjabat,"
            strcon = strcon + "(SELECT costName FROM H_A221 WHERE (costcode = (SELECT costcode FROM V_H001 AS V_H001_5 WHERE (TripNo = V_H002.TripNo)))) AS costname"
            strcon = strcon + " from V_H002 where decno ='" + idw + "' and stedit <> 2"

            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            If dtb.Rows().Count > 0 Then
                nik = dtb.Rows(0)!nik.ToString
                tripno = dtb.Rows(0)!tripno.ToString
                TransId.Value = tripno

                transdate = dtb.Rows(0)!transdate.ToString
                If transdate <> Nothing Or transdate <> "" Then
                    DateDocument.Value = CDate(transdate).ToString("dd MMM yyyy")
                    transdate = CDate(transdate).ToString("MM/dd/yyyy")
                End If


                remarks = dtb.Rows(0)!remarks.ToString
                reqby = dtb.Rows(0)!reqby.ToString
                reqdate = dtb.Rows(0)!reqdate.ToString
                If reqdate <> Nothing Or reqdate <> "" Then
                    reqdate = CDate(reqdate).ToString("MM/dd/yyyy")
                End If

                apprby = dtb.Rows(0)!apprby.ToString
                apprdt = dtb.Rows(0)!apprdate.ToString
                If apprdt <> Nothing Or apprdt <> "" Then
                    apprdt = CDate(apprdt).ToString("MM/dd/yyyy")
                End If

                reviewby = dtb.Rows(0)!reviewby.ToString
                reviewdt = dtb.Rows(0)!reviewdate.ToString
                If reviewdt <> Nothing Or reviewdt <> "" Then
                    reviewdt = CDate(reviewdt).ToString("MM/dd/yyyy")
                End If

                proceedby = dtb.Rows(0)!proceedby.ToString
                proceeddt = dtb.Rows(0)!proceeddate.ToString
                If proceeddt <> Nothing Or proceeddt <> "" Then
                    proceeddt = CDate(proceeddt).ToString("MM/dd/yyyy")
                End If

                name = dtb.Rows(0)!nama.ToString
                kdsite = dtb.Rows(0)!site.ToString
                ccd = dtb.Rows(0)!costcode.ToString
                kddep = dtb.Rows(0)!nmdepar.ToString
                supnik = dtb.Rows(0)!superior.ToString
                fsts = dtb.Rows(0)!fstatus.ToString
                If fsts = "0" Then
                    fsts = "Unprocess"
                Else
                    fsts = "Proses"
                End If

                If Session("kdsite") = "JKT" Or kdsite = "JKT" Then
                    btnPaymentUsage.Enabled = True
                End If

                kdjabat = dtb.Rows(0)!nmjabat.ToString

                decdate.Text = transdate.ToString
                'reqbydt.Text = reqdate.ToString
                'apprdate.Text = apprdt.ToString
                'reviewdate.Text = reviewdt.ToString
                'proceeddate.Text = proceeddt.ToString

                fstattus = dtb.Rows(0)!fstatus
            Else
                Response.Redirect("_blank.aspx")
            End If
        Catch ex As Exception

        End Try

        Try
            Dim sqlConn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

            sqlConn2.Open()
            Dim i As Integer
            Dim dtb2 As DataTable = New DataTable()
            Dim sel As String = ""
            Dim sel2 As String = ""

            Dim strcon2 As String = "select * from V_H00105 where decno = '" + idw + "' order by decdate asc"
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn2)
            sda2.Fill(dtb2)

            jml = dtb2.Rows.Count.ToString
            If jml = "0" Then jml = "1"
            Dim TotalAmount As Integer = 0

            If dtb2.Rows().Count > 0 Then
                For i = 1 To dtb2.Rows.Count
                    If dtb2.Rows(i - 1)!expensetype.ToString = "01" Then
                        sel = "<option value='01' selected='selected'>Tickets</option> <option value='02'>Hotel</option> <option value='03'>Transports</option> <option value='04'>Other</option> <option value='05'>Foods</option>"
                    ElseIf dtb2.Rows(i - 1)!expensetype.ToString = "02" Then
                        sel = "<option value='01'>Tickets</option> <option value='02' selected='selected'>Hotel</option> <option value='03'>Transports</option> <option value='04'>Other</option> <option value='05'>Foods</option>"
                    ElseIf dtb2.Rows(i - 1)!expensetype.ToString = "03" Then
                        sel = "<option value='01'>Tickets</option> <option value='02'>Hotel</option> <option value='03' selected='selected'>Transports</option> <option value='04'>Other</option> <option value='05'>Foods</option>"
                    ElseIf dtb2.Rows(i - 1)!expensetype.ToString = "04" Then
                        sel = "<option value='01'>Tickets</option> <option value='02'>Hotel</option> <option value='03'>Transports</option> <option value='04' selected='selected'>Other</option> <option value='05'>Foods</option>"
                    ElseIf dtb2.Rows(i - 1)!expensetype.ToString = "05" Then
                        sel = "<option value='01'>Tickets</option> <option value='02'>Hotel</option> <option value='03'>Transports</option> <option value='04'>Other</option> <option value='05' selected='selected'>Foods</option>"
                    End If

                    If dtb2.Rows(i - 1)!currency.ToString = "IDR" Then
                        sel2 = "<option value='IDR' selected='selected'>IDR</option> <option value='USD'>USD</option> <option value='MYR'>MYR</option><option value='SGD'>SGD</option>"
                    ElseIf dtb2.Rows(i - 1)!currency.ToString = "USD" Then
                        sel2 = "<option value='IDR'>IDR</option> <option value='USD' selected='selected'>USD</option> <option value='MYR'>MYR</option><option value='SGD'>SGD</option>"
                    ElseIf dtb2.Rows(i - 1)!currency.ToString = "MYR" Then
                        sel2 = "<option value='IDR'>IDR</option> <option value='USD'>USD</option> <option value='MYR' selected='selected'>MYR</option><option value='SGD'>SGD</option>"
                    ElseIf dtb2.Rows(i - 1)!currency.ToString = "SGD" Then
                        sel2 = "<option value='IDR'>IDR</option> <option value='USD'>USD</option> <option value='MYR'>MYR</option><option value='SGD' selected='selected'>SGD</option>"
                    End If

                    strtbl = strtbl + "<div id='input" + i.ToString + "' style='margin-bottom:2px; width='100%;' class='clonedInput'>"
                    strtbl = strtbl + " <input type='text' name='no" + i.ToString + "' id='no" + i.ToString + "' value='" + i.ToString + "'  style='width:3%;' disabled='disabled'/>"
                    strtbl = strtbl + " <input type='text' name='decdate" + i.ToString + "' id='decdate" + i.ToString + "' value='" + CDate(dtb2.Rows(i - 1)!decdate).ToString("MM/dd/yyyy") + "' style='width:15%;' disabled='disabled'/><button id='bdecdetdt" + i.ToString + "' disabled='disabled'>...</button>"
                    strtbl = strtbl + " <select name='expensetype" + i.ToString + "' id='expensetype" + i.ToString + "' style='width:10%;' disabled='disabled'>" + sel + "</select>"
                    strtbl = strtbl + " <select name='currency" + i.ToString + "' id='currency" + i.ToString + "' style='width:6%;' disabled='disabled'> " + sel2 + "</select>"
                    strtbl = strtbl + " <input type='text' name='subtotal" + i.ToString + "' id='subtotal" + i.ToString + "' value='" + dtb2.Rows(i - 1)!subtotal.ToString + "' style='width:14%;' disabled='disabled' onkeypress='return isNumberKey(event)' />"
                    strtbl = strtbl + " <input type='text' name='remarks" + i.ToString + "' id='remarks" + i.ToString + "' value='" + dtb2.Rows(i - 1)!remarks.ToString + "' style='width:46%;' disabled='disabled'/>"
                    strtbl = strtbl + " </div>"

                    strtbl = strtbl + "<div>"
                    strtbl = strtbl + "<input type='hidden' name='genid" + i.ToString + "' id='genid" + i.ToString + "' value='" + dtb2.Rows(i - 1)!genid + "' />"
                    strtbl = strtbl + "</div>"

                    jvc = jvc + "cal.manageFields('bdecdetdt" + i.ToString + "', 'decdate" + i.ToString + "', '%m/%d/%Y');"
                    TotalAmount += dtb2.Rows(i - 1)!subtotal
                Next
            Else
                If dtb2.Rows().Count = 0 Then
                    sel = "<option value='01' selected='selected'>Tickets</option> <option value='02'>Hotel</option> <option value='03'>Transports</option> <option value='04'>Other</option> <option value='05'>Foods</option>"
                    sel2 = "<option value='IDR' selected='selected'>IDR</option> <option value='USD'>USD</option> <option value='MYR'>MYR</option>"

                    strtbl = strtbl + "<div id='input1' style='margin-bottom:2px; width='100%;' class='clonedInput'>"
                    strtbl = strtbl + " <input type='text' name='no1' id='no1' value='1'  style='width:3%;' disabled='disabled'/>"
                    strtbl = strtbl + " <input type='text' name='decdate1' id='decdate1' value='' style='width:15%;' disabled='disabled'/><button id='bdecdetdt1' disabled='disabled'>...</button>"
                    strtbl = strtbl + " <select name='expensetype1' id='expensetype1' style='width:10%;' disabled='disabled'>" + sel + "</select>"
                    strtbl = strtbl + " <select name='currency1' id='currency1' style='width:6%;' disabled='disabled'> " + sel2 + "</select>"
                    strtbl = strtbl + " <input type='text' name='subtotal1' id='subtotal1' value='' style='width:14%;' disabled='disabled' onkeypress='return isNumberKey(event)' />"
                    strtbl = strtbl + " <input type='text' name='remarks1' id='remarks1' value='' style='width:46%;' disabled='disabled'/>"
                    strtbl = strtbl + " </div>"

                    strtbl = strtbl + "<div>"
                    strtbl = strtbl + "<input type='hidden' name='genid1' id='genid1' value='' />"
                    strtbl = strtbl + "</div>"

                    jvc = jvc + "cal.manageFields('bdecdetdt1', 'decdate1', '%m/%d/%Y');"
                End If
            End If

            Total.Value = TotalAmount
        Catch ex As Exception

        End Try

        'Dim int As Integer
        'For int = 1 To 100
        '    jvc = jvc + "if (newNum == " + int.ToString + ") cal.manageFields('bdecdetdt" + int.ToString + "', 'decdate" + int.ToString + "', '%m/%d/%Y'); "
        'Next
        jvc = "<script type ='text/javascript'> var cal = Calendar.setup({ onSelect: function(cal) { cal.hide() }, showTime: false });" + jvc + " </script>"
        'jvc = "<script type ='text/javascript'>" + jvc + " </script>"
    End Sub

    Protected Sub delete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles delete.Click
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        'If GridView1.Rows.Item Is Nothing Then Exit Sub

        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()
            Dim cmd As SqlCommand
            Dim strcon As String = "update V_H002 set stedit = '2' where decno = '" + idw + "'"
            cmd = New SqlCommand(strcon, sqlConn)
            cmd.ExecuteScalar()
            sqlConn.Close()
            Response.Redirect("declarationlist.aspx")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub edit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles edit.Click
        If fstattus = 0 Then
            strtbl = ""

            Try
                Dim sqlConn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

                sqlConn2.Open()
                Dim i As Integer
                Dim dtb2 As DataTable = New DataTable()
                Dim sel As String = ""
                Dim sel2 As String = ""

                Dim strcon2 As String = "select * from V_H00105 where decno = '" + idw + "' order by decdate asc"
                Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn2)
                sda2.Fill(dtb2)

                jml = dtb2.Rows.Count.ToString
                If jml = "0" Then jml = "1"

                If dtb2.Rows().Count > 0 Then
                    For i = 1 To dtb2.Rows.Count
                        If dtb2.Rows(i - 1)!expensetype.ToString = "01" Then
                            sel = "<option value='01' selected='selected'>Tickets</option> <option value='02'>Hotel</option> <option value='03'>Transports</option> <option value='04'>Other</option> <option value='05'>Foods</option>"
                        ElseIf dtb2.Rows(i - 1)!expensetype.ToString = "02" Then
                            sel = "<option value='01'>Tickets</option> <option value='02' selected='selected'>Hotel</option> <option value='03'>Transports</option> <option value='04'>Other</option> <option value='05'>Foods</option>"
                        ElseIf dtb2.Rows(i - 1)!expensetype.ToString = "03" Then
                            sel = "<option value='01'>Tickets</option> <option value='02'>Hotel</option> <option value='03' selected='selected'>Transports</option> <option value='04'>Other</option> <option value='05'>Foods</option>"
                        ElseIf dtb2.Rows(i - 1)!expensetype.ToString = "04" Then
                            sel = "<option value='01'>Tickets</option> <option value='02'>Hotel</option> <option value='03'>Transports</option> <option value='04' selected='selected'>Other</option> <option value='05'>Foods</option>"
                        ElseIf dtb2.Rows(i - 1)!expensetype.ToString = "05" Then
                            sel = "<option value='01'>Tickets</option> <option value='02'>Hotel</option> <option value='03'>Transports</option> <option value='04'>Other</option> <option value='05' selected='selected'>Foods</option>"
                        End If

                        If dtb2.Rows(i - 1)!currency.ToString = "IDR" Then
                            sel2 = "<option value='IDR' selected='selected'>IDR</option> <option value='USD'>USD</option><option value='MYR'>MYR</option><option value='SGD'>SGD</option>"
                        ElseIf dtb2.Rows(i - 1)!currency.ToString = "USD" Then
                            sel2 = "<option value='IDR'>IDR</option> <option value='USD' selected='selected'>USD</option><option value='MYR'>MYR</option><option value='SGD'>SGD</option>"
                        ElseIf dtb2.Rows(i - 1)!currency.ToString = "MYR" Then
                            sel2 = "<option value='IDR'>IDR</option> <option value='USD'>USD</option> <option value='MYR' selected='selected'>MYR</option><option value='SGD'>SGD</option>"
                        ElseIf dtb2.Rows(i - 1)!currency.ToString = "SGD" Then
                            sel2 = "<option value='IDR'>IDR</option> <option value='USD'>USD</option> <option value='MYR'>MYR</option><option value='SGD' selected='selected'>SGD</option>"
                        End If

                        strtbl = strtbl + "<div id='input" + i.ToString + "' style='margin-bottom:2px; width='100%;' class='clonedInput'>"
                        strtbl = strtbl + " <input type='text' name='no" + i.ToString + "' id='no" + i.ToString + "' value='" + i.ToString + "'  style='width:3%;' disabled='disabled'/>"
                        strtbl = strtbl + " <input type='text' name='decdate" + i.ToString + "' id='decdate" + i.ToString + "' value='" + CDate(dtb2.Rows(i - 1)!decdate).ToString("MM/dd/yyyy") + "' style='width:15%;' disabled='disabled'/><button id='bdecdetdt" + i.ToString + "'>...</button>"
                        strtbl = strtbl + " <select name='expensetype" + i.ToString + "' id='expensetype" + i.ToString + "' style='width:10%;'>" + sel + "</select>"
                        strtbl = strtbl + " <select name='currency" + i.ToString + "' id='currency" + i.ToString + "' style='width:6%;'> " + sel2 + "</select>"
                        strtbl = strtbl + " <input type='text' name='subtotal" + i.ToString + "' id='subtotal" + i.ToString + "' value='" + dtb2.Rows(i - 1)!subtotal.ToString + "' style='width:14%;' onkeypress='return isNumberKey(event)'/>"
                        strtbl = strtbl + " <input type='text' name='remarks" + i.ToString + "' id='remarks" + i.ToString + "' value='" + dtb2.Rows(i - 1)!remarks.ToString + "' onkeypress='return chkchar(event)' style='width:46%;'/>"
                        strtbl = strtbl + " </div>"

                        strtbl = strtbl + "<div>"
                        strtbl = strtbl + "<input type='hidden' name='genid" + i.ToString + "' id='genid" + i.ToString + "' value='" + dtb2.Rows(i - 1)!genid + "' />"
                        strtbl = strtbl + "</div>"
                    Next
                    detsts = "0"
                Else
                    If dtb2.Rows().Count = 0 Then
                        sel = "<option value='01' selected='selected'>Tickets</option> <option value='02'>Hotel</option> <option value='03'>Transports</option> <option value='04'>Other</option> <option value='05'>Foods</option>"
                        sel2 = "<option value='IDR' selected='selected'>IDR</option> <option value='USD'>USD</option><option value='MYR'>MYR</option>"

                        strtbl = strtbl + "<div id='input1' style='margin-bottom:2px; width='100%;' class='clonedInput'>"
                        strtbl = strtbl + " <input type='text' name='no1' id='no1' value='1'  style='width:3%;' disabled='disabled'/>"
                        strtbl = strtbl + " <input type='text' name='decdate1' id='decdate1' value='' style='width:15%;' disabled='disabled'/><button id='bdecdetdt1'>...</button>"
                        strtbl = strtbl + " <select name='expensetype1' id='expensetype1' style='width:10%;'>" + sel + "</select>"
                        strtbl = strtbl + " <select name='currency1' id='currency1' style='width:6%;'> " + sel2 + "</select>"
                        strtbl = strtbl + " <input type='text' name='subtotal1' id='subtotal1' value='' style='width:14%;' onkeypress='return isNumberKey(event)'/>"
                        strtbl = strtbl + " <input type='text' name='remarks1' id='remarks1' value='' onkeypress='return chkchar(event)' style='width:46%;'/>"
                        strtbl = strtbl + " </div>"

                        strtbl = strtbl + "<div>"
                        strtbl = strtbl + "<input type='hidden' name='genid1' id='genid1' value='' />"
                        strtbl = strtbl + "</div>"

                        detsts = "1"
                    End If
                End If
                btnsavedisable = "<input type='button' id='btnsave' value='save' onclick='save()' style='width: 80px; font-family: Calibri;'/>"
                delete.Enabled = True
            Catch ex As Exception

            End Try

            Try
                Dim conn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
                conn2.Open()
                Dim dtb3 As DataTable = New DataTable()
                Dim strcon3 As String = "Select max(genid) as genid from V_H00105"
                Dim sda3 As SqlDataAdapter = New SqlDataAdapter(strcon3, conn2)
                sda3.Fill(dtb3)

                If dtb3.Rows.Count = 1 Then
                    If genid = Nothing Then
                        genid = 0
                    Else
                        genid = dtb3.Rows(0)!genid
                    End If
                Else
                    genid = 0
                End If

                conn2.Close()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            lblinf.Text = "changes can not be done as the data is processed"
            btnsavedisable = "<input type='button' id='btnsave' value='save' onclick='save()' style='width: 80px; font-family: Calibri;' disabled='disabled'/>"
            delete.Enabled = False
        End If
    End Sub

    Protected Sub btnPaymentUsage_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPaymentUsage.Click
        Session("payment_usage") = String.Format("{0}|{1}|{2}|{3}|{4}", idw, String.Format("{0:0,0.00}", Integer.Parse(Total.Value)), "Business Declaration", DateDocument.Value, nik)

        Response.Redirect("payment_application_internal_form.aspx")
    End Sub
End Class