﻿Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports System.IO
Partial Public Class bussiness_trip_report_
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim tripno As String
        tripno = Request.QueryString("id")
        'Dim arg = ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString
        'Dim conn As New SqlConnection(arg)
        'Dim ob1 As SqlDataAdapter = New SqlDataAdapter("SELECT [TripNo], [Purpose] From V_H001", arg)
        'Dim DTabel As New DataTable()
        'ob1.Fill(DTabel)

        'ReportViewer1.Reset()

        'Me.SqlDataSource1.SelectCommand = "SELECT nama FROM H_A101 where nik = '0123456'"
        'Me.SqlDataSource2.SelectCommand = "SELECT * From V_H001 where TripNo = '201211/T/000001'"
        'Me.SqlDataSource3.SelectCommand = "SELECT * From H_A130 where kddepar = '01'"
        'Me.SqlDataSource4.SelectCommand = "SELECT * From H_A150 where kdjabat = '01.01.001.2M.00109'"
        'Me.SqlDataSource5.SelectCommand = "SELECT nama FROM H_A101 where nik = '0123456'"

        'Request.QueryString("query") = "201211/T/000001"
        'Me.SqlDataSource6.SelectCommand = "EXEC	[dbo].[NewSelectCommand] '201211/T/000001'"

        TextBox1.Text = tripno

        'TextBox1.Text = "201211/T/000001"
        'ReportViewer1.ServerReport.SetParameters[

        ReportViewer1.ShowPrintButton = True


    End Sub

    Protected Sub btnsavepdf_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsavepdf.Click
        Dim mimeType As String
        Dim Encoding As String
        Dim fileNameExtension As String
        Dim streams As String()
        Dim warnings As Microsoft.Reporting.WebForms.Warning()

        Dim pdfContent As Byte() = ReportViewer1.LocalReport.Render("PDF", Nothing, mimeType, Encoding, fileNameExtension, streams, warnings)
        'Creatr PDF file on disk
        Dim pdfPath As String = Server.MapPath("~/export/")
        pdfPath = Path.Combine(pdfPath, String.Format("btr{0}.pdf", Session("otorisasi")))
        Dim pdfFile As New System.IO.FileStream(pdfPath, System.IO.FileMode.Create)
        pdfFile.Write(pdfContent, 0, pdfContent.Length)
        pdfFile.Close()

        Response.Redirect("export\btr" + Session("otorisasi") + ".pdf")
    End Sub
End Class