﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master"
   CodeBehind="account.aspx.vb" Inherits="EXCELLENT.account" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
   Register Account - Application MEMO
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
   <style type="text/css">
      ul#navigation li a.m7
      {
         background-color: #00CC00;
         color: #000000;
      }
      .style1
      {
         width: 137px;
      }
      .style2
      {
         width: 58px;
      }
      .style5
      {
         width: 104px;
         height: 13px;
      }
      .style6
      {
         height: 24px;
      }
      .style8
      {
         width: 104px;
         height: 24px;
      }
      .style12
      {
         width: 204px;
      }
      .style13
      {
         width: 237px;
         height: 13px;
      }
      .style14
      {
         width: 237px;
         height: 24px;
      }
      .style15
      {
         width: 237px;
      }
      .style16
      {
         height: 13px;
         width: 10%;
         text-align: left;
      }
      ul.dropdown *.dir4
      {
         padding-right: 20px;
         background-image: url("/memo/css/dropdown/themes/default/images/nav-arrow-down.png");
         background-position: 100% 50%;
         background-repeat: no-repeat;
         background-color: #FFA500;
      }
   </style>

   <script type="text/javascript">
      function OpenPopup(key) {
         document.getElementById("ctrlToFind").value = key;
         window.open("SearchEmployee.aspx", "List", "scrollbars=no,resizable=no,width=500,height=400");
         return false;
     }
     
     function OpenPopupfield(key) {
         document.getElementById("ctrlToFind").value = key;
         window.open("searchfield.aspx", "List", "scrollbars=no,resizable=no,width=500,height=400");
         return false;
     }

      function getaccount(id) {
         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/UserMgmt.asmx/GetUser",
            data: "{'_nik':" + JSON.stringify(id) + "}",
            dataType: "json",
            success: function(res) {
               document.getElementById("nik").value = res.d.Nik;
               document.getElementById("nama").value = res.d.Nama;
               document.getElementById("kddepar").value = res.d.Departemen;
               document.getElementById("kdcabang").value = res.d.Jobsite;
               document.getElementById("password").value = res.d.Password;
               document.getElementById("jnsgroup").value = res.d.Grup;
               document.getElementById("jnsdivisi").value = res.d.Divisi;
               document.getElementById("jnsgrupshe").value = res.d.SheGrup;
               document.getElementById('jns1').checked = (Left(res.d.UpGrup, 1) == 1) ? true : false;
               document.getElementById('jns2').checked = (Right(Left(res.d.UpGrup, 2), 1) == 1) ? true : false;
               document.getElementById('jns3').checked = (Right(Left(res.d.UpGrup, 3), 1) == 1) ? true : false;
               document.getElementById('jns4').checked = (Right(Left(res.d.UpGrup, 4), 1) == 1) ? true : false;
               document.getElementById('jns5').checked = (Right(Left(res.d.UpGrup, 5), 1) == 1) ? true : false;
               document.getElementById('jns6').checked = (Right(res.d.UpGrup, 1) == 1) ? true : false;
               document.getElementById('add').style.display = 'none';
               document.getElementById('nik').disabled = 'disable';
               showLabel();
               var divisi = '';
               if (document.getElementById("jnsdivisi").style.display != "none") {
                  var divisiObj = document.getElementById('jnsdivisi');
                  var divisiselIndex = divisiObj.selectedIndex;
                  var divisi = divisiObj.options[divisiselIndex].value;
               }
               var selectedSection = res.d.Section
               $.ajax({
                  type: "POST",
                  contentType: "application/json; charset=utf-8",
                  url: "WS/UserMgmt.asmx/getSection",
                  data: "{'_funcid':" + JSON.stringify(divisi) + "}",
                  dataType: "json",
                  success: function(res) {
                     if (res.d != null) {
                        MyFunction = new Array();
                        MyFunction = res.d;
                        removeOption(document.getElementById("jnssection"));
                        for (var i = 0; i < MyFunction.length; i++) {
                           insertOption(document.getElementById("jnssection"), MyFunction[i].Nama, MyFunction[i].Id);
                        }
                        document.getElementById("jnssection").value = selectedSection;
                     } else {
                        removeOption(document.getElementById("jnssection"));
                     }
                  },
                  error: function(err) {
                     document.getElementById("log").innerHTML = "error: " + err.responseText;
                  }
               });
            },
            error: function(err) {
               document.getElementById("log").innerHTML = "error: " + err.responseText;
            }
         });
      }

      function save() {
         flag = 'new';
         if (document.getElementById('add').style.display == 'none')
            flag = 'edit';
         var s = (document.getElementById('jns1').checked == true) ? '1' : '0';
         s = s + ((document.getElementById('jns2').checked == true) ? '1' : '0');
         s = s + ((document.getElementById('jns3').checked == true) ? '1' : '0');
         s = s + ((document.getElementById('jns4').checked == true) ? '1' : '0');
         s = s + ((document.getElementById('jns5').checked == true) ? '1' : '0');
         s = s + ((document.getElementById('jns6').checked == true) ? '1' : '0');

         var divisi = "";
         var divisiid = "";
         var sectionid = "";
         if (document.getElementById("jnsgroup").value == 4 || document.getElementById("jnsgroup").value == 5 || document.getElementById("jnsgroup").value == 7 || document.getElementById("jnsgroup").value == 8 || document.getElementById("jnsgroup").value == 'A') {
            var divisiObj = document.getElementById('jnsdivisi');
            var divisiselIndex = divisiObj.selectedIndex;
            //divisi = divisiObj.options[divisiselIndex].innerText;
            divisi = "";
            if (document.getElementById("jnsdivisi").style.display == "none") {
               divisiid = 0
            } else {
               divisiid = document.getElementById("jnsdivisi").value;
            }
            if (document.getElementById("jnssection").style.display == "none") {
               sectionid = 0
            } else {
               sectionid = document.getElementById("jnssection").value;
            }
         }

         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/UserMgmt.asmx/AddUser",
            data: "{'_nik':" + JSON.stringify(document.getElementById("nik").value) + ",'_nama':" + JSON.stringify(document.getElementById("nama").value) + ",'_kddepar':" + JSON.stringify(document.getElementById("kddepar").value) + ",'_kdcabang':" + JSON.stringify(document.getElementById("kdcabang").value) + ",'_password':" + JSON.stringify(document.getElementById("password").value) + ",'_grup':" + JSON.stringify(document.getElementsByTagName('select')['jnsgroup'].value) + ",'_akses':" + JSON.stringify(s) + ",'_flag':" + JSON.stringify(flag) + ",'_divisi':'" + divisi + "','_divisiid':'" + divisiid + "','_sectionid':'" + sectionid + "','_grupshe':" + JSON.stringify(document.getElementsByTagName('select')['jnsgrupshe'].value) + "}",
            dataType: "json",
            success: function(res) {
               document.getElementById("log").innerHTML = res.d;
               window.location = "account.aspx";
            },
            error: function(err) {
               document.getElementById("log").innerHTML = "error: " + err.responseText;
            }
         });
      }

      function Left(str, n) {
         if (n <= 0)
            return "";
         else if (n > String(str).length)
            return str;
         else
            return String(str).substring(0, n);
      }

      function Right(str, n) {
         if (n <= 0)
            return "";
         else if (n > String(str).length)
            return str;
         else {
            var iLen = String(str).length;
            return String(str).substring(iLen, iLen - n);
         }
      }

      function showLabel() {
         if (document.getElementById("jnsgroup").value == 7 || document.getElementById("jnsgroup").value == 'A') { //S.Head dan S.Head Admin
            document.getElementById("jnsdivisi").style.display = "none";
            document.getElementById("jnssection").style.display = "inline";
         } else if (document.getElementById("jnsgroup").value == 4 || document.getElementById("jnsgroup").value == 5 || document.getElementById("jnsgroup").value == 8) {
            document.getElementById("jnsdivisi").style.display = "inline";
            document.getElementById("jnssection").style.display = "none";
         }
         else {
            document.getElementById("jnsdivisi").style.display = "none";
            document.getElementById("jnssection").style.display = "none";
         }
      }

      function removeOption(theSel) {
         var selIndex = theSel.selectedIndex;
         if (selIndex != -1) {
            for (i = theSel.length - 1; i >= 0; i--) {
               theSel.options[i] = null;
            }
            if (theSel.length > 0) {
               theSel.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
            }
         }
      }

      function insertOption(theSel, newText, newValue) {
         if (theSel.length == 0) {
            var newOpt1 = new Option(newText, newValue);
            theSel.options[0] = newOpt1;
            theSel.selectedIndex = 0;
         } else if (theSel.selectedIndex != -1) {
            var selText = new Array();
            var selValues = new Array();
            var selIsSel = new Array();
            var newCount = -1;
            var newSelected = -1;
            var i;
            for (i = 0; i < theSel.length; i++) {
               newCount++;
               if (newCount == theSel.selectedIndex) {
                  selText[newCount] = newText;
                  selValues[newCount] = newValue;
                  selIsSel[newCount] = false;
                  newCount++;
                  newSelected = newCount;
               }
               selText[newCount] = theSel.options[i].text;
               selValues[newCount] = theSel.options[i].value;
               selIsSel[newCount] = theSel.options[i].selected;
            }
            for (i = 0; i <= newCount; i++) {
               var newOpt = new Option(selText[i], selValues[i]);
               theSel.options[i] = newOpt;
               theSel.options[i].selected = selIsSel[i];
            }
         }
      }



   </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <input type="hidden" id="ctrlToFind" />
   <table cellspacing="2">
      <tr>
         <td style="border-style: solid; border-width: 1px; background-color: #FFFF66">
             <a href="account.aspx">Add User</a>
         </td>
         <td style="border-style: solid; border-width: 1px; background-color: #FFFF66">
            <a href="saccount.aspx">List User</a>
         </td>
      </tr>
   </table>
   <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
      <col />
      <col width="100px" />
      <col width="100px" />
      <col />
      <col />
      <col width="80" />
      <col width="*" />
      <caption style="text-align: center; font-size: 1.5em">
         Create User</caption>
      <tr>
         <td width="12%" class="style16">
            NIK
         </td>
         <td class="style13">
            <input type="text" id="nik" /><img alt="add" id="add" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopup('user')" />
         </td>
         <td class="style16">
         </td>
         <td width="10%" class="style16">
                         Name
         </td>
         <td class="style16">
            <input type="text" id="nama" disabled="disabled" />
         </td>
         <td class="style5" colspan="2">
      </tr>
      <tr>
         <td class="style6">
            Department          </td>
         <td class="style14">
            <input type="text" id="kddepar" disabled="disabled"/><img alt="add" id="Img1" src="images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopupfield('bidang')" />
         </td>
         <td class="style6">
         </td>
         <td class="style6">
            Jobsite
         </td>
         <td class="style6">
            <input type="text" id="kdcabang" disabled="disabled" />
         </td>
         <td class="style8" colspan="2">
         </td>
      </tr>
      <tr>
         <td style="vertical-align: text-top">
            Password
         </td>
         <td style="vertical-align: text-top" class="style15">
            <input type="password" id="password" />
         </td>
         <td>
            &nbsp;
         </td>
         <td style="vertical-align: text-top">
            Group
         </td>
         <td style="vertical-align: text-top">
            <select name="jnsgroup" id="jnsgroup" onchange="showLabel()">
               <option value="1">MSD</option>
               <option value="B">MSD Func. Head</option>
               <option value="2">MSD Officer</option>
               <option value="3">BOD</option>
               <option value="4">Func. Head</option>
               <option value="5">Dept. Head</option>
               <option value="6">PM/DPM</option>
               <option value="7">S. Head</option>
               <option value="8">PaDCA</option>
               <option value="A">SH Admin</option>
               <option value="9">Others</option>
            </select>
         </td>
         <td colspan="4">
            <select name="jnsdivisi" id="jnsdivisi" onchange="getSection()">
               <option value="OPR">OPR</option>
               <option value="ENG">ENG</option>
               <option value="FA">FA</option>
               <option value="HRGA">HRGA</option>
               <option value="MM">MM</option>
               <option value="PLT">PLT</option>
               <option value="IAS">IAS</option>
               <option value="SHE">SHE</option>
               <option value="IT">IT</option>
               <option value="MSD">MSD</option>
               <option value="CI">CI</option>
            </select>
            <select name="jnssection" id="jnssection">
            </select>
         </td>
      </tr>
      <tr>
         <td style="vertical-align: text-top">
            Access Meeting
         </td>
         <td class="style15" colspan="2">
            <table>
               <tr>
                  <td class="style12">
                     <input type="checkbox" id="jns1" value="1" />Weekly Review Jobsite
                  </td>
               </tr>
               <tr>
                  <td class="style12">
                     <input type="checkbox" id="jns2" value="1" />Monthly Review Jobsite
                  </td>
               </tr>
               <tr>
                  <td class="style12">
                     <input type="checkbox" id="jns3" value="1" />Weekly Internal Functional
                  </td>
               </tr>
               <tr>
                  <td class="style12">
                     <input type="checkbox" id="jns4" value="1" />Weekly Management Meeting
                  </td>
               </tr>
               <tr>
                  <td class="style12">
                     <input type="checkbox" id="jns5" value="1" />Monthly Management Meeting
                  </td>
               </tr>
               <tr>
                  <td class="style12">
                     <input type="checkbox" id="jns6" value="1" />Others
                  </td>
               </tr>
            </table>
         </td>
         <td style="vertical-align: top">
            Group SHE
         </td>
         <td style="vertical-align: top">
            <select name="jnsgrupshe" id="jnsgrupshe">
               <option value="0"></option>
               <option value="1">SHE Admin</option>
               <option value="2">SHE HO</option>
               <option value="3">SHE S. Head</option>
            </select>
         </td>
      </tr>
      <tr>
         <td colspan="5">
            <input type="button" value="Save" onclick="save()" style="width: 80px; font-family: Calibri;" />
         </td>
         <td colspan="2" style="text-align: right; font-size: 8px""" id="cellReveal">
         </td>
      </tr>
      <tr>
         <td colspan="7">
            <div id="log" style="color: red; font: Trebechuet;">
            </div>
         </td>
      </tr>
   </table>

   <script language="javascript" type="text/javascript">
      var doc = document;
      doc.getElementById("jnsdivisi").style.display = "none";
      doc.getElementById("jnssection").style.display = "none";

      function getReveal(arg) {
         if (arg != '') {
            doc.getElementById("cellReveal").innerHTML = arg.value;
         }
         else {
            doc.getElementById("cellReveal").innerHTML = '';
         }
      }

      function getSection() {
         var divisiObj = document.getElementById('jnsdivisi');
         var divisiselIndex = divisiObj.selectedIndex;
         var divisi = divisiObj.options[divisiselIndex].value;
         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/UserMgmt.asmx/getSection",
            data: "{'_funcid':" + JSON.stringify(divisi) + "}",
            dataType: "json",
            success: function(res) {
               if (res.d != null) {
                  MyFunction = new Array();
                  MyFunction = res.d;
                  removeOption(document.getElementById("jnssection"));
                  for (var i = 0; i < MyFunction.length; i++) {
                     insertOption(document.getElementById("jnssection"), MyFunction[i].Nama, MyFunction[i].Id);
                  }
               } else {
                  removeOption(document.getElementById("jnssection"));
               }
            },
            error: function(err) {
               document.getElementById("log").innerHTML = "error: " + err.responseText;
            }
         });
      }

      $.ajax({
         type: "POST",
         contentType: "application/json; charset=utf-8",
         url: "WS/UserMgmt.asmx/getFunction",
         data: "{}",
         dataType: "json",
         success: function(res) {
            MyFunction = new Array();
            MyFunction = res.d;
            removeOption(document.getElementById("jnsdivisi"));
            for (var i = 0; i < MyFunction.length; i++) {
               insertOption(document.getElementById("jnsdivisi"), MyFunction[i].Nama, MyFunction[i].Id);
            }
            getSection();
         },
         error: function(err) {
            document.getElementById("log").innerHTML = "error: " + err.responseText;
         }
      });

      
   </script>

</asp:Content>
