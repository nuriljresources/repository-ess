﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="vdetails.aspx.vb" Inherits="EXCELLENT.vdetails" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Details</title>
    <meta http-equiv="Expires" content="0"/>
    <meta http-equiv="Cache-Control" content="no-cache"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/json2.js" type="text/javascript"></script>
    <style type="text/css">
            body{
            font-family: "trebuchet MS", verdana, sans-serif;
            font-size: small;
        }
      #custom{
         font-size:10px;
      }
      .headerd{
         font-size:20px;
         background-color:#00CC00;
      }
     
      table.pica {
	      border-width: 1px;
	      border-spacing: 0px;
	      border-style: groove;
	      border-color: gray;
	      border-collapse: collapse;
	      background-color: white;
      }
      table.pica th {
	      border-width: 1px;
	      padding: 1px;
	      border-style: inset;
	      border-color: gray;
	      background-color: white;
      }
      table.pica td.picatd {
	      border-width: 1px;
	      padding: 1px;
	      border-style: inset;
	      border-color: gray;
	      background-color: white;
      }
      
      table.header {
	      border-width: 1px;
	      border-spacing: 0px;
	      border-style: groove;
	      border-color: gray;
	      border-collapse: collapse;
	      background-color: white;
      }
      table.header th {
	      border-width: 1px;
	      padding: 1px;
	      border-style: inset;
	      border-color: gray;
	      background-color: #F2F2F2;
      }
      table.header td {
	      border-width: 1px;
	      padding: 1px;
	      border-style: inset;
	      border-color: gray;
	      background-color: #F2F2F2;
      }
      textarea{
         border: 1px solid #C0C0C0;
      }      
      .plain:hover
      {
         background-color:#FFFFA8;         
      }
      .plain
      {
         border: 1px solid #C0C0C0;
      }  
    </style>
    <script type="text/javascript">
       function editcomment(id) {
          document.getElementById("editpicaid").value = id;
          document.getElementById("PI").innerText = document.getElementById("pi" + id).innerText;
          document.getElementById("CA").innerText = document.getElementById("ca" + id).innerText;
          document.getElementById("statupica").innerText = "EDIT";
          document.getElementById("btnBatal").style.visibility = "";          
       }
       function BatalKomen() {
          document.getElementById("editpicaid").value = "";
          document.getElementById("PI").innerText = "";
          document.getElementById("CA").innerText = "";
          document.getElementById("statupica").innerText = "NEW";
          document.getElementById("btnBatal").style.visibility = "hidden";
       }
       function SaveKomen() {
          if (document.getElementById("PI").value.length > 300 || document.getElementById("CA").value.length > 300) {
             alert('PI dan CA hanya bisa menampung 300 karakter');
             return;
          }
          if (document.getElementById("PI").value != "" && document.getElementById("CA").value != "") {
             $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "WS/BSCService.asmx/SaveKomentar",
                data: "{'id':" + JSON.stringify(document.getElementById("kpiid").value) + ",'year':" + JSON.stringify(document.getElementById("hyear").value) + ",'month':" + JSON.stringify(document.getElementById("hmonth").value) + ",'PI':" + JSON.stringify(document.getElementById("PI").value) + ",'createdby':" + JSON.stringify(document.getElementById("createdby").value) + ",'createdin':" + JSON.stringify(document.getElementById("createdin").value) + ",'CA':" + JSON.stringify(document.getElementById("CA").value) + ",'IDPICA':" + JSON.stringify(document.getElementById("editpicaid").value) + "}",
                dataType: "json",
                success: function(res) {
                   alert("Save Berhasil");
                   document.getElementById("PI").innerText = "";
                   document.getElementById("CA").innerText = "";
                   BatalKomen();
                   $.ajax({
                      type: "POST",
                      contentType: "application/json; charset=utf-8",
                      url: "WS/BSCService.asmx/GetKomentar",
                      data: "{'id':" + JSON.stringify(document.getElementById("kpiid").value) + ",'year':" + JSON.stringify(document.getElementById("hyear").value) + ",'month':" + JSON.stringify(document.getElementById("hmonth").value) + ",'createdby':" + JSON.stringify(document.getElementById("createdby").value) + "}",
                      dataType: "json",
                      success: function(res) {
                         document.getElementById("comment").innerHTML = res.d;
                      },
                      error: function(err) {
                         document.getElementById("comment").innerHTML = err.responseText;
                      }
                   });
                },
                error: function(err) {
                   alert("error: " + err.responseText);
                }
             });
          } else {
            alert('PI dan CA tidak boleh kosong');
          }
       }

       function getKomen() {
          $.ajax({
             type: "POST",
             contentType: "application/json; charset=utf-8",
             url: "WS/BSCService.asmx/GetKomentar",
             data: "{'id':" + JSON.stringify(document.getElementById("kpiid").value) + ",'year':" + JSON.stringify(document.getElementById("hyear").value) + ",'month':" + JSON.stringify(document.getElementById("hmonth").value) + ",'createdby':" + JSON.stringify(document.getElementById("createdby").value) + "}",
             dataType: "json",
             success: function(res) {
                document.getElementById("comment").innerHTML = res.d;
                showhidedivsucks('divkomen');
             },
             error: function(err) {
                document.getElementById("comment").innerHTML = err.responseText;
             }
          });
       }

       function getActivityPlan() {
          $.ajax({
             type: "POST",
             contentType: "application/json; charset=utf-8",
             url: "WS/BSCService.asmx/GetActivityPlanForKPI",
             data: "{'id':" + JSON.stringify(document.getElementById("kpiid").value) + ",'year':" + JSON.stringify(document.getElementById("hyear").value) + ",'month':" + JSON.stringify(document.getElementById("hmonth").value) + "}",
             dataType: "json",
             success: function(res) {
               document.getElementById("resultactivity").innerHTML = res.d;
             },
             error: function(err) {
               document.getElementById("resultactivity").innerHTML = err.responseText;
             }
          });
       }
      
       function finddetail(id, year, sucks) {
          if (document.getElementById(sucks).innerHTML != "") {
             showhidedivsucks(sucks);
          } else {
             $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "WS/BSCService.asmx/GetChild",
                data: "{'id':" + JSON.stringify(id) + ",'year':" + JSON.stringify(year) + "}",
                dataType: "json",
                success: function(res) {
                   //document.getElementById("resultchild").innerHTML = res.d;
                   document.getElementById(sucks).innerHTML = res.d;
                },
                error: function(err) {
                   alert("error: " + err.responseText);
                }
             });
          }
       }
       
       function showhidedivsucks(id) {
          if (document.getElementById(id).style.display == "block") {
             document.getElementById(id).style.display = "none";
          } else {
             document.getElementById(id).style.display = "block";
          }
       }

       function showhidedivsucks2(id) {
          if (document.getElementById(id).style.display == "block") {
             document.getElementById(id).style.display = "none";
             document.getElementById(id).innerHTML = document.getElementById(id).innerHTML.replace("[-]", "[+]");
          } else {
             document.getElementById(id).style.display = "block";
             document.getElementById(id).innerHTML = document.getElementById(id).innerHTML.replace("[-]", "[+]");
          }
       }

       function showhidediv(id,aid) {
          if (document.getElementById(id).style.display == "block") {
             document.getElementById(id).style.display = "none";
             document.getElementById(aid).innerHTML = document.getElementById(aid).innerHTML.replace("[-]", "[+]");
          } else {
             document.getElementById(id).style.display = "block";
             document.getElementById(aid).innerHTML = document.getElementById(aid).innerHTML.replace("[+]", "[-]");
          }
       }

       function viewdetailprogram(id,year,week,nomor) {
          var returnVal = window.showModalDialog('activitydetail.aspx?id=' + id + '&year=' + year + '&week=' + week + '&nomor=' + nomor, '', 'dialogWidth=' + screen.width + ';dialogHeight=' + screen.height + ';resizable=yes;help=no;unadorned=yes');
       }
      
      function Count(text,howlong)
      {
         var maxlength = new Number(howlong); // Change number to your max length.
         if (document.getElementById(text.id).value.length > maxlength) {
            text.value = text.value.substring(0, maxlength);
            alert("PI dan CA hanya bisa menampung 300 karakter");
         }
      }
      function download(nomor) {
         window.open("tdownload.aspx?n=" + nomor, "Download", "scrollbars=no,resizable=no,width=1,height=1");
      }
      function showLampiran(year, month, kpiid) {
         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/ShowLampiran",
            data: "{'year':'" + year + "','month':'" + month + "','kpiid':'" + kpiid + "'}",
            dataType: "json",
            success: function(res) {
               var afile = new Array();
               afile = res.d;

               var shtml = "<span><u><b>Daftar File Lampiran</b></u></span><br/>";
               if (afile.length > 0) {
                  for (var i = 0; i < afile.length; i++) {
                     shtml = shtml + (i + 1) + '.' + afile[i].NmFile + ' __ ';
                     shtml = shtml + '<a href="#" onclick="download(' + afile[i].Nomor + ')">[Download]</a><br/>';
                  }
               }
               else {
                  shtml = shtml + "Tidak Terdapat Lampiran";
               }
               document.getElementById('divdaftar').innerHTML = shtml;
            },
            error: function(err) {
               alert(err.responseText);
            }
         });
      }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <asp:HiddenField ID="kpiid" runat="server" />
       <asp:HiddenField ID="kpiname" runat="server" />
       <asp:HiddenField ID="hyear" runat="server" />
       <asp:HiddenField ID="hmonth" runat="server" />
       <asp:HiddenField ID="createdby" runat="server" />
       <asp:HiddenField ID="createdin" runat="server" />
       <asp:HiddenField ID="kpilevel" runat="server" />
       <asp:HiddenField ID="editpicaid" runat="server" />
       <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </div>
    <table id="wrapper" class="pica">
    <tr>
      <td colspan="2">
        <div id="resulth" runat="server"></div>       
      </td>
      <%--<td style="text-align:center;width:10%"><div id="headerlevel" class="headerd"></div></td>
      <td style="text-align:center;width:90%"><div id="header" class="headerd"></div></td>--%>
    </tr>
    <tr>
    <td class="picatd">
         <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" Height="320px" ShowToolBar="false" style="overflow:hidden" Font-Size="Smaller">
       </rsweb:ReportViewer>
    </td>
    <td class="picatd">
       <rsweb:ReportViewer ID="ReportViewer2" runat="server" Width="100%" Height="320px" ShowToolBar="false" style="overflow:hidden">
       </rsweb:ReportViewer>
    </td>
    </tr>
    <tr>
      <td valign="top" width="50%">
       <table border="0" width="100%" class="pica">
         <tr>
            <td><div id="resultactivity"></div></td>
         </tr>
         <tr>
            <td>
               <table class="pica" style="width:100%">
               <tr><td colspan="2" style="background-color:#D9D9D9;">PICA KPI Achievement <span id="statupica" style="color:red"><u>NEW</u></span></td></tr>
               <tr>
                  <td>PROBLEM IDENTIFICATION :</td>
                  <td rowspan = "5" valign="top" width="50%"><div id="comment" runat="server" style="width:100%;height:250px;background-color:#ffffff;overflow:auto;"></div></td>
               </tr>
               <tr valign="top">
                  <td><asp:TextBox class="plain" ID="PI" runat="server" Rows="5" TextMode="MultiLine" width="98%" onKeyUp="javascript:Count(this,300);" onChange="javascript:Count(this,300);"></asp:TextBox></td>
               </tr>
               <tr>
                  <td>CORRECTIVE ACTION :</td>
               </tr>
               <tr valign="top">
                  <td><asp:TextBox  class="plain" ID="CA" runat="server" Rows="5" TextMode="MultiLine" width="98%" onKeyUp="javascript:Count(this,300);" onChange="javascript:Count(this,300);"></asp:TextBox></td>
               </tr>
               <tr>
                  <td><input id="btnSaveKomen" type="button" style="background-image: url(../images/save-icon-small.png); background-repeat: no-repeat;cursor:hand;Width:55px;Height:55px;background-color:transparent;border:0" onclick="SaveKomen();"/>
                  <input id="btnBatal" type="button" style="background-image: url(../images/cancel.png); background-repeat: no-repeat;cursor:hand;Width:55px;Height:55px;background-color:transparent;border:0" onclick="BatalKomen();"/>
                  </td>
               </tr>               
               </table>
            </td>
         </tr>    
         <tr>
            <td>
             <table class="pica" style="width:100%">
               <tr><td colspan="2" style="background-color:#D9D9D9;">File Attachment</td></tr>
               <tr>
                  <td colspan="2">
                  <div id="divdaftar"></div>
                  </td>
               </tr>
               </table>
            </td>
         </tr>    
      </table>     
      </td>
      <td valign="top" width="100px">
         <table width="100%">
         <tr><td style="background-color:#D9D9D9;">DRILLDOWN ANALYSIS</td></tr>
         <tr><td><div id="result" runat="server"></div></td></tr>
         </table>         
      </td>
    </tr>
    </table>
    </form>
    <script type="text/javascript">
       getKomen(); getActivityPlan();
       //document.getElementById("header").innerHTML = document.getElementById("kpiname").value;
//       switch (document.getElementById("kpilevel").value) {
//          case '0':
//             document.getElementById("headerlevel").innerHTML = "BSC BUMA"; break;
//          case '1':
//             document.getElementById("headerlevel").innerHTML = "BSC FUNCTION"; break;
//          case '2':
//             document.getElementById("headerlevel").innerHTML = "BSC SECTION"; break;
//          case '3':
//             document.getElementById("headerlevel").innerHTML = "BSC JOBSITE"; break;
       //       }
       document.getElementById("wrapper").style.width = screen.width;
       document.getElementById("btnBatal").style.visibility = "hidden";
       showLampiran(document.getElementById('hyear').value, document.getElementById('hmonth').value, document.getElementById('kpiid').value);
    </script>
</body>
</html>
