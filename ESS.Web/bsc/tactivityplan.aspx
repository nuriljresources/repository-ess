﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="tactivityplan.aspx.vb"
   Inherits="EXCELLENT.tactivityplan" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
   <title></title>

   <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>

   <script src="../Scripts/json2.js" type="text/javascript"></script>

   <script src="scripts/dimmer.js" type="text/javascript"></script>

   <script type="text/javascript">
      var TABLE_NAME = 'activity';
      function imposeMaxLength(Event, Object, MaxLen) {
         return (Object.value.length < MaxLen) || (Event.keyCode == 8 || Event.keyCode == 46 || (Event.keyCode >= 35 && Event.keyCode <= 40))
      }
      function saveprogress(nomor, id) {
         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/InsertActivityProg",
            data: "{'id':" + JSON.stringify(id) + ",'progress':" + JSON.stringify(escape(document.getElementById("myprogress" + nomor).innerText)) + ",'nik':" + document.getElementById("ctl00_ContentPlaceHolder1_userid").value + "}",
            dataType: "json",
            success: function(res) {
               if (res.d == "0") {
                  document.getElementById("divprogress").style.visibility = 'hidden';
                  alert("Save Berhasil");
               }
            },
            error: function(err) {
               document.getElementById("divprogress").style.visibility = 'hidden';
               alert("error: " + err.responseText);
            }
         });
      }

      function retrieveprogress(id, nomor) {
         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/RetrieveActivityProg",
            data: "{'id':" + JSON.stringify(id) + "}",
            dataType: "json",
            success: function(res) {
               document.getElementById("logprogress" + nomor).innerHTML = unescape(res.d);
            },
            error: function(err) {
               alert("error: " + err.responseText);
            }
         });
      }

      function updateStatus(id, nomor, activity) {
         var obj = new Object();
         obj.id = id;
         obj.nomor = nomor;
         obj.activity = activity;
         obj.nik = document.getElementById("ctl00_ContentPlaceHolder1_userid").value;
         var returnVal = window.showModalDialog('updatestatus.aspx', obj, 'dialogWidth=350px;dialogHeight=235px;resizable=yes;help=no;unadorned=yes');
         if (returnVal != undefined) {
            switch (returnVal) {
               case '0':
                  document.getElementById('btnStatus' + nomor).value = 'IDLE';
                  break;
               case '1':
                  document.getElementById('btnStatus' + nomor).value = 'ONP';
                  break;
               case '2':
                  document.getElementById('btnStatus' + nomor).value = 'DONE';
                  break;
            }
         }
      }

      function displayWindow() {
         var w, h, l, t;
         w = 400;
         h = 200;
         l = screen.width / 6;
         t = screen.height / 8;

         // no title		        
         // displayFloatingDiv('windowcontent', '', w, h, l, t);

         // with title		        
         displayFloatingDiv('windowcontent', 'Filter', w, h, l, t);
      }
   </script>

   <link href="css/dimming.css" rel="stylesheet" type="text/css" />
   <style type="text/css">
      body
      {
         font-family: Calibri;
      }
      #divprogress
      {
         z-index: 1000;
         position: absolute;
         top: 0;
         bottom: 0;
         left: 0;
         width: 100%;
         background: #000;
         opacity: 0.55;
         -moz-opacity: 0.55;
         filter: alpha(opacity=55);
         visibility: hidden;
      }
      table.act
      {
         font-family: Calibri;
         border-width: 1px;
         border-spacing: 0px;
         border-style: groove;
         border-color: #ffffff;
         border-collapse: collapse;
         background-color: white;
         font-size: 11px;
      }
      table.act thead
      {
         border-width: 1px;
         padding: 1px;
         border-style: groove;
         border-color: #ebebeb;
      }
      table.act td
      {
         border-width: 1px;
         padding: 1px;
         border-style: groove;
         border-color: #ebebeb;
      }
      input.plain
      {
         border: 1px solid #C0C0C0;
         font-family: Calibri;
      }
      input.plains
      {
         border: 0;
         background-color: Transparent;
         font-family: Calibri;
      }
      .plain:hover
      {
         background-color: #FFFFA8;
      }
      select
      {
         border: 1px solid #C0C0C0;
         font-family: Calibri;
      }
      textarea.plain
      {
         border: 1px solid #C0C0C0;
         font-family: Calibri;
      }
      ul.dropdown *.dir2
      {
         padding-right: 20px;
         background-image: url("/remo/css/dropdown/themes/default/images/nav-arrow-down.png");
         background-position: 100% 50%;
         background-repeat: no-repeat;
         background-color: #FFA500;
         font-family: Calibri;
      }
      select.plains
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         border: 1px solid #C0C0C0;
         font-family: Calibri;
      }
      td.styleHeader
      {
         background-color: #00CC00;
         color: black;
         font-weight: bold;
         font-size: medium;
      }
   </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div id="divprogress">
      <center>
         <img id="imgloading" alt="" src="/remo/images/tloader.gif" style="margin-top: 100px" /></center>
   </div>
   <div>
      <asp:HiddenField ID="Grup" runat="server" />
      <asp:HiddenField ID="Jobsite" runat="server" />
      <asp:HiddenField ID="Divisi" runat="server" />
      <asp:HiddenField ID="Section" runat="server" />
      <asp:HiddenField ID="userid" runat="server" />
      <asp:HiddenField ID="userip" runat="server" />
      <table style="width: 100%">
         <tr>
            <td colspan="3" class="styleHeader" width="40%">
               Update Activity Plan
            </td>
         </tr>
         <tr>
            <td colspan="2" width="40%">
               <asp:Label ID="lblKet" runat="server" Text="" EnableViewState="false" Style="color: red;"></asp:Label>
            </td>
            <td rowspan="4" width="60%" align="right">
               <input type="button" id="btnSave" onclick="save();" style="background-image: url(../images/save-icon-small.png);
                  background-repeat: no-repeat; cursor: hand; width: 55px; height: 55px; background-color: transparent;
                  border: 0" />
            </td>
         </tr>
         <tr>
            <td>
               Tahun
            </td>
            <td>
               <asp:DropDownList ID="ddlYear" runat="server">
                  <asp:ListItem>2010</asp:ListItem>
                  <asp:ListItem>2011</asp:ListItem>
                  <asp:ListItem>2012</asp:ListItem>
               </asp:DropDownList>
            </td>
         </tr>
         <tr>
            <td>
               Level
            </td>
            <td>
               <asp:DropDownList ID="KPILevel" runat="server" AutoPostBack="true" CssClass="plains">
                  <asp:ListItem Value="0" Text="BUMA"></asp:ListItem>
                  <asp:ListItem Value="1" Text="Function"></asp:ListItem>
                  <asp:ListItem Value="2" Text="Section"></asp:ListItem>
                  <asp:ListItem Value="3" Text="Jobsite"></asp:ListItem>
               </asp:DropDownList>
               <asp:DropDownList ID="KPIFunc" runat="server" Visible="False" CssClass="plains">
               </asp:DropDownList>
               <asp:DropDownList ID="KPISection" runat="server" Visible="False" CssClass="plains">
               </asp:DropDownList>
               <asp:DropDownList ID="KPIJobsite" runat="server" Visible="False" CssClass="plains">
               </asp:DropDownList>
               <asp:Button ID="btnView" Text="View" runat="server" />
               <input type="button" value="Filter" onclick="displayWindow();" />
            </td>
         </tr>
      </table>
      <div id="result" runat="Server">
      </div>
      <input type="button" id="btnSave1" onclick="save();" style="background-image: url(../images/save-icon-small.png);
         background-repeat: no-repeat; float: right; cursor: hand; width: 55px; height: 55px;
         background-color: transparent; border: 0" />
      <!--  the following hidden div will be used by the script -->
      <div style="width: 300px; height: 100px; visibility: hidden" id="windowcontent">
         <table>
            <tr>
               <td colspan="2">
               </td>
            </tr>
            <tr>
               <td>
                  PIC:
               </td>
               <td>
                  <asp:DropDownList runat="server" ID="ddlPIC" EnableViewState="true" AutoPostBack="false">
                  </asp:DropDownList>
               </td>
               <td>
                  <asp:CheckBox runat="server" ID="chkPIC" />
               </td>
            </tr>
            <tr>
               <td>
                  Status:
               </td>
               <td>
                  <asp:DropDownList runat="server" ID="ddlStatus" EnableViewState="true" AutoPostBack="false">
                  </asp:DropDownList>
               </td>
               <td>
                  <asp:CheckBox runat="server" ID="chkStatus" />
               </td>
            </tr>
            <tr>
               <td colspan="3">
                  <asp:Button runat="server" ID="btnFilter" Text="Execute" />
               </td>
            </tr>
         </table>
      </div>
   </div>

   <script type="text/javascript">
      function save() {
         document.getElementById("btnSave").style.display = "none";
         document.getElementById("btnSave1").style.display = "none";
         document.getElementById("divprogress").style.visibility = 'visible';
         MyArray = new Array();
         var table = document.getElementById(TABLE_NAME);
         for (var r = 0, n = table.tBodies[0].rows.length - 1; r < n; r++) {
            if (table.tBodies[0].rows[r].cells.length > 2) {
               if (table.tBodies[0].rows[r].getElementsByTagName('textarea')[0] != undefined) {
                  if (escape(table.tBodies[0].rows[r].cells[5].getElementsByTagName('textarea')[0].value).length > 500) {
                     alert('Jumlah karakter pada field progress tidak boleh lebih dari 500 karakter');
                     table.tBodies[0].rows[r].cells[5].getElementsByTagName('textarea')[0].style = "background-color:red";
                     document.getElementById("divprogress").style.visibility = 'hidden';
                     return false;
                  }
                  MyArray[MyArray.length] = new objact(
                  table.tBodies[0].rows[r].cells[5].getElementsByTagName('input')[0].value,
                  escape(table.tBodies[0].rows[r].cells[5].getElementsByTagName('textarea')[0].value)
                  );
               }
            }
         }
         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/UpdateActivity",
            data: "{'x':" + JSON.stringify(MyArray) + ",'userid':" + document.getElementById("ctl00_ContentPlaceHolder1_userid").value + ",'userip':" + JSON.stringify(document.getElementById("ctl00_ContentPlaceHolder1_userip").value) + "}",
            dataType: "json",
            success: function(res) {
               //document.getElementById("resultchild").innerHTML = res.d;
               if (res.d == "0") {
                  alert("Save Berhasil");
                  document.getElementById("ctl00_ContentPlaceHolder1_btnView").click();
               } else if (res.d == "BLANK") {
                  alert("Tidak ada data untuk di save");
                  document.getElementById("divprogress").style.visibility = 'hidden';
               } else {
                  document.getElementById("divprogress").style.visibility = 'hidden';
                  alert(res.d);
               }
               document.getElementById("btnSave").style.display = "";
               document.getElementById("btnSave1").style.display = "";
            },
            error: function(err) {
               document.getElementById("btnSave").style.display = "";
               document.getElementById("btnSave1").style.display = "";
               document.getElementById("divprogress").style.visibility = 'hidden';
               alert("error: " + err.responseText);
            }
         });
      }

      function objact(id, progress) {
         this.id = id; this.progress = progress;

      }

      function openupload(what, activityid, actname) {
         //alert(what.parentNode.innerHTML);
         window.open('tactupload.aspx?actid=' + activityid + '&actname=' + escape(actname) + '&fn=' + what.parentNode.id, '', 'dialogWidth=300px;dialogHeight=300px;resizable=yes;help=no;unadorned=yes');
      }

      function openview(what, activityid, actname) {
         //alert(what.parentNode.innerHTML);
         window.open('tactviewfile.aspx?actid=' + activityid + '&actname=' + escape(actname) + '&fn=' + what.parentNode.id, '', 'dialogWidth=300px;dialogHeight=300px;resizable=yes;help=no;unadorned=yes');
      }


      function setTargetField(targetField, total) {
         if (targetField) {
            if (parseInt(total) > 0) {
               document.getElementById(targetField).innerHTML = document.getElementById(targetField).innerHTML.replace("&nbsp;", "<img src='../images/book.jpg' alt='attachment' height='30px' width='30px'/>");
            } else {
               document.getElementById(targetField).innerHTML = document.getElementById(targetField).innerHTML.replace('<IMG alt=attachment src="../images/book.jpg" width=30 height=30>', '&nbsp;');
            }
         }
      }

      
   </script>

</asp:Content>
