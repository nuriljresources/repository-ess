<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="entry_permit_doc.aspx.vb" Inherits="EXCELLENT.entry_permit_doc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head id="Head1" runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="description" content="Permit" />
<meta name="keywords" content="Permit" />


<script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="../Scripts/jscal2.js" type ="text/javascript"></script>
    <script src="../Scripts/en.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="../css/win2k/win2k.css" />
    
    <style type="text/css" >
        #form1
        {
            font-family:Tahoma,Arial;
        }
        #form1 h1 
        {
            color: #FFFFFF;
            font-size:x-large;
        }
        .title
        {
            color: blue;
            font-size: small;
            margin-bottom:10px;
        }
        .line
        {
            width: 100%;
            height: 20px;
            font-family:Tahoma,Arial;
            font-size:0.8em;
        }
        .general
        {
            font-size:0.8em;
            color:#33ADFF;
            font-weight:bold;
            border-style:solid;
            border-width:1px;
            border-color:#33ADFF;
            padding-left:5px;
        }
        
        .general a
        {
            text-decoration:none;
            color:#33ADFF;
        }
        
        .general a:hover
        {
            text-decoration:underline;
            color:#33ADFF;
        }
        .style1
        {
            height: 26px;
        }
    </style>
    
    <script language="javascript" type="text/javascript">
        var BrowserDetect = {
            init: function() {
                this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
                this.version = this.searchVersion(navigator.userAgent)
			        || this.searchVersion(navigator.appVersion)
			        || "an unknown version";
                this.OS = this.searchString(this.dataOS) || "an unknown OS";
            },
            searchString: function(data) {
                for (var i = 0; i < data.length; i++) {
                    var dataString = data[i].string;
                    var dataProp = data[i].prop;
                    this.versionSearchString = data[i].versionSearch || data[i].identity;
                    if (dataString) {
                        if (dataString.indexOf(data[i].subString) != -1)
                            return data[i].identity;
                    }
                    else if (dataProp)
                        return data[i].identity;
                }
            },
            searchVersion: function(dataString) {
                var index = dataString.indexOf(this.versionSearchString);
                if (index == -1) return;
                return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
            },
            dataBrowser: [
		        {
		            string: navigator.userAgent,
		            subString: "Chrome",
		            identity: "Chrome"
		        },
		        { string: navigator.userAgent,
		            subString: "OmniWeb",
		            versionSearch: "OmniWeb/",
		            identity: "OmniWeb"
		        },
		        {
		            string: navigator.vendor,
		            subString: "Apple",
		            identity: "Safari",
		            versionSearch: "Version"
		        },
		        {
		            prop: window.opera,
		            identity: "Opera"
		        },
		        {
		            string: navigator.vendor,
		            subString: "iCab",
		            identity: "iCab"
		        },
		        {
		            string: navigator.vendor,
		            subString: "KDE",
		            identity: "Konqueror"
		        },
		        {
		            string: navigator.userAgent,
		            subString: "Firefox",
		            identity: "Firefox"
		        },
		        {
		            string: navigator.vendor,
		            subString: "Camino",
		            identity: "Camino"
		        },
		        {		// for newer Netscapes (6+)
		            string: navigator.userAgent,
		            subString: "Netscape",
		            identity: "Netscape"
		        },
		        {
		            string: navigator.userAgent,
		            subString: "MSIE",
		            identity: "Explorer",
		            versionSearch: "MSIE"
		        },
		        {
		            string: navigator.userAgent,
		            subString: "Gecko",
		            identity: "Mozilla",
		            versionSearch: "rv"
		        },
		        { 		// for older Netscapes (4-)
		            string: navigator.userAgent,
		            subString: "Mozilla",
		            identity: "Netscape",
		            versionSearch: "Mozilla"
		        }
	        ],
            dataOS: [
		        {
		            string: navigator.platform,
		            subString: "Win",
		            identity: "Windows"
		        },
		        {
		            string: navigator.platform,
		            subString: "Mac",
		            identity: "Mac"
		        },
		        {
		            string: navigator.userAgent,
		            subString: "iPhone",
		            identity: "iPhone/iPod"
		        },
		        {
		            string: navigator.platform,
		            subString: "Linux",
		            identity: "Linux"
		        }
	        ]

        };
        BrowserDetect.init();
    </script>
    
    <script type="text/javascript">

        /***********************************************
        * Cool DHTML tooltip script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
        * This notice MUST stay intact for legal use
        * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
        ***********************************************/

        var offsetxpoint = -60 //Customize x offset of tooltip
        var offsetypoint = 20 //Customize y offset of tooltip
        var ie = document.all
        var ns6 = document.getElementById && !document.all
        var enabletip = false
        if (ie || ns6)
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : ""

        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body
        }

        function ddrivetip(thetext, thecolor, thewidth) {
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") tipobj.style.width = thewidth + "px"
                if (typeof thecolor != "undefined" && thecolor != "") tipobj.style.backgroundColor = thecolor
                tipobj.innerHTML = thetext
                enabletip = true
                return false
            }
        }

        function positiontip(e) {
            if (enabletip) {
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var rightedge = ie && !window.opera ? ietruebody().clientWidth - event.clientX - offsetxpoint : window.innerWidth - e.clientX - offsetxpoint - 20
                var bottomedge = ie && !window.opera ? ietruebody().clientHeight - event.clientY - offsetypoint : window.innerHeight - e.clientY - offsetypoint - 20

                var leftedge = (offsetxpoint < 0) ? offsetxpoint * (-1) : -1000

                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth)
                //move the horizontal position of the menu to the left by it's width
                    tipobj.style.left = ie ? ietruebody().scrollLeft + event.clientX - tipobj.offsetWidth + "px" : window.pageXOffset + e.clientX - tipobj.offsetWidth + "px"
                else if (curX < leftedge)
                    tipobj.style.left = "5px"
                else
                //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = curX + offsetxpoint + "px"

                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight)
                    tipobj.style.top = ie ? ietruebody().scrollTop + event.clientY - tipobj.offsetHeight - offsetypoint + "px" : window.pageYOffset + e.clientY - tipobj.offsetHeight - offsetypoint + "px"
                else
                    tipobj.style.top = curY + offsetypoint + "px"
                tipobj.style.visibility = "visible"
            }
        }

        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false
                tipobj.style.visibility = "hidden"
                tipobj.style.left = "-1000px"
                tipobj.style.backgroundColor = ''
                tipobj.style.width = ''
            }
        }

        document.onmousemove = positiontip

</script>
    
    <script type="text/javascript" >
    
        function OpenPopup(key) {
            document.getElementById("ctrlToFind").value = key;
            window.open("spic.aspx", "List", "scrollbars=no,resizable=no,width=500,height=400");
            return false;
        }
        
            window.onload = function() {
            var check = document.getElementById("rpt");
                check.onchange = function() {
                    if (this.checked == true)
                        document.getElementById('<%=rem_days.ClientID%>').disabled = false;
                    else
                        document.getElementById('<%=rem_days.ClientID%>').disabled = true;
                };
            var check2 = document.getElementById("chkpic1");
                check2.onchange = function() {
                    if (this.checked == true)
                        document.getElementById('<%=txtpic1.ClientID%>').style.visibility = "visible";
                    else
                        document.getElementById('<%=txtpic1.ClientID%>').style.visibility = "hidden";
                };
             var check3 = document.getElementById("chkpic2");
                check3.onchange = function() {
                    if (this.checked == true)
                        document.getElementById('<%=txtpic2.ClientID%>').style.visibility = "visible";
                    else
                        document.getElementById('<%=txtpic2.ClientID%>').style.visibility = "hidden";
                };
             var check4 = document.getElementById("chkpic3");
                check4.onchange = function() {
                    if (this.checked == true)
                        document.getElementById('<%=txtpic3.ClientID%>').style.visibility = "visible";
                    else
                        document.getElementById('<%=txtpic3.ClientID%>').style.visibility = "hidden";
                };
             var check5 = document.getElementById("chkpic4");
                check5.onchange = function() {
                    if (this.checked == true)
                        document.getElementById('<%=txtpic4.ClientID%>').style.visibility = "visible";
                    else
                        document.getElementById('<%=txtpic4.ClientID%>').style.visibility = "hidden";
                };
             var check6 = document.getElementById("chkpic5");
                check6.onchange = function() {
                    if (this.checked == true)
                        document.getElementById('<%=txtpic5.ClientID%>').style.visibility = "visible";
                    else
                        document.getElementById('<%=txtpic5.ClientID%>').style.visibility = "hidden";
                };        
            };

        function change() {
            var fullPath = document.getElementById('<%= Fileupload1.ClientID %>').value;

            if (fullPath) {
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                var obj1 = document.getElementById("filename");
                var obj2 = document.getElementById("AttachmentTitleTextbox");

                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    obj2.value = filename;
                }
                //                alert(filename);
            }
        }

    function getFileExtension(filename) {
        var ext = /^.+\.([^.]+)$/.exec(filename);
        return ext == null ? "" : ext[1];
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

     function simpan(){
     var fullPath = document.getElementById('<%= Fileupload1.ClientID %>').value;
     
     }

        function save() {
            var fullPath = document.getElementById('<%= Fileupload1.ClientID %>').value;
            alert(fullPath);
            if (fullPath.length == 0) {
                document.getElementById('<%= label1.ClientID %>').text = 'You have not specified a file.'
                return;
            }
            
            if (fullPath) {
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                var obj1 = document.getElementById("filename");
                var obj2 = document.getElementById("AttachmentTitleTextbox");

                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    obj2.value = filename;
                }
                //                alert(filename);
            }

            var extension = getFileExtension(fullPath);

            if (extension.toLowerCase() != 'pdf') {
                alert("Only .pdf files allowed!");
                return;
            }
            
            var d = document;
            var torpt;
            var s = document.getElementById('type');
            var type = s.options[s.selectedIndex].value;

            var s2 = document.getElementById('doctype');
            var dtype = s2.options[s2.selectedIndex].value;

            var fname = dtype + "_" + d.getElementById("subject").value + "_" + d.getElementById('<%=ddlcomp.ClientID%>').value + "_" + d.getElementById('<%=ddlsite.ClientID%>').value + "_" + d.getElementById("txtyear").value + "_" + filename;

            var check = document.getElementById("rpt");
            if (check.checked == true) {
                torpt = 1
            }

            if (check.checked == false) {
                torpt = 0
            }
            
            if (d.getElementById("type").value.length == 0) {
                alert("Please Entry Document Type");
                return false;
            }

            d.getElementById('<%=tipe.ClientID%>').value = type
            d.getElementById('<%=doctype2.ClientID%>').value = dtype 
            
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../WS/Service.asmx/Addpermit",
                data: "{'_type':" + JSON.stringify(type) + ",'_subject':" + JSON.stringify(document.getElementById("subject").value) + ",'_comp':" + JSON.stringify(document.getElementById('<%=ddlcomp.ClientID%>').value) + ",'_site':" + JSON.stringify(document.getElementById('<%=ddlsite.ClientID%>').value) + ",'_year':" + JSON.stringify(document.getElementById("txtyear").value) + ",'_category':" + JSON.stringify(document.getElementById("category").value) + ",'_doctype':" + JSON.stringify(dtype) + ",'_department':" + JSON.stringify(document.getElementById("Department").value) + ",'_subdept':" + JSON.stringify(document.getElementById("subdept").value) + ",'_number':" + JSON.stringify(document.getElementById("Number").value) + ",'_report':" + JSON.stringify(torpt) + ",'_dtissue':" + JSON.stringify(document.getElementById("dtissue").value) + ",'_dtexp':" + JSON.stringify(document.getElementById("dtexp").value) + ",'_fileupload1':" + JSON.stringify(fname) + ",'_filename':" + JSON.stringify(filename) + "}",
                dataType: "json",
                success: function(res) {
                alert("Data Saved Successfully");
                window.location = "entry_permit_doc.aspx";
                },
                error: function(err) {
                alert("Can't Save Data");
                    //window.location = "entry_permit_doc.aspx";
                }
            });
        }
    </script>
    <title>Document Control Entry</title>
    <script type="text/javascript">

        /***********************************************
        * Cool DHTML tooltip script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
        * This notice MUST stay intact for legal use
        * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
        ***********************************************/

        var offsetxpoint = -60 //Customize x offset of tooltip
        var offsetypoint = 20 //Customize y offset of tooltip
        var ie = document.all
        var ns6 = document.getElementById && !document.all
        var enabletip = false
        if (ie || ns6)
            var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : ""

        function ietruebody() {
            return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body
        }

        function ddrivetip(thetext, thecolor, thewidth) {
            if (ns6 || ie) {
                if (typeof thewidth != "undefined") tipobj.style.width = thewidth + "px"
                if (typeof thecolor != "undefined" && thecolor != "") tipobj.style.backgroundColor = thecolor
                tipobj.innerHTML = thetext
                enabletip = true
                return false
            }
        }

        function positiontip(e) {
            if (enabletip) {
                var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
                var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
                //Find out how close the mouse is to the corner of the window
                var rightedge = ie && !window.opera ? ietruebody().clientWidth - event.clientX - offsetxpoint : window.innerWidth - e.clientX - offsetxpoint - 20
                var bottomedge = ie && !window.opera ? ietruebody().clientHeight - event.clientY - offsetypoint : window.innerHeight - e.clientY - offsetypoint - 20

                var leftedge = (offsetxpoint < 0) ? offsetxpoint * (-1) : -1000

                //if the horizontal distance isn't enough to accomodate the width of the context menu
                if (rightedge < tipobj.offsetWidth)
                //move the horizontal position of the menu to the left by it's width
                    tipobj.style.left = ie ? ietruebody().scrollLeft + event.clientX - tipobj.offsetWidth + "px" : window.pageXOffset + e.clientX - tipobj.offsetWidth + "px"
                else if (curX < leftedge)
                    tipobj.style.left = "5px"
                else
                //position the horizontal position of the menu where the mouse is positioned
                    tipobj.style.left = curX + offsetxpoint + "px"

                //same concept with the vertical position
                if (bottomedge < tipobj.offsetHeight)
                    tipobj.style.top = ie ? ietruebody().scrollTop + event.clientY - tipobj.offsetHeight - offsetypoint + "px" : window.pageYOffset + e.clientY - tipobj.offsetHeight - offsetypoint + "px"
                else
                    tipobj.style.top = curY + offsetypoint + "px"
                tipobj.style.visibility = "visible"
            }
        }

        function hideddrivetip() {
            if (ns6 || ie) {
                enabletip = false
                tipobj.style.visibility = "hidden"
                tipobj.style.left = "-1000px"
                tipobj.style.backgroundColor = ''
                tipobj.style.width = ''
            }
        }

        document.onmousemove = positiontip

</script>
</head>
<body>

<div id="dhtmltooltip"></div>
    <form id="formmain" runat="server">
     <div id="wrapper" style="height:100%;">
<input type="hidden" id="ctrlToFind" />   
    <div id="top_content" style="background-color:#4DD9E3; border-bottom:solid 1px blue">
        <div id="logo" style="height:80px; width:80px;"><img alt="Permit Folder" src="../images/Folder_img.png" style="height:80px; width:80px;" /></div>
        <div style="width:450px; left:100px; text-align:left; position:absolute; top: 0px; height: 72px;">
            <h1 style="color:#FFF; font-family:Tahoma,Arial; font-size:x-large;">Welcome to the Document Center</h1>
        </div>
        <div id="button" style="width:450px; left:100px; text-align:left; position:absolute; top: 50px;">
            <%--<asp:Button ID="uploaddoc" Text="Upload Document" runat="server" />--%>
        </div>
    </div>
    
    <div style="padding-left:211px; padding-top:10px; position:absolute;">
    <asp:Button ID="Button1" runat="server" Text="" EnableTheming="false" style="background-image:url('../images/save-icon-small.png'); background-repeat:no-repeat; background-position:center; width:32px; height:32px;"/>
    <%--<img id="saveimg"  alt="Save" src="../images/save-icon-small.png" style="cursor:pointer" onclick="save();" title="Simpan" />--%>
    <asp:Button ID="Button2" runat="server" Text="" EnableTheming="false" style="background-image:url('../images/cancel.png'); background-repeat:no-repeat; background-position:center; width:32px; height:32px;"/>    
    </div>
    <div class="line" style="right:10px; text-align:right;">
    <a href="signout.aspx">Logout</a>
    </div>
    <div class="title">
        <strong>Menu</strong>
    </div>
    <div id="list_newest" class="general" style="width:200px; color:#33ADFF;">
        <div id="Div1"><img alt="open document" src="../images/open_doc_small.png" /> <a href="index.aspx">&nbsp;Home</a></div>
        <%=mnentry%>
        <%=mncontract%>
        <%=mnview%>
        <%=mnmgmt%>
    </div>
 
    <div id="list_permit" class="general" style="width:1000px; left:220px; top:135px; position:absolute; font-size:0.6em;">
        <asp:Label ID="label1" runat="server" ></asp:Label>
        <asp:Label ID="label2" runat="server" ></asp:Label>
        <table style="height:100%">
        <tr>
            <td class="style1">
                File Upload
            </td>
            <td class="style1">
            <input type="hidden" id="filename" />
            <input type="hidden" id="AttachmentTitleTextbox" />
                <asp:FileUpload ID="FileUpload1" runat="server"/>
            </td>
        </tr>
        <tr>
            <td>
                type
            </td>
            <td>
                <asp:HiddenField ID="tipe" runat="server" />
                <asp:DropDownList ID="doctp" runat="server" >
                <asp:ListItem Value="SK">Peraturan / SK</asp:ListItem>
                <asp:ListItem Value="Dokumen">Dokumen</asp:ListItem>
                <asp:ListItem Value="Surat">Surat</asp:ListItem>
                <asp:ListItem Value="Peta">Peta</asp:ListItem>
                <asp:ListItem Value="Surat Masuk">Surat Masuk</asp:ListItem>
                <asp:ListItem Value="Surat Keluar">Surat Keluar</asp:ListItem>
                </asp:DropDownList>
               <%-- <select id="doctype">
                <option value="SK">Peraturan / SK</option>
                <option value="Dokumen">Dokumen</option>
                <option value="Surat">Surat</option>
                <option value="Peta">Peta</option>
                <option value="Surat Masuk">Surat Masuk</option>
                <option value="Surat Keluar">Surat Keluar</option>
                </select>--%>
                <select id="type" style="visibility:hidden;">
                <option value=" "> </option>
                </select>
                <asp:HiddenField ID="doctype2" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                subject
            </td>
            <td>
                <input type="text" id="subject" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Company
            </td>
            <td>
                <asp:DropDownList ID="ddlcomp" runat="server" DataSourceID="SqlDataSource1" 
                    DataTextField="NmCompany" DataValueField="KdCompany"></asp:DropDownList> 
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:jrn_permit %>" 
                    SelectCommand="SELECT KdCompany, NmCompany FROM JRN_TEST.dbo.H_A110">
                </asp:SqlDataSource>
                <%--<input type="text" id="comp" runat="server" />--%>
            </td>
        </tr>
        <tr>
            <td>
                Site
            </td>
            <td>
                <asp:DropDownList ID="ddlsite" runat="server" DataSourceID="SqlDataSource2" 
                    DataTextField="NmSite" DataValueField="KdSite" ></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:jrn_permit %>" 
                    SelectCommand="SELECT KdSite, NmSite FROM JRN_TEST.dbo.H_A120">
                </asp:SqlDataSource>
                <%--<input type="text" id="site" runat="server" />--%>
            </td>
        </tr>
        <tr>
            <td>
                Year
            </td>
            <td>
                <input type="text" id="txtyear" runat="server"/>
            </td>
        </tr>
        <tr>
            <td>
                Category
            </td>
            <td>
                <input type="text" id="category" runat="server"/>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                
            </td>
        </tr>
        <tr>
            <td>
                Department
            </td>
            <td>
                <input type="text" id="Department" runat="server"/>
            </td>
        </tr>
        <tr>
            <td>
                Sub Department
            </td>
            <td>
                <input type="text" id="subdept" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Number
            </td>
            <td>
                <input type="text" id="Number" runat="server" onkeypress="return isNumberKey(event)"/>
            </td>
        </tr>
        <tr>
            <td>
                Wajib Lapor
            </td>
            <td>
                <input type="checkbox" id="rpt" runat="server" />
                <input type="hidden" id="report" />
                <asp:DropDownList ID="rem_days" runat="server" Enabled="false" >
                <asp:ListItem Value="0">-</asp:ListItem>
                <asp:ListItem Value="30">1 Bulan Sebelum Expire</asp:ListItem>
                <asp:ListItem Value="60">2 Bulan Sebelum Expire</asp:ListItem>
                <asp:ListItem Value="90">3 Bulan Sebelum Expire</asp:ListItem>
                <asp:ListItem Value="180">6 Bulan Sebelum Expire</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Date Issue
            </td>
            <td>
                <input type="text" id="dtissue" runat="server" readonly="readonly" /><button id="btdtissue">..</button>
            </td>
        </tr>
        <tr>
            <td>
                Date Expired
            </td>
            <td>
                <input type="text" id="dtexp" runat="server" readonly="readonly" /><button id="btdtexp">..</button>
            </td>
        </tr>  
        <tr>
            <td>
                PIC 1
            </td>
            <td><input type="checkbox" id="chkpic1" runat="server" />
                <asp:DropDownList ID="txtpic1" runat="server" style="visibility:hidden;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email"></asp:DropDownList>
                <asp:SqlDataSource ID="DsPIC" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:jrn_permit %>" 
                    
                    SelectCommand="SELECT [pic_name], [pic_email] FROM [docpermit_pic] ORDER BY [pic_name]">
                </asp:SqlDataSource>
                <%--<img alt="add" id="Img5" src="../images/Search.gif" align="absmiddle"
               style="cursor: pointer" onclick="OpenPopup('reqby')" />--%>
            </td>
        </tr>
        <tr>
            <td>
                PIC 2
            </td>
            <td> <input type="checkbox" id="chkpic2" runat="server" />
                <asp:DropDownList ID="txtpic2" runat="server" style="visibility:hidden;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                PIC 3
            </td>
            <td>
                <input type="checkbox" id="chkpic3" runat="server" />
                <asp:DropDownList ID="txtpic3" runat="server" style="visibility:hidden;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email">
                </asp:DropDownList>
                
            </td>
        </tr>
        <tr>
            <td>
                PIC 4
            </td>
            <td>
                <input type="checkbox" id="chkpic4" runat="server" />
                <asp:DropDownList ID="txtpic4" runat="server" style="visibility:hidden;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                PIC 5
            </td>
            <td>
                <input type="checkbox" id="chkpic5" runat="server" />
                <asp:DropDownList ID="txtpic5" runat="server" style="visibility:hidden;" DataSourceID="DsPIC" 
                    DataTextField="pic_name" DataValueField="pic_email">
                </asp:DropDownList>
            </td>
        </tr>                 
        </table>
    </div>
    </div>
    <div id="idnotif">        
        
    </div>   
    
    <center>
          <span id="idbar" style="color: #800000; background-color: #6699FF; font-weight: bold"></span>
    </center>
    </form>
    
    <script type="text/javascript" language="javascript">
        if (BrowserDetect.version != 8) {
            document.getElementById("idnotif").style.display = "block";
            document.getElementById("idnotif").style.textAlign = "center"
            document.getElementById("idnotif").style.fontsize = "large";
            document.getElementById("idnotif").style.fontWeight = "bold";
            document.getElementById("idnotif").style.color = "#FF0000";
            //        document.getElementById("idnotif").innerHTML = "*** Application HRMS not compatible with your browser ***<br />*** Please Update to Internet Explorer 8 ***";
        }
        else {
            document.getElementById("idnotif").style.display = "none";
            document.getElementById("idnotif").style.textAlign = "center"
            document.getElementById("idnotif").style.fontsize = "large";
            document.getElementById("idnotif").style.fontWeight = "bold";
            document.getElementById("idnotif").style.color = "#FFFFFF";
            document.getElementById("idnotif").innerHTML = "";
        }
        document.getElementById("idbar").style.display = "none";

        var cal = Calendar.setup({
            onSelect: function(cal) { cal.hide() },
            showTime: false
        });
        cal.manageFields("btdtissue", "dtissue", "%m/%d/%Y");
        cal.manageFields("btdtexp", "dtexp", "%m/%d/%Y");
          
</script>

</body>
</html>
