﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="popupnewkpi.aspx.vb" Inherits="EXCELLENT.popupnewkpi" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Master KPI</title>
   <meta http-equiv="Expires" content="0" />
   <meta http-equiv="Cache-Control" content="no-cache" />
   <meta http-equiv="Pragma" content="no-cache" />

   <script src="jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>

   <script src="../Scripts/json2.js" type="text/javascript"></script>

   <script type="text/javascript">
      var TABLE_NAME = 'childKPI';
      function addRowToTable(kpiid, kpiname) {
         var tbl = document.getElementById(TABLE_NAME);
         var nextRow = tbl.tBodies[0].rows.length;
         var num = nextRow;
         var row = tbl.tBodies[0].insertRow(num);

         var cell0 = row.insertCell(0);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'nox' + num + 1);
         txtInp.setAttribute('id', 'nox' + num + 1);
         txtInp.setAttribute('size', '1');
         txtInp.setAttribute('class', 'tb09');
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('value', num + 1);
         cell0.appendChild(txtInp);

         var cell1 = row.insertCell(1);
         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'hidden');
         txtInp.setAttribute('name', 'kcid' + num + 1);
         txtInp.setAttribute('id', 'kcid' + num + 1);
         txtInp.setAttribute('value', kpiid);
         cell1.appendChild(txtInp);

         var txtInp = document.createElement('input');
         txtInp.setAttribute('type', 'text');
         txtInp.setAttribute('name', 'kcname' + num + 1);
         txtInp.setAttribute('id', 'kcname' + num + 1);
         txtInp.setAttribute('disabled', 'disabled');
         txtInp.setAttribute('style', 'width:95%');
         txtInp.setAttribute('value', kpiname);
         cell1.appendChild(txtInp);

         var cell2 = row.insertCell(2);
         cell2.innerHTML = cell2.innerHTML + '<center><img title="Delete Row" class="tcalIcon" onclick="delRow(document.getElementById(\'nox' + num + 1 + '\').value,' + kpiid + ')" src="images/delete.gif"/></center>';
      }

      function delRow(num, kpiid) {
         //alert("{'_idparent':" + JSON.stringify(document.getElementById("KPIID").value) + ",'_idchild':" + JSON.stringify(kpiid) + "}");
         $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/BSCService.asmx/DelKPIRelation",
            data: "{'_idparent':" + JSON.stringify(document.getElementById("KPIID").value) + ",'_idchild':" + JSON.stringify(kpiid) + "}",
            dataType: "json",
            success: function(res) {
               if (res.d == "SUCCESS") {
                  var tbl = document.getElementById(TABLE_NAME);
                  tbl.deleteRow(num);
                  reorderRows(tbl);
               } else {
                  alert(res.d);
               }
            },
            error: function(err) {
               alert("error: " + err.responseText);
            }
         });
      }

      function reorderRows(tbl) {
         if (tbl.tBodies[0].rows[0]) {
            var count = 1;
            for (var i = 0; i < tbl.tBodies[0].rows.length; i++) {
               if (tbl.tBodies[0].rows[i].style.display != 'none') {
                  tbl.tBodies[0].rows[i].cells[0].getElementsByTagName('input')[0].value = count;
                  count++;
               }
            }
         }
      }


      function openModal() {
         if (document.getElementById("KPIID").value != "[AUTO]") {
            var selObj = document.getElementById('KPILevel');
            var selIndex = selObj.selectedIndex;
            var returnVal = window.showModalDialog('masterkpi.aspx?type=' + selObj.options[selIndex].value + '&id=' + document.getElementById("KPIID").value, '', 'dialogWidth=575px;dialogHeight=570px;resizable=yes;help=no;unadorned=yes');
            if (returnVal != undefined) {
               var splitter = returnVal.split('|');
               addRowToTable(splitter[0], splitter[1])
            }
         } else {
            alert("Harap Simpan Data KPI Anda terlebih dahulu");
         }
      }
      function Unload() {
         //if (document.getElementById("KPIID").value != "[AUTO]") {
         window.returnValue = 'OK';
         //}
      }
      function fncInputNumericValuesOnly() { if (!(event.keyCode == 46 || event.keyCode == 48 || event.keyCode == 49 || event.keyCode == 50 || event.keyCode == 51 || event.keyCode == 52 || event.keyCode == 53 || event.keyCode == 54 || event.keyCode == 55 || event.keyCode == 56 || event.keyCode == 57)) { event.returnValue = false; } }

      var obj = window.dialogArguments;

      function doPaste(val) {
         switch (val) {
            case "KPIName":
               document.getElementById("KPIName").value = obj.KPIName;
               break;
            case "KPIUoM":
               document.getElementById("KPIUOM").value = obj.KPIUOM;
               break;
            case "KPIDesc":
               document.getElementById("KPIDesc").value = obj.KPIDesc;
               break;
            case "KPIFDesc":
               document.getElementById("KPIFDesc").value = obj.KPIFDesc;
               break;
            case "KPILeads":
               document.getElementById("KPILeads").value = obj.KPILeads;
               break;
            case "KPIProcess":
               document.getElementById("KPIProcess").value = obj.KPIProcess;
               break;
         }
      }
      function openpopupcluster() {
         if (document.getElementById("KPIID").value == "[AUTO]") {
            alert("Harap Simpan KPI Anda terlebih dahulu!");
            return false;
         } else {
            switch (document.getElementById("KPICalcTypeC").value) {
               case "2":
                  var returnVal = window.open('popupdiscrete.aspx?id=' + document.getElementById("KPIID").value + '&n=' + escape(document.getElementById("KPIName").value) + '&c=' + document.getElementById("KPICalcTypeC").value, 'tes', 'width=600px;height=600px');
                  break;
               case "3":
                  var returnVal = window.open('popupstab.aspx?id=' + document.getElementById("KPIID").value + '&n=' + escape(document.getElementById("KPIName").value) + '&c=' + document.getElementById("KPICalcTypeC").value, 'tes', 'width=1000px;height=600px');
                  break;
               case "4":
                  var returnVal = window.open('popupdiscrete.aspx?id=' + document.getElementById("KPIID").value + '&n=' + escape(document.getElementById("KPIName").value) + '&c=' + document.getElementById("KPICalcTypeC").value, 'tes', 'width=600px;height=600px');
                  break;
            }
         }
      }

      function onchangekpicalch() {
         //alert(document.getElementById("ctl00_ContentPlaceHolder1_KPICalcTypeH").value);
         switch (document.getElementById("KPICalcTypeH").value) {
            case "0":
               document.getElementById("KPICalcTypeD").style.visibility = "visible";
               document.getElementById("KPICalcTypeD").style.width = "100%";
               document.getElementById("KPICalcTypeC").style.visibility = "hidden";
               document.getElementById("KPICalcTypeC").style.width = "0";
               document.getElementById("KPICluster").style.visibility = '';
               document.getElementById("KPICluster").style.visibility = 'hidden';
               break;
            case "1":
               document.getElementById("KPICalcTypeD").style.visibility = "hidden";
               document.getElementById("KPICalcTypeD").style.width = "0";
               document.getElementById("KPICalcTypeC").style.visibility = "visible";
               document.getElementById("KPICalcTypeC").style.width = "100%";
               document.getElementById("KPICluster").style.visibility = '';
               break;
         }
      }       
   </script>

   <style type="text/css">
      input.btn
      {
         color: #050;
         font: bold 90% 'trebuchet ms' ,helvetica,sans-serif;
         background-color: #fed;
         border: 1px solid;
         border-color: #696 #363 #363 #696;
         filter: progid:DXImageTransform.Microsoft.Gradient (GradientType=0,StartColorStr= '#ffffffff' ,EndColorStr= '#ffeeddaa' );
         padding: 0 10px 0 10px;
         cursor: hand;
      }
      #wrapper
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         font-size: small;
      }
      td.styleHeader
      {
         background-color: #00CC00;
         color: white;
         font-weight: bold;
         font-size: medium;
      }
      input.plain
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }
      .plain:hover
      {
         background-color: #FFFFA8;
      }
      select.plains
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }
      textarea
      {
         font-family: "trebuchet MS" , verdana, sans-serif;
         border: 1px solid #C0C0C0;
      }
      table.act
      {
         border-width: 1px;
         border-spacing: 0px;
         border-color: #C0C0C0;
         border-collapse: collapse;
         background-color: white;
      }
      table.act thead
      {
         border-width: 1px;
         padding: 1px;
         border-color: #C0C0C0;
      }
      table.act td
      {
         border-width: 1px;
         padding: 1px;
         border-color: #C0C0C0;
      }
   </style>
</head>
<body onbeforeunload="Unload();">
   <form id="form1" runat="server">
   <div id="wrapper">
      <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
      </asp:ToolkitScriptManager>
      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
            <asp:HiddenField ID="KPIID" runat="server" Value="[AUTO]" />
            <table border="1" class="act">
               <tr>
                  <td colspan="4">
                     <span style="font-style: italic; color: red;">* : Harus Diisi</span>
                  </td>
               </tr>
               <tr>
                  <td colspan="4" class="styleHeader">
                     KPI Definition
                  </td>
               </tr>
               <tr>
                  <td>
                     Tahun KPI
                  </td>
                  <td colspan="3">
                     <asp:DropDownList ID="KPIYear" runat="server" CssClass="plains" Enabled="false">
                        <asp:ListItem Value="2012" Text="2012"></asp:ListItem>
                        <asp:ListItem Value="2013" Text="2013"></asp:ListItem>
                        <asp:ListItem Value="2014" Text="2014"></asp:ListItem>
                        <asp:ListItem Value="2015" Text="2015"></asp:ListItem>
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td style="width: 25%">
                     KPI Level
                  </td>
                  <td colspan="3">
                     <asp:DropDownList ID="KPILevel" runat="server" AutoPostBack="true" CssClass="plains">
                        <asp:ListItem Value="0" Text="BUMA"></asp:ListItem>
                        <asp:ListItem Value="4" Text="Direktorat"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Function" Enabled="false"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Section"></asp:ListItem>
                        <asp:ListItem Value="3" Text="Jobsite"></asp:ListItem>
                     </asp:DropDownList>
                     <asp:DropDownList ID="KPIFunc" runat="server" CssClass="plains">
                     </asp:DropDownList>
                     <asp:DropDownList ID="KPISection" runat="server" CssClass="plains">
                     </asp:DropDownList>
                     <div id="mydirektorat" runat="Server">
                        <input type="checkbox" runat="server" id="cKPIDirektorat" />Check / Uncheck All
                        <asp:CheckBoxList ID="KPIDirektorat" runat="server" RepeatDirection="Horizontal"
                           CssClass="dir">
                        </asp:CheckBoxList>
                     </div>
                     <div id="myfunction" runat="Server">
                        <input type="checkbox" runat="server" id="cKPIFuncLevel0" />Check / Uncheck All
                        <asp:CheckBoxList ID="KPIFuncLevel0" runat="server" RepeatDirection="Vertical"
                           CssClass="func">
                        </asp:CheckBoxList>
                     </div>
                     <div id="myjobsite" runat="Server">
                        <input type="checkbox" runat="server" id="cKPIJobsite" />Check / Uncheck All
                        <asp:CheckBoxList ID="KPIJobsite" runat="server" RepeatDirection="Horizontal" CssClass="site">
                        </asp:CheckBoxList>
                     </div>
                  </td>
               </tr>
               <tr>
                  <td>
                     SO
                  </td>
                  <td colspan="3">
                     <asp:DropDownList ID="SO" runat="server" CssClass="plains">
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td>
                     KPI Name
                     <img alt="Paste From Parent" style="cursor: hand" src="../images/paste.gif" onclick="doPaste('KPIName');" />
                  </td>
                  <td>
                     <asp:TextBox ID="KPIName" runat="server" MaxLength="100" Columns="70" CssClass="plain"></asp:TextBox><img
                        alt="required" src="../images/required_star.png" />
                  </td>
               </tr>
               <tr>
                  <td>
                     Bobot
                  </td>
                  <td colspan="3">
                     <asp:TextBox ID="KPIBobot" runat="server" MaxLength="10" CssClass="plain"></asp:TextBox><img
                        alt="required" src="../images/required_star.png" />
                  </td>
               </tr>
               <tr>
                  <td>
                     Ready To Measure ?
                  </td>
                  <td colspan="3">
                     <asp:DropDownList ID="KPIReady" runat="server" CssClass="plains">
                        <asp:ListItem Text="Yes" Selected="True" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Not Yet" Value="1"></asp:ListItem>
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td>
                     UoM
                     <img alt="Paste From Parent" style="cursor: hand" src="../images/paste.gif" onclick="doPaste('KPIUoM');" />
                  </td>
                  <td>
                     <asp:TextBox ID="KPIUOM" runat="server" MaxLength="10" CssClass="plain"></asp:TextBox><img
                        alt="required" src="../images/required_star.png" />
                  </td>
               </tr>
               <tr>
                  <td>
                     KPI Owner
                  </td>
                  <td>
                     <asp:TextBox ID="KPIOwner" runat="server" MaxLength="100" Columns="70" CssClass="plain"></asp:TextBox><img
                        alt="required" src="../images/required_star.png" />
                  </td>
                  <%--<td>KPI Type</td>
                  <td>
                    <asp:DropDownList ID="KPIType" runat="server">
                        <asp:ListItem Text="Lag" Selected="True" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Lead" Value="1"></asp:ListItem>
                     </asp:DropDownList>
                  </td>--%>
               </tr>
               <tr>
                  <td valign="top">
                     KPI Description
                     <img alt="Paste From Parent" style="cursor: hand" src="../images/paste.gif" onclick="doPaste('KPIDesc');" />
                  </td>
                  <td colspan="3">
                     <asp:TextBox ID="KPIDesc" runat="server" TextMode="MultiLine" MaxLength="300" Rows="5"
                        Columns="50"></asp:TextBox><img alt="required" src="../images/required_star.png" />
                  </td>
               </tr>
               <tr>
                  <td>
                     Period Of Measurement
                  </td>
                  <td colspan="3">
                     <asp:DropDownList ID="KPIPeriod" runat="server" CssClass="plains">
                        <%-- <asp:ListItem Text="Daily" Selected="True" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Weekly" Value="1"></asp:ListItem>--%>
                        <asp:ListItem Text="Monthly" Value="2" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Quarterly" Value="3"></asp:ListItem>
                        <asp:ListItem Text="Semester" Value="4"></asp:ListItem>
                        <asp:ListItem Text="Yearly" Value="5"></asp:ListItem>
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td>
                     Type Of KPI
                  </td>
                  <td colspan="3">
                     <asp:DropDownList ID="KPIType" runat="server" CssClass="plains">
                        <asp:ListItem Text="Lag" Selected="True" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Lead" Value="1"></asp:ListItem>
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td>
                     KPI Leads to
                     <img alt="Paste From Parent" style="cursor: hand" src="../images/paste.gif" onclick="doPaste('KPILeads');" />
                  </td>
                  <td colspan="3">
                     <asp:TextBox ID="KPILeads" runat="server" TextMode="MultiLine" MaxLength="10" Rows="3"
                        Columns="50" CssClass="plain"></asp:TextBox>
                  </td>
               </tr>
               <tr>
                  <td colspan="4" class="styleHeader">
                     KPI Formulation
                  </td>
               </tr>
               <tr>
                  <td style="vertical-align: top">
                     Formula
                     <img alt="Paste From Parent" style="cursor: hand" src="../images/paste.gif" onclick="doPaste('KPIFDesc');" />
                  </td>
                  <td colspan="3">
                     <asp:TextBox ID="KPIFDesc" runat="server" TextMode="MultiLine" MaxLength="500" Rows="6"
                        Columns="50" CssClass="plain"></asp:TextBox>
                  </td>
               </tr>
               <tr>
                  <td colspan="4" class="styleHeader">
                     KPI Target Setting
                  </td>
               </tr>
               <tr>
                  <td valign="top">
                     Process To Setting Target
                     <img alt="Paste From Parent" style="cursor: hand" src="../images/paste.gif" onclick="doPaste('KPIProcess');" />
                  </td>
                  <td colspan="3">
                     <asp:TextBox ID="KPIProcess" runat="server" TextMode="MultiLine" MaxLength="10" Rows="5"
                        Columns="50" CssClass="plain"></asp:TextBox>
                  </td>
               </tr>
               <tr>
                  <td valign="top">
                     Targetting Behaviour
                  </td>
                  <td colspan="3">
                     <asp:DropDownList ID="KPITargetting" runat="server" CssClass="plains">
                        <asp:ListItem Text="Flat Target" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Moving Target" Value="1"></asp:ListItem>
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td colspan="4" class="styleHeader">
                     KPI Parameter
                  </td>
               </tr>
               <tr>
                  <td colspan="4">
                     <table>
                        <tr>
                           <td>
                              Based On
                           </td>
                           <td>
                              <asp:DropDownList ID="KPIBased" runat="server" CssClass="plains">
                                 <asp:ListItem Value="0" Text="Achievement" Selected="true"></asp:ListItem>
                                 <%--<asp:ListItem value="1" Text="Value"></asp:ListItem>--%>
                              </asp:DropDownList>
                           </td>
                        </tr>
                        <tr>
                           <td valign="top">
                              Calculation Type
                           </td>
                           <td valign="top">
                              <asp:DropDownList ID="KPICalcTypeH" runat="server" CssClass="plains" onchange="onchangekpicalch();">
                                 <asp:ListItem Value="0" Text="CONTINOUS"></asp:ListItem>
                                 <asp:ListItem Value="1" Text="DISCRETE"></asp:ListItem>
                              </asp:DropDownList>
                              <asp:DropDownList ID="KPICalcTypeD" runat="server" CssClass="plains">
                                 <asp:ListItem Value="0" Text="MIN"></asp:ListItem>
                                 <asp:ListItem Value="1" Text="MAX" Selected="true"></asp:ListItem>
                              </asp:DropDownList>
                              <asp:DropDownList ID="KPICalcTypeC" runat="server" CssClass="plains">
                                 <asp:ListItem Value="2" Text="MIN"></asp:ListItem>
                                 <asp:ListItem Value="4" Text="MAX"></asp:ListItem>
                                 <asp:ListItem Value="3" Text="STABILIZE"></asp:ListItem>
                              </asp:DropDownList>
                              <input type="button" id="KPICluster" value="Set Data" onclick="openpopupcluster();"
                                 class="btn" />
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td colspan="4">
                     <table>
                        <tr style="background-color: red">
                           <td>
                              Red
                           </td>
                           <td>
                              <asp:DropDownList ID="RedParam" runat="server" CssClass="plains">
                                 <asp:ListItem Value="0" Text="&gt;"></asp:ListItem>
                                 <asp:ListItem Value="1" Text="&ge;"></asp:ListItem>
                                 <asp:ListItem Value="2" Text="&lt;" Selected="true"></asp:ListItem>
                                 <asp:ListItem Value="3" Text="&le;"></asp:ListItem>
                              </asp:DropDownList>
                           </td>
                           <td>
                              <asp:TextBox ID="KPIRed" CssClass="plain" runat="server" onkeypress="fncInputNumericValuesOnly();"
                                 Text="90"></asp:TextBox><img alt="required" src="../images/required_star.png" />
                           </td>
                        </tr>
                        <tr style="background-color: yellow">
                           <td>
                              Yellow
                           </td>
                           <td colspan="2" align="center">
                              BETWEEN
                           </td>
                           <tr style="background-color: green">
                              <td>
                                 Green
                              </td>
                              <td>
                                 <asp:DropDownList ID="GreenParam" runat="server" CssClass="plains">
                                    <asp:ListItem Value="0" Text="&gt;" Selected="true"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="&ge;"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="&lt;"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="&le;"></asp:ListItem>
                                 </asp:DropDownList>
                              </td>
                              <td>
                                 <asp:TextBox ID="KPIGreen" CssClass="plain" runat="server" onkeypress="fncInputNumericValuesOnly();"
                                    Text="95"></asp:TextBox><img alt="required" src="../images/required_star.png" />
                              </td>
                           </tr>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td colspan="4" align="right">
                     <asp:Button ID="btnSave" runat="server" Width="55px" Height="55px" Style="background-image: url(../images/save-icon-small.png);
                        background-repeat: no-repeat; cursor: hand; vertical-align: bottom" BackColor="Transparent"
                        BorderStyle="None" />
                  </td>
               </tr>
            </table>
         </ContentTemplate>
      </asp:UpdatePanel>
   </div>
   </form>

   <script type="text/javascript">
      $("#cKPIFuncLevel0").click(function() {
         $(".func [type='checkbox']").attr('checked', $('#cKPIFuncLevel0').is(':checked'));
      });

      $("#cKPIJobsite").click(function() {
         $(".site [type='checkbox']").attr('checked', $('#cKPIJobsite').is(':checked'));
      });

      $("#cKPIDirektorat").click(function() {
         $(".dir [type='checkbox']").attr('checked', $('#cKPIDirektorat').is(':checked'));
      });
      onchangekpicalch();
   </script>

</body>
</html>
