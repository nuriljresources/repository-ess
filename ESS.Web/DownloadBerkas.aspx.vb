﻿Imports System.Data.SqlClient
Imports System.IO

Partial Public Class DownloadBerkas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("otorisasi") = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LinkButton1.Click
        Try
            Dim conn As SqlConnection
            Dim cmd As SqlCommand

            Dim imgByte As Byte() = Nothing
            Dim sidrap As String = Me.Id1.Value
            Dim sidiss As String = Me.Id2.Value
            Dim sidact As String = Me.Id3.Value
            Dim nofile As String = Me.Nomor.Value

            Dim sekuel As String = ""
            sekuel = "SELECT NmFile, MimeType, MimeData, MimeLength FROM TRLAMPIRAN WHERE Nomor = " + nofile + " AND IdRap = '" + sidrap + "' AND IdIssue = '" + sidiss + "' AND NoAction = '" + sidact + "'"

            conn = New SqlConnection(ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString)
            cmd = New SqlCommand(sekuel, conn)
            conn.Open()
            Dim sdr As SqlDataReader = cmd.ExecuteReader()

            Dim imgType As String = ""
            Dim nmFile As String = ""
            Dim imgLen As Integer
            While sdr.Read()
                imgLen = sdr("MimeLength")
                imgType = sdr("MimeType")
                nmFile = sdr("NmFile")
                imgByte = sdr("MimeData")
            End While
            conn.Close()

            nmFile = nmFile.Replace(" ", "")

            Response.AddHeader("Content-Disposition", "attachment; filename=" + nmFile)
            Dim bw As BinaryWriter = New BinaryWriter(Response.OutputStream)
            bw.Write(imgByte)
            bw.Close()
            Response.ContentType = imgType
            Response.End()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class