﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="edws.aspx.vb" Inherits="EXCELLENT.edws" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Frames List 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="No" OnRowCancelingEdit="GridView1_RowCancelingEdit"
       OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" OnRowCommand="GridView1_RowCommand"
       ShowFooter="True" OnRowDeleting="GridView1_RowDeleting" CssClass="data-table" ShowHeader = "True" >
       <Columns>
           <asp:TemplateField HeaderText="No">
               <EditItemTemplate>
               <asp:Label ID="Label4" runat="server" Text=''></asp:Label>
               </EditItemTemplate>
               <FooterTemplate>
                   <asp:Label ID="Label5" runat="server" Text=''></asp:Label>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label6" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Nik" HeaderStyle-Width="100px" >
               <EditItemTemplate>
               <div style="width:130px">
                   <asp:HiddenField ID="hnik" runat="server" Value='' />
                   <asp:TextBox ID="txtnik" runat="server" Width="100px" Text='' Font-Size="1.0em" onkeypress="return readonly(event)"></asp:TextBox>
               </div> 
               </EditItemTemplate>
               <FooterTemplate>
               <div style="width:130px">
                   <asp:HiddenField ID="hnewnik" runat="server" />
                   <asp:TextBox ID="txtNewNik" Width="100px" runat="server" Font-Size="1.0em" onkeypress="return readonly(event)"></asp:TextBox>
               </div>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label3" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>         
           <asp:TemplateField HeaderText="Name" HeaderStyle-Width="100px" >
               <EditItemTemplate>
               <div style="width:130px">
                   <asp:HiddenField ID="hperson" runat="server" Value='' />
                   <asp:TextBox ID="txtNama" runat="server" Width="100px" Text='' Font-Size="1.0em" onkeypress="return readonly(event)"></asp:TextBox>
               </div> 
               </EditItemTemplate>
               <FooterTemplate>
               <div style="width:130px">
                   <asp:HiddenField ID="hnewperson" runat="server" />
                   <asp:TextBox ID="txtNewNama" Width="100px" runat="server" Font-Size="1.0em" onkeypress="return readonly(event)"></asp:TextBox>
               </div>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label3" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Position" Visible="True" >
                <EditItemTemplate>
                    <asp:HiddenField ID="hkdjabat" runat = "server" />
                    <asp:TextBox ID="txtjabat" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnewjabat" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label30" Width="60px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="16" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt16" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew16" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label31" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="17" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt17" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew17" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label32" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="18" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt18" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew18" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label33" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="19" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt19" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew19" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label34" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="20" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt20" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew20" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label35" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="21" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt21" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew21" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label36" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="22" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt22" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew22" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label37" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="23" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt23" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew23" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label38" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="24" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt24" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew24" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label39" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="25" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt25" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew25" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label40" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="26" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt26" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew26" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label41" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="27" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt27" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew27" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label42" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="28" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt28" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew28" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label43" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="29" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt29" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew29" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label44" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="30" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt30" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew30" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label45" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="31" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt31" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew31" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label46" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="1" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt1" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew1" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label47" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="2" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt2" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew2" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label48" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="3" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt3" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew3" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label49" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="4" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt4" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew4" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label50" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="5" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt5" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew5" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label51" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="6" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt6" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew6" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label52" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="7" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt7" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew7" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label53" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="8" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt8" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew8" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label54" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="9" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt9" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew9" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label55" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="10" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt10" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew10" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label56" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
            <asp:TemplateField HeaderText="11" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt11" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew11" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label57" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="12" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt12" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew12" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label58" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="13" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt13" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew13" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label59" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="14" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt14" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew14" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label60" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="15" Visible="True" >
                <EditItemTemplate>
                    <asp:TextBox ID="txt15" Width="20px" runat="server" ></asp:TextBox>
                </EditItemTemplate>
                <FooterTemplate>
                   <asp:TextBox ID="txtnew15" Width="20px" runat="server" ></asp:TextBox>
               </FooterTemplate>
               <ItemTemplate>
                   <asp:Label ID="Label61" Width="20px" runat="server" Text=''></asp:Label>
               </ItemTemplate>
           </asp:TemplateField>
           
           
           <asp:TemplateField HeaderText="" ShowHeader="False" FooterStyle-BorderStyle="None" HeaderStyle-BorderStyle="None" ItemStyle-BorderStyle="None">
               <EditItemTemplate>
                <div style="width:85px;">
                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="Update">
                   </asp:LinkButton>
                   <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel">
                   </asp:LinkButton>
               </div>
               </EditItemTemplate>
               <FooterTemplate>
               <div style="width:30px;">
                   <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="AddNew" Text="Save"></asp:LinkButton>
                </div> 
               </FooterTemplate>
               <ItemTemplate>
               
                   <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit"></asp:LinkButton>
               
               </ItemTemplate>
           </asp:TemplateField>
           
                <asp:CommandField HeaderText="" HeaderStyle-BorderStyle="None" ShowDeleteButton="True" ShowHeader="False" ItemStyle-BorderStyle="None" FooterStyle-BorderStyle="None"/>
           
       </Columns>
        <FooterStyle Width="300px" />
   </asp:GridView>
</asp:Content>
