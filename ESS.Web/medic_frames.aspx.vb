﻿Imports System.Data.SqlClient
Partial Public Class medic_frames
    Inherits System.Web.UI.Page
    Public js As String
    Public myDt As DataTable
    Public benefit As String
    Public msg As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnPaymentUsage.Visible = ESS.Common.SITE_SETTINGS.PRINT_PAYMENT_APP

        Dim qryheader, sqledit As String
        Dim lddate As Date
        Dim dt As DataTable
        Dim dtb As DataTable = New DataTable()
        'Dim sqleditheader, sqleditdetail, sqlfindnama, terbilang As String
        Dim dtbeditmedic As DataTable = New DataTable()
        Dim dtbeditmedicd As DataTable = New DataTable()
        Dim dtbfindc As DataTable = New DataTable()
        'Dim lscount, ltotal As Integer

        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

        If Session("niksite") = Nothing Or Session("niksite") = "" Then Response.Redirect("login.aspx")
        msg = ""

        If Not IsPostBack Then
            If Session("framenoregst").ToString = "EditFrame" Then
                sqledit = "select *, (select nama from H_A101 where nik = H_H21502.nik) as nama,(select nmsite from H_A120 where KdSite = H_H21502.kdsite) as nmsite, (select niksite from H_A101 where nik=H_H21502.nik) as niksite, (select nmjabat from H_A150 where kdjabat = (select kdjabatan from H_A101 where nik = H_H21502.nik)) as nmjabat, (select kdjabatan from H_A101 where nik = H_H21502.nik) as kdjabat, " & _
                " fstatus, claim, claimframe from H_H21502 where noreg = '" + Session("framenoreg") + "'"
                Dim sdaeditheader As SqlDataAdapter = New SqlDataAdapter(sqledit, sqlConn)
                sdaeditheader.Fill(dtbeditmedic)
                If dtbeditmedic.Rows.Count > 0 Then
                    txtnoreg.Text = dtbeditmedic.Rows(0)!noreg.ToString
                    txtnmsite.Text = dtbeditmedic.Rows(0)!nmsite.ToString
                    hnmsite.Value = dtbeditmedic.Rows(0)!kdsite.ToString
                    txtnik.Text = dtbeditmedic.Rows(0)!niksite.ToString
                    hnik.Value = dtbeditmedic.Rows(0)!nik.ToString
                    txtlevel.Text = dtbeditmedic.Rows(0)!nmjabat.ToString
                    hkdlevel.Value = dtbeditmedic.Rows(0)!kdjabat.ToString
                    lddate = dtbeditmedic.Rows(0)!TglTrans.ToString
                    txtcostcode.Text = dtbeditmedic.Rows(0)!pycostcode.ToString
                    txtnama.Text = dtbeditmedic.Rows(0)!nama.ToString
                    txtremarks.Text = dtbeditmedic.Rows(0)!keterangan.ToString
                    'txtoutpatientmax.Text = Format(dtbeditmedic.Rows(0)!InapMax, "#,##.00").ToString
                    txtnoreg.Text = dtbeditmedic.Rows(0)!NoReg.ToString
                    txtlens.Text = Format(dtbeditmedic.Rows(0)!claim, "#").ToString
                    txtframe.Text = Format(dtbeditmedic.Rows(0)!claimframe, "#").ToString
                    txtdate.Text = lddate.ToString("MM/dd/yyyy")
                    If txtlens.Text = "" Then txtlens.Text = 0
                    If txtframe.Text = "" Then txtframe.Text = 0
                    txttotal.Text = CDbl(txtlens.Text) + CDbl(txtframe.Text)

                    If dtbeditmedic.Rows(0)!fstatus = 1 Then
                        btnsave.Enabled = False
                        btndelete.Enabled = False
                    End If
                End If
            End If

            If Session("framenoregst").ToString = "" Then
                qryheader = "select nik, KdSite, (select nmsite from H_A120 where KdSite = H_A101.kdsite) as nmsite,pycostcode,NIKSITE, Nama,KdJabatan, (select nmjabat from H_A150 where kdjabat = H_A101.KdJabatan) as nmjabat " & _
                            "from H_A101 where nik = '" + Session("niksite") + "' and( H_A101.Active = '1' ) AND ( H_A101.StEdit <> '2' )"
                Dim sda As SqlDataAdapter = New SqlDataAdapter(qryheader, sqlConn)
                sda.Fill(dtb)

                If dtb.Rows.Count > 0 Then
                    txtnmsite.Text = dtb.Rows(0)!nmsite.ToString
                    hnmsite.Value = dtb.Rows(0)!KdSite.ToString
                    txtcostcode.Text = dtb.Rows(0)!pycostcode.ToString
                    txtnik.Text = dtb.Rows(0)!NIKSITE.ToString
                    hnik.Value = dtb.Rows(0)!nik.ToString
                    txtnama.Text = dtb.Rows(0)!Nama.ToString
                    txtlevel.Text = dtb.Rows(0)!nmjabat.ToString
                    hkdlevel.Value = dtb.Rows(0)!KdJabatan.ToString
                    'txtoutpatientmax.Text = Format(dtb.Rows(0)!InapMax, "#,##.00")
                End If
            End If

            Session("framenoreg") = ""
            Session("framenoregst") = ""

            Dim sqlfindc As String = "select H_A21501.Kode, H_A21501.Amount3, H_A21501.Amount4, H_A21501.Amount5, H_A21501.TipeOutpatient FROM H_A21501 WHERE ( H_A21501.KdSite in ( '" + hnmsite.Value + "') ) AND ( H_A21501.TipeOutpatient = 2 ) AND ( H_A21501.StEdit <> '2' )"
            Dim sqlda As SqlDataAdapter = New SqlDataAdapter(sqlfindc, sqlConn)
            sqlda.Fill(dtbfindc)
            If dtbfindc.Rows.Count > 0 Then
                hamount1.Value = dtbfindc.Rows(0)!Amount3.ToString
                hamount2.Value = dtbfindc.Rows(0)!Amount4.ToString
                hamount3.Value = dtbfindc.Rows(0)!Amount5.ToString
                benefit = "<br> First Use IDR " + CDbl(hamount3.Value).ToString("#,##.00") + "<br> Lens Min 1 Year IDR " + CDbl(hamount1.Value).ToString("#,##.00") + "<br> Frame Min 2 Year IDR " + CDbl(hamount2.Value).ToString("#,##.00")
            End If

            'end postback
        End If
        'txtdate.Text = Today.Date.ToString("MM/dd/yyyy")
        'msg = "<script type='text/javascript'> alert('" + Session("kdsite") + "'); </script>"
    End Sub

    Private Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim qrytgl, sqlupdate, qrycekdata, createdin, tglmasuk As String
        Dim dtb As DataTable = New DataTable()
        Dim dtbcekdata As DataTable = New DataTable()
        Dim dtbcekamount As DataTable = New DataTable()
        Dim dtbcekemp As DataTable = New DataTable()
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Dim cmdupdate As SqlCommand
        Dim datenumberd As Double
        Dim liBlnSlisih As Double
        Dim jml As Double
        Dim tgltrans As String

        createdin = System.Net.Dns.GetHostName().ToString

        If txtlens.Text = "" Then txtlens.Text = 0
        If txtframe.Text = "" Then txtframe.Text = 0
        txttotal.Text = CDbl(txtframe.Text) + CDbl(txtlens.Text)

        Dim sqlcekemp As String = "select TglMasuk from H_A101 where nik = '" + hnik.Value + "' and Active = 1 and StEdit <> 2"
        Dim dtacekemp As SqlDataAdapter = New SqlDataAdapter(sqlcekemp, sqlConn)
        dtacekemp.Fill(dtbcekemp)

        If dtbcekemp.Rows.Count > 0 Then
            tglmasuk = dtbcekemp.Rows(0)!TglMasuk.ToString
            datenumberd = DateDiff("d", tglmasuk, txtdate.Text)
            datenumberd = datenumberd / 30
            liBlnSlisih = Math.Round(datenumberd, 2)
        End If

        If tglmasuk = "" Then tglmasuk = "1/1/1900"

        If liBlnSlisih < 12 Then
            If Trim(Session("kdsite").ToString) <> "LAN" And Trim(Session("kdsite").ToString) <> "BAK" And Trim(Session("kdsite").ToString) <> "PAN" And Trim(Session("kdsite").ToString) <> "DOU" Then
                msg = "<script type='text/javascript'> alert('Save Failed : You can not claim'); </script>"
                Return
                'ElseIf Trim(Session("kdsite").ToString) = "LAN" Or Trim(Session("kdsite").ToString) = "BAK" Or Trim(Session("kdsite").ToString) = "PAN" Or Trim(Session("kdsite").ToString) = "DOU" Then
                '    If liBlnSlisih < 6 Then
                '        msg = "<script type='text/javascript'> alert('Save Failed : You can not claim'); </script>"
                '        Return
                '    End If
            End If
        End If


        Dim dtEmpIn As DateTime = CDate(tglmasuk)
        Dim dtClaim As DateTime = CDate(txtdate.Text)

        Dim valLens As Integer = CInt(txtlens.Text)
        Dim valFrame As Integer = CInt(txtframe.Text)
        Me.lblinfo.Text = ""

        If (Trim(Session("kdsite").ToString) = "JKT") Then
            If (dtClaim.Subtract(dtEmpIn).TotalDays <= 365) Then
                msg = "<script type='text/javascript'> alert('Save Failed : You can not claim lens & frame right now'); </script>"
                Return
            End If
        End If

        Dim sqlClaimData As String = <![CDATA[
                    SELECT 
                    (SELECT TglTrans FROM ( SELECT ROW_NUMBER() OVER(PARTITION BY Nik ORDER BY TglTrans) RW, TglTrans FROM H_H21502 WHERE Nik = '@NIK' AND fstatus = 1 AND StEdit <> 2 ) XXX WHERE RW = 1) AS FirstClaim,
                    MAX(LastClaimLensa) AS LastClaimLensa,
                    MAX(LastClaimFrame) AS LastClaimFrame
                    FROM (
	                    SELECT
		                    CASE WHEN Claim > 0 THEN TglTrans ELSE NULL END LastClaimLensa,
		                    CASE WHEN ClaimFrame > 0 THEN TglTrans ELSE NULL END LastClaimFrame
	                    FROM H_H21502 WHERE Nik = '@NIK' AND fstatus = 1 AND StEdit <> 2
                    ) XXX
                ]]>.Value

        sqlClaimData = sqlClaimData.Replace("@NIK", hnik.Value)

        Dim adapterClaimData As New SqlDataAdapter(sqlClaimData, sqlConn)
        Dim dtClaimData As New DataTable
        adapterClaimData.Fill(dtClaimData)
        Dim rowClaimData As System.Data.DataRow = dtClaimData.Rows(0)


        If (valFrame > 0) Then
            If Not String.IsNullOrEmpty(rowClaimData!LastClaimFrame.ToString) Then
                If dtClaim.Subtract(CDate(rowClaimData!LastClaimFrame)).Days < 730 Then
                    Me.msg = "<script type='text/javascript'> alert('Save Failed : Frames can only be claimed after 2 years from the last claim'); </script>"
                    Return
                End If
            End If

            If (Trim(Session("kdsite").ToString) = "JKT") Then
                If CDec(valFrame) > CDec(hamount2.Value) Then
                    Me.msg = ("<script type='text/javascript'> alert('Information : Frame cannot over " & CDec(hamount2.Value).ToString("#,##.00") & "'); </script>")
                    Me.lblinfo.Text = ("Information : Frame cannot over " & CDec(hamount2.Value).ToString("#,##.00") & "")
                    Return
                End If
            Else
                If CDec(valFrame) > CDec(hamount3.Value) Then
                    Me.msg = ("<script type='text/javascript'> alert('Information : Frame cannot over " & CDec(hamount3.Value).ToString("#,##.00") & "'); </script>")
                    Me.lblinfo.Text = ("Information : Frame cannot over " & CDec(hamount3.Value).ToString("#,##.00") & "")
                    Return
                End If
            End If
        End If


        If (valLens > 0) Then
            If Not String.IsNullOrEmpty(rowClaimData!LastClaimLensa.ToString) Then
                If dtClaim.Subtract(CDate(rowClaimData!LastClaimLensa)).Days < 365 Then
                    Me.msg = "<script type='text/javascript'> alert('Save Failed : Lens have already been claimed this year'); </script>"
                    Return
                End If
            End If
            If CDec(valLens) > CDec(hamount1.Value) Then
                Me.msg = ("<script type='text/javascript'> alert('Information : Lens cannot over " & CDec(Me.hamount1.Value).ToString("#,##.00") & "'); </script>")
                Me.lblinfo.Text = ("Information : Lens cannot over " & CDec(Me.hamount1.Value).ToString("#,##.00") & "")
                Return
            End If
        End If



        'qrycekdata = "select COUNT(*) as number from H_H21502 where nik = '" + hnik.Value + "' and StEdit <> 2 and NoReg <> '" + txtnoreg.Text + "' and fstatus = 1"
        'Dim sqldacekdata As SqlDataAdapter = New SqlDataAdapter(qrycekdata, sqlConn)
        'sqldacekdata.Fill(dtbcekdata)

        'If Trim(Session("kdsite").ToString) <> "LAN" And Trim(Session("kdsite").ToString) <> "BAK" And Trim(Session("kdsite").ToString) <> "PAN" And Trim(Session("kdsite").ToString) <> "DOU" Then
        '    If dtbcekdata.Rows.Count > 0 Then
        '        jml = dtbcekdata.Rows(0)!number.ToString
        '        If txtframe.Text <> 0 And txtlens.Text = 0 Then
        '            If liBlnSlisih <= ((jml * 2) + 1) * 12 Then
        '                msg = "<script type='text/javascript'> alert('Save Failed : You can not claim frame right now'); </script>"
        '                Return
        '            End If
        '        ElseIf txtframe.Text <> 0 And txtlens.Text <> 0 Then
        '            If liBlnSlisih <= ((jml * 2) + 1) * 12 Then
        '                msg = "<script type='text/javascript'> alert('Save Failed : You can not claim frame right now'); </script>"
        '                Return
        '            End If
        '        Else
        '            If liBlnSlisih <= ((jml * 1) + 1) * 12 Then
        '                msg = "<script type='text/javascript'> alert('Save Failed : You can not claim frame right now'); </script>"
        '                Return
        '            End If
        '        End If
        '    End If
        'End If

        If CDbl(txtframe.Text) < 0 Then
            msg = "<script type='text/javascript'> alert('Save Failed : Nominal claim value must not minus'); </script>"
            Return
        End If

        If CDbl(txtframe.Text) = 0 And CDbl(txtlens.Text) = 0 Then
            msg = "<script type='text/javascript'> alert('Save Failed : Please complete data entry'); </script>"
            Return
        End If

        If txttotal.Text = 0 Then
            msg = "<script type='text/javascript'> alert('Save Failed : Total Nominal cannot zero'); </script>"
            Return
        End If

        If CDate(txtdate.Text) > Date.Today Then
            msg = "<script type='text/javascript'> alert('Save Failed : Date over today date'); </script>"
            Return
        End If


        If txtnoreg.Text <> "Auto" Then

            sqlConn.Open()

            'cek quota, sudah lewat satu kali atau belum
            Dim sqlcekquotaedit As String = "select noreg from H_H21502 where nik = '" + hnik.Value + "' and StEdit <> 2 and noreg <> '" + txtnoreg.Text + "' and fstatus = 1"
            Dim dtaedit As SqlDataAdapter = New SqlDataAdapter(sqlcekquotaedit, sqlConn)
            dtaedit.Fill(dtbcekamount)

            If Trim(Session("kdsite").ToString) = "JKT" Then
                If dtbcekamount.Rows.Count > 0 Then
                    If CDbl(txtlens.Text) > CDbl(hamount1.Value) Then
                        msg = "<script type='text/javascript'> alert('Information : Lens cannot over " + CDbl(hamount1.Value).ToString("#,##.00") + "'); </script>"
                        lblinfo.Text = "Information : Lens cannot over " + CDbl(hamount1.Value).ToString("#,##.00") + ""
                        hprint.Value = "1"
                        'Return
                    End If

                    If CDbl(txtframe.Text) > CDbl(hamount2.Value) Then
                        msg = "<script type='text/javascript'> alert('Information : Frame cannot over " + CDbl(hamount2.Value).ToString("#,##.00") + "'); </script>"
                        lblinfo.Text = "Information : Frame cannot over " + CDbl(hamount2.Value).ToString("#,##.00") + ""
                        hprint.Value = "1"
                        'Return
                    End If
                Else
                    If CDbl(txttotal.Text) > CDbl(hamount3.Value) Then
                        msg = "<script type='text/javascript'> alert('Information : Total Amount cannot over " + CDbl(hamount3.Value).ToString("#,##.00") + "'); </script>"
                        lblinfo.Text = "Information : Total Amount cannot over " + CDbl(hamount3.Value).ToString("#,##.00") + ""
                        hprint.Value = "1"
                        'Return
                    End If
                End If
            Else
                'tambahan untuk setiap site 5 Feb 2016
                If CDbl(txtlens.Text) > CDbl(hamount1.Value) Then
                    msg = "<script type='text/javascript'> alert('Information : Lens cannot over " + CDbl(hamount1.Value).ToString("#,##.00") + "'); </script>"
                    lblinfo.Text = "Information : Lens cannot over " + CDbl(hamount1.Value).ToString("#,##.00") + ""
                    hprint.Value = "1"
                    'Return
                End If

                If CDbl(txtframe.Text) > CDbl(hamount3.Value) Then
                    msg = "<script type='text/javascript'> alert('Information : Frame cannot over " + CDbl(hamount3.Value).ToString("#,##.00") + "'); </script>"
                    lblinfo.Text = "Information : Frame cannot over " + CDbl(hamount3.Value).ToString("#,##.00") + ""
                    hprint.Value = "1"
                    'Return
                End If
            End If

            sqlupdate = "Update H_H21502 set Kdsite = '" + hnmsite.Value + "', TglTrans = '" + txtdate.Text + "', nik = '" + hnik.Value + "', Claim = '" + txtlens.Text + "', ClaimFrame = '" + txtframe.Text + "', Keterangan = '" + txtremarks.Text + "', Modifiedby = '" + Session("otorisasi").ToString + "',ModifiedIn = '" + createdin + "', ModifiedTime = '" + DateTime.Today.ToString + "', pycostcode = '" + txtcostcode.Text + "', fprint = '" + hprint.Value + "' where noreg = '" + txtnoreg.Text + "'"
            cmdupdate = New SqlCommand(sqlupdate, sqlConn)
            cmdupdate.ExecuteScalar()

            If hprint.Value = "1" Then
                btnprint.Enabled = False
                btnPaymentUsage.Enabled = False
            Else
                btnprint.Enabled = True
                If Session("kdsite") = "JKT" Then
                    btnPaymentUsage.Enabled = True
                End If
                msg = "<script type='text/javascript'> alert('Data has been update'); </script>"
            End If

        Else
            If txtnoreg.Text = "Auto" Then

                Dim dtmnow, dtynow, stransno, transno As String

                sqlConn.Open()

                'cek quota, sudah lewat satu kali atau belum
                Dim sqlcekquota As String = "select noreg from H_H21502 where nik = '" + hnik.Value + "' and StEdit <> 2 and fstatus = 1"
                Dim dta As SqlDataAdapter = New SqlDataAdapter(sqlcekquota, sqlConn)
                dta.Fill(dtbcekamount)

                If Trim(Session("kdsite").ToString) = "JKT" Then
                    If dtbcekamount.Rows.Count > 0 Then
                        If CDbl(txtlens.Text) > CDbl(hamount1.Value) Then
                            msg = "<script type='text/javascript'> alert('Information : Lens cannot over " + CDbl(hamount1.Value).ToString("#,##.00") + "'); </script>"
                            lblinfo.Text = "Information : Lens cannot over " + CDbl(hamount1.Value).ToString("#,##.00") + ""
                            hprint.Value = "1"
                            'Return
                        End If

                        If CDbl(txtframe.Text) > CDbl(hamount2.Value) Then
                            msg = "<script type='text/javascript'> alert('Information : Frame cannot over " + CDbl(hamount2.Value).ToString("#,##.00") + "'); </script>"
                            lblinfo.Text = "Information : Frame cannot over " + CDbl(hamount2.Value).ToString("#,##.00") + ""
                            hprint.Value = "1"
                            'Return
                        End If
                    Else
                        If CDbl(txttotal.Text) > CDbl(hamount3.Value) Then
                            msg = "<script type='text/javascript'> alert('Information : Total Amount cannot over " + CDbl(hamount3.Value).ToString("#,##.00") + "'); </script>"
                            lblinfo.Text = "Information : Total Amount cannot over " + CDbl(hamount3.Value).ToString("#,##.00") + ""
                            hprint.Value = "1"
                            'Return
                        End If
                    End If
                Else
                    'tambahan untuk setiap site 5 Feb 2016
                    If CDbl(txtlens.Text) > CDbl(hamount1.Value) Then
                        msg = "<script type='text/javascript'> alert('Information : Lens cannot over " + CDbl(hamount1.Value).ToString("#,##.00") + "'); </script>"
                        lblinfo.Text = "Information : Lens cannot over " + CDbl(hamount1.Value).ToString("#,##.00") + ""
                        hprint.Value = "1"
                        'Return
                    End If

                    If CDbl(txtframe.Text) > CDbl(hamount3.Value) Then
                        msg = "<script type='text/javascript'> alert('Information : Frame cannot over " + CDbl(hamount3.Value).ToString("#,##.00") + "'); </script>"
                        lblinfo.Text = "Information : Frame cannot over " + CDbl(hamount3.Value).ToString("#,##.00") + ""
                        hprint.Value = "1"
                        'Return
                    End If
                End If


                dtmnow = DateTime.Now.ToString("MM")
                dtynow = Date.Now.Year.ToString

                stransno = Session("kdsite") + "/" + dtynow + "/" + dtmnow + "/" + "2" + "/"

                Dim strcon As String = "select Noreg from H_H21502 where Noreg like '%" + stransno + "%'"
                Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
                sda.Fill(dtb)

                If dtb.Rows.Count = 0 Then
                    transno = stransno + "000001"
                ElseIf dtb.Rows.Count > 0 Then
                    I = 0
                    I = dtb.Rows.Count + 1
                    If dtb.Rows.Count > 0 And dtb.Rows.Count < 9 Then
                        transno = stransno + "00000" + I.ToString
                    ElseIf dtb.Rows.Count >= 9 And dtb.Rows.Count < 99 Then
                        transno = stransno + "0000" + I.ToString
                    ElseIf dtb.Rows.Count >= 99 And dtb.Rows.Count < 999 Then
                        transno = stransno + "000" + I.ToString
                    ElseIf dtb.Rows.Count >= 999 And dtb.Rows.Count < 9999 Then
                        transno = stransno + "00" + I.ToString
                    ElseIf dtb.Rows.Count >= 9999 And dtb.Rows.Count < 99999 Then
                        transno = stransno + "0" + I.ToString
                    ElseIf dtb.Rows.Count >= 99999 And dtb.Rows.Count < 999999 Then
                        transno = stransno + I.ToString
                    ElseIf dtb.Rows.Count >= 999999 Then
                        transno = "Error on generate Noreg"
                    End If
                End If

                Try
                    Dim sqlinsert As String
                    Dim cmdinsert As SqlCommand
                    sqlinsert = "Insert into H_H21502 (NoReg, KdSite, TglTrans, Tipe, Nik, TipeLensa, TipeLensa2, Claim, ClaimFrame, Keterangan, Createdby, CreatedIn, CreatedTime,  StEdit, pycostcode, fprint) Values ('" + transno + "', '" + hnmsite.Value + "', '" + txtdate.Text + "', 2, '" + hnik.Value + "', ' ', ' ', '" + txtlens.Text + "', '" + txtframe.Text + "', '" + txtremarks.Text + "', '" + Session("otorisasi").ToString + "', '" + createdin + "', '" + DateTime.Today.ToString + "', 0,'" + txtcostcode.Text + "', '" + hprint.Value + "')"
                    cmdinsert = New SqlCommand(sqlinsert, sqlConn)
                    cmdinsert.ExecuteScalar()

                    If hprint.Value = "1" Then
                        btnprint.Enabled = False
                        btnPaymentUsage.Enabled = False
                    Else
                        btnprint.Enabled = True
                        If Session("kdsite") = "JKT" Then
                            btnPaymentUsage.Enabled = True
                        End If
                        msg = "<script type='text/javascript'> alert('Data has been Save'); </script>"
                        txtnoreg.Text = transno
                    End If

                    'msg = "<script type='text/javascript'> alert('Data has been save'); </script>"
                Catch ex As Exception
                    sqlConn.Close()
                    msg = "<script type='text/javascript'> alert('Failed to save data'); </script>"
                End Try
            End If
        End If
    End Sub

    Public Sub txtlens_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtlens.TextChanged
        If txtlens.Text = "" Then txtlens.Text = 0
        If txtframe.Text = "" Then txtframe.Text = 0
        txttotal.Text = CDbl(txtframe.Text) + CDbl(txtlens.Text)
    End Sub

    Public Sub txtframe_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtframe.TextChanged
        If txtlens.Text = "" Then txtlens.Text = 0
        If txtframe.Text = "" Then txtframe.Text = 0
        txttotal.Text = CDbl(txtframe.Text) + CDbl(txtlens.Text)
    End Sub

    Private Sub btnprint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnprint.Click
        Session("printframe") = txtnoreg.Text
        Response.Redirect("cetak_frame.aspx")
    End Sub

    Private Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Dim sqlupdate As String
        Dim cmdupdate As SqlCommand
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

        Try
            conn.Open()
            sqlupdate = "update H_H21502 set stedit = 2, deleteby = '" + Session("otorisasi") + "', Deletetime = '" + DateTime.Today.ToString + "' where noreg = '" + txtnoreg.Text + "'"
            cmdupdate = New SqlCommand(sqlupdate, conn)
            cmdupdate.ExecuteScalar()

            Response.Redirect("medic_frameslist.aspx")
        Catch ex As Exception

        Finally
            conn.Close()
        End Try

    End Sub

    Protected Sub btnPaymentUsage_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPaymentUsage.Click

        Dim user As ESS.DataAccess.EmployeeModel = ESS.DataAccess.UserLogic.GetUserByNIKSite(txtnik.Text)
        Session("payment_usage") = String.Format("{0}|{1}|{2}|{3}|{4}", txtnoreg.Text, txttotal.Text, "Glasses Claim", DateTime.Now.ToString("dd MMM yyyy"), user.Nik)

        msg = ""
        Response.Redirect("payment_application_internal_form.aspx")
    End Sub
End Class