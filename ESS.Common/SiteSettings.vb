﻿Imports System.Configuration

Public Class SITE_SETTINGS

    Public Shared ReadOnly Property JRN_CITRIXCON() As String
        Get
            Dim _jrn_citrixcon As String
            _jrn_citrixcon = ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString

            Return _jrn_citrixcon
        End Get
    End Property

    Public Shared ReadOnly Property PATH_REPORT() As String
        Get
            Dim _jrn_citrixcon As String
            _jrn_citrixcon = ConfigurationManager.ConnectionStrings("PATH_REPORT").ConnectionString

            Return _jrn_citrixcon
        End Get
    End Property

    Public Shared ReadOnly Property SITE_URL() As String
        Get
            Dim _site_url As String
            _site_url = ConfigurationManager.AppSettings("SITE_URL")

            Return _site_url
        End Get
    End Property

    Public Shared ReadOnly Property IS_LIVE() As Boolean
        Get
            Dim _is_LIVE As Boolean = False

            Dim strPa As String = ConfigurationManager.AppSettings("IS_LIVE")
            If Not String.IsNullOrEmpty(strPa) Then
                If strPa.ToUpper() = "TRUE" Then
                    _is_LIVE = True
                End If

            End If

            Return _is_LIVE
        End Get
    End Property

    Public Shared ReadOnly Property PRINT_PAYMENT_APP() As Boolean
        Get
            Dim PRINT_PA As Boolean = False

            Dim strPa As String = ConfigurationManager.AppSettings("PRINT_PAYMENT_APP")
            If Not String.IsNullOrEmpty(strPa) Then
                If strPa.ToUpper() = "TRUE" Then
                    PRINT_PA = True
                End If

            End If

            Return PRINT_PA
        End Get
    End Property

    Public Shared ReadOnly Property DOA_URL() As String
        Get
            Dim _site_url As String
            _site_url = ConfigurationManager.AppSettings("DOA_URL")

            Return _site_url
        End Get
    End Property

    Public Shared ReadOnly Property PATH_UPLOAD() As String
        Get
            Dim str As String = ConfigurationManager.AppSettings.Item("PATH_UPLOAD")
            If String.IsNullOrEmpty(str) Then
                str = String.Empty
            End If
            Return str
        End Get
    End Property





End Class
