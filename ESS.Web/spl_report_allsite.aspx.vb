﻿Imports Microsoft.Reporting.WebForms
Partial Public Class spl_report_allsite
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtdate1.Text = "2014-01-01"
            txtdate2.Text = "2014-01-01"
        End If
    End Sub

    Public Sub report_retrieve()

        Dim rds As New ReportDataSource("SPLDataSet_H_H1101", SqlDataSource1)

        ReportViewer1.LocalReport.DataSources.Clear()

        Dim p1 As New ReportParameter("date1", txtdate1.Text)
        Dim p2 As New ReportParameter("date2", txtdate2.Text)

        Me.ReportViewer1.LocalReport.SetParameters(New ReportParameter() {p1, p2})

        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.Refresh()

    End Sub

    Private Sub btnok_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnok.Click
        report_retrieve()
    End Sub
End Class