﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="dec_list_report.aspx.vb" Inherits="EXCELLENT.dec_list_report" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:TextBox ID="mth" runat="server" Visible="false" ></asp:TextBox><br />
        <asp:TextBox ID="yr" runat="server" Visible="false" ></asp:TextBox><br />
        <asp:TextBox ID="nik" runat="server" Visible="false" ></asp:TextBox><br />
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
            Font-Size="8pt" Height="721px" Width="943px">
            <LocalReport ReportPath="Laporan\business_dec_list.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource1" 
                        Name="DataSet6_V_H0021" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:jrn_citrixcon %>" 
            SelectCommand="SELECT nik, DecNo, TripNo, TransDate, reqby, reqdate, apprby, apprdate, reviewby, reviewdate, proceedby, proceeddate, ModifiedTime, StEdit, CASE fstatus WHEN 1 THEN 'Process' WHEN 0 THEN 'Unprocess' END AS fstatus FROM V_H002 WHERE (MONTH(TransDate) = @mth) AND (YEAR(TransDate) = @yr) AND (StEdit &lt;&gt; 2) AND (nik = @nik)">
            <SelectParameters>
                <asp:ControlParameter ControlID="mth" Name="mth" PropertyName="Text" />
                <asp:ControlParameter ControlID="yr" Name="yr" PropertyName="Text" />
                <asp:ControlParameter ControlID="nik" Name="nik" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            SelectMethod="GetData" 
            TypeName="EXCELLENT.DataSet6TableAdapters.V_H0021TableAdapter">
        </asp:ObjectDataSource>
    
    </div>
    </form>
</body>
</html>
