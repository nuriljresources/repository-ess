﻿Imports System.Data.SqlClient

Partial Public Class searchcostcode
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Not Page.IsPostBack Then
        '    If Trim(SearchKey.Text) <> "" Then
        '        LoadData(False)
        '    End If
        'End If
        Dim dt As New DataTable
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        'Dim myCMD As SqlCommand = New SqlCommand("SELECT costcode, costname from H_A221 WHERE stedit <> '2' OR (costcod like '%' + @Nama + '%' OR costname like '%' + @Nama + '%') ", sqlConn)
        Dim myCMD As SqlCommand = New SqlCommand("SELECT costcode, costname from H_A221 WHERE stedit <> '2'", sqlConn)

        'Dim param As New SqlParameter()
        'param.ParameterName = "@Nama"
        'param.Value = SearchKey.Text
        'myCMD.Parameters.Add(param)
        sqlConn.Open()
        Dim reader As SqlDataReader = myCMD.ExecuteReader()
        dt.Load(reader, LoadOption.OverwriteChanges)
        sqlConn.Close()
        ViewState("dt") = dt
        PageNumber = 0

        Dim pgitems As New PagedDataSource()
        Dim dv As New DataView(ViewState("dt"))
        pgitems.DataSource = dv
        pgitems.AllowPaging = True
        pgitems.PageSize = 15
        pgitems.CurrentPageIndex = PageNumber

        If pgitems.PageCount > 1 Then
            rptPages.Visible = True
            Dim pages As New ArrayList()
            For i As Integer = 0 To pgitems.PageCount - 1
                pages.Add((i + 1).ToString())
            Next
            rptPages.DataSource = pages
            rptPages.DataBind()
        Else
            rptPages.Visible = False
        End If
        rptResult.DataSource = pgitems
        rptResult.DataBind()
        'LoadData(True)

    End Sub

    Public Property PageNumber() As Integer
        Get
            If ViewState("PageNumber") IsNot Nothing Then
                Return Convert.ToInt32(ViewState("PageNumber"))
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PageNumber") = value
        End Set
    End Property

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        If (SearchKey.Text.Length < 1) Then
        Else
            'LoadData(True)
            
            Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
            Try
                Dim dt As New DataTable
                Dim myCMD As SqlCommand = New SqlCommand("SELECT costcode, costname from H_A221 WHERE stedit <> '2' AND (costcode like '%' + @Nama + '%' OR costname like '%' + @Nama + '%') ", sqlConn)
                Dim param As New SqlParameter()
                param.ParameterName = "@Nama"
                param.Value = SearchKey.Text
                myCMD.Parameters.Add(param)

                sqlConn.Open()
                Dim reader As SqlDataReader = myCMD.ExecuteReader()
                dt.Load(reader, LoadOption.OverwriteChanges)
                sqlConn.Close()
                ViewState("dt") = dt
                PageNumber = 0

                Dim pgitems As New PagedDataSource()
                Dim dv As New DataView(ViewState("dt"))
                pgitems.DataSource = dv
                pgitems.AllowPaging = True
                pgitems.PageSize = 15
                pgitems.CurrentPageIndex = PageNumber

                If pgitems.PageCount > 1 Then
                    rptPages.Visible = True
                    Dim pages As New ArrayList()
                    For i As Integer = 0 To pgitems.PageCount - 1
                        pages.Add((i + 1).ToString())
                    Next
                    rptPages.DataSource = pages
                    rptPages.DataBind()
                Else
                    rptPages.Visible = False
                End If
                rptResult.DataSource = pgitems
                rptResult.DataBind()
            Catch ex As Exception
                MsgBox(ex.Message)
            Finally
                If sqlConn.State = ConnectionState.Open Then
                    sqlConn.Close()
                End If
            End Try
        End If

    End Sub

    Function LoadData(ByVal bool As Boolean)
        Dim sqlConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Try
            Dim dt As New DataTable
            If bool Then
                'Dim myCMD As SqlCommand = New SqlCommand("SELECT costcode, costname from H_A221 WHERE stedit <> '2' OR (costcod like '%' + @Nama + '%' OR costname like '%' + @Nama + '%') ", sqlConn)
                Dim myCMD As SqlCommand = New SqlCommand("SELECT costcode, costname from H_A221 WHERE stedit <> '2'", sqlConn)

                'Dim param As New SqlParameter()
                'param.ParameterName = "@Nama"
                'param.Value = SearchKey.Text
                'myCMD.Parameters.Add(param)
                sqlConn.Open()
                Dim reader As SqlDataReader = myCMD.ExecuteReader()
                dt.Load(reader, LoadOption.OverwriteChanges)
                sqlConn.Close()
                ViewState("dt") = dt
                PageNumber = 0
            End If
            Dim pgitems As New PagedDataSource()
            Dim dv As New DataView(ViewState("dt"))
            pgitems.DataSource = dv
            pgitems.AllowPaging = True
            pgitems.PageSize = 15
            pgitems.CurrentPageIndex = PageNumber

            If pgitems.PageCount > 1 Then
                rptPages.Visible = True
                Dim pages As New ArrayList()
                For i As Integer = 0 To pgitems.PageCount - 1
                    pages.Add((i + 1).ToString())
                Next
                rptPages.DataSource = pages
                rptPages.DataBind()
            Else
                rptPages.Visible = False
            End If
            rptResult.DataSource = pgitems
            rptResult.DataBind()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            If sqlConn.State = ConnectionState.Open Then
                sqlConn.Close()
            End If
        End Try
    End Function

    Private Sub rptPages_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPages.ItemCommand
        PageNumber = Convert.ToInt32(e.CommandArgument) - 1
        LoadData(False)
    End Sub
End Class