﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="cuti_transb_secure.aspx.vb" Inherits="EXCELLENT.cuti_transb_secure" %>



<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    cuti_transb_secure
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<script src="Scripts/calendar_us.js" type="text/javascript"></script>
<script src="Scripts/jsm01.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/calendar-c.css"/> 
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="tripno" AllowPaging="true" PageSize="10" Width="800px">
<Columns>
<asp:CommandField ShowSelectButton="True" SelectText="Pilih" ShowEditButton="true" ButtonType="Button"  HeaderStyle-Width="20px" ItemStyle-VerticalAlign="top" />
            <asp:BoundField DataField="tripno" HeaderText="Trip Number" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top"/>
            <asp:BoundField DataField="nik" HeaderText="NIK" HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top"/>
            <asp:BoundField DataField="sup_nik" HeaderText="Supervisor" HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top"/>
            <asp:BoundField DataField="detailtrip" HeaderText="Trip Detail" HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top"/>
            <asp:BoundField DataField="purpose" HeaderText="Purpose" HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top"/>
            <asp:BoundField DataField="tripfrom" HeaderText="Trip From" HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top"/>
            <asp:BoundField DataField="tripto" HeaderText="Trip To" HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top"/>
            <asp:BoundField DataField="apprby" HeaderText="Approve By" HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top"/>
            <asp:BoundField DataField="apprdate" HeaderText="Approve Date" HeaderStyle-Width="90px" ItemStyle-VerticalAlign="top"/>
</Columns> 
</asp:GridView>
</asp:Content>
