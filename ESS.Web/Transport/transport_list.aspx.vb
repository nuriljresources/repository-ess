﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Partial Public Class transport_list
    Inherits System.Web.UI.Page
    Public tglaw As String
    Public th As String
    Public v1 As String
    Public v2 As String
    Public imonth As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("niksite") = "" Or Session("niksite") = Nothing Then
            Response.Redirect("startpage.aspx")
        End If
        If Not IsPostBack Then
            Dim a As String

            tglaw = Request(ddlbulan.UniqueID)
            th = Request(ddltahun.UniqueID)
            Dim i As Integer
            'ddlbulan.Items.Clear()
            'ddlbulan.Items.Add("January")
            'ddlbulan.Items.Add("February")
            'ddlbulan.Items.Add("March")
            'ddlbulan.Items.Add("April")
            'ddlbulan.Items.Add("May")
            'ddlbulan.Items.Add("June")
            'ddlbulan.Items.Add("July")
            'ddlbulan.Items.Add("August")
            'ddlbulan.Items.Add("September")
            'ddlbulan.Items.Add("October")
            'ddlbulan.Items.Add("November")
            'ddlbulan.Items.Add("December")

            Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

            sqlConn.Open()
            Dim strcon As String = "select min(reqdate) as min_date ,max(reqdate) as max_date from T_H003"
            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            v1 = dtb.Rows(0)!min_date.ToString
            If v1.ToString = "" Then
                v1 = Today.Year.ToString
            Else
                v1 = CDate(v1).ToString("yyyy")
            End If

            v2 = dtb.Rows(0)!max_date.ToString
            If v2.ToString = "" Then
                v2 = Today.Year.ToString
            Else
                v2 = CDate(v2).ToString("yyyy")
            End If

            ddltahun.Items.Clear()
            For i = v1 To v2
                ddltahun.Items.Add(i)
            Next

            ddltahun.SelectedValue = (i - 1).ToString

            Select Case tglaw
                Case "January"
                    imonth = 1
                Case "February"
                    imonth = 2
                Case "March"
                    imonth = 3
                Case "April"
                    imonth = 4
                Case "May"
                    imonth = 5
                Case "June"
                    imonth = 6
                Case "July"
                    imonth = 7
                Case "August"
                    imonth = 8
                Case "September"
                    imonth = 9
                Case "October"
                    imonth = 10
                Case "November"
                    imonth = 11
                Case "December"
                    imonth = 12
                Case Else
                    imonth = 1
            End Select

            Dim sqlConn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
            Try

                Dim dtb2 As DataTable = New DataTable()

                Dim strcon2 As String = "SELECT TransportNo, nik, (select niksite from H_A101 where nik = T_H003.nik) as niksite, (select nama from H_A101 where nik = T_H003.nik) as nama, kddepar, (select nmdepar from H_A130 where kddepar = T_H003.kddepar) as nmdepar, kddivisi, costcode, sup_nik, (case vehicle_type when '00' then 'Pool Car' when '01' then 'Taxi' end) as vehicle_type, reqby, (convert(char(11), reqdate, 106)) as reqdate FROM dbo.T_H003 "
                strcon2 = strcon2 + " where month(reqdate) = " + Today.Month.ToString + " and YEAR(reqdate) = " + Today.Year.ToString + " and nik = '" + Session("niksite") + "' and stedit <> '2'"
                strcon2 = strcon2 + " union"
                strcon2 = strcon2 + " SELECT TransportNo, nik, (select niksite from H_A101 where nik = T_H003.nik) as niksite, (select nama from H_A101 where nik = T_H003.nik) as nama, kddepar, (select nmdepar from H_A130 where kddepar = T_H003.kddepar) as nmdepar, kddivisi, costcode, sup_nik, (case vehicle_type when '00' then 'Pool Car' when '01' then 'Taxi' end) as vehicle_type, reqby, (convert(char(11), reqdate, 106)) as reqdate FROM dbo.T_H003 "
                strcon2 = strcon2 + " where month(reqdate) = " + Today.Month.ToString + " and YEAR(reqdate) = " + Today.Year.ToString + " and nik in (select nikb from V_A004 where nika = '" + Session("niksite") + "' and module = 'TRANS') and stedit <> '2'"
                Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn2)
                sda2.Fill(dtb2)

                GridView1.DataSource = dtb2
                GridView1.DataBind()
                ddlbulan.SelectedValue = Today.Month

            Catch ex As Exception
            Finally
                sqlConn.Close()
            End Try

        End If
    End Sub
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()

            Dim strcon As String = "SELECT TransportNo, nik, (select niksite from H_A101 where nik = T_H003.nik) as niksite, (select nama from H_A101 where nik = T_H003.nik) as nama, kddepar, (select nmdepar from H_A130 where kddepar = T_H003.kddepar) as nmdepar, kddivisi, costcode, sup_nik, (case vehicle_type when '00' then 'Pool Car' when '01' then 'Taxi' end) as vehicle_type, reqby, (convert(char(11), reqdate, 106)) as reqdate FROM dbo.T_H003 "
            strcon = strcon + " where month(reqdate) = " + ddlbulan.SelectedValue + " and YEAR(reqdate) = " + ddltahun.SelectedValue + " and nik = '" + Session("niksite") + "' and stedit <> '2'"
            strcon = strcon + " union"
            strcon = strcon + " SELECT TransportNo, nik, (select niksite from H_A101 where nik = T_H003.nik) as niksite, (select nama from H_A101 where nik = T_H003.nik) as nama, kddepar, (select nmdepar from H_A130 where kddepar = T_H003.kddepar) as nmdepar, kddivisi, costcode, sup_nik, (case vehicle_type when '00' then 'Pool Car' when '01' then 'Taxi' end) as vehicle_type, reqby, (convert(char(11), reqdate, 106)) as reqdate FROM dbo.T_H003 "
            strcon = strcon + " where month(reqdate) = " + ddlbulan.SelectedValue + " and YEAR(reqdate) = " + ddltahun.SelectedValue + " and nik in (select nikb from V_A004 where nika = '" + Session("niksite") + "' and module = 'TRANS') and stedit <> '2'"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            GridView1.DataSource = dtb
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()

            Dim strcon As String = "SELECT TransportNo, nik, (select niksite from H_A101 where nik = T_H003.nik) as niksite, (select nama from H_A101 where nik = T_H003.nik) as nama, kddepar, (select nmdepar from H_A130 where kddepar = T_H003.kddepar) as nmdepar, kddivisi, costcode, sup_nik, (case vehicle_type when '00' then 'Pool Car' when '01' then 'Taxi' end) as vehicle_type, reqby, (convert(char(11), reqdate, 106)) as reqdate FROM dbo.T_H003 "
            strcon = strcon + " where month(reqdate) = " + ddlbulan.SelectedValue + " and YEAR(reqdate) = " + ddltahun.SelectedValue + " and nik = '" + Session("niksite") + "' and stedit <> '2'"
            strcon = strcon + " union"
            strcon = strcon + " SELECT TransportNo, nik, (select niksite from H_A101 where nik = T_H003.nik) as niksite, (select nama from H_A101 where nik = T_H003.nik) as nama, kddepar, (select nmdepar from H_A130 where kddepar = T_H003.kddepar) as nmdepar, kddivisi, costcode, sup_nik, (case vehicle_type when '00' then 'Pool Car' when '01' then 'Taxi' end) as vehicle_type, reqby, (convert(char(11), reqdate, 106)) as reqdate FROM dbo.T_H003 "
            strcon = strcon + " where month(reqdate) = " + ddlbulan.SelectedValue + " and YEAR(reqdate) = " + ddltahun.SelectedValue + " and nik in (select nikb from V_A004 where nika = '" + Session("niksite") + "' and module = 'TRANS') and stedit <> '2'"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            GridView1.DataSource = dtb
            GridView1.PageIndex = e.NewPageIndex
            GridView1.DataBind()
        Catch ex As Exception

        End Try
        

    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        If GridView1.SelectedRow Is Nothing Then Exit Sub

        Dim row As GridViewRow = GridView1.SelectedRow
        Session("vtrans") = GridView1.DataKeys(row.RowIndex).Value.ToString

        Response.Redirect("transport_edit.aspx")
    End Sub
End Class