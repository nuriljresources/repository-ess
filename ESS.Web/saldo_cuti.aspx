﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="saldo_cuti.aspx.vb" Inherits="EXCELLENT.saldo_cuti" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Leave Balance
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<%--<table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
    <caption style="text-align: center; font-size: 1.5em">Saldo Entry</caption>
    <tr style="width:100%;">
             <td>Pers. No</td>
             <td><input style="width:110px" disabled="disabled" type="text" id="txtpers" value="<%=persno %>" />
             <input style="width:140px" disabled="disabled" type="text" id="txtname" value="<%=name %>" />
             </td>
             <td>Marital Status</td>
             <td><input style="width:90px;" disabled="disabled" type="text" id="txtmarital1" /> 
             <input style="width:90px;" disabled="disabled" type="text" id="txtmarital2" /></td>
             <td></td>
     </tr>
     
     <tr><td><strong style="color:Red">Organization</strong></td></tr>
     <tr>
     
     <td>Site</td><td>
     <input style="width:110px" disabled="disabled" type="text" id="txtsite1" value="<%=site %>" />
     <input style="width:140px" disabled="disabled" type="text" id="txtsite2" value="<%=nmcompany %>" /></td>
     
     <td>Position</td><td><input style="width:190px" disabled="disabled" type="text" id="txtposition" value="<%=nmjabat %>"/></td>
     
     </tr>
     <tr>
     <td>Departement</td><td>
     <input style="width:110px" disabled="disabled" type="text" id="txtdept1" value = "<%=nmdepar %>" />
     <input style="width:140px" disabled="disabled" type="text" id="txtdept2" value = "<%=nmdivisi %>" /></td>
     
     <td>Position Job</td><td><input style="width:190px" disabled="disabled" type="text" id="txtposjob" /></td>
     </tr>
     <tr>
     <td> </td><td> </td>
     <td>Level</td><td><input style="width:190px" disabled="disabled" type="text" id="txtlevel" value = "<%=nmlevel %>" /></td></tr>
     <tr><td><strong style="color:Red">Employment</strong></td></tr>
     <tr>
     <td>In Date:</td> <td><input style="width:90px" disabled="disabled" type="text" id="txtindate" value="<%=tglmasuk %>"/></td>
     <td rowspan="4">
     <table style="border-width:medium; border-color:Blue; border-style:double;">
     <tr><td><strong style="color:Blue;">Contract</strong></td></tr>
     <tr><td>Start</td><td><input style="width:90px" disabled="disabled" type="text" id="txtconstart" /></td></tr>
     <tr><td>End</td><td><input style="width:90px" disabled="disabled" type="text" id="txtconend" /></td></tr>
     <tr><td>Contract</td><td><input style="width:90px" disabled="disabled" type="text" id="txtcon" /></td></tr></table></td>
     <td>Permanent Date</td><td><input style="width:90px" disabled="disabled" type="text" id="txtpermdate" /></td>
     <td>Default Shift</td><td><input style="width:90px" disabled="disabled" type="text" id="txtdefshf" /> Fixed overtime <input type="checkbox" id="chkfixover" /> </td>
     </tr>
     
     <tr>
     <td>Eff.date:</td><td><input style="width:90px" disabled="disabled" type="text" id="txteffdt" value="<%=tglefektif %>" /></td>
     <td>Expatriate</td><td><input type="checkbox" id="chkexpat" /></td>
     <td>Point Of Hire</td><td><input style="width:90px" disabled="disabled" type="text" id="txtpoh" /></td>
     </tr>
     
     <tr>
     <td>Type:</td><td><input style="width:90px" disabled="disabled" type="text" id="txttype" value="<%=emptype %>"/></td>
     <td>Scheduling shift</td><td><input type="checkbox" id="chkschshf" /></td>
     <td></td><td><input style="width:90px" disabled="disabled" type="text" id="Text18" /></td>
     </tr>
     <tr><td></td><td><p>-</p></td></tr>
     
     
</table>--%> 

<table width="40%" border="0" cellpadding="0" cellspacing="0" class="data-table" style="margin-left:400px;">
<caption style="text-align: center; font-size: 1.5em; color:White;">Annual Leave</caption>
    <tr>
        <%--<td width="30%">
            <table style="border-style:solid; border-color:Blue; border-width:thin; height:210px; width:100%; padding-bottom:72px;">
                <tr>
                    <td>
                        Coverage Percentage
                    </td>
                    <td>
                        <input style="width:40px" disabled="disabled" type="text" id="txtcovpercen" /> &nbsp; %
                    </td>
                </tr>
                <tr><td> </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">Maximum</td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">Minimum</td>
                </tr>
                <tr>
                    <td>Outpatient Coverage :</td>
                    <td><input style="width:90px" disabled="disabled" type="text" id="txtoutpat1" value="<%=outpatcovmax %>"/></td>
                    <td><input style="width:90px" disabled="disabled" type="text" id="txtoutpat2" value="<%=outpatcovrem %>"/></td>
                </tr>
                <tr>
                    <td>Frame Coverage :</td>
                    <td><input style="width:90px" disabled="disabled" type="text" id="txtfrmcove" value="<%=framecov %>"/></td>
                </tr>
                <tr>
                    <td>Lens Normal Coverage :</td>
                    <td><input style="width:90px" disabled="disabled" type="text" id="txtlensnormcov" value="<%=lensnormcov %>" /></td>
                </tr>
                 <tr><td> </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">Caesar</td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">Normal</td>
                </tr>
                <tr>
                    <td>Maternity Coverage :</td>
                    <td><input style="width:90px" disabled="disabled" type="text" id="txtmaternitycov1" value="<%=matcovcaesar %>"/></td>
                    <td><input style="width:90px" disabled="disabled" type="text" id="txtmaternitycov2" value="<%=matcovnormal %>"/></td>
                </tr>
            </table>
        </td>--%>
        <td width="100%">
            <table style="border-style:solid; border-color:Blue; border-width:thin; height:210px; width:100%; padding-bottom:5px;">
                <%--<caption style="text-align: center; font-size: 1.5em">Field Break</caption>--%>
                <tr>
                    <td>
                        Year Period
                    </td>
                    <td>
                        <input style="width:40px; text-align:center;" disabled="disabled" type="text" id="txtyear" value="<%=year %>"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        
                    </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Eligible
                    </td>
                    <%--<td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Add
                    </td>--%>
                </tr>
                <tr>
                    <td>
                        Annual Leave Eligible:   
                    </td>
                    <td colspan = "2">
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="txtannualleaveeli1" value="<%=anualleaveeli %>" /> day(s)
                    </td>
                    <%--<td>
                        <input style="width:90px" disabled="disabled" type="text" id="txtannualleaveeli2" value ="<%=anualleaveadd %>" />
                    </td>--%>
                </tr>
                <tr>
                    <td>
                        
                    </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Taken
                    </td>
                    <%--<td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Expired
                    </td>--%>
                </tr>
                <tr>
                    <td>
                        Annual Leave Taken:   
                    </td>
                    <td colspan = "2">
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="txtanuallevusg1" value = "<%=anualleaveusg %>" /> day(s)
                    </td>
                    <%--<td>
                        <input style="width:90px" disabled="disabled" type="text" id="txtanuallevusg2" value = "<%=anualleavefor %>" />
                    </td>--%>
                </tr>
                <tr>
                    <td>
                        Annual Leave Remaining:   
                    </td>
                    <td colspan="2">
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="txtanuallevrem1" value="<%=anualleaverem %>" /> day(s)
                    </td>
                    <%--<td>
                        <input style="width:90px" disabled="disabled" type="text" id="txtanuallevrem2" />
                    </td>--%>
                </tr>
                <tr>
                    <td>
                        Carry Forward Remaining:   
                    </td>
                    <td colspan = "2">
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="txtcarryforrem1" value="<%=caryfwdrem %>" /> day(s)
                    </td>
                    <%--<td>
                        <input style="width:90px" disabled="disabled" type="text" id="txtcarryforrem2" />
                    </td>--%>
                </tr>
                <tr>
                    <td>
                        
                    </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Eligible Date
                    </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Expired Date
                    </td>
                </tr>
                <tr>
                    <td>
                        Annual Leave Date:   
                    </td>
                    <td>
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="txtanuallevdate1" value="<%=anualleavedtap %>"/>
                    </td>
                    <td>
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="txtanuallevdate2" value="<%=anualleavedtfor %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Carry Forward:   
                    </td>
                    <td>
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="txtcarryforw1" value="<%=caryfwdap %>" />
                    </td>
                    <td>
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="txtcarryforw2" value ="<%=caryfwdfor %>" />
                    </td>
                </tr>
                
            </table>
            
        </td>
    </tr>
   
</table>

<br />

<table width="40%" border="0" cellpadding="0" cellspacing="0" class="data-table" style="margin-left:400px;">
<caption style="text-align: center; font-size: 1.5em; color:White;">Long Leave</caption>
    <tr>
        <%--<td width="30%">
            <table style="border-style:solid; border-color:Blue; border-width:thin; height:210px; width:100%; padding-bottom:72px;">
                <tr>
                    <td>
                        Coverage Percentage
                    </td>
                    <td>
                        <input style="width:40px" disabled="disabled" type="text" id="txtcovpercen" /> &nbsp; %
                    </td>
                </tr>
                <tr><td> </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">Maximum</td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">Minimum</td>
                </tr>
                <tr>
                    <td>Outpatient Coverage :</td>
                    <td><input style="width:90px" disabled="disabled" type="text" id="txtoutpat1" value="<%=outpatcovmax %>"/></td>
                    <td><input style="width:90px" disabled="disabled" type="text" id="txtoutpat2" value="<%=outpatcovrem %>"/></td>
                </tr>
                <tr>
                    <td>Frame Coverage :</td>
                    <td><input style="width:90px" disabled="disabled" type="text" id="txtfrmcove" value="<%=framecov %>"/></td>
                </tr>
                <tr>
                    <td>Lens Normal Coverage :</td>
                    <td><input style="width:90px" disabled="disabled" type="text" id="txtlensnormcov" value="<%=lensnormcov %>" /></td>
                </tr>
                 <tr><td> </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">Caesar</td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">Normal</td>
                </tr>
                <tr>
                    <td>Maternity Coverage :</td>
                    <td><input style="width:90px" disabled="disabled" type="text" id="txtmaternitycov1" value="<%=matcovcaesar %>"/></td>
                    <td><input style="width:90px" disabled="disabled" type="text" id="txtmaternitycov2" value="<%=matcovnormal %>"/></td>
                </tr>
            </table>
        </td>--%>
        <td width="100%">
            <table style="border-style:solid; border-color:Blue; border-width:thin; height:210px; width:100%; padding-bottom:5px;">
                <%--<caption style="text-align: center; font-size: 1.5em">Field Break</caption>--%>
                <tr>
                    <td>
                        Year Period
                    </td>
                    <td>
                        <input style="width:40px; text-align:center;" disabled="disabled" type="text" id="Text5" value="<%=year %>"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        
                    </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Eligible
                    </td>
                    <%--<td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Add
                    </td>--%>
                </tr>
                <tr>
                    <td>
                        Long Leave Eligible:   
                    </td>
                    <td colspan = "2">
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="Text6" value="<%=anualleaveeli_L %>" /> day(s)
                    </td>
                    <%--<td>
                        <input style="width:90px" disabled="disabled" type="text" id="txtannualleaveeli2" value ="<%=anualleaveadd %>" />
                    </td>--%>
                </tr>
                <tr>
                    <td>
                        
                    </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Taken
                    </td>
                    <%--<td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Expired
                    </td>--%>
                </tr>
                <tr>
                    <td>
                        Long Leave Taken:   
                    </td>
                    <td colspan = "2">
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="Text7" value = "<%=anualleaveusg_L %>" /> day(s)
                    </td>
                    <%--<td>
                        <input style="width:90px" disabled="disabled" type="text" id="txtanuallevusg2" value = "<%=anualleavefor %>" />
                    </td>--%>
                </tr>
                <tr>
                    <td>
                        Long Leave Remaining:   
                    </td>
                    <td colspan="2">
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="Text8" value="<%=anualleaverem_L %>" /> day(s)
                    </td>
                    <%--<td>
                        <input style="width:90px" disabled="disabled" type="text" id="txtanuallevrem2" />
                    </td>--%>
                </tr>
                <tr>
                    <td>
                        Carry Forward Remaining:   
                    </td>
                    <td colspan = "2">
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="Text9" value="<%=caryfwdrem_L %>" /> day(s)
                    </td>
                    <%--<td>
                        <input style="width:90px" disabled="disabled" type="text" id="txtcarryforrem2" />
                    </td>--%>
                </tr>
                <tr>
                    <td>
                        
                    </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Eligible Date
                    </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Expired Date
                    </td>
                </tr>
                <tr>
                    <td>
                        Long Leave Date:   
                    </td>
                    <td>
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="Text10" value="<%=anualleavedtap_L %>"/>
                    </td>
                    <td>
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="Text11" value="<%=anualleavedtfor_L %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Carry Forward:   
                    </td>
                    <td>
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="Text12" value="<%=caryfwdap_L %>" />
                    </td>
                    <td>
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="Text13" value ="<%=caryfwdfor_L %>" />
                    </td>
                </tr>
                
            </table>
            
        </td>
    </tr>
   
</table>


<table style="border-style:solid; border-color:Blue; border-width:thin; width:40%;margin-left:400px; display:none;" class="data-table">
<caption style="text-align: center; font-size: 1.5em; color:White;">Field Break</caption>
                <tr>
                    <td>
                        
                    </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Eligible
                    </td>
                </tr>
                <tr>
                    <td>
                        Field Break Eligible:
                    </td>
                    <td colspan="2">
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="Text1" value="<%=fieldbrkeli %>" /> day(s)
                    </td>
                </tr>
                <tr>
                    <td>
                        
                    </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Taken
                    </td>
                    <td style="background-color:Blue; border-color:Black; border-width:thin; border-style:solid; width:90px; color:White; text-align:center;">
                        Expired
                    </td>
                </tr>
                <tr>
                    <td>
                        Field Break Taken:
                    </td>
                    <td>
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="Text2" value="<%=fieldbrkusd %>" />
                    </td>
                    <td>
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="Text3" value="<%=fieldbrkfor %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Field Break Remaining:
                    </td>
                    <td colspan="2">
                        <input style="width:90px; text-align:center;" disabled="disabled" type="text" id="Text4" value="<%=fieldbrkrem %>" /> day(s)
                    </td>
                </tr>
            </table>

</asp:Content>
