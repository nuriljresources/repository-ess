﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PopUpSearch.aspx.vb" Inherits="EXCELLENT.PopUpSearch" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Search KPI</title>
    <meta http-equiv="Expires" content="0"/>
    <meta http-equiv="Cache-Control" content="no-cache"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <script type="text/javascript">
       function executeClose(id) {
          window.returnValue = id;
          window.close();
       }
       function executeCloseCmbn(id,name) {
          window.returnValue = id+"|"+name;
          window.close();
       } 
    </script>
    <style type="text/css">
     input.btn{
        color:#050;
        font: bold 90% 'trebuchet ms',helvetica,sans-serif;
        background-color:#fed;
        border: 1px solid;
        border-color: #696 #363 #363 #696;
        filter:progid:DXImageTransform.Microsoft.Gradient
        (GradientType=0,StartColorStr='#ffffffff',EndColorStr='#ffeeddaa');
        padding:0 2px 0 2px;
        cursor:hand;
       }
      body{
       font-family:"trebuchet MS", verdana, sans-serif;
       font-size:12px;
      }
      
      .plain
      {
      border: 1px solid #000000;
      }
      
      .grid{
          cursor:hand;
          border: 1px solid #C0C0C0;
      }
      
       .grid2 td{
          border: 1px solid #C0C0C0;
      }
      
      .gridHeader th{
          border: 1px solid #C0C0C0;
          font-weight:bold;
          background:#00CC00;
      }
    </style>
</head>
<body>

    <form id="form1" runat="server">
    <div>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
       <table style="width:100%">
         <tr>
            <td colspan="3"><asp:Label ID="lblKet" runat="server" ForeColor="#FF3300"></asp:Label></td>
         </tr>
         <tr>
            <td>Nama KPI</td>
            <td><asp:TextBox ID="srchTextBox" runat="server" CssClass="plain"></asp:TextBox>&nbsp;&nbsp;<asp:TextBox ID="TextBox1" runat="server" style="visibility:hidden;display:none;" /><asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn"/></td>
            <td></td>
         </tr>
         <tr>
            <td colspan="3">
               <asp:GridView ID="srchResult" runat="server" AllowPaging="True" Width="100%" 
                  CssClass="grid">
                  <PagerSettings Position="Top"/>
                  <RowStyle CssClass="grid2"/>
                  <PagerStyle BorderStyle="None" />
                  <HeaderStyle CssClass="gridHeader" BackColor="#00CC00" />
                  <AlternatingRowStyle BackColor="silver" />
               </asp:GridView> 
            </td>
         </tr>
    </table>
    </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="progress1" runat="server" DisplayAfter="50">    
                  <ProgressTemplate>
                  <asp:Panel ID="pnlProgressBar" runat="server">    
                     <div><asp:Image ID="Image1" runat="Server" ImageUrl="../images/ajax-loader.gif"/>Harap Sabar Menunggu...</div>    
                  </asp:Panel>   
                  </ProgressTemplate> 
                  </asp:UpdateProgress>
    </div>
    </form>
</body>
</html>
