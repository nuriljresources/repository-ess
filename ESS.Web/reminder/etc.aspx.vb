﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Security.Cryptography
Imports System.IO

Partial Public Class etc
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ec As String
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_permit").ConnectionString)
        Dim empConn As New SqlConnection(ConfigurationManager.ConnectionStrings("EMPLOYEEConn").ConnectionString)
        Dim dept As String
        Dim lsnik

        Session("dept") = ""
        Session("otorisasi_permit") = ""
        Session("permission_permit") = ""
        Session("NmUser_permit") = ""
        Session("divisi") = ""
        Session("modul_permit") = ""

        Try
            If Request.QueryString("EC") <> Nothing Then
                'lblts.Text = Decrypt(Replace(Request.QueryString("EC"), " ", "+"))
                lsnik = Decrypt(Replace(Request.QueryString("EC"), " ", "+"))

                Dim dtb As DataTable = New DataTable()
                'HashString
                'Dim strcon As String = "select kduser,nmuser,kdsite,kddivisi,kddepar, kdlevel from B_C012 where kduser = '" + Replace(Me.TextBox1.Text, "'", "") + "' and password = '" + SecureIt.Secure.Encrypt(Replace(Me.TextBox2.Text, "'", "")) + "' and stedit in ('0','1')"
                'Dim strcon As String = "select kduser,nmuser,kdsite,kddivisi,kddepar, kdlevel from B_C012 where kduser = '" + Replace(Me.TextBox1.Text, "'", "") + "' and password = '" + HashString(Replace(Me.TextBox2.Text, "'", "")) + "' and stedit in ('0','1')"
                Dim strcon As String = "select emp_code,employee_nik,name,lvl,modul from ms_user where emp_code = '" + Replace(lsnik, "'", "") + "' and stedit <> '2'"
                'untuk tes
                'strcon = "select emp_code,employee_nik,name,lvl,modul from ms_user where emp_code = '" + Replace(TextBox1.Text, "'", "") + "' and stedit <> '2'"

                'Dim strcon As String = "select emp_code,employee_nik,name,lvl from ms_user where emp_code = '" + Replace(lsnik, "'", "") + "'"
                Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
                sda.Fill(dtb)

                If dtb.Rows.Count > 0 Then
                    Dim dtb2 As DataTable = New DataTable()
                    'Dim str As String = "select (select nmdepar from H_A130 where KdDepar = H_A101.KdDepar) as nmdepar from JRN_TEST.dbo.H_A101 where NIKSITE ='" + Replace(lsnik, "'", "") + "'"
                    Dim str As String = "select (select nmdepar from H_A130 where KdDepar = H_A101.KdDepar) as nmdepar, (SELECT KdDivisi from H_A130 where KdDepar = H_A101.KdDepar) as divisi from JRN_TEST.dbo.H_A101 where NIKSITE ='" + Replace(lsnik, "'", "") + "'"
                    'untuk tes
                    'str = "select (select nmdepar from H_A130 where KdDepar = H_A101.KdDepar) as nmdepar, (SELECT KdDivisi from H_A130 where KdDepar = H_A101.KdDepar) as divisi from JRN_TEST.dbo.H_A101 where NIKSITE ='" + Replace(TextBox1.Text, "'", "") + "'"

                    Dim sda2 As SqlDataAdapter = New SqlDataAdapter(str, empConn)
                    sda2.Fill(dtb2)

                    If dtb2.Rows.Count = 0 Then
                        dept = ""
                        sqlConn.Close()

                        Response.Redirect("http://portal.jresources.com/sitepages/err.aspx")
                    Else
                        dept = dtb2.Rows(0)("nmdepar").ToString
                    End If

                    If Session("otorisasi_permit") = "" Then
                        Session("dept") = dept
                        Session("otorisasi_permit") = dtb.Rows(0)("emp_code").ToString
                        Session("permission_permit") = dtb.Rows(0)("lvl").ToString
                        Session("NmUser_permit") = dtb.Rows(0)("name").ToString
                        Session("divisi") = dtb2.Rows(0)("divisi").ToString
                        Session("modul_permit") = dtb.Rows(0)("modul").ToString
                        If dtb.Rows.Count = 1 Then
                            Response.Redirect("index.aspx")
                        Else
                            Response.Redirect("crossdept.aspx")
                        End If
                    End If
                Else
                    Response.Redirect("http://portal.jresources.com/sitepages/err.aspx")
                End If

            Else
                Response.Redirect("http://portal.jresources.com/sitepages/err.aspx")
            End If
        Catch ex As Exception
            'Response.Redirect("http://moss.jresources.com/sitepages/home4.aspx")
        End Try


        'Dim sresult = Decrypt(Replace(Request.QueryString("EC"), "'", ""))

        'Dim sresult = Decrypt(ec)

    End Sub
    Private Function Decrypt(ByVal input As String) As String
        Return Encoding.UTF8.GetString(Decrypt(Convert.FromBase64String(input)))
    End Function
    Private Function Decrypt(ByVal input As Byte()) As Byte()

        Dim pdb As New PasswordDeriveBytes("hjiweykaksd", New Byte() {&H43, &H87, &H23, &H72, &H45, &H56, _
         &H68, &H14, &H62, &H84})
        Dim ms As New MemoryStream()
        Dim aes As Aes = New AesManaged()
        aes.Key = pdb.GetBytes(aes.KeySize / 8)
        aes.IV = pdb.GetBytes(aes.BlockSize / 8)
        Dim cs As New CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write)
        cs.Write(input, 0, input.Length)
        cs.Close()
        Return ms.ToArray()
    End Function
End Class