﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="popupstab.aspx.vb" Inherits="EXCELLENT.popupstab" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
     <script type="text/javascript">
       function val() {
            document.getElementById("MR2").value = document.getElementById("E1").value;  
            document.getElementById("MR3").value = document.getElementById("E2").value;
            document.getElementById("BL2").value = document.getElementById("MR1").value;
            document.getElementById("BL3").value = document.getElementById("MR4").value;
            document.getElementById("P1").value = document.getElementById("BL1").value; 
            document.getElementById("P2").value = document.getElementById("BL4").value;
         }

         function dovalidate() {
            if (parseFloat(document.getElementById("E1").value) > parseFloat(document.getElementById("E2").value)) {
               alert("Nilai batas bawah excellent 1 harus lebih kecil daripada batas atas excellent 2");
               return false;
            } else if (parseFloat(document.getElementById("MR1").value) > parseFloat(document.getElementById("MR2").value)) {
               alert("Nilai batas bawah meet requirement 1 harus lebih kecil daripada batas atas meet requirement 2");
               return false;
            } else if (parseFloat(document.getElementById("MR3").value) > parseFloat(document.getElementById("MR4").value)) {
               alert("Nilai batas bawah meet requirement 3 harus lebih kecil daripada batas atas meet requirement 4");
               return false;
            } else if (parseFloat(document.getElementById("BL3").value) > parseFloat(document.getElementById("BL4").value)) {
               alert("Nilai batas bawah below standard 3 harus lebih kecil daripada batas atas below standard 4");
               return false;
            } else if (parseFloat(document.getElementById("BL1").value) > parseFloat(document.getElementById("BL2").value)) {
               alert("Nilai batas bawah below standard 1 harus lebih kecil daripada batas atas below standard 2");
               return false;
            }

            if (parseFloat(document.getElementById("EA").value) > 105) {
               alert("Maksimum Nilai Achievement adalah 105");
               return false;
            }
            
            if ((parseFloat(document.getElementById("EA").value) <= parseFloat(document.getElementById("MRA").value)) || (parseFloat(document.getElementById("MRA").value) < parseFloat(document.getElementById("BLA").value)) || (parseFloat(document.getElementById("BLA").value) < parseFloat(document.getElementById("PA").value))) {
               alert("Silahkan cek ulang input achievement anda!");
               return false;
            }
            
            return true;
         }
         
         function fncInputNumericValuesOnly() { if (!(event.keyCode == 45 || event.keyCode == 46 || event.keyCode == 48 || event.keyCode == 49 || event.keyCode == 50 || event.keyCode == 51 || event.keyCode == 52 || event.keyCode == 53 || event.keyCode == 54 || event.keyCode == 55 || event.keyCode == 56 || event.keyCode == 57)) { event.returnValue = false; } }
         function fncInputNumericValuesOnlyNoMinus() { if (!(event.keyCode == 46 || event.keyCode == 48 || event.keyCode == 49 || event.keyCode == 50 || event.keyCode == 51 || event.keyCode == 52 || event.keyCode == 53 || event.keyCode == 54 || event.keyCode == 55 || event.keyCode == 56 || event.keyCode == 57)) { event.returnValue = false; } }
         function checkpermission() {
            if (document.getElementById("Grup").value == '5') {
               document.getElementById("btnSave").style.visibility = "hidden";
            }
         }
    </script>
    <style type="text/css">
    body{
      font-family:"trebuchet MS", verdana, sans-serif;
         font-size:small;
         }
       table.act {
	         border-width: 1px;
	         border-spacing: 0px;
	         border-color: #C0C0C0;
	         border-collapse: collapse;
	         background-color: white;
            }
         table.act thead {
	         border-width: 1px;
	         padding: 1px;
	         border-color: #C0C0C0;
         }
         table.act td {
	         border-width: 1px;
	         padding: 1px;
	         border-color: #C0C0C0;
         }
         input.plain
         {
            border: 1px solid #C0C0C0;
         }
         .plain:hover
         {
            background-color:#FFFFA8;
         }
    </style>
</head>
<body>
  <form id="form1" runat="server">
     <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
       </asp:ToolkitScriptManager>
       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
             <asp:Label id="lblError" runat="server" /><br />
             <asp:HiddenField id="Grup" runat="Server" />
             <asp:HiddenField id="KPIID" runat="Server" />
             <asp:HiddenField id="KPICALCTYPE" runat="Server" />
             <table border="1" style="border-width: 1px;border-color: #C0C0C0;border-spacing: 0px;border-collapse: collapse;width:100%"><tr><td style="background-color:#00CC00;color:white;font-weight:bold;font-size:medium;">Calculation Type : STABILIZE</td></tr></table>
             <b>Nama KPI : <asp:Label id="lblNama" runat="server" /></b><br />           
             <table border="1" style="width:100%" class="act">
               <tr align="center" style="background-color:#EAE8E4"><td>Kategori</td>
               <td colspan="2"  align="center">Batas Bawah</td><td  colspan="2"  align="center">Batas Atas</td>
               <td>Ach</td></tr>
               <tr><td>Excellent</td>
                   <td colspan="2" align="center"><asp:TextBox ID="E1" runat="server" onchange="val();" CssClass="plain" onkeypress="fncInputNumericValuesOnly();"></asp:TextBox></td>
                   <td colspan="2" align="center"><asp:TextBox ID="E2" runat="server" onchange="val();" CssClass="plain" onkeypress="fncInputNumericValuesOnly();"></asp:TextBox></td>
                   <td><asp:textbox id="EA" runat="Server" CssClass="plain" onkeypress="fncInputNumericValuesOnlyNoMinus();"></asp:textbox>
               </tr>
               <tr align="center" style="background-color:#EAE8E4"><td>Kategori</td><td>Batas Bawah</td><td>Batas Atas</td><td>Batas Bawah</td><td>Batas Atas</td><td>Achievement</td></tr>
               <tr><td>Meet Requirement</td>
                   <td><asp:TextBox ID="MR1" runat="server" onchange="val();" CssClass="plain" onkeypress="fncInputNumericValuesOnly();"></asp:TextBox></td>
                   <td><asp:TextBox ID="MR2" runat="server" onchange="val();" CssClass="plain" onkeypress="fncInputNumericValuesOnly();"></asp:TextBox></td>
                   <td><asp:TextBox ID="MR3" runat="server" onchange="val();" CssClass="plain" onkeypress="fncInputNumericValuesOnly();"></asp:TextBox></td>
                   <td><asp:TextBox ID="MR4" runat="server" onchange="val();" CssClass="plain" onkeypress="fncInputNumericValuesOnly();"></asp:TextBox></td>
                   <td><asp:TextBox ID="MRA" runat="server" CssClass="plain" onkeypress="fncInputNumericValuesOnlyNoMinus();"></asp:TextBox></td>
                   </tr>
               <tr><td>Below Expectation</td>
                   <td><asp:TextBox ID="BL1" runat="server" onchange="val();" CssClass="plain" onkeypress="fncInputNumericValuesOnly();"></asp:TextBox></td>
                   <td><asp:TextBox ID="BL2" runat="server" onchange="val();" CssClass="plain" onkeypress="fncInputNumericValuesOnly();"></asp:TextBox></td>
                   <td><asp:TextBox ID="BL3" runat="server" onchange="val();" CssClass="plain" onkeypress="fncInputNumericValuesOnly();"></asp:TextBox></td>
                   <td><asp:TextBox ID="BL4" runat="server" onchange="val();" CssClass="plain" onkeypress="fncInputNumericValuesOnly();"></asp:TextBox></td>
                   <td><asp:TextBox ID="BLA" runat="server" CssClass="plain" onkeypress="fncInputNumericValuesOnlyNoMinus();"></asp:TextBox></td>
                   </tr>
               <tr><td>Poor</td>
                   <td>&lt;</td>
                   <td><asp:TextBox ID="P1" runat="server" onchange="val();" CssClass="plain" onkeypress="fncInputNumericValuesOnly();"></asp:TextBox></td>
                   <td>&gt;</td>
                   <td><asp:TextBox ID="P2" runat="server" onchange="val();" CssClass="plain" onkeypress="fncInputNumericValuesOnly();"></asp:TextBox></td>
                   <td><asp:TextBox ID="PA" runat="server" CssClass="plain" onkeypress="fncInputNumericValuesOnlyNoMinus();"></asp:TextBox></td>
                   </tr>
                   <tr><td colspan="6" align="center"><img src="../images/Stabilize.jpg" /></td></tr>
             </table>         
            <asp:Button ID="btnSave" runat="server" Width="55px" Height="55px" Style="background-image: url(../images/save-icon-small.png); background-repeat: no-repeat;cursor:hand;vertical-align:bottom" BackColor="Transparent" BorderStyle="None" onclientclick="return dovalidate();"/>   
         </ContentTemplate>
       </asp:UpdatePanel>    
    </form>
     <script type="text/javascript">
        checkpermission();
    </script>
</body>
</html>
