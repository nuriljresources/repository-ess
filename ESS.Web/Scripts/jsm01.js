﻿var TABLE_NAME = 'tblIssue';
var tpeserta = document.getElementById('tblPeserta');
var hasLoaded = true;
var ROW_BASE = 0;
var numbering = 1;
var pnumbering = 1;
deletearray = new Array();
deletearrayp = new Array();

function OpenPopup(key) {
    document.getElementById("ctrlToFind").value = key;
    window.open("SearchEmployee.aspx", "List", "scrollbars=yes,resizable=no,width=500,height=400");
    return false;
}

function OpenPopupfield(key) {
    document.getElementById("ctrlToFind").value = key;
    window.open("searchfield.aspx", "List", "scrollbars=yes,resizable=no,width=500,height=400");
    return false;
}

function OpenPrint() {
    if (document.getElementById("ctl00_ContentPlaceHolder1_txtIdRap").value == 'NEW') {
        alert('Please first Save / Search Meeting');
    }
    else {
        window.open("PrintNotulen.aspx?id=" + document.getElementById("ctl00_ContentPlaceHolder1_txtIdRap").value, "List", "location=0,menubar=0,scrollbars=yes,resizable=no,toolbar=no,width=900,height=600");
        return false;
    }
}

function generateNumber() {
    var returnValue = "REMO";
    for (var i = 1; i <= (3 - numbering.toString().length); i++) {
        returnValue = returnValue + "0";
    }
    return returnValue + numbering
}

function generateNumberP() {
    var returnValue = "REMO";
    for (var i = 1; i <= (3 - pnumbering.toString().length); i++) {
        returnValue = returnValue + "0";
    }
    return returnValue + pnumbering
}

function insertRowToTable() {
    if (hasLoaded) {
        var tbl = document.getElementById(TABLE_NAME);
        var rowToInsertAt = tbl.tBodies[0].rows.length;
        for (var i = 0; i < tbl.tBodies[0].rows.length; i++) {
            if (tbl.tBodies[0].rows[i].myRow && tbl.tBodies[0].rows[i].myRow.four.getAttribute('type') == 'radio' && tbl.tBodies[0].rows[i].myRow.four.checked) {
                rowToInsertAt = i;
                break;
            }
        }
        addRowToTable(rowToInsertAt);
        reorderRows(tbl, rowToInsertAt);
    }
}

// tambahkan baris detail rapat
function addRowToTable(num, ref, caller, objIssue) {
    if (hasLoaded) {
        var tbl = document.getElementById(TABLE_NAME);
        var nextRow = tbl.tBodies[0].rows.length;
        var iteration = numbering;
        if (num == null) {
            num = nextRow;
            var row = tbl.tBodies[0].insertRow(num);
            var cell0 = row.insertCell(0);
            var txtInp = document.createElement('input');
            txtInp.setAttribute('type', 'text');
            txtInp.setAttribute('name', 'nox' + iteration);
            txtInp.setAttribute('id', 'nox' + iteration);
            txtInp.setAttribute('size', '1');
            txtInp.setAttribute('class', 'tb09');
            txtInp.setAttribute('disabled', 'disabled');
            txtInp.setAttribute('value', iteration);
            cell0.appendChild(txtInp);
            var txtInp = document.createElement('input');
            txtInp.setAttribute('type', 'hidden');
            txtInp.setAttribute('name', 'nomeet' + iteration);
            txtInp.setAttribute('id', 'nomeet' + iteration);
            txtInp.setAttribute('value', (objIssue != null) ? objIssue.NoMeet : 'NEW');
            cell0.appendChild(txtInp);
            var txtInp = document.createElement('input');
            txtInp.setAttribute('type', 'hidden');
            txtInp.setAttribute('name', 'stat' + iteration);
            txtInp.setAttribute('id', 'stat' + iteration);
            txtInp.setAttribute('value', '0');
            cell0.appendChild(txtInp);
            var cell1 = row.insertCell(1);
            cell1.setAttribute('style', 'width:20%');
            var txtInp = document.createElement('input');
            txtInp.setAttribute('type', 'hidden');
            txtInp.setAttribute('name', 'idIssue' + iteration);
            txtInp.setAttribute('id', 'idIssue' + iteration);
            txtInp.setAttribute('value', (objIssue != null) ? objIssue.NoIssue : generateNumber());
            cell1.appendChild(txtInp);
            var txtInp = document.createElement('textarea');
            txtInp.setAttribute('name', 'nmIssue' + iteration);
            txtInp.setAttribute('id', 'nmIssue' + iteration);
            txtInp.setAttribute('rows', '3');
            txtInp.setAttribute('cols', '30');
            txtInp.setAttribute('onkeypress', 'return imposeMaxLength(event,this, 100)')
            txtInp.setAttribute('class', 'tb10');
            txtInp.setAttribute('style', 'width:87%');
            var txt = document.createTextNode((objIssue != null) ? objIssue.NmIssue : '');
            txtInp.appendChild(txt);
            cell1.appendChild(txtInp);
            cell1.innerHTML = cell1.innerHTML + '<img src="images/add.gif" alt="Add" onclick="addRowToTable();" style="cursor:pointer"/>';
            var txtInp = document.createElement('input');
            txtInp.setAttribute('type', 'hidden');
            txtInp.setAttribute('name', 'ref' + iteration);
            txtInp.setAttribute('id', 'idref' + iteration);
            txtInp.setAttribute('value', (objIssue != null) ? objIssue.NoIssue : generateNumber());
            cell1.appendChild(txtInp);
        }
        else {
            var row = tbl.tBodies[0].insertRow(num);
            var cell0 = row.insertCell(0);
            var txtInp = document.createElement('input');
            txtInp.setAttribute('type', 'text');
            txtInp.setAttribute('name', 'nox' + iteration);
            txtInp.setAttribute('id', 'nox' + iteration);
            txtInp.setAttribute('size', '1');
            txtInp.setAttribute('disabled', 'disabled');
            txtInp.setAttribute('value', iteration);
            txtInp.setAttribute('class', 'tb09');
            cell0.appendChild(txtInp);
            var txtInp = document.createElement('input');
            txtInp.setAttribute('type', 'hidden');
            txtInp.setAttribute('name', 'nomeet' + iteration);
            txtInp.setAttribute('id', 'nomeet' + iteration);
            txtInp.setAttribute('value', (objIssue != null) ? objIssue.NoMeet : 'NEW');
            cell0.appendChild(txtInp);
            var txtInp = document.createElement('input');
            txtInp.setAttribute('type', 'hidden');
            txtInp.setAttribute('name', 'stat' + iteration);
            txtInp.setAttribute('id', 'stat' + iteration);
            txtInp.setAttribute('value', '0');
            txtInp.setAttribute('size', '1');
            cell0.appendChild(txtInp);
            var cell1 = row.insertCell(1);
            var txtInp = document.createElement('input');
            txtInp.setAttribute('type', 'hidden');
            txtInp.setAttribute('name', 'idIssue' + iteration);
            txtInp.setAttribute('id', 'idIssue' + iteration);
            //txtInp.setAttribute('value', (objIssue != null) ? objIssue.NoIssue : generateNumber());
            txtInp.setAttribute('value', (objIssue != null) ? objIssue.NoIssue : ref);

            cell1.appendChild(txtInp);
            var txtInp = document.createElement('textarea');
            txtInp.setAttribute('name', 'nmIssue' + iteration);
            txtInp.setAttribute('id', 'nmIssue' + iteration);
            txtInp.setAttribute('style', 'display:none');
            var txt = document.createTextNode((objIssue != null) ? objIssue.NmIssue : document.getElementById('nmIssue' + caller).value);
            txtInp.appendChild(txt);
            cell1.appendChild(txtInp);
            var txtInp = document.createElement('input');
            txtInp.setAttribute('type', 'hidden');
            txtInp.setAttribute('name', 'ref' + iteration);
            txtInp.setAttribute('id', 'idref' + iteration);
            //txtInp.setAttribute('value', (objIssue != null) ? objIssue.NoRef : ref);
            txtInp.setAttribute('value', (objIssue != null) ? objIssue.NoRef : generateNumber());

            cell1.appendChild(txtInp);
        }
        numbering++;
        var cell1 = row.insertCell(2);
        var txtInp = document.createElement('textarea');
        txtInp.setAttribute('name', 'action' + iteration);
        txtInp.setAttribute('rows', '3');
        txtInp.setAttribute('cols', '40');
        txtInp.setAttribute('style', 'width:90%');
        txtInp.setAttribute('class', 'tb10');
        txtInp.setAttribute('onkeypress', 'return imposeMaxLength(event,this, 500)')
//        var txt = document.createTextNode((objIssue != null) ? objIssue.Action : '');
        //        txtInp.appendChild(txt);
        txtInp.value = ((objIssue != null) ? objIssue.Action : '');
        cell1.appendChild(txtInp);
        var img = document.createElement('img');
        img.setAttribute('src', 'images/add.gif');
        img.setAttribute('style', 'cursor:pointer')
        img.setAttribute('align', 'AbsMiddle');
        img.onclick = function() { addRowToTable(document.getElementById("nox" + iteration).value, document.getElementById('idIssue' + iteration).value, iteration) };
        cell1.appendChild(img);
        var cell1 = row.insertCell(3);
        cell1.setAttribute('style', 'width:12%');
        var txtInp = document.createElement('textarea');
        txtInp.setAttribute('name', 'deliverable' + iteration);
        txtInp.setAttribute('rows', '3');
        txtInp.setAttribute('cols', '20');
        txtInp.setAttribute('style', 'width:93%');
        txtInp.setAttribute('class', 'tb10');
        txtInp.setAttribute('onkeypress', 'return imposeMaxLength(event,this, 50)')
//        var txt = document.createTextNode((objIssue != null) ? objIssue.Deliverables : '');
        //        txtInp.appendChild(txt);
        txtInp.value = ((objIssue != null) ? objIssue.Deliverables : '');
        cell1.appendChild(txtInp);

        var cell1 = row.insertCell(4);
        cell1.setAttribute('style', 'width:18%');
        //PIC
        //var txt = document.createTextNode('PIC\u00A0\u00A0');
        var txt = document.createTextNode('P  .  I  .  C ');
        cell1.appendChild(txt);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'text');
        txtInp.setAttribute('name', 'pic' + iteration + 'name');
        txtInp.setAttribute('value', (objIssue != null) ? objIssue.Nmpic : '');
        txtInp.setAttribute('disabled', 'disabled');
        txtInp.setAttribute('style', 'width:40%');
        txtInp.setAttribute('class', 'tb10');
        cell1.appendChild(txtInp);
        var img = document.createElement('img');
        img.setAttribute('src', 'images/Search.gif');
        img.setAttribute('align', 'AbsMiddle');
        img.setAttribute('style', 'cursor:pointer;');
        img.onclick = function() { OpenPopup('pic' + iteration) };
        cell1.appendChild(img);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'hidden');
        txtInp.setAttribute('name', 'pic' + iteration);
        txtInp.setAttribute('value', (objIssue != null) ? objIssue.PIC : '');
        cell1.appendChild(txtInp);

        //info Ke
        var breakElem = document.createElement("BR");
        cell1.appendChild(breakElem);
        var txt = document.createTextNode('INFO KE ');
        cell1.appendChild(txt);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'text');
        txtInp.setAttribute('name', 'info' + iteration + 'name');
        txtInp.setAttribute('value', (objIssue != null) ? objIssue.NmInfo : '');
        txtInp.setAttribute('disabled', 'disabled');
        txtInp.setAttribute('style', 'width:40%');
        txtInp.setAttribute('class', 'tb10');
        cell1.appendChild(txtInp);
        var img = document.createElement('img');
        img.setAttribute('src', 'images/Search.gif');
        img.setAttribute('align', 'AbsMiddle');
        img.setAttribute('style', 'cursor:pointer;');
        img.onclick = function() { OpenPopup('info' + iteration) };
        cell1.appendChild(img);
        //button osongkan info ke - 8/4/2010
        var imghapus = document.createElement('img');
        imghapus.setAttribute('src', 'images/delete.gif');
        imghapus.setAttribute('align', 'AbsMiddle');
        imghapus.setAttribute('style', 'cursor:pointer;');
        imghapus.onclick = function() { hapusInfoKe(iteration) };
        cell1.appendChild(imghapus);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'hidden');
        txtInp.setAttribute('name', 'info' + iteration);
        txtInp.setAttribute('value', (objIssue != null) ? objIssue.Info : '');
        cell1.appendChild(txtInp);

        var cell1 = row.insertCell(5);
        cell1.setAttribute('style', 'width:15%');
        //Due Date
        //var txt = document.createTextNode('Due ');
        var txt = document.createTextNode('JTH TEMPO ');
        cell1.appendChild(txt);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'text');
        txtInp.setAttribute('name', 'due' + iteration);
        txtInp.setAttribute('size', '12');
        txtInp.setAttribute('value', (objIssue != null) ? objIssue.due : '');
        txtInp.setAttribute('class', 'tb10');
        txtInp.setAttribute('READONLY', 'READONLY');
        cell1.appendChild(txtInp);
        cell1.innerHTML = cell1.innerHTML + '<img title="Open Calendar" class="tcalIcon" onclick="A_TCALS[\'myCalID' + iteration + '\'].f_toggle()" id="tcalico_myCalID' + iteration + '" src="images/cal.gif"/><br />';
        new tcal({
            'formname': 'aspnetForm',
            'controlname': 'due' + iteration,
            'id': 'myCalID' + iteration
        });

        // jenis PICA
        var txt = document.createTextNode('J .E .N .I .S ');
        cell1.appendChild(txt);
        var selector = document.createElement('select');
        var theOption = document.createElement("OPTION");
        theOption.value = "0";
        theOption.innerHTML = "PICAR";
        selector.appendChild(theOption);
        var theOption = document.createElement("OPTION");
        theOption.value = "1";
        theOption.innerHTML = "PICAB";
        selector.appendChild(theOption);
        var theOption = document.createElement("OPTION");
        theOption.value = "2";
        theOption.innerHTML = "NONPICA";
        selector.appendChild(theOption);

        selector.value = ((objIssue != null) ? objIssue.jenis : '2');
        selector.setAttribute('name', 'jenis' + iteration);
        cell1.appendChild(selector);

        var cell1 = row.insertCell(6);
        cell1.innerHTML = cell1.innerHTML + '<center><img title="Delete Row" class="tcalIcon" onclick="deleteRows(document.getElementById(\'nox' + iteration + '\').value)" src="images/delete.gif"/></center>';
        reorderRows(tbl);
        taInit(false);
    }
}

function validate() {
    if ((document.getElementById("ctl00_ContentPlaceHolder1_txtNmRap").value) == '') {
        alert("Please Entry Meeting Name");
        return false;
    } else if (IsTime(document.getElementById("ctl00_ContentPlaceHolder1_txtTime").value) == false) {
        alert("Wrong Meeting Time Start");
        return false;
    }
    else if ((document.getElementById("ctl00_ContentPlaceHolder1_txtTime").value) == '') {
        alert("Please Entry Meeting Time Start");
        return false;
    }
    else if ((document.getElementById("ctl00_ContentPlaceHolder1_txtAkhir").value) == '') {
        alert("Please Entry Meeting Time End");
        return false;
    }
    else if (IsTime(document.getElementById("ctl00_ContentPlaceHolder1_txtAkhir").value) == false) {
        alert("Wrong Meeting Time End");
        return false;
    }
    else if ((document.getElementById("ctl00_ContentPlaceHolder1_txtTmpRap").value) == '') {
        alert("Please Entry Meeting Room");
        return false;
    }
    else if ((document.getElementById("nikPimRap").value) == '') {
        alert("Please Entry Meeting Chairman");
        return false;
    }
    else if (mil(document.getElementById("ctl00_ContentPlaceHolder1_txtTime").value) > mil(document.getElementById("ctl00_ContentPlaceHolder1_txtAkhir").value)) {
        alert("Meeting time start must not greater than meeting time end");
        return false;
    }
    else if (document.getElementById("jnsmeeting").value == "0") {
        alert("Please Choose Type Meeting");
        return false;
    } else if (parseFloat(document.getElementById("ctl00_ContentPlaceHolder1_TextInformasi").value.length) > 5000) {
        alert("Length Text Information must not greater than 5000 character, Your Information Text length : " + document.getElementById("ctl00_ContentPlaceHolder1_TextInformasi").value.length);
        return false;
    }
    
    else {
        var table = document.getElementById(TABLE_NAME);
        for (var r = 1, n = table.rows.length; r < n; r++) {
//            if (table.rows[r].cells[5].getElementsByTagName('input')[0].value == '') {
//                alert('Due Date at Row ' + r + '. Not yet entried!');
//                return false;
//                //table.rows[r].cells[4].getElementsByTagName('input')[1].value
//            } else if (table.rows[r].cells[4].getElementsByTagName('input')[1].value == '') {
//                alert('PIC at Row ' + r + '. Not yet entried!');
//                return false;
//            } else if ((trim(table.rows[r].cells[1].getElementsByTagName('textarea')[0].value) == '') && (table.rows[r].cells[1].getElementsByTagName('input')[0].value == table.rows[r].cells[1].getElementsByTagName('input')[1].value)) {
//                alert('Issue at Row ' + r + '. Not yet entried!');
//                return false;
//            } else if ((trim(table.rows[r].cells[2].getElementsByTagName('textarea')[0].value) == '')) {
//                alert('Action at Row ' + r + '. Not yet entried!');
//                return false;
//            } else if ((trim(table.rows[r].cells[3].getElementsByTagName('textarea')[0].value) == '')) {
//                alert('Deliverables at Row ' + r + '. Not yet entried!');
//                return false;
            //            }
            if ((trim(table.rows[r].cells[1].getElementsByTagName('textarea')[0].value) == '')) {
            //alert('tanggal: ' + table.rows[r].cells[5].getElementsByTagName('input')[0].value);
                //return false;

                table.rows[r].cells[1].getElementsByTagName('textarea')[0].value = ' '
                table.rows[r].cells[2].getElementsByTagName('textarea')[0].value = ' '
                table.rows[r].cells[3].getElementsByTagName('textarea')[0].value = ' '
                table.rows[r].cells[4].getElementsByTagName('input')[1].value = ' '
                table.rows[r].cells[4].getElementsByTagName('input')[0].value = ' '
                table.rows[r].cells[5].getElementsByTagName('input')[0].value = '01/01/1900'
            }else{
                 if ((trim(table.rows[r].cells[2].getElementsByTagName('textarea')[0].value) == '')) {
                    alert('Action at Row ' + r + '. Not yet entried!');
                    return false;
                  } else if ((trim(table.rows[r].cells[3].getElementsByTagName('textarea')[0].value) == '')) {
                    alert('Deliverables at Row ' + r + '. Not yet entried!');
                    return false;
                  } else if (table.rows[r].cells[4].getElementsByTagName('input')[1].value == '') {
                    alert('PIC at Row ' + r + '. Not yet entried!');
                    return false;
                  } else if (table.rows[r].cells[5].getElementsByTagName('input')[0].value == '') {
                    alert('Due Date at Row ' + r + '. Not yet entried!');
                    return false;
                  }
              }
          }
        var table = document.getElementById('tblPeserta');
        for (var r = 1, n = table.rows.length; r < n; r++) {
            if (table.rows[r].cells[1].getElementsByTagName('input')[0].value == '') {
                alert('Participant at Row ' + r + '. Not yet entried!');
                return false;
            }
        }

        //cek NIK dobel peserta rapat
        var j = table.rows.length;
        for (var i = 1; i < j; i++) {
            var nik = table.rows[i].cells[1].getElementsByTagName('input')[0].value;
            if (i < j) {
                for (var k = i + 1; k < j; k++) {
                    if (table.rows[k].cells[1].getElementsByTagName('input')[0].value == nik) {
                        alert('Participant with NIK : ' + nik + '. Entried twice');
                        return false;
                    }
                }
            }
        }
    }
    return true;
}

// tambahkan baris peserta rapat
function addPToTable(objPeserta) {
    if (hasLoaded) {
        var tbl = document.getElementById('tblPeserta');
        var nextRow = tbl.tBodies[0].rows.length;
        var iteration = pnumbering;
        num = nextRow;
        var row = tbl.tBodies[0].insertRow(num);
        var cell0 = row.insertCell(0);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'text');
        txtInp.setAttribute('id', 'pnox' + iteration);
        txtInp.setAttribute('size', '1');
        txtInp.setAttribute('disabled', 'disabled');
        txtInp.setAttribute('value', iteration);
        txtInp.setAttribute('class', 'tb09');
        cell0.appendChild(txtInp);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'hidden');
        txtInp.setAttribute('id', 'pnomeet' + iteration);
        txtInp.setAttribute('value', (objPeserta != null) ? objPeserta.IdRap : 'NEW');
        cell0.appendChild(txtInp);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'hidden');
        txtInp.setAttribute('id', 'statp' + iteration);
        txtInp.setAttribute('value', '0');
        txtInp.setAttribute('size', '1');
        cell0.appendChild(txtInp);
        pnumbering++;
        var cell1 = row.insertCell(1);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'text');
        txtInp.setAttribute('name', 'ps' + iteration + 'nik');
        txtInp.setAttribute('value', (objPeserta != null) ? objPeserta.NikOri : '');
        txtInp.setAttribute('disabled', 'disabled');
        txtInp.setAttribute('class', 'tb10');
        cell1.appendChild(txtInp);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'hidden');
        txtInp.setAttribute('name', 'ops' + iteration + 'nik');
        txtInp.setAttribute('value', (objPeserta != null) ? objPeserta.NikOri : '');
        cell1.appendChild(txtInp);
        var img = document.createElement('img');
        img.setAttribute('src', 'images/Search.gif');
        img.setAttribute('align', 'AbsMiddle');
        img.setAttribute('style', 'cursor:pointer')
        img.onclick = function() { OpenPopup('qqp' + iteration) };
        cell1.appendChild(img);
        var cell1 = row.insertCell(2);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'text');
        txtInp.setAttribute('name', 'ps' + iteration + 'name');
        txtInp.setAttribute('value', (objPeserta != null) ? objPeserta.NmMember : '');
        txtInp.setAttribute('style', 'width:98%');
        txtInp.setAttribute('disabled', 'disabled');
        txtInp.setAttribute('class', 'tb10');
        cell1.appendChild(txtInp);
        var cell1 = row.insertCell(3);
        cell1.setAttribute('align', 'center');
        var selector = document.createElement('select');
        var theOption = document.createElement("OPTION");
        theOption.value = "0";
        theOption.innerHTML = "Attend";
        selector.appendChild(theOption)
        var theOption = document.createElement("OPTION");
        theOption.value = "1";
        theOption.innerHTML = "Not Attend";
        selector.appendChild(theOption)

        selector.value = ((objPeserta != null) ? objPeserta.StHadir : '0');
        cell1.appendChild(selector);
        var cell1 = row.insertCell(4);
        cell1.innerHTML = cell1.innerHTML + '<center><img title="Add Row" src="images/add.gif" alt="Add" onclick="addPToTable();" style="cursor:pointer"/><br /></center>'
        var cell1 = row.insertCell(5);
        cell1.innerHTML = cell1.innerHTML + '<center><img title="Delete Row" class="tcalIcon" onclick="deleteRowsP(document.getElementById(\'pnox' + iteration + '\').value)" style="cursor:pointer" src="images/delete.gif"/><br /></center>'
        reorderRows(tbl);

        if (iteration > 1) {
            OpenPopup('qqp' + iteration);
        }
    }
}

// duplikat dari "addPToTable" khusus digunakan di funtion getRapat
function addPToTableWhenSearch(objPeserta) {
    if (hasLoaded) {
        var tbl = document.getElementById('tblPeserta');
        var nextRow = tbl.tBodies[0].rows.length;
        var iteration = pnumbering;
        num = nextRow;
        var row = tbl.tBodies[0].insertRow(num);
        var cell0 = row.insertCell(0);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'text');
        txtInp.setAttribute('id', 'pnox' + iteration);
        txtInp.setAttribute('size', '1');
        txtInp.setAttribute('disabled', 'disabled');
        txtInp.setAttribute('value', iteration);
        txtInp.setAttribute('class', 'tb09');
        cell0.appendChild(txtInp);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'hidden');
        txtInp.setAttribute('id', 'pnomeet' + iteration);
        txtInp.setAttribute('value', (objPeserta != null) ? objPeserta.IdRap : 'NEW');
        cell0.appendChild(txtInp);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'hidden');
        txtInp.setAttribute('id', 'statp' + iteration);
        txtInp.setAttribute('value', '0');
        txtInp.setAttribute('size', '1');
        cell0.appendChild(txtInp);
        pnumbering++;
        var cell1 = row.insertCell(1);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'text');
        txtInp.setAttribute('name', 'ps' + iteration + 'nik');
        txtInp.setAttribute('value', (objPeserta != null) ? objPeserta.NikOri : '');
        txtInp.setAttribute('disabled', 'disabled');
        txtInp.setAttribute('class', 'tb10');
        cell1.appendChild(txtInp);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'hidden');
        txtInp.setAttribute('name', 'ops' + iteration + 'nik');
        txtInp.setAttribute('value', (objPeserta != null) ? objPeserta.NikOri : '');
        cell1.appendChild(txtInp);
        var img = document.createElement('img');
        img.setAttribute('src', 'images/Search.gif');
        img.setAttribute('align', 'AbsMiddle');
        img.setAttribute('style', 'cursor:pointer')
        img.onclick = function() { OpenPopup('qqp' + iteration) };
        cell1.appendChild(img);
        var cell1 = row.insertCell(2);
        var txtInp = document.createElement('input');
        txtInp.setAttribute('type', 'text');
        txtInp.setAttribute('name', 'ps' + iteration + 'name');
        txtInp.setAttribute('value', (objPeserta != null) ? objPeserta.NmMember : '');
        txtInp.setAttribute('style', 'width:98%');
        txtInp.setAttribute('disabled', 'disabled');
        txtInp.setAttribute('class', 'tb10');
        cell1.appendChild(txtInp);
        var cell1 = row.insertCell(3);
        cell1.setAttribute('align', 'center');
        var selector = document.createElement('select');
        var theOption = document.createElement("OPTION");
        theOption.value = "0";
        theOption.innerHTML = "Attend";
        selector.appendChild(theOption)
        var theOption = document.createElement("OPTION");
        theOption.value = "1";
        theOption.innerHTML = "Not Attend";
        selector.appendChild(theOption)

        selector.value = ((objPeserta != null) ? objPeserta.StHadir : '0');
        cell1.appendChild(selector);
        var cell1 = row.insertCell(4);
        cell1.innerHTML = cell1.innerHTML + '<center><img title="Add Row" src="images/add.gif" alt="Add" onclick="addPToTable();" style="cursor:pointer"/><br /></center>'
        var cell1 = row.insertCell(5);
        cell1.innerHTML = cell1.innerHTML + '<center><img title="Delete Row" class="tcalIcon" onclick="deleteRowsP(document.getElementById(\'pnox' + iteration + '\').value)" style="cursor:pointer" src="images/delete.gif"/><br /></center>'
        reorderRows(tbl);
    }
}

function saveheader() {
    if (validate()) {
        document.getElementById("saveimg").style.visibility = "hidden";
        var doc = document;

        iobjRapat = new objRapat(
        doc.getElementById("ctl00_ContentPlaceHolder1_txtIdRap").value,
        doc.getElementById("ctl00_ContentPlaceHolder1_txtNmRap").value,
        doc.getElementById("ctl00_ContentPlaceHolder1_txtTmpRap").value,
        doc.getElementById("ctl00_ContentPlaceHolder1_txtTime").value,
        doc.getElementById("ctl00_ContentPlaceHolder1_txtAkhir").value,
        doc.getElementById("ctl00_ContentPlaceHolder1_txtTglRap").value,
        doc.getElementById("ctl00_ContentPlaceHolder1_txtAgenda").value,
        doc.getElementById("nikPimRap").value, document.getElementById("namaPimRap").value,
        doc.getElementById("nikNotRap").value, document.getElementById("namaNotRap").value,
        doc.getElementById("jnsmeeting").value,
        doc.getElementById("ctl00_ContentPlaceHolder1_TextInformasi").value, '0',
        doc.getElementById("ctl00_ContentPlaceHolder1_txtNonBuma").value, doc.getElementById("ctl00_ContentPlaceHolder1_TextKebijakan").value);

        MyArrayP = new Array();
        var table = document.getElementById('tblPeserta');
        for (var r = 1, n = table.rows.length; r < n; r++) {
            MyArrayP[MyArrayP.length] = new objPeserta(
                table.rows[r].cells[0].getElementsByTagName('input')[1].value,
                table.rows[r].cells[1].getElementsByTagName('input')[1].value,
                table.rows[r].cells[1].getElementsByTagName('input')[0].value,
                table.rows[r].cells[2].getElementsByTagName('input')[0].value,
                table.rows[r].cells[3].getElementsByTagName('select')[0].value, '0'
                );
        }
        for (var r = 0, n = deletearrayp.length; r < n; r++) {
            MyArrayP[MyArrayP.length] = deletearrayp[r];
        }
        MyArray = new Array();
        var table = document.getElementById(TABLE_NAME);
        for (var r = 1, n = table.rows.length; r < n; r++) {
            MyArray[MyArray.length] = new objIssue(
                document.getElementById("ctl00_ContentPlaceHolder1_txtIdRap").value,
                table.rows[r].cells[0].getElementsByTagName('input')[1].value,
                table.rows[r].cells[1].getElementsByTagName('input')[0].value,
                table.rows[r].cells[1].getElementsByTagName('textarea')[0].value,
                table.rows[r].cells[1].getElementsByTagName('input')[1].value,
                table.rows[r].cells[2].getElementsByTagName('textarea')[0].value,
                table.rows[r].cells[4].getElementsByTagName('input')[1].value,
                table.rows[r].cells[4].getElementsByTagName('input')[0].value,
                table.rows[r].cells[5].getElementsByTagName('input')[0].value,
                table.rows[r].cells[5].getElementsByTagName('select')[0].value,
                '0', '0',
                table.rows[r].cells[4].getElementsByTagName('input')[3].value,
                table.rows[r].cells[4].getElementsByTagName('input')[2].value,
                table.rows[r].cells[3].getElementsByTagName('textarea')[0].value
                );
        }
        for (var r = 0, n = deletearray.length; r < n; r++) {
            MyArray[MyArray.length] = deletearray[r];
        }
        $("#log").html('**' + new Date() + ' Proses Saving... Please Wait...');
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "WS/Service.asmx/SaveRapat",
            data: "{'_rapat':" + JSON.stringify(iobjRapat) + ",'_peserta':" + JSON.stringify(MyArrayP) + ",'_issue':" + JSON.stringify(MyArray) + ",'_depar':" + JSON.stringify(document.getElementById("KdDepar").value) + ",'_site':" + JSON.stringify(document.getElementById("KdCabang").value) + "}",
            dataType: "json",
            success: function(res) {
                var temp = new Array();
                temp = res.d.split(';');
                if (temp[0] == 'success') {
                    var table = document.getElementById('tblPeserta');
                    if (temp[1] == 'edit') {
                        for (var r = 1, n = table.rows.length; r < n; r++) {
                            table.rows[r].cells[0].getElementsByTagName('input')[1].value = document.getElementById("ctl00_ContentPlaceHolder1_txtIdRap").value;
                            table.rows[r].cells[1].getElementsByTagName('input')[1].value = table.rows[r].cells[1].getElementsByTagName('input')[0].value;
                        }
                        alert('Save Data Success ....!');
                        document.getElementById('log').innerHTML = '**' + new Date() + ': Save Data Success';
                        document.getElementById("saveimg").style.visibility = "";
                        deletearray = new Array();
                        deletearrayp = new Array();
                    }
                    else {
                        document.getElementById("ctl00_ContentPlaceHolder1_txtIdRap").value = temp[1];
                        for (var r = 1, n = table.rows.length; r < n; r++) {
                            table.rows[r].cells[0].getElementsByTagName('input')[1].value = document.getElementById("ctl00_ContentPlaceHolder1_txtIdRap").value;
                            table.rows[r].cells[1].getElementsByTagName('input')[1].value = table.rows[r].cells[1].getElementsByTagName('input')[0].value;
                        }
                        alert('Save Success ....!');
                        document.getElementById('log').innerHTML = '**' + new Date() + ': Save Success';
                        document.getElementById("saveimg").style.visibility = "";
                    }

                    var table = document.getElementById(TABLE_NAME)
                    for (var r = 1, n = table.rows.length; r < n; r++) {
                        table.rows[r].cells[0].getElementsByTagName('input')[1].value = document.getElementById("ctl00_ContentPlaceHolder1_txtIdRap").value;
                    }

                }
                else {
                    document.getElementById('log').innerHTML = '**' + new Date() + ': Save Failed ' + temp[1];
                    document.getElementById("saveimg").style.visibility = "";
                }
            },
            error: function(err) {
                alert("error: " + err.responseText);
            }
        });
        
    }
}

function deleteRows(num) {
    var table = document.getElementById(TABLE_NAME);
    if (table.rows.length == 2) return false;
    if ((table.rows[num].cells[1].getElementsByTagName('input')[0].value == table.rows[num].cells[1].getElementsByTagName('input')[1].value) && (parseFloat(num) + 1 < table.rows.length)) {
        for (var r = parseFloat(num) + 1, n = table.rows.length; r < n; r++) {
            if (table.rows[num].cells[1].getElementsByTagName('input')[0].value == table.rows[r].cells[1].getElementsByTagName('input')[0].value) {
                alert('There Exist Sub Action');
                return false;
            }
        }
        if (table.rows[num].cells[0].getElementsByTagName('input')[1].value == 'NEW') {
        } else {
            table.rows[num].cells[0].getElementsByTagName('input')[2].value = '1';
            deletearray[deletearray.length] = new objIssue(
                    document.getElementById("ctl00_ContentPlaceHolder1_txtIdRap").value,
                    table.rows[num].cells[0].getElementsByTagName('input')[1].value,
                    table.rows[num].cells[1].getElementsByTagName('input')[0].value,
                    table.rows[num].cells[1].getElementsByTagName('textarea')[0].value,
                    table.rows[num].cells[1].getElementsByTagName('input')[1].value,
                    table.rows[num].cells[2].getElementsByTagName('textarea')[0].value,
                    table.rows[num].cells[4].getElementsByTagName('input')[1].value,
                    table.rows[num].cells[4].getElementsByTagName('input')[0].value,
                    table.rows[num].cells[5].getElementsByTagName('input')[0].value,
                    table.rows[num].cells[5].getElementsByTagName('select')[0].value,
                    '0', '1',
                    table.rows[num].cells[4].getElementsByTagName('input')[3].value,
                    table.rows[num].cells[4].getElementsByTagName('input')[2].value,
                    table.rows[num].cells[3].getElementsByTagName('textarea')[0].value
                    );
        }
        table.deleteRow(num);
        reorderRows(table);
    }
    else {
        if (table.rows[num].cells[0].getElementsByTagName('input')[1].value == 'NEW') {
        } else {
            table.rows[num].cells[0].getElementsByTagName('input')[2].value = '1';
            deletearray[deletearray.length] = new objIssue(
                    document.getElementById("ctl00_ContentPlaceHolder1_txtIdRap").value,
                    table.rows[num].cells[0].getElementsByTagName('input')[1].value,
                    table.rows[num].cells[1].getElementsByTagName('input')[0].value,
                    table.rows[num].cells[1].getElementsByTagName('textarea')[0].value,
                    table.rows[num].cells[1].getElementsByTagName('input')[1].value,
                    table.rows[num].cells[2].getElementsByTagName('textarea')[0].value,
                    table.rows[num].cells[4].getElementsByTagName('input')[1].value,
                    table.rows[num].cells[4].getElementsByTagName('input')[0].value,
                    table.rows[num].cells[5].getElementsByTagName('input')[0].value,
                    table.rows[num].cells[5].getElementsByTagName('select')[0].value,
                    '0', '1',
                    table.rows[num].cells[4].getElementsByTagName('input')[3].value,
                    table.rows[num].cells[4].getElementsByTagName('input')[2].value,
                    table.rows[num].cells[3].getElementsByTagName('textarea')[0].value
                    );
        }
        table.deleteRow(num);
        reorderRows(table);
    }
}

function deleteRowsP(num) {
    var table = document.getElementById('tblPeserta');
    if (table.rows.length == 2) return false;
    if (table.rows[num].cells[0].getElementsByTagName('input')[1].value == 'NEW') { }
    else {
        table.rows[num].cells[0].getElementsByTagName('input')[2].value = '1';
        deletearrayp[deletearrayp.length] = new objPeserta(
                table.rows[num].cells[0].getElementsByTagName('input')[1].value,
                table.rows[num].cells[1].getElementsByTagName('input')[1].value,
                table.rows[num].cells[1].getElementsByTagName('input')[0].value,
                table.rows[num].cells[2].getElementsByTagName('input')[0].value,
                table.rows[num].cells[3].getElementsByTagName('select')[0].value, '1'
                );
    }
    table.deleteRow(num);
    reorderRows(table);
}
function reorderRows(tbl) {
    if (hasLoaded) {
        if (tbl.tBodies[0].rows[0]) {
            var count = 1;
            for (var i = 0; i < tbl.tBodies[0].rows.length; i++) {
                if (tbl.tBodies[0].rows[i].style.display != 'none') {
                    tbl.tBodies[0].rows[i].cells[0].getElementsByTagName('input')[0].value = count;
                    count++;
                }
            }
        }
    }
}

function getrapat(id) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "WS/Service.asmx/GetRapat",
        data: "{'id':'" + id + "'}",
        dataType: "json",
        success: function(res) {
            MyPeserta = new Array();
            MyIssue = new Array();
            MyPeserta = res.d.MyPeserta;
            MyIssue = res.d.MyIssue;
            if (res.d.MyRapat1.Mail != 1) {
                var doc = document;
                doc.getElementById("ctl00_ContentPlaceHolder1_txtIdRap").value = res.d.MyRapat1.IdRap;
                doc.getElementById("ctl00_ContentPlaceHolder1_txtNmRap").value = res.d.MyRapat1.NmRap;
                doc.getElementById("ctl00_ContentPlaceHolder1_txtTmpRap").value = res.d.MyRapat1.TmpRap;
                doc.getElementById("ctl00_ContentPlaceHolder1_txtTglRap").value = res.d.MyRapat1.TglRap;
                doc.getElementById("ctl00_ContentPlaceHolder1_txtAgenda").value = res.d.MyRapat1.Agenda;
                doc.getElementById("ctl00_ContentPlaceHolder1_txtTime").value = res.d.MyRapat1.WkRap;
                doc.getElementById("ctl00_ContentPlaceHolder1_txtAkhir").value = res.d.MyRapat1.SdwkRap;
                doc.getElementById("ctl00_ContentPlaceHolder1_txtAgenda").value = res.d.MyRapat1.Agenda;
                doc.getElementById("ctl00_ContentPlaceHolder1_TextInformasi").value = res.d.MyRapat1.Informasi;
                doc.getElementById("nikPimRap").value = res.d.MyRapat1.Pimpinan;
                doc.getElementById("namaPimRap").value = res.d.MyRapat1.Nmpimpinan;
                doc.getElementById("nikNotRap").value = res.d.MyRapat1.Notulis;
                doc.getElementById("namaNotRap").value = res.d.MyRapat1.Nmnotulis;
                doc.getElementById("jnsmeeting").value = res.d.MyRapat1.JenisRap;
                doc.getElementById("ctl00_ContentPlaceHolder1_txtNonBuma").value = res.d.MyRapat1.MemberEx;
                doc.getElementById("ctl00_ContentPlaceHolder1_TextKebijakan").value = res.d.MyRapat1.Kebijakan;

                for (var i = 0; i < MyPeserta.length; i++) {
                    //Sughus
                    //addPToTable(MyPeserta[i]);
                    addPToTableWhenSearch(MyPeserta[i]);
                }

                for (var i = 0; i < MyIssue.length; i++) {
                    if (MyIssue[i].NoIssue == MyIssue[i].NoRef)
                        addRowToTable(null, null, null, MyIssue[i]);
                    else
                        addRowToTable(i, null, null, MyIssue[i]);
                }
                var terakhir;
                for (var i = 0; i < MyIssue.length; i++) {
                    if (i == 0)
                        terakhir = MyIssue[i].NoRef
                    else {
                        if (parseFloat(Right(MyIssue[i].NoRef, 3)) > parseFloat(Right(terakhir, 3)))
                            terakhir = MyIssue[i].NoRef;
                    }
                }
                numbering = parseFloat(Right(terakhir, 3)) + 1;
                //numbering = parseFloat(Right(MyIssue[MyIssue.length - 1].NoRef, 3)) + 1;
                //numbering = parseFloat(MyIssue.length)+1; //COMMENTED BY RICKY ''Ambil yang paling terakhir nomornya seharusnya
            }
        },
        error: function(err) {
            alert("error: " + err.responseText);
        }
    });
}

function Right(str, n) {
    if (n <= 0)
        return "";
    else if (n > String(str).length)
        return str;
    else {
        var iLen = String(str).length;
        return String(str).substring(iLen, iLen - n);
    }
}

function objIssue(IDMeet, NoMeet, NoIssue, NmIssue, NoRef, Action, PIC, NmPIC, due, jenis, status, deleted, Info, NmInfo, Deliverables) {
    this.IDMeet = IDMeet; this.NoMeet = NoMeet; this.NoIssue = NoIssue;
    this.NmIssue = NmIssue; this.NoRef = NoRef;
    this.Action = Action; this.PIC = PIC; this.NmPIC = NmPIC; this.due = due;
    this.jenis = jenis; this.status = status; this.deleted = deleted; this.info = Info; this.NmInfo = NmInfo; this.Deliverables = Deliverables
}
function objRapat(IdRap, NmRap, TmpRap, WkRap, SdWkRap, TglRap, Agenda, Pimpinan, NmPimpinan, Notulis, NmNotulis, JenisRap, Informasi, Mail, MemberEx, Kebijakan) {
    this.IdRap = IdRap; this.NmRap = NmRap; this.TmpRap = TmpRap;
    this.WkRap = WkRap; this.SdWkRap = SdWkRap; this.TglRap = TglRap;
    this.Agenda = Agenda; this.Pimpinan = Pimpinan; this.NmPimpinan = NmPimpinan;
    this.Notulis = Notulis; this.NmNotulis = NmNotulis; this.JenisRap = JenisRap;
    this.Informasi = Informasi; this.Mail = Mail; this.MemberEx = MemberEx; this.Kebijakan = Kebijakan;
}
function objPeserta(IdRap, NikOri, NikMember, NmMember, StHadir, deleted) {
    this.IdRap = IdRap; this.NikOri = NikOri;
    this.NikMember = NikMember; this.NmMember = NmMember; this.StHadir = StHadir; this.deleted = deleted;
}

function taInit(bCols) {
    var i, ta = document.getElementsByTagName('textarea');
    for (i = 0; i < ta.length; ++i) {
        if (ta[i].id != "ctl00_ContentPlaceHolder1_TextInformasi") {
        ta[i]._ta_resize_cols_ = bCols;
        ta[i]._ta_default_rows_ = ta[i].rows;
        ta[i]._ta_default_cols_ = ta[i].cols;
        ta[i].onkeyup = taExpand;
        ta[i].onmouseover = taExpand;
        ta[i].onmouseout = taRestore;
        ta[i].onfocus = taOnFocus;
        ta[i].onblur = taOnBlur;
        //ta[i].onpaste = alert(e);
        //jQuery(ta[i]).bind('input paste', function(e) { alert(e) })
        }
    }
}
function taOnFocus(e) {
    this._ta_is_focused_ = true;
    this.onmouseover();
}

function Left(str, n) {
    if (n <= 0)
        return "";
    else if (n > String(str).length)
        return str;
    else
        return String(str).substring(0, n);
}

function taOnBlur() {
    var a = 0;
    a = this.value.length;
    if (this.name.indexOf('nmIssue') != -1)
    { if (a > 150) this.value = Left(this.value, 150) }
    else if (this.name.indexOf('action') != -1)
    { if (a > 500) this.value = Left(this.value, 500) }
    else if (this.name.indexOf('deliverable') != -1)
    { if (a > 70) this.value = Left(this.value, 70) }
}

function taRestore() {
    if (!this._ta_is_focused_) {
        this.rows = this._ta_default_rows_;
        if (this._ta_resize_cols_) {
            this.cols = this._ta_default_cols_;
        }
    }
}

function taExpand() {
    var a, i, r, c = 0;
    a = this.value.split('\n');
    if (this._ta_resize_cols_) {
        for (i = 0; i < a.length; i++) // find max line length
        {
            if (a[i].length > c) {
                c = a[i].length;
            }
        }
        if (c < this._ta_default_cols_) {
            c = this._ta_default_cols_;
        }
        this.cols = c;
        r = a.length;
    }
    else {
        for (i = 0; i < a.length; i++) {
            if (a[i].length > this.cols) {
                c += Math.floor(a[i].length / this.cols);
            }
        }
        r = c + a.length;
    }
    if (r < this._ta_default_rows_) {
        r = this._ta_default_rows_;
    }
    this.rows = r;
}

function textboxstyle() {
    var inputs = document.getElementsByTagName('input');
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].type.toLowerCase() == 'text') {
            inputs[i].className += 'tb10';
        }
    }
}

function applystyle() {
    taInit(false);
}

window.onload = applystyle;

function displayRow(tn) {
    var table = document.getElementById(tn);
    for (var r = 1, n = table.rows.length; r < n; r++) {
        if (table.rows[r].style.display == '') {
            table.rows[r].style.display = 'none';
            document.getElementById('d' + tn).innerHTML = 'Show (' + (table.rows.length - 1) + ' Peserta)';
        }
        else {
            table.rows[r].style.display = '';
            document.getElementById('d' + tn).innerHTML = 'Hide';
        }
    }
}

function imposeMaxLength(Event, Object, MaxLen) {
    return (Object.value.length < MaxLen) || (Event.keyCode == 8 || Event.keyCode == 46 || (Event.keyCode >= 35 && Event.keyCode <= 40))
}

function IsTime(strTime) {
    var strTime1 = /^(\d{1,2})(\:)(\d{2})\2(\d{2})(\ )\w[AM|PM|am|pm]$/;
    var strTime2 = /^(\d{1,2})(\:)(\d{2})(\ )\w[A|P|a|p]\w[M|m]$/;
    var strTime3 = /^(\d{1,2})(\:)(\d{1,2})$/;

    var strFormat1 = strTime.match(strTime1);
    var strFormat2 = strTime.match(strTime2);
    var strFormat3 = strTime.match(strTime3);
    // Check to see if it matches one of the
    //     3 Format Strings.
    if (strFormat1 == null && strFormat2 == null && strFormat3 == null) {
        return false;
    }
    else if (strFormat1 != null) {
        // Validate for this format: 3:48:01 PM
        if (strFormat1[1] > 12 || strFormat1[1] < 00) {
            return false;
        }
        if (strFormat1[3] > 59 || strFormat1[3] < 00) {
            return false;
        }
        if (strFormat1[4] > 59 || strFormat1[4] < 00) {
            return false;
        }
    }
    else if (strFormat2 != null) {
        // Validate for this format: 3:48 PM
        if (strFormat2[1] > 12 || strFormat2[1] < 00) {
            return false;
        }
        if (strFormat2[3] > 59 || strFormat2[3] < 00) {
            return false;
        }
    }
    else if (strFormat3 != null) {
        // Validate for this format: 15:48
        if (strFormat3[1] > 23 || strFormat3[1] < 00) {
            return false;
        }
        if (strFormat3[3] > 59 || strFormat3[3] < 00) {
            return false;
        }
    }
    return true;
}

function mil(str) {
    var t = str.split(':')
    var hh = parseInt((Left(t[0], 1) == 0) ? Right(t[0], 1) : t[0]);
    var mm = parseInt(t[1]);
    var d = new Date(2007, 0, 1, hh, mm, 00); // just a date not around daylightsaving
    return d.getTime();
}

function trim(str) {
    return str.replace(/^[\s]+/, '').replace(/[\s]+$/, '').replace(/[\s]{2,}/, ' ');
}

function doMail() {
    if (document.getElementById("ctl00_ContentPlaceHolder1_txtIdRap").value != "NEW") {
        var answer = confirm("Data Emailed will not be editable. Are you sure?")
        if (answer) {
            document.getElementById("saveimg").style.display = "none";
            document.getElementById("printimg").style.display = "none";
            document.getElementById("mailimg").style.display = "none";
            window.location = "PrintNotulen.aspx?id=" + document.getElementById("ctl00_ContentPlaceHolder1_txtIdRap").value + "&m=t";
        }
        else
        { }
    }
    else {
        alert("Please first Save Minutes of Meeting")
    }
}

function hapusInfoKe(ite) {
    var table = document.getElementById(TABLE_NAME);
    table.rows[ite].cells[4].getElementsByTagName('input')[2].value = '';
    table.rows[ite].cells[4].getElementsByTagName('input')[3].value = '';
}

function limiter1() {
    var count = "5000";
    var teks = document.getElementById("ctl00_ContentPlaceHolder1_TextInformasi").value;
    var pnjg = teks.length;
    if (pnjg > count) {
        teks = teks.substring(0, count);
        document.getElementById("ctl00_ContentPlaceHolder1_TextInformasi").value = teks;
        return false;
    }
    document.getElementById("lblsisainfo").innerHTML = '(' + (count - pnjg) + ' available character)';
}

function limiter2() {
    var count = "1500";
    var teks = document.getElementById("ctl00_ContentPlaceHolder1_TextKebijakan").value;
    var pnjg = teks.length;
    if (pnjg > count) {
        teks = teks.substring(0, count);
        document.getElementById("ctl00_ContentPlaceHolder1_TextKebijakan").value = teks;
        return false;
    }
    document.getElementById("lblsisa").innerHTML = '(' + (count - pnjg) + ' available character)';
}

function Left(str, n) {
    if (n <= 0)
        return "";
    else if (n > String(str).length)
        return str;
    else
        return String(str).substring(0, n);
}
function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}
