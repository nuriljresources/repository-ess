﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UploadFile.aspx.vb" Inherits="EXCELLENT.UploadFile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>     
    <script src="Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="scripts/jquery.multifile.js" type="text/javascript"></script>   
    <script src="Scripts/jslamp.js" type="text/javascript"></script>    
</head>
<body>    
    <form id="form1" runat="server">
    <asp:Label ID="LblError" runat="server" Text="" Font-Bold="True" 
        Font-Size="Large" ForeColor="#FF3300"></asp:Label>
    <center>
    <div id="divfile" style="border: 1px solid #C0C0C0;width: 320px; margin-top:20px;"> 
        <asp:FileUpload ID="FileChooser" runat="server" /> 
        <br />
        <asp:Button ID="BtnUpload" runat="server" Text="Upload file" Font-Names="Calibri" Width="100px" />
        <input id="BtnClosed" type="button" value="Close" onclick="window.close()"  style="font-family:Calibri; width:100px;"/>
        <span>(max 600 KB)</span>
        <br />    
    </div> 
                    
       <input type="hidden" id="Id1" runat="server"/>
       <input type="hidden" id="Id2" runat="server"/>
       <input type="hidden" id="Id3" runat="server"/>
       <input type="hidden" id="NoProg" runat="server"/>
       <input type="hidden" id="Nomor" runat="server"/>
    
    <div id="divdaftar" style="border: 1px solid #dddddd; font-family: Calibri; padding: 1px; margin: left; overflow-y: scroll; overflow-x: hidden position: relative; width: 320px; height: 180px;">
    List file upload ....     
          
    </div>
    </center>  
    </form>
            
    <script language="javascript" type="text/javascript">
        var wop = window.opener;
        var wincaller = wop.document.forms['aspnetForm'];

        var NoProg = wincaller.elements['NoProg'].value;
        var IdRap = wincaller.elements['IdRap'].value;
        var IdIssue = wincaller.elements['IdIssue'].value;
        var NoAction = wincaller.elements['NoAction'].value;
               
        document.getElementById('NoProg').value = NoProg;
        document.getElementById('Id1').value = IdRap;
        document.getElementById('Id2').value = IdIssue;
        document.getElementById('Id3').value = NoAction;
          
        showLampiran(NoProg, IdRap, IdIssue, NoAction);
    </script>    
</body>
</html>
