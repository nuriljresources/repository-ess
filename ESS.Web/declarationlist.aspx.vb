﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Partial Public Class declarationlist
    Inherits System.Web.UI.Page
    Public tglaw As String
    Public th As String
    Public v1 As String
    Public v2 As String
    Public imonth As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") = "" Or Session("permission") = Nothing Then
            Response.Redirect("Login.aspx", False)
            Exit Sub
        End If

        tglaw = Request(ddlbulan.UniqueID)
        th = Request(ddltahun.UniqueID)
        Dim i As Integer
        ddlbulan.Items.Clear()
        ddlbulan.Items.Add("January")
        ddlbulan.Items.Add("February")
        ddlbulan.Items.Add("March")
        ddlbulan.Items.Add("April")
        ddlbulan.Items.Add("May")
        ddlbulan.Items.Add("June")
        ddlbulan.Items.Add("July")
        ddlbulan.Items.Add("August")
        ddlbulan.Items.Add("September")
        ddlbulan.Items.Add("October")
        ddlbulan.Items.Add("November")
        ddlbulan.Items.Add("December")

        'Session("otorisasi") = "JRN0143"

        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

        sqlConn.Open()
        Dim strcon As String = "select max(transdate) as max_date, min(transdate) as min_date from V_H002"
        Dim dtb As DataTable = New DataTable
        Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
        sda.Fill(dtb)

        v1 = dtb.Rows(0)!min_date.ToString
        v1 = CDate(v1).ToString("yyyy")

        v2 = dtb.Rows(0)!max_date.ToString
        v2 = CDate(v2).ToString("yyyy")

        ddltahun.Items.Clear()
        For i = v1 To v2
            ddltahun.Items.Add(i)
        Next

        ddltahun.SelectedValue = (i - 1).ToString

        Select Case tglaw
            Case "January"
                imonth = 1
            Case "February"
                imonth = 2
            Case "March"
                imonth = 3
            Case "April"
                imonth = 4
            Case "May"
                imonth = 5
            Case "June"
                imonth = 6
            Case "July"
                imonth = 7
            Case "August"
                imonth = 8
            Case "September"
                imonth = 9
            Case "October"
                imonth = 10
            Case "November"
                imonth = 11
            Case "December"
                imonth = 12
            Case Else
                imonth = 1
        End Select
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        GridView1.DataBind()
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        If GridView1.SelectedRow Is Nothing Then Exit Sub

        Dim row As GridViewRow = GridView1.SelectedRow
        Session("vdec") = GridView1.DataKeys(row.RowIndex).Value.ToString
        Response.Redirect("view_dec.aspx?ID=" & GridView1.DataKeys(row.RowIndex).Value.ToString)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        'Dim dt As DateTime
        'tglaw = Format(DateTime.ParseExact(Request(tripfrom.UniqueID), "MM/dd/yyyy", Nothing), "MM/dd/yyyy")
        'tglak = Format(DateTime.ParseExact(Request(tripto.UniqueID), "MM/dd/yyyy", Nothing), "MM/dd/yyyy")
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        Try
            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()

            Dim strcon As String = "select nik, decno, tripno, nik,  (select nama from H_A101 where nik = V_H002.nik) as nama, convert(char(10),transdate,101) as transdate, reqby, reqdate, apprby, apprdate, reviewby, reviewdate, proceedby, proceeddate, convert(char(10),modifiedtime,101) as modifiedtime, stedit, case fstatus when 1 then 'Process' when 0 then 'Unprocess' end as fstatus from V_H002 "
            strcon = strcon + " where month(transdate) = '" + imonth.ToString + "' and YEAR(transdate) = '" + th + "' and stedit <> 2 and nik = '" + Session("niksite") + "' UNION "
            strcon = strcon + "select A.nik, A.decno, A.tripno, A.nik, (select nama from H_A101 where nik = A.nik) as nama, convert(char(10),A.transdate,101) as transdate, A.reqby, A.reqdate, A.apprby, A.apprdate, A.reviewby, A.reviewdate, A.proceedby, A.proceeddate, convert(char(10),A.modifiedtime,101) as modifiedtime, A.stedit, case A.fstatus when 1 then 'Process' when 0 then 'Unprocess' end as fstatus from V_H002 A, V_A004 B "
            strcon = strcon + " where month(A.transdate) = '" + imonth.ToString + "' and  YEAR(A.transdate) = '" + th + "' and stedit <> 2 and A.nik in(B.nikb) and B.nika = '" + Session("niksite") + "' and B.module='TRAVEL' order by transdate"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            GridView1.DataSource = dtb
            GridView1.DataBind()
        Catch ex As Exception

        End Try
    End Sub
End Class