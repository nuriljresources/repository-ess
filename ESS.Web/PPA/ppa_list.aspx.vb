﻿Imports System.Data.SqlClient
Partial Public Class ppa_list
    Inherits System.Web.UI.Page
    Public sdata As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("localdev").ConnectionString)
        Dim icount As Integer

        If Session("ppa_otorisasi") = "" Then
            Response.Redirect("login_ppa.aspx")
        End If

        sqlConn.Open()

        Try

            Dim strcon As String = "select Top 1 A.nobuk, (select nik from tr_ppa_h where tr_ppa_h.nobuk = A.nobuk) as nik, " & _
            "(select score_isi from tr_ppa_d where tr_ppa_d.nobuk = A.nobuk and ms_id = 1) as question_1, " & _
            "(select score_isi from tr_ppa_d where tr_ppa_d.nobuk = A.nobuk and ms_id = 2) as question_2, " & _
            "(select score_isi from tr_ppa_d where tr_ppa_d.nobuk = A.nobuk and ms_id = 3) as question_3, " & _
            "(select score_isi from tr_ppa_d where tr_ppa_d.nobuk = A.nobuk and ms_id = 4) as question_4, " & _
            "(select score_isi from tr_ppa_d where tr_ppa_d.nobuk = A.nobuk and ms_id = 5) as question_5, " & _
            "(select score_isi from tr_ppa_d where tr_ppa_d.nobuk = A.nobuk and ms_id = 6) as question_6, " & _
            "(select score_isi from tr_ppa_d where tr_ppa_d.nobuk = A.nobuk and ms_id = 7) as question_7, " & _
            "(select score_isi from tr_ppa_d where tr_ppa_d.nobuk = A.nobuk and ms_id = 8) as question_8, " & _
            "(select score_isi from tr_ppa_d where tr_ppa_d.nobuk = A.nobuk and ms_id = 9) as question_9 " & _
            "from tr_ppa_d A, tr_ppa_h B where A.nobuk = B.nobuk and B.nik = '" + Session("ppa_niksite") + "' group by A.nobuk"

            Dim dtb As DataTable = New DataTable
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            If dtb.Rows.Count > 0 Then
                For icount = 0 To dtb.Rows.Count - 1
                    sdata = sdata + "<tr><td><a href='ppa_list.aspx?num=" + dtb.Rows(icount)!nobuk + "'>" + dtb.Rows(icount)!nobuk + "</a></td></tr>"
                Next
            End If

            If Request.QueryString("num").ToString <> "" Then
                Dim strcon2 As String = "select nobuk, ms_id, score_isi, komentar_isi from tr_ppa_d where nobuk = '" + Request.QueryString("num").ToString + "'"
                Dim dtb2 As DataTable = New DataTable
                Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn)
                sda2.Fill(dtb2)

                GridView1.DataSource = dtb2
                GridView1.DataBind()
            End If

        Catch ex As Exception

        End Try
    End Sub

End Class