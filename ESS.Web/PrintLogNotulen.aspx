<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="PrintLogNotulen.aspx.vb" Inherits="EXCELLENT.PrintLogNotulen" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ PreviousPageType VirtualPath="~/SMeeting.aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="divhead">
    <table width="100%" class="data-table-1" cellpadding="0" cellspacing="0">
        <caption>PRINT LOG MINUTES OF MEETING</caption>
    </table>
    </div>
    
    <div id="divcetak" style="border: 1px solid #000000">
    <center>
        
        <asp:SqlDataSource ID="SqlDataSourceHR" runat="server" 
            ConnectionString="<%$ ConnectionStrings:EXCELLENTConnectionString %>" 
            SelectCommand="SELECT [IdRap], [NmRap], [TmpRap], [WkRap], [SdWkRap], [TglRap], [Agenda], [Pimpinan], [NmPimpinan], [Notulis], [NmNotulis], [JenisRap] FROM [HRAPAT]">
        </asp:SqlDataSource>
    
        <rsweb:ReportViewer ID="ReportViewer" runat="server"  runat="server" Height="600px" 
            Width="95%" BackColor="White" SizeToReportContent="True" 
            ShowBackButton="True" Font-Names="Verdana" Font-Size="8pt">
            <localreport reportpath="Laporan\Logmeeting.rdlc">
                <datasources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                        Name="Excellent_HRAPAT" />
                </datasources>
            </localreport>
        </rsweb:ReportViewer>        
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            SelectMethod="GetData" 
            TypeName="EXCELLENT.ExcellentTableAdapters.HRAPATTableAdapter">
        </asp:ObjectDataSource>
        <br />
        
    </center>
    </div>
</asp:Content>
