﻿Imports System.Data.SqlClient

Partial Public Class masterkpifunction
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") = "" Or Session("permission") = Nothing Then
            Response.Redirect("../Login.aspx", False)
            Exit Sub
        ElseIf ("1;8").IndexOf(Session("permission")) = -1 Then 'MDV + PIC PDCA
            Response.Redirect("../default.aspx", False)
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim dr As SqlDataReader = Nothing
            Try
                Dim type As String = ""
                type = Request.QueryString("type")
                Dim cmd As SqlCommand
                Dim query As String = "select id,kdsite,active from site order by id"
                Dim dt As New DataTable
                cmd = New SqlCommand(query, conn)
                conn.Open()
                'Initialize Function
                query = "select ID,FunctionName,active from MFunction order by ID"
                cmd = New SqlCommand(query, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    If Session("permission") = "1" Then 'GRUP MDV
                        KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                    ElseIf Session("permission") = "8" Then 'PIC PDCA
                        If Session("bscdivisi") = dr(0) Then
                            KPIFunc.Items.Add(New ListItem(dr(1), dr(0)))
                        End If
                    End If
                End While
                KPIFunc.Visible = True
                dr.Close()
                dr = Nothing
                'Initialize SO
                query = "select ID,SO from MSO order by ID"
                cmd = New SqlCommand(query, conn)
                dr = cmd.ExecuteReader
                While dr.Read
                    SO.Items.Add(New ListItem(dr(1), dr(0)))
                End While
                dr.Close()
                dr = Nothing
            Catch ex As Exception
                lblMessage.Text = ex.Message
            Finally
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If
                If dr IsNot Nothing Then
                    dr = Nothing
                End If
            End Try
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim query As String = ""
            Dim result As String
            Dim cmd As SqlCommand
            If KPIID.Value = "[AUTO]" Then
                query = "BEGIN TRANSACTION; BEGIN TRY "
                query += "insert into MKPI " _
                        & " (Level,SOID,Name,Func,Jobsite,Section,UoM,Owner,Type,[Desc],Period,Leads,Formula,FormulaDesc,RedParam,GreenParam,BasedOn,CalcType,Parent,CreatedBy,CreatedIn) values " _
                        & "('" & KPILevel.SelectedValue & "','" & SO.SelectedValue & "','" & KPIName.Text & "'," _
                        & "'" & KPIFunc.SelectedValue & "','','','" _
                        & KPIUOM.Text & "','" & KPIOwner.Text & "','" & KPIType.SelectedValue() & "','" _
                        & KPIDesc.Text & "','" & KPIPeriod.SelectedValue & "','" & KPILeads.Text & "','" _
                        & KPIFormula.SelectedValue & "','" & KPIFDesc.Text & "','" & KPIRed.Text & "','" _
                        & KPIGreen.Text & "','" & KPIBased.SelectedValue & "','" & KPICalcType.SelectedValue & "','','" & Request.UserHostName & "','" & Request.UserHostName & "');"
                query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select IDENT_CURRENT('MKPI');end "
            Else
                query = "BEGIN TRANSACTION; BEGIN TRY "
                query += "Update MKPI Set " _
                        & "SOID='" & SO.SelectedValue & "',Name='" & KPIName.Text & "'," _
                        & "UoM='" & KPIUOM.Text & "', Owner='" & KPIOwner.Text & "'," _
                        & "Type='" & KPIType.SelectedValue() & "', [Desc]='" & KPIDesc.Text & "'," _
                        & "Period='" & KPIPeriod.SelectedValue & "',Leads='" & KPILeads.Text & "'," _
                        & "Formula='" & KPIFormula.SelectedValue & "',FormulaDesc='" & KPIFDesc.Text & "'," _
                        & "RedParam=" & KPIRed.Text & ",GreenParam=" & KPIGreen.Text & "," _
                        & "BasedOn='" & KPIBased.SelectedValue & "',CalcType='" & KPICalcType.SelectedValue & "'," _
                        & "ModifiedBy='" & Request.UserHostName & "', modifiedtime=getdate() where " _
                        & "ID='" & KPIID.Value & "'"
                query += " END TRY  BEGIN CATCH   SELECT     ERROR_MESSAGE() AS ErrorMessage	,ERROR_NUMBER() AS ErrorNumber        ,ERROR_SEVERITY() AS ErrorSeverity        ,ERROR_STATE() AS ErrorState        ,ERROR_PROCEDURE() AS ErrorProcedure        ,ERROR_LINE() AS ErrorLine;    IF @@TRANCOUNT > 0        ROLLBACK TRANSACTION;END CATCH;IF @@TRANCOUNT > 0 begin     COMMIT TRANSACTION;	select 0;end "

            End If

            cmd = New SqlCommand(query, conn)
            conn.Open()
            result = cmd.ExecuteScalar()
            If IsNumeric(result) Then
                If result = 0 Then
                    lblMessage.Text = "**Update Berhasil**ID KPI LEVEL BUMA:" & CStr(result)
                Else
                    KPIID.Value = result
                    lblMessage.Text = "**Save Berhasil**ID KPI LEVEL BUMA:" & CStr(KPIID.Value)
                End If
            Else
                lblMessage.Text = result
            End If
        Catch ex As Exception
            lblMessage.Text = ex.Message
        Finally
            conn.Close()
        End Try
    End Sub

    ''' <summary>
    ''' Value="0" Text=BUMA
    ''' Value="1" Text="Function"
    ''' Value="2" Text="Section"
    ''' Value="3" Text="Jobsite"
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GenerateFunction() As String
        Try
            Dim result As String = ""
            Select Case KPILevel.SelectedValue
                Case 0
                    Return ""
                Case 1
                    For Each c As ListItem In KPIFunc.Items
                        If c.Selected Then
                            result += "1"
                        Else
                            result += "0"
                        End If
                    Next
                    Return result
                Case 2
                    Return ""
                Case 3
                    Return ""
            End Select
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
        Return ""
    End Function
End Class