﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/POAPR/poapr.Master" CodeBehind="prepay_entry.aspx.vb" Inherits="EXCELLENT.prepay_entry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PRE-PAYMENT
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .tb
        {
        	width:100px;
        }
        .tbtx
        {
        	width:200px;
        	margin-right:10px;
        	text-align:right;
        }
        
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <table class="data-table breadcrumb" style="width: 100%;">
    <tr>
            <td class="tb">
                Po No
            </td>
            <td class="tbtx">
                <asp:TextBox ID="txtpono" runat="server" class="tb10 tbtx" AutoPostBack="true" OnTextChanged="txtpono_TextChanged"></asp:TextBox>
            </td>
            <td class="tb">
                Date
            </td>
            <td>
                <asp:TextBox ID="txtdate" runat="server" class="tb10 tbtx"></asp:TextBox>
            </td>
            <td class="tb">
                Supplier Name
            </td>
            <td>
                <asp:TextBox ID="txtsupname" runat="server" class="tb10 tbtx"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <table class="data-table breadcrumb" style="width: 100%;">
        <tr>
            <td class="tb">
                Bank Name
            </td>
            <td class="tbtx">
                <asp:TextBox ID="txtbanktname" runat="server" class="tb10 tbtx"></asp:TextBox>
            </td>
            <td></td>
        </tr>
        <tr>
            <td class="tb">
                Account Name
            </td>
            <td class="tbtx">
                <asp:TextBox ID="txtaccname" runat="server" class="tb10 tbtx"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="tb">
                Account No
            </td>
            <td class="tbtx">
                <asp:TextBox ID="txtaccno" runat="server" class="tb10 tbtx"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="tb">
                SWIFT Code
            </td>
            <td class="tbtx">
                <asp:TextBox ID="txtswiftcd" runat="server" class="tb10 tbtx"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="tb">
                Amount
            </td>
            <td class="tbtx">
                <asp:TextBox ID="txtamount" runat="server" class="tb10 tbtx"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="tb">
                Amount To Be Paid
            </td>
            <td class="tbtx">
                <asp:TextBox ID="txtamounttopaid" runat="server" class="tb10 tbtx"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>