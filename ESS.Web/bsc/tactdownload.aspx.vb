﻿Imports System.Data.SqlClient
Imports System
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography

Partial Public Class tactdownload
    Inherits System.Web.UI.Page
    Private Const saltkey As String = "@dmin1t8um4H0"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("permission") = "" Or Session("permission") Is Nothing Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If
        Try
            Dim conn As SqlConnection
            Dim cmd As SqlCommand

            Dim imgByte As Byte() = Nothing

            Dim sekuel As String = ""
            Dim n As String = Request.QueryString("n")
            Dim dummy As String = ""
            n = Right(n, Len(n) - 5)
            For i As Integer = 1 To Len(n)
                dummy += MAPNUMBER(Mid(n, i, 1))
            Next
            sekuel = "SELECT NmFile, MimeType, MimeData, MimeLength FROM TACTLAMPIRAN WHERE Nomor = " + dummy

            conn = New SqlConnection(ConfigurationManager.ConnectionStrings("EXCELLENTConnectionString").ConnectionString)
            cmd = New SqlCommand(sekuel, conn)
            conn.Open()
            Dim sdr As SqlDataReader = cmd.ExecuteReader()

            Dim imgType As String = ""
            Dim nmFile As String = ""
            Dim imgLen As Integer
            While sdr.Read()
                imgLen = sdr("MimeLength")
                imgType = sdr("MimeType")
                nmFile = sdr("NmFile")
                imgByte = sdr("MimeData")
            End While
            conn.Close()

            nmFile = nmFile.Replace(" ", "")

            Response.AddHeader("Content-Disposition", "attachment; filename=" + nmFile)
            Dim bw As BinaryWriter = New BinaryWriter(Response.OutputStream)
            bw.Write(imgByte)
            bw.Close()
            Response.ContentType = imgType
            Response.End()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Public Shared Function Decrypt(ByVal cipher As String, Optional ByVal password As String = saltkey) As String
        Dim ciphertext As String
        ciphertext = cipher

        Try
            Dim rijndaelCipher As New RijndaelManaged()
            rijndaelCipher.Padding = PaddingMode.PKCS7

            Dim ciphertextByte As Byte() = Convert.FromBase64String(ciphertext)
            Dim saltByte As Byte() = Encoding.ASCII.GetBytes(password)

            Dim secretKey As New Rfc2898DeriveBytes(password, saltByte)
            Dim decryptor As ICryptoTransform = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16))
            Dim memoryStream As New MemoryStream(ciphertextByte)
            Dim cryptoStream As New CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)

            Dim plainText As Byte() = New Byte(ciphertextByte.Length - 1) {}

            Dim decryptedCount As Integer = cryptoStream.Read(plainText, 0, plainText.Length)

            memoryStream.Close()
            cryptoStream.Close()

            Return Encoding.Unicode.GetString(plainText, 0, decryptedCount)
        Catch e As Exception
            Throw New InvalidDataException("[" + e.ToString() + " : " + e.Message + "]  Data corrupt")
        End Try
    End Function

    Function MAPNUMBER(ByVal x As String) As String
        Select Case x
            Case "X"
                Return "0"
            Case "Y"
                Return "1"
            Case "Z"
                Return "2"
            Case "A"
                Return "3"
            Case "B"
                Return "4"
            Case "C"
                Return "5"
            Case "D"
                Return "6"
            Case "E"
                Return "7"
            Case "F"
                Return "8"
            Case "G"
                Return "9"
        End Select
    End Function
End Class