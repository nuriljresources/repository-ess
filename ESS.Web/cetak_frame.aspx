﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="cetak_frame.aspx.vb" Inherits="EXCELLENT.cetak_frame" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Print Inpatient
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="598px" 
            Width="1148px" Font-Names="Verdana" Font-Size="8pt">
            <LocalReport ReportPath="Laporan\cetak_frame.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                        Name="medic_H_H21502" />
                </DataSources>
            </LocalReport>
            
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            DeleteMethod="Delete" InsertMethod="Insert" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            TypeName="EXCELLENT.medicTableAdapters.H_H21502TableAdapter" 
            UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_NoReg" Type="String" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="KdSite" Type="String" />
                <asp:Parameter Name="TglTrans" Type="DateTime" />
                <asp:Parameter Name="Tipe" Type="String" />
                <asp:Parameter Name="Nik" Type="String" />
                <asp:Parameter Name="fstatus" Type="String" />
                <asp:Parameter Name="TipeLensa" Type="String" />
                <asp:Parameter Name="TipeLensa2" Type="String" />
                <asp:Parameter Name="Claim" Type="Decimal" />
                <asp:Parameter Name="ClaimFrame" Type="Decimal" />
                <asp:Parameter Name="Keterangan" Type="String" />
                <asp:Parameter Name="CreatedBy" Type="String" />
                <asp:Parameter Name="CreatedIn" Type="String" />
                <asp:Parameter Name="CreatedTime" Type="DateTime" />
                <asp:Parameter Name="ModifiedBy" Type="String" />
                <asp:Parameter Name="ModifiedIn" Type="String" />
                <asp:Parameter Name="ModifiedTime" Type="DateTime" />
                <asp:Parameter Name="StEdit" Type="String" />
                <asp:Parameter Name="DeleteBy" Type="String" />
                <asp:Parameter Name="DeleteTime" Type="DateTime" />
                <asp:Parameter Name="pycostcode" Type="String" />
                <asp:Parameter Name="Expr1" Type="String" />
                <asp:Parameter Name="Expr2" Type="Decimal" />
                <asp:Parameter Name="Expr3" Type="Decimal" />
                <asp:Parameter Name="Original_NoReg" Type="String" />
            </UpdateParameters>
            <SelectParameters>
                <asp:SessionParameter Name="noreg" SessionField="printframe" Type="String" />
            </SelectParameters>
            <InsertParameters>
                <asp:Parameter Name="NoReg" Type="String" />
                <asp:Parameter Name="KdSite" Type="String" />
                <asp:Parameter Name="TglTrans" Type="DateTime" />
                <asp:Parameter Name="Tipe" Type="String" />
                <asp:Parameter Name="Nik" Type="String" />
                <asp:Parameter Name="fstatus" Type="String" />
                <asp:Parameter Name="TipeLensa" Type="String" />
                <asp:Parameter Name="TipeLensa2" Type="String" />
                <asp:Parameter Name="Claim" Type="Decimal" />
                <asp:Parameter Name="ClaimFrame" Type="Decimal" />
                <asp:Parameter Name="Keterangan" Type="String" />
                <asp:Parameter Name="CreatedBy" Type="String" />
                <asp:Parameter Name="CreatedIn" Type="String" />
                <asp:Parameter Name="CreatedTime" Type="DateTime" />
                <asp:Parameter Name="ModifiedBy" Type="String" />
                <asp:Parameter Name="ModifiedIn" Type="String" />
                <asp:Parameter Name="ModifiedTime" Type="DateTime" />
                <asp:Parameter Name="StEdit" Type="String" />
                <asp:Parameter Name="DeleteBy" Type="String" />
                <asp:Parameter Name="DeleteTime" Type="DateTime" />
                <asp:Parameter Name="pycostcode" Type="String" />
                <asp:Parameter Name="Expr1" Type="String" />
                <asp:Parameter Name="Expr2" Type="Decimal" />
                <asp:Parameter Name="Expr3" Type="Decimal" />
            </InsertParameters>
        </asp:ObjectDataSource>

</asp:Content> 