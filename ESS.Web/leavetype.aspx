﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="leavetype.aspx.vb" Inherits="EXCELLENT.leavetype" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="css/site.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function UseSelected(ltype, nama, kddepar, nmjabat, kdsite, kdjabat, kddepart, nik, costcode) {
            var pw = window.opener;
            var inputFrm = pw.document.forms['aspnetForm'];
            if (inputFrm.elements['ctrlToFind'].value == 'Originator') {

            } else if (inputFrm.elements['ctrlToFind'].value == 'PenanggungJawab') {

            } else if (inputFrm.elements['ctrlToFind'].value == 'leatype') {
                inputFrm.elements['ctl00_ContentPlaceHolder1_txtleavetype'].value = nama;
                inputFrm.elements['ctl00_ContentPlaceHolder1_leavetype'].value = ltype;
                pw.document.getElementById("ctl00_ContentPlaceHolder1_txtdayoff").disabled = true;
                inputFrm.elements['ctl00_ContentPlaceHolder1_txtdayoff'].value = '';

                if (ltype == '03') {

                    if (inputFrm.elements['Img2'] != undefined) {
                        inputFrm.elements['Img2'].src = 'images/Search.gif';
                        inputFrm.elements['Img2'].style.height = 15;
                    }
                } else {
                    if (ltype == '00') {
                        inputFrm.elements['ctl00_ContentPlaceHolder1_txtholiday'].disabled = false;
                        inputFrm.elements['ctl00_ContentPlaceHolder1_txtholiday'].value = '';
                    } else {

                    if (ltype == '07') {
                        pw.document.getElementById("ctl00_ContentPlaceHolder1_txtdayoff").disabled = false;
                    }
                        
                        
                        inputFrm.elements['ctl00_ContentPlaceHolder1_txtpaidleave'].value = '';
                        inputFrm.elements['ctl00_ContentPlaceHolder1_paidleave'].value = '';



                    }
                }
            } else {
                inputFrm.elements[inputFrm.elements['ctrlToFind'].value].value = ltype;
                inputFrm.elements[inputFrm.elements['ctrlToFind'].value + 'name'].value = nama;
            }
            window.close();
        }
        function Left(str, n) {
            if (n <= 0)
                return "";
            else if (n > String(str).length)
                return str;
            else
                return String(str).substring(0, n);
        }

        function Right(str, n) {
            if (n <= 0)
                return "";
            else if (n > String(str).length)
                return str;
            else {
                var iLen = String(str).length;
                return String(str).substring(iLen, iLen - n);
            }
        }

        function keyPressed(e) {
            switch (e.keyCode) {
                case 13:
                    {
                        document.getElementById("btnSearch").click();
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        function cekArgumen() {
            var d = document;
            if ((d.getElementById("SearchKey").value.length < 3) || (d.getElementById("SearchKey").value.length == 0)) {
                alert("Please Entry Specific NIK / Name (minimal 3 Character)");
                return false;
            }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" />
    <asp:UpdatePanel ID="MyUpdatePanel" runat="server">
        <ContentTemplate>
            <div>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table">
                    <caption style="text-align: center">
                        Search Employee</caption>
                    <tr>
                        <td style="padding: 5px; width: 20%">
                        </td>
                        <td style="width: 15%">
                            <%--<asp:TextBox ID="SearchKey" runat="server" class="tb10" onkeydown="keyPressed(event);"/>--%>
                            <asp:DropDownList ID="SearchKey" runat="server" class="tb10" Visible="False">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <input type="text" id="hide" style="display: none;" /><asp:Button ID="btnSearch"
                                runat="server" Text="Cari Nama" Font-Bold="False" ForeColor="#0033CC" OnClientClick="cekArgumen()"
                                Height="26px" Width="90px" Visible="False" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Repeater ID="rptPages" runat="server">
                                <HeaderTemplate>
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="vertical-align: top">
                                                <b>Page:</b>&nbsp;
                                            </td>
                                            <td>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                        CssClass="text" runat="server" Visible='<%# (CInt(Container.DataItem) <> (PageNumber+1)).ToString() %>'>
                         <%#Container.DataItem%> 
                                    </asp:LinkButton>
                                    <asp:Label ID="lblPage" runat="server" Visible='<%# (CInt(Container.DataItem) = (PageNumber+1)).ToString()%>'
                                        Text="<%#Container.DataItem%>" ForeColor="black" Font-Bold="true">
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </td> </tr> </td> </table>
                                </FooterTemplate>
                            </asp:Repeater>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Repeater ID="rptResult" runat="server">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="data-table-inside">
                                        <tr>
                                            <th>
                                                No.
                                            </th>
                                            <th>
                                                Type Leave
                                            </th>
                                            <th>
                                                Description
                                            </th>
                                            <%--<th>Kd. Departemen</th>--%>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <%# Container.ItemIndex + 1 %>
                                        </td>
                                        <td>
                                            <a href="#" onclick="UseSelected('<%#DataBinder.Eval(Container.DataItem, "typeLeave")%>','<%#DataBinder.Eval(Container.DataItem, "typeDesc")%>','<%#DataBinder.Eval(Container.DataItem, "typeLeave")%>','<%#DataBinder.Eval(Container.DataItem, "typeLeave")%>','<%#DataBinder.Eval(Container.DataItem, "typeLeave")%>','<%#DataBinder.Eval(Container.DataItem, "typeLeave")%>','<%#DataBinder.Eval(Container.DataItem, "typeLeave")%>','<%#DataBinder.Eval(Container.DataItem, "typeLeave")%>','<%#DataBinder.Eval(Container.DataItem, "typeLeave")%>');">
                                                <%#DataBinder.Eval(Container.DataItem, "typeDesc")%></a>
                                        </td>
                                        <td>
                                            <%#DataBinder.Eval(Container.DataItem, "typeLeave")%>
                                        </td>
                                        <%--<td align="center"><%#DataBinder.Eval(Container.DataItem, "KdDepar")%></td>--%>
                                        <%--<td align="center"><%#DataBinder.Eval(Container.DataItem, "KdSite")%></td>--%>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="progress1" runat="server" DisplayAfter="50">
        <ProgressTemplate>
            <asp:Panel ID="pnlProgressBar" runat="server">
                <div>
                    <asp:Image ID="Image1" runat="Server" ImageUrl="images/ajax-loader.gif" />Please
                    Wait...</div>
            </asp:Panel>
        </ProgressTemplate>
    </asp:UpdateProgress>
    </form>
</body>
</html>
