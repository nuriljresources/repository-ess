﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="up.aspx.vb" Inherits="EXCELLENT.up" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="err" runat="server"></asp:TextBox>
        <asp:FileUpload ID="FileUpload1" runat="server" />
        <asp:Button ID="Button1" runat="server" style="height: 26px" Text="upload" /><br />
 
        <asp:GridView ID="GridView1" runat="server" 
                Width="100%" Height="80%" CssClass="data-table" AutoGenerateColumns="False" 
                  DataKeyNames = "fname">
            <Columns>
            <asp:BoundField DataField="No" HeaderText="No" ItemStyle-Width = "20px" HeaderStyle-Width = "20px" />
            <asp:CommandField ShowSelectButton="True" SelectText="Select" ButtonType="Link" HeaderStyle-Width="50px" ItemStyle-VerticalAlign="top" >
                <HeaderStyle Width="50px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:CommandField>
                <asp:BoundField DataField="fname" HeaderText="File Name" ItemStyle-Width = "100px" HeaderStyle-Width = "100px" />
                
                <asp:BoundField DataField="path" HeaderText="Path" HeaderStyle-Width = "200px" 
                    ItemStyle-Width = "200px" ItemStyle-Wrap = "true" >
<HeaderStyle Width="200px"></HeaderStyle>

<ItemStyle Width="200px"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderText="File Open" HeaderStyle-Width="50px">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" Text='Open' NavigateUrl='<%# eval("path") %>' Target="_blank" runat="server"></asp:HyperLink>
                </ItemTemplate>

<HeaderStyle Width="50px"></HeaderStyle>
                </asp:TemplateField> 
            </Columns>
            <SelectedRowStyle Font-Bold="True" />
            <PagerStyle HorizontalAlign="Left" />
            <HeaderStyle Font-Bold="True" Width="100px" />
            <RowStyle Font-Bold="True" Width="100px" Wrap="True" />
        </asp:GridView>
    </div>
    </form>
</body>
</html>
