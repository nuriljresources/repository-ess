﻿Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports EXCELLENT.BSCDATASET

Partial Public Class ShowGraph
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
        Try
            Dim cmd As SqlCommand
            Dim dr As SqlDataReader
            Select Case Request.QueryString("c")
                Case "1"
                    Dim isZero As String = ""
                    Dim year As String = Request.QueryString("y")
                    Dim func As String = Request.QueryString("f")
                    Dim dt As New BSCDATASET.FULLYEARDataTable()
                    cmd = New SqlCommand
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "dbo.sp_summarylevel1perfunc"
                    cmd.Parameters.Add(New SqlParameter("@year", year))
                    cmd.Parameters.Add(New SqlParameter("@func", func))
                    cmd.Connection = conn
                    conn.Open()
                    dr = cmd.ExecuteReader
                    Dim row As BSCDATASET.FULLYEARRow
                    While dr.Read
                        row = dt.NewFULLYEARRow
                        row.BULAN = dr("Month")
                        row.VALUE = CDec(dr("ach")).ToString("#,##0.00")
                        row.TIPE = "M"
                        If dr("ach") = 0 Then
                            isZero += dr("Month") + ";"
                        End If
                        dt.AddFULLYEARRow(row)
                    End While
                    dr.Close()
                    cmd = New SqlCommand
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "dbo.sp_yearlyfunction"
                    cmd.Parameters.Add(New SqlParameter("@year", year))
                    cmd.Parameters.Add(New SqlParameter("@func", func))
                    cmd.Parameters.Add(New SqlParameter("@month", "12"))
                    cmd.Connection = conn
                    dr = cmd.ExecuteReader
                    While dr.Read
                        row = dt.NewFULLYEARRow
                        row.BULAN = dr("Month")
                        If isZero.IndexOf(dr("Month") + ";") > -1 Then
                            row.VALUE = 0
                        Else
                            row.VALUE = CDec(dr("ach")).ToString("#,##0.00")
                        End If
                        row.TIPE = "Y"

                        dt.AddFULLYEARRow(row)
                    End While
                    dr.Close()
                    dr = Nothing
                    conn.Close()

                    ReportViewer1.LocalReport.ReportPath = "Laporan\rptGraph.rdlc"
                    ReportViewer1.LocalReport.DataSources.Clear()
                    ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource("BSCDATASET_FULLYEAR", dt))
                    ReportViewer1.LocalReport.Refresh()
                Case "11"
                    Dim isZero As String = ""
                    Dim year As String = Request.QueryString("y")
                    Dim func As String = Request.QueryString("f")
                    Dim query As String
                    Dim js As String
                    Dim secid As String
                    Dim dt As New BSCDATASET.FULLYEARDataTable()
                    cmd = New SqlCommand
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "dbo.sp_summarylevel2perfunc"
                    cmd.Parameters.Add(New SqlParameter("@year", year))
                    cmd.Parameters.Add(New SqlParameter("@func", func))
                    cmd.Connection = conn
                    conn.Open()
                    dr = cmd.ExecuteReader
                    Dim row As BSCDATASET.FULLYEARRow
                    While dr.Read
                        If dr("SectionName") = Request.QueryString("s") And dr("kdsite") = Request.QueryString("js") Then
                            row = dt.NewFULLYEARRow
                            row.BULAN = dr("Month")
                            row.VALUE = CDec(dr("ach")).ToString("#,##0.00")
                            row.TIPE = "M"
                            If dr("ach") = 0 Then
                                isZero += dr("Month") + ";"
                            End If
                            dt.AddFULLYEARRow(row)
                        End If
                    End While
                    dr.Close()
                    Dim connSearch As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
                    Try
                        Dim cmdsearch As SqlCommand
                        query = "select id from site where kdsite='" & Request.QueryString("js") & "'"
                        connSearch.Open()
                        cmdsearch = New SqlCommand(query, connSearch)
                        js = cmdsearch.ExecuteScalar()
                        cmdsearch = New SqlCommand("select id from MSECTION where SectionName='" & Request.QueryString("s") & "'", connSearch)
                        secid = cmdsearch.ExecuteScalar()
                        connSearch.Close()
                    Catch ex As Exception

                    Finally
                        If connSearch.State = ConnectionState.Open Then
                            connSearch.Close()
                        End If
                    End Try


                    cmd = New SqlCommand
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "dbo.sp_yearlysection"
                    cmd.Parameters.Add(New SqlParameter("@year", year))
                    cmd.Parameters.Add(New SqlParameter("@section", secid))
                    cmd.Parameters.Add(New SqlParameter("@jobsite", js))
                    cmd.Parameters.Add(New SqlParameter("@month", "12"))
                    cmd.Connection = conn
                    dr = cmd.ExecuteReader
                    While dr.Read
                        row = dt.NewFULLYEARRow
                        row.BULAN = dr("Month")
                        If isZero.IndexOf(dr("Month") + ";") > -1 Then
                            row.VALUE = 0
                        Else
                            row.VALUE = CDec(dr("ach")).ToString("#,##0.00")
                        End If
                        row.TIPE = "Y"
                        dt.AddFULLYEARRow(row)
                    End While
                    dr.Close()
                    dr = Nothing
                    conn.Close()

                    ReportViewer1.LocalReport.ReportPath = "Laporan\rptGraph.rdlc"
                    ReportViewer1.LocalReport.DataSources.Clear()
                    ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource("BSCDATASET_FULLYEAR", dt))
                    ReportViewer1.LocalReport.Refresh()
            End Select
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Sub

End Class