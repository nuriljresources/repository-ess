﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="login_ppa.aspx.vb" Inherits="EXCELLENT.login_ppa" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PPA Login</title>
    <style type="text/css">


body{
  padding: 1em;
  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAAQElEQVQIW2P89OvDfwYo+PHjJ4zJwMHBzsAIk0SXAKkCS2KTAEu++vQSbizIKGQAl0SXAJkGlsQmAbcT2Shk+wH0sCzAEOZW1AAAAABJRU5ErkJggg==);
}
a{
  color: #739931;
}
.page{
  max-width: 70em;
  margin: 0 auto;
}
table th,
table td{
  text-align: left;
}
table.layout{
  width: 100%;
  border-collapse: collapse;
}
table.display{
  margin: 1em 0;
}
table.display th,
table.display td{
  border: 1px solid #B3BFAA;
  padding: .5em 1em;
}

table.display th{ background: #D5E0CC; }
table.display td{ background: #fff; }

table.responsive-table{
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
}

.center{
  text-align:center;
  }
.none{
	border-style:none;
    text-align:center;
  }

@media (max-width: 30em){
    table.responsive-table{
      box-shadow: none;  
    }
    table.responsive-table thead{
      display: none; 
    }
  table.display th,
  table.display td{
    padding: .5em;
  }
    
  table.responsive-table td:nth-child(1):before{
    content: 'Number';
  }
  table.responsive-table td:nth-child(2):before{
    content: 'Name';
  }
  table.responsive-table td:nth-child(1),
  table.responsive-table td:nth-child(2){
    padding-left: 25%;
  }
  table.responsive-table td:nth-child(1):before,
  table.responsive-table td:nth-child(2):before{
    position: absolute;
    left: .5em;
    font-weight: bold;
  }
  
    table.responsive-table tr,
    table.responsive-table td{
        display: block;
    }
    table.responsive-table tr{
        position: relative;
        margin-bottom: 1em;
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
    }
    table.responsive-table td{
        border-top: none;
    }
    table.responsive-table td.organisationnumber{
        background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    table.responsive-table td.actions{
        position: absolute;
        top: 0;
        right: 0;
        border: none;
        background: none;
    }
    table.responsive-table td.top{
    	background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
}

</style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="padding-bottom:20px;">
            <br /><center>        
       <div><center>
       <table class="responsive-table" style="border: 1px solid #D5E0CC; width: 25%">
            <tr>
            <td colspan="2" bgcolor="#D5E0CC" 
                    
                    style="text-align:center;font-size: large; font-family: calibri; font-weight: bold; width:100%">
                PPA LOGIN
            </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label1" runat="server" Text="Domain\UserName" Font-Size="Small">
                </asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox1" runat="server" class="tb10" Width="92%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:40%">
                    <asp:Label ID="Label2" runat="server" Text="Label" Font-Size="Small">Password</asp:Label>
                </td>
                <td style="width:60%">
                    <asp:TextBox ID="TextBox2" TextMode="password" runat="server" class="tb10" Width="92%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">v.1.0.0</td>                
            </tr>
            <tr>    
                <td style="width:40%">
                    <img alt="" src="../images/Locked.png" />
                </td>
                <td style="width:60%">
                    <asp:Button ID="btnLogin" runat="server" Text="Login" Width="80" Font-Names="Calibri"/>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="80" Font-Names="Calibri" />
                </td>
            </tr>
            <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblConfirm" runat="server" Text="" Font-Size="Small" ForeColor="Red"></asp:Label>
            </td>
            </tr>
        </table> </center> 
    </div>
    </center>
    <br />
    <center>Best View In :</center>
    <br />
    <center>
        <img src="Image/ie8logo.jpg" /></center>
    </div>
    <div style="margin-top:-100px; position:relative; top:120px; bottom:0px; clear:both;"><center><img style="width:956px; height:201px;" src="Image/picture1.png"/></center></div>
    </form>
</body>
</html>
