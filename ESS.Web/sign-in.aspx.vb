﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Security.Cryptography
Imports System.IO
Imports System.Text
Imports System.Configuration
Imports System.Collections.Generic
Imports System.Linq
Imports System.DirectoryServices  'Note dicek perlu ditick pada add reference
Imports System.Net.Mail

Partial Public Class sign_in
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLogin.Click
        Dim msg As String
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)
        If Trim(Me.TextBox2.Text) = "" Then
            ' Me.lblConfirm.Text = "Harap Isi Password Anda"
            Me.lblConfirm.Text = "Password Should Not Empty Please Fill Your Password"
            Exit Sub
        End If

        'Try
        '    sqlConn.Open()
        '    Dim dtb As DataTable = New DataTable()
        '    'Dim strcon As String = "select nikuser,password,iduser,nmuser,departemen,map.site as jobsite,Grup,UpGrup,Divisi,isnull(b.bscdivisi,0) as bscdivisi, isnull(b.bscdepar,0) as bscdepar from muser a left join bscfuncmap b on a.nikuser=b.nik left outer join OPENROWSET('SQLNCLI','SERVER=172.16.0.7\hris;UID=IICS;PWD=$IICS-123-ONLINE$', 'SELECT PERNR,PNALT,WERKS,BTRTL from BERP_CENTRAL.dbo.H_A101_sap') x on x.pernr = a.nikuser or x.pnalt=a.nikuser left join MAPSITESAP map on map.WERKS = x.WERKS and map.btrtl=x.btrtl where iduser = '" + Replace(Me.TextBox1.Text, "'", "") + "' and password = '" + SecureIt.Secure.Encrypt(Replace(Me.TextBox2.Text, "'", "")) + "' and stedit in ('0','1')"
        '    'replace
        '    Dim strcon As String = "select nikuser,password,iduser,nmuser,departemen, jobsite,Grup,UpGrup,Divisi,isnull(b.bscdivisi,0) as bscdivisi, isnull(b.bscdepar,0) as bscdepar from muser a left join bscfuncmap b on a.nikuser=b.nik where iduser = '" + Replace(Me.TextBox1.Text, "'", "") + "' and password = '" + SecureIt.Secure.Encrypt(Replace(Me.TextBox2.Text, "'", "")) + "' and stedit in ('0','1')"
        '    Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
        '    sda.Fill(dtb)

        '    If dtb.Rows.Count = 0 Then
        '        Me.lblConfirm.Text = "Periksa Kembali Login Anda ..."
        '        sqlConn.Close()
        '        Exit Sub
        '    End If

        '    If Session("otorisasi") = "" Then
        '        Session("otorisasi") = dtb.Rows(0)("IdUser").ToString
        '        Session("NikUser") = dtb.Rows(0)("NikUser").ToString
        '        Session("NmUser") = dtb.Rows(0)("NmUser").ToString
        '        'Session("departemen") = dtb.Rows(0)("departemen").ToString
        '        Session("jobsite") = dtb.Rows(0)("jobsite").ToString
        '        Session("permission") = dtb.Rows(0)("Grup").ToString
        '        Session("jnsmeeting") = dtb.Rows(0)("UpGrup").ToString
        '        'Session("Divisi") = dtb.Rows(0)("Divisi").ToString
        '        Session("bscdivisi") = dtb.Rows(0)("bscdivisi").ToString
        '        Session("bscdepar") = dtb.Rows(0)("bscdepar").ToString
        '        'Response.Redirect("MyIssueList.aspx")
        '        'Response.Redirect("bsc/defaultbsc.aspx")
        '        If Session("permission") = "9" Then
        '            Response.Redirect("MyIssueList.aspx")
        '        Else
        '       Response.Redirect("bsc/gauge.aspx")
        '        End If
        '        sqlConn.Close()
        '    Else
        '        Session("otorisasi") = dtb.Rows(0)("IdUser").ToString
        '        Session("NikUser") = dtb.Rows(0)("NikUser").ToString
        '        Session("NmUser") = dtb.Rows(0)("NmUser").ToString
        '        'Session("departemen") = dtb.Rows(0)("departemen").ToString
        '        Session("jobsite") = dtb.Rows(0)("jobsite").ToString
        '        Session("permission") = dtb.Rows(0)("Grup").ToString
        '        Session("jnsmeeting") = dtb.Rows(0)("UpGrup").ToString
        '        'Session("Divisi") = dtb.Rows(0)("Divisi").ToString
        '        Session("bscdivisi") = dtb.Rows(0)("bscdivisi").ToString
        '        Session("bscdepar") = dtb.Rows(0)("bscdepar").ToString
        '        'Response.Redirect("bsc/defaultbsc.aspx")
        '        sqlConn.Close()
        '    End If
        'Catch ex As Exception
        '    Me.lblConfirm.Text = ex.Message
        'Finally
        '    If sqlConn.State = ConnectionState.Open Then
        '        sqlConn.Close()
        '    End If
        'End Try

        Dim ls_user, ls_pass, lsErr As String
        Dim lsnik As String
        Dim lbl_login As Boolean = False
        Dim Gsuser As String

        Dim index As Integer = TextBox1.Text.IndexOf("\")
        Dim dm As String
        Dim nm As String
        Dim count As Integer
        If index > 0 Then
            count = TextBox1.Text.Length
            dm = TextBox1.Text.Substring(0, index)
            nm = TextBox1.Text.Substring(index, count - index)
            nm = Replace(nm, "\", "")
        End If

        'ls_user = Mid(TextBox1.Text, 12)
        ls_pass = TextBox2.Text

        'Dim Domain As String = Environment.UserDomainName 'domain otomatic ambil dr login window domain
        'Dim User As String = Environment.UserName
        Dim domain As String = dm
        'Verifikasi Check user password domain   
        lbl_login = ValidateActiveDirectoryLogin(domain, nm, ls_pass, lsnik, lsErr)

        If lbl_login = True Then
            'Check login id.
            If lsnik = "" Then
                'MsgBox("Nik user name : " + ls_user + " not found,Please inform SysAdmin Check user domain setting.", MsgBoxStyle.Exclamation, "Confirmation")
                Me.lblConfirm.Text = "Nik user name : " + ls_user + " not found,Please inform SysAdmin Check user domain setting."
                Return
            Else
                Gsuser = lsnik  ' GsUser yg dipakai untuk tampung NIK user dlm transaksi
                'MsgBox("Sukses login masuk", MsgBoxStyle.Exclamation, Gsuser)
                'Masuk Form Login
                Me.lblConfirm.Text = "Login Success"
            End If
        Else
            'MsgBox(lsErr, MsgBoxStyle.Exclamation, "Confirmation")
            'Me.lblConfirm.Text = lsErr + " " + "user : " + ls_user + " " + "password : " + ls_pass + " " + "nik : " + lsnik + " domain: " + Domain
            Me.lblConfirm.Text = lsErr
            Return
        End If

        'cek nik user didatabase
        Try
            If Not String.IsNullOrEmpty(TextUserId.Text) Then
                lsnik = TextUserId.Text
            End If

            sqlConn.Open()
            Dim dtb As DataTable = New DataTable()
            'HashString
            'Dim strcon As String = "select kduser,nmuser,kdsite,kddivisi,kddepar, kdlevel from B_C012 where kduser = '" + Replace(Me.TextBox1.Text, "'", "") + "' and password = '" + SecureIt.Secure.Encrypt(Replace(Me.TextBox2.Text, "'", "")) + "' and stedit in ('0','1')"
            'Dim strcon As String = "select kduser,nmuser,kdsite,kddivisi,kddepar, kdlevel from B_C012 where kduser = '" + Replace(Me.TextBox1.Text, "'", "") + "' and password = '" + HashString(Replace(Me.TextBox2.Text, "'", "")) + "' and stedit in ('0','1')"
            Dim strcon As String = "select kduser,nmuser,(select kdsite from H_A101 where niksite = B_C012.kduser) as kdsite,kddivisi,(select kddepar from H_A101 where niksite = B_C012.kduser) as kddepar, (select kdlevel from H_A101 where niksite = B_C012.kduser) as kdlevel from B_C012 where kduser = '" + Trim(Replace(lsnik, "'", "")) + "' and stedit in ('0','1')"
            Dim sda As SqlDataAdapter = New SqlDataAdapter(strcon, sqlConn)
            sda.Fill(dtb)

            If dtb.Rows.Count = 0 Then
                Me.lblConfirm.Text = "Please Check Again your login ..."
                sqlConn.Close()
                Exit Sub
            End If

            If Session("otorisasi") = "" Then

                Session("otorisasi") = dtb.Rows(0)("kduser").ToString
                Session("kdsite") = dtb.Rows(0)("kdsite").ToString
                Session("permission") = "1;B"
                Session("NmUser") = dtb.Rows(0)("nmuser").ToString
                Session("medicnoregst") = ""
                Session("medicnoreg") = ""
                Session("medicnoregstout") = ""
                Session("medicnoregout") = ""
                Session("framenoreg") = ""
                Session("framenoregst") = ""
                Session("kddepar") = dtb.Rows(0)("kddepar").ToString
                Session("kdlevel") = dtb.Rows(0)("kdlevel").ToString
                'Response.Redirect("dw_trip_header.aspx")
                Session("otdate1") = ""
                Session("otdate2") = ""
                Session("otnik") = ""
            End If

        Catch ex As Exception
            Me.lblConfirm.Text = ex.Message
        End Try

        Try

            Dim sqlConn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("jrn_citrixcon").ConnectionString)

            Dim dtb2 As DataTable = New DataTable()
            Dim strcon2 As String = "select nik from H_A101 where niksite='" + Session("otorisasi") + "'"
            Dim sda2 As SqlDataAdapter = New SqlDataAdapter(strcon2, sqlConn)
            sda2.Fill(dtb2)

            Session("niksite") = dtb2.Rows(0)("nik").ToString

            Response.Redirect("home.aspx")
        Catch ex As Exception
            Me.lblConfirm.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Session("otorisasi") = ""
        Session("departemen") = ""
        Session("jobsite") = ""
        Session.Abandon()
        Response.Redirect("Logout.aspx")
    End Sub

    'Private Sub btnConvert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConvert.Click
    '    Try

    '    Catch ex As Exception
    '        Me.lblConfirm.Text = ex.Message
    '    Finally

    '    End Try

    'End Sub

    Public Function HashString(ByVal Text As String) As String
        Dim hashedPassword As String = ""
        Dim hashProvider As SHA256Managed
        Try
            Dim passwordBytes() As Byte
            'Dim hashBytes() As Byte
            passwordBytes = System.Text.Encoding.Unicode.GetBytes(Text)
            hashProvider = New SHA256Managed()
            passwordBytes = hashProvider.ComputeHash(passwordBytes)
            hashedPassword = Convert.ToBase64String(passwordBytes)
        Catch ex As Exception
            'logger.LogRequest("At HashString: " + ex.Message)
            MsgBox(ex.Message)
        Finally
            If Not hashProvider Is Nothing Then
                hashProvider.Clear()
                hashProvider = Nothing
            End If
        End Try
        Return hashedPassword
    End Function

    Private Function ValidateActiveDirectoryLogin(ByVal Domain As String, ByVal Username As String, ByVal Password As String, ByRef NIK As String, ByRef msgErr As String) As Boolean
        Dim Success As Boolean = False
        Dim Entry As New System.DirectoryServices.DirectoryEntry("LDAP://" & Domain, Username, Password)
        Dim Searcher As New System.DirectoryServices.DirectorySearcher(Entry)
        Dim RetArray As New Hashtable()
        Dim oResult As SearchResult
        Dim oResults As SearchResultCollection
        Dim lsnik As String
        Dim msg As String

        Searcher.PropertiesToLoad.Add("SAMAccountName")
        Searcher.PropertiesToLoad.Add("givenname")
        Searcher.PropertiesToLoad.Add("employeeid")
        Searcher.PropertiesToLoad.Add("mail")

        Searcher.SearchScope = DirectoryServices.SearchScope.Subtree


        Try
            Dim Results As System.DirectoryServices.SearchResult = Searcher.FindOne
            Success = Not (Results Is Nothing)

            If Success = True Then
                Searcher.Filter = "(samAccountName=" + Username + ")"

                Dim Resultsxx As System.DirectoryServices.SearchResultCollection = Searcher.FindAll

                For Each oResult In Resultsxx
                    Dim de As System.DirectoryServices.DirectoryEntry = oResult.GetDirectoryEntry()

                    If de.Properties("GivenName").Value() Is Nothing Then
                    Else
                        lsnik = de.Properties("GivenName").Value().ToString()
                    End If


                    If de.Properties("employeeid").Value() Is Nothing Then
                        lsnik = ""
                    Else
                        lsnik = de.Properties("employeeid").Value().ToString()
                    End If

                Next

                NIK = lsnik
            End If


        Catch ex As Exception
            msgErr = ex.Message
            Success = False

        End Try
        Return Success
    End Function
End Class