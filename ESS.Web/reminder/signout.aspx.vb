﻿Public Partial Class signout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("otorisasi_permit") = ""
        Session("permission_permit") = ""
        Session("NmUser_permit") = ""
        Session("dept") = ""
        Session("divisi") = ""
        Session("modul_permit") = ""
        Session.Clear()
        Session.Abandon()
        Response.Redirect("signin_permit.aspx")
    End Sub

End Class