﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="cetak_inpatient.aspx.vb" Inherits="EXCELLENT.cetak_inpatient" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Print Inpatient
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
        Font-Size="8pt" Height="598px" Width="1148px">
        <LocalReport ReportPath="Laporan\cetak_inpatient.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                    Name="medic_DataTable2" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" 
                    Name="medic_DataTable1" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="EXCELLENT.medicTableAdapters.DataTable1TableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="tipe" QueryStringField="tipe" Type="String" />
            <asp:SessionParameter Name="noreg" SessionField="printinpatient" 
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="EXCELLENT.medicTableAdapters.DataTable2TableAdapter">
        <SelectParameters>
            <asp:SessionParameter Name="noreg" SessionField="printinpatient" 
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>

</asp:Content> 