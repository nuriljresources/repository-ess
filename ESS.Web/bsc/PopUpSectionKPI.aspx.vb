﻿Imports System.Data.SqlClient

Partial Public Class PopUpSectionKPI
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        BindGrid()
    End Sub

    Private Sub srchResult_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles srchResult.PageIndexChanging
        BindGrid()
        srchResult.PageIndex = e.NewPageIndex
        srchResult.DataBind()
    End Sub

    Sub BindGrid()
        Try
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("BSCConnectionString").ConnectionString)
            Dim query As String = ""

            Select Case Request.QueryString("type")
                Case "0"
                    query = "select a.ID,p.abbr as Perspective,a.Name from MKPI a left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='0' order by p.id,a.name"
                Case "1"
                    query = "select a.ID,p.abbr as Perspective,a.Name,c.functionname from MKPI a inner join MFUNCTION c on a.Func=c.ID left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='1' order by p.id,a.func,a.name"
                Case "2"
                    query = "select a.ID,p.abbr as Perspective,a.Name,d.kdsite,c.sectionname from MKPI a inner join MSECTION c on a.section=c.ID inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='2' order by p.id,a.section,a.name"
                Case "3"
                    query = "select a.ID,p.abbr as Perspective,a.Name,d.kdsite,c.sectionname from MKPI a inner join MSECTION c on a.section=c.ID inner join SITE d on a.jobsite=d.id left join MSO so on a.soid=so.id left join  dbo.MST st on so.stid=st.stid left join MPERSPECTIVE p on st.prid=p.id where a.name like '%" & srchTextBox.Text & "%' and level='2' and a.jobsite='" & Request.QueryString("site") & "' order by p.id,a.section,a.name"
            End Select
            Dim cmd As SqlCommand
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(query, conn)
            cmd = New SqlCommand(query, conn)
            conn.Open()
            da.Fill(dt)
            conn.Close()
            srchResult.DataSource = dt
            srchResult.DataBind()
        Catch ex As Exception
            lblKet.Text = ex.Message
        End Try
    End Sub

    Private Sub srchResult_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles srchResult.RowDataBound
        Try
            e.Row.Attributes.Add("ondblclick", "javascript:executeClose(" & e.Row.Cells(0).Text & ")")
        Catch ex As Exception
            lblKet.Text = ex.Message
        End Try
    End Sub


End Class