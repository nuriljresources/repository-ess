﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/reminder/master.Master" CodeBehind="permit_list.aspx.vb" Inherits="EXCELLENT.permit_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Document Center Lists
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<script src="../Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>

    <style type="text/css" >
        #form1
        {
            font-family:Tahoma,Arial;
        }
        #form1 h1 
        {
            color: #FFFFFF;
            font-size:x-large;
        }
        .title
        {
            color: blue;
            font-size: small;
            margin-bottom:10px;
        }
        .line
        {
            width: 100%;
            height: 20px;
            font-family:Tahoma,Arial;
            font-size:0.8em;
        }
        .general
        {
            font-size:0.9em;
            color:#33ADFF;
            font-weight:bold;
            border-style:solid;
            border-width:1px;
            border-color:#33ADFF;
            padding-left:5px;
        }
        
        .general a
        {
            text-decoration:none;
            color:#fff;
        }
        
        .general a:hover
        {
            text-decoration:underline;
            color:#fff;
        }
        
        .year
        {
            font-size:0.9em;
            color:#33ADFF;
            font-weight:bold;
            border-style:solid;
            border-width:1px;
            border-color:#33ADFF;
            padding-left:5px;
        }
        
        .year a
        {
            text-decoration:none;
            color:#33ADFF;
        }
        
        .year a:hover
        {
            text-decoration:underline;
            color:#33ADFF;
        }
    </style>

    <script language="javascript" type="text/javascript">
// <!CDATA[

        function btnfind_onclick() {

        }

// ]]>
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="line" style="right:10px; text-align:right;">
    <a href="signout.aspx">Logout</a>
    </div>
    <div class="title">
        <strong>Menu</strong>
    </div>
    <div id="list_newest" class="general" style="width:200px; color:#33ADFF;">
        <div id="Div1"><img alt="open document" src="../images/open_doc_small.png" /> <a href="index.aspx">&nbsp;Home</a></div>
        <%=mnentry%>
        <%=mncontract%>
        <%=mnview%>
        <%=mnmgmt%>
        <div><img alt="open document" src="../images/open_doc_small.png" /> <a href="J Resources Document Center Manual Version 1.pdf" target="_blank">&nbsp;Help</a></div>
    </div>
    <div class="title">
        <strong>Search</strong>  
    </div>
    
    <div id="search" class="general" style="width:200px; font-size:0.7em; margin-bottom:5px; padding-bottom:5px; padding-top:5px;">
    Category
    <asp:DropDownList ID="ddlsearch" runat="server" style="width:125px;">
    <asp:ListItem Value="01">All</asp:ListItem>
    <asp:ListItem Value="02">Company</asp:ListItem>
    <asp:ListItem Value="03">Site</asp:ListItem>
   <%-- <asp:ListItem Value="04">Department</asp:ListItem>
    <asp:ListItem Value="05">Sub Department</asp:ListItem>
    <asp:ListItem Value="06">Doctype</asp:ListItem>--%>
    <asp:ListItem Value="07">File Name</asp:ListItem>
    <%--<asp:ListItem Value="08">Year</asp:ListItem>
    <asp:ListItem Value="09">Category</asp:ListItem>--%>
    </asp:DropDownList> <br />
    
     Keyword <asp:TextBox id="find"  style="width:120px;" runat="server" ></asp:TextBox> <asp:ImageButton ID="btnscrh" ImageUrl="~/images/Search.gif" runat="server" />
     </div>
    
     <div id="Div6" class="title" style="width:200px; position:relative;">
        <p><strong>Will Be Expired</strong></p>
    </div> 
    
    <div id="expired_list" class="general" style="width:200px; position:relative;">
        <%=listex%>
    </div> 
    
    <div class="line">
    <a href="rpt_expired_1month.aspx" target="_blank" style="color:Blue;"><strong>Report Details..</strong></a>
    </div>
    
    <div id="list_permit" class="general" style="width:1000px; border-style:none; height:400px; left:240px; top:20px; position:absolute; font-size:1.0em;">
       
        <asp:GridView ID="GridView1" runat="server" DataKeyNames="id" AutoGenerateColumns="False"
            AllowPaging="True" PageSize="9" Width="1000px" BorderColor="Black"
            BorderStyle="Solid">
            <Columns>
            <asp:CommandField ShowSelectButton="True" SelectText="Edit" ButtonType="Link" HeaderStyle-Width="40px" ItemStyle-VerticalAlign="top" >
                <HeaderStyle Width="40px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:CommandField>
            <%--<asp:CommandField ShowDeleteButton="true" DeleteText="Delete" ButtonType="Link" HeaderStyle-Width="40px" ItemStyle-VerticalAlign ="Top">
                <HeaderStyle Width="40px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:CommandField>--%>
            
            <asp:BoundField DataField="doctype" HeaderText="Type" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="fsubject" HeaderText="Subject" HeaderStyle-Width="200px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="200px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="company_name" HeaderText="Company" HeaderStyle-Width="400px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="400px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="site_name" HeaderText="Site" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
          <%--  <asp:BoundField DataField="fyear" HeaderText="Year" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="fcategory" HeaderText="Category" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>--%>
            <asp:BoundField DataField="Vendor" HeaderText="Supplier" HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top"> 
                <HeaderStyle Width="200px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="cms_start" HeaderText="Start Date" HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top"> 
                <HeaderStyle Width="200px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="cms_end" HeaderText="Expired Date" HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top"> 
                <HeaderStyle Width="200px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
           <%-- <asp:BoundField DataField="pic1" HeaderText="Doc Admin" HeaderStyle-Width="200px" ItemStyle-VerticalAlign="top"> 
                <HeaderStyle Width="200px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            <asp:TemplateField HeaderText="PIC" HeaderStyle-Width="100px">
                <ItemTemplate>
                    <asp:Label ID="PIC" Text='<%# eval("pic2") + "," + eval("pic3") + "," + eval("pic4") + "," + eval("pic5") %>' runat="server" ></asp:Label>
                </ItemTemplate> 
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="File Name" HeaderStyle-Width="300px">
                <ItemTemplate>
                    <%--<asp:HyperLink ID="HyperLink1" Text='<%# Bind("f_name") %>' NavigateUrl='<%#"~/permit/view_doc.aspx?ID=" + eval("doctype") + "_" + eval("fsubject") + "_" + eval("kdcom") + "_" + eval("kdsite") + "_" + eval("fyear") + "_" + eval("f_name") %>' Target="_blank" runat="server"></asp:HyperLink>--%>
                    <asp:HyperLink ID="HyperLink1" Text='<%# Bind("f_name") %>' NavigateUrl='<%#"http://moss.jresources.com/" + eval("f_path") %>' Target="_blank" runat="server"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField> 
            </Columns>
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#8F8F8F" Font-Bold="True" ForeColor="White" />
        </asp:GridView>
        <br />
        <asp:Label ID="lblinfo" runat="server"></asp:Label>
        <br />
        
        <asp:GridView ID="gvlistlp" runat="server" DataKeyNames="id" AutoGenerateColumns="False"
        AllowPaging="True" PageSize="9" Width="1000px" BorderColor="Black"
        BorderStyle="Solid">
        <Columns>
            <asp:CommandField ShowSelectButton="True" SelectText="Edit" ButtonType="Link" HeaderStyle-Width="40px" ItemStyle-VerticalAlign="top" >
                <HeaderStyle Width="40px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:CommandField>
            
            <asp:BoundField DataField="fsubject" HeaderText="Subject" HeaderStyle-Width="100px" ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            
            <asp:BoundField DataField="supplier_name" HeaderText="Supplier" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            
            <asp:BoundField DataField="f_name" HeaderText="Type" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            
            <asp:BoundField DataField="CreatedTime" HeaderText="Created Date" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            
            <asp:BoundField DataField="rem_code" HeaderText="Reminder Days" HeaderStyle-Width="100px"  ItemStyle-VerticalAlign="top">
                <HeaderStyle Width="100px"></HeaderStyle>
                <ItemStyle VerticalAlign="Top"></ItemStyle>
            </asp:BoundField>
            
        </Columns>
        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#8F8F8F" Font-Bold="True" ForeColor="White" />
        </asp:GridView>
            
    </div>

    <%=msginfo%>
</asp:Content>