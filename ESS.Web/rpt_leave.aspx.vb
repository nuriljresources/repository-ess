﻿Imports Microsoft.Reporting.WebForms
Partial Public Class rpt_leave
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'Dim newlistitem As ListItem
            txtdate1.Text = Today.Date.ToString("yyyy-MM-dd")
            txtdate2.Text = Today.Date.ToString("yyyy-MM-dd")
            hkdsite.Value = Session("kdsite").ToString

            'newlistitem = New ListItem("Departure and arrival", "1")
            'ddlrptlist.Items.Add(newlistitem)
            'newlistitem = New ListItem("Accomodation", "2")
            'ddlrptlist.Items.Add(newlistitem)

        End If
    End Sub

    Protected Sub btnok_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnok.Click
        report_retrieve()
    End Sub

    Public Sub report_retrieve()

        ReportViewer1.Visible = True
        ReportViewer1.LocalReport.DataSources.Clear()

        ReportViewer1.LocalReport.ReportPath = "laporan\rptleave.rdlc"
        Dim rds As New ReportDataSource("hr_dtleave", ObjectDataSource1)
        rds.DataSourceId = "ObjectDataSource1"
        rds.Name = "hr_dtleave"

        'Dim p1 As New ReportParameter("kdsite", hkdsite.Value)
        Dim p1 As New ReportParameter("date1", txtdate1.Text)
        Dim p2 As New ReportParameter("date2", txtdate2.Text)

        Me.ReportViewer1.LocalReport.SetParameters(New ReportParameter() {p1, p2})

        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.Refresh()
    End Sub
End Class