﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/site.master" CodeBehind="MyIssueList.aspx.vb" Inherits="EXCELLENT.MyIssueList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
Tugas Saya - Aplikasi MEMO
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <script src="Scripts/jsmlist.js" type="text/javascript"></script>  
    <script src="Scripts/calendar_us.js" type="text/javascript"></script> 
    <script language="javascript" type="text/javascript">
        function cekTanggal() {
            var d = document;
            var tg1 = d.getElementById("TxtTgl1").value;
            var tg2 = d.getElementById("TxtTgl2").value;

            var d11 = tg1.substring(0, 2);
            var d12 = tg1.substring(3, 5);
            var d13 = tg1.substring(6,10);

            var d21 = tg2.substring(0, 2);
            var d22 = tg2.substring(3, 5); 
            var d23 = tg2.substring(6,10);
            
            if (parseFloat(d23) < parseFloat(d13)) {
                alert("Date end must not smaller than date start ...!");
                return false;
            }
            if (parseFloat(d23) == parseFloat(d13)){
                if (parseFloat(d22) < parseFloat(d12)){
                    alert("Date end must not smaller than date start ...!");
                    return false;
                }
            }
            if (parseFloat(d23) == parseFloat(d13)) {
                if (parseFloat(d22) == parseFloat(d12)) {
                    if (parseFloat(d21) < parseFloat(d11)) {
                        alert("Date end must not smaller than date start ...!");
                        return false;
                    }
                }
            }
            return true;
        }
    </script>        
    <link href="css/calendar.css"rel="stylesheet" />   
    <link href="css/general.css" rel="stylesheet" type="text/css"/>
    <style type="text/css">
         ul#navigation li a.m3
         {        	
	        background-color:#00CC00;color: #000000;
         }
         ul.dropdown *.dir3 {
          padding-right: 20px;
          background-image: url("/memo/css/dropdown/themes/default/images/nav-arrow-down.png");
          background-position: 100% 50%;
          background-repeat: no-repeat;
          background-color: #FFA500;
         }
   </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
    <iframe id="helptext_canvas"></iframe>
    <asp:HiddenField ID="HNikUser" runat="Server" />
    <asp:HiddenField ID="HPermission" runat="Server" />
    <script type="text/javascript" src="Scripts/tooltip.js"></script>
    <div style="left: -1000px; top: 494px; visibility: hidden;" id="helptext"></div>

    <div id="divnav">
    <table id="Table1" cellspacing="2">
           <tr>
           <td style="border-style: solid; border-width: 1px; background-color: #FFFF66"><a href="#" onclick="retrieveIssue(1);">My Issue</a></td>
           <td style="border-style: solid; border-width: 1px; background-color: #FFFF66"><a href="#" onclick="showInfo(1,0);">Info to Me</a></td>
           <td>
               <input type="hidden" id="IdRap" />
               <input type="hidden" id="IdIssue" />
               <input type="hidden" id="NoAction" />
               <input type="hidden" id="NoProg" />
           </td>
           </tr>
    </table>    
    </div>    
          
    <div id="divbutton" align="left">
    <table><tr>
    <td align="left" width="10px">
        <input id="btnhide" type="button" value="Hide Filter" onclick="hideOption();" style="width: 150px; font-family: Calibri;"  />
        <input id="btnshow" type="button" value="Show Filter" onclick="showOption();" style="width: 150px; font-family: Calibri;"/>
      
     </td>
     <td>
       <% If Session("permission")="1" Or Session("permission")="2" Or Session("permission")="8" then   %>        
         <input id="btnshowmytask" type="button" value="Show My Task" onclick="showmytask();" style="width: 150px; font-family: Calibri;"/>
        <% End If %>
     </td>
    </tr>
    </table>   
    </div>
               
         <div id="criteria" style="width:500px">
         <div id="divfilter1" align="right">
            <table>
            <tr>
            <td>
                <a href="#" onclick="showFilterSt()">>></a>
                Date</td>
                <td>
                <select id="seltanggal" style="width: 80px">
                    <option value="rap" selected="selected">Meeting</option>
                    <option value="due" >Due To</option>
                </select>
            </td>
            <td>
            <input id="TxtTgl1" type="text" disabled="disabled" style="width: 80px" />
            <script language="JavaScript" type="text/javascript">
                new tcal({
                    'formname': 'aspnetForm',
                    'controlname': 'TxtTgl1',
                    'id': 'Tgl1'
                });
            </script>     
            </td>
            <td>        
            <label id="lblsd"> to </label>
            </td>
            <td style="width: 25%" >
            <input id="TxtTgl2" type="text" disabled="disabled" style="width: 80px" />
            <script language="JavaScript" type="text/javascript">
                new tcal({
                      'formname': 'aspnetForm',
                      'controlname': 'TxtTgl2',
                      'id': 'Tgl2'
                });
            </script> 
            </td>
            <td>
                <input id="BtnFilter1" type="button" value="Find" 
                    onclick="cari(0,0);cekTanggal();" style="width: 50px; font-family: Calibri;" />
            </td>
            </tr> 
            </table>        
        </div>
        
        <div id="divfilter3" align="right">
            <table>
            <tr>
            <td>
                <a href="#" onclick="showFilterTg()">>></a>
                Status
            </td>
            <td align="left">
                <select id="cmbsts" style="width: 80px">
                    <option value="Open" selected="selected">Open</option>
                    <option value="Close">Close</option>                    
                </select>
            </td>
            <td>
                <input id="BtnFilter3" type="button" value="Find" 
                    onclick="cari(1,0)" style="width: 50px; font-family: Calibri;" />
            </td>
            </tr>
            </table>  
        </div>              
        </div>
        
        <input id="TxtNIK" type="text" value="<%=Session("otorisasi")%>" 
        style="visibility: hidden;" disabled="disabled" />
        <input id="TxtPage" type="text" style="visibility: hidden; width: 58px;" 
        disabled="disabled"/>
            
        <div id="divlinfo" align="right">            
        </div>
        
        <div id="divlist">             
        </div> 
        
        <div id="divinfo">             
        </div>
        
        <div id="divedit">   
        </div>
    
        <div id="divkomen"> 
        </div>
       
        <script language="JavaScript" type="text/javascript">
           retrieveIssue(1);
           hideOption();
        </script>
</asp:Content>