﻿var gIssue = new Array();var nikpic = "";
var tempissue = ""; var tempaction = ""; 
var gpage = 0;var gpageop = 0;
var batas = 10; var firstfind = true;
var nlast = 0;
var doc = document;
var halaman = "";
var n1 = "";
var n2 = "";
var n3 = "";

function retrieveIssueHead(page) {
    document.getElementById("menutop").style.display = "none";
    document.getElementById("divbutton").style.display = "block";
    var nik = document.getElementById('TxtNIK').value;
    nikpic = nik;
    gpage = page;

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "WS/MyList.asmx/GetMasterList",
        data: "{'_page':" + gpage + ",'_iduser':" + nikpic + "}",
        dataType: "json",
        success: function(res) {
            MyIssue = new Array();
            MyIssue = res.d.MyTaskList;
            gIssue = MyIssue;

            var slist = '<center>';
            slist = slist + '<table id="tabgrid" width="100%" cellpadding="0" cellspacing="0">';
            slist = slist + '<caption class="captiongrid" />Daftar Issue</caption>';
            slist = slist + '<col width="30px"><col width="180px"><col width="*px"><col width="150px"><col width="30px"><col width="150px"><col width="150px"><col width="30px"><col width="80px">';
            slist = slist + '<tr>';
            slist = slist + '<th class="thstart">No</th><th class="thstart">No Notulen</th><th class="thstart">Isu</th>';
            slist = slist + '<th class="thstart">Progress Terakhir</th><th class="thstart">Jatuh Tempo</th><th class="thstart">PIC</th>';
            slist = slist + '<th class="thstart">Infokan Ke</th><th class="thstart">Status Isu</th><th class="thstart">Tindakan</th>';
            slist = slist + '</tr>';

            var j = 0;
            for (var i = 0; i < res.d.MyTaskList.length; i++) {
                var ind = MyIssue[i].Indeks;
                var sid = MyIssue[i].IDMeet;
                var sis = MyIssue[i].NoIssue;
                var sia = MyIssue[i].NoRef;
                var pic = MyIssue[i].Nmpic;
                var nin = MyIssue[i].NmInfo;
                var sts = (MyIssue[i].status == '0') ? 'Open' : 'Close';
                var sign = getColor(MyIssue[i].Flag);
               
                n1 = MyIssue[0].Indeks;
                n2 = MyIssue[i].Indeks;
                n3 = MyIssue[i].MaxRecord;

                slist = slist + '<tr>';
                slist = slist + '<td class="tdstart" align="left"' + sign + '>' + ind + '</td><td class="tdstart" ' + sign + '>' + MyIssue[i].IDMeet + '</td>';
                slist = slist + '<td class="tdstart" align="left"' + sign + '>' + MyIssue[i].NmIssue + '</td>';
                slist = slist + '<td class="tdstart" align="center"' + sign + '>' + MyIssue[i].Output + '</td>';
                slist = slist + '<td class="tdstart" ' + sign + '>' + MyIssue[i].due + ' </td>';
                slist = slist + '<td class="tdstart" align="center" ' + sign + '>' + getSplit(pic) + ' </td>';
                slist = slist + '<td class="tdstart" align="center"' + sign + '>' + getSplit(nin) + '</td>';
                if (sts == 'Open') {
                    slist = slist + '<td class="tdstart" align="center">' + sts + '</td>';
                }
                else {
                    slist = slist + '<td class="tdstart" align="center" style="color: #0000FF">' + sts + '</td>';
                }
                slist = slist + '<td class="tdend" align="center"><input id="btntindakan" type="button" value="Detail" onclick="retrieveActionHead(\'' + sid + '\',\'' + sis + '\')" ></td>';
                slist = slist + '</tr>';
            }

            halaman = n1 + ' - ' + n2 + ' dari ' + n3;

            slist = slist + '<tr><td colspan="8"><br/>' + halaman + '</td></tr>';
            slist = slist + '<tr><td colspan="8"><br/></td></tr>';
            slist = slist + '<tr><td colspan="8" align="left"><a href="#" onclick="prevPage(\'common\')"><< Prev</a>';
            slist = slist + '                                         ';
            slist = slist + '<a href="#" onclick="nextPage(\'common\')">Next >></a></td></tr>';
            slist = slist + '</table></center>';

            document.getElementById('divaction').innerHTML = "";
            document.getElementById('divkomen').innerHTML = "";
            tempissue = slist;

            if (res.d.MyTaskList.length > 0) {
                nlast = 0;
                tmpissue = slist;
                document.getElementById('divmain').innerHTML = tmpissue;
            }
            else {
                nlast = 1;
                gpage = gpage - 1;
                document.getElementById('divmain').innerHTML = tmpissue;
            }
        },
        error: function(err) {
            alert(err.responseText);
        }
    });   
}

function cari() {
    gpageop = 0;
    if (doc.getElementById('chktgl').checked == false) {
        if (doc.getElementById('chkjob').checked == false) {
            if (doc.getElementById('chksts').checked == false) {
                retrieveIssueHead(1);
            }
            else {retrieveIssueOpsi(0);}
        }
        else {retrieveIssueOpsi(0);}
    }
    else {retrieveIssueOpsi(0);}       
}

function retrieveIssueOpsi(nflag) {    
    doc.getElementById("btnshow").style.display = "none";
    doc.getElementById("btnhide").style.display = "block";

    var argtg1 = doc.getElementById('TxtTgl1').value;
    var argtg2 = doc.getElementById('TxtTgl2').value;
    var argcbg = doc.getElementById('cmbjob').value;
    var argsts = doc.getElementById('cmbsts').value;
    var jnstgl = doc.getElementById('seltanggal').value;

    if (doc.getElementById('chktgl').checked == false) {
        argtg1 = ''; argtg2 = '';
    }
    if (doc.getElementById('chkjob').checked == false) {
        argcbg = '';
    }
    if (doc.getElementById('chksts').checked == false) {
        argsts = '';
    }
    
    var obCari = new objOpsi(argtg1, argtg2, argcbg, argsts);    
    var nik = document.getElementById('TxtNIK').value;
    nikpic = nik;

    if (gpageop == 0) { gpageop = 1; }

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "WS/MyList.asmx/GetMasterListOpsi",
        data: "{'_page':" + gpageop + ",'_opsi':" + JSON.stringify(obCari) + ",'_iduser':" + nikpic + ",'_jnstgl':'" + jnstgl + "'}",
        dataType: "json",
        success: function(res) {
            MyIssue = new Array();
            MyIssue = res.d.MyTaskList;
            gIssue = MyIssue;
            
            var slist = '<center>';
            slist = slist + '<table id="tabgrid" width="100%" cellpadding="0" cellspacing="0">';
            slist = slist + '<caption class="captiongrid" />Daftar Issue Berdasar Pilihan Kriteria</caption>';
            slist = slist + '<col width="30px"><col width="180px"><col width="*px"><col width="150px"><col width="30px"><col width="150px"><col width="150px"><col width="60px"><col width="30px">';
            slist = slist + '<tr>';
            slist = slist + '<th class="thstart">No</th><th class="thstart">No Notulen</th><th class="thstart">Isu</th>';
            slist = slist + '<th class="thstart">Progress Terakhir</th><th class="thstart">Jatuh Tempo</th><th class="thstart">PIC</th>';
            slist = slist + '<th class="thstart">Infokan Ke</th><th class="thstart">Status Isu</th><th class="thstart">Tindakan</th>';
            slist = slist + '</tr>';

            var j = 0;
            for (var i = 0; i < res.d.MyTaskList.length; i++) {
                var ind = MyIssue[i].Indeks;
                var sid = MyIssue[i].IDMeet;
                var sis = MyIssue[i].NoIssue;
                var sia = MyIssue[i].NoRef;
                var pic = MyIssue[i].Nmpic;
                var nin = MyIssue[i].NmInfo;
                var sts = (MyIssue[i].status == '0') ? 'Open' : 'Close';
                var sign = getColor(MyIssue[i].Flag);

                n1 = MyIssue[0].Indeks;
                n2 = MyIssue[i].Indeks;
                n3 = MyIssue[i].MaxRecord;

                slist = slist + '<tr>';
                slist = slist + '<td class="tdstart" align="left" ' + sign + ' >' + ind + '</td><td class="tdstart" ' + sign + ' >' + MyIssue[i].IDMeet + '</td>';
                slist = slist + '<td class="tdstart" align="left" ' + sign + ' >' + MyIssue[i].NmIssue + '</td>';
                slist = slist + '<td class="tdstart" align="center" ' + sign + ' >' + MyIssue[i].Output + '</td>';
                slist = slist + '<td class="tdstart" ' + sign + ' >' + MyIssue[i].due + ' </td>';
                slist = slist + '<td class="tdstart" align="center" ' + sign + ' >' + getSplit(pic) + ' </td>';
                slist = slist + '<td class="tdstart" align="center" ' + sign + ' >' + getSplit(nin) + '</td>';
                if (sts == 'Open') {
                    slist = slist + '<td class="tdstart" align="center">' + sts + '</td>';
                }
                else {
                    slist = slist + '<td class="tdstart" align="center" style="color: #0000FF">' + sts + '</td>';
                }
                slist = slist + '<td class="tdend"><input id="btntindakan" type="button" value="Detail" onclick="retrieveActionHead(\'' + sid + '\',\'' + sis + '\')" ></td>';
                slist = slist + '</tr>';
            }

            halaman = n1 + ' - ' + n2 + ' dari ' + n3;
            
            slist = slist + '<tr><td colspan="8"><br/>' + halaman + '</td></tr>';
            slist = slist + '<tr><td colspan="8"><br/></td></tr>';
            slist = slist + '<tr><td colspan="8" align="left"><a href="#" onclick="prevPage(\'option\')"><< Prev</a>';
            slist = slist + '                                         ';
            slist = slist + '<a href="#" onclick="nextPage(\'option\')">Next >></a></td></tr>';
            slist = slist + '</table></center>';

            document.getElementById('divaction').innerHTML = "";
            document.getElementById('divkomen').innerHTML = "";

            if (res.d.MyTaskList.length > 0) {
                nlast = 0;
                tmpissue = slist;
                document.getElementById('divmain').innerHTML = tmpissue;
            }
            else {
                //nlast = 1;
                gpageop = gpageop - 1;
                if (nflag == 0) {
                    document.getElementById('divmain').innerHTML = getMessage('DATA TIDAK DITEMUKAN');
                }
                else {
                    document.getElementById('divmain').innerHTML = tmpissue;
                }
            }
        },
        error: function(err) {
            alert(err.responseText);
        }
    });  
}

function retrieveActionHead(idr, idi) {
    hideOption();
    document.getElementById("divbutton").style.display = "none";
    document.getElementById("menutop").style.display = "block";

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "WS/MyList.asmx/GetMasterTindakan",
        data: "{'_IdRap':'" + idr + "','_IdIssue':'" + idi + "'}",
        dataType: "json",
        success: function(res) {
            MyIssue = new Array();
            MyIssue = res.d.MyTaskList;
            gIssue = MyIssue;

            var slist = '<center>';
            slist = slist + '<table id="tabaction" width="100%" cellpadding="0" cellspacing="0">';
            slist = slist + '<caption class="captiongrid" />Issue : ' + MyIssue[0].NmIssue + '</caption>';
            slist = slist + '<col width="30px"><col width="*px"><col width="250px"><col width="100px"><col width="150px"><col width="30px"><col width="30px">';
            slist = slist + '<tr><th class="thstart">No</th><th class="thstart">Tindakan</th><th class="thstart">Progress Terakhir</th>';
            slist = slist + '<th class="thstart">Target Hasil</th><th class="thstart">PIC</th><th class="thstart">Status Tindakan</th><th class="thstart">Jatuh Tempo</th></tr>';

            var j = 0;
            for (var i = 0; i < res.d.MyTaskList.length; i++) {
                var sts = (MyIssue[i].status == '0') ? 'Open' : 'Close';

                j = j + 1;
                slist = slist + '<tr>';
                slist = slist + '<td class="tdstart" align="left">' + (j) + '</td>';
                slist = slist + '<td class="tdstart" align="left">' + MyIssue[i].Action + '</td>';
                slist = slist + '<td class="tdstart" align="center">' + MyIssue[i].Output + '</td>';
                slist = slist + '<td class="tdstart" align="center">' + MyIssue[i].Deliverable + '</td>';
                slist = slist + '<td class="tdstart" align="center">' + MyIssue[i].Nmpic + '</td>';
                if (sts == 'Open') {
                    slist = slist + '<td class="tdstart" align="center">' + sts + '</td>';
                }
                else {
                    slist = slist + '<td class="tdstart" align="center" style="color: #0000FF">' + sts + '</td>';
                }                
                slist = slist + '<td class="tdend" align="center">' + MyIssue[i].due + '</td>';
                slist = slist + '</tr>';
            }
            slist = slist + '</table></center>';

            document.getElementById('divaction').innerHTML = slist;
            document.getElementById('divmain').innerHTML = "";
            document.getElementById('divkomen').innerHTML = "";
            tempaction = slist;
        },
        error: function(err) {
            alert(err.responseText);
        }
    });
}

function getSplit(argumen) {    
    var myArr = new Array();
    myArr = argumen.split(",");

    var chasil = "";
    for (var i = 0; i < myArr.length-1; i++) {
        chasil = chasil + myArr[i] + "<br/>";
    }
    return chasil;
}

function nextPage(jenis) {
    if (nlast != 1) {    
        if (jenis == "common") {
            gpage = gpage + 1;
            retrieveIssueHead(gpage);
        }
        if (jenis == "option") {
            gpageop = gpageop + 1;
            retrieveIssueOpsi(gpageop);
        }
    }
}

function prevPage(jenis) {  
    // STEP 1
    if (gpage == 1) {
        gpage = 2;
    }
    if ((gpageop == 0) || (gpageop == 1)) {
        gpageop = 2;
    }

    // STEP 2
    if (jenis == "common") {
        gpage = gpage - 1;
        retrieveIssueHead(gpage);
    }
    if (jenis == "option") {
        gpageop = gpageop - 1;
        retrieveIssueOpsi(gpageop);
    }
}

function initPage() {
    document.getElementById("btnhide").style.display = "none";
    document.getElementById("divopsi1").style.display = "none";
    document.getElementById("divopsi2").style.display = "none";
}

function showOption() {
    doc.getElementById("btnhide").style.display = "block";
    doc.getElementById("btnshow").style.display = "none";
    doc.getElementById("divopsi1").style.display = "block";
    doc.getElementById("divopsi2").style.display = "block";
    doc.getElementById("btncari").style.display = "block";
    doc.getElementById("criteria").style.border = "dashed orange 1px";
}

function hideOption() {
    doc.getElementById("btnshow").style.display = "block";
    doc.getElementById("btnhide").style.display = "none";
    doc.getElementById("divopsi1").style.display = "none";
    doc.getElementById("divopsi2").style.display = "none";
    doc.getElementById("btncari").style.display = "none";
    doc.getElementById("criteria").style.border = "solid 0px";
}

function objOpsi(Tgl1, Tgl2, KdCab, Status) {
    this.Tgl1 = Tgl1; this.Tgl2 = Tgl2; this.KdCab = KdCab; this.Status = Status;
}

function getMessage(pesan) {
    return '<center><p><b style="font-size: x-large; color: #0066FF">' + pesan + '</b></p></center>';
}

function getColor(flag){
    var warna = "";
    /* jangan hilangkan spasi di nilai warna */
    switch (flag) {
        case 'R':
            warna = ' style="color: #FF0000" ';
            break;
        case 'Y':
            warna = ' style="color: #CCCC00" ';
            break;
        default:
            warna = ' ';
    }
    return warna
}